﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_NhapThoiKhoaBieu.aspx.cs" Inherits="admin_page_module_function_module_LichCongTac_module_NhapThoiKhoaBieu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <style>
        .inputmon {
            width: 100%;
            padding-left: 5px;
        }

        .inputtextarea {
            width: 100%;
        }

        .table {
            width: 500px;
        }

            .table th, .table td {
                border: 1px solid #a6a9ab;
                text-align: center;
            }

            .table tr.tbheader {
                background: #a3a7a199;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="card card-block">
        <div class="form-group row">
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <div class="col-12">
                        <div class="col-2 form-group">
                            <label class="col-5 form-control-label">Lớp học:</label>
                            <asp:DropDownList ID="ddlLop" runat="server" CssClass=""></asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="col-6">
                            <div runat="server" id="dvThu2">
                                <table class="table table-hover">
                                    <tr class="tbheader">
                                        <th style="text-align: center; width: 80px">Thứ</th>
                                        <th style="text-align: center; width: 80px">Buổi</th>
                                        <th style="text-align: center; width: 80px">Tiết</th>
                                        <th style="text-align: center">Môn</th>
                                    </tr>
                                    <%--thứ 2--%>
                                    <tr>
                                        <td rowspan="8">Thứ 2</td>
                                        <td rowspan="4" style="text-align: center">Sáng</td>
                                        <td style="text-align: center">1</td>
                                        <td style="text-align: center">
                                            <input class="inputmon" runat="server" id="txtThu2Tiet1_Mon" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center">2</td>
                                        <td style="text-align: center">
                                            <input class="inputmon" runat="server" id="txtThu2Tiet2_Mon" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center">3</td>
                                        <td style="text-align: center">
                                            <input class="inputmon" runat="server" id="txtThu2Tiet3_Mon" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center">4</td>
                                        <td style="text-align: center">
                                            <input class="inputmon" runat="server" id="txtThu2Tiet4_Mon" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td rowspan="4" style="text-align: center">Chiều</td>
                                        <td style="text-align: center">5</td>
                                        <td style="text-align: center">
                                            <input class="inputmon" runat="server" id="txtThu2Tiet5_Mon" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center">6</td>
                                        <td style="text-align: center">
                                            <input class="inputmon" runat="server" id="txtThu2Tiet6_Mon" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center">7</td>
                                        <td style="text-align: center">
                                            <input class="inputmon" runat="server" id="txtThu2Tiet7_Mon" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center">8</td>
                                        <td style="text-align: center">
                                            <input class="inputmon" runat="server" id="txtThu2Tiet8_Mon" />
                                        </td>
                                    </tr>
                                    <%-- <tr>
                            <td colspan="6" style="text-align: center">
                                <asp:Button ID="btnLuuThu2" runat="server" Text="Tiếp tục" CssClass="btn btn-primary" OnClientClick="return checkNULL()" OnClick="btnLuuThu2_Click" />
                            </td>
                        </tr>--%>
                                </table>
                            </div>
                        </div>
                        <div class="col-6">
                            <div runat="server" id="dvThu3">
                                <table class="table table-hover">
                                    <tr class="tbheader">
                                        <th style="text-align: center; width: 80px">Thứ</th>
                                        <th style="text-align: center; width: 80px">Buổi</th>
                                        <th style="text-align: center; width: 80px">Tiết</th>
                                        <th style="text-align: center">Môn</th>
                                    </tr>
                                    <%--thứ 3--%>
                                    <tr>
                                        <td rowspan="8">Thứ 3</td>
                                        <td rowspan="4" style="text-align: center">Sáng</td>
                                        <td style="text-align: center">1</td>
                                        <td style="text-align: center">
                                            <input class="inputmon" runat="server" id="txtThu3Tiet1_Mon" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center">2</td>
                                        <td style="text-align: center">
                                            <input class="inputmon" runat="server" id="txtThu3Tiet2_Mon" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center">3</td>
                                        <td style="text-align: center">
                                            <input class="inputmon" runat="server" id="txtThu3Tiet3_Mon" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center">4</td>
                                        <td style="text-align: center">
                                            <input class="inputmon" runat="server" id="txtThu3Tiet4_Mon" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td rowspan="4" style="text-align: center">Chiều</td>
                                        <td style="text-align: center">5</td>
                                        <td style="text-align: center">
                                            <input class="inputmon" runat="server" id="txtThu3Tiet5_Mon" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center">6</td>
                                        <td style="text-align: center">
                                            <input class="inputmon" runat="server" id="txtThu3Tiet6_Mon" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center">7</td>
                                        <td style="text-align: center">
                                            <input class="inputmon" runat="server" id="txtThu3Tiet7_Mon" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center">8</td>
                                        <td style="text-align: center">
                                            <input class="inputmon" runat="server" id="txtThu3Tiet8_Mon" />
                                        </td>
                                    </tr>
                                    <%-- <tr>
                            <td colspan="6" style="text-align: center">
                                <asp:Button ID="btnLuuThu3" runat="server" Text="Tiếp tục" CssClass="btn btn-primary" OnClick="btnLuuThu3_Click" />
                            </td>
                        </tr>--%>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="col-6">
                            <div runat="server" id="dvThu4">
                                <table class="table table-hover">
                                    <tr class="tbheader">
                                        <th style="text-align: center; width: 80px">Thứ</th>
                                        <th style="text-align: center; width: 80px">Buổi</th>
                                        <th style="text-align: center; width: 80px">Tiết</th>
                                        <th style="text-align: center">Môn</th>
                                    </tr>
                                    <%--thứ 4--%>
                                    <tr>
                                        <td rowspan="8">Thứ 4</td>
                                        <td rowspan="4" style="text-align: center">Sáng</td>
                                        <td style="text-align: center">1</td>
                                        <td style="text-align: center">
                                            <input class="inputmon" runat="server" id="txtThu4Tiet1_Mon" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center">2</td>
                                        <td style="text-align: center">
                                            <input class="inputmon" runat="server" id="txtThu4Tiet2_Mon" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center">3</td>
                                        <td style="text-align: center">
                                            <input class="inputmon" runat="server" id="txtThu4Tiet3_Mon" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center">4</td>
                                        <td style="text-align: center">
                                            <input class="inputmon" runat="server" id="txtThu4Tiet4_Mon" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td rowspan="4" style="text-align: center">Chiều</td>
                                        <td style="text-align: center">5</td>
                                        <td style="text-align: center">
                                            <input class="inputmon" runat="server" id="txtThu4Tiet5_Mon" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center">6</td>
                                        <td style="text-align: center">
                                            <input class="inputmon" runat="server" id="txtThu4Tiet6_Mon" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center">7</td>
                                        <td style="text-align: center">
                                            <input class="inputmon" runat="server" id="txtThu4Tiet7_Mon" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>8</td>
                                        <td style="text-align: center">
                                            <input class="inputmon" runat="server" id="txtThu4Tiet8_Mon" />
                                        </td>
                                    </tr>
                                    <%--  <tr>
                                <td colspan="6" style="text-align: center">
                                    <asp:Button ID="btnLuuThu4" runat="server" Text="Tiếp tục" CssClass="btn btn-primary" OnClick="btnLuuThu4_Click" />
                                </td>
                            </tr>--%>
                                </table>
                            </div>
                        </div>
                        <div class="col-6">
                            <div runat="server" id="dvThu5">
                                <table class="table table-hover">
                                    <tr class="tbheader">
                                        <th style="text-align: center; width: 80px">Thứ</th>
                                        <th style="text-align: center; width: 80px">Buổi</th>
                                        <th style="text-align: center; width: 80px">Tiết</th>
                                        <th style="text-align: center">Môn</th>
                                    </tr>
                                    <%--thứ 5--%>
                                    <tr>
                                        <td rowspan="8">Thứ 5</td>
                                        <td rowspan="4" style="text-align: center">Sáng</td>
                                        <td style="text-align: center">1</td>
                                        <td style="text-align: center">
                                            <input class="inputmon" runat="server" id="txtThu5Tiet1_Mon" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center">2</td>
                                        <td style="text-align: center">
                                            <input class="inputmon" runat="server" id="txtThu5Tiet2_Mon" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center">3</td>
                                        <td style="text-align: center">
                                            <input class="inputmon" runat="server" id="txtThu5Tiet3_Mon" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center">4</td>
                                        <td style="text-align: center">
                                            <input class="inputmon" runat="server" id="txtThu5Tiet4_Mon" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td rowspan="4" style="text-align: center">Chiều</td>
                                        <td style="text-align: center">5</td>
                                        <td style="text-align: center">
                                            <input class="inputmon" runat="server" id="txtThu5Tiet5_Mon" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center">6</td>
                                        <td style="text-align: center">
                                            <input class="inputmon" runat="server" id="txtThu5Tiet6_Mon" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center">7</td>
                                        <td style="text-align: center">
                                            <input class="inputmon" runat="server" id="txtThu5Tiet7_Mon" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center">8</td>
                                        <td style="text-align: center">
                                            <input class="inputmon" runat="server" id="txtThu5Tiet8_Mon" />
                                        </td>
                                    </tr>
                                    <%--<tr>
                                <td colspan="6" style="text-align: center">
                                    <asp:Button ID="btnLuuThu5" runat="server" Text="Tiếp tục" CssClass="btn btn-primary" OnClick="btnLuuThu5_Click" />
                                </td>
                            </tr>--%>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="col-6">
                            <div runat="server" id="dvThu6">
                                <table class="table table-hover">
                                    <tr class="tbheader">
                                        <th style="text-align: center; width: 80px">Thứ</th>
                                        <th style="text-align: center; width: 80px">Buổi</th>
                                        <th style="text-align: center; width: 80px">Tiết</th>
                                        <th style="text-align: center">Môn</th>
                                    </tr>
                                    <%--thứ 6--%>
                                    <tr>
                                        <td rowspan="8">Thứ 6</td>
                                        <td rowspan="4" style="text-align: center">Sáng</td>
                                        <td style="text-align: center">1</td>
                                        <td style="text-align: center">
                                            <input class="inputmon" runat="server" id="txtThu6Tiet1_Mon" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center">2</td>
                                        <td style="text-align: center">
                                            <input class="inputmon" runat="server" id="txtThu6Tiet2_Mon" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center">3</td>
                                        <td style="text-align: center">
                                            <input class="inputmon" runat="server" id="txtThu6Tiet3_Mon" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center">4</td>
                                        <td style="text-align: center">
                                            <input class="inputmon" runat="server" id="txtThu6Tiet4_Mon" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td rowspan="4" style="text-align: center">Chiều</td>
                                        <td style="text-align: center">5</td>
                                        <td style="text-align: center">
                                            <input class="inputmon" runat="server" id="txtThu6Tiet5_Mon" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center">6</td>
                                        <td style="text-align: center">
                                            <input class="inputmon" runat="server" id="txtThu6Tiet6_Mon" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center">7</td>
                                        <td style="text-align: center">
                                            <input class="inputmon" runat="server" id="txtThu6Tiet7_Mon" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center">8</td>
                                        <td style="text-align: center">
                                            <input class="inputmon" runat="server" id="txtThu6Tiet8_Mon" />
                                        </td>
                                    </tr>
                                    <%-- <tr>
                                <td colspan="6" style="text-align: center">
                                    <asp:Button ID="btnLuuThu6" runat="server" Text="Hoàn thành" CssClass="btn btn-primary" OnClick="btnLuuThu6_Click" />
                                </td>
                            </tr>--%>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <a href="javascript:void(0)" class="btn btn-primary" runat="server" id="btnHoanThanh" onserverclick="btnHoanThanh_ServerClick">Hoàn thành</a>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

