﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_LichCongTac_module_NhapThoiKhoaBieu : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //hiển thị ds khối
            var listLop = from l in db.tbLops select l;
            ddlLop.Items.Clear();
            ddlLop.Items.Insert(0, new ListItem("--Chọn lớp--", "0"));
            ddlLop.AppendDataBoundItems = true;
            ddlLop.DataTextField = "lop_name";
            ddlLop.DataValueField = "lop_id";
            ddlLop.DataSource = listLop;
            ddlLop.DataBind();
        }
    }

    protected void btnHoanThanh_ServerClick(object sender, EventArgs e)
    {
        if (Convert.ToInt32(ddlLop.SelectedValue) == 0)
        {
            alert.alert_Warning(Page, "Bạn chưa chọn lớp học", "");
        }
        else
        {
            for (int i = 2; i <= 6; i++)
            {
                //lưu thứ 2
                if (i == 2)
                {
                    for (int j = 1; j <= 8; j++)
                    {
                        tbThoiKhoaBieu insertT2 = new tbThoiKhoaBieu();
                        insertT2.lop_id = Convert.ToInt32(ddlLop.SelectedValue);
                        insertT2.thoikhoabieu_thu = "Thứ " + i;
                        insertT2.thoikhoabieu_tiet = "Tiết " + j;
                        if (j == 1)
                        {
                            //nếu j = 1 thì insert dữ liệu của tiết 1
                            insertT2.monhoc_name = txtThu2Tiet1_Mon.Value;
                        }
                        else if (j == 2)
                        {
                            //nếu j = 2 thì insert dữ liệu của tiết 2
                            insertT2.monhoc_name = txtThu2Tiet2_Mon.Value;
                        }
                        else if (j == 3)
                        {
                            //nếu j = 3 thì insert dữ liệu của tiết 3
                            insertT2.monhoc_name = txtThu2Tiet3_Mon.Value;
                        }
                        else if (j == 4)
                        {
                            //nếu j = 4 thì insert dữ liệu của tiết 4
                            insertT2.monhoc_name = txtThu2Tiet4_Mon.Value;
                        }
                        else if (j == 5)
                        {
                            //nếu j = 5 thì insert dữ liệu của tiết 5
                            insertT2.monhoc_name = txtThu2Tiet5_Mon.Value;
                        }
                        else if (j == 6)
                        {
                            //nếu j = 6 thì insert dữ liệu của tiết 6
                            insertT2.monhoc_name = txtThu2Tiet6_Mon.Value;
                        }
                        else if (j == 7)
                        {
                            //nếu j = 7 thì insert dữ liệu của tiết 7
                            insertT2.monhoc_name = txtThu2Tiet7_Mon.Value;
                        }
                        else
                        {
                            // j = 8 thì insert dữ liệu của tiết 8
                            insertT2.monhoc_name = txtThu2Tiet8_Mon.Value;
                        }
                        db.tbThoiKhoaBieus.InsertOnSubmit(insertT2);
                        db.SubmitChanges();
                    }
                }
                //lưu thứ 3
                if (i == 3)
                {
                    for (int j = 1; j <= 8; j++)
                    {
                        tbThoiKhoaBieu insertT3 = new tbThoiKhoaBieu();
                        insertT3.lop_id = Convert.ToInt32(ddlLop.SelectedValue);
                        insertT3.thoikhoabieu_thu = "Thứ " + i;
                        insertT3.thoikhoabieu_tiet = "Tiết " + j;
                        if (j == 1)
                        {
                            //nếu j = 1 thì insert dữ liệu của tiết 1
                            insertT3.monhoc_name = txtThu3Tiet1_Mon.Value;
                        }
                        else if (j == 2)
                        {
                            //nếu j = 2 thì insert dữ liệu của tiết 2
                            insertT3.monhoc_name = txtThu3Tiet2_Mon.Value;
                        }
                        else if (j == 3)
                        {
                            //nếu j = 3 thì insert dữ liệu của tiết 3
                            insertT3.monhoc_name = txtThu3Tiet3_Mon.Value;
                        }
                        else if (j == 4)
                        {
                            //nếu j = 4 thì insert dữ liệu của tiết 4
                            insertT3.monhoc_name = txtThu3Tiet4_Mon.Value;
                        }
                        else if (j == 5)
                        {
                            //nếu j = 5 thì insert dữ liệu của tiết 5
                            insertT3.monhoc_name = txtThu3Tiet5_Mon.Value;
                        }
                        else if (j == 6)
                        {
                            //nếu j = 6 thì insert dữ liệu của tiết 6
                            insertT3.monhoc_name = txtThu3Tiet6_Mon.Value;
                        }
                        else if (j == 7)
                        {
                            //nếu j = 7 thì insert dữ liệu của tiết 7
                            insertT3.monhoc_name = txtThu3Tiet7_Mon.Value;
                        }
                        else
                        {
                            // j = 8 thì insert dữ liệu của tiết 8
                            insertT3.monhoc_name = txtThu3Tiet8_Mon.Value;
                        }
                        db.tbThoiKhoaBieus.InsertOnSubmit(insertT3);
                        db.SubmitChanges();
                    }
                }
                //lưu thứ 4
                if (i == 4)
                {
                    for (int j = 1; j <= 8; j++)
                    {
                        tbThoiKhoaBieu insertT4 = new tbThoiKhoaBieu();
                        insertT4.lop_id = Convert.ToInt32(ddlLop.SelectedValue);
                        insertT4.thoikhoabieu_thu = "Thứ " + i;
                        insertT4.thoikhoabieu_tiet = "Tiết " + j;
                        if (j == 1)
                        {
                            //nếu j = 1 thì insert dữ liệu của tiết 1
                            insertT4.monhoc_name = txtThu4Tiet1_Mon.Value;
                        }
                        else if (j == 2)
                        {
                            //nếu j = 2 thì insert dữ liệu của tiết 2
                            insertT4.monhoc_name = txtThu4Tiet2_Mon.Value;
                        }
                        else if (j == 3)
                        {
                            //nếu j = 3 thì insert dữ liệu của tiết 3
                            insertT4.monhoc_name = txtThu4Tiet3_Mon.Value;
                        }
                        else if (j == 4)
                        {
                            //nếu j = 4 thì insert dữ liệu của tiết 4
                            insertT4.monhoc_name = txtThu4Tiet4_Mon.Value;
                        }
                        else if (j == 5)
                        {
                            //nếu j = 5 thì insert dữ liệu của tiết 5
                            insertT4.monhoc_name = txtThu4Tiet5_Mon.Value;
                        }
                        else if (j == 6)
                        {
                            //nếu j = 6 thì insert dữ liệu của tiết 6
                            insertT4.monhoc_name = txtThu4Tiet6_Mon.Value;
                        }
                        else if (j == 7)
                        {
                            //nếu j = 7 thì insert dữ liệu của tiết 7
                            insertT4.monhoc_name = txtThu4Tiet7_Mon.Value;
                        }
                        else
                        {
                            // j = 8 thì insert dữ liệu của tiết 8
                            insertT4.monhoc_name = txtThu4Tiet8_Mon.Value;
                        }
                        db.tbThoiKhoaBieus.InsertOnSubmit(insertT4);
                        db.SubmitChanges();
                    }
                }
                //lưu thứ 5
                if (i == 5)
                {
                    for (int j = 1; j <= 8; j++)
                    {
                        tbThoiKhoaBieu insertT5 = new tbThoiKhoaBieu();
                        insertT5.lop_id = Convert.ToInt32(ddlLop.SelectedValue);
                        insertT5.thoikhoabieu_thu = "Thứ " + i;
                        insertT5.thoikhoabieu_tiet = "Tiết " + j;
                        if (j == 1)
                        {
                            //nếu j = 1 thì insert dữ liệu của tiết 1
                            insertT5.monhoc_name = txtThu5Tiet1_Mon.Value;
                        }
                        else if (j == 2)
                        {
                            //nếu j = 2 thì insert dữ liệu của tiết 2
                            insertT5.monhoc_name = txtThu5Tiet2_Mon.Value;
                        }
                        else if (j == 3)
                        {
                            //nếu j = 3 thì insert dữ liệu của tiết 3
                            insertT5.monhoc_name = txtThu5Tiet3_Mon.Value;
                        }
                        else if (j == 4)
                        {
                            //nếu j = 4 thì insert dữ liệu của tiết 4
                            insertT5.monhoc_name = txtThu5Tiet4_Mon.Value;
                        }
                        else if (j == 5)
                        {
                            //nếu j = 5 thì insert dữ liệu của tiết 5
                            insertT5.monhoc_name = txtThu5Tiet5_Mon.Value;
                        }
                        else if (j == 6)
                        {
                            //nếu j = 6 thì insert dữ liệu của tiết 6
                            insertT5.monhoc_name = txtThu5Tiet6_Mon.Value;
                        }
                        else if (j == 7)
                        {
                            //nếu j = 7 thì insert dữ liệu của tiết 7
                            insertT5.monhoc_name = txtThu5Tiet7_Mon.Value;
                        }
                        else
                        {
                            // j = 8 thì insert dữ liệu của tiết 8
                            insertT5.monhoc_name = txtThu5Tiet8_Mon.Value;
                        }
                        db.tbThoiKhoaBieus.InsertOnSubmit(insertT5);
                        db.SubmitChanges();
                    }
                }
                //lưu thứ 6
                if (i == 6)
                {
                    for (int j = 1; j <= 8; j++)
                    {
                        tbThoiKhoaBieu insertT6 = new tbThoiKhoaBieu();
                        insertT6.lop_id = Convert.ToInt32(ddlLop.SelectedValue);
                        insertT6.thoikhoabieu_thu = "Thứ " + i;
                        insertT6.thoikhoabieu_tiet = "Tiết " + j;
                        if (j == 1)
                        {
                            //nếu j = 1 thì insert dữ liệu của tiết 1
                            insertT6.monhoc_name = txtThu6Tiet1_Mon.Value;
                        }
                        else if (j == 2)
                        {
                            //nếu j = 2 thì insert dữ liệu của tiết 2
                            insertT6.monhoc_name = txtThu6Tiet2_Mon.Value;
                        }
                        else if (j == 3)
                        {
                            //nếu j = 3 thì insert dữ liệu của tiết 3
                            insertT6.monhoc_name = txtThu6Tiet3_Mon.Value;
                        }
                        else if (j == 4)
                        {
                            //nếu j = 4 thì insert dữ liệu của tiết 4
                            insertT6.monhoc_name = txtThu6Tiet4_Mon.Value;
                        }
                        else if (j == 5)
                        {
                            //nếu j = 5 thì insert dữ liệu của tiết 5
                            insertT6.monhoc_name = txtThu6Tiet5_Mon.Value;
                        }
                        else if (j == 6)
                        {
                            //nếu j = 6 thì insert dữ liệu của tiết 6
                            insertT6.monhoc_name = txtThu6Tiet6_Mon.Value;
                        }
                        else if (j == 7)
                        {
                            //nếu j = 7 thì insert dữ liệu của tiết 7
                            insertT6.monhoc_name = txtThu6Tiet7_Mon.Value;
                        }
                        else
                        {
                            // j = 8 thì insert dữ liệu của tiết 8
                            insertT6.monhoc_name = txtThu6Tiet8_Mon.Value;
                        }
                        db.tbThoiKhoaBieus.InsertOnSubmit(insertT6);
                        db.SubmitChanges();
                    }
                }
            }
            alert.alert_Success(Page, "Lưu thành công", "");
        }
    }
}