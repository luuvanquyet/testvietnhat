﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="Admin_Default.aspx.cs" Inherits="Admin_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <script>
        function myDuyet(id) {
            document.getElementById("<%=txtThongBao_id.ClientID%>").value = id
            document.getElementById("<%=btnXem.ClientID%>").click();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <style>
        .link__icon {
            position: relative;
        }

        .icon__new {
            background-color: transparent;
            width: 30px;
            height: 30px;
            position: absolute;
            top: -28px;
            right: 13px;
        }

        .new-item {
            width: 35px;
            height: 20px;
            background-color: #fbc531;
            position: absolute;
            z-index: 1;
            top: -28px;
            right: 9px;
            border-top-right-radius: 7px;
            text-align: center;
            color: #fff;
            font-weight: 700;
            font-size: 13px;
        }

            .new-item:before {
                content: "";
                border-width: 10px;
                border-style: solid;
                border-color: #fbc531 transparent #fbc531 transparent;
                position: absolute;
                top: 0;
                left: -11px;
            }

            .new-item:after {
                content: "";
                border-width: 9px;
                border-style: solid;
                border-color: transparent transparent transparent #fbc531;
                position: absolute;
                top: 10px;
                right: -10px;
                z-index: -1 !important;
            }
    </style>
    <div class="container">
        <div class="row">
               <marquee>
                                    <b>
Kế hoạch dạy học đã cập nhật phiên bản mới vào ngày 14/8/2021 | lịch báo giảng đã cập nhật phiên bản mới vào ngày 17/8/2021
                                        </b>

</marquee>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="text-align:center">
                           
                <div style="font-size: 20PX; font-weight: bold; text-align: center">LỊCH CÔNG TÁC</div>
                <hr />
                <div style="margin-bottom: 20px" class="col-lg-3 col-md-4 col-sm-6 col-12">
                    <a class="link__icon" href="../../admin-xem-lich-cong-tac-hang-tuan">
                        <asp:Repeater ID="rpLichCongTacTuan" runat="server">
                            <ItemTemplate>
                                <img class=" " style="width: 250px; height: 60px" src="<%#Eval("image_name") %>" alt="Lịch có tác tuần" />
                            </ItemTemplate>
                        </asp:Repeater>
                    </a>
                    <a class="link__icon" href="../../admin-xem-lich-cong-tac-thang">
                        <%-- <div class="new-item">New</div>--%>
                        <asp:Repeater ID="rpLichCongTacThang" runat="server">
                            <ItemTemplate>
                                <img class=" " style="width: 250px; height: 64px" src="<%#Eval("image_name") %>" alt="Lịch công tác tháng" />
                            </ItemTemplate>
                        </asp:Repeater>
                        <%-- Lịch công tác tháng--%>
                    </a>
                    <a class="link__icon" href="../../admin-xem-ke-hoach-day-hoc-giao-vien">
                        <%-- <div class="new-item">New</div>--%>
                        <img class=" " style="width: 250px; height: 58px" src="/admin_images/kehoachdayhoc.png" alt="kế hoạch dạy học" />
                        <%-- Lịch công tác hàng tuần--%>
                    </a>
                </div>
                <br />
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="text-align:center">
                <div style="font-size: 20PX; font-weight: bold; text-align: center">
                    THỜI KHÓA BIỂU<%-- CHÍNH KHÓA - LỊCH BÁO GIẢNG - TKB TIẾT 8--%>
                    <%--  <br />
                    (Lưu ý:  TKB tiết 8 đã cập nhật mới bắt đầu từ ngày 29/03/2021)--%>
                </div>
                <hr />
                <div style="margin-bottom: 20px;" class="col-lg-3 col-md-4 col-sm-6 col-12">
                    <a class="link__icon" href="../../admin-thoi-khoa-bieu-chinh-khoa">
                        <img class="" style="width: 250px; height: 60px;" src="/admin_images/tbk__images/ThoiKhoabieu.png" alt="Thời khóa biểu" />
                    </a>
                    <a class="link__icon" href="../../admin-lich-bao-giang-tong">
                        <%-- <div class="new-item">New</div>--%>
                        <img class="" style="width: 250px; height: 64px;" src="/admin_images/tbk__images/lich-bao-giang.png" alt="Lịch báo giảng" />
                    </a>
                    <a class="link__icon" href="../../admin-thoi-khoa-bieu-tiet-8">
                        <img class="" style="width: 250px; height: 58px;" src="/admin_images/tbk__images/TKB-Tiet8.png" />
                    </a>
                </div>
                <br />
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="text-align:center">
                <div style="font-size: 20PX; font-weight: bold; text-align: center">KHO TƯ LIỆU</div>
                <hr />
                <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                    <a class="link__icon" href="../../kho-tu-lieu-chung">
                        <img class="" style="width: 250px; height: 60px;" src="admin_images/tu-lieu-chung.png" alt="" />
                    </a>
                    <a class="link__icon" href="../../kho-tu-lieu-tieu-hoc">
                        <img class="" style="width: 250px; height: 64px;" src="admin_images/tu-lieu-th.png" alt="" />
                    </a>
                    <a class="link__icon" href="../../kho-tu-lieu-trung-hoc">
                        <img class="" style="width: 250px; height: 58px;" src="admin_images/THCS_THPT.png" alt="" />
                    </a>
                   <%-- <a class="link__icon" href="../../kho-tai-lieu-lien-quan-cac-chuyen-de">
                        <img class="" style="width: 250px; height: 58px;" src="uploadimages/lich-cong-tac-tuan/tai-lieu-lien-quan-cac-chuyen-de.png" alt="" />
                    </a>--%>
                </div>
                <br />
            </div>
            <div class="col-12" style="display:none">
                <div style="font-size: 20PX; font-weight: bold; text-align: center">
                    Địa điểm dạy tiết 8 cho lễ hội thể thao:
                <asp:Label ID="lblTuan" runat="server"></asp:Label>
                </div>
                <hr />
                <div style="background: #fff">
                    <asp:UpdatePanel ID="upLoc" runat="server">
                        <ContentTemplate>
                            <div class="form-group row">
                            </div>
                            <table id="example" class="display nowrap table-hover table-bordered" style="width: 100%; text-align: center">
                                <thead>
                                    <tr style="text-align: center; height: 40px">
                                        <th style="text-align: center">Ngày\Địa điểm</th>
                                        <th style="text-align: center">Sân bóng đá</th>
                                        <th style="text-align: center">Sân bóng rổ</th>
                                        <th style="text-align: center">Sân cầu lông</th>
                                        <th style="text-align: center">Hội trường tầng 1</th>
                                        <th style="text-align: center">Hội trường tầng 6 bên ngoài</th>
                                        <th style="text-align: center">Hội trường tầng 6 bên trong</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="rpBookRoom" runat="server" OnItemDataBound="rpBookRoom_ItemDataBound">
                                        <ItemTemplate>
                                            <tr>
                                                <td><%#Eval("lichbookphongthongminh_name") %></td>
                                                <asp:Repeater ID="rpChiTiet" runat="server">
                                                    <ItemTemplate>
                                                        <td><a href="#" class="btn btn-<%#Eval("lichbookphongchitiet_class") %>" data-toggle="modal"><%#Eval("giaovien") %></a></td>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <br />
            </div>
            <%--   Thông báo khẩn câp--%>
            <div style="font-size: 20PX; font-weight: bold; text-align: center; display:none">THÔNG BÁO TRONG TUẦN</div>
           <%-- <hr />--%>
            <div class="col-lg-3 col-md-4 col-sm-6 col-12" style="display:none">
                <table class="table table-bordered table-hover" style="background-color: white">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Tiêu đề</th>
                            <th scope="col">Ngày thông báo</th>
                            <th scope="col">#</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rpList" runat="server">
                            <ItemTemplate>
                                <th scope="row"><%=STT++ %></th>
                                <td><%#Eval("thongbao_name") %> </td>
                                <td style="text-align: center"><%#Convert.ToDateTime(Eval("thongbao_datecreate")).ToShortDateString()%></td>
                                <td style="text-align: center"><a class="btn btn-primary" style="color: white" id="btnDuyet" onclick="myDuyet(<%#Eval("thongbao_id") %>)">Xem</a></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>
            <div style="display: none">
                <input type="text" id="txtThongBao_id" runat="server" />
                <a href="#" id="btnXem" runat="server" onserverclick="btnXem_ServerClick"></a>
            </div>
            <br />
            <%-- Thời khóa biểu của từng giáo viên--%>
            <div id="dvThoiKhoaBieu" runat="server" style="display: none">
                <div style="font-size: 20PX; font-weight: bold; text-align: center">Thời khóa biểu</div>
                <hr />
                <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                    <table class="table table-bordered" style="background-color: white">
                        <thead>
                            <tr class="head_table">
                                <%-- <th class="table_header" scope="col">Buổi</th>--%>
                                <th scope="col">Tiết</th>
                                <th scope="col">Thứ 2</th>
                                <th scope="col">Thứ 3</th>
                                <th scope="col">Thứ 4</th>
                                <th scope="col">Thứ 5</th>
                                <th scope="col">Thứ 6</th>
                                <%--   <th scope="col">Thứ 7</th>--%>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Repeater ID="rpThoiKhoaBieu" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td class="tiet" style="text-align: center"><%#Eval("tkb_tiet") %></td>
                                        <td class="tiet" style="text-align: center"><%#Eval("tkb_thu2") %></td>
                                        <td class="tiet" style="text-align: center"><%#Eval("tkb_thu3") %></td>
                                        <td class="tiet" style="text-align: center"><%#Eval("tkb_thu4") %></td>
                                        <td class="tiet" style="text-align: center"><%#Eval("tkb_thu5") %></td>
                                        <td class="tiet" style="text-align: center"><%#Eval("tkb_thu6") %></td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
                </div>
            </div>
            <br />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

