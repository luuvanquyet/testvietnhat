﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Default : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public int STT;
    cls_Alert alert = new cls_Alert();
    protected void Page_Load(object sender, EventArgs e)
    {
        STT = 1;
        if (Request.Cookies["UserName"] != null)
        {
            var checkuserid = (from u in db.admin_Users where u.username_username == Request.Cookies["UserName"].Value select u).First();

            var checkThoiKhoaBieu = from tkb in db.tbThoiKhoaBieuTungGiaoViens
                                    where tkb.giaovien_id == checkuserid.username_id
                                    select tkb;
            if (checkThoiKhoaBieu.Count() > 0)
            {
                rpThoiKhoaBieu.DataSource = checkThoiKhoaBieu;
                rpThoiKhoaBieu.DataBind();
            }
            else
            {
                dvThoiKhoaBieu.Visible = false;
            }
        }
        rpList.DataSource = from l in db.tbThongBaos orderby l.thongbao_id descending select l;
        rpList.DataBind();
        rpLichCongTacThang.DataSource = (from lct in db.tbLichCongTac_Thangs
                                         join i in db.tbImage_Liches on lct.image_id equals i.image_lich
                                         orderby lct.lichcongtac_thang_id descending
                                         select i).Take(1);
        rpLichCongTacThang.DataBind();
        rpLichCongTacTuan.DataSource = (from lct in db.tbLichCongTacs
                                        join i in db.tbImage_Liches on lct.image_lich equals i.image_lich
                                        orderby lct.lichcongtac_id descending
                                        select i).Take(1);
        rpLichCongTacTuan.DataBind();
        int tuan_id = Convert.ToInt32((from t in db.tbLichBookPhongThongMinhs orderby t.lichbookphongthongminh_id descending select t).First().tuan_id);
        lblTuan.Text = (from t in db.tbHocTap_Tuans where t.tuan_id == tuan_id select t).SingleOrDefault().tuan_name + "";
        rpBookRoom.DataSource = from nb in db.tbLichBookPhongThongMinhs
                                where nb.tuan_id == tuan_id
                                select nb;
        rpBookRoom.DataBind();


    }

    protected void btnXem_ServerClick(object sender, EventArgs e)
    {
        //alert.alert_Success(Page,"get_id" + txtThongBao_id.Value,"");
        Response.Redirect("admin-file-huong-dan-danh-gia-" + txtThongBao_id.Value);
    }
    protected void rpBookRoom_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Repeater rpChiTiet = e.Item.FindControl("rpChiTiet") as Repeater;
        int lichbookphongthongminh_id = int.Parse(DataBinder.Eval(e.Item.DataItem, "lichbookphongthongminh_id").ToString());
        rpChiTiet.DataSource = from ct in db.tblichbookphongchitiets
                               where ct.lichbookphongthongminh_id == lichbookphongthongminh_id
                               select new
                               {
                                   ct.lichbookphongchitiet_class,
                                   ct.lichbookphongchitiet_id,
                                   giaovien = ct.lichbookphongchitiet_giaovien == null ? "Chưa book" : ct.lichbookphongchitiet_giaovien,
                               };
        rpChiTiet.DataBind();
    }
}