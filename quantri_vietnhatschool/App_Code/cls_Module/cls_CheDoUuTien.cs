﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for cls_CheDoUuTien
/// </summary>
public class cls_CheDoUuTien
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public cls_CheDoUuTien()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public bool ThemMoi(string tenchedo, string mota)
    {
        tbDangky_CheDoUuTien insert = new tbDangky_CheDoUuTien();
        insert.chedo_name = tenchedo;
        insert.chedo_tomtat = mota;
        db.tbDangky_CheDoUuTiens.InsertOnSubmit(insert);
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool CapNhat(int chedo_id, string tenchedo, string mota)
    {
        tbDangky_CheDoUuTien update = db.tbDangky_CheDoUuTiens.Where(x => x.chedo_id == chedo_id).FirstOrDefault();
        update.chedo_name = tenchedo;
        update.chedo_tomtat = mota;
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
}