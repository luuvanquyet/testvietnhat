﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for cls_ChuongTrinhUuDai
/// </summary>
public class cls_ChuongTrinhUuDai
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public cls_ChuongTrinhUuDai()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public bool ThemMoi(string thoihan, string noidung, DateTime tungay, DateTime denngay)
    {
        tbWebsite_ChuongTrinhUuDai insert = new tbWebsite_ChuongTrinhUuDai();
        insert.ctud_thoigian = thoihan;
        insert.ctud_noidung = noidung;
        insert.ctud_thoigianbatdau = tungay;
        insert.ctud_thoigianketthuc = denngay;
        insert.ctud_status = 0;
        db.tbWebsite_ChuongTrinhUuDais.InsertOnSubmit(insert);
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool CapNhat(int ctud_id, string thoihan, string noidung, DateTime tungay, DateTime denngay)
    {
        tbWebsite_ChuongTrinhUuDai update = db.tbWebsite_ChuongTrinhUuDais.Where(x => x.ctud_id == ctud_id).FirstOrDefault();
        update.ctud_thoigian = thoihan;
        update.ctud_noidung = noidung;
        update.ctud_thoigianbatdau = tungay;
        update.ctud_thoigianketthuc = denngay;
        update.ctud_status = 0;
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Xoa(int ctud_id)
    {
        tbWebsite_ChuongTrinhUuDai update = db.tbWebsite_ChuongTrinhUuDais.Where(x => x.ctud_id == ctud_id).FirstOrDefault();
        update.ctud_status = -1;
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
}