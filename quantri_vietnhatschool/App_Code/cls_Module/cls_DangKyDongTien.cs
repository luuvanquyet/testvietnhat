﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for cls_DangKyDongTien
/// </summary>
public class cls_DangKyDongTien
{
	dbcsdlDataContext db = new dbcsdlDataContext();
	public cls_DangKyDongTien()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public bool ThemMoi(int hocsinh_id, string ct_uudai, int username_id, string tiencoc)
    {
        tbWebsite_DangKyDongTien insert = new tbWebsite_DangKyDongTien();
        insert.hocsinh_dangky_id = hocsinh_id;
        //insert.ctud_id = ctud_id;
        insert.dongtien_chuongtrinh_uudai = ct_uudai;
        insert.username_id = username_id;
        insert.dongtien_ngaydangky = DateTime.Now;
        insert.dongtien_ngaycapnhat = DateTime.Now;
        insert.dongtien_tiencoc = tiencoc;
        insert.dongtien_trangthai = 0;
        db.tbWebsite_DangKyDongTiens.InsertOnSubmit(insert);
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool CapNhat(int dangky_id,  string ct_uudai, int username_id, string tiencoc)
    {
        tbWebsite_DangKyDongTien update = db.tbWebsite_DangKyDongTiens.Where(x => x.dongtien_id == dangky_id).FirstOrDefault();
        //update.ctud_id = ctud_id;
        update.dongtien_chuongtrinh_uudai = ct_uudai;
        update.username_id = username_id;
        update.dongtien_ngaycapnhat = DateTime.Now;
        update.dongtien_tiencoc = tiencoc;
        update.dongtien_trangthai = 0;
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
}