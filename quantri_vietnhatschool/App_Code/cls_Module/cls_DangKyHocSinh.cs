﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for cls_DangKyHocSinh4
/// </summary>
public class cls_DangKyHocSinh
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public cls_DangKyHocSinh()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public bool ThemMoi(string hocsinh_name, DateTime hocsinh_dob, string hocsinh_quoctich, string hocsinh_lopdangky, string hocsinh_phuhuynh_name, string hocsinh_address, string hocsinh_email, string hocsinh_phone, int username_id, int coso_dangky, int chedo_id, string hotenme, string sdtme, string emailme, string lydouutien, string ghichu,string nguondangky)
    {
        tbWebsite_DangKiTuyenSinh insert = new tbWebsite_DangKiTuyenSinh();
        insert.hocsinh_name = hocsinh_name;
        insert.hocsinh_dateofbirth = hocsinh_dob;
        insert.hocsinh_quoctich = hocsinh_quoctich;
        insert.hocsinh_dangki_class = hocsinh_lopdangky;
        insert.hocsinh_phuhuynh_name = hocsinh_phuhuynh_name;
        insert.hocsinh_address = hocsinh_address;
        insert.hocsinh_email = hocsinh_email;
        insert.hocsinh_phone = hocsinh_phone;
        insert.hocsinh_trangthai_dangky = 0;
        insert.hocsinh_ngaydangky = DateTime.Now;
        insert.username_id = username_id;
        insert.hocsinh_noidangky = "Văn phòng";
        insert.hocsinh_trangthai_guimail = false;
        insert.hocsinh_danhsachcho_khaosat = false;
        insert.hocsinh_cosodangky = coso_dangky;
        insert.chedo_id = chedo_id;
        insert.hocsinh_tenme = hotenme;
        insert.hocsinh_sdtme = sdtme;
        insert.hocsinh_emailme = emailme;
        insert.hocsinh_lydo_uutien = lydouutien;
        insert.hocsinh_ghichu = ghichu;
        insert.hocsinh_nguondangky = nguondangky;
        insert.hocsinh_tinhtrang = 6; //đăng ký mới
        db.tbWebsite_DangKiTuyenSinhs.InsertOnSubmit(insert);
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool CapNhat(int hocsinh_id, string hocsinh_name, DateTime hocsinh_dob, string hocsinh_quoctich, string hocsinh_lopdangky, string hocsinh_phuhuynh_name, string hocsinh_address, string hocsinh_email, string hocsinh_phone, int username_id, int coso_dangky, int chedo_id, string hotenme, string sdtme, string emailme, string lydouutien, string ghichu, string nguondangky)
    {
        tbWebsite_DangKiTuyenSinh update = db.tbWebsite_DangKiTuyenSinhs.Where(x => x.hocsinh_id == hocsinh_id).FirstOrDefault();
        update.hocsinh_name = hocsinh_name;
        update.hocsinh_dateofbirth = hocsinh_dob;
        update.hocsinh_quoctich = hocsinh_quoctich;
        update.hocsinh_dangki_class = hocsinh_lopdangky;
        update.hocsinh_phuhuynh_name = hocsinh_phuhuynh_name;
        update.hocsinh_address = hocsinh_address;
        update.hocsinh_email = hocsinh_email;
        update.hocsinh_phone = hocsinh_phone;
        //update.hocsinh_trangthai_dangky = 0;
        //insert.hocsinh_ngaydangky = DateTime.Now;
        //insert.username_id = username_id;
        //insert.hocsinh_noidangky = "Văn phòng";
        //insert.hocsinh_trangthai_guimail = false;
        //insert.hocsinh_danhsachcho_khaosat = false;
        update.hocsinh_cosodangky = coso_dangky;
        update.chedo_id = chedo_id;
        update.hocsinh_tenme = hotenme;
        update.hocsinh_sdtme = sdtme;
        update.hocsinh_emailme = emailme;
        update.hocsinh_lydo_uutien = lydouutien;
        update.hocsinh_ghichu = ghichu;
        update.hocsinh_nguondangky = nguondangky;
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
}