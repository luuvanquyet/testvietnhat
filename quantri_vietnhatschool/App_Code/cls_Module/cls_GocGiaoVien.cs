﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for cls_NewsCate
/// </summary>
public class cls_GocGiaoVien
{
    dbcsdlDataContext db = new dbcsdlDataContext();
	public cls_GocGiaoVien()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public bool Linq_Them(string newscate_title, string content)
    {
        tbwebsite_GocGiaoVien insert = new tbwebsite_GocGiaoVien();
        insert.gocgiaovien_title = newscate_title;
        insert.gocgiaovien_summary = content;
        db.tbwebsite_GocGiaoViens.InsertOnSubmit(insert);
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Sua(int newscate_id, string newscate_title, string content)
    {
        tbwebsite_GocGiaoVien update = db.tbwebsite_GocGiaoViens.Where(x => x.gocgiaovien_id == newscate_id).FirstOrDefault();
        update.gocgiaovien_title = newscate_title;
        update.gocgiaovien_summary = content;
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Xoa( int newscate_id)
    {
        tbwebsite_GocGiaoVien delete = db.tbwebsite_GocGiaoViens.Where(x => x.gocgiaovien_id == newscate_id).FirstOrDefault();
        db.tbwebsite_GocGiaoViens.DeleteOnSubmit(delete);
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
}