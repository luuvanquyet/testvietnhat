﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for cls_NewsCate
/// </summary>
public class cls_LoaiLinhVuc
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public cls_LoaiLinhVuc()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public bool Linq_Them( string tusachname)
    {
        tbThuVien_TuSach insert = new tbThuVien_TuSach();
        insert.thuvien_tusach_name = tusachname;
        db.tbThuVien_TuSaches.InsertOnSubmit(insert);
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Sua(int tusach_id,  string tusachname)
    {
        tbThuVien_TuSach update = db.tbThuVien_TuSaches.Where(x => x.thuvien_tusach_id == tusach_id).FirstOrDefault();
        update.thuvien_tusach_name = tusachname;
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Xoa( int newscate_id)
    {
        tbThuVien_TuSach delete = db.tbThuVien_TuSaches.Where(x => x.thuvien_tusach_id == newscate_id).FirstOrDefault();
        db.tbThuVien_TuSaches.DeleteOnSubmit(delete);
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
}