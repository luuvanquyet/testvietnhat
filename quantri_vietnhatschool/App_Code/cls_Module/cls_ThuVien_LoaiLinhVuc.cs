﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

/// <summary>
/// Summary description for cls_NewsCate
/// </summary>
public class cls_ThuVien_LoaiLinhVuc
{
    dbcsdlDataContext db = new dbcsdlDataContext();
	public cls_ThuVien_LoaiLinhVuc()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public bool Linq_Them( string newscate_title)
    {
        //tbThuVien_LoaiLinhVuc insert = new tbThuVien_LoaiLinhVuc();
        //insert.thuvien_loailinhvuc_name = newscate_title;
        //db.tbThuVien_LoaiLinhVucs.InsertOnSubmit(insert);
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Sua(int newscate_id, string newscate_title)
    {
        //tbThuVien_LoaiLinhVuc update = db.tbThuVien_LoaiLinhVucs.Where(x => x.thuvien_loailinhvuc_id == newscate_id).FirstOrDefault();
        //update.thuvien_loailinhvuc_name = newscate_title;
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Xoa( int newscate_id)
    {
        //tbThuVien_LoaiLinhVuc delete = db.tbThuVien_LoaiLinhVucs.Where(x => x.thuvien_loailinhvuc_id == newscate_id).FirstOrDefault();
        //db.tbThuVien_LoaiLinhVucs.DeleteOnSubmit(delete);
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
}