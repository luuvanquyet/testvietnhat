﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="frmPhiDichVuTruong.aspx.cs" Inherits="Report_PhiDichVu_frmPhiDichVuTruong" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" Runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" Runat="Server">
    <div>
        <asp:DropDownList ID="ddlLop" CssClass="form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlLop_SelectedIndexChanged" Width="30%"></asp:DropDownList>
    </div>
    <br />

     <div style="width:100%;height:auto; background-color:#fff">
        <rsweb:ReportViewer ID="ReportViewer1" runat="server"  SizeToReportContent = "true" width = "3500px" height = "4000px"></rsweb:ReportViewer>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" Runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" Runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" Runat="Server">
</asp:Content>

