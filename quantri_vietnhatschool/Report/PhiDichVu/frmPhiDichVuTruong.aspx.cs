﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Report_PhiDichVu_frmPhiDichVuTruong : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            var listNV = from l in db.tbLops
                         join gvtl in db.tbGiaoVienTrongLops on l.lop_id equals gvtl.lop_id
                         select l;
            ddlLop.Items.Clear();
            ddlLop.Items.Insert(0, "Chọn Lớp");
            ddlLop.AppendDataBoundItems = true;
            ddlLop.DataTextField = "lop_name";
            ddlLop.DataValueField = "lop_id";
            ddlLop.DataSource = listNV;
            ddlLop.DataBind();
        }
    }
    private dsPhiDichVuTruong getPhiDichVuTruong(string query)
    {
        string conString = ConfigurationManager.ConnectionStrings["admin_VietNhatConnectionString"].ConnectionString;

        SqlCommand cmd = new SqlCommand(query);

        using (SqlConnection con = new SqlConnection(conString))
        {
            using (SqlDataAdapter sda = new SqlDataAdapter())
            {
                cmd.Connection = con;
                sda.SelectCommand = cmd;
                using (dsPhiDichVuTruong dsPhiDichVuTruong = new dsPhiDichVuTruong())
                {
                    sda.Fill(dsPhiDichVuTruong, "dtPhiDichVuTruong");
                    return dsPhiDichVuTruong;
                }
            }
        }
    }
    protected void ddlLop_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlLop.SelectedValue != "Chọn Lớp")
        {
            int lopid = Convert.ToInt32(ddlLop.SelectedValue);
            ReportViewer1.ProcessingMode = ProcessingMode.Local;
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Report/PhiDichVu/designPhiDichVuTruong.rdlc");
            dsPhiDichVuTruong dsPhiDichVuTruong = getPhiDichVuTruong("select lop_name, hocsinh_name,CONCAT(phidichvu_dongphucdihoc_ao,' áo,', phidichvu_dongphucdihoc_quandai, N' quần dài,', phidichvu_dongphucdihoc_quanngan, N' quần ngắn,', phidichvu_dongphucdihoc_vay,N' váy') as dongphucdihoc,CONCAT(phidichvu_dongphuctheduc_soluong, ' ', phidichvu_dongphuctheduc_loai) as dongphuctheduc,phidichvu_bhyt_noidangky,phidichvu_bhtn,phidichvu_ansangtaitruong,phidichvu_xeduadon, phidichvu_ghichu" +
                                                    " from tbLop, tbHocSinhTrongLop, tbHocSinh, tbDangKyPhiDichVu " +
                                                    " where tbLop.lop_id = tbHocSinhTrongLop.lop_id" +
                                                    " and tbHocSinhTrongLop.hocsinh_id = tbHocSinh.hocsinh_id" +
                                                    " and tbHocSinhTrongLop.hstl_id = tbDangKyPhiDichVu.hstl_id" +
                                                    " and tbLop.lop_id ="+ lopid + "  order by tbHocSinhTrongLop.position asc");
            ReportDataSource datasource = new ReportDataSource("dsPhiDichVuTruong", dsPhiDichVuTruong.Tables[0]);
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportViewer1.LocalReport.DataSources.Add(datasource);
        }
    }
}