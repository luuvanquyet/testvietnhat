﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Report_frmKeHoachDayHoc : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Mon"] != null)
            {
                if (Session["Mon"].ToString() == "")
                {
                    ReportViewer1.ProcessingMode = ProcessingMode.Local;
                    ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Report/designKeHoachDayHoc.rdlc");
                    dsKeHoachDayHoc dsKeHoachDayHoc = getdsKeHoachDayHoc("SELECT * FROM tbQuantri_kehoachdayhoc where tbQuantri_kehoachdayhoc.username_id ='" + Session["GiaoVien"].ToString() + "' and kehoachdayhoc_lop='" + Session["Lop"].ToString() + "' order by kehoachdayhoc_tiet ASC");
                    ReportDataSource datasource = new ReportDataSource("dsKeHoachDayHoc", dsKeHoachDayHoc.Tables[0]);
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportViewer1.LocalReport.DataSources.Add(datasource);
                }
                else
                {
                    ReportViewer1.ProcessingMode = ProcessingMode.Local;
                    ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Report/designKeHoachDayHoc.rdlc");
                    dsKeHoachDayHoc dsKeHoachDayHoc = getdsKeHoachDayHoc("SELECT * FROM tbQuantri_kehoachdayhoc where tbQuantri_kehoachdayhoc.username_id ='" + Session["GiaoVien"].ToString() + "' and kehoachdayhoc_lop='" + Session["Lop"].ToString() + "' and kehoachdayhoc_mon =N'" + Session["Mon"].ToString() + "' order by kehoachdayhoc_tiet ASC");
                    ReportDataSource datasource = new ReportDataSource("dsKeHoachDayHoc", dsKeHoachDayHoc.Tables[0]);
                    ReportViewer1.LocalReport.DataSources.Clear();
                    ReportViewer1.LocalReport.DataSources.Add(datasource);
                }
            }
            else
            {
                ReportViewer1.ProcessingMode = ProcessingMode.Local;
                ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Report/designKeHoachDayHoc.rdlc");
                dsKeHoachDayHoc dsKeHoachDayHoc = getdsKeHoachDayHoc("SELECT * FROM tbQuantri_kehoachdayhoc where tbQuantri_kehoachdayhoc.username_id ='" + Session["GiaoVien"].ToString() + "' and kehoachdayhoc_lop='" + Session["Lop"].ToString() + "' order by kehoachdayhoc_tiet ASC");
                ReportDataSource datasource = new ReportDataSource("dsKeHoachDayHoc", dsKeHoachDayHoc.Tables[0]);
                ReportViewer1.LocalReport.DataSources.Clear();
                ReportViewer1.LocalReport.DataSources.Add(datasource);
            }
        }  
    }
    private dsKeHoachDayHoc getdsKeHoachDayHoc(string query)
    {
        string conString = ConfigurationManager.ConnectionStrings["vie65506_VietNhatConnectionString"].ConnectionString;

        SqlCommand cmd = new SqlCommand(query);

        using (SqlConnection con = new SqlConnection(conString))
        {
            using (SqlDataAdapter sda = new SqlDataAdapter())
            {
                cmd.Connection = con;
                sda.SelectCommand = cmd;
                using (dsKeHoachDayHoc dsKeHoachDayHoc = new dsKeHoachDayHoc())
                {
                    sda.Fill(dsKeHoachDayHoc, "tbQuantri_kehoachdayhoc");
                    return dsKeHoachDayHoc;
                }
            }
        }
    }
}