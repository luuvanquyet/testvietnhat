﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_DangKyCuocHop.aspx.cs" Inherits="admin_page_module_function_module_DangKyCuocHop" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <script type="text/javascript">
        function OnCheckedChanged(s, e) {
            label.SetText("danh sách giáo viên:");
            ListCheckedNodes(s);
        }

        function ListCheckedNodes(parent) {
            for (var i = 0; i < parent.GetNodeCount(); i++) {
                if (parent.GetNode(i).GetChecked()) {
                    label.SetText(label.GetText() + ", " + parent.GetNode(i).GetText());
                }
                if (parent.GetNode(i).GetNodeCount() != 0) {
                    ListCheckedNodes(parent.GetNode(i));
                }
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">

    <%-- js báo cáo --%>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css" />
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript">

        function onClick(element) {
            document.getElementById("img01").src = element.src;
            document.getElementById("modal01").style.display = "block";
        }

        function onClickDel(element) {
            document.getElementById('<%=txtImgPath.ClientID%>').value = element;
            document.getElementById('<%=btnDelImg.ClientID%>').click();
        }
        function showPreview1(input) {
            if (input.files && input.files[0]) {
                var filerdr = new FileReader();
                filerdr.onload = function (e) {
                    //$('#imgPreview1').attr('src', e.target.result);
                    $('#imgPreview1').attr('src', '/images/icon_PDF.png');
                }
                filerdr.readAsDataURL(input.files[0]);

            }
        }

        function xemBaocao(cuochop_id) {
            document.getElementById('<%=txtCuocHop_ID.ClientID%>').value = cuochop_id;
            document.getElementById('<%=btnXemNhanXetBaoCao.ClientID%>').click();
        }
        function show_image() {
            document.getElementById("imgPreview1").style.display = "block";

        }
    </script>

    <style>
        /*kim - button X close*/
        .close {
            position: absolute;
            top: 0;
            right: 0;
            font-size: 1.3rem;
            opacity: 0.8;
            color: red !important;
            padding: 4px;
            text-decoration: none !important;
            border-radius: 4px;
            font-family: monospace;
        }

        .AClass {
            right: 0px;
            position: absolute;
        }
    </style>

    <%-- ĐĂNG KÝ CUỘC HỌP --%>
    <div class="form-group row">
        <div style="text-align: center; font-size: 28px; font-weight: bold; color: blue">
            <label>ĐĂNG KÝ CUỘC HỌP</label>
        </div>
        <asp:UpdatePanel ID="udPopup" runat="server">
            <ContentTemplate>
                <div class="popup-main">
                    <div class="div_content col-12">
                        <div class="col-12 form-group">
                            <label class="col-2 form-control-label">Nội dung:</label>
                            <div class="col-10">
                                <input id="txtNoiDungCuocHop" type="text" runat="server" class="form-control" />

                            </div>
                        </div>
                    </div>
                    <div class="div_content col-12">
                        <div class="col-12 form-group">
                            <label class="col-2 form-control-label">Ngày họp :</label>
                            <div class="col-10">
                                <input id="dteCreateDate" runat="server" type="date" class="form-control" />
                            </div>
                        </div>
                    </div>

                    <div class="div_content col-12">
                        <div class="col-12 form-group">
                            <label class="col-2 form-control-label">Giờ họp:</label>
                            <div class="col-10">
                                <dx:ASPxComboBox ID="cbbGio" runat="server" Width="100%"></dx:ASPxComboBox>
                                <label>giờ</label>
                                <dx:ASPxComboBox ID="cbbPhut" runat="server" Width="100%"></dx:ASPxComboBox>
                                <label>phút</label>
                            </div>
                        </div>
                    </div>

                    <div class="div_content col-12">
                        <div class="col-12 form-group">
                            <label class="col-2 form-control-label">Hình thức:</label>
                            <div class="col-10">
                                <dx:ASPxComboBox ID="cbbFormat" class="form-control" runat="server" Width="100%">
                                </dx:ASPxComboBox>
                            </div>
                        </div>
                    </div>
                    <div class="div_content col-12">
                        <div class="col-12 form-group">
                            <label class="col-2 form-control-label">Bộ phận:</label>
                            <div class="col-10">
                                <asp:DropDownList ID="ddlBoPhan" CssClass="form-control" runat="server" Width="100%" DataValueField="tocm_id" DataTextField="tocm_name"></asp:DropDownList>
                            </div>
                        </div>
                    </div>
                    <%-- <div class="div_content col-12" id="divLink" runat="server">
                        <div class="col-12 form-group">
                            <label class="col-2 form-control-label">Link Team:</label>
                            <div class="col-10">
                                <input id="txtLink" type="text" runat="server" class="form-control" style="width: 90%" />
                            </div>
                        </div>
                    </div>--%>
                    <%--<div id="divBoPhan" runat="server">
                        <div class="col-12 form-group">
                            <label class="col-2 form-control-label">Chọn bộ phận:</label>
                            <div class="col-10">
                                <dx:ASPxTreeView ID="tvTest" runat="server" EnableAnimation="true" CheckNodesRecursive="true"
                                    AllowCheckNodes="true" AllowSelectNode="true" SyncSelectionMode="CurrentPath">
                                    <ClientSideEvents CheckedChanged="OnCheckedChanged" />
                                </dx:ASPxTreeView>
                            </div>
                        </div>
                        <div class="col-12 form-group">
                            <dx:ASPxLabel ID="ASPxLabel1" runat="server" ClientInstanceName="label" Text="Checked nodes:">
                            </dx:ASPxLabel>
                        </div>
                    </div>--%>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="upButton" runat="server">
            <ContentTemplate>
                <asp:Button ID="btnThem" runat="server" Text="Đăng ký" CssClass="btn btn-primary" OnClick="btnThem_Click" />
                <div style="display: none">
                    <asp:Button ID="btnChiTiet" runat="server" Text="Sửa" CssClass="btn btn-primary hidden" OnClick="btnChiTiet_Click" />
                    <asp:Button ID="btnXoa" runat="server" Text="Xóa" CssClass="btn btn-primary hidden" OnClick="btnXoa_Click" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>

        <br />
        <br />

        <%-- =============================================================================================================================================== --%>

        <%-- DANH SÁCH CÁC CUỘC HỌP--%>
        <div style="text-align: center; font-size: 28px; font-weight: bold; color: blue">
            <label>DANH SÁCH CÁC CUỘC HỌP</label>
        </div>
        <asp:UpdatePanel ID="upListCuocHop" runat="server">
            <ContentTemplate>

                <dx:ASPxGridView ID="grvListCuocHop" runat="server" CssClass="table-hover" ClientInstanceName="grvListCuocHop" KeyFieldName="cuochop_id" Width="100%">
                    <Columns>
                        <dx:GridViewCommandColumn ShowSelectCheckbox="True" SelectAllCheckboxMode="Page" VisibleIndex="0" Width="2%">
                        </dx:GridViewCommandColumn>
                        <dx:GridViewDataColumn Caption="Tổ" FieldName="tocm_name" HeaderStyle-HorizontalAlign="Center" Width="15%" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="Nội dung cuộc họp" FieldName="cuochop_content" HeaderStyle-HorizontalAlign="Center" Width="30%" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>
                        <%-- <dx:GridViewDataColumn Caption="Thứ" FieldName="cuochopchitiet_thu" HeaderStyle-HorizontalAlign="Center" Width="10%" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>--%>
                        <dx:GridViewDataColumn Caption="Ngày" FieldName="ngayhop" HeaderStyle-HorizontalAlign="Center" Width="20%" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="Giờ" FieldName="cuochopchitiet_gio" HeaderStyle-HorizontalAlign="Center" Width="10%" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="Trạng thái" FieldName="cuochop_status" HeaderStyle-HorizontalAlign="Center" Width="30%" HeaderStyle-Font-Bold="true">
                        </dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="Hình thức" FieldName="cuochop_format" HeaderStyle-HorizontalAlign="Center" Width="30%" HeaderStyle-Font-Bold="true">
                        </dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="" FieldName="" HeaderStyle-HorizontalAlign="Center" Width="50%" HeaderStyle-Font-Bold="true">
                            <DataItemTemplate>
                                <a href="javascript:void(0)" id="<%#Eval("cuochop_id") %>" onclick="xemBaocao(<%#Eval("cuochop_id") %>)"><%#Eval("xembaocao") %></a>
                            </DataItemTemplate>
                        </dx:GridViewDataColumn>
                    </Columns>
                    <SettingsSearchPanel Visible="true" />
                    <SettingsBehavior AllowFocusedRow="true" />
                    <SettingsText EmptyDataRow="Trống" SearchPanelEditorNullText="Gõ từ cần tìm kiếm và enter..." />
                    <SettingsLoadingPanel Text="Đang tải..." />
                    <SettingsPager PageSize="10" Summary-Text="Trang {0} / {1} ({2} trang)"></SettingsPager>
                </dx:ASPxGridView>
                <br />
                <asp:Button ID="btnBaoCao" runat="server" Text="Gửi biên bản cuộc họp" CssClass="btn btn-primary" OnClick="btnBaoCao_Click" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <%-- POPUP BÁO CÁO --%>
    <dx:ASPxPopupControl ID="popupControl" runat="server" Width="700" Height="500" CloseAction="CloseButton" ShowCollapseButton="True" ShowMaximizeButton="True" ScrollBars="Auto" CloseOnEscape="true" Modal="True"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" ClientInstanceName="popupControl" ShowFooter="true"
        HeaderText="GỬI BÁO CÁO CUỘC HỌP" AllowDragging="True" PopupAnimationType="Fade" EnableViewState="False" AutoUpdatePosition="true" ClientSideEvents-CloseUp="function(s,e){grvListCuocHop.Refresh();}">
        <ContentCollection>
            <dx:PopupControlContentControl runat="server">
                <div class="popup-main">
                    <h5 class="text-capitalize px-1">Nội dung báo cáo</h5>
                    <asp:FileUpload ID="fuUpload" runat="server" onchange="show_image()" accept=".pdf" />
                    <br />
                    <button type="button" class="btn-chang">
                        <img id="imgPreview1" src="/images/icon_PDF.png" style="max-width: 100%; height: 150px; display: none" />
                    </button>
                    <div class="div_content col-12">
                        <asp:Literal ID="ltEmbed" runat="server" />
                    </div>
                </div>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <FooterContentTemplate>

            <div class="mar_but button">
                <asp:UpdatePanel ID="udSave" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnLuuImg" runat="server" Text="Lưu" CssClass="btn btn-primary" OnClick="btnLuuImg_Click" ClientMode="static" />
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnLuuImg" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>

        </FooterContentTemplate>
        <ContentStyle>
            <Paddings PaddingBottom="0px" />
        </ContentStyle>
    </dx:ASPxPopupControl>

    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <div style="display: none">
                <input type="text" name="name" value="" runat="server" id="txtImgPath" />
                <a href="javascript:void(0)" runat="server" id="btnDelImg" onserverclick="btnDelImg_ServerClick"></a>
                <input type="text" name="name" value="" runat="server" id="txtCuocHop_ID" />
                <a href="#" runat="server" id="btnXemNhanXetBaoCao" onserverclick="btnXemNhanXetBaoCao_ServerClick">content</a>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <script type="text/javascript">
        function clickavatar1() {
            $("#up1 input[type=file]").click();
        }
    </script>
    <%--popup xem nhạn xét báo cáo--%>
    <dx:ASPxPopupControl ID="popupNhanXetBaoCao" runat="server" Width="700px" Height="400px" CloseAction="CloseButton" ShowCollapseButton="True" ShowMaximizeButton="True" ScrollBars="Auto" CloseOnEscape="true" Modal="True"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" ClientInstanceName="popupNhanXetBaoCao" ShowFooter="true"
        HeaderText="CHI TIẾT NHẬN XÉT BÁO CÁO" AllowDragging="True" PopupAnimationType="Fade" EnableViewState="False" AutoUpdatePosition="true" ClientSideEvents-CloseUp="function(s,e){grvList.Refresh();}">
        <ContentCollection>
            <dx:PopupControlContentControl runat="server">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <div class="popup-main">
                            <div class="div_content row">
                                <div class="col-12 form-group">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">Nội dung nhận xét</th>
                                                <th scope="col">Ngày nhận xét</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <asp:Repeater ID="rptNoiDungNhanXet" runat="server">
                                                <ItemTemplate>
                                                    <tr>
                                                        <th><%#Container.ItemIndex+1 %></th>
                                                        <td><%#Eval("nhanxet_noidung") %></td>
                                                        <td><%#Eval("nhanxet_ngay","{0: dd/MM/yyyy}") %></td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <FooterContentTemplate>
        </FooterContentTemplate>
        <ContentStyle>
            <Paddings PaddingBottom="0px" />
        </ContentStyle>
    </dx:ASPxPopupControl>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

