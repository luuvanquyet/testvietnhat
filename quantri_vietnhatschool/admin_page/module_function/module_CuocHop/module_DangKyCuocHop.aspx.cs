﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_DangKyCuocHop : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private static int _idUser;
    private static int _idCuocHop;
    private static string options = "";
    private static int _idBoPhan;
    private static string _nameBoPhan = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        //divLink.Visible = false;
        //get user access
        _idUser = (from u in db.admin_Users
                   where u.username_username == Request.Cookies["UserName"].Value
                   select u).FirstOrDefault().username_id;
        if (!IsPostBack)
        {
            loadDll();
            //loadTreeViewBoPhan();
        }
        loadData();

    }

    //public void loadTreeViewBoPhan()
    //{
    //    List<tbBoPhan> allBoPhan = new List<tbBoPhan>();
    //    allBoPhan = (from bp in db.tbBoPhans where bp.bophan_position != null && bp.hidden == true select bp).ToList();
    //    int i = 0;
    //    foreach (var item in allBoPhan)
    //    {
    //        tvTest.Nodes.Add(item.bophan_name, item.bophan_id.ToString());
    //        var allPeople = (from user in db.admin_Users
    //                         where user.bophan_id == item.bophan_id  select user).ToList();
    //        foreach (var people in allPeople)
    //        {
    //            tvTest.Nodes[i].Nodes.Add(people.username_fullname, people.username_id.ToString());
    //        }
    //        i++;
    //    }
    //}

    public void ResetData()
    {
        cbbFormat.Text = "";
        cbbGio.Text = "";
        cbbPhut.Text = "";
        dteCreateDate.Value = "";
        //txtLink.Value = "";
        txtNoiDungCuocHop.Value = "";
    }

    public void loadDll()
    {
        //clear all dropdownlist
        cbbFormat.Items.Clear();
        cbbGio.Items.Clear();
        cbbPhut.Items.Clear();

        //load dllTo
        var listBp = from bp in db.tbBoPhans select bp;

        //load cbbGio
        for (int i = 0; i <= 23; i++)
        {
            cbbGio.Items.Add(i.ToString());
        }

        //load cbbPhut
        for (int i = 0; i <= 59; i++)
        {
            if (i < 10)
            {
                cbbPhut.Items.Add("0" + i.ToString());
            }
            else
            {
                cbbPhut.Items.Add(i.ToString());
            }
        }

        //load cbbFormat
        cbbFormat.Items.Add("Online");
        cbbFormat.Items.Add("Offline");
        var getBoPhan = from bp in db.tbQuanTri_ToChuyenMons
                        join cmgv in db.tbQuanTri_ToChuyenMon_GiaoViens on bp.tocm_id equals cmgv.tocm_id
                        where cmgv.username_id == _idUser
                        select new
                        {
                            bp.tocm_id,
                            bp.tocm_name,
                        };
        ddlBoPhan.DataSource = getBoPhan;
        ddlBoPhan.DataBind();

    }


    public void loadData()
    {
        if (Request.Cookies["UserName"] != null)
        {
            var checkuserid = (from u in db.admin_Users where u.username_username == Request.Cookies["UserName"].Value select u).First();
            var getData = from ch in db.tbQuanTri_CuocHops
                          join chct in db.tbQuanTri_CuocHop_ChiTiets on ch.cuochop_id equals chct.cuochop_id
                          //join bp in db.tbBoPhans on ch.bophan_id equals bp.bophan_id
                          join cm in db.tbQuanTri_ToChuyenMons on ch.bophan_id equals cm.tocm_id
                          where ch.hidden == false && ch.username_id == checkuserid.username_id// && ch.bophan_id == checkuserid.bophan_id//khong bi xoa
                          orderby ch.cuochop_createdate descending
                          select new
                          {
                              ch.cuochop_id,
                              //bp.bophan_name,
                              ch.cuochop_title,
                              ngayhop = Convert.ToDateTime(chct.cuochopchitiet_ngay).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                              chct.cuochopchitiet_gio,
                              chct.cuochopchitiet_thu,
                              ch.cuochop_status,
                              ch.cuochop_format,
                              ch.cuochop_content,
                              cm.tocm_name,
                              xembaocao = db.tbQuanTri_NhanXetBaoCaoHangTuans.Any(x => x.cuochop_id == ch.cuochop_id) == true ? "Xem" : "",
                          };
            grvListCuocHop.DataSource = getData;
            grvListCuocHop.DataBind();

        }
    }
    //kiểm tra trùng cuộc họp
    private bool checkCuocHop(DateTime ngayhop, string giohop, int username_id)
    {
        //string name = hoten.ToLower();
        var check_CuocHop = db.tbQuanTri_CuocHop_ChiTiets.Any(x => x.cuochopchitiet_ngay == ngayhop
        && x.cuochopchitiet_gio == giohop && x.username_id == username_id);
        if (check_CuocHop == true)
            return false;
        else
            return true;
    }
    protected void btnThem_Click(object sender, EventArgs e)
    {
        //var a = ddlBoPhan.SelectedItem.Text;

        var checkuserid = (from u in db.admin_Users where u.username_username == Request.Cookies["UserName"].Value select u).First();
        if (dteCreateDate.Value == "" || cbbGio.SelectedItem.Text == ""
       || cbbPhut.SelectedItem.Text == "" || cbbFormat.SelectedItem.Text == "" || txtNoiDungCuocHop.Value == "" || ddlBoPhan.SelectedItem.Text == "")
        {
            alert.alert_Warning(Page, "Vui lòng nhập đầy đủ thông tin!", "");
        }
        else
        {
            if (checkCuocHop(Convert.ToDateTime(dteCreateDate.Value), cbbGio.SelectedItem.Text + " : " + cbbPhut.SelectedItem.Text, _idUser) == false)
            {
                loadData();
                ResetData();
            }
            else
            {
                //var findBp = (from bp in db.tbBoPhans where bp.bophan_name == cbbTo.SelectedItem.Text select bp).FirstOrDefault();
                string dayOfWeek = Convert.ToString((int)Convert.ToDateTime(dteCreateDate.Value).DayOfWeek + 1);

                //get thông tin lịch công tác tuần cuối cùng để xem ngày đăng ký họp có bị quá hạn không
                var check_LCT = (from lct in db.tbLichCongTacs
                                 where lct.lichcongtac_hidden == null
                                 orderby lct.lichcongtac_id descending
                                 select lct).Take(1).First();

                // insert new cuochop
                tbQuanTri_CuocHop ch = new tbQuanTri_CuocHop();
                //ch.cuochop_title = "Cuộc họp định kỳ tổ " + cbbTo.SelectedItem.Text + " vào thứ " + dayOfWeek;
                ch.cuochop_status = "Chưa xem";
                ch.cuochop_content = txtNoiDungCuocHop.Value;
                ch.cuochop_createdate = DateTime.Now;
                //ch.bophan_id = checkuserid.bophan_id;          
                ch.bophan_id = Convert.ToInt32(ddlBoPhan.SelectedItem.Value);
                ch.username_id = _idUser;
                ch.hidden = false;
                ch.cuochop_format = cbbFormat.SelectedItem.Text;
                if (DateTime.Now >= check_LCT.lichcongtac_tungay && DateTime.Now <= check_LCT.lichcongtac_denngay)
                    ch.cuochop_trangthai = "Bình thường";
                else
                    ch.cuochop_trangthai = "Quá hạn";
                //SendMail("ducpn@vjis.edu.vn,ngocdd@vjis.edu.vn");
                db.tbQuanTri_CuocHops.InsertOnSubmit(ch);
                db.SubmitChanges();

                tbQuanTri_CuocHop_ChiTiet chct = new tbQuanTri_CuocHop_ChiTiet();
                chct.cuochopchitiet_gio = cbbGio.SelectedItem.Text + " : " + cbbPhut.SelectedItem.Text;
                chct.cuochopchitiet_ngay = Convert.ToDateTime(dteCreateDate.Value);
                chct.cuochop_id = ch.cuochop_id;
                chct.username_id = _idUser;
                db.tbQuanTri_CuocHop_ChiTiets.InsertOnSubmit(chct);
                db.SubmitChanges();
                //SendMail(ducpn@vjis.edu.vn);
                SendMail("ducpn@vjis.edu.vn");
                loadData();
                alert.alert_Success(Page, "Đã tạo cuộc họp!", "");
                ResetData();
            }
        }
    }

    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        //on process
        options = "Sửa";
        //cbbTo.Enabled = false;
        List<object> selectedId = grvListCuocHop.GetSelectedFieldValues(new string[] { "cuochop_id" });
        if (selectedId.Count == 1)
        {
            foreach (var item in selectedId)
            {
                _idCuocHop = Convert.ToInt32(item);
            }
            var findData = (from ch in db.tbQuanTri_CuocHops
                            join chct in db.tbQuanTri_CuocHop_ChiTiets on ch.cuochop_id equals chct.cuochop_id
                            join bp in db.tbBoPhans on ch.bophan_id equals bp.bophan_id
                            where ch.cuochop_id == _idCuocHop
                            select new
                            {
                                bp.bophan_name,
                                chct.cuochopchitiet_gio,
                                chct.cuochopchitiet_ngay,
                                ch.cuochop_format,
                                ch.cuochop_status,
                            }).FirstOrDefault();
            if (findData.cuochop_status == "Đã xem")
            {
                alert.alert_Info(Page, "Cuộc họp đã được xem. Không thể sửa!", "");
            }
            else
            {
                string[] time = findData.cuochopchitiet_gio.Split(':');
                var hour = time[0];
                var minute = time[1];
                cbbFormat.Text = findData.cuochop_format;
                //cbbTo.Text = findData.bophan_name;
                var value = ((Convert.ToDateTime(findData.cuochopchitiet_ngay)).ToString("yyyy-MM-dd"));
                dteCreateDate.Value = value;
                cbbGio.Text = hour;
                cbbPhut.Text = minute.Replace(" ", "");

                ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupControl.Show();", true);
            }

        }
        else if (selectedId.Count == 0)
        {
            ;
        }
        else if (selectedId.Count > 1)
        {
            alert.alert_Warning(Page, "Chỉ được chọn 1 cuộc họp", "");
        }

    }

    protected void btnXoa_Click(object sender, EventArgs e)
    {
        try
        {
            List<object> selectedId = grvListCuocHop.GetSelectedFieldValues(new string[] { "cuochop_id" });
            if (selectedId.Count == 1)
            {
                foreach (var item in selectedId)
                {
                    _idCuocHop = Convert.ToInt32(item);
                }

                //hide cuoc hop
                var delete = (from ch in db.tbQuanTri_CuocHops
                              where ch.cuochop_id == _idCuocHop
                              select ch).FirstOrDefault();
                delete.hidden = true;
                db.SubmitChanges();
                loadData();
                alert.alert_Success(Page, "Xóa thành công!", "");
            }
            else if (selectedId.Count == 0)
            {
            }
            else if (selectedId.Count > 1)
            {
                alert.alert_Warning(Page, "Chỉ được chọn 1 cuộc họp để xóa!", "");
            }
        }
        catch (Exception)
        {
            alert.alert_Error(Page, "Lỗi! Xin vui lòng liên hệ tổ IT!", "");
        }
    }
    private bool SendMail(string email)
    {

        if (email != "")
        {
            try
            {
                var fromAddress = "thongbaovietnhatschool@gmail.com";//  Email Address from where you send the mail 
                var toAddress = email;
                const string fromPassword = "neiabcekdjluofid";
                string subject, title;
                title = "Thông báo";
                subject = "<!DOCTYPE html><html><head><title></title></head><body ><div>" +
                "<h3 style=\"margin-top:0px; text-align:center; color:#029ada\">Có giáo viên đăng ký họp định kì. Xem chi tiết <a href='http://quantri.vietnhatschool.edu.vn/admin-duyet-cuoc-hop'>tại đây.</a></h3>" +
                "</div></body></html>";
                var smtp = new SmtpClient();
                {
                    smtp.Host = "smtp.gmail.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    smtp.Credentials = new NetworkCredential(fromAddress, fromPassword);
                    smtp.Timeout = 20000;
                }
                MailMessage mm = new MailMessage();
                mm.From = new MailAddress(fromAddress, "Trường Liên cấp Việt Nhật");
                mm.Subject = title;
                mm.To.Add(toAddress);
                mm.IsBodyHtml = true;
                mm.Body = subject;
                smtp.Send(mm);
                return true;
            }
            catch
            {
                return false;
            }
        }
        else
            return false;
    }
    protected void btnLuu_Click(object sender, EventArgs e)
    {

        //else if (options == "Sửa")
        //{
        //    if (dteCreateDate.Value == "" || cbbGio.Text == ""
        // || cbbPhut.Text == "" || cbbFormat.Text == "")
        //    {
        //        alert.alert_Warning(Page, "Vui lòng nhập đầy đủ thông tin!", "");
        //    }
        //    else
        //    {
        //        string dayOfWeek = Convert.ToString((int)Convert.ToDateTime(dteCreateDate.Value).DayOfWeek + 1);
        //        tbQuanTri_CuocHop updateCh = (from c in db.tbQuanTri_CuocHops where c.cuochop_id == _idCuocHop select c).FirstOrDefault();
        //        tbQuanTri_CuocHop_ChiTiet updateChct = (from chct in db.tbQuanTri_CuocHop_ChiTiets where chct.cuochop_id == _idCuocHop select chct).FirstOrDefault();

        //        updateCh.cuochop_format = cbbFormat.SelectedItem.Text;
        //        updateCh.cuochop_createdate = DateTime.Now;
        //        updateChct.cuochopchitiet_ngay = Convert.ToDateTime(dteCreateDate.Value);
        //        updateChct.cuochopchitiet_gio = cbbGio.Text + " : " + cbbPhut.Text;

        //        db.SubmitChanges();
        //        popupControl.ShowOnPageLoad = false;
        //        loadData();
        //        ResetData();
        //        alert.alert_Success(Page, "Cập nhật cuộc họp thành công!", "");
        //    }
        //}

    }


    //protected void cbbFormat_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    ASPxComboBox comboBox = (ASPxComboBox)sender;
    //    cbbFormat.Text = comboBox.SelectedItem.ToString();
    //    if(cbbFormat.Text =="Online")
    //    {
    //        divLink.Visible = true;
    //    }
    //    else
    //    {
    //        divLink.Visible = false;
    //    }
    //}

    protected void btnLuuImg_Click(object sender, EventArgs e)
    {
        try
        {
            if (!fuUpload.HasFile)
            {
                alert.alert_Warning(Page, "Chưa chọn file báo cáo!", "");
            }
            else
            {
                string IMG_path = "";
                HttpFileCollection _HttpFileCollection = Request.Files;
                //find baocaodinhky
                var checkBaoCaoDinhKy = (from bcdk in db.tbQuanTri_BaoCaoDinhKies
                                         where bcdk.cuochop_id == _idCuocHop
                                         select bcdk).FirstOrDefault();

                //neu da ton tai
                if (checkBaoCaoDinhKy != null)
                {
                    //for (int i = 0; i < _HttpFileCollection.Count; i++)
                    //{
                    //HttpPostedFile _HttpPostedFile = _HttpFileCollection[i];
                    //if (_HttpPostedFile.ContentLength > 0)
                    //{
                    //    IMG_path = ("/images_cuochop/" + DateTime.Now.ToString("dd-MM-yyyy_hh-mm-ss_") + Path.GetFileName(_HttpPostedFile.FileName));
                    //    _HttpPostedFile.SaveAs(Server.MapPath(IMG_path));
                    //}

                    //inset hinh anh
                    String folderUser = Server.MapPath("~/uploadimages/file-bao-cao-cuoc-hop-tuan/");
                    if (!Directory.Exists(folderUser))
                    {
                        Directory.CreateDirectory(folderUser);
                    }
                    string ulr = "/uploadimages/file-bao-cao-cuoc-hop-tuan/";
                    string filename = Path.GetFileName(fuUpload.FileName);
                    string fileName_save = Path.Combine(Server.MapPath("~/uploadimages/file-bao-cao-cuoc-hop-tuan/"), DateTime.Now.ToString("dd-MM-yyyy_hh-mm-ss_") + filename);
                    fuUpload.SaveAs(fileName_save);
                    IMG_path = ulr + filename;
                    //tbQuanTri_Images_BaoCao insert = new tbQuanTri_Images_BaoCao();
                    //insert.baocaodinhky_id = checkBaoCaoDinhKy.baocaodinhky_id;
                    //insert.imgbaocao_path = IMG_path;
                    //db.tbQuanTri_Images_BaoCaos.InsertOnSubmit(insert);
                    //db.SubmitChanges();
                    //}
                    //ghi đè file báo cáo
                    var updateBaoCao = (from bc in db.tbQuanTri_Images_BaoCaos
                                        where bc.baocaodinhky_id == checkBaoCaoDinhKy.baocaodinhky_id
                                        select bc).Single();
                    updateBaoCao.imgbaocao_path = IMG_path;
                    db.SubmitChanges();
                }
                else
                {
                    //insert bao cao dinh ky
                    tbQuanTri_BaoCaoDinhKy baocao = new tbQuanTri_BaoCaoDinhKy();
                    baocao.baocaodinhky_createdate = DateTime.Now;
                    baocao.cuochop_id = _idCuocHop;
                    baocao.hidden = false;
                    baocao.username_id = _idUser;
                    baocao.baocaodinhky_content = "Báo cáo";
                    //baocao.baocaodinhky_linkvideo = null;
                    db.tbQuanTri_BaoCaoDinhKies.InsertOnSubmit(baocao);
                    db.SubmitChanges();

                    //for (int i = 0; i < _HttpFileCollection.Count; i++)
                    //{
                    //    HttpPostedFile _HttpPostedFile = _HttpFileCollection[i];
                    //    if (_HttpPostedFile.ContentLength > 0)
                    //    {
                    //        IMG_path = ("/images_cuochop/" + DateTime.Now.ToString("dd-MM-yyyy_hh-mm-ss_") + Path.GetFileName(_HttpPostedFile.FileName));
                    //        _HttpPostedFile.SaveAs(Server.MapPath(IMG_path));
                    //    }
                    String folderUser = Server.MapPath("~/uploadimages/file-bao-cao-cuoc-hop-tuan/");
                    if (!Directory.Exists(folderUser))
                    {
                        Directory.CreateDirectory(folderUser);
                    }
                    string ulr = "/uploadimages/file-bao-cao-cuoc-hop-tuan/";
                    string filename = Path.GetFileName(fuUpload.FileName);
                    string fileName_save = Path.Combine(Server.MapPath("~/uploadimages/file-bao-cao-cuoc-hop-tuan/"), DateTime.Now.ToString("dd-MM-yyyy_hh-mm-ss_") + filename);
                    fuUpload.SaveAs(fileName_save);
                    IMG_path = ulr + filename;
                    //inset hinh anh
                    tbQuanTri_Images_BaoCao insert = new tbQuanTri_Images_BaoCao();
                    insert.baocaodinhky_id = baocao.baocaodinhky_id;
                    insert.imgbaocao_path = IMG_path;
                    db.tbQuanTri_Images_BaoCaos.InsertOnSubmit(insert);
                    db.SubmitChanges();
                    //}
                }
                //alert.alert_Success(Page, "Báo cáo thành công!", "");
                //LoadImgBaoCao();
                ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Báo cáo thành công!','','success').then(function(){grvListCuocHop.UnselectRows();})", true);
                popupControl.ShowOnPageLoad = false;
            }
        }
        catch (Exception)
        {
            alert.alert_Error(Page, "Lỗi! Vui lòng liên hệ tổ IT", "");
        }
    }

    protected void btnBaoCao_Click(object sender, EventArgs e)
    {
        List<object> selectedId = grvListCuocHop.GetSelectedFieldValues(new string[] { "cuochop_id" });
        if (selectedId.Count == 1)
        {
            foreach (var item in selectedId)
            {
                _idCuocHop = Convert.ToInt32(item);
            }
            //kiểm tra nếu cuộc nộp báo cáo quá 24h sau cuộc họp diễn ra thì không cho nộp
            var check_NgayHop = (from ch in db.tbQuanTri_CuocHops
                                 join chct in db.tbQuanTri_CuocHop_ChiTiets on ch.cuochop_id equals chct.cuochop_id
                                 where ch.cuochop_id == _idCuocHop
                                 select chct.cuochopchitiet_ngay).Single();
            if (DateTime.Now > Convert.ToDateTime(check_NgayHop).AddDays(2))
                alert.alert_Error(Page, "Đã hết hạn thời gian nộp báo cáo!", "Vui lòng kiểm tra lại");
            else
            {
                LoadImgBaoCao();
                ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Update", "popupControl.Show();", true);
            }
        }
        else if (selectedId.Count == 0)
        {
            alert.alert_Warning(Page, "Chưa chọn cuộc họp nào!", "");
        }
        else if (selectedId.Count > 1)
        {
            alert.alert_Warning(Page, "Chỉ được chọn 1 cuộc họp!", "");
        }
    }

    public void LoadImgBaoCao()
    {
        //load nội  dung báo  cáo
        var findData = (from ch in db.tbQuanTri_CuocHops
                        join bcdk in db.tbQuanTri_BaoCaoDinhKies on ch.cuochop_id equals bcdk.cuochop_id
                        join img in db.tbQuanTri_Images_BaoCaos on bcdk.baocaodinhky_id equals img.baocaodinhky_id
                        where ch.cuochop_id == _idCuocHop
                        select img).Single();
        string embed = "<object data=\"{0}\" type=\"application/pdf\" width=\"100%\" height=\"700px\">";
        embed += "</object>";
        ltEmbed.Text = string.Format(embed, ResolveUrl(findData.imgbaocao_path));
    }

    protected void btnDelImg_ServerClick(object sender, EventArgs e)
    {
        bool success = false;
        var delImg = (from img in db.tbQuanTri_Images_BaoCaos
                      where img.imgbaocao_path == txtImgPath.Value
                      select img).FirstOrDefault();
        String path = Server.MapPath(txtImgPath.Value);

        if (File.Exists(path))
        {
            File.Delete(path);
            success = true;
        }

        if (success)
        {
            db.tbQuanTri_Images_BaoCaos.DeleteOnSubmit(delImg);
            db.SubmitChanges();
        }
        alert.alert_Success(Page, "Xóa thành công", "");
        LoadImgBaoCao();
    }

    protected void btnXemNhanXetBaoCao_ServerClick(object sender, EventArgs e)
    {
        int _idcuohop = Convert.ToInt32(txtCuocHop_ID.Value);
        var getNhanXet = from nx in db.tbQuanTri_NhanXetBaoCaoHangTuans
                         where nx.cuochop_id == _idcuohop
                         select nx;
        rptNoiDungNhanXet.DataSource = getNhanXet;
        rptNoiDungNhanXet.DataBind();
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "View", "popupNhanXetBaoCao.Show();", true);
    }
}