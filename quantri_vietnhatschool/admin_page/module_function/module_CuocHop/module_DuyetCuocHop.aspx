﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_DuyetCuocHop.aspx.cs" Inherits="admin_page_module_function_module_DuyetCuocHop" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">

    <div style="text-align: center; font-size: 28px; font-weight: bold; color: blue">
        <label>DANH SÁCH CÁC CUỘC HỌP ĐĂNG KÝ</label>
    </div>
    <div class="form-group row" >
        <div style="display:none">
           <asp:UpdatePanel ID="upButton" runat="server">
            <ContentTemplate>
                <asp:Button ID="btnDuyet" runat="server" Text="Duyệt" CssClass="btn btn-primary" OnClick="btnDuyet_Click" />
            </ContentTemplate>
        </asp:UpdatePanel>
        </div>
   

        <asp:UpdatePanel ID="upListCuocHop" runat="server">
            <ContentTemplate>

                <dx:ASPxGridView ID="grvListCuocHop" runat="server" CssClass="table-hover" ClientInstanceName="grvListCuocHop" KeyFieldName="cuochop_id" Width="100%">
                    <Columns>
                        <dx:GridViewCommandColumn ShowSelectCheckbox="True" SelectAllCheckboxMode="Page" VisibleIndex="0" Width="2%">
                        </dx:GridViewCommandColumn>
                        <dx:GridViewDataColumn Caption="Tổ" FieldName="tocm_name" HeaderStyle-HorizontalAlign="Center" Width="15%" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="Nội dung cuộc họp" FieldName="cuochop_content" HeaderStyle-HorizontalAlign="Center" Width="30%" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>
                        <%-- <dx:GridViewDataColumn Caption="Thứ" FieldName="cuochopchitiet_thu" HeaderStyle-HorizontalAlign="Center" Width="10%" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>--%>
                        <dx:GridViewDataColumn Caption="Ngày" FieldName="ngayhop" HeaderStyle-HorizontalAlign="Center" Width="20%" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="Giờ" FieldName="cuochopchitiet_gio" HeaderStyle-HorizontalAlign="Center" Width="10%" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="Hình thức" FieldName="cuochop_format" HeaderStyle-HorizontalAlign="Center" Width="30%" HeaderStyle-Font-Bold="true">
                        </dx:GridViewDataColumn>
                        <dx:GridViewDataColumn Caption="Trạng thái" FieldName="cuochop_status" HeaderStyle-HorizontalAlign="Center" Width="10%" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>
                    </Columns>
                    <SettingsSearchPanel Visible="true" />
                    <SettingsBehavior AllowFocusedRow="true" />
                    <SettingsText EmptyDataRow="Trống" SearchPanelEditorNullText="Gõ từ cần tìm kiếm và enter..." />
                    <SettingsLoadingPanel Text="Đang tải..." />
                    <SettingsPager PageSize="10" Summary-Text="Trang {0} / {1} ({2} trang)"></SettingsPager>
                </dx:ASPxGridView>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>


    <dx:ASPxPopupControl ID="popupControl" runat="server" Width="700" Height="170" CloseAction="CloseButton" ShowCollapseButton="True" ShowMaximizeButton="True" ScrollBars="Auto" CloseOnEscape="true" Modal="True"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" ClientInstanceName="popupControl" ShowFooter="true"
        HeaderText="Nhập link Teams" AllowDragging="True" PopupAnimationType="Fade" EnableViewState="False" AutoUpdatePosition="true" ClientSideEvents-CloseUp="function(s,e){grvListCuocHop.Refresh();}">
        <ContentCollection>
            <dx:PopupControlContentControl runat="server">
                <asp:UpdatePanel ID="udPopup" runat="server">
                    <ContentTemplate>
                        <div class="popup-main">
                            <div class="div_content col-12">
                                <div class="col-12 form-group">
                                    <label class="col-3 form-control-label">Link Teams: </label>
                                    <div class="col-9">
                                        <asp:TextBox ID="txtLinkHop" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="90%"> </asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <FooterContentTemplate>
            <div class="mar_but button">
                <asp:UpdatePanel ID="udSave" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnHoanTat" runat="server" Text="Hoàn tất" CssClass="btn btn-primary" OnClick="btnHoanTat_Click" ClientMode="static" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </FooterContentTemplate>
        <ContentStyle>
            <Paddings PaddingBottom="0px" />
        </ContentStyle>
    </dx:ASPxPopupControl>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

