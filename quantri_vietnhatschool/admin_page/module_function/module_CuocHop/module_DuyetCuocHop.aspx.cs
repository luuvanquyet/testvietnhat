﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_DuyetCuocHop : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private static int _idUser;
    private static int _idCuocHop;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        ManageData();
        LoadData();
    }

    private bool SendMail(string email, string format = "Online")
    {
        return false;
        //if (email != "")
        //{
        //    try
        //    {
        //        var ch = (from c in db.tbQuanTri_CuocHops
        //                  join chct in db.tbQuanTri_CuocHop_ChiTiets on c.cuochop_id equals chct.cuochop_id
        //                  where c.cuochop_id == _idCuocHop
        //                  select new
        //                  { 
        //                  c.cuochop_id,
        //             //     c.cuochop_content,
        //                  chct.cuochopchitiet_gio,
        //                  chct.cuochopchitiet_ngay,
        //                  }).FirstOrDefault();

        //        string linkhop = txtLinkHop.Text; 
        //     //   string noidung = ch.cuochop_content + " vào lúc " + ch.cuochopchitiet_gio + ", ngày: " + Convert.ToDateTime(ch.cuochopchitiet_ngay).ToString("dd/MM/yyyy") ;

        //        var fromAddress = "thongbaovietnhatschool@gmail.com";//  Email Address from where you send the mail 
        //        var toAddress = email;
        //        const string fromPassword = "neiabcekdjluofid";
        //        string  title;
        //        title = "Thông báo: Xác nhận duyệt đăng ký cuộc họp";

        //        var smtp = new System.Net.Mail.SmtpClient();
        //        {
        //            smtp.Host = "smtp.gmail.com";
        //            smtp.Port = 587;
        //            smtp.EnableSsl = true;
        //            smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
        //            smtp.Credentials = new NetworkCredential(fromAddress, fromPassword);
        //            smtp.Timeout = 20000;
        //        }

        //        MailMessage mm = new MailMessage();
        //        mm.From = new MailAddress(fromAddress, "Trường Liên cấp Việt Nhật");
        //        mm.Subject = title;
        //        mm.To.Add(toAddress);
        //        mm.IsBodyHtml = true;
        //        if (format == "Online")
        //        {
        //            mm.Body = "<!DOCTYPE html><html><head><title></title></head><body ><div>" +
        //                       "<h3 style=\"margin-top:0px; text-align:center; color:#029ada\">Cuộc họp bạn đăng ký đã được xác nhận!. " +
        //                       "<br />Nội dung: " + /*noidung +*/ "<br />Đường dẫn tới cuộc họp:" + " <a href='" + linkhop + "'> LINK </a></h3>" +
        //                       "</div></body></html>";
        //        }
        //        else if(format == "Offline")
        //        {
        //            mm.Body = "<!DOCTYPE html><html><head><title></title></head><body ><div>" +
        //                      "<h3 style=\"margin-top:0px; text-align:center; color:#029ada\">Cuộc họp bạn đăng ký đã được xác nhận!. " +
        //                      "<br />Nội dung: " + /*noidung +*/ "</h3>" +
        //                      "</div></body></html>";
        //        }
        //        smtp.Send(mm);
        //        return true;
        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //}
        //else
        //    return false;
    }

    public void ManageData()
    {
        var manageData = from ch in db.tbQuanTri_CuocHops
                      join chct in db.tbQuanTri_CuocHop_ChiTiets on ch.cuochop_id equals chct.cuochop_id
                      join bp in db.tbQuanTri_ToChuyenMons on ch.bophan_id equals bp.tocm_id
                      where ch.hidden == false //khong bi xoa
                      select new
                      {
                          ch.cuochop_id,
                          ch.cuochop_status,
                      };

        foreach (var cuoc_hop in manageData.ToList())
        {
            if (cuoc_hop.cuochop_status == "Chưa xem")
            {
                var find = (from ch in db.tbQuanTri_CuocHops where ch.cuochop_id == cuoc_hop.cuochop_id select ch).FirstOrDefault();
                find.cuochop_status = "Đã xem";
                db.SubmitChanges();
            }
            else {; }
        }

    }

    public void LoadData()
    {
        var getData = from ch in db.tbQuanTri_CuocHops
                      join chct in db.tbQuanTri_CuocHop_ChiTiets on ch.cuochop_id equals chct.cuochop_id
                      //join bp in db.tbBoPhans on ch.bophan_id equals bp.bophan_id
                      join cm in db.tbQuanTri_ToChuyenMons on ch.bophan_id equals cm.tocm_id
                      where ch.hidden == false //khong bi xoa
                      orderby ch.cuochop_createdate descending, ch.cuochop_id descending
                      select new
                      {
                          ch.cuochop_id,
                          //bp.bophan_name,
                          ch.cuochop_title,
                          ch.cuochop_content,
                          ngayhop = Convert.ToDateTime(chct.cuochopchitiet_ngay).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                          chct.cuochopchitiet_gio,
                          chct.cuochopchitiet_thu,
                          ch.cuochop_format,
                          ch.cuochop_status,
                          cm.tocm_name,
                      };
        grvListCuocHop.DataSource = getData;
        grvListCuocHop.DataBind();
    }

    protected void btnDuyet_Click(object sender, EventArgs e)
    {
        //List<object> selectedId = grvListCuocHop.GetSelectedFieldValues(new string[] { "cuochop_id" });
        //if (selectedId.Count == 1)
        //{
        //    foreach (var item in selectedId)
        //    {
        //        _idCuocHop = Convert.ToInt32(item);
        //    }

        //    var ch = (from c in db.tbQuanTri_CuocHops where c.cuochop_id == _idCuocHop select c).FirstOrDefault();
        //    var format = ch.cuochop_format;
        //    if (format == "Online")
        //    { 
        //        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Update", "popupControl.Show();", true); 
        //    }    

        //    else if (format == "Offline")
        //    {
        //        ch.cuochop_status = "Đã duyệt";

        //        var user = (from u in db.admin_Users
        //                    join c in db.tbQuanTri_CuocHops on u.username_id equals c.username_id
        //                    where c.cuochop_id == _idCuocHop
        //                    select u).FirstOrDefault();

        //        if (SendMail(user.username_email, format))
        //        {
        //            db.SubmitChanges(); 
        //            loadData();
        //            popupControl.ShowOnPageLoad = false;
        //            alert.alert_Success(Page, "Duyệt thành công!", "");
        //        }
        //        else
        //        {
        //            alert.alert_Error(Page, "Lỗi!", "");
        //        }

        //    }    

       
        //}
        //else if (selectedId.Count == 0)
        //{
        //    ;
        //}
        //else if (selectedId.Count > 1)
        //{
        //    alert.alert_Warning(Page, "Chỉ được chọn 1 cuộc họp để duyệt!", "");
        //}
     
    }

    protected void btnHoanTat_Click(object sender, EventArgs e)
    {

        //if (txtLinkHop.Text == "")
        //{
        //    alert.alert_Warning(Page, "Không được để trống!", "");
        //}
        //else
        //{
        //    //duyet
        //    var duyetCh = (from ch in db.tbQuanTri_CuocHops
        //                   where ch.cuochop_id == _idCuocHop
        //                   select ch).FirstOrDefault();
        //    duyetCh.cuochop_status = "Đã duyệt";

        //    var duyetChct = (from chct in db.tbQuanTri_CuocHop_ChiTiets
        //                     where chct.cuochop_id == _idCuocHop
        //                     select chct).FirstOrDefault();
        //    //only online
        //    duyetChct.cuochopchitiet_linkhop = txtLinkHop.Text;

        //    var user = (from u in db.admin_Users
        //                join ch in db.tbQuanTri_CuocHops on u.username_id equals ch.username_id
        //                where ch.cuochop_id == _idCuocHop
        //                select u).FirstOrDefault();

        //    if (SendMail(user.username_email))
        //    {
        //        db.SubmitChanges();
        //        loadData();
        //        popupControl.ShowOnPageLoad = false;
        //        alert.alert_Success(Page, "Duyệt thành công!", "");
        //    }
        //    else
        //    {
        //        alert.alert_Error(Page, "Lỗi!", "");
        //    }
        //}
    }
}