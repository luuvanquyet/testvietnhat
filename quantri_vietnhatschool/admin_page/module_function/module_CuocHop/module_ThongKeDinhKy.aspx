﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_ThongKeDinhKy.aspx.cs" Inherits="admin_page_module_function_module_ThongKeDinhKy" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">

    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css" />
    <script>
        function BaoCaoHinhAnh(id_cuochop) {
            document.getElementById("<%=txtCuocHop.ClientID%>").value = id_cuochop;
            document.getElementById("<%=btnShowImgBaoCao.ClientID%>").click();

        }

        function onClick(element) {
            document.getElementById("img01").src = element.src;
            document.getElementById("modal01").style.display = "block";
        }
        function myChonBoPhan(id_bophan) {
            document.getElementById("<%=txtBoPhan.ClientID%>").value = id_bophan;
            document.getElementById("<%=btnBoPhanChon.ClientID%>").click();
            var element = document.querySelectorAll("a.btn-grad-active");
            if (element.length > 0) {
                element[0].classList.remove('btn-grad-active');
            }
            document.getElementById("btnChon_" + id_bophan).classList.add('btn-grad-active');


        }
        function myBaoCao() {
            var xemBaoCao = document.getElementsByClassName("btnXemBaoCao");

            for (var i = 0; i < xemBaoCao.length; i++) {
                if (xemBaoCao[i].innerHTML == "Chưa có báo cáo")
                    xemBaoCao[i].style.pointerEvents = "none"
                else {
                    xemBaoCao[i].style.pointerEvents = "auto";
                    xemBaoCao[i].classList.add("btn", "btn-primary");
                }
            }
        }
    </script>
    <style>
        .btn-grad {
            background-image: linear-gradient(to right, #77A1D3 0%, #79CBCA 51%, #77A1D3 100%);
        }

        .btn-grad {
            width: 11.77%;
            display: grid;
            margin: 7px;
            padding: 1px 0px;
            text-align: center;
            text-transform: uppercase;
            transition: 0.5s;
            background-size: 200% auto;
            color: white;
            box-shadow: 0 0 20px #eee;
            border-radius: 6px;
            cursor: pointer;
        }

            .btn-grad span.count_number {
                color: #3f00a5;
                font-weight: bold;
                font-size: 22px;
            }

            .btn-grad:hover {
                background-position: right center; /* change the direction of the change here */
                color: #fff;
                text-decoration: none;
            }

        .btn-grad-active {
            background: #0d63bc;
        }

            .btn-grad-active span.count_number {
                color: #e0d00d;
            }
    </style>
    <div class="form-group row">
        <h3 style="text-align: center; font-size: 28px; font-weight: bold; color: blue">THỐNG KÊ ĐỊNH KỲ</h3>
        <div id="div_TuNgayToiNgay" runat="server" class="col-12" style="margin-top: 10px">
            <div class="col-4" style="padding: 0 1% 0 0;">
                <label>Từ ngày:</label>
                <input type="date" id="dteTuNgay" class="form-control" runat="server" />
            </div>
            <div class="col-4">
                <label>Đến ngày:</label>
                <input type="date" id="dteDenNgay" class="form-control" runat="server" />
            </div>
            <div class="col-4" style="padding: 3% 0 0 1%;">
                <asp:Button ID="btnChon" runat="server" Text="Kiểm tra" CssClass="btn btn-primary" OnClick="btnChon_Click" />
            </div>
        </div>
        <div class="row col-12" style="text-align: center">
            Danh sách các tổ chuyển môn
        </div>
        <div class="row col-12" style="text-align: center">
            <asp:Repeater ID="rpBoPhanTo" runat="server">
                <ItemTemplate>
                    <a class="btn-grad col-1" id="btnChon_<%#Eval("bophan_id")%>" onclick='myChonBoPhan("<%#Eval("bophan_id")%>")'>
                        <span style="font-weight: bold; color: white; padding-top: 5px;"><%#Eval("bophan_name")%></span>
                        <span class="count_number"><%#Eval("bophan_soluong")%>/<%#Eval("tong_cuochop")%></span>
                    </a>
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <div class="row col-12" style="text-align: center">
            Danh sách các tổ nhóm môn
        </div>
        <div class="row col-12" style="text-align: center">
            <asp:Repeater ID="rpBoPhanToNhom" runat="server">
                <ItemTemplate>
                    <a class="btn-grad col-1" id="btnChon_<%#Eval("bophan_id")%>" onclick='myChonBoPhan("<%#Eval("bophan_id")%>")'>
                        <span style="font-weight: bold; color: white; padding-top: 5px;"><%#Eval("bophan_name")%></span>
                        <span class="count_number"><%#Eval("bophan_soluong")%>/<%#Eval("tong_cuochop")%></span>
                    </a>
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <asp:UpdatePanel ID="upChiTiet" runat="server">
            <ContentTemplate>


                <div id="div_ChiTietCacCuocHop" runat="server" class="col-12" style="margin-top: 10px; width: 100%">
                    <div class="form-group table-responsive">
                        <table style="background-color: white" class="table table-bordered table-hover">
                            <thead>
                                <tr class="table-info table__point-head">
                                    <th scope="col">STT</th>
                                    <th scope="col">Nội dung cuộc họp</th>
                                    <th scope="col">Ngày họp</th>
                                    <th scope="col">Giờ họp</th>
                                    <th scope="col">Trạng thái</th>
                                    <th scope="col">Hình thức</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater runat="server" ID="rpChiTietCuocHop">
                                    <ItemTemplate>
                                        <tr class="table-light table__point">
                                            <th scope="row"><%=STT++ %></th>
                                            <td><%#Eval("cuochop_content") %></td>
                                            <td><%#Eval("ngay") %></td>
                                            <td><%#Eval("cuochopchitiet_gio") %></td>
                                            <td><%# Eval("cuochop_trangthai") %></td>
                                            <td><%# Eval("cuochop_format") %></td>
                                            <td>
                                                <a href="javascript:void(0)" class="button check__point serif glass btnXemBaoCao" onclick="BaoCaoHinhAnh('<%#Eval("cuochop_id")%>')"><%#Eval("baocao")%></a>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                    </div>
                </div>

                <%-- Các hàm xử lý ẩn phía dưới--%>
                <div style="display: none">

                    <input type="text" id="txtBoPhan" runat="server" />
                    <a href="#" id="btnBoPhanChon" runat="server" onserverclick="btnChiTiet_Click"></a>
                    <%--  điều kiện xuất báo cáo--%>
                    <input type="text" id="txtCuocHop" runat="server" />
                    <a href="#" id="btnShowImgBaoCao" runat="server" onserverclick="btnShowImgBaoCao_ServerClick"></a>
                </div>
                <div class="div_content col-12">
                    <asp:Literal ID="ltEmbed" runat="server" />
                </div>
                <%--nhận xét báo cáo--%>
                <div id="div_guinhanxetbaocao" runat="server">
                    <div class="col-12 form-group">
                        <label class="col-2 form-control-label">Nhận xét:</label>
                        <div class="col-10">
                            <textarea id="txtNhanXet" runat="server" class="form-control" rows="3"></textarea>
                        </div>
                    </div>
                    <a href="#" id="btnGuiNhanXet" runat="server" class="btn btn-primary" onserverclick="btnGuiNhanXet_ServerClick">Gửi</a>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

