﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_ThongKeDinhKy : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private static int _idBoPhan;
    private static int _idCuocHop;
    private static int _idUser;
    public int STT = 1;
    private static DateTime tungay;
    private static DateTime denngay;
    protected void Page_Load(object sender, EventArgs e)
    {

        loadData();
        //LoadImgBaoCao();
        if (!IsPostBack)
        {
            div_ChiTietCacCuocHop.Visible = false;
            //grvListCuocHop.DataSource = null;
            //grvListCuocHop.DataBind();
            div_guinhanxetbaocao.Visible = false;
        }
        _idUser = (from u in db.admin_Users
                   where u.username_username == Request.Cookies["UserName"].Value
                   select u).FirstOrDefault().username_id;

    }

    public void loadData()
    {
        if (tungay != Convert.ToDateTime(null) || denngay != Convert.ToDateTime(null))
        {
            var getData = (from bp in db.tbQuanTri_ToChuyenMons
                           where bp.tocm_thongkebaocao_nhom == 1
                           orderby bp.tocm_thongkebaocao_nhom
                           group bp by bp.tocm_id into item
                           select new
                           {
                               bophan_id = item.Key,
                               bophan_name = (from b in db.tbQuanTri_ToChuyenMons where b.tocm_id == item.Key select b).First().tocm_name,
                               bophan_soluong = (from b in db.tbQuanTri_ToChuyenMons
                                                 join ch in db.tbQuanTri_CuocHops on b.tocm_id equals ch.bophan_id
                                                 join chct in db.tbQuanTri_CuocHop_ChiTiets on ch.cuochop_id equals chct.cuochop_id
                                                 where b.tocm_id == item.Key
                                                 && ch.hidden == false //khong bi xoa
                                                 && chct.cuochopchitiet_ngay.Value.Date >= tungay.Date
                                                 && chct.cuochopchitiet_ngay.Value.Date <= denngay.Date
                                                 && ch.cuochop_status == "Đã xem"
                                                 select new
                                                 {
                                                     ch.cuochop_id,
                                                 }).Count(),
                               tong_cuochop = (from ch in db.tbLichCongTacs
                                               where ch.lichcongtac_cuochopto == true
                                               && ch.lichcongtac_tungay.Value.Date >= tungay.Date
                                               && ch.lichcongtac_tungay.Value.Month >= tungay.Month
                                               && ch.lichcongtac_tungay.Value.Year >= tungay.Year
                                               && ch.lichcongtac_tungay.Value.Date <= denngay.Date
                                               && ch.lichcongtac_tungay.Value.Month <= denngay.Month
                                               && ch.lichcongtac_tungay.Value.Year <= denngay.Year
                                               && ch.lichcongtac_hidden == null
                                               select ch).Count(),

                           }); ;
            rpBoPhanTo.DataSource = getData;
            rpBoPhanTo.DataBind();

            var getDataNhom = (from bp in db.tbQuanTri_ToChuyenMons
                               where bp.tocm_thongkebaocao_nhom == 2
                               orderby bp.tocm_thongkebaocao_nhom
                               group bp by bp.tocm_id into item
                               select new
                               {
                                   bophan_id = item.Key,
                                   bophan_name = (from b in db.tbQuanTri_ToChuyenMons where b.tocm_id == item.Key select b).First().tocm_name,
                                   bophan_soluong = (from b in db.tbQuanTri_ToChuyenMons
                                                     join ch in db.tbQuanTri_CuocHops on b.tocm_id equals ch.bophan_id
                                                     join chct in db.tbQuanTri_CuocHop_ChiTiets on ch.cuochop_id equals chct.cuochop_id
                                                     where b.tocm_id == item.Key
                                                     && ch.hidden == false //khong bi xoa
                                                     && chct.cuochopchitiet_ngay.Value.Date >= tungay.Date
                                                     && chct.cuochopchitiet_ngay.Value.Date <= denngay.Date
                                                     && ch.cuochop_status == "Đã xem"
                                                     select new
                                                     {
                                                         ch.cuochop_id,
                                                     }).Count(),
                                   tong_cuochop = (from ch in db.tbLichCongTacs
                                                   where ch.lichcongtac_cuochopnhom == true
                                                   && ch.lichcongtac_tungay.Value.Date >= tungay.Date
                                                   && ch.lichcongtac_tungay.Value.Month >= tungay.Month
                                                   && ch.lichcongtac_tungay.Value.Year >= tungay.Year
                                                   && ch.lichcongtac_tungay.Value.Date <= denngay.Date
                                                   && ch.lichcongtac_tungay.Value.Month <= denngay.Month
                                                   && ch.lichcongtac_tungay.Value.Year <= denngay.Year
                                                   && ch.lichcongtac_hidden == null
                                                   select ch).Count(),
                               });
            rpBoPhanToNhom.DataSource = getDataNhom;
            rpBoPhanToNhom.DataBind();
        }
    }

    public void Reset()
    {
        dteDenNgay.Value = "";
        dteTuNgay.Value = "";
    }
    public void ShowChiTietCuocHop(int bophan_id)
    {
        div_ChiTietCacCuocHop.Visible = true;
        var getData = from ch in db.tbQuanTri_CuocHops
                      join chct in db.tbQuanTri_CuocHop_ChiTiets on ch.cuochop_id equals chct.cuochop_id
                      //join bp in db.tbBoPhans on ch.bophan_id equals bp.bophan_id
                      //where bp.bophan_id == _idBoPhan
                      join cm in db.tbQuanTri_ToChuyenMons on ch.bophan_id equals cm.tocm_id
                      where cm.tocm_id == bophan_id
                      && chct.cuochopchitiet_ngay.Value.Date >= tungay.Date
                      && chct.cuochopchitiet_ngay.Value.Date <= denngay.Date
                      && ch.hidden == false
                      && ch.cuochop_status == "Đã xem"
                      orderby ch.cuochop_createdate descending
                      select new
                      {
                          ch.cuochop_id,
                          ch.cuochop_content,
                          ngay = Convert.ToDateTime(chct.cuochopchitiet_ngay).ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                          chct.cuochopchitiet_gio,
                          ch.cuochop_status,
                          ch.cuochop_format,
                          ch.cuochop_trangthai,
                          baocao = db.tbQuanTri_BaoCaoDinhKies.Any(x => x.cuochop_id == ch.cuochop_id) == true ? "Xem báo cáo" : "Chưa có báo cáo"
                      };
        rpChiTietCuocHop.DataSource = getData;
        rpChiTietCuocHop.DataBind();
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "text", "myBaoCao()", true);
    }

    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        int bophan_id = Convert.ToInt32(txtBoPhan.Value);
        ShowChiTietCuocHop(bophan_id);
        //rpImage.DataSource = null;
        //rpImage.DataBind();
    }

    protected void btnChon_Click(object sender, EventArgs e)
    {
        if (dteTuNgay.Value == "" || dteDenNgay.Value == "")
        {
            alert.alert_Warning(Page, "Nhập đầy đủ ngày!", "");
        }
        else if (Convert.ToDateTime(dteTuNgay.Value) >= Convert.ToDateTime(dteDenNgay.Value))
        {
            alert.alert_Error(Page, "Lỗi ngày!", "");
            Reset();
        }
        else
        {
            tungay = Convert.ToDateTime(dteTuNgay.Value);
            denngay = Convert.ToDateTime(dteDenNgay.Value);
            loadData();
            //alert.alert_Success(Page, "Chọn ngày thành công!", "");
        }
    }

    //public void LoadImgBaoCao()
    //{
    //    var findData = from ch in db.tbQuanTri_CuocHops
    //                   join bcdk in db.tbQuanTri_BaoCaoDinhKies on ch.cuochop_id equals bcdk.cuochop_id
    //                   join img in db.tbQuanTri_Images_BaoCaos on bcdk.baocaodinhky_id equals img.baocaodinhky_id
    //                   where ch.cuochop_id == _idCuocHop
    //                   select new
    //                   {
    //                       img.imgbaocao_path,
    //                   };
    //    rpImage.DataSource = findData;
    //    rpImage.DataBind();
    //}


    protected void btnShowImgBaoCao_ServerClick(object sender, EventArgs e)
    {
        _idCuocHop = Convert.ToInt32(txtCuocHop.Value);
        var checkCuocHop = (from ch in db.tbQuanTri_BaoCaoDinhKies
                            where ch.cuochop_id == _idCuocHop
                            select ch).FirstOrDefault();
        if (checkCuocHop == null)
        {
            alert.alert_Error(Page, "Chưa có báo cáo!", "");
        }
        else
        {
            //load nội  dung báo  cáo
            var findData = (from ch in db.tbQuanTri_CuocHops
                            join bcdk in db.tbQuanTri_BaoCaoDinhKies on ch.cuochop_id equals bcdk.cuochop_id
                            join img in db.tbQuanTri_Images_BaoCaos on bcdk.baocaodinhky_id equals img.baocaodinhky_id
                            where ch.cuochop_id == _idCuocHop
                            select img).Single();
            string embed = "<object data=\"{0}\" type=\"application/pdf\" width=\"100%\" height=\"700px\">";
            embed += "If you are unable to view file, you can download from <a href = \".{0}\">here</a>";
            embed += " or download <a target = \"_blank\" href = \"http://get.adobe.com/reader/\">Adobe PDF Reader</a> to view the file.";
            embed += "</object>";
            ltEmbed.Text = string.Format(embed, ResolveUrl(findData.imgbaocao_path));
            div_guinhanxetbaocao.Visible = true;
        }
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "text", "myBaoCao()", true);
    }
    private bool SendMail(string email)
    {

        if (email != "")
        {
            try
            {
                var fromAddress = "thongbaovietnhatschool@gmail.com";//  Email Address from where you send the mail 
                var toAddress = email;
                const string fromPassword = "neiabcekdjluofid";
                string subject, title;
                title = "Thông báo";
                subject = "<!DOCTYPE html><html><head><title></title></head><body ><div>" +
                "<h3 style=\"margin-top:0px; text-align:center; color:#029ada\">Bạn có nhận xét báo cáo cuộc họp từ Ban Giám Hiệu. Chi tiết xem <a href='http://quantridemo.vietnhatschool.edu.vn/admin-dang-ky-cuoc-hop'>tại đây.</a></h3>" +
                "</div></body></html>";
                var smtp = new SmtpClient();
                {
                    smtp.Host = "smtp.gmail.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    smtp.Credentials = new NetworkCredential(fromAddress, fromPassword);
                    smtp.Timeout = 20000;
                }
                MailMessage mm = new MailMessage();
                mm.From = new MailAddress(fromAddress, "Trường Liên cấp Việt Nhật");
                mm.Subject = title;
                mm.To.Add(toAddress);
                mm.IsBodyHtml = true;
                mm.Body = subject;
                smtp.Send(mm);
                return true;
            }
            catch
            {
                return false;
            }
        }
        else
            return false;
    }
    protected void btnGuiNhanXet_ServerClick(object sender, EventArgs e)
    {
        if (txtNhanXet.Value.Trim() == "")
        {
            alert.alert_Warning(Page, "Vui lòng nhập nội dung nhận xét", "");
        }
        else
        {
            var getEmail = (from u in db.admin_Users
                            join ch in db.tbQuanTri_CuocHops on u.username_id equals ch.username_id
                            where ch.cuochop_id == Convert.ToInt32(txtCuocHop.Value)
                            select u).FirstOrDefault().username_email;
            tbQuanTri_NhanXetBaoCaoHangTuan nx = new tbQuanTri_NhanXetBaoCaoHangTuan();
            nx.cuochop_id = Convert.ToInt32(txtCuocHop.Value);
            nx.username_id = _idUser;
            nx.nhanxet_noidung = txtNhanXet.Value;
            nx.nhanxet_ngay = DateTime.Now;
            db.tbQuanTri_NhanXetBaoCaoHangTuans.InsertOnSubmit(nx);
            db.SubmitChanges();
            SendMail(getEmail);
            alert.alert_Success(Page, "Đã gửi nhận xét thành công!", "");
            txtNhanXet.Value = "";
        }

    }
}