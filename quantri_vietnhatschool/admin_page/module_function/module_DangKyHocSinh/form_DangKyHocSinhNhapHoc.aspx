﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="form_DangKyHocSinhNhapHoc.aspx.cs" Inherits="admin_page_module_function_module_DangKyHocSinh_form_DangKyHocSinhNhapHoc" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Trang quản trị Việt Nhật - Đăng ký dự tuyển</title>
    <link href="../../../css/bootstrap.min.css" rel="stylesheet" />
    <link href="../../../css/register.css" rel="stylesheet" />
    <script src="/admin_js/sweetalert.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container-fluid">
            <div class="button_comeback">
                <a href="/admin-danh-sach-dang-ky-hoc-sinh" class="btn btn-primary btn_comeback">Quay lại</a>
            </div>
            <div class="all_war">
                <div class="background">
                    <div class="container">
                        <div class="noidung">
                            <div class="name mt-4">PHIẾU ĐĂNG KÝ DỰ TUYỂN LỚP 1</div>
                            <div class="name_male">
                                <div class="row mt-5">
                                    <div class="col-sm-6 col-lg-6">
                                        <div class="box">
                                            <label>Họ và tên học sinh: </label>
                                            <input class="frmInput" id="txtHocSinh" runat="server" />
                                        </div>

                                    </div>
                                    <div class="col-sm-6 col-lg-6">
                                        Giới tính:
                                        <input type="radio" id="rdNam" runat="server" name="gender" value="male" />
                                        <label for="male" class="mr-4">Nam</label>
                                        <input type="radio" id="rdNu" runat="server" name="gender" value="female" />
                                        <label for="female">Nữ</label>
                                    </div>
                                </div>
                            </div>
                            <div class="dayb_born">
                                <div class="row">
                                    <div class="col-sm-6 col-lg-6">
                                        <div class="box mt-4">
                                            Ngày sinh:   
                                            <input id="txtNgaySinh" class="frmInput" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-lg-6">
                                        <div class="box mt-4">
                                            Nơi sinh: 
                                            <input class="frmInput" id="txtNoiSinh" runat="server" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div>
                                <div class="box mt-4">
                                    Đã học Mầm non tại trường: 
                                    <input class="frmInput" id="txtDaHocMamNon" runat="server" />
                                </div>
                            </div>
                            <div>
                                <div class="box mt-4">
                                    Địa chỉ thường trú: 
                                    <input class="frmInput" id="txtDiaChiThuongTru" runat="server" />
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 col-lg-6">
                                        <div class="box mt-4">
                                            Họ và tên ba:
                                            <input class="frmInput" id="txtHoTenBa" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-lg-6">
                                        <div class="box mt-4">
                                            Số điện thoại
                                            <input class="frmInput" id="txtDienThoaiBa" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 col-lg-6">
                                        <div class="box mt-4">
                                            Email:
                                            <input class="frmInput" id="txtEmaiBa" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-lg-6">
                                        <div class="box mt-4">
                                            Nghề nghiệp
                                            <input class="frmInput" id="txtNgheNghiepBa" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 col-lg-6">
                                        <div class="box mt-4">
                                            Họ và tên mẹ
                                            <input class="frmInput" id="txtHotenMe" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-lg-6">
                                        <div class="box mt-4">
                                            Số điện thoại
                                            <input class="frmInput" id="txtDienThoaiMe" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 col-lg-6">
                                        <div class="box mt-4">
                                            Email:
                                            <input class="frmInput" id="txtEmailMe" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-lg-6">
                                        <div class="box mt-4">
                                            Nghề nghiệp
                                            <input class="frmInput" id="txtngheNghiepMe" runat="server" />
                                        </div>
                                    </div>
                                </div>
                                <div class="sdt_sendtn">
                                    <div class="row mt-4">
                                        <div class="box">
                                            Số điện thoại đăng ký sẽ nhận tin nhắn của trường:
                                        </div>
                                        <input class="frmInput" id="txtSoDienThoaiDangKyNhanTinNhanCuaTruong" runat="server" />
                                    </div>
                                </div>
                                <div class="name mt-4">THÔNG TIN KẾ TOÁN CẦN</div>
                                <div class="row mt-4">
                                    <div class="box mt-4 col-12">
                                        <div class="box">
                                            1) Cân nặng của bé
                                        </div>
                                        <input class="frmInput" id="txtCanNangBe" runat="server" />
                                    </div>
                                </div>
                                <div>
                                    <div class="row mt-4">
                                        -	Số lượng đồng phục đi học đăng ký 
                                        <input class="frmInput" id="txtSoLuongDongPhucDiHoc" runat="server" />
                                    </div>
                                </div>
                                <div>
                                    <div class="row mt-4">
                                        -	Số lượng đồng phục thể dục đăng ký 
                                        <input class="frmInput" id="txtSoLuongDongPhucTheDuc" runat="server" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="mt-4 col-sm-6 col-lg-6">
                                        2) Bé có đi xe đưa đón của nhà trường không ?
                                    </div>
                                    <div class="mt-4 col-sm-6 col-lg-6">
                                        <input class="time" type="radio" id="rdXeDuaDon" runat="server" name="xeduadon" value="Có" />
                                        <label for="rdXeDuaDon" class="mr-4">Có</label>
                                        <input type="radio" id="rdXeDuaDonKhong" runat="server" name="xeduadon" value="Không" />
                                        <label for="rdXeDuaDonKhong">không</label>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <div class="row mt-4">
                                    Địa chỉ đưa đón
                                    <input class="frmInput" id="txtDiaChiDuaDon" runat="server" />
                                </div>
                            </div>

                            <div class="row mt-4">
                                <div class="col-sm-6 col-lg-6">
                                    3) Bé có tham gia Bảo hiểm y tế không?
                                </div>
                                <div class="col-sm-6 col-lg-6">
                                    <input type="radio" id="rdBHYT" runat="server" name="BHYT" value="Có" />
                                    <label for="rdBHYT" class="mr-4">Có</label>
                                    <input type="radio" id="rdBHYTKhong" runat="server" name="BHYT" value="Không" />
                                    <label for="rdBHYTKhong">không</label>
                                </div>
                            </div>
                            <div>
                                <div class="row mt-4">
                                    Nếu không : Nêu lí do không tham gia
                                    <input class="frmInput" id="txtLyDoKhongThamGiaBHYT" runat="server" />
                                </div>
                            </div>
                            <div>
                                <div class="row mt-4">
                                    Nếu có: Bệnh viện đăng ký khám chữa bệnh
                                    <input class="frmInput" id="txtBenhVienDangKyThamGiaBHYT" runat="server" />
                                </div>
                            </div>
                            <div>
                                <div class="row mt-4">
                                    <div class="col-sm-6 col-lg-6">
                                        4)  Bé có tham gia Bảo hiểm tai nạn không? (Không bắt buộc)
                                    </div>
                                    <div class="col-sm-6 col-lg-6">
                                        <input type="radio" id="rdBHTN" runat="server" name="BHTN" value="Có" />
                                        <label for="rdBHTN" class="mr-4">Có</label>
                                        <input type="radio" id="rdBHTNKhong" runat="server" name="BHTN" value="Không" />
                                        <label for="rdBHTNKhong">không</label>
                                    </div>
                                </div>
                                <div>
                                    <div class="row mt-4">
                                        <div class="col-sm-6 col-lg-6">
                                            5)  Bé có anh (chị, em) ruột học tại trường Việt Nhật không? 
                                        </div>
                                        <div class="col-sm-6 col-lg-6">
                                            <input type="radio" id="rdAnhChiRuot" runat="server" name="anhchiruot" value="Có" />
                                            <label for="rdAnhChiRuot" class="mr-4">Có</label>
                                            <input type="radio" id="rdAnhChiRuotKhong" runat="server" name="anhchiruot" value="Không" />
                                            <label for="rdAnhChiRuotKhong">không</label>
                                        </div>
                                    </div>
                                    <div>
                                        + Họ và tên của anh (chị, em) ruột:
                                        <input class="frmInput" id="txtHoTenCuaAnhChiRuot" runat="server" />
                                    </div>

                                    <div>
                                        Lớp:
                                        <input class="frmInput" id="txtLopCuaAnhChiRuot" runat="server" />
                                    </div>
                                    <div>
                                        + Họ và tên của anh (chị, em) ruột:
                                        <input class="frmInput" id="txtHoTenCuaAnhChiRuot2" runat="server" />
                                    </div>
                                    <div>
                                        Lớp:
                                        <input class="frmInput" id="txtLopCuaAnhChiRuot2" runat="server" />
                                    </div>
                                    <div class="row mt-4">
                                        <div class="col-sm-6 col-lg-6">
                                            6) Bé có tham gia khóa học hè “Chào lớp 1” từ ngày 20/7/2020 đến 20/8/2020 không? 
                            
                                        </div>
                                        <div class="col-sm-6 col-lg-6">
                                            <input type="radio" id="rdThamGiaKhoaHoche" runat="server" name="thamgiakhoahe" value="Có" />
                                            <label for="rdThamGiaKhoaHoche" class="mr-4">Có</label>
                                            <input type="radio" id="rdThamGiaKhoaHocheKhong" runat="server" name="thamgiakhoahe" value="Không" />
                                            <label for="rdThamGiaKhoaHocheKhong">không</label>
                                        </div>
                                    </div>
                                    <div class="button">
                                        <div class="mt-4">
                                            <asp:Button ID="btnDangKy" runat="server" OnClick="btnDangKy_Click" Text="Đăng ký" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
