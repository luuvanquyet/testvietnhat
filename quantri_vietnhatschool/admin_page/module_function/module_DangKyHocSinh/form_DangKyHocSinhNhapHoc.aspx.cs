﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_DangKyHocSinh_form_DangKyHocSinhNhapHoc : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnDangKy_Click(object sender, EventArgs e)
    {
        tbDangKyKhoi1VietNhat insert = new tbDangKyKhoi1VietNhat();
        insert.hocsinh_hoten = txtHocSinh.Value;
        if (rdNam.Checked == true)
            insert.hocsinh_gioitinh = true;
        else
            insert.hocsinh_gioitinh = false;
        insert.hocsinh_ngaysinh = txtNgaySinh.Value;
        insert.hocsinh_noisinh = txtNoiSinh.Value;
        insert.hocsinh_dahocmamnontaitruong = txtDaHocMamNon.Value;
        insert.hocsinh_diachithuongtru = txtDiaChiThuongTru.Value;
        insert.hocsinh_hotenba = txtHoTenBa.Value;
        insert.hocsinh_sodienthoaiba = txtDienThoaiBa.Value;
        insert.hocsinh_emailba = txtEmaiBa.Value;
        insert.hocsinh_nghenghiepba = txtNgheNghiepBa.Value;
        insert.hocsinh_hotenme = txtHotenMe.Value;
        insert.hocsinh_sodienthoaime = txtDienThoaiMe.Value;
        insert.hocsinh_emailme = txtEmailMe.Value;
        insert.hocsinh_nghenghiepme = txtngheNghiepMe.Value;
        insert.hocsinh_sodienthoaidangkysenhantinnhancuatruong = txtSoDienThoaiDangKyNhanTinNhanCuaTruong.Value;
        insert.hocsinh_cannangcuabe = txtCanNangBe.Value;
        insert.hocsinh_soluongdongphucdihoc = txtSoLuongDongPhucDiHoc.Value;
        insert.hocsinh_soluongdongphuctheduc = txtSoLuongDongPhucTheDuc.Value;
        if (rdXeDuaDon.Checked == true)
            insert.hocsinh_becodixeduadoncuanhatruongkhong = true;
        else
            insert.hocsinh_becodixeduadoncuanhatruongkhong = false;
        insert.hocsinh_diachiduadon = txtDiaChiDuaDon.Value;
        if(rdBHYT.Checked==true)
        insert.hocsinh_becothamgiabaohiemytekhong = true;
        else
            insert.hocsinh_becothamgiabaohiemytekhong = false;
        insert.hocsinh_neulidokhongthamgiabaohiemyte= txtLyDoKhongThamGiaBHYT.Value;
        insert.hocsinh_benhviendangkykhamchuabenh = txtBenhVienDangKyThamGiaBHYT.Value;
        if(rdBHTN.Checked==true)
        insert.hocsinh_becothamgiabaohiemtainankhong = true;
        else
            insert.hocsinh_becothamgiabaohiemtainankhong = false;
        if(rdAnhChiRuot.Checked==true)
        insert.hocsinh_becoanhchiemruothoctaitruongvietnhatkhong =true;
        else
            insert.hocsinh_becoanhchiemruothoctaitruongvietnhatkhong = false;
        insert.hocsinh_hotenanhchiruot1 = txtHoTenCuaAnhChiRuot.Value;
        insert.hocsinh_lopanhchiruot1 = txtLopCuaAnhChiRuot.Value;
        insert.hocsinh_hotenanhchiruot2 = txtHoTenCuaAnhChiRuot2.Value;
        insert.hocsinh_lopanhchiruot2 = txtLopCuaAnhChiRuot2.Value;
        if (rdThamGiaKhoaHoche.Checked == true)
            insert.hocsinh_becothamgiakhoahochechaolop1 = true;
        else
            insert.hocsinh_becothamgiakhoahochechaolop1 = false;
        db.tbDangKyKhoi1VietNhats.InsertOnSubmit(insert);
        db.SubmitChanges();
        alert.alert_Success(Page, "Đã hoàn thành đăng ký", "");
    }
}