﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="form_ThoiKhoaBieuCuaGiaoVien.aspx.cs" Inherits="admin_page_module_function_form_ThoiKhoaBieuCuaGiaoVien" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../../css/bootstrap.min.css" rel="stylesheet" />
    <link href="css_table/table.css" rel="stylesheet" />
   

</head>
<body>
    <form id="form1" runat="server">
        <div class="background">
            <div class="container">
                <div style="overflow-x: auto;">
                    <table class="table table-bordered">
                        <thead>
                            <tr class="head_table">
                                <th class="table_header" scope="col">Thứ</th>
                                <th scope="col">Buổi</th>
                                <th scope="col">Tiết(TKB)</th>
                                <th scope="col">Lớp</th>
                                <th scope="col">Tiết(PPCT)</th>
                                <th scope="col">Tên bài dạy</th>
                                <th scope="col">Ghi chú</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <span class="table_day">Hai
								<span>05/05
                                </span>
                                    </span>
                                </td>
                                <td>chiều</td>
                                <td class="tiet">1</td>
                                <td class="tiet">2</td>
                                <td class="tiet">3</td>
                                <td>1/4</td>
                                <td>10</td>
                                <td>Đàn gà của em
                                    <td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="table_day">Ba
									<span>06/05
                                    </span>
                                    </span>
                                </td>
                                <td>chiều</td>
                                <td class="tiet">2</td>
                                <td class="tiet">2</td>
                                <td class="tiet">3</td>
                                <td>1/4</td>
                                <td>10</td>
                                <td>Đàn gà của em
                                    <td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="table_day">Tư
										<span>07/05
                                        </span>
                                    </span>
                                </td>
                                <td>chiều</td>
                                <td class="tiet">3</td>
                                <td class="tiet">4</td>
                                <td class="tiet">5</td>
                                <td>1/4</td>
                                <td>10</td>
                                <td>Đàn gà của em
                                    <td>
                            </tr>
							<tr>
                                <td>
                                    <span class="table_day">Năm
										<span>07/05
                                        </span>
                                    </span>
                                </td>
                                <td>chiều</td>
                                <td class="tiet">1</td>
                                <td class="tiet">2</td>
                                <td class="tiet">3</td>
                                <td>1/4</td>
                                <td>10</td>
                                <td>Đàn gà của em
                                    <td>
                            </tr>
							<tr>
                                <td>
                                    <span class="table_day">Sau
										<span>08/05
                                        </span>
                                    </span>
                                </td>
                                <td>chiều</td>
                                <td class="tiet">2</td>
                                <td class="tiet">2</td>
                                <td class="tiet">3</td>
                                <td>1/4</td>
                                <td>10</td>
                                <td>Đàn gà của em
                                    <td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </form>
</body>
</html>
