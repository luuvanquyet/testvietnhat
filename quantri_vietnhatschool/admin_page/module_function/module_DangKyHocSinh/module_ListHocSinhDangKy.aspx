﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_ListHocSinhDangKy.aspx.cs" Inherits="admin_page_module_function_module_DangKyHocSinh_module_ListHocSinhDangKy" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v17.1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    
    <script type="text/javascript">
        function func() {
            grvList.Refresh();
        }
       
    </script>
    <div class="card card-block">
        <div class="title__form text-md-center">
            <h3 class="text-uppercase">Danh sách đăng ký tuyển sinh</h3>
        </div>
        <div class="form-group row">
            <div class="col-sm-10">
                <asp:UpdatePanel ID="udButton" runat="server">
                    <ContentTemplate>
                        <a href="/admin-dang-ky-hoc-sinh-tuyen-sinh" class="btn btn-primary">Thêm mới</a>
                        <%--<asp:Button ID="btnThemMoi" runat="server" OnClick="btnThemMoi_Click" Text="Thêm" CssClass="btn btn-primary" />--%>
                        <%--<input type="submit" class="btn btn-primary" value="Xóa" onclick="confirmDel()" />
                        <asp:Button ID="btnXoa" runat="server" CssClass="invisible" OnClick="btnXoa_Click" />
                        <asp:Button ID="btnKetThuc" runat="server" OnClick="btnKetThuc_Click" Text="Kết thúc" CssClass="btn btn-primary" />--%>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <div class="form-group table-responsive">
            <dx:ASPxGridView ID="grvList" runat="server" ClientInstanceName="grvList" KeyFieldName="dangkyhocsinh_id" Width="100%">
                <%-- <Toolbars>
                    <dx:GridViewToolbar SettingsAdaptivity-Enabled="true">
                        <Items>
                            <dx:GridViewToolbarItem Command="ExportToXls" />
                            <dx:GridViewToolbarItem Command="ExportToXlsx" />
                            <dx:GridViewToolbarItem Command="ExportToCsv" />
                        </Items>
                    </dx:GridViewToolbar>
                </Toolbars>--%>
                <Columns>
                    <dx:GridViewCommandColumn ShowSelectCheckbox="True" SelectAllCheckboxMode="Page" VisibleIndex="0" Width="5%">
                    </dx:GridViewCommandColumn>
                    <dx:GridViewDataColumn Caption="Họ tên bé" FieldName="hocsinh_hoten" HeaderStyle-HorizontalAlign="Center" Width="20%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Giới tính" FieldName="gioitinh" HeaderStyle-HorizontalAlign="Center" Width="20%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Ngày sinh" FieldName="hocsinh_ngaysinh" HeaderStyle-HorizontalAlign="Center" Width="30%" Settings-AllowEllipsisInText="True"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Số phone ba" FieldName="hocsinh_sodienthoaiba" Width="20%" HeaderStyle-HorizontalAlign="Center" CellStyle-VerticalAlign="Middle"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Số phone mẹ" FieldName="hocsinh_sodienthoaime" Width="20%" HeaderStyle-HorizontalAlign="Center" CellStyle-VerticalAlign="Middle"></dx:GridViewDataColumn>
                </Columns>
                <SettingsBehavior AllowFocusedRow="true" />
                <SettingsText EmptyDataRow="Không có dữ liệu" SearchPanelEditorNullText="Gỏ từ cần tìm kiếm và enter..." />
                <SettingsLoadingPanel Text="Đang tải..." />
                <SettingsPager PageSize="10" Summary-Text="Trang {0} / {1} ({2} trang)"></SettingsPager>
            </dx:ASPxGridView>
        </div>
    </div>
    
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

