﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_DangKyHocSinh_module_ListHocSinhDangKy : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    cls_DangKyHocSinh cls = new cls_DangKyHocSinh();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["UserName"] != null)
        {
            if (!IsPostBack)
            {
                Session["_id"] = 0;
            }
            loadData();
        }
        else
        {
            Response.Redirect("/admin-login");
        }

    }
    private void loadData()
    {

        // load data đổ vào var danh sách
        var getData = from nc in db.tbDangKyKhoi1VietNhats
                      select new
                      {
                          nc.dangkyhocsinh_id,
                          nc.hocsinh_ngaysinh,
                          nc.hocsinh_hoten,
                          gioitinh = nc.hocsinh_gioitinh == true ? "Nam" : "Nữ",
                          nc.hocsinh_sodienthoaime,
                          nc.hocsinh_sodienthoaiba
                      };
        // đẩy dữ liệu vào gridivew
        grvList.DataSource = getData;
        grvList.DataBind();

    }

    protected void btnThemMoi_Click(object sender, EventArgs e)
    {
        Response.Redirect("/admin-dang-ky-hoc-sinh-tuyen-sinh");

    }


}