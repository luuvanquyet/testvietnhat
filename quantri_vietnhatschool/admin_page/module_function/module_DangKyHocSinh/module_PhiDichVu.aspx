﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_PhiDichVu.aspx.cs" Inherits="admin_page_module_function_module_DangKyHocSinh_module_PhiDichVu" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v17.1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <script type="text/javascript">
        function func() {
            grvList.Refresh();
        }
    </script>
    <div class="card card-block">
        <div class="form-group table-responsive">
            <dx:ASPxGridView ID="grvList" runat="server" ClientInstanceName="grvList" KeyFieldName="phidichvu_id" Width="100%">
               <%-- <Toolbars>
                    <dx:GridViewToolbar SettingsAdaptivity-Enabled="true">
                        <Items>
                            <dx:GridViewToolbarItem Command="ExportToXls" />
                            <dx:GridViewToolbarItem Command="ExportToXlsx" />
                            <dx:GridViewToolbarItem Command="ExportToCsv" />
                        </Items>
                    </dx:GridViewToolbar>
                </Toolbars>--%>
                <Columns>
                    <dx:GridViewCommandColumn ShowSelectCheckbox="True" SelectAllCheckboxMode="Page" VisibleIndex="0" Width="5%">
                    </dx:GridViewCommandColumn>
                     <dx:GridViewDataColumn Caption="mã tài khoản" FieldName="hocsinh_code" HeaderStyle-HorizontalAlign="Center" Width="15%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Họ tên" FieldName="hocsinh_name" HeaderStyle-HorizontalAlign="Center" Width="15%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="ĐP đi học" FieldName="dongphucdihoc" HeaderStyle-HorizontalAlign="Center" Width="15%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="ĐP thể dục" FieldName="dongphuctheduc" HeaderStyle-HorizontalAlign="Center" Width="15%" Settings-AllowEllipsisInText="True"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Nơi ĐK BHYT" FieldName="phidichvu_bhyt_noidangky" Width="20%" HeaderStyle-HorizontalAlign="Center" CellStyle-VerticalAlign="Middle"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="BHTN" FieldName="phidichvu_bhtn" Width="10%" HeaderStyle-HorizontalAlign="Center" CellStyle-VerticalAlign="Middle"></dx:GridViewDataColumn>
                     <dx:GridViewDataColumn Caption="Ăn sáng tại trường" FieldName="phidichvu_ansangtaitruong" Width="20%" HeaderStyle-HorizontalAlign="Center" CellStyle-VerticalAlign="Middle"></dx:GridViewDataColumn>
                     <dx:GridViewDataColumn Caption="Xe đưa đón" FieldName="phidichvu_xeduadon" Width="20%" HeaderStyle-HorizontalAlign="Center" CellStyle-VerticalAlign="Middle"></dx:GridViewDataColumn>
                     <dx:GridViewDataColumn Caption="Ghi chú" FieldName="phidichvu_ghichu" Width="20%" HeaderStyle-HorizontalAlign="Center" CellStyle-VerticalAlign="Middle"></dx:GridViewDataColumn>
                     <dx:GridViewDataColumn Caption="Email ba" FieldName="hocsinh_eamilba" Width="20%" HeaderStyle-HorizontalAlign="Center" CellStyle-VerticalAlign="Middle"></dx:GridViewDataColumn>
                     <dx:GridViewDataColumn Caption="Email mẹ" FieldName="hocsinh_eamilme" Width="20%" HeaderStyle-HorizontalAlign="Center" CellStyle-VerticalAlign="Middle"></dx:GridViewDataColumn>
                </Columns>
                <SettingsBehavior AllowFocusedRow="true" />
                <SettingsText EmptyDataRow="Không có dữ liệu" SearchPanelEditorNullText="Gỏ từ cần tìm kiếm và enter..." />
                <SettingsLoadingPanel Text="Đang tải..." />
                <SettingsPager PageSize="10" Summary-Text="Trang {0} / {1} ({2} trang)"></SettingsPager>
            </dx:ASPxGridView>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

