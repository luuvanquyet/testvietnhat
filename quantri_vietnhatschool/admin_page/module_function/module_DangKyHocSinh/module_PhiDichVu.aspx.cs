﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_DangKyHocSinh_module_PhiDichVu : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    protected void Page_Load(object sender, EventArgs e)
    {
        loadData();
    }
    private void loadData()
    {
        if (Request.Cookies["UserName"] != null)
        {
            var checkuserid = (from u in db.admin_Users where u.username_username == Request.Cookies["UserName"].Value select u).First();
            // load data đổ vào var danh sách
            var getData = from nc in db.tbDangKyPhiDichVus
                          join hstl in db.tbHocSinhTrongLops on nc.hstl_id equals hstl.hstl_id
                          join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                          join l in db.tbLops on hstl.lop_id equals l.lop_id
                          join gvtl in db.tbGiaoVienTrongLops on l.lop_id equals gvtl.lop_id
                          where gvtl.taikhoan_id == checkuserid.username_id
                          orderby hstl.position
                          select new
                          {
                              hs.hocsinh_code,
                              nc.phidichvu_id,
                              hs.hocsinh_name,
                              dongphucdihoc = nc.phidichvu_dongphucdihoc_ao+" áo," + nc.phidichvu_dongphucdihoc_quandai+" quàn dài," +nc.phidichvu_dongphucdihoc_quanngan+" quần ngắn," + nc.phidichvu_dongphucdihoc_vay+ " váy",
                              dongphuctheduc = nc.phidichvu_dongphuctheduc_soluong + nc.phidichvu_dongphuctheduc_loai,
                              nc.phidichvu_bhyt_noidangky,
                              nc.phidichvu_bhtn,
                              nc.phidichvu_ansangtaitruong,
                              nc.phidichvu_xeduadon,
                              nc.phidichvu_ghichu,
                              hs.hocsinh_eamilba,
                              hs.hocsinh_eamilme,
                          };
            // đẩy dữ liệu vào gridivew
            grvList.DataSource = getData;
            grvList.DataBind();
        }

    }


}