﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_DangKyTuyenSinh_module_CheDoUuTien : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    cls_CheDoUuTien cls = new cls_CheDoUuTien();
    private int _id;
    private static int _idUser;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["UserName"] != null)
        {
            if (!IsPostBack)
            {
                Session["_id"] = 0;
            }
            loadData();
            _idUser = (from u in db.admin_Users
                       where u.username_username == Request.Cookies["UserName"].Value
                       select u).FirstOrDefault().username_id;
        }
        else
        {
            Response.Redirect("/admin-login");
        }
    }

    private void loadData()
    {

        // load data đổ vào var danh sách
        var getData = from nc in db.tbDangky_CheDoUuTiens
                      orderby nc.chedo_id descending
                      select new
                      {
                          nc.chedo_id,
                          nc.chedo_name,
                          nc.chedo_tomtat,

                      };
        // đẩy dữ liệu vào gridivew
        grvList.DataSource = getData;
        grvList.DataBind();

    }
    private void setNULL()
    {
        txtTenCheDo.Text = "";
        txtMota.Value = "";
    }
    protected void btnThemMoi_Click(object sender, EventArgs e)
    {
        Session["_id"] = 0;
        setNULL();
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupControl.Show();", true);
    }

    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "chedo_id" }));
        Session["_id"] = _id;
        var getData = (from n in db.tbDangky_CheDoUuTiens
                       where n.chedo_id == _id
                       select new
                       {
                           n.chedo_id,
                           n.chedo_name,
                           n.chedo_tomtat,
                       }).Single();
        txtTenCheDo.Text = getData.chedo_name;
        txtMota.Value = getData.chedo_tomtat;
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupControl.Show();", true);
    }

    protected void btnLuu_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["_id"].ToString() == "0")
            {
                if (cls.ThemMoi(txtTenCheDo.Text, txtMota.Value))
                    alert.alert_Success(Page, "Lưu thành công!", "");
                else
                    alert.alert_Error(Page, "Lưu thất bại!", "");
            }
            else
            {
                if (cls.CapNhat(Convert.ToInt32(Session["_id"].ToString()), txtTenCheDo.Text, txtMota.Value))
                    ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Lưu thành công!','','success').then(function(){grvList.Refresh();grvList.UnselectRows()})", true);
                else
                    alert.alert_Error(Page, "Lưu thất bại!", "");
            }
            popupControl.ShowOnPageLoad = false;
            loadData();
        }
        catch (Exception)
        {
            alert.alert_Error(Page, "Đã có lỗi xảy ra!", "Vui lòng liên hệ IT");
        }
    }
    protected void btnXoa_Click(object sender, EventArgs e)
    {
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "chedo_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                tbDangky_CheDoUuTien checkCheDo = (from dm in db.tbDangky_CheDoUuTiens where dm.chedo_id == Convert.ToInt32(item) select dm).SingleOrDefault();
                db.tbDangky_CheDoUuTiens.DeleteOnSubmit(checkCheDo);
                db.SubmitChanges();
            }
            ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Xóa thành công!','','success').then(function(){grvList.UnselectRows()})", true);
        }
        else
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
        loadData();
    }
}