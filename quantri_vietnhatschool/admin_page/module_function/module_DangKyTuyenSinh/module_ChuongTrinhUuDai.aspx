﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_ChuongTrinhUuDai.aspx.cs" Inherits="admin_page_module_function_module_DangKyTuyenSinh_module_ChuongTrinhUuDai" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <style>
        span.field_important {
            color: red;
            font-size: 20px;
        }
    </style>
    <script>
        function func() {
            grvList.Refresh();

        }
        function checkNULL() {
            var name = document.getElementById('<%= txtThoiHan.ClientID%>');
            var noiDung = document.getElementById('<%= txtNoiDung.ClientID%>');
            var tuNgay = document.getElementById('<%= dteTuNgay.ClientID%>');
            var denNgay = document.getElementById('<%= dteDenNgay.ClientID%>');
            if (name.value.trim() == "") {
                swal('Tên ưu đãi không được để trống!', '', 'warning').then(function () { name.focus(); });
                return false;
            }
            if (noiDung.value.trim() == "") {
                swal('Nội dung ưu đãi không được để trống!', '', 'warning').then(function () { noiDung.focus(); });
                return false;
            }
            if (tuNgay.value.trim() == "") {
                swal('Vui lòng chọn thời gian bắt đầu ưu đãi!', '', 'warning').then(function () { tuNgay.focus(); });
                return false;
            }
            if (denNgay.value.trim() == "") {
                swal('Vui lòng chọn thời kết thúc đầu ưu đãi!', '', 'warning').then(function () { denNgay.focus(); });
                return false;
            }
            return true;
        }
        function confirmDel() {
            swal("Bạn có thực sự muốn xóa?",
                "Nếu xóa, dữ liệu sẽ không thể khôi phục.",
                "warning",
                {
                    buttons: true,
                    dangerMode: true
                }).then(function (value) {
                    if (value == true) {
                        var xoa = document.getElementById('<%=btnXoa.ClientID%>');
                        xoa.click();
                    }
                });
        }
    </script>
    <div class="card card-block">
        <div class="title__form text-md-center">
            <h3 class="text-uppercase">Danh sách chương trình ưu đãi</h3>
        </div>
        <div class="form-group row">
            <div class="col-sm-10">
                <asp:UpdatePanel ID="udButton" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnThemMoi" runat="server" OnClick="btnThemMoi_Click" Text="Thêm" CssClass="btn btn-primary" />
                        <asp:Button ID="btnChiTiet" runat="server" OnClick="btnChiTiet_Click" Text="Chi tiết" CssClass="btn btn-primary" />
                        <input type="submit" class="btn btn-primary" value="Xóa" onclick="confirmDel()" />
                        <asp:Button ID="btnXoa" runat="server" CssClass="invisible" OnClick="btnXoa_Click" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <div class="form-group table-responsive">
            <dx:ASPxGridView ID="grvList" runat="server" ClientInstanceName="grvList" KeyFieldName="ctud_id" Width="100%">
                <Columns>
                    <dx:GridViewCommandColumn ShowSelectCheckbox="True" SelectAllCheckboxMode="Page" VisibleIndex="0" Width="5%">
                    </dx:GridViewCommandColumn>
                    <dx:GridViewDataColumn Caption="Thời hạn ưu đãi" FieldName="ctud_thoigian" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Nội dung ưu đãi" FieldName="ctud_noidung" HeaderStyle-HorizontalAlign="Center" Width="50%" Settings-AllowEllipsisInText="true"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Thời gian áp dụng"  HeaderStyle-HorizontalAlign="Center" Width="20%">
                        <DataItemTemplate>
                            Từ <%#Eval("ctud_thoigianbatdau","{0:dd/MM/yyyy}") %> đến <%#Eval("ctud_thoigianketthuc","{0:dd/MM/yyyy}") %>
                        </DataItemTemplate>
                    </dx:GridViewDataColumn>
                </Columns>
                <SettingsSearchPanel Visible="true" />
                <SettingsBehavior AllowFocusedRow="true" />
                <SettingsText EmptyDataRow="Trống" SearchPanelEditorNullText="Gỏ từ cần tìm kiếm và enter..." />
                <SettingsLoadingPanel Text="Đang tải..." />
                <SettingsPager PageSize="20" Summary-Text="Trang {0} / {1} ({2} trang)"></SettingsPager>
            </dx:ASPxGridView>
        </div>
    </div>
    <dx:ASPxPopupControl ID="popupControl" runat="server" Width="800px" Height="400px" CloseAction="CloseButton" ShowCollapseButton="True" ShowMaximizeButton="True" ScrollBars="Auto" CloseOnEscape="true" Modal="True"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" ClientInstanceName="popupControl" ShowFooter="true"
        HeaderText="THÔNG TIN ƯU ĐÃI" AllowDragging="True" PopupAnimationType="Fade" EnableViewState="False" AutoUpdatePosition="true" ClientSideEvents-CloseUp="function(s,e){grvList.Refresh();}">
        <ContentCollection>
            <dx:PopupControlContentControl runat="server">
                <asp:UpdatePanel ID="udPopup" runat="server">
                    <ContentTemplate>
                        <div class="popup-main">
                            <div class="div_content row ml-0 mt-0">
                                <div class="col-12">
                                    <div class="col-12 form-group">
                                        <label class="col-2 form-control-label">Tên ưu đãi <span class="field_important">*</span>:</label>
                                        <div class="col-10">
                                            <asp:TextBox ID="txtThoiHan" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%"> </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-12 form-group">
                                        <label class="col-2 form-control-label">Nội dung ưu đãi<span class="field_important">*</span>:</label>
                                        <div class="col-10">
                                            <textarea class="form-control" rows="5" style="width: 95%" id="txtNoiDung" runat="server"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-12 form-group">
                                        <label class="col-2 form-control-label">Thời gian áp dụng:</label>
                                        <div class="col-10">
                                            Từ ngày:
                                            <input type="date" value="" id="dteTuNgay" runat="server" />
                                            đến ngày:
                                            <input type="date" value="" id="dteDenNgay" runat="server" />
                                        </div>
                                    </div>

                                    <div class="col-12 form-group" hidden>
                                        <label class="col-2 form-control-label">Năm học áp dụng:</label>
                                        <div class="col-10">
                                            <asp:DropDownList runat="server" ID="ddlNamHoc" CssClass="form-control" Width="95%">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <FooterContentTemplate>
            <div class="mar_but button">
                <asp:Button ID="btnLuu" runat="server" ClientIDMode="Static" Text="Lưu" CssClass="btn btn-primary" OnClientClick="return checkNULL()" OnClick="btnLuu_Click" />
            </div>
        </FooterContentTemplate>
        <ContentStyle>
            <Paddings PaddingBottom="0px" />
        </ContentStyle>
    </dx:ASPxPopupControl>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

