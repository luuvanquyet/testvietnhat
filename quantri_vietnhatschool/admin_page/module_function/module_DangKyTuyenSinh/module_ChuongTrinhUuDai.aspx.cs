﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_DangKyTuyenSinh_module_ChuongTrinhUuDai : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    cls_ChuongTrinhUuDai cls = new cls_ChuongTrinhUuDai();
    private int _id;
    private static int _idUser;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["UserName"] != null)
        {
            if (!IsPostBack)
            {
                Session["_id"] = 0;
            }
            loadData();
            _idUser = (from u in db.admin_Users
                       where u.username_username == Request.Cookies["UserName"].Value
                       select u).FirstOrDefault().username_id;
        }
        else
        {
            Response.Redirect("/admin-login");
        }
    }

    private void loadData()
    {

        // load data đổ vào var danh sách
        var getData = from nc in db.tbWebsite_ChuongTrinhUuDais
                      where nc.ctud_status == 0
                      orderby nc.ctud_id descending
                      select new
                      {
                          nc.ctud_id,
                          nc.ctud_thoigian,
                          nc.ctud_noidung,
                          nc.namhoc_apdung,
                          nc.ctud_thoigianbatdau,
                          nc.ctud_thoigianketthuc,
                      };
        // đẩy dữ liệu vào gridivew
        grvList.DataSource = getData;
        grvList.DataBind();
        //get năm học
        var getNamHoc = from n in db.tbHoctap_NamHocs
                        where n.namhoc_id > 2
                        select new
                        {
                            n.namhoc_id,
                            namhoc = n.namhoc_hocky + " - Năm học " + n.namhoc_nienkhoa,
                        };
        ddlNamHoc.DataValueField = "namhoc";
        ddlNamHoc.DataTextField = "namhoc";
        ddlNamHoc.DataSource = getNamHoc;
        ddlNamHoc.DataBind();

    }
    private void setNULL()
    {
        txtThoiHan.Text = "";
        txtNoiDung.Value = "";
        dteTuNgay.Value = "";
        dteDenNgay.Value = "";
    }
    protected void btnThemMoi_Click(object sender, EventArgs e)
    {
        Session["_id"] = 0;
        setNULL();
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupControl.Show();", true);
    }

    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "ctud_id" }));
        Session["_id"] = _id;
        var getData = (from n in db.tbWebsite_ChuongTrinhUuDais
                       where n.ctud_id == _id
                       select new
                       {
                           n.ctud_id,
                           n.ctud_thoigian,
                           n.ctud_noidung,
                           n.namhoc_apdung,
                           n.ctud_thoigianbatdau,
                           n.ctud_thoigianketthuc,
                       }).Single();
        txtThoiHan.Text = getData.ctud_thoigian;
        txtNoiDung.Value = getData.ctud_noidung;
        dteTuNgay.Value = getData.ctud_thoigianbatdau.Value.ToString("yyyy-MM-dd").Replace(' ', 'T');
        dteDenNgay.Value = getData.ctud_thoigianketthuc.Value.ToString("yyyy-MM-dd").Replace(' ', 'T');

        ddlNamHoc.SelectedValue = getData.namhoc_apdung;
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupControl.Show();", true);
    }

    protected void btnLuu_Click(object sender, EventArgs e)
    {
        try
        {
            if (Convert.ToDateTime(dteTuNgay.Value) >= Convert.ToDateTime(dteDenNgay.Value))
            {
                alert.alert_Warning(Page, "Ngày kết thúc không được nhỏ hơn ngày bắt đầu", "");
            }
            else
            {
                if (Session["_id"].ToString() == "0")
                {
                    if (cls.ThemMoi(txtThoiHan.Text, txtNoiDung.Value, Convert.ToDateTime(dteTuNgay.Value), Convert.ToDateTime(dteDenNgay.Value)))
                        alert.alert_Success(Page, "Lưu thành công!", "");
                    else
                        alert.alert_Error(Page, "Lưu thất bại!", "");
                }
                else
                {
                    if (cls.CapNhat(Convert.ToInt32(Session["_id"].ToString()), txtThoiHan.Text, txtNoiDung.Value, Convert.ToDateTime(dteTuNgay.Value), Convert.ToDateTime(dteDenNgay.Value)))
                        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Lưu thành công!','','success').then(function(){grvList.UnselectRows()})", true);
                    else
                        alert.alert_Error(Page, "Lưu thất bại!", "");
                }
                popupControl.ShowOnPageLoad = false;
                loadData();
            }
        }
        catch (Exception)
        {
            alert.alert_Error(Page, "Đã có lỗi xảy ra!", "Vui lòng liên hệ IT");
        }
    }

    protected void btnXoa_Click(object sender, EventArgs e)
    {
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "ctud_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                tbWebsite_ChuongTrinhUuDai checkUuDai = (from dm in db.tbWebsite_ChuongTrinhUuDais where dm.ctud_id == Convert.ToInt32(item) select dm).SingleOrDefault();
                checkUuDai.ctud_status = -1;
                db.SubmitChanges();
            }
            ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Xóa thành công!','','success').then(function(){grvList.UnselectRows()})", true);
        }
        else
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
        loadData();
    }
}