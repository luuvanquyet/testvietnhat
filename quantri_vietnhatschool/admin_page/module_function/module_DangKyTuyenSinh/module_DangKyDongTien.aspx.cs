﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_DangKyTuyenSinh_module_DangKyDongTien : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    cls_DangKyDongTien cls = new cls_DangKyDongTien();
    cls_DangKyHocSinh cls_dangki = new cls_DangKyHocSinh();
    private static int _idUser;
    private int _id;
    private string noidung;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["UserName"] != null)
        {
            if (!IsPostBack)
            {
                Session["_id"] = 0;
                Session["_idHocSinh"] = 0;
            }
            loadData();
            _idUser = (from u in db.admin_Users
                       where u.username_username == Request.Cookies["UserName"].Value
                       select u).FirstOrDefault().username_id;
        }
        else
        {
            Response.Redirect("/admin-login");
        }
    }
    private void loadData()
    {

        // load data đổ vào var danh sách
        var getData = from nc in db.tbWebsite_DangKiTuyenSinhs
                      where nc.hocsinh_tinhtrang == 5//khảo sát đã thành công chờ đóng tiền
                      //&& nc.hocsinh_dongtien == false // chưa đăng ký đóng tiền
                      orderby nc.chedo_id descending
                      select new
                      {
                          nc.hocsinh_id,
                          nc.hocsinh_name,
                          hocsinh_namsinh = Convert.ToDateTime(nc.hocsinh_dateofbirth).Year,
                          hocsinh_tuoi = DateTime.Now.Year - Convert.ToDateTime(nc.hocsinh_dateofbirth).Year + " tuổi",
                          nc.hocsinh_phuhuynh_name,
                          nc.hocsinh_tenme,
                          nc.hocsinh_address,
                          nc.hocsinh_phone,
                          nc.hocsinh_noidangky,
                          nc.hocsinh_dangki_class,
                          nc.hocsinh_sdtme,
                          nc.hocsinh_ngaykhaosat,
                          username_fullname = (from u in db.admin_Users
                                               where u.username_id == nc.username_id
                                               select u).FirstOrDefault().username_fullname,
                          trangthai = nc.hocsinh_trangthai_dangky == 0 ? "Chưa TV" : nc.hocsinh_trangthai_dangky == 1 && nc.hocsinh_danhsachcho_khaosat == true ? "Chờ KS" : nc.hocsinh_trangthai_dangky == 1 ? "Đã TV" : "",
                          cosodangky = nc.hocsinh_cosodangky == 1 ? "CS1" : nc.hocsinh_cosodangky == 2 ? "CS2" : nc.hocsinh_cosodangky == 3 ? "CS3" : nc.hocsinh_cosodangky == 4 ? "TH-THCS-THPT" : "",
                          count = (from dk in db.tbWebsite_DangKiTuyenSinhs
                                   join tv in db.tbWebsite_TuVan_DangKiTuyenSinhs on dk.hocsinh_id equals tv.hocsinh_dangky_id
                                   where tv.hocsinh_dangky_id == nc.hocsinh_id
                                   select tv).Any() ? "" + db.tbWebsite_TuVan_DangKiTuyenSinhs.Where(x => x.hocsinh_dangky_id == nc.hocsinh_id).GroupBy(x => x.tuvan_thutu).Count() : "",
                          chedo_name = (from cd in db.tbDangky_CheDoUuTiens
                                        where cd.chedo_id == nc.chedo_id
                                        select cd).Any() ? "" + db.tbDangky_CheDoUuTiens.Where(x => x.chedo_id == nc.chedo_id).FirstOrDefault().chedo_name : "",
                      };
        // đẩy dữ liệu vào gridivew
        grvList.DataSource = getData;
        grvList.DataBind();
        //load ds chương trình ưu đãi

        var getCTUD = from ct in db.tbWebsite_ChuongTrinhUuDais
                      where ct.ctud_status == 0
                      select new
                      {
                          ct.ctud_id,
                          ct.ctud_thoigian,
                          ct.ctud_noidung,
                          ct.ctud_thoigianbatdau,
                          ct.ctud_thoigianketthuc,
                          active = Convert.ToDateTime(ct.ctud_thoigianketthuc) >= DateTime.Now.AddDays(-1) ? "" : "disable_Active",
                          show = Convert.ToDateTime(ct.ctud_thoigianketthuc) >= DateTime.Now.AddDays(-1) ? "" : "show_input",
                      };
        rpCTUD.DataSource = getCTUD;
        rpCTUD.DataBind();
        //get ds chế độ ưu tiên
        rpCheDoUuTien.DataSource = from c in db.tbDangky_CheDoUuTiens
                                   select c;
        rpCheDoUuTien.DataBind();
        rptPhieuThu.DataSource = getData;
        rptPhieuThu.DataBind();
    }


    protected void btnDangKy_Click(object sender, EventArgs e)
    {
        List<object> ds_Checked = grvList.GetSelectedFieldValues(new string[] { "hocsinh_id" });
        if (ds_Checked.Count <= 0)
        {
            alert.alert_Warning(Page, "Vui lòng chọn học sinh để đăng ký đóng tiền!", "");
        }
        else if (ds_Checked.Count > 1)
        {
            alert.alert_Warning(Page, "Chỉ được chọn 1 học sinh để đăng ký đóng tiền!", "");
        }
        else
        {
            _id = Convert.ToInt32(ds_Checked[0]);
            Session["_id"] = _id;
            txtTienCoc.Value = "0";
            txtNoiDung.Value = "";
            txtCTUD_ID.Value = "";
            chkDatCoc.Checked = false;
            ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupControl.Show();", true);
        }
    }
    protected void btnLuu_Click(object sender, EventArgs e)
    {
        //try
        //{
        int ctud_id = 0;
        if (txtCTUD_ID.Value == "")
            ctud_id = 0;
        else
            ctud_id = Convert.ToInt32(txtCTUD_ID.Value);
        var getDetail_CTUD = (from ct in db.tbWebsite_ChuongTrinhUuDais
                              where ct.ctud_id == ctud_id
                              select ct).SingleOrDefault();
        var checkDK = (from dk in db.tbWebsite_DangKyDongTiens
                       where dk.hocsinh_dangky_id == Convert.ToInt32(Session["_id"].ToString())
                       select dk).FirstOrDefault();
        if (ctud_id == 0)
            noidung = txtNoiDung.Value;
        else
            noidung = getDetail_CTUD.ctud_noidung;
        if (checkDK==null)
        {
            tbWebsite_DangKyDongTien insert = new tbWebsite_DangKyDongTien();
            insert.hocsinh_dangky_id = Convert.ToInt32(Session["_id"].ToString());
            insert.ctud_id = ctud_id;
            insert.dongtien_chuongtrinh_uudai = noidung;
            insert.username_id = _idUser;
            insert.dongtien_ngaydangky = DateTime.Now;
            insert.dongtien_ngaycapnhat = DateTime.Now;
            insert.dongtien_tiencoc = txtTienCoc.Value;
            insert.dongtien_trangthai = 0;
            if (chkDatCoc.Checked == true)
                insert.dongtien_datcoc = true;
            else
                insert.dongtien_datcoc = false;
            db.tbWebsite_DangKyDongTiens.InsertOnSubmit(insert);
            db.SubmitChanges();
        }
        else
        {
            checkDK.ctud_id = ctud_id;
            checkDK.dongtien_chuongtrinh_uudai = noidung;
            checkDK.username_id = _idUser;
            //insert.dongtien_ngaydangky = DateTime.Now;
            checkDK.dongtien_ngaycapnhat = DateTime.Now;
            checkDK.dongtien_tiencoc = txtTienCoc.Value;
            checkDK.dongtien_trangthai = 0;
            if (chkDatCoc.Checked == true)
                checkDK.dongtien_datcoc = true;
            else
                checkDK.dongtien_datcoc = false;
            db.SubmitChanges();
        }
        //update trạng thái đóng tiền ở bảng đăng ký về true;
        tbWebsite_DangKiTuyenSinh upadte = db.tbWebsite_DangKiTuyenSinhs.Where(x => x.hocsinh_id == Convert.ToInt32(Session["_id"].ToString())).First();
        upadte.hocsinh_dongtien = true;
       upadte.hocsinh_tinhtrang = 7; // đóng tiền xong chuyển qua ds đang học
        db.SubmitChanges();
        //if (cls.ThemMoi(Convert.ToInt32(selectedId[0]), noidung, _idUser, txtTienCoc.Value))
        //    ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Lưu thành công!','','success').then(function(){grvList.Refresh();grvList.UnselectRows();})", true);
        alert.alert_Success(Page, "Lưu thành công!", "");
        popupControl.ShowOnPageLoad = false;
        loadData();
        //}
        //catch (Exception)
        //{
        //    alert.alert_Error(Page, "Đã có lỗi xảy ra!", "Vui lòng liên hệ IT");
        //}
    }
    protected void btnXem_Click(object sender, EventArgs e)
    {
        try
        {
            List<object> ds_Checked = grvList.GetSelectedFieldValues(new string[] { "hocsinh_id" });
            if (ds_Checked.Count() == 0)
            {
                alert.alert_Warning(Page, "Bạn chưa chọn học sinh!", "");
            }
            else if (ds_Checked.Count > 1)
            {
                alert.alert_Warning(Page, "Chỉ được chọn 1 học sinh để xem!", "");
            }
            else
            {
                Session["_idHocSinh"] = Convert.ToInt32(ds_Checked[0]);
                var getDetail = (from hs in db.tbWebsite_DangKiTuyenSinhs
                                 where hs.hocsinh_id == Convert.ToInt32(ds_Checked[0])
                                 select hs).Single();
                txtHoTenHS.Text = getDetail.hocsinh_name;
                txtDoB.Value = getDetail.hocsinh_dateofbirth.Value.ToString("yyyy-MM-dd").Replace(' ', 'T');
                txtQuocTich.Text = getDetail.hocsinh_quoctich;
                ddlLop.SelectedValue = getDetail.hocsinh_dangki_class;
                ddlCoSo.SelectedValue = getDetail.hocsinh_cosodangky + "";
                txtHoTenBa.Text = getDetail.hocsinh_phuhuynh_name;
                txtSDTBa.Text = getDetail.hocsinh_phone;
                txtEmailBa.Text = getDetail.hocsinh_email;
                txtHoTenMe.Text = getDetail.hocsinh_tenme;
                txtSDTMe.Text = getDetail.hocsinh_sdtme;
                txtEmailMe.Text = getDetail.hocsinh_emailme;
                txtDiaChi.Text = getDetail.hocsinh_address;
                if (getDetail.chedo_id != null)
                    txtCheDo.Value = getDetail.chedo_id + "";
                else
                    txtCheDo.Value = "0";
                txtLyDoUuTien.Value = getDetail.hocsinh_lydo_uutien;
                txtGhiChu.Value = getDetail.hocsinh_ghichu;
                txtNguonDangKy.Value = getDetail.hocsinh_nguondangky;
                ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupChiTiet.Show();setChecked();", true);

            }
        }
        catch (Exception)
        {
            alert.alert_Error(Page, "Đã có lỗi xảy ra!", "Vui lòng liên hệ IT");
        }
    }
    protected void btnCapNhat_Click(object sender, EventArgs e)
    {
        try
        {
            if
                    (cls_dangki.CapNhat(Convert.ToInt32(Session["_idHocSinh"].ToString()), txtHoTenHS.Text, Convert.ToDateTime(txtDoB.Value), txtQuocTich.Text, ddlLop.SelectedValue, txtHoTenBa.Text, txtDiaChi.Text, txtEmailBa.Text, txtSDTBa.Text, _idUser, Convert.ToInt32(ddlCoSo.SelectedValue), Convert.ToInt32(txtCheDo.Value), txtHoTenMe.Text, txtSDTMe.Text, txtEmailMe.Text, txtLyDoUuTien.Value, txtGhiChu.Value, txtNguonDangKy.Value))
            {
                ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Cập nhật thành công!','','success').then(function(){grvList.UnselectRows();})", true);
            }
            else
            {
                alert.alert_Error(Page, "Lưu thất bại!", "");
            }
            popupChiTiet.ShowOnPageLoad = false;
            loadData();
        }
        catch (Exception)
        {
            alert.alert_Error(Page, "Đã có lỗi xảy ra!", "Vui lòng liên hệ IT");

        }
    }

    protected void btnPrint_Click(object sender, EventArgs e)
    {
        //List<object> ds_Checked = grvList.GetSelectedFieldValues(new string[] { "hocsinh_id" });
        //foreach (var item in ds_Checked)
        //{
        //    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "print", "print('" + Convert.ToInt32(item) + "')", true);
        //}
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "text", "window.print();", true);
    }
}

