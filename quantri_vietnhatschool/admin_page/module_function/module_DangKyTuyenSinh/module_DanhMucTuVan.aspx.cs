﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_DangKyTuyenSinh_module_DanhMucTuVan : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    private static int _idUser;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["UserName"] != null)
        {
            if (!IsPostBack)
            {
                Session["_id"] = 0;
            }
            loadData();
            _idUser = (from u in db.admin_Users
                       where u.username_username == Request.Cookies["UserName"].Value
                       select u).FirstOrDefault().username_id;
        }
        else
        {
            Response.Redirect("/admin-login");
        }
    }

    private void loadData()
    {

        // load data đổ vào var danh sách
        var getData = from dm in db.tbWebsite_DanhMucTuVans
                      where dm.hidden == false
                      orderby dm.danhmuc_id descending
                      select dm;
        // đẩy dữ liệu vào gridivew
        grvList.DataSource = getData;
        grvList.DataBind();

    }
    private void setNULL()
    {
        txtTenDanhMuc.Text = "";

    }

    protected void btnThemMoi_Click(object sender, EventArgs e)
    {
        Session["_id"] = 0;
        setNULL();
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupControl.Show();", true);
    }

    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "danhmuc_id" }));
        Session["_id"] = _id;
        var getData = (from dm in db.tbWebsite_DanhMucTuVans
                       where dm.danhmuc_id == _id
                       select dm).Single();
        txtTenDanhMuc.Text = getData.danhmuc_name;
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupControl.Show();", true);
    }

    protected void btnLuu_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["_id"].ToString() == "0")
            {
                tbWebsite_DanhMucTuVan insert = new tbWebsite_DanhMucTuVan();
                insert.danhmuc_name = txtTenDanhMuc.Text;
                insert.hidden = false;
                db.tbWebsite_DanhMucTuVans.InsertOnSubmit(insert);
                db.SubmitChanges();
                alert.alert_Success(Page, "Lưu thành công!", "");
            }
            else
            {
                tbWebsite_DanhMucTuVan update = db.tbWebsite_DanhMucTuVans.Where(x => x.danhmuc_id == Convert.ToInt32(Session["_id"].ToString())).FirstOrDefault();
                update.danhmuc_name = txtTenDanhMuc.Text;
                db.SubmitChanges();
                ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Lưu thành công!','','success').then(function(){grvList.UnselectRows()})", true);
            }
            popupControl.ShowOnPageLoad = false;
            loadData();
        }
        catch (Exception)
        {
            alert.alert_Error(Page, "Đã có lỗi xảy ra!", "Vui lòng liên hệ IT");
        }
    }

    protected void btnXoa_Click(object sender, EventArgs e)
    {
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "danhmuc_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                tbWebsite_DanhMucTuVan checkDanhMuc = (from dm in db.tbWebsite_DanhMucTuVans where dm.danhmuc_id == Convert.ToInt32(item) select dm).SingleOrDefault();
                checkDanhMuc.hidden = true;
                db.SubmitChanges();
            }
            ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Xóa thành công!','','success').then(function(){grvList.UnselectRows()})", true);
        }
        else
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
        loadData();
    }
}