﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_DangKyTuyenSinh_module_DanhSachCu_module_DanhSachCho : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    cls_DangKyHocSinh cls = new cls_DangKyHocSinh();
    private static int _idUser;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["UserName"] != null)
        {
            if (!IsPostBack)
            {
                Session["_id"] = 0;
                Session["_idHocSinh"] = 0;
                setDropDown();
            }
            _idUser = (from u in db.admin_Users
                       where u.username_username == Request.Cookies["UserName"].Value
                       select u).FirstOrDefault().username_id;
            if (_idUser == 154)
            {
                loadDataCoSo(1);
            }
            else if (_idUser == 155)
            {
                loadDataCoSo(2);
            }
            else if (_idUser == 156)
            {
                loadDataCoSo(3);
            }
            else if (_idUser == 157)
            {
                loadDataCoSo(4);
            }
            else
            {
                loadData();
            }

        }
        else
        {
            Response.Redirect("/admin-login");
        }
    }
    //set item dropdown
    private void setDropDown()
    {
        int years = Convert.ToDateTime(DateTime.Now).Year;
        int vitri = 1;
        for (int i = 2005; i < years; i++)
        {
            ddlNam.Items.Insert(vitri, new ListItem(i + "", i + ""));
            vitri++;
        }

    }
    //load tất cả data
    private void loadData()
    {

        // load data đổ vào var danh sách
        var getData = from nc in db.tbWebsite_DangKiTuyenSinhs
                          //where nc.hocsinh_trangthai_dangky >= 0 //chưa tư vấn hoặc đã đang tư vấn
                          //&& nc.hocsinh_trangthai_dangky < 2
                          //&& nc.hocsinh_danhsachcho_khaosat == false // chưa thêm vào ds khảo sát
                          //Convert.ToDateTime(nc.hocsinh_dateofbirth).Year == nam
                      where nc.hocsinh_tinhtrang == 2 //ds hs đang chờ
                      orderby nc.chedo_id descending
                      select new
                      {
                          nc.hocsinh_id,
                          nc.hocsinh_name,
                          hocsinh_namsinh = nc.hocsinh_dateofbirth,
                          hocsinh_tuoi = DateTime.Now.Year - Convert.ToDateTime(nc.hocsinh_dateofbirth).Year + " tuổi",
                          nc.hocsinh_phuhuynh_name,
                          nc.hocsinh_tenme,
                          nc.hocsinh_address,
                          nc.hocsinh_phone,
                          nc.hocsinh_noidangky,
                          nc.hocsinh_dangki_class,
                          nc.hocsinh_sdtme,
                          nc.hocsinh_ghichu,
                          username_fullname = (from u in db.admin_Users
                                               where u.username_id == nc.username_id
                                               select u).FirstOrDefault().username_fullname,
                          trangthai = nc.hocsinh_trangthai_dangky == 0 ? "Chưa TV" : nc.hocsinh_trangthai_dangky == 1 ? "Đã TV" : "",
                          cosodangky = nc.hocsinh_cosodangky == 1 ? "CS1" : nc.hocsinh_cosodangky == 2 ? "CS2" : nc.hocsinh_cosodangky == 3 ? "CS3" : nc.hocsinh_cosodangky == 4 ? "TH-THCS-THPT" : "",
                          count = (from dk in db.tbWebsite_DangKiTuyenSinhs
                                   join tv in db.tbWebsite_TuVan_DangKiTuyenSinhs on dk.hocsinh_id equals tv.hocsinh_dangky_id
                                   where tv.hocsinh_dangky_id == nc.hocsinh_id
                                   select tv).Any() ? "" + db.tbWebsite_TuVan_DangKiTuyenSinhs.Where(x => x.hocsinh_dangky_id == nc.hocsinh_id).GroupBy(x => x.tuvan_thutu).Count() : "",
                          chedo_name = (from cd in db.tbDangky_CheDoUuTiens
                                        where cd.chedo_id == nc.chedo_id
                                        select cd).Any() ? "" + db.tbDangky_CheDoUuTiens.Where(x => x.chedo_id == nc.chedo_id).FirstOrDefault().chedo_name : "",
                      };
        // đẩy dữ liệu vào gridivew
        grvList.DataSource = getData;
        grvList.DataBind();
        //get ds chế độ ưu tiên
        rpCheDoUuTien.DataSource = from c in db.tbDangky_CheDoUuTiens
                                   orderby c.chedo_id descending
                                   select c;
        rpCheDoUuTien.DataBind();
    }
    //load data theo từng cơ sở
    private void loadDataCoSo(int coso)
    {

        // load data đổ vào var danh sách
        var getData = from nc in db.tbWebsite_DangKiTuyenSinhs
                          //where nc.hocsinh_trangthai_dangky >= 0 //chưa tư vấn hoặc đã đang tư vấn
                          //&& nc.hocsinh_trangthai_dangky < 2
                          //&& nc.hocsinh_danhsachcho_khaosat == false // chưa thêm vào ds khảo sát
                          // Convert.ToDateTime(nc.hocsinh_dateofbirth).Year == nam
                      where nc.hocsinh_cosodangky == coso
                      && nc.hocsinh_tinhtrang == 2 //ds hs đang chờ
                      orderby nc.chedo_id descending
                      select new
                      {
                          nc.hocsinh_id,
                          nc.hocsinh_name,
                          hocsinh_namsinh = nc.hocsinh_dateofbirth,
                          hocsinh_tuoi = DateTime.Now.Year - Convert.ToDateTime(nc.hocsinh_dateofbirth).Year + " tuổi",
                          nc.hocsinh_phuhuynh_name,
                          nc.hocsinh_tenme,
                          nc.hocsinh_address,
                          nc.hocsinh_phone,
                          nc.hocsinh_noidangky,
                          nc.hocsinh_dangki_class,
                          nc.hocsinh_sdtme,
                          nc.hocsinh_ghichu,
                          username_fullname = (from u in db.admin_Users
                                               where u.username_id == nc.username_id
                                               select u).FirstOrDefault().username_fullname,
                          trangthai = nc.hocsinh_trangthai_dangky == 0 ? "Chưa TV" : nc.hocsinh_trangthai_dangky == 1 ? "Đã TV" : "",
                          cosodangky = nc.hocsinh_cosodangky == 1 ? "CS1" : nc.hocsinh_cosodangky == 2 ? "CS2" : nc.hocsinh_cosodangky == 3 ? "CS3" : nc.hocsinh_cosodangky == 4 ? "TH-THCS-THPT" : "",
                          count = (from dk in db.tbWebsite_DangKiTuyenSinhs
                                   join tv in db.tbWebsite_TuVan_DangKiTuyenSinhs on dk.hocsinh_id equals tv.hocsinh_dangky_id
                                   where tv.hocsinh_dangky_id == nc.hocsinh_id
                                   select tv).Any() ? "" + db.tbWebsite_TuVan_DangKiTuyenSinhs.Where(x => x.hocsinh_dangky_id == nc.hocsinh_id).GroupBy(x => x.tuvan_thutu).Count() : "",
                          chedo_name = (from cd in db.tbDangky_CheDoUuTiens
                                        where cd.chedo_id == nc.chedo_id
                                        select cd).Any() ? "" + db.tbDangky_CheDoUuTiens.Where(x => x.chedo_id == nc.chedo_id).FirstOrDefault().chedo_name : "",
                      };
        // đẩy dữ liệu vào gridivew
        grvList.DataSource = getData;
        grvList.DataBind();
        //get ds chế độ ưu tiên
        rpCheDoUuTien.DataSource = from c in db.tbDangky_CheDoUuTiens
                                   orderby c.chedo_id descending
                                   select c;
        rpCheDoUuTien.DataBind();
    }
    //load data theo năm
    private void loadDatatheoNam(int nam)
    {

        // load data đổ vào var danh sách
        var getData = from nc in db.tbWebsite_DangKiTuyenSinhs
                          //where nc.hocsinh_trangthai_dangky >= 0 //chưa tư vấn hoặc đã đang tư vấn
                          //&& nc.hocsinh_trangthai_dangky < 2
                          //&& nc.hocsinh_danhsachcho_khaosat == false // chưa thêm vào ds khảo sát
                          //Convert.ToDateTime(nc.hocsinh_dateofbirth).Year == nam
                      where nc.hocsinh_tinhtrang == 2 //ds hs đang chờ
                      && Convert.ToDateTime(nc.hocsinh_dateofbirth).Year == nam
                      orderby nc.chedo_id descending
                      select new
                      {
                          nc.hocsinh_id,
                          nc.hocsinh_name,
                          hocsinh_namsinh = nc.hocsinh_dateofbirth,
                          hocsinh_tuoi = DateTime.Now.Year - Convert.ToDateTime(nc.hocsinh_dateofbirth).Year + " tuổi",
                          nc.hocsinh_phuhuynh_name,
                          nc.hocsinh_tenme,
                          nc.hocsinh_address,
                          nc.hocsinh_phone,
                          nc.hocsinh_noidangky,
                          nc.hocsinh_dangki_class,
                          nc.hocsinh_sdtme,
                          nc.hocsinh_ghichu,
                          username_fullname = (from u in db.admin_Users
                                               where u.username_id == nc.username_id
                                               select u).FirstOrDefault().username_fullname,
                          trangthai = nc.hocsinh_trangthai_dangky == 0 ? "Chưa TV" : nc.hocsinh_trangthai_dangky == 1 ? "Đã TV" : "",
                          cosodangky = nc.hocsinh_cosodangky == 1 ? "CS1" : nc.hocsinh_cosodangky == 2 ? "CS2" : nc.hocsinh_cosodangky == 3 ? "CS3" : nc.hocsinh_cosodangky == 4 ? "TH-THCS-THPT" : "",
                          count = (from dk in db.tbWebsite_DangKiTuyenSinhs
                                   join tv in db.tbWebsite_TuVan_DangKiTuyenSinhs on dk.hocsinh_id equals tv.hocsinh_dangky_id
                                   where tv.hocsinh_dangky_id == nc.hocsinh_id
                                   select tv).Any() ? "" + db.tbWebsite_TuVan_DangKiTuyenSinhs.Where(x => x.hocsinh_dangky_id == nc.hocsinh_id).GroupBy(x => x.tuvan_thutu).Count() : "",
                          chedo_name = (from cd in db.tbDangky_CheDoUuTiens
                                        where cd.chedo_id == nc.chedo_id
                                        select cd).Any() ? "" + db.tbDangky_CheDoUuTiens.Where(x => x.chedo_id == nc.chedo_id).FirstOrDefault().chedo_name : "",
                      };
        // đẩy dữ liệu vào gridivew
        grvList.DataSource = getData;
        grvList.DataBind();
        //get ds chế độ ưu tiên
        rpCheDoUuTien.DataSource = from c in db.tbDangky_CheDoUuTiens
                                   orderby c.chedo_id descending
                                   select c;
        rpCheDoUuTien.DataBind();
    }
    //load data theo từng cơ sở theo năm
    private void loadDataCoSotheoNam(int coso, int nam)
    {

        // load data đổ vào var danh sách
        var getData = from nc in db.tbWebsite_DangKiTuyenSinhs
                          //where nc.hocsinh_trangthai_dangky >= 0 //chưa tư vấn hoặc đã đang tư vấn
                          //&& nc.hocsinh_trangthai_dangky < 2
                          //&& nc.hocsinh_danhsachcho_khaosat == false // chưa thêm vào ds khảo sát
                          // Convert.ToDateTime(nc.hocsinh_dateofbirth).Year == nam
                      where nc.hocsinh_cosodangky == coso
                      && nc.hocsinh_tinhtrang == 2 //ds hs đang chờ
                      && Convert.ToDateTime(nc.hocsinh_dateofbirth).Year == nam
                      orderby nc.chedo_id descending
                      select new
                      {
                          nc.hocsinh_id,
                          nc.hocsinh_name,
                          hocsinh_namsinh = nc.hocsinh_dateofbirth,
                          hocsinh_tuoi = DateTime.Now.Year - Convert.ToDateTime(nc.hocsinh_dateofbirth).Year + " tuổi",
                          nc.hocsinh_phuhuynh_name,
                          nc.hocsinh_tenme,
                          nc.hocsinh_address,
                          nc.hocsinh_phone,
                          nc.hocsinh_noidangky,
                          nc.hocsinh_dangki_class,
                          nc.hocsinh_sdtme,
                          nc.hocsinh_ghichu,
                          username_fullname = (from u in db.admin_Users
                                               where u.username_id == nc.username_id
                                               select u).FirstOrDefault().username_fullname,
                          trangthai = nc.hocsinh_trangthai_dangky == 0 ? "Chưa TV" : nc.hocsinh_trangthai_dangky == 1 ? "Đã TV" : "",
                          cosodangky = nc.hocsinh_cosodangky == 1 ? "CS1" : nc.hocsinh_cosodangky == 2 ? "CS2" : nc.hocsinh_cosodangky == 3 ? "CS3" : nc.hocsinh_cosodangky == 4 ? "TH-THCS-THPT" : "",
                          count = (from dk in db.tbWebsite_DangKiTuyenSinhs
                                   join tv in db.tbWebsite_TuVan_DangKiTuyenSinhs on dk.hocsinh_id equals tv.hocsinh_dangky_id
                                   where tv.hocsinh_dangky_id == nc.hocsinh_id
                                   select tv).Any() ? "" + db.tbWebsite_TuVan_DangKiTuyenSinhs.Where(x => x.hocsinh_dangky_id == nc.hocsinh_id).GroupBy(x => x.tuvan_thutu).Count() : "",
                          chedo_name = (from cd in db.tbDangky_CheDoUuTiens
                                        where cd.chedo_id == nc.chedo_id
                                        select cd).Any() ? "" + db.tbDangky_CheDoUuTiens.Where(x => x.chedo_id == nc.chedo_id).FirstOrDefault().chedo_name : "",
                      };
        // đẩy dữ liệu vào gridivew
        grvList.DataSource = getData;
        grvList.DataBind();
        //get ds chế độ ưu tiên
        rpCheDoUuTien.DataSource = from c in db.tbDangky_CheDoUuTiens
                                   orderby c.chedo_id descending
                                   select c;
        rpCheDoUuTien.DataBind();
    }

    protected void ddlNam_SelectedIndexChanged(object sender, EventArgs e)
    {
        int nam = Convert.ToInt32(ddlNam.SelectedValue);
        if (_idUser == 154)
        {
            loadDataCoSotheoNam(1, nam);
        }
        else if (_idUser == 155)
        {
            loadDataCoSotheoNam(2, nam);
        }
        else if (_idUser == 156)
        {
            loadDataCoSotheoNam(3, nam);
        }
        else if (_idUser == 157)
        {
            loadDataCoSotheoNam(4, nam);
        }
        else
        {
            loadDatatheoNam(nam);
        }
    }
    protected void btnXem_Click(object sender, EventArgs e)
    {
        try
        {
            List<object> ds_Checked = grvList.GetSelectedFieldValues(new string[] { "hocsinh_id" });
            if (ds_Checked.Count() == 0)
            {
                alert.alert_Warning(Page, "Bạn chưa chọn học sinh!", "");
            }
            else if (ds_Checked.Count > 1)
            {
                alert.alert_Warning(Page, "Chỉ được chọn 1 học sinh để xem!", "");
            }
            else
            {
                Session["_idHocSinh"] = Convert.ToInt32(ds_Checked[0]);
                var getDetail = (from hs in db.tbWebsite_DangKiTuyenSinhs
                                 where hs.hocsinh_id == Convert.ToInt32(ds_Checked[0])
                                 select hs).Single();
                txtHoTenHS.Text = getDetail.hocsinh_name;
                txtDoB.Value = getDetail.hocsinh_dateofbirth.Value.ToString("yyyy-MM-dd").Replace(' ', 'T');
                txtQuocTich.Text = getDetail.hocsinh_quoctich;
                ddlLop.SelectedValue = getDetail.hocsinh_dangki_class;
                ddlCoSo.SelectedValue = getDetail.hocsinh_cosodangky + "";
                txtHoTenBa.Text = getDetail.hocsinh_phuhuynh_name;
                txtSDTBa.Text = getDetail.hocsinh_phone;
                txtEmailBa.Text = getDetail.hocsinh_email;
                txtHoTenMe.Text = getDetail.hocsinh_tenme;
                txtSDTMe.Text = getDetail.hocsinh_sdtme;
                txtEmailMe.Text = getDetail.hocsinh_emailme;
                txtDiaChi.Text = getDetail.hocsinh_address;
                if (getDetail.chedo_id != null)
                    txtCheDo.Value = getDetail.chedo_id + "";
                else
                    txtCheDo.Value = "0";
                txtLyDoUuTien.Value = getDetail.hocsinh_lydo_uutien;
                txtGhiChu.Value = getDetail.hocsinh_ghichu;
                txtNguonDangKy.Value = getDetail.hocsinh_nguondangky;
                ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupChiTiet.Show();setChecked();", true);

            }
        }
        catch (Exception)
        {
            alert.alert_Error(Page, "Đã có lỗi xảy ra!", "Vui lòng liên hệ IT");
        }
    }
    protected void btnCapNhat_Click(object sender, EventArgs e)
    {
        try
        {
            if
                    (cls.CapNhat(Convert.ToInt32(Session["_idHocSinh"].ToString()), txtHoTenHS.Text, Convert.ToDateTime(txtDoB.Value), txtQuocTich.Text, ddlLop.SelectedValue, txtHoTenBa.Text, txtDiaChi.Text, txtEmailBa.Text, txtSDTBa.Text, _idUser, Convert.ToInt32(ddlCoSo.SelectedValue), Convert.ToInt32(txtCheDo.Value), txtHoTenMe.Text, txtSDTMe.Text, txtEmailMe.Text, txtLyDoUuTien.Value, txtGhiChu.Value, txtNguonDangKy.Value))
            {
                ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Cập nhật thành công!','','success').then(function(){grvList.UnselectRows();})", true);
            }
            else
            {
                alert.alert_Error(Page, "Lưu thất bại!", "");
            }
            popupChiTiet.ShowOnPageLoad = false;
            if (_idUser == 154)
            {
                loadDataCoSo(1);
            }
            else if (_idUser == 155)
            {
                loadDataCoSo(2);
            }
            else if (_idUser == 156)
            {
                loadDataCoSo(3);
            }
            else if (_idUser == 157)
            {
                loadDataCoSo(4);
            }
            else
            {
                loadData();
            }
        }
        catch (Exception)
        {
            alert.alert_Error(Page, "Đã có lỗi xảy ra!", "Vui lòng liên hệ IT");

        }
    }
    protected void btnChuyenDanhSach_ServerClick(object sender, EventArgs e)
    {
        try
        {
            List<object> ds_Checked = grvList.GetSelectedFieldValues(new string[] { "hocsinh_id" });
            if (ds_Checked.Count() == 0)
            {
                alert.alert_Warning(Page, "Bạn chưa chọn học sinh!", "");
                ddlChuyenDanhSach.SelectedValue = "";
            }
            else if (ddlChuyenDanhSach.SelectedValue == "")
            {
                alert.alert_Warning(Page, "Bạn chưa chọn danh sách!", "");
            }
            else
            {
                foreach (var item in ds_Checked)
                {
                    tbWebsite_DangKiTuyenSinh find = db.tbWebsite_DangKiTuyenSinhs.Where(x => x.hocsinh_id == Convert.ToInt32(item)).FirstOrDefault();
                    find.hocsinh_tinhtrang = Convert.ToInt32(ddlChuyenDanhSach.SelectedValue);
                    db.SubmitChanges();
                }

                ddlChuyenDanhSach.SelectedValue = "";
                ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Chuyển danh sách thành công!','','success').then(function(){grvList.Refresh();grvList.UnselectRows();})", true);
            }
        }
        catch (Exception)
        {
            alert.alert_Error(Page, "Đã có lỗi xảy ra!", "Vui lòng liên hệ IT");
        }
    }
}