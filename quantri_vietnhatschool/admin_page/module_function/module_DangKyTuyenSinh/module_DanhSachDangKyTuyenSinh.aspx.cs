﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_DangKyTuyenSinh_module_DanhSachDangKyTuyenSinh : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    cls_DangKyHocSinh cls = new cls_DangKyHocSinh();
    private int _idHocSinh;
    private static int _idUser;
    public string hidden_Action;
    public string hidden_Button;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["UserName"] != null)
        {
            if (!IsPostBack)
            {
                Session["_id"] = 0;
                Session["_idHocSinh"] = 0;
            }
            _idUser = (from u in db.admin_Users
                       where u.username_username == Request.Cookies["UserName"].Value
                       select u).FirstOrDefault().username_id;
            //if (_idUser != 153)
            //{
            hidden_Action = "hidden_khaosat";
            //}
            if (_idUser == 154)
            {
                loadDataCoSo(1);
                hiddenButton();
                div_Search.Visible = false;
            }
            else if (_idUser == 155)
            {
                loadDataCoSo(2);
                hiddenButton();
                hidden_Button = "hidden_chuyencoso";
                div_Search.Visible = false;
            }
            else if (_idUser == 156)
            {
                loadDataCoSo(3);
                hiddenButton();
                hidden_Button = "hidden_chuyencoso";
                div_Search.Visible = false;
            }
            else if (_idUser == 157)
            {
                loadDataCoSo(4);
                if (ddlSearch.SelectedValue != "0")
                    filterDataByClass(ddlSearch.SelectedValue);
                hiddenButton();
                hidden_Button = "hidden_chuyencoso";
            }
            else
            {
                loadData();
            }

        }
        else
        {
            Response.Redirect("/admin-login");
        }
    }
    private void hiddenButton()
    {
        btnChuyenCoSo.Visible = false;
    }
    private void loadData()
    {

        // load data đổ vào var danh sách
        var getData = from nc in db.tbWebsite_DangKiTuyenSinhs
                      where nc.hocsinh_trangthai_dangky >= 0 //chưa tư vấn hoặc đã đang tư vấn
                      //&& nc.hocsinh_trangthai_dangky < 2
                      //&& nc.hocsinh_danhsachcho_khaosat == false // chưa thêm vào ds khảo sát
                      && nc.hocsinh_tinhtrang == 6 //đăng kí mới
                      orderby nc.chedo_id descending
                      select new
                      {
                          nc.hocsinh_id,
                          nc.hocsinh_name,
                          hocsinh_namsinh = nc.hocsinh_dateofbirth,
                          hocsinh_tuoi = DateTime.Now.Year - Convert.ToDateTime(nc.hocsinh_dateofbirth).Year + " tuổi",
                          nc.hocsinh_phuhuynh_name,
                          nc.hocsinh_tenme,
                          nc.hocsinh_address,
                          nc.hocsinh_phone,
                          nc.hocsinh_noidangky,
                          nc.hocsinh_dangki_class,
                          nc.hocsinh_sdtme,
                          nc.hocsinh_ghichu,
                          username_fullname = (from u in db.admin_Users
                                               where u.username_id == nc.username_id
                                               select u).FirstOrDefault().username_fullname,
                          trangthai = nc.hocsinh_trangthai_dangky == 0 ? "Chưa TV" : nc.hocsinh_trangthai_dangky == 1 ? "Đã TV" : "",
                          cosodangky = nc.hocsinh_cosodangky == 1 ? "CS1" : nc.hocsinh_cosodangky == 2 ? "CS2" : nc.hocsinh_cosodangky == 3 ? "CS3" : nc.hocsinh_cosodangky == 4 ? "TH-THCS-THPT" : "",
                          count = (from dk in db.tbWebsite_DangKiTuyenSinhs
                                   join tv in db.tbWebsite_TuVan_DangKiTuyenSinhs on dk.hocsinh_id equals tv.hocsinh_dangky_id
                                   where tv.hocsinh_dangky_id == nc.hocsinh_id
                                   select tv).Any() ? "" + db.tbWebsite_TuVan_DangKiTuyenSinhs.Where(x => x.hocsinh_dangky_id == nc.hocsinh_id).GroupBy(x => x.tuvan_thutu).Count() : "",
                          chedo_name = (from cd in db.tbDangky_CheDoUuTiens
                                        where cd.chedo_id == nc.chedo_id
                                        select cd).Any() ? "" + db.tbDangky_CheDoUuTiens.Where(x => x.chedo_id == nc.chedo_id).FirstOrDefault().chedo_name : "",
                      };
        // đẩy dữ liệu vào gridivew
        grvList.DataSource = getData;
        grvList.DataBind();
        //get ds chế độ ưu tiên
        rpCheDoUuTien.DataSource = from c in db.tbDangky_CheDoUuTiens
                                   orderby c.chedo_id descending
                                   select c;
        rpCheDoUuTien.DataBind();
    }
    //load danh sách đăng kí theo từng cơ sở
    private void loadDataCoSo(int coso)
    {

        // load data đổ vào var danh sách
        var getData = from nc in db.tbWebsite_DangKiTuyenSinhs
                      where nc.hocsinh_trangthai_dangky >= 0 //chưa tư vấn hoặc đã đang tư vấn
                      //&& nc.hocsinh_trangthai_dangky < 2
                      //&& nc.hocsinh_danhsachcho_khaosat == false // chưa thêm vào ds khảo sát
                      && nc.hocsinh_cosodangky == coso
                      && nc.hocsinh_tinhtrang == 6 //đăng kí mới
                      orderby nc.chedo_id descending
                      select new
                      {
                          nc.hocsinh_id,
                          nc.hocsinh_name,
                          hocsinh_namsinh = nc.hocsinh_dateofbirth,
                          hocsinh_tuoi = DateTime.Now.Year - Convert.ToDateTime(nc.hocsinh_dateofbirth).Year + " tuổi",
                          nc.hocsinh_phuhuynh_name,
                          nc.hocsinh_tenme,
                          nc.hocsinh_address,
                          nc.hocsinh_phone,
                          nc.hocsinh_noidangky,
                          nc.hocsinh_dangki_class,
                          nc.hocsinh_sdtme,
                          nc.hocsinh_ghichu,
                          username_fullname = (from u in db.admin_Users
                                               where u.username_id == nc.username_id
                                               select u).FirstOrDefault().username_fullname,
                          trangthai = nc.hocsinh_trangthai_dangky == 0 ? "Chưa TV" : nc.hocsinh_trangthai_dangky == 1 ? "Đã TV" : "",
                          cosodangky = nc.hocsinh_cosodangky == 1 ? "CS1" : nc.hocsinh_cosodangky == 2 ? "CS2" : nc.hocsinh_cosodangky == 3 ? "CS3" : nc.hocsinh_cosodangky == 4 ? "TH-THCS-THPT" : "",
                          count = (from dk in db.tbWebsite_DangKiTuyenSinhs
                                   join tv in db.tbWebsite_TuVan_DangKiTuyenSinhs on dk.hocsinh_id equals tv.hocsinh_dangky_id
                                   where tv.hocsinh_dangky_id == nc.hocsinh_id
                                   select tv).Any() ? "" + db.tbWebsite_TuVan_DangKiTuyenSinhs.Where(x => x.hocsinh_dangky_id == nc.hocsinh_id).GroupBy(x => x.tuvan_thutu).Count() : "",
                          chedo_name = (from cd in db.tbDangky_CheDoUuTiens
                                        where cd.chedo_id == nc.chedo_id
                                        select cd).Any() ? "" + db.tbDangky_CheDoUuTiens.Where(x => x.chedo_id == nc.chedo_id).FirstOrDefault().chedo_name : "",
                      };
        // đẩy dữ liệu vào gridivew
        grvList.DataSource = getData;
        grvList.DataBind();
        //get ds chế độ ưu tiên
        rpCheDoUuTien.DataSource = from c in db.tbDangky_CheDoUuTiens
                                   orderby c.chedo_id descending
                                   select c;
        rpCheDoUuTien.DataBind();
    }
    private void setNULL()
    {
        txtHoTenHS.Text = "";
        txtNgaySinh.Text = "";
        txtQuocTich.Text = "";
        txtHoTenBa.Text = "";
        txtSDTBa.Text = "";
        txtEmailBa.Text = "";
        txtHoTenMe.Text = "";
        txtSDTMe.Text = "";
        txtEmailMe.Text = "";
        txtDiaChi.Text = "";
        ddlLop.SelectedValue = "0";
        ddlCoSo.SelectedValue = "0";
        txtGhiChu.Value = "";
        txtCheDo.Value = "0";
        txtLyDoUuTien.Value = "";
        txtNguonDangKy.Value = "";
    }
    //thêm thông tin mới
    protected void btnThemMoi_Click(object sender, EventArgs e)
    {
        Session["_idHocSinh"] = 0;
        setNULL();
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupControl.Show();", true);
    }

    //kiểm tra học sinh tồn tại
    private bool checkHocSinh(string hoten, DateTime ngaysinh)
    {
        string name = hoten.ToLower();
        var check_HS = db.tbWebsite_DangKiTuyenSinhs.Any(x => x.hocsinh_name.ToLower() == name && x.hocsinh_dateofbirth.Value.Day == ngaysinh.Day && x.hocsinh_dateofbirth.Value.Month == ngaysinh.Month && x.hocsinh_dateofbirth.Value.Year == ngaysinh.Year);
        if (check_HS == true)
            return false;
        else
            return true;

    }


    //lưu thông tin học sinh
    protected void btnLuu_Click(object sender, EventArgs e)
    {
        try
        {

            if (Session["_idHocSinh"].ToString() == "0")
            {
                if (checkHocSinh(txtHoTenHS.Text, Convert.ToDateTime(txtNgaySinh.Text)) == false)
                {
                    alert.alert_Warning(Page, "Đã tồn tại học sinh!", "");
                }
                else
                {
                    if
                        (cls.ThemMoi(txtHoTenHS.Text, Convert.ToDateTime(txtNgaySinh.Text), txtQuocTich.Text, ddlLop.SelectedValue, txtHoTenBa.Text, txtDiaChi.Text, txtEmailBa.Text, txtSDTBa.Text, _idUser, Convert.ToInt32(ddlCoSo.SelectedValue), Convert.ToInt32(txtCheDo.Value), txtHoTenMe.Text, txtSDTMe.Text, txtEmailMe.Text, txtLyDoUuTien.Value, txtGhiChu.Value, txtNguonDangKy.Value))
                    {
                        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Lưu thành công!','','success').then(function(){grvList.Refresh();})", true);
                    }
                    else
                    {
                        alert.alert_Error(Page, "Lưu thất bại!", "");
                    }
                    popupControl.ShowOnPageLoad = false;
                }
            }
            else
            {
                if
                        (cls.CapNhat(Convert.ToInt32(Session["_idHocSinh"].ToString()), txtHoTenHS.Text, Convert.ToDateTime(txtNgaySinh.Text), txtQuocTich.Text, ddlLop.SelectedValue, txtHoTenBa.Text, txtDiaChi.Text, txtEmailBa.Text, txtSDTBa.Text, _idUser, Convert.ToInt32(ddlCoSo.SelectedValue), Convert.ToInt32(txtCheDo.Value), txtHoTenMe.Text, txtSDTMe.Text, txtEmailMe.Text, txtLyDoUuTien.Value, txtGhiChu.Value, txtNguonDangKy.Value))
                {
                    ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Lưu thành công!','','success').then(function(){grvList.Refresh();grvList.UnselectRows();})", true);
                }
                else
                {
                    alert.alert_Error(Page, "Lưu thất bại!", "");
                }
                popupControl.ShowOnPageLoad = false;
            }
        }
        catch (Exception)
        {
            alert.alert_Error(Page, "Đã có lỗi xảy ra!", "Vui lòng liên hệ IT");

        }
    }

    //lưu nội dung tư vấn
    protected void btnLuu_Tuvan_Click(object sender, EventArgs e)
    {
        try
        {
            string checkid = txtDanhSachChecked.Value;
            string[] getcheckid = checkid.Split(',');
            int lanTV = 1;
            //get lần tư vấn cuối cùng của hs
            if (db.tbWebsite_TuVan_DangKiTuyenSinhs.Any(x => x.hocsinh_dangky_id == Convert.ToInt32(Session["_id"].ToString())))
            {
                var getTuVan = (from tv in db.tbWebsite_TuVan_DangKiTuyenSinhs
                                where tv.hocsinh_dangky_id == Convert.ToInt32(Session["_id"].ToString())
                                orderby tv.tuvan_id descending
                                select tv).First();
                lanTV = Convert.ToInt32(getTuVan.tuvan_thutu) + 1;

            }
            foreach (var item in getcheckid)
            {
                //lưu nội dung tư vấn vào bảng tư vấn
                tbWebsite_TuVan_DangKiTuyenSinh insert = new tbWebsite_TuVan_DangKiTuyenSinh();
                insert.hocsinh_dangky_id = Convert.ToInt32(Session["_id"].ToString());
                insert.tuvan_noidung = edtnoidung.Html;
                insert.tuvan_user = _idUser;
                insert.tuvan_ngaytuvan = DateTime.Now;
                insert.tuvan_trangthai = 0;
                insert.danhmuc_id = Convert.ToInt32(item);
                insert.tuvan_thutu = lanTV;
                db.tbWebsite_TuVan_DangKiTuyenSinhs.InsertOnSubmit(insert);
                db.SubmitChanges();
            }
            //
            //cập nhật trạng thái đã tư vấn bên bảng đăng ký tuyển sinh
            var getHocSinh = (from hs in db.tbWebsite_DangKiTuyenSinhs
                              where hs.hocsinh_id == Convert.ToInt32(Session["_id"].ToString())
                              select hs).FirstOrDefault();
            getHocSinh.hocsinh_trangthai_dangky = 1; //đã được tư vấn
            //nếu checked vào button chờ khảo sát thì chuyển qua ds chờ khảo sát
            if (chkChoKhaoSat.Checked == true)
            {
                getHocSinh.hocsinh_danhsachcho_khaosat = true;
                getHocSinh.hocsinh_tinhtrang = 3; //ds chờ khảo sát
            }
            db.SubmitChanges();
            ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Lưu thành công!','','success').then(function(){grvList.UnselectRows();})", true);
            loadData();
            popupTuvan.ShowOnPageLoad = false;
        }
        catch (Exception)
        {
            alert.alert_Error(Page, "Đã có lỗi xảy ra!", "Vui lòng liên hệ IT");
        }
    }
    //get ds tư vấn
    private void danhSachTuVan(int hs_id)
    {
        var getData = (from tv in db.tbWebsite_TuVan_DangKiTuyenSinhs
                           //join u in db.admin_Users on tv.tuvan_user equals u.username_id
                       where tv.tuvan_trangthai == 0 && tv.hocsinh_dangky_id == hs_id
                       group tv by tv.tuvan_thutu into k
                       select new
                       {
                           tuvan_noidung = k.First().tuvan_noidung,
                           tuvan_ngaytuvan = k.First().tuvan_ngaytuvan,
                           username_fullname = (from u in db.admin_Users
                                                where u.username_id == Convert.ToInt32(k.First().tuvan_user)
                                                select u.username_fullname).First(),
                           danhmuc = string.Join(", ", (from tv in db.tbWebsite_TuVan_DangKiTuyenSinhs
                                                        join dm in db.tbWebsite_DanhMucTuVans on tv.danhmuc_id equals dm.danhmuc_id
                                                        where tv.tuvan_thutu == k.Key && tv.hocsinh_dangky_id == hs_id
                                                        select dm.danhmuc_name)),

                       }); ;
        rpTuVan.DataSource = getData;
        rpTuVan.DataBind();

        //load danh mục đã tư vấn
        var listDaTuVan = (from tv in db.tbWebsite_TuVan_DangKiTuyenSinhs
                           join dm in db.tbWebsite_DanhMucTuVans on tv.danhmuc_id equals dm.danhmuc_id
                           where tv.tuvan_trangthai == 0 && tv.hocsinh_dangky_id == hs_id
                           select new
                           {
                               dm.danhmuc_id,
                               dm.danhmuc_name,
                               dm.hidden
                           });
        //load list all danh mục 
        var listDanhMuc = (from dm in db.tbWebsite_DanhMucTuVans
                           where dm.hidden == false
                           select new
                           {
                               dm.danhmuc_id,
                               dm.danhmuc_name,
                               dm.hidden
                           }).ToList();
        var listConLai = listDanhMuc.Except(listDaTuVan);
        rptDanhMucTuVan.DataSource = listConLai;
        rptDanhMucTuVan.DataBind();

    }

    //show form tư vấn
    protected void btnTuVan_Click(object sender, EventArgs e)
    {
        try
        {
            //int hs_id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "hocsinh_id" }));
            List<object> ds_Checked = grvList.GetSelectedFieldValues(new string[] { "hocsinh_id" });
            if (ds_Checked.Count <= 0)
            {
                alert.alert_Warning(Page, "Vui lòng chọn học sinh cần tư vấn!", "");
            }
            else if (ds_Checked.Count > 1)
            {
                alert.alert_Warning(Page, "Chỉ được chọn 1 học sinh để tư vấn!", "");
            }

            else
            {
                foreach (var item in ds_Checked)
                {
                    _idHocSinh = Convert.ToInt32(item);
                }
                Session["_id"] = _idHocSinh;
                var checkTrangThai = (from hs in db.tbWebsite_DangKiTuyenSinhs
                                      where hs.hocsinh_id == _idHocSinh
                                      select hs).FirstOrDefault();
                if (checkTrangThai.hocsinh_trangthai_dangky == -1)//đã khảo sát đạt
                {
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "hiddenbutton", "hiddenButton()", true);
                }
                if (checkTrangThai.hocsinh_danhsachcho_khaosat == true)
                    chkChoKhaoSat.Checked = true;
                else
                    chkChoKhaoSat.Checked = false;
                //nếu chưa có cơ sở đăng ký thì ấn nút chờ khảo sát luôn
                if (checkTrangThai.hocsinh_cosodangky == 0)
                {
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "hiddenbutton", "hiddenRadio()", true);
                }
                danhSachTuVan(_idHocSinh);
                //loadDanhMucTuVan();
                edtnoidung.Html = "";
                txtDanhSachChecked.Value = "";
                txtCountChecked.Value = "";
                ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupTuvan.Show();", true);
            }
        }
        catch (Exception)
        {
            alert.alert_Error(Page, "Đã có lỗi xảy ra!", "Vui lòng liên hệ IT");
        }
    }

    private bool SendMail(string email)
    {

        if (email != "")
        {
            try
            {
                var fromAddress = "thongbaovietnhatschool@gmail.com";//  Email Address from where you send the mail 
                var toAddress = email;
                const string fromPassword = "neiabcekdjluofid";
                string subject, title;
                title = "Thông báo";
                subject = "<!DOCTYPE html><html><head><title></title></head><body ><div>" +
                "<h3 style=\"margin-top:0px; text-align:center; color:#029ada\">Có học sinh mới cần khảo sát. Xem chi tiết <a href='http://quantridemo.vietnhatschool.edu.vn/admin-danh-sach-khao-sat'>tại đây.</a></h3>" +
                "</div></body></html>";
                var smtp = new SmtpClient();
                {
                    smtp.Host = "smtp.gmail.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    smtp.Credentials = new NetworkCredential(fromAddress, fromPassword);
                    smtp.Timeout = 20000;
                }
                MailMessage mm = new MailMessage();
                mm.From = new MailAddress(fromAddress, "Trường Liên cấp Việt Nhật");
                mm.Subject = title;
                mm.To.Add(toAddress);
                mm.IsBodyHtml = true;
                mm.Body = subject;
                smtp.Send(mm);
                return true;
            }
            catch
            {
                return false;
            }
        }
        else
            return false;
    }

    //thêm vào ds chờ khảo sát
    protected void btnThemDSChoKS_Click(object sender, EventArgs e)
    {
        try
        {
            List<object> ds_Checked = grvList.GetSelectedFieldValues(new string[] { "hocsinh_id" });
            var listCoSo = (from hs in db.tbWebsite_DangKiTuyenSinhs
                            where hs.hocsinh_cosodangky == 0 || hs.hocsinh_cosodangky == null
                            select hs.hocsinh_id).ToArray();
            var listTuVan = (from hs in db.tbWebsite_DangKiTuyenSinhs
                             where hs.hocsinh_trangthai_dangky == 0 //chuă tư vấn
                             select hs.hocsinh_id).ToArray();
            var arr_checked = ds_Checked.ToArray();
            var arr_coso = listCoSo.ToArray();
            var arr_tuvan = listTuVan.ToArray();
            var check_coso = from item1 in arr_checked
                             join item2 in arr_coso on item1 equals item2
                             select item1;
            var check_tuvan = from hs in arr_checked
                              join tv in arr_tuvan on hs equals tv
                              select hs;
            if (ds_Checked.Count() == 0)
            {
                alert.alert_Warning(Page, "Bạn chưa chọn học sinh!", "");
            }
            else if (check_coso.Count() > 0)
            {
                alert.alert_Warning(Page, "Có học sinh chưa chuyển cơ sở!", "Vui lòng kiểm tra lại");
            }
            else if (check_tuvan.Count() > 0)
            {
                alert.alert_Warning(Page, "Có học sinh chưa chưa được tư vấn!", "Vui lòng kiểm tra lại");
            }
            else
            {
                foreach (var item in ds_Checked)
                {
                    tbWebsite_DangKiTuyenSinh find = db.tbWebsite_DangKiTuyenSinhs.Where(x => x.hocsinh_id == Convert.ToInt32(item)).FirstOrDefault();
                    find.hocsinh_danhsachcho_khaosat = true;
                    find.hocsinh_tinhtrang = 3;//chờ khảo sát
                    db.SubmitChanges();
                }
                ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Thêm vào danh sách chờ thành công!','','success').then(function(){grvList.UnselectRows();})", true);
                loadData();
            }
        }
        catch (Exception)
        {
            alert.alert_Error(Page, "Đã có lỗi xảy ra!", "Vui lòng liên hệ IT");
        }
    }

    //chuyển cơ sở
    protected void btnChuyenCoSo_Click(object sender, EventArgs e)
    {
        try
        {
            List<object> ds_Checked = grvList.GetSelectedFieldValues(new string[] { "hocsinh_id" });
            if (ds_Checked.Count() == 0)
            {
                alert.alert_Warning(Page, "Bạn chưa chọn học sinh!", "");
            }
            else if (ddlChuyenCoSo.SelectedValue == "0")
            {
                alert.alert_Warning(Page, "Bạn chưa chọn cơ sở!", "");
            }
            else
            {
                foreach (var item in ds_Checked)
                {
                    tbWebsite_DangKiTuyenSinh find = db.tbWebsite_DangKiTuyenSinhs.Where(x => x.hocsinh_id == Convert.ToInt32(item)).FirstOrDefault();
                    find.hocsinh_cosodangky = Convert.ToInt32(ddlChuyenCoSo.SelectedValue);
                    db.SubmitChanges();
                }

                ddlChuyenCoSo.SelectedValue = "0";
                ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Chuyển cơ sở thành công!','','success').then(function(){grvList.Refresh();grvList.UnselectRows();})", true);
            }
        }
        catch (Exception)
        {
            alert.alert_Error(Page, "Đã có lỗi xảy ra!", "Vui lòng liên hệ IT");
        }
    }

    //thêm vào ds khảo sát
    protected void btnXacNhanKhaoSat_ServerClick(object sender, EventArgs e)
    {
        try
        {
            List<object> ds_Checked = grvList.GetSelectedFieldValues(new string[] { "hocsinh_id" });
            var listCoSo = (from hs in db.tbWebsite_DangKiTuyenSinhs
                            where hs.hocsinh_cosodangky == 0 || hs.hocsinh_cosodangky == null
                            select hs.hocsinh_id).ToArray();
            var listTuVan = (from hs in db.tbWebsite_DangKiTuyenSinhs
                             where hs.hocsinh_trangthai_dangky == 0 //chuă tư vấn
                             select hs.hocsinh_id).ToArray();
            var arr_checked = ds_Checked.ToArray();
            var arr_coso = listCoSo.ToArray();
            var arr_tuvan = listTuVan.ToArray();
            var check_coso = from item1 in arr_checked
                             join item2 in arr_coso on item1 equals item2
                             select item1;
            var check_tuvan = from hs in arr_checked
                              join tv in arr_tuvan on hs equals tv
                              select hs;
            if (ds_Checked.Count() == 0)
            {
                alert.alert_Warning(Page, "Bạn chưa chọn học sinh!", "");
            }
            else if (txtNgayKhaoSat.Value == "")
            {
                alert.alert_Warning(Page, "Bạn chưa chọn ngày khảo sát!", "");
            }
            else if (check_coso.Count() > 0)
            {
                alert.alert_Warning(Page, "Có học sinh chưa chuyển cơ sở!", "Vui lòng kiểm tra lại");
            }
            else if (check_tuvan.Count() > 0)
            {
                alert.alert_Warning(Page, "Có học sinh chưa chưa được tư vấn!", "Vui lòng kiểm tra lại");
            }
            else
            {
                foreach (var item in ds_Checked)
                {
                    tbWebsite_DangKiTuyenSinh find = db.tbWebsite_DangKiTuyenSinhs.Where(x => x.hocsinh_id == Convert.ToInt32(item)).FirstOrDefault();
                    find.hocsinh_ngaykhaosat = Convert.ToDateTime(txtNgayKhaoSat.Value);
                    find.hocsinh_trangthai_dangky = 2;//được khảo sát
                    find.hocsinh_tinhtrang = 4; //ds khảo sát
                    db.SubmitChanges();
                }
                ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Lưu thành công!','','success').then(function(){grvList.UnselectRows();})", true);
                SendMail("luuvanquyet2612@gmail.com");
                loadData();
                txtNgayKhaoSat.Value = "";
            }
        }
        catch (Exception)
        {
            alert.alert_Error(Page, "Đã có lỗi xảy ra!", "Vui lòng liên hệ IT");
        }
    }

    protected void btnXem_Click(object sender, EventArgs e)
    {
        try
        {
            List<object> ds_Checked = grvList.GetSelectedFieldValues(new string[] { "hocsinh_id" });
            if (ds_Checked.Count() == 0)
            {
                alert.alert_Warning(Page, "Bạn chưa chọn học sinh!", "");
            }
            else if (ds_Checked.Count > 1)
            {
                alert.alert_Warning(Page, "Chỉ được chọn 1 học sinh để xem!", "");
            }
            else
            {
                Session["_idHocSinh"] = Convert.ToInt32(ds_Checked[0]);
                var getDetail = (from hs in db.tbWebsite_DangKiTuyenSinhs
                                 where hs.hocsinh_id == Convert.ToInt32(ds_Checked[0])
                                 select hs).Single();
                txtHoTenHS.Text = getDetail.hocsinh_name;
                txtNgaySinh.Text = getDetail.hocsinh_dateofbirth.Value.ToString("yyyy-MM-dd").Replace(' ', 'T');
                txtQuocTich.Text = getDetail.hocsinh_quoctich;
                ddlLop.SelectedValue = getDetail.hocsinh_dangki_class;
                ddlCoSo.SelectedValue = getDetail.hocsinh_cosodangky + "";
                txtHoTenBa.Text = getDetail.hocsinh_phuhuynh_name;
                txtSDTBa.Text = getDetail.hocsinh_phone;
                txtEmailBa.Text = getDetail.hocsinh_email;
                txtHoTenMe.Text = getDetail.hocsinh_tenme;
                txtSDTMe.Text = getDetail.hocsinh_sdtme;
                txtEmailMe.Text = getDetail.hocsinh_emailme;
                txtDiaChi.Text = getDetail.hocsinh_address;
                if (getDetail.chedo_id != null)
                    txtCheDo.Value = getDetail.chedo_id + "";
                else
                    txtCheDo.Value = "0";
                txtLyDoUuTien.Value = getDetail.hocsinh_lydo_uutien;
                txtGhiChu.Value = getDetail.hocsinh_ghichu;
                txtNguonDangKy.Value = getDetail.hocsinh_nguondangky;
                ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupControl.Show();setChecked();", true);

            }
        }
        catch (Exception)
        {
            alert.alert_Error(Page, "Đã có lỗi xảy ra!", "Vui lòng liên hệ IT");
        }
    }
    public bool CheckExistHocSinh(DateTime ns, string hocsinh_name)
    {
        bool isExist = false;
        var hocsinh = (from hsdk in db.tbWebsite_DangKiTuyenSinhs
                       where hsdk.hocsinh_dateofbirth.Value.Day == ns.Day
                       && hsdk.hocsinh_dateofbirth.Value.Month == ns.Month
                       && hsdk.hocsinh_dateofbirth.Value.Year == ns.Year
                       && hsdk.hocsinh_name.ToLower() == hocsinh_name.ToLower()
                       select hsdk).FirstOrDefault();
        if (hocsinh != null)
            isExist = true;
        return isExist;
    }
    protected void btnImport_Excel_Click(object sender, EventArgs e)
    {
        if (!fuUpload.HasFile)
            alert.alert_Warning(Page, "Chưa chọn file!", "");
        else
        {
            string fileName = fuUpload.FileName;
            string ext = Path.GetExtension(fileName);

            if (ext.ToLower() == ".xls" || ext.ToLower().Equals(".xlsx"))
            {
                string path = string.Concat(Server.MapPath("~/Excel Files/" + fileName));

                //Nếu trong thư mục trùng file (name) thì xóa và lưu file excel mới
                if (File.Exists(path))
                    File.Delete(path);
                fuUpload.SaveAs(path);

                cls_ExcelApiTest eat = new cls_ExcelApiTest(path);
                eat.OpenExcel();
                int rowCount = eat.GetRowCount("Sheet1");// Sheet phải có tên là Sheet1
                try
                {
                    for (int i = 4; i <= rowCount; i++) // i = 4 tùy thuộc vào hàng nhận giá trị đầu tiên trong File Excel
                    {
                        string ngaysinh = eat.GetCellData("Sheet1", "Ngày sinh", i);
                        string ngaydangky = eat.GetCellData("Sheet1", "Ngày đăng ký", i).Trim();
                        if (ngaysinh != "end") // chưa phải là dòng cuối cùng (không có dữ liệu) nhưng vẫn kẻ ô 
                        {
                            //lấy giá trị từng cells trong file excel bởi column name
                            string hocsinh_hoten = eat.GetCellData("Sheet1", "Họ tên học sinh", i);
                            DateTime hocsinh_ngaysinh = Convert.ToDateTime(ngaysinh);


                            if (CheckExistHocSinh(hocsinh_ngaysinh, hocsinh_hoten)) // check trùng học sinh
                            {
                                continue;
                            }
                            else
                            {
                                string hocsinh_lopdangky = eat.GetCellData("Sheet1", "Lớp đăng kí học", i);
                                string hocsinh_tenme = eat.GetCellData("Sheet1", "Họ tên Mẹ", i);
                                string hocsinh_tenba = eat.GetCellData("Sheet1", "Họ tên Ba", i);
                                string hocsinh_sdtba = eat.GetCellData("Sheet1", "Số ĐT Ba", i);
                                string hocsinh_sdtme = eat.GetCellData("Sheet1", "Số ĐT Mẹ", i);
                                string hocsinh_emailba = eat.GetCellData("Sheet1", "Email Ba", i);
                                string hocsinh_emailme = eat.GetCellData("Sheet1", "Email Mẹ", i);
                                string hocsinh_noio = eat.GetCellData("Sheet1", "Địa chỉ", i);
                                string hocsinh_ghichu = eat.GetCellData("Sheet1", "Ghi chú", i);


                                //lưu dữ liệu học sinh đăng ký
                                tbWebsite_DangKiTuyenSinh hsdk = new tbWebsite_DangKiTuyenSinh();
                                hsdk.hocsinh_name = hocsinh_hoten;
                                hsdk.hocsinh_dateofbirth = hocsinh_ngaysinh;
                                hsdk.hocsinh_dangki_class = hocsinh_lopdangky;
                                hsdk.hocsinh_tenme = hocsinh_tenme;
                                hsdk.hocsinh_phuhuynh_name = hocsinh_tenba;
                                hsdk.hocsinh_phone = hocsinh_sdtba;
                                hsdk.hocsinh_sdtme = hocsinh_sdtme;
                                hsdk.hocsinh_address = hocsinh_noio;
                                hsdk.hocsinh_email = hocsinh_emailba;
                                hsdk.hocsinh_emailme = hocsinh_emailme;
                                if (ngaydangky != "")
                                {
                                    DateTime hocsinh_ngaydangky = Convert.ToDateTime(ngaydangky);
                                    hsdk.hocsinh_ngaydangky = hocsinh_ngaydangky;
                                }
                                else
                                {
                                }
                                hsdk.hocsinh_quoctich = "Việt Nam";
                                hsdk.hocsinh_trangthai_dangky = 0;
                                hsdk.username_id = _idUser;
                                if (_idUser == 154)
                                {
                                    hsdk.hocsinh_cosodangky = 1;
                                }
                                else if (_idUser == 155)
                                {
                                    hsdk.hocsinh_cosodangky = 2;
                                }
                                else if (_idUser == 156)
                                {
                                    hsdk.hocsinh_cosodangky = 3;
                                }
                                else if (_idUser == 157)
                                {
                                    hsdk.hocsinh_cosodangky = 4;
                                }
                                hsdk.hocsinh_noidangky = "Văn phòng";
                                hsdk.hocsinh_danhsachcho_khaosat = false;
                                hsdk.hocsinh_ghichu = hocsinh_ghichu;
                                hsdk.hocsinh_tinhtrang = 3; //0 lưu trữ, 1 đang học, 2 đang chờ, 3 chờ khảo sát, 4 đã khảo sát, 5 khảo sát đạt,6 đăng ký mới, 7 đã đóng tiền -1 đã nghỉ, -2 khảo sát không đạt
                                db.tbWebsite_DangKiTuyenSinhs.InsertOnSubmit(hsdk);
                                db.SubmitChanges();
                            }
                        }
                        else
                            break;
                    }
                    eat.CloseExcel();
                    alert.alert_Success(Page, "Lưu dữ liệu thành công!", "");
                }
                catch
                {
                    alert.alert_Error(Page, "Có lỗi!", "");
                    eat.CloseExcel();
                }
                if (_idUser == 154)
                {
                    loadDataCoSo(1);
                }
                else if (_idUser == 155)
                {
                    loadDataCoSo(2);
                }
                else if (_idUser == 156)
                {
                    loadDataCoSo(3);
                }
                else if (_idUser == 157)
                {
                    loadDataCoSo(4);
                    if (ddlSearch.SelectedValue != "0")
                        filterDataByClass(ddlSearch.SelectedValue);
                }
                else
                {
                    loadData();
                }
            }
            else
                alert.alert_Warning(Page, "File chọn không đúng định dạng!", "");
        }
    }

    //lọc dữ liệu theo từng khối lớp cho cơ sở liên cấp
    private void filterDataByClass(string class_name)
    {

        // load data đổ vào var danh sách
        var getData = from nc in db.tbWebsite_DangKiTuyenSinhs
                      where nc.hocsinh_trangthai_dangky >= 0 //chưa tư vấn hoặc đã đang tư vấn
                      && nc.hocsinh_trangthai_dangky < 2
                      //&& nc.hocsinh_danhsachcho_khaosat == false // chưa thêm vào ds khảo sát
                      && nc.hocsinh_cosodangky == 4
                      && nc.hocsinh_dangki_class == class_name
                      orderby nc.hocsinh_id descending
                      select new
                      {
                          nc.hocsinh_id,
                          nc.hocsinh_name,
                          hocsinh_namsinh = nc.hocsinh_dateofbirth,
                          hocsinh_tuoi = DateTime.Now.Year - Convert.ToDateTime(nc.hocsinh_dateofbirth).Year + " tuổi",
                          nc.hocsinh_phuhuynh_name,
                          nc.hocsinh_tenme,
                          nc.hocsinh_address,
                          nc.hocsinh_phone,
                          nc.hocsinh_noidangky,
                          nc.hocsinh_dangki_class,
                          nc.hocsinh_sdtme,
                          username_fullname = (from u in db.admin_Users
                                               where u.username_id == nc.username_id
                                               select u).FirstOrDefault().username_fullname,
                          trangthai = nc.hocsinh_trangthai_dangky == 0 ? "Chưa TV" : nc.hocsinh_trangthai_dangky == 1 ? "Đã TV" : "",
                          cosodangky = nc.hocsinh_cosodangky == 1 ? "CS1" : nc.hocsinh_cosodangky == 2 ? "CS2" : nc.hocsinh_cosodangky == 3 ? "CS3" : nc.hocsinh_cosodangky == 4 ? "TH-THCS-THPT" : "",
                          count = (from dk in db.tbWebsite_DangKiTuyenSinhs
                                   join tv in db.tbWebsite_TuVan_DangKiTuyenSinhs on dk.hocsinh_id equals tv.hocsinh_dangky_id
                                   where tv.hocsinh_dangky_id == nc.hocsinh_id
                                   select tv).Any() ? "" + db.tbWebsite_TuVan_DangKiTuyenSinhs.Where(x => x.hocsinh_dangky_id == nc.hocsinh_id).GroupBy(x => x.tuvan_thutu).Count() : "",
                          chedo_name = (from cd in db.tbDangky_CheDoUuTiens
                                        where cd.chedo_id == nc.chedo_id
                                        select cd).Any() ? "" + db.tbDangky_CheDoUuTiens.Where(x => x.chedo_id == nc.chedo_id).FirstOrDefault().chedo_name : "",
                      };
        // đẩy dữ liệu vào gridivew
        grvList.DataSource = getData;
        grvList.DataBind();
        //get ds chế độ ưu tiên
        rpCheDoUuTien.DataSource = from c in db.tbDangky_CheDoUuTiens
                                   orderby c.chedo_id descending
                                   select c;
        rpCheDoUuTien.DataBind();
    }
    protected void ddlSearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlSearch.SelectedValue == "0")
            alert.alert_Warning(Page, "Vui lòng chọn lớp để lọc!", "");
        else
            filterDataByClass(ddlSearch.SelectedValue);
    }
    protected void btnChuyenDanhSach_ServerClick(object sender, EventArgs e)
    {
        try
        {
            List<object> ds_Checked = grvList.GetSelectedFieldValues(new string[] { "hocsinh_id" });
            if (ds_Checked.Count() == 0)
            {
                alert.alert_Warning(Page, "Bạn chưa chọn học sinh!", "");
                ddlChuyenDanhSach.SelectedValue = "";
            }
            else if (ddlChuyenDanhSach.SelectedValue == "")
            {
                alert.alert_Warning(Page, "Bạn chưa chọn danh sách!", "");
            }
            else
            {
                foreach (var item in ds_Checked)
                {
                    tbWebsite_DangKiTuyenSinh find = db.tbWebsite_DangKiTuyenSinhs.Where(x => x.hocsinh_id == Convert.ToInt32(item)).FirstOrDefault();
                    find.hocsinh_tinhtrang = Convert.ToInt32(ddlChuyenDanhSach.SelectedValue);
                    db.SubmitChanges();
                }

                ddlChuyenDanhSach.SelectedValue = "";
                ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Chuyển danh sách thành công!','','success').then(function(){grvList.Refresh();grvList.UnselectRows();})", true);
            }
        }
        catch (Exception)
        {
            alert.alert_Error(Page, "Đã có lỗi xảy ra!", "Vui lòng liên hệ IT");
        }
    }
}