﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_DanhSachHoanThanhDangKy.aspx.cs" Inherits="admin_page_module_function_module_DangKyTuyenSinh_module_DanhSachHoanThanhDangKy" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <style>
        .check_radio {
            cursor: pointer;
            padding-left: 10px;
        }

        .chedo_mota {
            margin-left: 50px;
        }

        label {
            display: flex;
        }

        .disable_Active {
            color: #d23e2f;
        }

        label.show_input {
            display: none;
        }
    </style>
    <script>
        function setChecked() {
            var id = document.getElementById('<%=txtCheDo.ClientID%>').value;
            if (id != "0") {
                document.getElementById(id).checked = true;
            }
            else { }
            //checked nguồn đăng ký
            var nguondangky = document.getElementById('<%=txtNguonDangKy.ClientID%>').value;
            let radios = document.getElementsByName("rdNguonDangKy");
            for (let i = 0, length = radios.length; i < length; i++) {
                if (radios[i].value == nguondangky) {
                    radios[i].checked = true;
                    break;
                }
            }

        }
        //func chọn nguồn đăng kí
        function check_NguonDangKy(nguon_id) {
            document.getElementById('<%=txtNguonDangKy.ClientID%>').value = document.getElementById(nguon_id).value;
        }
        function check_CheDo(chedo_id) {
            document.getElementById('<%=txtCheDo.ClientID%>').value = chedo_id;
            document.getElementById('<%=txtLyDoUuTien.ClientID%>').focus();
        }
        function showMoTa(chedo_id) {
            document.getElementById('chedo_' + chedo_id).style.display = "block";
        }
        function hiddenMoTa() {
            var ele = document.getElementsByClassName('chedo_mota');
            for (var i = 0; i < ele.length; i++) {
                ele[i].style.display = "none";
            }
        }
        function formatCurrency() {
            let element = document.getElementById("txtTienCocNhap");
            if (element.value.trim() != "") {
                let value_format = parseFloat(element.value.replace(/,/g, ""))
                    .toFixed(0)
                    .toString()
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                element.value = value_format;
                document.getElementById('<%=txtTienCoc.ClientID%>').value = value_format.replace(/,/g, "");
            }
            else {
                document.getElementById('<%=txtTienCoc.ClientID%>').value = 0;
            }
        };
        //set format currency
        function setFormatCurrency() {
            //set format tiền
            let element = document.getElementById("txtTienCocNhap");
            var tiencoc = document.getElementById('<%=txtTienCoc.ClientID%>').value;
            console.log(tiencoc);
            let value_format = parseFloat(tiencoc.replace(/,/g, ""))
                .toFixed(0)
                .toString()
                .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            element.value = value_format;
            //set checked chương trình ưu đãi
            var cdud = document.getElementById('<%=txtCTUD_ID.ClientID%>').value;
            let radios = document.getElementsByName("check_ctud");
            for (let i = 0, length = radios.length; i < length; i++) {
                if (radios[i].value == cdud) {
                    radios[i].checked = true;
                    break;
                }
            }
            //set style nếu ctud khác
            if (document.getElementById('<%= txtNoiDung.ClientID%>').value != "") {
                document.getElementById('noidunguudai_0').style.display = "block";
            }
        }
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
        //func check null cập nhật đóng tiền 
        function checkNULL() {
            var noidunguudai = document.getElementById('<%= txtNoiDung.ClientID%>');
            var tiencoc = document.getElementById("txtTienCocNhap");
            var uudai_id = document.getElementById('<%= txtCTUD_ID.ClientID%>');
            //if (uudai_id.value == "") {
            //    swal('Vui lòng chọn chương trình ưu đãi!', '', 'warning');
            //    return false;
            //}
            //if (tiencoc.value == "") {
            //    swal('Vui lòng nhập tiền cọc!', '', 'warning').then(function () { tiencoc.focus(); });
            //    return false;
            //}
            if (uudai_id.value == "0" && noidunguudai.value == "") {
                swal('Vui lòng nhập nội dung ưu đãi!', '', 'warning').then(function () { noidunguudai.focus(); });
                return false;
            }
            var checkDatCoc = document.getElementById('<%=chkDatCoc.ClientID%>')
            if (checkDatCoc.checked == true && tiencoc.value == "") {
                swal('Vui lòng nhập tiền cọc!', '', 'warning').then(function () { tiencoc.focus(); });
                return false;
            }
            return true;
        }
        function checkCTUD(id) {
            document.getElementById('<%=txtCTUD_ID.ClientID%>').value = id;
            if (id == "0") {
                document.getElementById('noidunguudai_0').style.display = "block";
                document.getElementById('<%= txtNoiDung.ClientID%>').focus();
            }
            else {
                document.getElementById('noidunguudai_0').style.display = "none";
            }
        }
    </script>
    <div class="card card-block">
        <div class="title__form text-md-center">
            <h3 class="text-uppercase">Danh sách hoàn thành đăng ký</h3>
        </div>
        <div class="form-group row">
            <div class="col-sm-10">
                <asp:UpdatePanel ID="udButton" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnXem" runat="server" OnClick="btnXem_Click" Text="Xem" CssClass="btn btn-primary" />
                        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modal_ChuyenDanhSach">Chuyển danh sách</a>
                        <asp:Button ID="btnCapNhatDatCoc" runat="server" OnClick="btnCapNhatDatCoc_Click" Text="Cập nhật" CssClass="btn btn-primary" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <div class="form-group table-responsive">
            <dx:ASPxGridView ID="grvList" runat="server" ClientInstanceName="grvList" KeyFieldName="hocsinh_id" Width="100%">
                <Columns>
                    <dx:GridViewCommandColumn ShowSelectCheckbox="True" SelectAllCheckboxMode="Page" VisibleIndex="0" Width="5%">
                    </dx:GridViewCommandColumn>
                    <dx:GridViewDataColumn Caption="Họ tên HS" FieldName="hocsinh_name" HeaderStyle-HorizontalAlign="Center" Width="20%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Năm sinh" FieldName="hocsinh_namsinh" HeaderStyle-HorizontalAlign="Center" Width="5%"></dx:GridViewDataColumn>
                    <%--<dx:GridViewDataColumn Caption="Tuổi" FieldName="hocsinh_tuoi" HeaderStyle-HorizontalAlign="Center" Width="8%"></dx:GridViewDataColumn>--%>
                    <%--<dx:GridViewDataColumn Caption="Lớp đk" FieldName="hocsinh_dangki_class" HeaderStyle-HorizontalAlign="Center" Width="5%"></dx:GridViewDataColumn>--%>
                    <dx:GridViewDataColumn Caption="Họ tên ba" FieldName="hocsinh_phuhuynh_name" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="SĐT ba" FieldName="hocsinh_phone" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Họ tên mẹ" FieldName="hocsinh_tenme" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="SĐT mẹ" FieldName="hocsinh_sdtme" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Địa chỉ" FieldName="hocsinh_address" HeaderStyle-HorizontalAlign="Center" Width="20%"></dx:GridViewDataColumn>
                    <%--<dx:GridViewDataColumn Caption="Nơi ĐK" FieldName="hocsinh_noidangky" HeaderStyle-HorizontalAlign="Center" Width="5%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="CSDK" FieldName="cosodangky" HeaderStyle-HorizontalAlign="Center" Width="5%"></dx:GridViewDataColumn>--%>
                    <%--<dx:GridViewDataColumn Caption="Người ĐK" FieldName="username_fullname" HeaderStyle-HorizontalAlign="Center" Width="5%"></dx:GridViewDataColumn>--%>
                    <dx:GridViewDataColumn Caption="Số lần TV" FieldName="trangthai" HeaderStyle-HorizontalAlign="Center" Width="3%">
                        <DataItemTemplate>
                            <p><%#Eval("count") %></p>
                        </DataItemTemplate>
                    </dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Trạng thái" FieldName="chedo_name" HeaderStyle-HorizontalAlign="Center" Width="3%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Ngày đóng tiền" FieldName="dongtien_ngaydangky" HeaderStyle-HorizontalAlign="Center" Width="5%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Tiền cọc" FieldName="dongtien_tiencoc" HeaderStyle-HorizontalAlign="Center" Width="5%">
                        <DataItemTemplate>
                            <%#Eval("dongtien_tiencoc") %> VNĐ
                        </DataItemTemplate>
                    </dx:GridViewDataColumn>
                </Columns>
                <%--<ClientSideEvents RowDblClick="btnChiTiet" />--%>
                <SettingsSearchPanel Visible="true" />
                <SettingsBehavior AllowFocusedRow="true" />
                <SettingsText EmptyDataRow="Trống" SearchPanelEditorNullText="Gỏ từ cần tìm kiếm và enter..." />
                <SettingsLoadingPanel Text="Đang tải..." />
                <SettingsPager PageSize="20" Summary-Text="Trang {0} / {1} ({2} trang)"></SettingsPager>
            </dx:ASPxGridView>
        </div>
    </div>
    <%--popup chi tiết--%>
    <dx:ASPxPopupControl ID="popupChiTiet" runat="server" Width="1000px" Height="550px" CloseAction="CloseButton" ShowCollapseButton="True" ShowMaximizeButton="True" ScrollBars="Auto" CloseOnEscape="true" Modal="True"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" ClientInstanceName="popupChiTiet" ShowFooter="true"
        HeaderText="THÔNG TIN CHI TIẾT HỌC SINH" AllowDragging="True" PopupAnimationType="Fade" EnableViewState="False" AutoUpdatePosition="true" ClientSideEvents-CloseUp="function(s,e){grvList.Refresh();}">
        <ContentCollection>
            <dx:PopupControlContentControl runat="server">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="popup-main">
                            <div class="div_content row ml-0 mt-0">
                                <p class="title_info" style="color: #1058dc; font-size: 20px"><b>Thông tin của HS:</b></p>
                                <div class="col-12">
                                    <div class="col-12 form-group">
                                        <label class="col-2 form-control-label">Họ tên <span class="field_important">*</span>:</label>
                                        <div class="col-10">
                                            <asp:TextBox ID="txtHoTenHS" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%"> </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-12 form-group">
                                        <label class="col-2 form-control-label">Ngày sinh <span class="field_important">*</span>:</label>
                                        <div class="col-10">
                                            <input type="date" runat="server" id="txtDoB" class="form-control boxed" value="" style="width: 95%" />
                                        </div>
                                    </div>
                                    <div class="col-12 form-group">
                                        <label class="col-2 form-control-label">Quốc tịch:</label>
                                        <div class="col-10">
                                            <asp:TextBox ID="txtQuocTich" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%"> </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-12 form-group">
                                        <div class="col-6">
                                            <label class="col-4 form-control-label">Lớp đăng ký<span class="field_important">*</span>:</label>
                                            <div class="col-6">
                                                <asp:DropDownList runat="server" ID="ddlLop" class=" form-control form-select_class" Width="95%">
                                                    <asp:ListItem Value="0" Text="Chọn lớp" />
                                                    <asp:ListItem Value="Mầm non" Text="Mầm non" />
                                                    <asp:ListItem Value="Lớp 1" Text="Lớp 1" />
                                                    <asp:ListItem Value="Lớp 2" Text="Lớp 2" />
                                                    <asp:ListItem Value="Lớp 3" Text="Lớp 3" />
                                                    <asp:ListItem Value="Lớp 4" Text="Lớp 4" />
                                                    <asp:ListItem Value="Lớp 5" Text="Lớp 5" />
                                                    <asp:ListItem Value="Lớp 6" Text="Lớp 6" />
                                                    <asp:ListItem Value="Lớp 7" Text="Lớp 7" />
                                                    <asp:ListItem Value="Lớp 8" Text="Lớp 8" />
                                                    <asp:ListItem Value="Lớp 9" Text="Lớp 9" />
                                                    <asp:ListItem Value="Lớp 10" Text="Lớp 10" />
                                                    <asp:ListItem Value="Lớp 11" Text="Lớp 11" />
                                                    <asp:ListItem Value="Lớp 12" Text="Lớp 12" />
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <label class="col-4 form-control-label">Cơ sở đăng ký:</label>
                                            <div class="col-8">
                                                <asp:DropDownList runat="server" ID="ddlCoSo" class=" form-control form-select_class" Width="90%">
                                                    <asp:ListItem Value="0" Text="Chọn cơ sở" />
                                                    <asp:ListItem Value="1" Text="Cơ sở 1" />
                                                    <asp:ListItem Value="2" Text="Cơ sở 2" />
                                                    <asp:ListItem Value="3" Text="Cơ sở 3" />
                                                    <asp:ListItem Value="4" Text="Trường liên cấp TH-THCS-THPT" />
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 form-group">
                                        <label class="col-2 form-control-label">Độ ưu tiên:</label>
                                        <div class="col-10">
                                            <asp:Repeater runat="server" ID="rpCheDoUuTien">
                                                <ItemTemplate>
                                                    <label>
                                                        <input type="radio" name="rdCheDo" id="<%#Eval("chedo_id") %>" />
                                                        <%--<span class="check_radio"></span>--%>
                                                        <p class="pr-name check_radio" onmousemove="showMoTa('<%#Eval("chedo_id") %>')" onmouseout="hiddenMoTa()"><%#Eval("chedo_name") %></p>
                                                        <p class="chedo_mota" id="chedo_<%#Eval("chedo_id") %>" style="display: none"><%#Eval("chedo_tomtat") %></p>
                                                    </label>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </div>
                                        <label class="col-2 form-control-label">Lý do ưu tiên:</label>
                                        <div class="col-10">
                                            <textarea class="form-control" runat="server" id="txtLyDoUuTien" rows="3" style="width: 95%;"></textarea>
                                        </div>
                                        <input type="text" name="name" value="0" id="txtCheDo" runat="server" hidden />
                                    </div>
                                </div>
                            </div>
                            <div class="row ml-0">
                                <p class="title_info" style="color: #1058dc; font-size: 20px"><b>Thông tin của Phụ huynh:</b></p>
                                <div class="col-6">
                                    <div class="col-12 form-group">
                                        <label class="col-2 form-control-label">Họ tên ba:</label>
                                        <div class="col-10">
                                            <asp:TextBox ID="txtHoTenBa" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%"> </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-12 form-group">
                                        <label class="col-2 form-control-label">SĐT ba:</label>
                                        <div class="col-10">
                                            <asp:TextBox ID="txtSDTBa" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%" onkeypress="return isNumberKey(event)"> </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-12 form-group">
                                        <label class="col-2 form-control-label">Email ba:</label>
                                        <div class="col-10">
                                            <asp:TextBox ID="txtEmailBa" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%"> </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="col-12 form-group">
                                        <label class="col-2 form-control-label">Họ tên mẹ:</label>
                                        <div class="col-10">
                                            <asp:TextBox ID="txtHoTenMe" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%"> </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-12 form-group">
                                        <label class="col-2 form-control-label">SĐT mẹ:</label>
                                        <div class="col-10">
                                            <asp:TextBox ID="txtSDTMe" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%" onkeypress="return isNumberKey(event)"> </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-12 form-group">
                                        <label class="col-2 form-control-label">Email mẹ:</label>
                                        <div class="col-10">
                                            <asp:TextBox ID="txtEmailMe" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%"> </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 form-group">
                                <label class="col-2 form-control-label">Địa chỉ:</label>
                                <div class="col-10">
                                    <asp:TextBox ID="txtDiaChi" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%"> </asp:TextBox>
                                </div>
                            </div>
                            <div class="col-12 form-group">
                                <label class="col-2 form-control-label">Nội dung ghi chú:</label>
                                <div class="col-10">
                                    <textarea class="form-control" runat="server" id="txtGhiChu" rows="3" style="width: 95%;"></textarea>
                                </div>
                            </div>
                            <div class="col-12 form-group">
                                <label class="col-2 form-control-label">Nguồn đăng ký:</label>
                                <div class="col-10">
                                    <span style="display: flex">
                                        <input type="radio" name="rdNguonDangKy" id="dentruongtructiep" onclick="check_NguonDangKy(this.id)" value="Đến trường trực tiếp" />
                                        <label class="pr-name check_radio" for="dentruongtructiep">Đến trường trực tiếp</label>
                                    </span>
                                    <span style="display: flex">
                                        <input type="radio" name="rdNguonDangKy" id="zalo" onclick="check_NguonDangKy(this.id)" value="Zalo" />
                                        <label class="pr-name check_radio" for="zalo">Zalo</label>
                                    </span>
                                    <span style="display: flex">
                                        <input type="radio" name="rdNguonDangKy" id="goidientructiep" onclick="check_NguonDangKy(this.id)" value="Gọi điện trực tiếp" />
                                        <label class="pr-name check_radio" for="goidientructiep">Gọi điện trực tiếp</label>
                                    </span>
                                    <span style="display: flex">
                                        <input type="radio" name="rdNguonDangKy" id="facebook" onclick="check_NguonDangKy(this.id)" value="Facebook" />
                                        <label class="pr-name check_radio" for="facebook">Facebook</label>
                                    </span>
                                    <span style="display: flex">
                                        <input type="radio" name="rdNguonDangKy" id="nguoiquengioithieu" onclick="check_NguonDangKy(this.id)" value="Người quen giới thiệu" />
                                        <label class="pr-name check_radio" for="nguoiquengioithieu">Người quen giới thiệu</label>
                                    </span>
                                    <span style="display: flex">
                                        <input type="radio" name="rdNguonDangKy" id="website" onclick="check_NguonDangKy(this.id)" value="Website" />
                                        <label class="pr-name check_radio" for="website">Website</label>
                                    </span>
                                </div>
                                <input type="text" name="name" value="" id="txtNguonDangKy" runat="server" hidden="hidden" />
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <FooterContentTemplate>
        </FooterContentTemplate>
        <ContentStyle>
            <Paddings PaddingBottom="0px" />
        </ContentStyle>
    </dx:ASPxPopupControl>
    <%--modal chuyển danh sách--%>
    <div class="modal fade" id="modal_ChuyenDanhSach" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <span class="title_modal">Chuyển danh sách
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </span>
                </div>
                <div class="modal-body" style="display: flex; justify-content: center;">
                    <span>Danh sách chuyển đến:</span>
                    <asp:DropDownList runat="server" ID="ddlChuyenDanhSach" class=" form-control form-select_class ml-2" Width="350px">
                        <asp:ListItem Value="" Text="Chọn danh sách" />
                        <asp:ListItem Value="0" Text="Danh sách lưu trữ" />
                        <asp:ListItem Value="1" Text="Danh sách đang học" />
                        <asp:ListItem Value="2" Text="Danh sách chờ" />
                        <asp:ListItem Value="3" Text="Danh sách chờ khảo sát" />
                        <asp:ListItem Value="4" Text="Danh sách khảo sát" />
                        <asp:ListItem Value="5" Text="Danh sách khảo sát đạt" />
                        <asp:ListItem Value="6" Text="Danh sách đăng ký mới" />
                        <asp:ListItem Value="-1" Text="Danh sách đã nghỉ/không học" />
                        <asp:ListItem Value="-2" Text="Danh sách khảo sát không đạt" />
                    </asp:DropDownList>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-primary" id="btnChuyenDanhSach" runat="server" onserverclick="btnChuyenDanhSach_ServerClick">Lưu</a>
                </div>
            </div>
        </div>
    </div>
    <%--popup đóng tiền--%>
    <dx:ASPxPopupControl ID="popupDongTien" runat="server" Width="800px" Height="500px" CloseAction="CloseButton" ShowCollapseButton="True" ShowMaximizeButton="True" ScrollBars="Auto" CloseOnEscape="true" Modal="True"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" ClientInstanceName="popupDongTien" ShowFooter="true"
        HeaderText="THÔNG TIN ĐĂNG KÝ ĐÓNG TIỀN" AllowDragging="True" PopupAnimationType="Fade" EnableViewState="False" AutoUpdatePosition="true" ClientSideEvents-CloseUp="function(s,e){grvList.Refresh();}">
        <ContentCollection>
            <dx:PopupControlContentControl runat="server">
                <asp:UpdatePanel ID="udPopup" runat="server">
                    <ContentTemplate>
                        <div class="popup-main">
                            <div class="div_content row ml-0 mt-0">
                                <div class="col-12">
                                    <div class="col-12 form-group">
                                        <h4 class="mb-3">Chương trình ưu đãi:</h4>
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th style="width: 50px">#</th>
                                                    <th>Tên ưu đãi</th>
                                                    <th>Nội dung ưu đãi</th>
                                                    <th style="width: 120px">Thời hạn ưu đãi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater runat="server" ID="rpCTUD">
                                                    <ItemTemplate>
                                                        <tr class="<%#Eval("active") %>">
                                                            <td style="text-align: center">
                                                                <label class="<%#Eval("show") %>">
                                                                    <input type="radio" name="check_ctud" id="<%#Eval("ctud_id") %>" onclick="checkCTUD(this.id)" value="<%#Eval("ctud_id") %>" />
                                                                    <%--<input class="checkbox" type="radio" name="check_ctud" id="<%#Eval("ctud_id") %>" onclick="checkCTUD(this.id)" />--%>
                                                                    <span></span>
                                                                </label>
                                                            </td>
                                                            <td>
                                                                <p class="pr-name"><%#Eval("ctud_thoigian") %></p>
                                                            </td>
                                                            <td>
                                                                <p class="pr-name"><%#Eval("ctud_noidung") %></p>
                                                            </td>
                                                            <td>
                                                                <p class="pr-name"><%#Eval("ctud_thoigianbatdau","{0:dd/MM/yyyy}") %>-<%#Eval("ctud_thoigianketthuc","{0:dd/MM/yyyy}") %></p>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                <tr>
                                                    <td style="text-align: center">
                                                        <label>
                                                            <input type="radio" name="check_ctud" id="0" onclick="checkCTUD(this.id)" value="0" />
                                                            <%--<input class="checkbox" type="radio" name="check_ctud" id="0" onclick="checkCTUD(this.id)" />--%>
                                                            <span></span>
                                                        </label>
                                                    </td>
                                                    <td>
                                                        <p class="pr-name">Khác</p>
                                                    </td>
                                                    <td colspan="2">
                                                        <div id="noidunguudai_0" style="display: none">
                                                            <textarea class="form-control mb-2" id="txtNoiDung" runat="server" rows="3" style="width: 95%;"></textarea>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <div>
                                            <input type="text" value="" id="txtCTUD_ID" runat="server" />
                                        </div>
                                    </div>
                                    <div class="col-12 form-group">
                                        <label>
                                            <input class="checkbox" type="checkbox" id="chkDatCoc" runat="server" />
                                            <span></span>
                                            <p class="pr-name">Đã đặt cọc</p>
                                        </label>
                                        <label class="col-2 form-control-label">Tiền cọc:</label>
                                        <div class="col-10" style="display: flex">
                                            <input type="text" value="" id="txtTienCocNhap" class="form-control" style="width: 45%" autocomplete="off" onkeypress="return isNumberKey(event)" onkeyup="formatCurrency()" /><span style="padding: 10px 0 0 5px">VND</span>
                                            <input type="text" value="" id="txtTienCoc" runat="server" class="form-control" autocomplete="off" style="display: none" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <FooterContentTemplate>
            <div class="mar_but button">
                <asp:Button ID="btnCapNhat" runat="server" ClientIDMode="Static" Text="Cập nhật" CssClass="btn btn-primary" OnClientClick="return checkNULL()" OnClick="btnCapNhat_Click" />
            </div>
        </FooterContentTemplate>
        <ContentStyle>
            <Paddings PaddingBottom="0px" />
        </ContentStyle>
    </dx:ASPxPopupControl>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

