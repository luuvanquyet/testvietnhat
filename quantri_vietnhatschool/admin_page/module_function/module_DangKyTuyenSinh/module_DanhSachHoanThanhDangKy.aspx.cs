﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_DangKyTuyenSinh_module_DanhSachHoanThanhDangKy : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private static int _idUser;
    private string noidung;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["UserName"] != null)
        {
            _idUser = (from u in db.admin_Users
                       where u.username_username == Request.Cookies["UserName"].Value
                       select u).FirstOrDefault().username_id;
            if (_idUser == 154)
            {
                loadDataCoSo(1);
            }
            else if (_idUser == 155)
            {
                loadDataCoSo(2);
            }
            else if (_idUser == 156)
            {
                loadDataCoSo(3);
            }
            else if (_idUser == 157)
            {
                loadDataCoSo(4);
            }
            else
            {
                loadData();
            }
        }
        else
        {
            Response.Redirect("/admin-login");
        }
    }
    private void loadData()
    {

        // load data đổ vào var danh sách
        var getData = from nc in db.tbWebsite_DangKiTuyenSinhs
                      join dt in db.tbWebsite_DangKyDongTiens on nc.hocsinh_id equals dt.hocsinh_dangky_id
                      where nc.hocsinh_tinhtrang == 7 // đã hoàn thành đóng tiền
                      orderby dt.dongtien_id descending
                      select new
                      {
                          nc.hocsinh_id,
                          nc.hocsinh_name,
                          hocsinh_namsinh = nc.hocsinh_dateofbirth,
                          hocsinh_tuoi = DateTime.Now.Year - Convert.ToDateTime(nc.hocsinh_dateofbirth).Year + " tuổi",
                          nc.hocsinh_phuhuynh_name,
                          nc.hocsinh_tenme,
                          nc.hocsinh_address,
                          nc.hocsinh_phone,
                          nc.hocsinh_noidangky,
                          nc.hocsinh_dangki_class,
                          nc.hocsinh_sdtme,
                          username_fullname = (from u in db.admin_Users
                                               where u.username_id == nc.username_id
                                               select u).FirstOrDefault().username_fullname,
                          trangthai = nc.hocsinh_trangthai_dangky == 0 ? "Chưa TV" : nc.hocsinh_trangthai_dangky == 1 && nc.hocsinh_danhsachcho_khaosat == true ? "Chờ KS" : nc.hocsinh_trangthai_dangky == 1 ? "Đã TV" : "",
                          cosodangky = nc.hocsinh_cosodangky == 1 ? "CS1" : nc.hocsinh_cosodangky == 2 ? "CS2" : nc.hocsinh_cosodangky == 3 ? "CS3" : nc.hocsinh_cosodangky == 4 ? "TH-THCS-THPT" : "",
                          count = (from dk in db.tbWebsite_DangKiTuyenSinhs
                                   join tv in db.tbWebsite_TuVan_DangKiTuyenSinhs on dk.hocsinh_id equals tv.hocsinh_dangky_id
                                   where tv.hocsinh_dangky_id == nc.hocsinh_id
                                   select tv).Any() ? "" + db.tbWebsite_TuVan_DangKiTuyenSinhs.Where(x => x.hocsinh_dangky_id == nc.hocsinh_id).GroupBy(x => x.tuvan_thutu).Count() : "",
                          chedo_name = (from cd in db.tbDangky_CheDoUuTiens
                                        where cd.chedo_id == nc.chedo_id
                                        select cd).Any() ? "" + db.tbDangky_CheDoUuTiens.Where(x => x.chedo_id == nc.chedo_id).FirstOrDefault().chedo_name : "",
                          dt.dongtien_ngaydangky,
                          dt.dongtien_tiencoc,

                      };
        // đẩy dữ liệu vào gridivew
        grvList.DataSource = getData;
        grvList.DataBind();
        //get ds chế độ ưu tiên
        rpCheDoUuTien.DataSource = from c in db.tbDangky_CheDoUuTiens
                                   select c;
        rpCheDoUuTien.DataBind();
        //load ds chương trình ưu đãi

        var getCTUD = from ct in db.tbWebsite_ChuongTrinhUuDais
                      where ct.ctud_status == 0
                      select new
                      {
                          ct.ctud_id,
                          ct.ctud_thoigian,
                          ct.ctud_noidung,
                          ct.ctud_thoigianbatdau,
                          ct.ctud_thoigianketthuc,
                          active = Convert.ToDateTime(ct.ctud_thoigianketthuc) >= DateTime.Now.AddDays(-1) ? "" : "disable_Active",
                          show = Convert.ToDateTime(ct.ctud_thoigianketthuc) >= DateTime.Now.AddDays(-1) ? "" : "show_input",
                      };
        rpCTUD.DataSource = getCTUD;
        rpCTUD.DataBind();
    }
    private void loadDataCoSo(int coso)
    {
        // load data đổ vào var danh sách
        var getData = from nc in db.tbWebsite_DangKiTuyenSinhs
                      join dt in db.tbWebsite_DangKyDongTiens on nc.hocsinh_id equals dt.hocsinh_dangky_id
                      where nc.hocsinh_trangthai_dangky == -1//được khảo sát
                      && nc.hocsinh_dongtien == true // đã hoàn thành đóng tiền
                       && nc.hocsinh_cosodangky == coso
                      orderby dt.dongtien_id descending
                      select new
                      {
                          nc.hocsinh_id,
                          nc.hocsinh_name,
                          hocsinh_namsinh = nc.hocsinh_dateofbirth,
                          hocsinh_tuoi = DateTime.Now.Year - Convert.ToDateTime(nc.hocsinh_dateofbirth).Year + " tuổi",
                          nc.hocsinh_phuhuynh_name,
                          nc.hocsinh_tenme,
                          nc.hocsinh_address,
                          nc.hocsinh_phone,
                          nc.hocsinh_noidangky,
                          nc.hocsinh_dangki_class,
                          nc.hocsinh_sdtme,
                          username_fullname = (from u in db.admin_Users
                                               where u.username_id == nc.username_id
                                               select u).FirstOrDefault().username_fullname,
                          trangthai = nc.hocsinh_trangthai_dangky == 0 ? "Chưa TV" : nc.hocsinh_trangthai_dangky == 1 && nc.hocsinh_danhsachcho_khaosat == true ? "Chờ KS" : nc.hocsinh_trangthai_dangky == 1 ? "Đã TV" : "",
                          cosodangky = nc.hocsinh_cosodangky == 1 ? "CS1" : nc.hocsinh_cosodangky == 2 ? "CS2" : nc.hocsinh_cosodangky == 3 ? "CS3" : nc.hocsinh_cosodangky == 4 ? "TH-THCS-THPT" : "",
                          count = (from dk in db.tbWebsite_DangKiTuyenSinhs
                                   join tv in db.tbWebsite_TuVan_DangKiTuyenSinhs on dk.hocsinh_id equals tv.hocsinh_dangky_id
                                   where tv.hocsinh_dangky_id == nc.hocsinh_id
                                   select tv).Any() ? "" + db.tbWebsite_TuVan_DangKiTuyenSinhs.Where(x => x.hocsinh_dangky_id == nc.hocsinh_id).GroupBy(x => x.tuvan_thutu).Count() : "",
                          chedo_name = (from cd in db.tbDangky_CheDoUuTiens
                                        where cd.chedo_id == nc.chedo_id
                                        select cd).Any() ? "" + db.tbDangky_CheDoUuTiens.Where(x => x.chedo_id == nc.chedo_id).FirstOrDefault().chedo_name : "",
                          dt.dongtien_ngaydangky,
                          dt.dongtien_tiencoc,

                      };
        // đẩy dữ liệu vào gridivew
        grvList.DataSource = getData;
        grvList.DataBind();
        //get ds chế độ ưu tiên
        rpCheDoUuTien.DataSource = from c in db.tbDangky_CheDoUuTiens
                                   select c;
        rpCheDoUuTien.DataBind();
        //load ds chương trình ưu đãi

        var getCTUD = from ct in db.tbWebsite_ChuongTrinhUuDais
                      where ct.ctud_status == 0
                      select new
                      {
                          ct.ctud_id,
                          ct.ctud_thoigian,
                          ct.ctud_noidung,
                          ct.ctud_thoigianbatdau,
                          ct.ctud_thoigianketthuc,
                          active = Convert.ToDateTime(ct.ctud_thoigianketthuc) >= DateTime.Now.AddDays(-1) ? "" : "disable_Active",
                          show = Convert.ToDateTime(ct.ctud_thoigianketthuc) >= DateTime.Now.AddDays(-1) ? "" : "show_input",
                      };
        rpCTUD.DataSource = getCTUD;
        rpCTUD.DataBind();
    }
    protected void btnXem_Click(object sender, EventArgs e)
    {
        try
        {
            List<object> ds_Checked = grvList.GetSelectedFieldValues(new string[] { "hocsinh_id" });
            if (ds_Checked.Count() == 0)
            {
                alert.alert_Warning(Page, "Bạn chưa chọn học sinh!", "");
            }
            else if (ds_Checked.Count > 1)
            {
                alert.alert_Warning(Page, "Chỉ được chọn 1 học sinh để xem!", "");
            }
            else
            {
                Session["_id"] = Convert.ToInt32(ds_Checked[0]);
                var getDetail = (from hs in db.tbWebsite_DangKiTuyenSinhs
                                 where hs.hocsinh_id == Convert.ToInt32(ds_Checked[0])
                                 select hs).Single();
                txtHoTenHS.Text = getDetail.hocsinh_name;
                txtDoB.Value = getDetail.hocsinh_dateofbirth.Value.ToString("yyyy-MM-dd").Replace(' ', 'T');
                txtQuocTich.Text = getDetail.hocsinh_quoctich;
                ddlLop.SelectedValue = getDetail.hocsinh_dangki_class;
                ddlCoSo.SelectedValue = getDetail.hocsinh_cosodangky + "";
                txtHoTenBa.Text = getDetail.hocsinh_phuhuynh_name;
                txtSDTBa.Text = getDetail.hocsinh_phone;
                txtEmailBa.Text = getDetail.hocsinh_email;
                txtHoTenMe.Text = getDetail.hocsinh_tenme;
                txtSDTMe.Text = getDetail.hocsinh_sdtme;
                txtEmailMe.Text = getDetail.hocsinh_emailme;
                txtDiaChi.Text = getDetail.hocsinh_address;
                if (getDetail.chedo_id != null)
                    txtCheDo.Value = getDetail.chedo_id + "";
                else
                    txtCheDo.Value = "0";
                txtLyDoUuTien.Value = getDetail.hocsinh_lydo_uutien;
                txtGhiChu.Value = getDetail.hocsinh_ghichu;
                txtNguonDangKy.Value = getDetail.hocsinh_nguondangky;
                ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupChiTiet.Show();setChecked();", true);

            }
        }
        catch (Exception)
        {
            alert.alert_Error(Page, "Đã có lỗi xảy ra!", "Vui lòng liên hệ IT");
        }
    }
    protected void btnChuyenDanhSach_ServerClick(object sender, EventArgs e)
    {
        try
        {
            List<object> ds_Checked = grvList.GetSelectedFieldValues(new string[] { "hocsinh_id" });
            if (ds_Checked.Count() == 0)
            {
                alert.alert_Warning(Page, "Bạn chưa chọn học sinh!", "");
                ddlChuyenDanhSach.SelectedValue = "";
            }
            else if (ddlChuyenDanhSach.SelectedValue == "")
            {
                alert.alert_Warning(Page, "Bạn chưa chọn danh sách!", "");
            }
            else
            {
                foreach (var item in ds_Checked)
                {
                    tbWebsite_DangKiTuyenSinh find = db.tbWebsite_DangKiTuyenSinhs.Where(x => x.hocsinh_id == Convert.ToInt32(item)).FirstOrDefault();
                    find.hocsinh_tinhtrang = Convert.ToInt32(ddlChuyenDanhSach.SelectedValue);
                    db.SubmitChanges();
                }

                ddlChuyenDanhSach.SelectedValue = "";
                ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Chuyển danh sách thành công!','','success').then(function(){grvList.Refresh();grvList.UnselectRows();})", true);
            }
        }
        catch (Exception)
        {
            alert.alert_Error(Page, "Đã có lỗi xảy ra!", "Vui lòng liên hệ IT");
        }
    }

    protected void btnCapNhatDatCoc_Click(object sender, EventArgs e)
    {
        List<object> ds_Checked = grvList.GetSelectedFieldValues(new string[] { "hocsinh_id" });
        if (ds_Checked.Count() == 0)
        {
            alert.alert_Warning(Page, "Bạn chưa chọn học sinh!", "");
        }
        else
        {
            Session["_idHocSinh"] = Convert.ToInt32(ds_Checked[0]);
            var getDetail = (from hs in db.tbWebsite_DangKyDongTiens
                             where hs.hocsinh_dangky_id == Convert.ToInt32(ds_Checked[0])
                             select hs).Single();
            if (getDetail.dongtien_datcoc == true)
                chkDatCoc.Checked = true;
            else
                chkDatCoc.Checked = false;
            txtTienCoc.Value = getDetail.dongtien_tiencoc;
            txtCTUD_ID.Value = getDetail.ctud_id + "";
            if (getDetail.ctud_id == 0)
                txtNoiDung.Value = getDetail.dongtien_chuongtrinh_uudai;
            ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupDongTien.Show();setFormatCurrency();", true);
        }
    }

    protected void btnCapNhat_Click(object sender, EventArgs e)
    {
        int ctud_id = 0;
        if (txtCTUD_ID.Value == "")
            ctud_id = 0;
        else
            ctud_id = Convert.ToInt32(txtCTUD_ID.Value);
        var getDetail_CTUD = (from ct in db.tbWebsite_ChuongTrinhUuDais
                              where ct.ctud_id == ctud_id
                              select ct).SingleOrDefault();
        if (ctud_id == 0)
            noidung = txtNoiDung.Value;
        else
            noidung = getDetail_CTUD.ctud_noidung;
        var checkDK = (from dk in db.tbWebsite_DangKyDongTiens
                       where dk.hocsinh_dangky_id == Convert.ToInt32(Session["_idHocSinh"].ToString())
                       select dk).FirstOrDefault();
        checkDK.ctud_id = ctud_id;
        checkDK.dongtien_chuongtrinh_uudai = noidung;
        checkDK.username_id = _idUser;
        //insert.dongtien_ngaydangky = DateTime.Now;
        checkDK.dongtien_ngaycapnhat = DateTime.Now;
        checkDK.dongtien_tiencoc = txtTienCoc.Value;
        checkDK.dongtien_trangthai = 0;
        if (chkDatCoc.Checked == true)
            checkDK.dongtien_datcoc = true;
        else
            checkDK.dongtien_datcoc = false;
        db.SubmitChanges();
        alert.alert_Success(Page, "Cập nhật thành công!", "");
        popupDongTien.ShowOnPageLoad = false;
        loadData();
    }
}