﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_DangKyTuyenSinh_module_DanhSachKhaoSat : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    cls_DangKyHocSinh cls = new cls_DangKyHocSinh();
    private static int _idUser;
    private int _idHocSinh;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["UserName"] != null)
        {
            if (!IsPostBack)
            {
                Session["_id"] = 0;
                Session["_idHocSinh"] = 0;
            }
            loadData();
            _idUser = (from u in db.admin_Users
                       where u.username_username == Request.Cookies["UserName"].Value
                       select u).FirstOrDefault().username_id;
            if (_idUser == 154)
            {
                loadDataCoSo(1);
            }
            else if (_idUser == 155)
            {
                loadDataCoSo(2);
            }
            else if (_idUser == 156)
            {
                loadDataCoSo(3);
            }
            else if (_idUser == 157)
            {
                loadDataCoSo(4);
            }
            else
            {
                loadData();
            }
        }
        else
        {
            Response.Redirect("/admin-login");
        }
    }
    private void loadData()
    {
        // load data đổ vào var danh sách
        var getData = from nc in db.tbWebsite_DangKiTuyenSinhs
                      where nc.hocsinh_tinhtrang == 4 //ds khảo sát
                      orderby nc.chedo_id descending
                      select new
                      {
                          nc.hocsinh_id,
                          nc.hocsinh_name,
                          hocsinh_namsinh = nc.hocsinh_dateofbirth,
                          hocsinh_tuoi = DateTime.Now.Year - Convert.ToDateTime(nc.hocsinh_dateofbirth).Year + " tuổi",
                          nc.hocsinh_phuhuynh_name,
                          nc.hocsinh_tenme,
                          nc.hocsinh_address,
                          nc.hocsinh_phone,
                          nc.hocsinh_noidangky,
                          nc.hocsinh_dangki_class,
                          nc.hocsinh_sdtme,
                          nc.hocsinh_ngaykhaosat,
                          nc.hocsinh_ghichu,
                          username_fullname = (from u in db.admin_Users
                                               where u.username_id == nc.username_id
                                               select u).FirstOrDefault().username_fullname,
                          trangthai = nc.hocsinh_trangthai_dangky == 0 ? "Chưa TV" : nc.hocsinh_trangthai_dangky == 1 && nc.hocsinh_danhsachcho_khaosat == true ? "Chờ KS" : nc.hocsinh_trangthai_dangky == 1 ? "Đã TV" : "",
                          cosodangky = nc.hocsinh_cosodangky == 1 ? "CS1" : nc.hocsinh_cosodangky == 2 ? "CS2" : nc.hocsinh_cosodangky == 3 ? "CS3" : nc.hocsinh_cosodangky == 4 ? "TH-THCS-THPT" : "",
                          count = (from dk in db.tbWebsite_DangKiTuyenSinhs
                                   join tv in db.tbWebsite_TuVan_DangKiTuyenSinhs on dk.hocsinh_id equals tv.hocsinh_dangky_id
                                   where tv.hocsinh_dangky_id == nc.hocsinh_id
                                   select tv).Any() ? "" + db.tbWebsite_TuVan_DangKiTuyenSinhs.Where(x => x.hocsinh_dangky_id == nc.hocsinh_id).GroupBy(x => x.tuvan_thutu).Count() : "",
                          chedo_name = (from cd in db.tbDangky_CheDoUuTiens
                                        where cd.chedo_id == nc.chedo_id
                                        select cd).Any() ? "" + db.tbDangky_CheDoUuTiens.Where(x => x.chedo_id == nc.chedo_id).FirstOrDefault().chedo_name : "",
                      };
        // đẩy dữ liệu vào gridivew
        grvList.DataSource = getData;
        grvList.DataBind();

        //load ds lý do không đạt
        var getLyDo = from dm in db.tbWebsite_NhomLyDoKhongDats
                      where dm.hidden == false
                      select dm;
        rptDanhMucLyDo.DataSource = getLyDo;
        rptDanhMucLyDo.DataBind();
        //get ds chế độ ưu tiên
        rpCheDoUuTien.DataSource = from c in db.tbDangky_CheDoUuTiens
                                   orderby c.chedo_id descending
                                   select c;
        rpCheDoUuTien.DataBind();
    }
    private void loadDataCoSo(int coso)
    {

        // load data đổ vào var danh sách
        var getData = from nc in db.tbWebsite_DangKiTuyenSinhs
                      where nc.hocsinh_tinhtrang == 4 //ds khảo sát
                      && nc.hocsinh_cosodangky == coso
                      orderby nc.chedo_id descending
                      select new
                      {
                          nc.hocsinh_id,
                          nc.hocsinh_name,
                          hocsinh_namsinh = nc.hocsinh_dateofbirth,
                          hocsinh_tuoi = DateTime.Now.Year - Convert.ToDateTime(nc.hocsinh_dateofbirth).Year + " tuổi",
                          nc.hocsinh_phuhuynh_name,
                          nc.hocsinh_tenme,
                          nc.hocsinh_address,
                          nc.hocsinh_phone,
                          nc.hocsinh_noidangky,
                          nc.hocsinh_dangki_class,
                          nc.hocsinh_sdtme,
                          nc.hocsinh_ngaykhaosat,
                          nc.hocsinh_ghichu,
                          username_fullname = (from u in db.admin_Users
                                               where u.username_id == nc.username_id
                                               select u).FirstOrDefault().username_fullname,
                          trangthai = nc.hocsinh_trangthai_dangky == 0 ? "Chưa TV" : nc.hocsinh_trangthai_dangky == 1 ? "Đã TV" : "",
                          cosodangky = nc.hocsinh_cosodangky == 1 ? "CS1" : nc.hocsinh_cosodangky == 2 ? "CS2" : nc.hocsinh_cosodangky == 3 ? "CS3" : nc.hocsinh_cosodangky == 4 ? "TH-THCS-THPT" : "",
                          count = (from dk in db.tbWebsite_DangKiTuyenSinhs
                                   join tv in db.tbWebsite_TuVan_DangKiTuyenSinhs on dk.hocsinh_id equals tv.hocsinh_dangky_id
                                   where tv.hocsinh_dangky_id == nc.hocsinh_id
                                   select tv).Any() ? "" + db.tbWebsite_TuVan_DangKiTuyenSinhs.Where(x => x.hocsinh_dangky_id == nc.hocsinh_id).GroupBy(x => x.tuvan_thutu).Count() : "",
                          chedo_name = (from cd in db.tbDangky_CheDoUuTiens
                                        where cd.chedo_id == nc.chedo_id
                                        select cd).Any() ? "" + db.tbDangky_CheDoUuTiens.Where(x => x.chedo_id == nc.chedo_id).FirstOrDefault().chedo_name : "",
                      };
        // đẩy dữ liệu vào gridivew
        grvList.DataSource = getData;
        grvList.DataBind();
        //load ds lý do không đạt
        var getLyDo = from dm in db.tbWebsite_NhomLyDoKhongDats
                      where dm.hidden == false
                      select dm;
        rptDanhMucLyDo.DataSource = getLyDo;
        rptDanhMucLyDo.DataBind();
        //get ds chế độ ưu tiên
        rpCheDoUuTien.DataSource = from c in db.tbDangky_CheDoUuTiens
                                   orderby c.chedo_id descending
                                   select c;
        rpCheDoUuTien.DataBind();
    }
    protected void btnDat_Click(object sender, EventArgs e)
    {
        try
        {
            List<object> ds_Checked = grvList.GetSelectedFieldValues(new string[] { "hocsinh_id" });
            if (ds_Checked.Count() == 0)
            {
                alert.alert_Warning(Page, "Bạn chưa chọn học sinh!", "");
            }
            else
            {
                foreach (var item in ds_Checked)
                {
                    tbWebsite_DangKiTuyenSinh update = db.tbWebsite_DangKiTuyenSinhs.Where(x => x.hocsinh_id == Convert.ToInt32(item)).FirstOrDefault();
                    update.hocsinh_trangthai_dangky = -1; //khảo sát đạt
                    update.hocsinh_danhsachcho_khaosat = false;
                    update.hocsinh_dongtien = false;
                    update.hocsinh_tinhtrang = 5;//ds khảo sát đạt
                    db.SubmitChanges();
                }
                ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Khảo sát thành công!','','success').then(function(){grvList.Refresh();grvList.UnselectRows();})", true);
            }
        }
        catch (Exception)
        {
            alert.alert_Error(Page, "Đã có lỗi xảy ra!", "Vui lòng liên hệ IT");
        }
    }

    protected void btnKhongDat_Click(object sender, EventArgs e)
    {
        try
        {
            List<object> ds_Checked = grvList.GetSelectedFieldValues(new string[] { "hocsinh_id" });
            if (ds_Checked.Count() == 0)
            {
                alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu!", "");
            }
            else
            {
                string checkid = txtDanhSachChecked.Value;
                string[] getcheckid = checkid.Split(',');
                foreach (var item in ds_Checked)
                {
                    tbWebsite_DangKiTuyenSinh update = db.tbWebsite_DangKiTuyenSinhs.Where(x => x.hocsinh_id == Convert.ToInt32(item)).FirstOrDefault();
                    update.hocsinh_trangthai_dangky = -2; //khảo sát nhưng không đạt
                    update.hocsinh_khongdat_lydo = txtLyDo.Value;
                    update.hocsinh_danhsachcho_khaosat = false;
                    update.hocsinh_tinhtrang = -2;//ds khảo sát không đạt
                    db.SubmitChanges();
                    foreach (var id in getcheckid)
                    {
                        tbWebsite_DangKyTuyenSinh_KhongDat insert = new tbWebsite_DangKyTuyenSinh_KhongDat();
                        insert.hocsinh_id = Convert.ToInt32(item);
                        insert.username_id = _idUser;
                        insert.nhomlydo_id = Convert.ToInt32(id);
                        insert.khongdat_lydochitiet = txtLyDo.Value;
                        insert.create_date = DateTime.Now;
                        db.tbWebsite_DangKyTuyenSinh_KhongDats.InsertOnSubmit(insert);
                        db.SubmitChanges();
                    }
                }
                ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Khảo sát thành công!','','success').then(function(){grvList.Refresh();grvList.UnselectRows();})", true);
                txtLyDo.Value = "";
                txtDanhSachChecked.Value = "";
                txtCountChecked.Value = "";
            }
        }
        catch (Exception)
        {
            alert.alert_Error(Page, "Đã có lỗi xảy ra!", "Vui lòng liên hệ IT");
        }
    }
    protected void btnXem_Click(object sender, EventArgs e)
    {
        try
        {
            List<object> ds_Checked = grvList.GetSelectedFieldValues(new string[] { "hocsinh_id" });
            if (ds_Checked.Count() == 0)
            {
                alert.alert_Warning(Page, "Bạn chưa chọn học sinh!", "");
            }
            else if (ds_Checked.Count > 1)
            {
                alert.alert_Warning(Page, "Chỉ được chọn 1 học sinh để xem!", "");
            }
            else
            {
                Session["_idHocSinh"] = Convert.ToInt32(ds_Checked[0]);
                var getDetail = (from hs in db.tbWebsite_DangKiTuyenSinhs
                                 where hs.hocsinh_id == Convert.ToInt32(ds_Checked[0])
                                 select hs).Single();
                txtHoTenHS.Text = getDetail.hocsinh_name;
                txtDoB.Value = getDetail.hocsinh_dateofbirth.Value.ToString("yyyy-MM-dd").Replace(' ', 'T');
                txtQuocTich.Text = getDetail.hocsinh_quoctich;
                ddlLop.SelectedValue = getDetail.hocsinh_dangki_class;
                ddlCoSo.SelectedValue = getDetail.hocsinh_cosodangky + "";
                txtHoTenBa.Text = getDetail.hocsinh_phuhuynh_name;
                txtSDTBa.Text = getDetail.hocsinh_phone;
                txtEmailBa.Text = getDetail.hocsinh_email;
                txtHoTenMe.Text = getDetail.hocsinh_tenme;
                txtSDTMe.Text = getDetail.hocsinh_sdtme;
                txtEmailMe.Text = getDetail.hocsinh_emailme;
                txtDiaChi.Text = getDetail.hocsinh_address;
                if (getDetail.chedo_id != null)
                    txtCheDo.Value = getDetail.chedo_id + "";
                else
                    txtCheDo.Value = "0";
                txtLyDoUuTien.Value = getDetail.hocsinh_lydo_uutien;
                txtGhiChu.Value = getDetail.hocsinh_ghichu;
                txtNguonDangKy.Value = getDetail.hocsinh_nguondangky;
                ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupChiTiet.Show();setCheckedRadio();", true);

            }
        }
        catch (Exception)
        {
            alert.alert_Error(Page, "Đã có lỗi xảy ra!", "Vui lòng liên hệ IT");
        }
    }
    //get ds tư vấn
    private void danhSachTuVan(int hs_id)
    {
        var getData = (from tv in db.tbWebsite_TuVan_DangKiTuyenSinhs
                           //join u in db.admin_Users on tv.tuvan_user equals u.username_id
                       where tv.tuvan_trangthai == 0 && tv.hocsinh_dangky_id == hs_id
                       group tv by tv.tuvan_thutu into k
                       select new
                       {
                           tuvan_noidung = k.First().tuvan_noidung,
                           tuvan_ngaytuvan = k.First().tuvan_ngaytuvan,
                           username_fullname = (from u in db.admin_Users
                                                where u.username_id == Convert.ToInt32(k.First().tuvan_user)
                                                select u.username_fullname).First(),
                           danhmuc = string.Join(", ", (from tv in db.tbWebsite_TuVan_DangKiTuyenSinhs
                                                        join dm in db.tbWebsite_DanhMucTuVans on tv.danhmuc_id equals dm.danhmuc_id
                                                        where tv.tuvan_thutu == k.Key && tv.hocsinh_dangky_id == hs_id
                                                        select dm.danhmuc_name)),

                       }); ;
        rpTuVan.DataSource = getData;
        rpTuVan.DataBind();

        //load danh mục đã tư vấn
        var listDaTuVan = (from tv in db.tbWebsite_TuVan_DangKiTuyenSinhs
                           join dm in db.tbWebsite_DanhMucTuVans on tv.danhmuc_id equals dm.danhmuc_id
                           where tv.tuvan_trangthai == 0 && tv.hocsinh_dangky_id == hs_id
                           select new
                           {
                               dm.danhmuc_id,
                               dm.danhmuc_name,
                               dm.hidden
                           });
        //load list all danh mục 
        var listDanhMuc = (from dm in db.tbWebsite_DanhMucTuVans
                           where dm.hidden == false
                           select new
                           {
                               dm.danhmuc_id,
                               dm.danhmuc_name,
                               dm.hidden
                           }).ToList();
        var listConLai = listDanhMuc.Except(listDaTuVan);
        rptDanhMucTuVan.DataSource = listConLai;
        rptDanhMucTuVan.DataBind();

    }
    protected void btnTuVan_Click(object sender, EventArgs e)
    {
        try
        {
            List<object> ds_Checked = grvList.GetSelectedFieldValues(new string[] { "hocsinh_id" });
            if (ds_Checked.Count <= 0)
            {
                alert.alert_Warning(Page, "Vui lòng chọn học sinh cần tư vấn!", "");
            }
            else if (ds_Checked.Count > 1)
            {
                alert.alert_Warning(Page, "Chỉ được chọn 1 học sinh để tư vấn!", "");
            }

            else
            {
                foreach (var item in ds_Checked)
                {
                    _idHocSinh = Convert.ToInt32(item);
                }
                Session["_id"] = _idHocSinh;
                var checkTrangThai = (from hs in db.tbWebsite_DangKiTuyenSinhs
                                      where hs.hocsinh_id == _idHocSinh
                                      select hs).FirstOrDefault();
                if (checkTrangThai.hocsinh_trangthai_dangky == -1)//đã khảo sát đạt
                {
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "hiddenbutton", "hiddenButton()", true);
                }
                //if (checkTrangThai.hocsinh_danhsachcho_khaosat == true)
                //    chkChoKhaoSat.Checked = true;
                //else
                //    chkChoKhaoSat.Checked = false;
                ////nếu chưa có cơ sở đăng ký thì ấn nút chờ khảo sát luôn
                //if (checkTrangThai.hocsinh_cosodangky == 0)
                //{
                //    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "hiddenbutton", "hiddenRadio()", true);
                //}
                danhSachTuVan(_idHocSinh);
                //loadDanhMucTuVan();
                edtnoidung.Html = "";
                txtDanhSachChecked.Value = "";
                txtCountChecked.Value = "";
                ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupTuvan.Show();", true);
            }
        }
        catch (Exception)
        {
            alert.alert_Error(Page, "Đã có lỗi xảy ra!", "Vui lòng liên hệ IT");
        }
    }

    protected void btnChuyenCoSo_ServerClick(object sender, EventArgs e)
    {
        try
        {
            List<object> ds_Checked = grvList.GetSelectedFieldValues(new string[] { "hocsinh_id" });
            if (ds_Checked.Count() == 0)
            {
                alert.alert_Warning(Page, "Bạn chưa chọn học sinh!", "");
            }
            else if (ddlChuyenCoSo.SelectedValue == "0")
            {
                alert.alert_Warning(Page, "Bạn chưa chọn cơ sở!", "");
            }
            else
            {
                foreach (var item in ds_Checked)
                {
                    tbWebsite_DangKiTuyenSinh find = db.tbWebsite_DangKiTuyenSinhs.Where(x => x.hocsinh_id == Convert.ToInt32(item)).FirstOrDefault();
                    find.hocsinh_cosodangky = Convert.ToInt32(ddlChuyenCoSo.SelectedValue);
                    db.SubmitChanges();
                }

                ddlChuyenCoSo.SelectedValue = "0";
                ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Chuyển cơ sở thành công!','','success').then(function(){grvList.Refresh();grvList.UnselectRows();})", true);
            }
        }
        catch (Exception)
        {
            alert.alert_Error(Page, "Đã có lỗi xảy ra!", "Vui lòng liên hệ IT");
        }
    }

    protected void btnLuu_Tuvan_Click(object sender, EventArgs e)
    {
        try
        {
            string checkid = txtDanhSachChecked.Value;
            string[] getcheckid = checkid.Split(',');
            int lanTV = 1;
            //get lần tư vấn cuối cùng của hs
            if (db.tbWebsite_TuVan_DangKiTuyenSinhs.Any(x => x.hocsinh_dangky_id == Convert.ToInt32(Session["_id"].ToString())))
            {
                var getTuVan = (from tv in db.tbWebsite_TuVan_DangKiTuyenSinhs
                                where tv.hocsinh_dangky_id == Convert.ToInt32(Session["_id"].ToString())
                                orderby tv.tuvan_id descending
                                select tv).First();
                lanTV = Convert.ToInt32(getTuVan.tuvan_thutu) + 1;

            }
            foreach (var item in getcheckid)
            {
                //lưu nội dung tư vấn vào bảng tư vấn
                tbWebsite_TuVan_DangKiTuyenSinh insert = new tbWebsite_TuVan_DangKiTuyenSinh();
                insert.hocsinh_dangky_id = Convert.ToInt32(Session["_id"].ToString());
                insert.tuvan_noidung = edtnoidung.Html;
                insert.tuvan_user = _idUser;
                insert.tuvan_ngaytuvan = DateTime.Now;
                insert.tuvan_trangthai = 0;
                insert.danhmuc_id = Convert.ToInt32(item);
                insert.tuvan_thutu = lanTV;
                db.tbWebsite_TuVan_DangKiTuyenSinhs.InsertOnSubmit(insert);
                db.SubmitChanges();
            }
            //
            //cập nhật trạng thái đã tư vấn bên bảng đăng ký tuyển sinh
            //var getHocSinh = (from hs in db.tbWebsite_DangKiTuyenSinhs
            //                  where hs.hocsinh_id == Convert.ToInt32(Session["_id"].ToString())
            //                  select hs).FirstOrDefault();
            //getHocSinh.hocsinh_trangthai_dangky = 1; //đã được tư vấn
            //if (chkChoKhaoSat.Checked == true)
            //{
            //    getHocSinh.hocsinh_danhsachcho_khaosat = true;
            //}
            //db.SubmitChanges();
            ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Lưu thành công!','','success').then(function(){grvList.UnselectRows();})", true);
            if (_idUser == 154)
            {
                loadDataCoSo(1);
            }
            else if (_idUser == 155)
            {
                loadDataCoSo(2);
            }
            else if (_idUser == 156)
            {
                loadDataCoSo(3);
            }
            else if (_idUser == 157)
            {
                loadDataCoSo(4);
            }
            else
            {
                loadData();
            }
            popupTuvan.ShowOnPageLoad = false;
        }
        catch (Exception)
        {
            alert.alert_Error(Page, "Đã có lỗi xảy ra!", "Vui lòng liên hệ IT");
        }
    }
    protected void btnCapNhat_Click(object sender, EventArgs e)
    {
        try
        {
            if
                    (cls.CapNhat(Convert.ToInt32(Session["_idHocSinh"].ToString()), txtHoTenHS.Text, Convert.ToDateTime(txtDoB.Value), txtQuocTich.Text, ddlLop.SelectedValue, txtHoTenBa.Text, txtDiaChi.Text, txtEmailBa.Text, txtSDTBa.Text, _idUser, Convert.ToInt32(ddlCoSo.SelectedValue), Convert.ToInt32(txtCheDo.Value), txtHoTenMe.Text, txtSDTMe.Text, txtEmailMe.Text, txtLyDoUuTien.Value, txtGhiChu.Value, txtNguonDangKy.Value))
            {
                ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Cập nhật thành công!','','success').then(function(){grvList.UnselectRows();})", true);
            }
            else
            {
                alert.alert_Error(Page, "Lưu thất bại!", "");
            }
            popupChiTiet.ShowOnPageLoad = false;
            if (_idUser == 154)
            {
                loadDataCoSo(1);
            }
            else if (_idUser == 155)
            {
                loadDataCoSo(2);
            }
            else if (_idUser == 156)
            {
                loadDataCoSo(3);
            }
            else if (_idUser == 157)
            {
                loadDataCoSo(4);
            }
            else
            {
                loadData();
            }
        }
        catch (Exception)
        {
            alert.alert_Error(Page, "Đã có lỗi xảy ra!", "Vui lòng liên hệ IT");

        }
    }

    protected void btnChuyenDanhSach_ServerClick(object sender, EventArgs e)
    {
        try
        {
            List<object> ds_Checked = grvList.GetSelectedFieldValues(new string[] { "hocsinh_id" });
            if (ds_Checked.Count() == 0)
            {
                alert.alert_Warning(Page, "Bạn chưa chọn học sinh!", "");
                ddlChuyenDanhSach.SelectedValue = "";
            }
            else if (ddlChuyenDanhSach.SelectedValue == "")
            {
                alert.alert_Warning(Page, "Bạn chưa chọn danh sách!", "");
            }
            else
            {
                foreach (var item in ds_Checked)
                {
                    tbWebsite_DangKiTuyenSinh find = db.tbWebsite_DangKiTuyenSinhs.Where(x => x.hocsinh_id == Convert.ToInt32(item)).FirstOrDefault();
                    find.hocsinh_tinhtrang = Convert.ToInt32(ddlChuyenDanhSach.SelectedValue);
                    db.SubmitChanges();
                }

                ddlChuyenDanhSach.SelectedValue = "";
                ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Chuyển danh sách thành công!','','success').then(function(){grvList.Refresh();grvList.UnselectRows();})", true);
            }
        }
        catch (Exception)
        {
            alert.alert_Error(Page, "Đã có lỗi xảy ra!", "Vui lòng liên hệ IT");
        }
    }
}