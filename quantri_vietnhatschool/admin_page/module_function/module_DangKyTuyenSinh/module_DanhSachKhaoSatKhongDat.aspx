﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_DanhSachKhaoSatKhongDat.aspx.cs" Inherits="admin_page_module_function_module_DangKyTuyenSinh_module_DanhSachKhaoSatKhongDat" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <style>
        .btn_XemDetail {
            text-decoration: none !important;
            padding: 6px 8px;
            color: #ffffff !important;
            background-color: #52BCD3;
            border-color: #52BCD3;
        }

        span.title_modal {
            font-size: 1.5em
        }

        .check_radio {
            cursor: pointer;
            padding-left: 10px;
        }

        .chedo_mota {
            margin-left: 50px;
        }

        label {
            display: flex;
        }

        [type="radio"] {
            cursor: pointer;
        }

        span.field_important {
            color: red;
            font-size: 20px;
        }
    </style>
    <script>
        function checkNULL(s, e) {
            // phải chọn danh mục tư vấn
            var danhmuc = document.getElementById('<%=txtDanhSachChecked.ClientID%>');
            if (danhmuc.value.trim() == "") {
                swal('Vui lòng chọn danh mục tư vấn!', '', 'warning');
                return false;
            }
            var noidung = edtnoidung.GetHtml();
            if (noidung == "") {
                swal('Nội dung tư vấn không được để trống!', '', 'warning');
                return false;
            }
            return true;
        }
        function checkid(id) {
            var arrayvalue = document.getElementById("<%= txtDanhSachChecked.ClientID %>").value;
            var array = JSON.parse("[" + arrayvalue + "]");
            var index = array.indexOf(id);
            if (index > -1) {
                array.splice(index, 1);
                document.getElementById("<%= txtDanhSachChecked.ClientID %>").value = array;
                document.getElementById("<%= txtCountChecked.ClientID %>").value = document.getElementById("<%= txtCountChecked.ClientID %>").value - 1;
            }
            else {
                document.getElementById("<%= txtCountChecked.ClientID %>").value = array.push(id);
                document.getElementById("<%= txtDanhSachChecked.ClientID %>").value = array;
            }
        }
        function setChecked() {
            var id = document.getElementById('<%=txtCheDo.ClientID%>').value;
            if (id != "0") {
                document.getElementById(id).checked = true;
            }
            else { }
            //checked nguồn đăng ký
            var nguondangky = document.getElementById('<%=txtNguonDangKy.ClientID%>').value;
            let radios = document.getElementsByName("rdNguonDangKy");
            for (let i = 0, length = radios.length; i < length; i++) {
                if (radios[i].value == nguondangky) {
                    radios[i].checked = true;
                    break;
                }
            }
        }
        function showMoTa(chedo_id) {
            document.getElementById('chedo_' + chedo_id).style.display = "block";
        }
        function hiddenMoTa() {
            var ele = document.getElementsByClassName('chedo_mota');
            for (var i = 0; i < ele.length; i++) {
                ele[i].style.display = "none";
            }
        }
        function checkNULLCapNhat() {
            var tenHS = document.getElementById('<%= txtHoTenHS.ClientID%>');
            var DoB = document.getElementById('<%= txtDoB.ClientID%>');
            var tenBa = document.getElementById('<%= txtHoTenBa.ClientID%>');
            var dtBa = document.getElementById('<%= txtSDTBa.ClientID%>');
            var tenMe = document.getElementById('<%= txtHoTenMe.ClientID%>');
            var dtMe = document.getElementById('<%= txtSDTMe.ClientID%>');
            var classSelect = document.getElementById('<%= ddlLop.ClientID%>');

            var chedo_id = document.getElementById('<%=txtCheDo.ClientID%>');
            var lydouutien = document.getElementById('<%=txtLyDoUuTien.ClientID%>');

            if (tenHS.value.trim() == "") {
                swal('Họ tên học sinh không được để trống!', '', 'warning').then(function () { tenHS.focus(); });
                return false;
            }
            if (DoB.value.trim() == "") {
                swal('Ngày sinh không được để trống!', '', 'warning');
                return false;
            }
            if (tenBa.value.trim() == "" && tenMe.value.trim() == "") {
                swal('Họ tên phụ huynh không được để trống!', '', 'warning').then(function () { tenPH.focus(); });
                return false;
            }
            if (dtBa.value.trim() == "" && dtBa.value.trim() == "") {
                swal('Số điện thọai không được để trống!', '', 'warning').then(function () { dienThoai.focus(); });
                return false;
            }
            if (classSelect.value == "0") {
                swal('Vui lòng chọn lớp đăng ký!', '', 'warning').then(function () { classSelect.focus(); });
                return false;
            }
            if (chedo_id.value.trim() != "0" && lydouutien.value.trim() == "") {
                swal('Vui lòng nhập lý do được ưu tiên!', '', 'warning').then(function () { lydouutien.focus(); });
                return false;
            }
            return true;
        }
        function check_CheDo(chedo_id) {
            document.getElementById('<%=txtCheDo.ClientID%>').value = chedo_id;
            document.getElementById('<%=txtLyDoUuTien.ClientID%>').focus();
        }
        //func chọn nguồn đăng kí
        function check_NguonDangKy(nguon_id) {
            document.getElementById('<%=txtNguonDangKy.ClientID%>').value = document.getElementById(nguon_id).value;
        }
    </script>
    <div class="card card-block">
        <div class="title__form text-md-center">
            <h3 class="text-uppercase">Danh sách khảo sát không đạt</h3>
        </div>
        <div class="form-group row">
            <div class="col-sm-10">
                <asp:UpdatePanel ID="udButton" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnXem" runat="server" OnClick="btnXem_Click" Text="Xem" CssClass="btn btn-primary" />
                        <asp:Button ID="btnTuVan" runat="server" OnClick="btnTuVan_Click" Text="Tư vấn lại" CssClass="btn btn-primary" />
                        <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#modal_ChuyenDanhSach">Chuyển danh sách</a>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <div class="form-group table-responsive">
            <dx:ASPxGridView ID="grvList" runat="server" ClientInstanceName="grvList" KeyFieldName="hocsinh_id" Width="100%">
                <Columns>
                    <dx:GridViewCommandColumn ShowSelectCheckbox="True" SelectAllCheckboxMode="Page" VisibleIndex="0" Width="5%">
                    </dx:GridViewCommandColumn>
                    <dx:GridViewDataColumn Caption="STT" HeaderStyle-HorizontalAlign="Center" Width="3%">
                        <DataItemTemplate>
                            <%#Container.ItemIndex+1 %>
                        </DataItemTemplate>
                    </dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Họ tên HS" FieldName="hocsinh_name" HeaderStyle-HorizontalAlign="Center" Width="20%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Năm sinh" FieldName="hocsinh_namsinh" HeaderStyle-HorizontalAlign="Center" Width="5%"></dx:GridViewDataColumn>
                    <%--<dx:GridViewDataColumn Caption="Tuổi" FieldName="hocsinh_tuoi" HeaderStyle-HorizontalAlign="Center" Width="8%"></dx:GridViewDataColumn>--%>
                    <%--<dx:GridViewDataColumn Caption="Lớp đk" FieldName="hocsinh_dangki_class" HeaderStyle-HorizontalAlign="Center" Width="5%"></dx:GridViewDataColumn>--%>
                    <dx:GridViewDataColumn Caption="Họ tên ba" FieldName="hocsinh_phuhuynh_name" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="SĐT ba" FieldName="hocsinh_phone" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Họ tên mẹ" FieldName="hocsinh_tenme" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="SĐT mẹ" FieldName="hocsinh_sdtme" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Ngày khảo sát" FieldName="hocsinh_ngaykhaosat" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>
                    <%--<dx:GridViewDataColumn Caption="Địa chỉ" FieldName="hocsinh_address" HeaderStyle-HorizontalAlign="Center" Width="20%"></dx:GridViewDataColumn>--%>
                    <%--<dx:GridViewDataColumn Caption="Nơi ĐK" FieldName="hocsinh_noidangky" HeaderStyle-HorizontalAlign="Center" Width="5%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="CSDK" FieldName="cosodangky" HeaderStyle-HorizontalAlign="Center" Width="5%"></dx:GridViewDataColumn>--%>
                    <%--<dx:GridViewDataColumn Caption="Người ĐK" FieldName="username_fullname" HeaderStyle-HorizontalAlign="Center" Width="5%"></dx:GridViewDataColumn>--%>
                    <dx:GridViewDataColumn Caption="Số lần TV" FieldName="trangthai" HeaderStyle-HorizontalAlign="Center" Width="3%">
                        <DataItemTemplate>
                            <p><%#Eval("count") %></p>
                        </DataItemTemplate>
                    </dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Trạng thái" FieldName="chedo_name" HeaderStyle-HorizontalAlign="Center" Width="3%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Ghi chú" FieldName="hocsinh_ghichu" HeaderStyle-HorizontalAlign="Center" Width="25%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="" HeaderStyle-HorizontalAlign="Center" Width="5%">
                        <DataItemTemplate>
                            <a href="#" class="btn_XemDetail" data-toggle="modal" data-target="#modal_Lydo_<%#Eval("hocsinh_id") %>">Xem</a>
                        </DataItemTemplate>
                    </dx:GridViewDataColumn>
                </Columns>
                <SettingsSearchPanel Visible="true" />
                <SettingsBehavior AllowFocusedRow="true" />
                <SettingsText EmptyDataRow="Trống" SearchPanelEditorNullText="Gỏ từ cần tìm kiếm và enter..." />
                <SettingsLoadingPanel Text="Đang tải..." />
                <SettingsPager PageSize="20" Summary-Text="Trang {0} / {1} ({2} trang)"></SettingsPager>
            </dx:ASPxGridView>
        </div>
    </div>
    <%--repater modal detail--%>
    <asp:Repeater runat="server" ID="rpDetail_LyDo">
        <ItemTemplate>
            <div class="modal fade" id="modal_Lydo_<%#Eval("hocsinh_id") %>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <span class="title_modal">CHI TIẾT
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                            </span>
                        </div>
                        <div class="modal-body" style="display: flex;">
                            <table>
                                <tr>
                                    <td>Tình trạng khảo sát: <strong>KHÔNG ĐẠT</strong></td>
                                </tr>
                                <tr>
                                    <td>Danh mục không đạt: <%#Eval("danhmuc") %></td>
                                </tr>
                                <tr>
                                    <td>Lý do không đạt: <%#Eval("hocsinh_khongdat_lydo") %></td>
                                </tr>
                            </table>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                        </div>
                    </div>
                </div>
            </div>
        </ItemTemplate>
    </asp:Repeater>
    <dx:ASPxPopupControl ID="popupTuvan" runat="server" Width="800px" Height="550px" CloseAction="CloseButton" ShowCollapseButton="True" ShowMaximizeButton="True" ScrollBars="Auto" CloseOnEscape="true" Modal="True"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" ClientInstanceName="popupTuvan" ShowFooter="true"
        HeaderText="TƯ VẤN ĐĂNG KÝ TUYỂN SINH" AllowDragging="True" PopupAnimationType="Fade" EnableViewState="False" AutoUpdatePosition="true" ClientSideEvents-CloseUp="function(s,e){grvList.Refresh();}">
        <ContentCollection>
            <dx:PopupControlContentControl runat="server">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <div class="popup-main">
                            <div class="div_content row ml-0 mt-0">
                                <input type="text" name="name" id="txtHocSinh_id" runat="server" hidden />
                                <%--danh sách tư vấn trước đó--%>
                                <div class="col-12">
                                    <table class="table table-bordered mr-2">
                                        <thead>
                                            <tr>
                                                <th>Danh mục tư vấn</th>
                                                <th>Nội dung tư vấn</th>
                                                <th>Ngày tư vấn</th>
                                                <th>Người tư vấn</th>
                                                <th>Trạng thái</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <asp:Repeater runat="server" ID="rpTuVan">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%#Eval("danhmuc") %></td>
                                                        <td><%#Eval("tuvan_noidung") %></td>
                                                        <td><%#Eval("tuvan_ngaytuvan", "{0: dd/MM/yyyy}") %></td>
                                                        <td><%#Eval("username_fullname") %></td>
                                                        <td><%#Eval("tinhtrang") %></td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-12 form-group">
                                    <label class="col-2 form-control-label">Danh mục tư vấn:</label>
                                    <div class="col-10">
                                        <div class="row">
                                            <asp:Repeater ID="rptDanhMucTuVan" runat="server">
                                                <ItemTemplate>
                                                    <div class="col-2">
                                                        <label>
                                                            <input class="checkbox" type="checkbox" id="<%#Eval("danhmuc_id") %>" onclick="checkid(<%#Eval("danhmuc_id") %> )" />
                                                            <span></span>
                                                            <p class="pr-name"><%#Eval("danhmuc_name")%></p>
                                                        </label>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </div>
                                        <div style="display: none">
                                            <input id="txtDanhSachChecked" runat="server" type="text" placeholder="iddm" />
                                            <input id="txtCountChecked" runat="server" type="text" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12" id="div_content_tuvan">
                                    <div class="col-12 form-group">
                                        <label class="col-12 form-control-label">Nội dung:</label>
                                    </div>
                                    <div class="col-12 form-group">
                                        <div class="col-12">
                                            <dx:ASPxHtmlEditor ID="edtnoidung" ClientInstanceName="edtnoidung" runat="server" Width="100%" Height="300px" Border-BorderStyle="Solid" Border-BorderWidth="1px" Border-BorderColor="#dddddd">
                                                <SettingsHtmlEditing EnablePasteOptions="true" AllowYouTubeVideoIFrames="true" />
                                                <Settings AllowHtmlView="true" AllowContextMenu="Default" />
                                                <settingsimageupload uploadfolder="~/editorimages"></settingsimageupload>
                                                <Toolbars>
                                                    <dx:HtmlEditorToolbar>
                                                        <Items>
                                                            <dx:ToolbarCustomCssEdit Width="120px">
                                                                <Items>
                                                                    <dx:ToolbarCustomCssListEditItem TagName="" Text="Clear Style" CssClass="" />
                                                                    <dx:ToolbarCustomCssListEditItem TagName="H1" Text="Title" CssClass="CommonTitle">
                                                                        <PreviewStyle CssClass="CommonTitlePreview" />
                                                                    </dx:ToolbarCustomCssListEditItem>
                                                                    <dx:ToolbarCustomCssListEditItem TagName="H3" Text="Header1" CssClass="CommonHeader1">
                                                                        <PreviewStyle CssClass="CommonHeader1Preview" />
                                                                    </dx:ToolbarCustomCssListEditItem>
                                                                    <dx:ToolbarCustomCssListEditItem TagName="H4" Text="Header2" CssClass="CommonHeader2">
                                                                        <PreviewStyle CssClass="CommonHeader2Preview" />
                                                                    </dx:ToolbarCustomCssListEditItem>
                                                                    <dx:ToolbarCustomCssListEditItem TagName="Div" Text="Content" CssClass="CommonContent">
                                                                        <PreviewStyle CssClass="CommonContentPreview" />
                                                                    </dx:ToolbarCustomCssListEditItem>
                                                                    <dx:ToolbarCustomCssListEditItem TagName="Strong" Text="Features" CssClass="CommonFeatures">
                                                                        <PreviewStyle CssClass="CommonFeaturesPreview" />
                                                                    </dx:ToolbarCustomCssListEditItem>
                                                                    <dx:ToolbarCustomCssListEditItem TagName="Div" Text="Footer" CssClass="CommonFooter">
                                                                        <PreviewStyle CssClass="CommonFooterPreview" />
                                                                    </dx:ToolbarCustomCssListEditItem>
                                                                    <dx:ToolbarCustomCssListEditItem TagName="" Text="Link" CssClass="Link">
                                                                        <PreviewStyle CssClass="LinkPreview" />
                                                                    </dx:ToolbarCustomCssListEditItem>
                                                                    <dx:ToolbarCustomCssListEditItem TagName="EM" Text="ImageTitle" CssClass="ImageTitle">
                                                                        <PreviewStyle CssClass="ImageTitlePreview" />
                                                                    </dx:ToolbarCustomCssListEditItem>
                                                                    <dx:ToolbarCustomCssListEditItem TagName="" Text="ImageMargin" CssClass="ImageMargin">
                                                                        <PreviewStyle CssClass="ImageMarginPreview" />
                                                                    </dx:ToolbarCustomCssListEditItem>
                                                                </Items>
                                                            </dx:ToolbarCustomCssEdit>
                                                            <dx:ToolbarParagraphFormattingEdit>
                                                                <Items>
                                                                    <dx:ToolbarListEditItem Text="Normal" Value="p" />
                                                                    <dx:ToolbarListEditItem Text="Heading  1" Value="h1" />
                                                                    <dx:ToolbarListEditItem Text="Heading  2" Value="h2" />
                                                                    <dx:ToolbarListEditItem Text="Heading  3" Value="h3" />
                                                                    <dx:ToolbarListEditItem Text="Heading  4" Value="h4" />
                                                                    <dx:ToolbarListEditItem Text="Heading  5" Value="h5" />
                                                                    <dx:ToolbarListEditItem Text="Heading  6" Value="h6" />
                                                                    <dx:ToolbarListEditItem Text="Address" Value="address" />
                                                                    <dx:ToolbarListEditItem Text="Normal (DIV)" Value="div" />
                                                                </Items>
                                                            </dx:ToolbarParagraphFormattingEdit>
                                                            <dx:ToolbarFontNameEdit>
                                                                <Items>
                                                                    <dx:ToolbarListEditItem Value="Times New Roman" Text="Times New Roman"></dx:ToolbarListEditItem>
                                                                    <dx:ToolbarListEditItem Value="Tahoma" Text="Tahoma"></dx:ToolbarListEditItem>
                                                                    <dx:ToolbarListEditItem Value="Verdana" Text="Verdana"></dx:ToolbarListEditItem>
                                                                    <dx:ToolbarListEditItem Value="Arial" Text="Arial"></dx:ToolbarListEditItem>
                                                                    <dx:ToolbarListEditItem Value="MS Sans Serif" Text="MS Sans Serif"></dx:ToolbarListEditItem>
                                                                    <dx:ToolbarListEditItem Value="Courier" Text="Courier"></dx:ToolbarListEditItem>
                                                                    <dx:ToolbarListEditItem Value="bodoni MT" Text="bodoni MT"></dx:ToolbarListEditItem>
                                                                </Items>
                                                            </dx:ToolbarFontNameEdit>
                                                            <dx:ToolbarFontSizeEdit>
                                                                <Items>
                                                                    <dx:ToolbarListEditItem Value="1" Text="1 (8pt)"></dx:ToolbarListEditItem>
                                                                    <dx:ToolbarListEditItem Value="2" Text="2 (10pt)"></dx:ToolbarListEditItem>
                                                                    <dx:ToolbarListEditItem Value="3" Text="3 (12pt)"></dx:ToolbarListEditItem>
                                                                    <dx:ToolbarListEditItem Value="4" Text="4 (14pt)"></dx:ToolbarListEditItem>
                                                                    <dx:ToolbarListEditItem Value="5" Text="5 (18pt)"></dx:ToolbarListEditItem>
                                                                    <dx:ToolbarListEditItem Value="6" Text="6 (24pt)"></dx:ToolbarListEditItem>
                                                                    <dx:ToolbarListEditItem Value="7" Text="7 (36pt)"></dx:ToolbarListEditItem>
                                                                </Items>
                                                            </dx:ToolbarFontSizeEdit>
                                                            <dx:ToolbarBoldButton BeginGroup="True" />
                                                            <dx:ToolbarItalicButton />
                                                            <dx:ToolbarUnderlineButton />
                                                            <dx:ToolbarStrikethroughButton />
                                                            <dx:ToolbarJustifyLeftButton BeginGroup="True" />
                                                            <dx:ToolbarJustifyCenterButton />
                                                            <dx:ToolbarJustifyRightButton />
                                                            <dx:ToolbarJustifyFullButton />
                                                            <dx:ToolbarBackColorButton BeginGroup="True" />
                                                            <dx:ToolbarFontColorButton />
                                                        </Items>
                                                    </dx:HtmlEditorToolbar>
                                                </Toolbars>
                                            </dx:ASPxHtmlEditor>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <FooterContentTemplate>
            <div class="mar_but button">
                <asp:Button ID="btnLuu_Tuvan" runat="server" ClientIDMode="Static" Text="Lưu" CssClass="btn btn-primary" OnClientClick="return checkNULL()" OnClick="btnLuu_Tuvan_Click" />
            </div>
        </FooterContentTemplate>
        <ContentStyle>
            <Paddings PaddingBottom="0px" />
        </ContentStyle>
    </dx:ASPxPopupControl>
    <%--popup chi tiết--%>
    <dx:ASPxPopupControl ID="popupChiTiet" runat="server" Width="1000px" Height="550px" CloseAction="CloseButton" ShowCollapseButton="True" ShowMaximizeButton="True" ScrollBars="Auto" CloseOnEscape="true" Modal="True"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" ClientInstanceName="popupChiTiet" ShowFooter="true"
        HeaderText="THÔNG TIN CHI TIẾT HỌC SINH" AllowDragging="True" PopupAnimationType="Fade" EnableViewState="False" AutoUpdatePosition="true" ClientSideEvents-CloseUp="function(s,e){grvList.Refresh();}">
        <ContentCollection>
            <dx:PopupControlContentControl runat="server">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="popup-main">
                            <div class="div_content row ml-0 mt-0">
                                <p class="title_info" style="color: #1058dc; font-size: 20px"><b>Thông tin của HS:</b></p>
                                <div class="col-12">
                                    <div class="col-12 form-group">
                                        <label class="col-2 form-control-label">Họ tên <span class="field_important">*</span>:</label>
                                        <div class="col-10">
                                            <asp:TextBox ID="txtHoTenHS" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%"> </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-12 form-group">
                                        <label class="col-2 form-control-label">Ngày sinh <span class="field_important">*</span>:</label>
                                        <div class="col-10">
                                            <input type="date" runat="server" id="txtDoB" class="form-control boxed" value="" style="width: 95%" />
                                        </div>
                                    </div>
                                    <div class="col-12 form-group">
                                        <label class="col-2 form-control-label">Quốc tịch:</label>
                                        <div class="col-10">
                                            <asp:TextBox ID="txtQuocTich" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%"> </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-12 form-group">
                                        <div class="col-6">
                                            <label class="col-4 form-control-label">Lớp đăng ký<span class="field_important">*</span>:</label>
                                            <div class="col-6">
                                                <asp:DropDownList runat="server" ID="ddlLop" class=" form-control form-select_class" Width="95%">
                                                    <asp:ListItem Value="0" Text="Chọn lớp" />
                                                    <asp:ListItem Value="Mầm non" Text="Mầm non" />
                                                    <asp:ListItem Value="Lớp 1" Text="Lớp 1" />
                                                    <asp:ListItem Value="Lớp 2" Text="Lớp 2" />
                                                    <asp:ListItem Value="Lớp 3" Text="Lớp 3" />
                                                    <asp:ListItem Value="Lớp 4" Text="Lớp 4" />
                                                    <asp:ListItem Value="Lớp 5" Text="Lớp 5" />
                                                    <asp:ListItem Value="Lớp 6" Text="Lớp 6" />
                                                    <asp:ListItem Value="Lớp 7" Text="Lớp 7" />
                                                    <asp:ListItem Value="Lớp 8" Text="Lớp 8" />
                                                    <asp:ListItem Value="Lớp 9" Text="Lớp 9" />
                                                    <asp:ListItem Value="Lớp 10" Text="Lớp 10" />
                                                    <asp:ListItem Value="Lớp 11" Text="Lớp 11" />
                                                    <asp:ListItem Value="Lớp 12" Text="Lớp 12" />
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <label class="col-4 form-control-label">Cơ sở đăng ký:</label>
                                            <div class="col-8">
                                                <asp:DropDownList runat="server" ID="ddlCoSo" class=" form-control form-select_class" Width="90%">
                                                    <asp:ListItem Value="0" Text="Chọn cơ sở" />
                                                    <asp:ListItem Value="1" Text="Cơ sở 1" />
                                                    <asp:ListItem Value="2" Text="Cơ sở 2" />
                                                    <asp:ListItem Value="3" Text="Cơ sở 3" />
                                                    <asp:ListItem Value="4" Text="Trường liên cấp TH-THCS-THPT" />
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 form-group">
                                        <label class="col-2 form-control-label">Độ ưu tiên:</label>
                                        <div class="col-10">
                                            <asp:Repeater runat="server" ID="rpCheDoUuTien">
                                                <ItemTemplate>
                                                    <span style="display: flex">
                                                        <input type="radio" name="rdCheDo" id="<%#Eval("chedo_id") %>" />
                                                        <label class="pr-name check_radio" for="<%#Eval("chedo_id") %>"><%#Eval("chedo_name") %></label>
                                                    </span>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </div>
                                        <label class="col-2 form-control-label">Lý do ưu tiên:</label>
                                        <div class="col-10">
                                            <textarea class="form-control" runat="server" id="txtLyDoUuTien" rows="3" style="width: 95%;"></textarea>
                                        </div>
                                        <input type="text" name="name" value="0" id="txtCheDo" runat="server" hidden="hidden" />
                                    </div>
                                </div>
                            </div>
                            <div class="row ml-0">
                                <p class="title_info" style="color: #1058dc; font-size: 20px"><b>Thông tin của Phụ huynh:</b></p>
                                <div class="col-6">
                                    <div class="col-12 form-group">
                                        <label class="col-2 form-control-label">Họ tên ba:</label>
                                        <div class="col-10">
                                            <asp:TextBox ID="txtHoTenBa" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%"> </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-12 form-group">
                                        <label class="col-2 form-control-label">SĐT ba:</label>
                                        <div class="col-10">
                                            <asp:TextBox ID="txtSDTBa" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%" onkeypress="return isNumberKey(event)"> </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-12 form-group">
                                        <label class="col-2 form-control-label">Email ba:</label>
                                        <div class="col-10">
                                            <asp:TextBox ID="txtEmailBa" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%"> </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="col-12 form-group">
                                        <label class="col-2 form-control-label">Họ tên mẹ:</label>
                                        <div class="col-10">
                                            <asp:TextBox ID="txtHoTenMe" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%"> </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-12 form-group">
                                        <label class="col-2 form-control-label">SĐT mẹ:</label>
                                        <div class="col-10">
                                            <asp:TextBox ID="txtSDTMe" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%" onkeypress="return isNumberKey(event)"> </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-12 form-group">
                                        <label class="col-2 form-control-label">Email mẹ:</label>
                                        <div class="col-10">
                                            <asp:TextBox ID="txtEmailMe" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%"> </asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 form-group">
                                <label class="col-2 form-control-label">Địa chỉ:</label>
                                <div class="col-10">
                                    <asp:TextBox ID="txtDiaChi" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="95%"> </asp:TextBox>
                                </div>
                            </div>
                            <div class="col-12 form-group">
                                <label class="col-2 form-control-label">Nội dung ghi chú:</label>
                                <div class="col-10">
                                    <textarea class="form-control" runat="server" id="txtGhiChu" rows="3" style="width: 95%;"></textarea>
                                </div>
                            </div>
                            <div class="col-12 form-group">
                                <label class="col-2 form-control-label">Nguồn đăng ký:</label>
                                <div class="col-10">
                                    <span style="display: flex">
                                        <input type="radio" name="rdNguonDangKy" id="dentruongtructiep" onclick="check_NguonDangKy(this.id)" value="Đến trường trực tiếp" />
                                        <label class="pr-name check_radio" for="dentruongtructiep">Đến trường trực tiếp</label>
                                    </span>
                                    <span style="display: flex">
                                        <input type="radio" name="rdNguonDangKy" id="zalo" onclick="check_NguonDangKy(this.id)" value="Zalo" />
                                        <label class="pr-name check_radio" for="zalo">Zalo</label>
                                    </span>
                                    <span style="display: flex">
                                        <input type="radio" name="rdNguonDangKy" id="goidientructiep" onclick="check_NguonDangKy(this.id)" value="Gọi điện trực tiếp" />
                                        <label class="pr-name check_radio" for="goidientructiep">Gọi điện trực tiếp</label>
                                    </span>
                                    <span style="display: flex">
                                        <input type="radio" name="rdNguonDangKy" id="facebook" onclick="check_NguonDangKy(this.id)" value="Facebook" />
                                        <label class="pr-name check_radio" for="facebook">Facebook</label>
                                    </span>
                                    <span style="display: flex">
                                        <input type="radio" name="rdNguonDangKy" id="nguoiquengioithieu" onclick="check_NguonDangKy(this.id)" value="Người quen giới thiệu" />
                                        <label class="pr-name check_radio" for="nguoiquengioithieu">Người quen giới thiệu</label>
                                    </span>
                                    <span style="display: flex">
                                        <input type="radio" name="rdNguonDangKy" id="website" onclick="check_NguonDangKy(this.id)" value="Website" />
                                        <label class="pr-name check_radio" for="website">Website</label>
                                    </span>
                                </div>
                                <input type="text" name="name" value="" id="txtNguonDangKy" runat="server" hidden="hidden" />
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <FooterContentTemplate>
            <div class="mar_but button">
                <asp:Button ID="btnCapNhat" runat="server" ClientIDMode="Static" Text="Cập nhật" CssClass="btn btn-primary" OnClientClick="return checkNULLCapNhat()" OnClick="btnCapNhat_Click" />
            </div>
        </FooterContentTemplate>
        <ContentStyle>
            <Paddings PaddingBottom="0px" />
        </ContentStyle>
    </dx:ASPxPopupControl>
    <%--modal chuyển danh sách--%>
    <div class="modal fade" id="modal_ChuyenDanhSach" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <span class="title_modal">Chuyển danh sách
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </span>
                </div>
                <div class="modal-body" style="display: flex; justify-content: center;">
                    <span>Danh sách chuyển đến:</span>
                    <asp:DropDownList runat="server" ID="ddlChuyenDanhSach" class=" form-control form-select_class ml-2" Width="350px">
                        <asp:ListItem Value="" Text="Chọn danh sách" />
                        <asp:ListItem Value="0" Text="Danh sách lưu trữ" />
                        <asp:ListItem Value="1" Text="Danh sách đang học" />
                        <asp:ListItem Value="2" Text="Danh sách chờ" />
                        <asp:ListItem Value="3" Text="Danh sách chờ khảo sát" />
                        <asp:ListItem Value="4" Text="Danh sách khảo sát" />
                        <asp:ListItem Value="5" Text="Danh sách khảo sát đạt" />
                        <asp:ListItem Value="6" Text="Danh sách đăng ký mới" />
                        <asp:ListItem Value="-1" Text="Danh sách đã nghỉ/không học" />
                        <%--<asp:ListItem Value="-2" Text="Danh sách khảo sát không đạt" />--%>
                    </asp:DropDownList>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn btn-primary" id="btnChuyenDanhSach" runat="server" onserverclick="btnChuyenDanhSach_ServerClick">Lưu</a>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

