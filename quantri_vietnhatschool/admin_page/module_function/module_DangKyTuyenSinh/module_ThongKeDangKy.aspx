﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_ThongKeDangKy.aspx.cs" Inherits="admin_page_module_function_module_DangKyTuyenSinh_module_ThongKeDangKy" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@0.7.0"></script>
    <script src="https://code.jquery.com/jquery-latest.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <script>

        function drawingPieChart() {
            for (var a = [], i = 0; i < 8; ++i) a[i] = "#" + (Math.random() * 0xFFFFFF << 0).toString(16).padStart(6, '0');


            var list_TieuDe = document.getElementById("<%=txtTieuDe.ClientID%>").value.replaceAll("'", '"');
            var array_TieuDe = JSON.parse("[" + list_TieuDe + "]");

            var kq_SoLanKhaoSat = document.getElementById("<%=txtSoHSDK.ClientID%>").value.replaceAll("'", "");
            var array_SoLanKhaoSat = JSON.parse("[" + kq_SoLanKhaoSat + "]");

            var data = [{
                data: array_SoLanKhaoSat,
                backgroundColor: a,
                borderColor: "#fff"
            }];
            var options = {
                tooltips: {
                    enabled: true
                },
                plugins: {
                    datalabels: {
                        formatter: (value, ctx) => {
                            let sum = ctx.dataset._meta[0].total;
                            let percentage = (value * 100 / sum).toFixed(2) + "%";
                            return percentage;
                        },
                        color: '#ffffff'
                    }
                },
                title: {
                    display: true,
                    text: "Thống kê tỉ lệ đăng ký"
                },
            };
            var ctx = document.getElementById('pie-chart').getContext('2d'),
                elements = [];
            var myChart = new Chart(ctx, {
                type: 'pie',
                data: {
                    labels: array_TieuDe,
                    datasets: data
                },
                options: options
            });
        }
    </script>
    <div class="main-omt">
        <div class="omt-top">
            <div class="wrapper__select mt-2">
                <div class="col-4" style="margin-right: 10px; display: flex">
                    <span style="padding: 10px 0 5px 5px">Từ ngày</span>
                    <input type="date" id="dteTuNgay" runat="server" class="form-control" style="width: 80%" />
                </div>
                <div class="col-4" style="margin-right: 10px; display: flex">
                    <span style="padding: 10px 0 5px 5px">Đến ngày</span>
                    <input type="date" id="dteDenNgay" runat="server" class="form-control" style="width: 80%" />
                </div>
                <div class="col-1">
                    <a href="#" id="btnXem" class="btn btn-primary" runat="server" onserverclick="btnXem_ServerClick">Xem</a>
                </div>
            </div>
        </div>
        <%--Day la bieu do tron--%>
        <div class="wrapper__select">
            <div id="div_BieuDoTron" runat="server" class="col-12" style="margin-top: 10px">
                <canvas id="pie-chart"></canvas>
            </div>
            <div style="display: none">
                <input type="text" id="txtTieuDe" runat="server" />
                <input type="text" id="txtSoHSDK" runat="server" />
                <%--<a href="#" onclick="drawingPieChart()">xem thông kê</a>--%>
            </div>
        </div>
    </div>
    

</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

