﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_DangKyTuyenSinh_module_ThongKeDangKy : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    public string solan = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["UserName"] != null)
        {
            if (!IsPostBack)
            {
                div_BieuDoTron.Visible = false;

            }
        }
        else
        {
            Response.Redirect("/admin-login");
        }
    }
    protected void LoadThongKeBieuDoTron(DateTime tungay, DateTime denngay)
    {
        div_BieuDoTron.Visible = true;
        //GET RA số hs khảo sát đạt, không đạt, chưa khảo sat
        var getKhaoSatDat = (from kq in db.tbWebsite_DangKiTuyenSinhs
                             where kq.hocsinh_trangthai_dangky == -1
                             && kq.hocsinh_ngaydangky.Value.Day >= tungay.Day
                             && kq.hocsinh_ngaydangky.Value.Month >= tungay.Month
                             && kq.hocsinh_ngaydangky.Value.Year >= tungay.Year
                             && kq.hocsinh_ngaydangky.Value.Day <= denngay.Day
                             && kq.hocsinh_ngaydangky.Value.Month <= denngay.Month
                             && kq.hocsinh_ngaydangky.Value.Year <= denngay.Year
                             select kq).Count();
        solan = solan + "'" + getKhaoSatDat + "',";
        var getKhaoSatKhongDat = (from kq in db.tbWebsite_DangKiTuyenSinhs
                                  where kq.hocsinh_trangthai_dangky == -2
                                 && kq.hocsinh_ngaydangky.Value.Day >= tungay.Day
                             && kq.hocsinh_ngaydangky.Value.Month >= tungay.Month
                             && kq.hocsinh_ngaydangky.Value.Year >= tungay.Year
                             && kq.hocsinh_ngaydangky.Value.Day <= denngay.Day
                             && kq.hocsinh_ngaydangky.Value.Month <= denngay.Month
                             && kq.hocsinh_ngaydangky.Value.Year <= denngay.Year
                                  select kq).Count();
        solan = solan + "'" + getKhaoSatKhongDat + "',";
        var getChuaKhaoSat = (from kq in db.tbWebsite_DangKiTuyenSinhs
                              where kq.hocsinh_trangthai_dangky == 0
                              && kq.hocsinh_ngaydangky.Value.Day >= tungay.Day
                             && kq.hocsinh_ngaydangky.Value.Month >= tungay.Month
                             && kq.hocsinh_ngaydangky.Value.Year >= tungay.Year
                             && kq.hocsinh_ngaydangky.Value.Day <= denngay.Day
                             && kq.hocsinh_ngaydangky.Value.Month <= denngay.Month
                             && kq.hocsinh_ngaydangky.Value.Year <= denngay.Year
                              select kq).Count();
        solan = solan + "'" + getChuaKhaoSat + "',";
        string so_lan = solan.TrimEnd(',');
        txtSoHSDK.Value = so_lan;
        txtTieuDe.Value = "'Đạt','Không đạt','Chưa khảo sát'";
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "text", "drawingPieChart()", true);
    }
    public void ResetData()
    {
        dteTuNgay.Value = "";
        dteDenNgay.Value = "";
    }
    protected void btnXem_ServerClick(object sender, EventArgs e)
    {
        if (dteTuNgay.Value == "" && dteDenNgay.Value == "")
        {
            alert.alert_Warning(Page, "Vui lòng chọn từ ngày đến ngày nào để thống kê!", "");
        }
        else if (Convert.ToDateTime(dteTuNgay.Value) >= Convert.ToDateTime(dteDenNgay.Value))
        {
            ResetData();
            alert.alert_Error(Page, "Dữ liệu ngày không hợp lệ !", "");
        }
        else
        {
            LoadThongKeBieuDoTron(Convert.ToDateTime(dteTuNgay.Value), Convert.ToDateTime(dteDenNgay.Value));
        }
    }
}