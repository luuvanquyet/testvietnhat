﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_BaiVietNoiDungTieuChuan2 : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!IsPostBack)
        {
            edtnoidung.Toolbars.Add(HtmlEditorToolbar.CreateStandardToolbar1());
            if (Request.Cookies["UserName"] == null)
            {
                Response.Redirect("/admin-login");
            }
            else
            {
                var checkNoidung = (from nd in db.tbNoiDungDanhGiaGiaoVienVietNhats
                                    join tk in db.admin_Users on nd.taikhoan_id equals tk.username_id
                                    where tk.username_username == Request.Cookies["UserName"].Value
                                    select nd).SingleOrDefault();
                edtnoidung.Html = checkNoidung.noidungdanhgiagiaovien_minhchung2;
                if (checkNoidung.noidungdanhgiagiaovien_mucdotieuchi3 == "Đạt")
                    txtDatTieuChi3_1.Checked = true;
                if (checkNoidung.noidungdanhgiagiaovien_mucdotieuchi3 == "Khá")
                    txtDatTieuChi3_2.Checked = true;
                if (checkNoidung.noidungdanhgiagiaovien_mucdotieuchi3 == "Tốt")
                    txtDatTieuChi3_3.Checked = true;

                if (checkNoidung.noidungdanhgiagiaovien_mucdotieuchi4 == "Đạt")
                    txtDatTieuChi4_1.Checked = true;
                if (checkNoidung.noidungdanhgiagiaovien_mucdotieuchi4 == "Khá")
                    txtDatTieuChi4_2.Checked = true;
                if (checkNoidung.noidungdanhgiagiaovien_mucdotieuchi4 == "Tốt")
                    txtDatTieuChi4_3.Checked = true;

                if (checkNoidung.noidungdanhgiagiaovien_mucdotieuchi5 == "Đạt")
                    txtDatTieuChi5_1.Checked = true;
                if (checkNoidung.noidungdanhgiagiaovien_mucdotieuchi5 == "Khá")
                    txtDatTieuChi5_2.Checked = true;
                if (checkNoidung.noidungdanhgiagiaovien_mucdotieuchi5 == "Tốt")
                    txtDatTieuChi5_3.Checked = true;

                if (checkNoidung.noidungdanhgiagiaovien_mucdotieuchi6 == "Đạt")
                    txtDatTieuChi6_1.Checked = true;
                if (checkNoidung.noidungdanhgiagiaovien_mucdotieuchi6 == "Khá")
                    txtDatTieuChi6_2.Checked = true;
                if (checkNoidung.noidungdanhgiagiaovien_mucdotieuchi6 == "Tốt")
                    txtDatTieuChi6_3.Checked = true;

                if (checkNoidung.noidungdanhgiagiaovien_mucdotieuchi7 == "Đạt")
                    txtDatTieuChi7_1.Checked = true;
                if (checkNoidung.noidungdanhgiagiaovien_mucdotieuchi7 == "Khá")
                    txtDatTieuChi7_2.Checked = true;
                if (checkNoidung.noidungdanhgiagiaovien_mucdotieuchi7 == "Tốt")
                    txtDatTieuChi7_3.Checked = true;
            }
        }
    }

    private void setNULL()
    {
        edtnoidung.Html = "";
    }

    protected void btnLuu_Click(object sender, EventArgs e)
    {
        var checkTaiKhoan = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
        if (checkTaiKhoan != null)
        {

            // tiêu chí 3
            string tieuchicuatieuchuan3 = "";
            if (txtDatTieuChi3_1.Checked == true)
            {
                tieuchicuatieuchuan3 = "Đạt";
            }
            else if (txtDatTieuChi3_2.Checked == true)
            {
                tieuchicuatieuchuan3 = "Khá";
            }
            else if (txtDatTieuChi3_3.Checked == true)
            {
                tieuchicuatieuchuan3 = "Tốt";
            }
            else
            {
                alert.alert_Error(Page, "Vui lòng chọn mức độ tiêu chi 3 để đánh giá", "");
            }
            // Tiêu chí 4
            string tieuchicuatieuchuan4 = "";
            if (txtDatTieuChi4_1.Checked == true)
            {
                tieuchicuatieuchuan4 = "Đạt";
            }
            else if (txtDatTieuChi4_2.Checked == true)
            {
                tieuchicuatieuchuan4 = "Khá";
            }
            else if (txtDatTieuChi4_3.Checked == true)
            {
                tieuchicuatieuchuan4 = "Tốt";
            }
            else
            {
                alert.alert_Error(Page, "Vui lòng chọn mức độ tiêu chi 4 để đánh giá", "");
            }
            // Tiêu chí 5
            string tieuchicuatieuchuan5 = "";
            if (txtDatTieuChi5_1.Checked == true)
            {
                tieuchicuatieuchuan5 = "Đạt";
            }
            else if (txtDatTieuChi5_2.Checked == true)
            {
                tieuchicuatieuchuan5 = "Khá";
            }
            else if (txtDatTieuChi5_3.Checked == true)
            {
                tieuchicuatieuchuan5 = "Tốt";
            }
            else
            {
                alert.alert_Error(Page, "Vui lòng chọn mức độ tiêu chi 5 để đánh giá", "");
            }
            string tieuchicuatieuchuan6 = "";
            if (txtDatTieuChi6_1.Checked == true)
            {
                tieuchicuatieuchuan6 = "Đạt";
            }
            else if (txtDatTieuChi6_2.Checked == true)
            {
                tieuchicuatieuchuan6 = "Khá";
            }
            else if (txtDatTieuChi6_3.Checked == true)
            {
                tieuchicuatieuchuan6 = "Tốt";
            }
            else
            {
                alert.alert_Error(Page, "Vui lòng chọn mức độ tiêu chi 6 để đánh giá", "");
            }
            string tieuchicuatieuchuan7 = "";
            if (txtDatTieuChi7_1.Checked == true)
            {
                tieuchicuatieuchuan7 = "Đạt";
            }
            else if (txtDatTieuChi7_2.Checked == true)
            {
                tieuchicuatieuchuan7 = "Khá";
            }
            else if (txtDatTieuChi7_3.Checked == true)
            {
                tieuchicuatieuchuan7 = "Tốt";
            }
            else
            {
                alert.alert_Error(Page, "Vui lòng chọn mức độ tiêu chi 7 để đánh giá", "");
            }
            try
            {
                // tiêu chí 1
                if (tieuchicuatieuchuan3 != "" && tieuchicuatieuchuan4 != "" && tieuchicuatieuchuan5 != "" && tieuchicuatieuchuan6 != "" && tieuchicuatieuchuan7 != "")
                {
                    tbNoiDungDanhGiaGiaoVienVietNhat checkNoiDungCuaTaiKhoan = (from tk in db.tbNoiDungDanhGiaGiaoVienVietNhats where tk.taikhoan_id == checkTaiKhoan.username_id select tk).SingleOrDefault();
                    if (checkNoiDungCuaTaiKhoan != null)
                    {
                        checkNoiDungCuaTaiKhoan.noidungdanhgiagiaovien_minhchung2 = edtnoidung.Html;
                        checkNoiDungCuaTaiKhoan.noidungdanhgiagiaovien_dateupdate = DateTime.Now;
                        checkNoiDungCuaTaiKhoan.noidungdanhgiagiaovien_solanupdate = checkNoiDungCuaTaiKhoan.noidungdanhgiagiaovien_solanupdate + 1;
                        checkNoiDungCuaTaiKhoan.noidungdanhgiagiaovien_mucdotieuchi3 = tieuchicuatieuchuan3;
                        checkNoiDungCuaTaiKhoan.noidungdanhgiagiaovien_mucdotieuchi4 = tieuchicuatieuchuan4;
                        checkNoiDungCuaTaiKhoan.noidungdanhgiagiaovien_mucdotieuchi5 = tieuchicuatieuchuan5;
                        checkNoiDungCuaTaiKhoan.noidungdanhgiagiaovien_mucdotieuchi6 = tieuchicuatieuchuan6;
                        checkNoiDungCuaTaiKhoan.noidungdanhgiagiaovien_mucdotieuchi7 = tieuchicuatieuchuan7;
                        checkNoiDungCuaTaiKhoan.taikhoan_id = checkTaiKhoan.username_id;
                        db.SubmitChanges();
                        alert.alert_Success(Page, "Lưu thành công", "");
                    }
                   
                }
            }
            catch (Exception ex)
            {
                alert.alert_Error(Page, "Error", "");
            }
        }
    }
}