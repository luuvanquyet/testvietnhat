﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_BaiVietNoiDungTieuChuan3 : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {
       
        if (!IsPostBack)
        {
            edtnoidung.Toolbars.Add(HtmlEditorToolbar.CreateStandardToolbar1());
            if (Request.Cookies["UserName"] == null)
            {
                Response.Redirect("/admin-login");
            }
            else
            {
                var checkNoidung = (from nd in db.tbNoiDungDanhGiaGiaoVienVietNhats
                                    join tk in db.admin_Users on nd.taikhoan_id equals tk.username_id
                                    where tk.username_username == Request.Cookies["UserName"].Value
                                    select nd).SingleOrDefault();
                edtnoidung.Html = checkNoidung.noidungdanhgiagiaovien_minhchung3;
                if (checkNoidung.noidungdanhgiagiaovien_mucdotieuchi8 == "Đạt")
                    txtDatTieuChi1_1.Checked = true;
                if (checkNoidung.noidungdanhgiagiaovien_mucdotieuchi8 == "Khá")
                    txtDatTieuChi1_2.Checked = true;
                if (checkNoidung.noidungdanhgiagiaovien_mucdotieuchi8 == "Tốt")
                    txtDatTieuChi1_3.Checked = true;
                if (checkNoidung.noidungdanhgiagiaovien_mucdotieuchi9 == "Đạt")
                    txtDatTieuChi2_1.Checked = true;
                if (checkNoidung.noidungdanhgiagiaovien_mucdotieuchi9 == "Khá")
                    txtDatTieuChi2_2.Checked = true;
                if (checkNoidung.noidungdanhgiagiaovien_mucdotieuchi9 == "Tốt")
                    txtDatTieuChi2_3.Checked = true;
                if (checkNoidung.noidungdanhgiagiaovien_mucdotieuchi10 == "Đạt")
                    txtDatTieuChi10_1.Checked = true;
                if (checkNoidung.noidungdanhgiagiaovien_mucdotieuchi10 == "Khá")
                    txtDatTieuChi10_2.Checked = true;
                if (checkNoidung.noidungdanhgiagiaovien_mucdotieuchi10 == "Tốt")
                    txtDatTieuChi10_3.Checked = true;
            }
        }
    }

    private void setNULL()
    {
        edtnoidung.Html = "";
    }

    protected void btnLuu_Click(object sender, EventArgs e)
    {
        var checkTaiKhoan = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
        if (checkTaiKhoan != null)
        {
            string tieuchicuatieuchuan8 = "";
            if (txtDatTieuChi1_1.Checked == true)
            {
                tieuchicuatieuchuan8 = "Đạt";
            }
            else if (txtDatTieuChi1_2.Checked == true)
            {
                tieuchicuatieuchuan8 = "Khá";
            }
            else if (txtDatTieuChi1_3.Checked == true)
            {
                tieuchicuatieuchuan8 = "Tốt";
            }
            else
            {
                alert.alert_Error(Page, "Vui lòng chọn mức độ tiêu chi 8 để đánh giá", "");
            }
            string tieuchicuatieuchuan9 = "";
            if (txtDatTieuChi2_1.Checked == true)
            {
                tieuchicuatieuchuan9 = "Đạt";
            }
            else if (txtDatTieuChi2_2.Checked == true)
            {
                tieuchicuatieuchuan9 = "Khá";
            }
            else if (txtDatTieuChi2_3.Checked == true)
            {
                tieuchicuatieuchuan9 = "Tốt";
            }
            else
            {
                alert.alert_Error(Page, "Vui lòng chọn mức độ tiêu chi 9 để đánh giá", "");
            }
            string tieuchicuatieuchuan10 = "";
            if (txtDatTieuChi10_1.Checked == true)
            {
                tieuchicuatieuchuan10 = "Đạt";
            }
            else if (txtDatTieuChi10_2.Checked == true)
            {
                tieuchicuatieuchuan10 = "Khá";
            }
            else if (txtDatTieuChi10_3.Checked == true)
            {
                tieuchicuatieuchuan10 = "Tốt";
            }
            else
            {
                alert.alert_Error(Page, "Vui lòng chọn mức độ tiêu chi 10 để đánh giá", "");
            }
            try
            {
                // tiêu chí 1
                if (tieuchicuatieuchuan8 != "" && tieuchicuatieuchuan9 != "" && tieuchicuatieuchuan10 != "")
                {
                    tbNoiDungDanhGiaGiaoVienVietNhat checkNoiDungCuaTaiKhoan = (from tk in db.tbNoiDungDanhGiaGiaoVienVietNhats where tk.taikhoan_id == checkTaiKhoan.username_id select tk).SingleOrDefault();
                    if (checkNoiDungCuaTaiKhoan != null)
                    {
                        checkNoiDungCuaTaiKhoan.noidungdanhgiagiaovien_minhchung3 = edtnoidung.Html;
                        //insert.noidungdanhgiagiaovien_mucdodatduoccuatieuchi = ddlMucDoTieuChuan1.Text;
                        checkNoiDungCuaTaiKhoan.noidungdanhgiagiaovien_dateupdate = DateTime.Now;
                        checkNoiDungCuaTaiKhoan.noidungdanhgiagiaovien_solanupdate = checkNoiDungCuaTaiKhoan.noidungdanhgiagiaovien_solanupdate + 1;
                        checkNoiDungCuaTaiKhoan.noidungdanhgiagiaovien_mucdotieuchi8 = tieuchicuatieuchuan8;
                        checkNoiDungCuaTaiKhoan.noidungdanhgiagiaovien_mucdotieuchi9 = tieuchicuatieuchuan9;
                        checkNoiDungCuaTaiKhoan.noidungdanhgiagiaovien_mucdotieuchi10 = tieuchicuatieuchuan10;
                        checkNoiDungCuaTaiKhoan.taikhoan_id = checkTaiKhoan.username_id;
                        db.SubmitChanges();
                        alert.alert_Success(Page, "Lưu thành công", "");
                    }
                }
            }
            catch (Exception ex)
            {
                alert.alert_Error(Page, "Error", "");
            }
        }
    }
}