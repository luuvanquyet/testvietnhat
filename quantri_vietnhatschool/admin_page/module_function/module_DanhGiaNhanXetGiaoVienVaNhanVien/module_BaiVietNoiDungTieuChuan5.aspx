﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_BaiVietNoiDungTieuChuan5.aspx.cs" Inherits="admin_page_module_function_module_BaiVietNoiDungTieuChuan5" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v17.1" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
     <style>
        .label-checkbox {
            display: inline-block;
            position: relative;
            padding-left: 28px;
            margin-bottom: 12px;
            cursor: pointer;
            font-size: 16px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            line-height: 23px;
        }

            .label-checkbox:nth-child(2) {
                margin: 0 15px;
            }

            .label-checkbox input[type="radio"] {
                position: absolute;
                opacity: 0;
                cursor: pointer;
                height: 0;
                width: 0;
                vertical-align: middle;
            }

            .label-checkbox .checkmark {
                position: absolute;
                top: 0;
                left: 0;
                height: 20px;
                width: 20px;
                background-color: #eee;
            }

            .label-checkbox input[type="radio"]:checked ~ .checkmark {
                background-color: #2196F3;
            }

            .label-checkbox .checkmark:after {
                content: "";
                position: absolute;
                display: none;
            }

            .label-checkbox input[type="radio"]:checked ~ .checkmark:after {
                display: block;
            }

            .label-checkbox .checkmark:after {
                left: 9px;
                top: 5px;
                width: 5px;
                height: 10px;
                border: solid white;
                border-width: 0 3px 3px 0;
                -webkit-transform: rotate(45deg);
                -ms-transform: rotate(45deg);
                transform: rotate(45deg);
            }
    </style>
    <div class="card card-block">
        <asp:UpdatePanel ID="udPopup" runat="server">
            <ContentTemplate>
                <div class="popup-main">
                    <%-- Start tiêu chuẩn 1--%>
                    <div class="div_content col-12">
                        <div class="col-12">
                            <div class="col-12 form-group">
                                <label>Tiêu chuẩn 5</label>
                                <hr />
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="col-12 form-group">
                                <label class="col-12 form-control-label">Minh chứng:</label>
                            </div>
                            <div class="col-12 form-group">
                                <div class="col-12">
                                    <dx:ASPxHtmlEditor ID="edtnoidung" ClientInstanceName="edtnoidung" runat="server" Width="100%" Height="400px" Border-BorderStyle="Solid" Border-BorderWidth="1px" Border-BorderColor="#dddddd">
                                        <SettingsHtmlEditing AllowIFrames="true" AllowYouTubeVideoIFrames="True" EnablePasteOptions="true" />
                                        <Settings AllowHtmlView="true" AllowContextMenu="Default" />
                                        <settingsimageupload uploadfolder="~/editorimages"></settingsimageupload>
                                        <Toolbars>
                                            <dx:HtmlEditorToolbar>
                                                <Items>
                                                    <dx:ToolbarCustomCssEdit Width="120px">
                                                        <Items>
                                                            <dx:ToolbarCustomCssListEditItem TagName="" Text="Clear Style" CssClass="" />
                                                            <dx:ToolbarCustomCssListEditItem TagName="H1" Text="Title" CssClass="CommonTitle">
                                                                <PreviewStyle CssClass="CommonTitlePreview" />
                                                            </dx:ToolbarCustomCssListEditItem>
                                                            <dx:ToolbarCustomCssListEditItem TagName="H3" Text="Header1" CssClass="CommonHeader1">
                                                                <PreviewStyle CssClass="CommonHeader1Preview" />
                                                            </dx:ToolbarCustomCssListEditItem>
                                                            <dx:ToolbarCustomCssListEditItem TagName="H4" Text="Header2" CssClass="CommonHeader2">
                                                                <PreviewStyle CssClass="CommonHeader2Preview" />
                                                            </dx:ToolbarCustomCssListEditItem>
                                                            <dx:ToolbarCustomCssListEditItem TagName="Div" Text="Content" CssClass="CommonContent">
                                                                <PreviewStyle CssClass="CommonContentPreview" />
                                                            </dx:ToolbarCustomCssListEditItem>
                                                            <dx:ToolbarCustomCssListEditItem TagName="Strong" Text="Features" CssClass="CommonFeatures">
                                                                <PreviewStyle CssClass="CommonFeaturesPreview" />
                                                            </dx:ToolbarCustomCssListEditItem>
                                                            <dx:ToolbarCustomCssListEditItem TagName="Div" Text="Footer" CssClass="CommonFooter">
                                                                <PreviewStyle CssClass="CommonFooterPreview" />
                                                            </dx:ToolbarCustomCssListEditItem>
                                                            <dx:ToolbarCustomCssListEditItem TagName="" Text="Link" CssClass="Link">
                                                                <PreviewStyle CssClass="LinkPreview" />
                                                            </dx:ToolbarCustomCssListEditItem>
                                                            <dx:ToolbarCustomCssListEditItem TagName="EM" Text="ImageTitle" CssClass="ImageTitle">
                                                                <PreviewStyle CssClass="ImageTitlePreview" />
                                                            </dx:ToolbarCustomCssListEditItem>
                                                            <dx:ToolbarCustomCssListEditItem TagName="" Text="ImageMargin" CssClass="ImageMargin">
                                                                <PreviewStyle CssClass="ImageMarginPreview" />
                                                            </dx:ToolbarCustomCssListEditItem>
                                                        </Items>
                                                    </dx:ToolbarCustomCssEdit>
                                                    <dx:ToolbarParagraphFormattingEdit>
                                                        <Items>
                                                            <dx:ToolbarListEditItem Text="Normal" Value="p" />
                                                            <dx:ToolbarListEditItem Text="Heading  1" Value="h1" />
                                                            <dx:ToolbarListEditItem Text="Heading  2" Value="h2" />
                                                            <dx:ToolbarListEditItem Text="Heading  3" Value="h3" />
                                                            <dx:ToolbarListEditItem Text="Heading  4" Value="h4" />
                                                            <dx:ToolbarListEditItem Text="Heading  5" Value="h5" />
                                                            <dx:ToolbarListEditItem Text="Heading  6" Value="h6" />
                                                            <dx:ToolbarListEditItem Text="Address" Value="address" />
                                                            <dx:ToolbarListEditItem Text="Normal (DIV)" Value="div" />
                                                        </Items>
                                                    </dx:ToolbarParagraphFormattingEdit>
                                                    <dx:ToolbarFontNameEdit>
                                                        <Items>
                                                            <dx:ToolbarListEditItem Value="Times New Roman" Text="Times New Roman"></dx:ToolbarListEditItem>
                                                            <dx:ToolbarListEditItem Value="Tahoma" Text="Tahoma"></dx:ToolbarListEditItem>
                                                            <dx:ToolbarListEditItem Value="Verdana" Text="Verdana"></dx:ToolbarListEditItem>
                                                            <dx:ToolbarListEditItem Value="Arial" Text="Arial"></dx:ToolbarListEditItem>
                                                            <dx:ToolbarListEditItem Value="MS Sans Serif" Text="MS Sans Serif"></dx:ToolbarListEditItem>
                                                            <dx:ToolbarListEditItem Value="Courier" Text="Courier"></dx:ToolbarListEditItem>
                                                            <dx:ToolbarListEditItem Value="bodoni MT" Text="bodoni MT"></dx:ToolbarListEditItem>
                                                        </Items>
                                                    </dx:ToolbarFontNameEdit>
                                                    <dx:ToolbarFontSizeEdit>
                                                        <Items>
                                                            <dx:ToolbarListEditItem Value="1" Text="1 (8pt)"></dx:ToolbarListEditItem>
                                                            <dx:ToolbarListEditItem Value="2" Text="2 (10pt)"></dx:ToolbarListEditItem>
                                                            <dx:ToolbarListEditItem Value="3" Text="3 (12pt)"></dx:ToolbarListEditItem>
                                                            <dx:ToolbarListEditItem Value="4" Text="4 (14pt)"></dx:ToolbarListEditItem>
                                                            <dx:ToolbarListEditItem Value="5" Text="5 (18pt)"></dx:ToolbarListEditItem>
                                                            <dx:ToolbarListEditItem Value="6" Text="6 (24pt)"></dx:ToolbarListEditItem>
                                                            <dx:ToolbarListEditItem Value="7" Text="7 (36pt)"></dx:ToolbarListEditItem>
                                                        </Items>
                                                    </dx:ToolbarFontSizeEdit>
                                                    <dx:ToolbarBoldButton BeginGroup="True" />
                                                    <dx:ToolbarItalicButton />
                                                    <dx:ToolbarUnderlineButton />
                                                    <dx:ToolbarStrikethroughButton />
                                                    <dx:ToolbarJustifyLeftButton BeginGroup="True" />
                                                    <dx:ToolbarJustifyCenterButton />
                                                    <dx:ToolbarJustifyRightButton />
                                                    <dx:ToolbarJustifyFullButton />
                                                    <dx:ToolbarBackColorButton BeginGroup="True" />
                                                    <dx:ToolbarFontColorButton />
                                                    <dx:ToolbarInsertYouTubeVideoDialogButton>
                                                    </dx:ToolbarInsertYouTubeVideoDialogButton>
                                                </Items>
                                            </dx:HtmlEditorToolbar>
                                        </Toolbars>
                                    </dx:ASPxHtmlEditor>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <label>Mức độ đạt được ở tiêu chí 14</label>
                            <div class="col-12 form-group">
                                <label class="label-checkbox font-weight-normal">
                                    Đạt
                                        <input type="radio" id="txtDatTieuChi1_1" value="Đạt" runat="server" name="imgavatart" />
                                    <span class="checkmark"></span>
                                </label>
                                <label class="label-checkbox font-weight-normal">
                                    Khá
                                        <input type="radio" id="txtDatTieuChi1_2" value="Khá" runat="server" name="imgavatart" />
                                    <span class="checkmark"></span>
                                </label>
                                <label class="label-checkbox font-weight-normal">
                                    Tốt
                                        <input type="radio" id="txtDatTieuChi1_3" value="Tốt" runat="server" name="imgavatart" />
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-12">
                            <label>Mức độ đạt được ở tiêu chí 15</label>
                            <div class="col-12 form-group">
                                <label class="label-checkbox font-weight-normal">
                                    Đạt
                                        <input type="radio" id="txtDatTieuChi2_1" value="Đạt" runat="server" name="imgavatart2" />
                                    <span class="checkmark"></span>
                                </label>
                                <label class="label-checkbox font-weight-normal">
                                    Khá
                                        <input type="radio" id="txtDatTieuChi2_2" value="Khá" runat="server" name="imgavatart2" />
                                    <span class="checkmark"></span>
                                </label>
                                <label class="label-checkbox font-weight-normal">
                                    Tốt
                                        <input type="radio" id="txtDatTieuChi2_3" value="Tốt" runat="server" name="imgavatart2" />
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <%-- <div>
                        <input id="txtTieuChuan1" runat="server" />
                    </div>--%>
                    <%-- End tiêu chuẩn 1--%>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="mar_but button">
            <asp:UpdatePanel ID="pnLuu" runat="server">
                <ContentTemplate>
                    <a href="../../admin-noi-dung-bai-viet-tieu-chuan-4" class="btn btn-primary">Quay lại</a>
                    <asp:Button ID="btnLuu" runat="server" ClientIDMode="Static" Text="Hoàn thành" CssClass="btn btn-primary" OnClick="btnLuu_Click" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

