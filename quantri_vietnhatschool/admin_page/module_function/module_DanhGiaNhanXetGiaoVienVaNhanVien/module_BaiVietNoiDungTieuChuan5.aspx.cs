﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_BaiVietNoiDungTieuChuan5 : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            edtnoidung.Toolbars.Add(HtmlEditorToolbar.CreateStandardToolbar1());
            if (Request.Cookies["UserName"] == null)
            {
                Response.Redirect("/admin-login");
            }
            else
            {
                var checkNoidung = (from nd in db.tbNoiDungDanhGiaGiaoVienVietNhats
                                    join tk in db.admin_Users on nd.taikhoan_id equals tk.username_id
                                    where tk.username_username == Request.Cookies["UserName"].Value
                                    select nd).SingleOrDefault();
                edtnoidung.Html = checkNoidung.noidungdanhgiagiaovien_minhchung5;
                if (checkNoidung.noidungdanhgiagiaovien_mucdotieuchi14 == "Đạt")
                    txtDatTieuChi1_1.Checked = true;
                if (checkNoidung.noidungdanhgiagiaovien_mucdotieuchi14 == "Khá")
                    txtDatTieuChi1_2.Checked = true;
                if (checkNoidung.noidungdanhgiagiaovien_mucdotieuchi14 == "Tốt")
                    txtDatTieuChi1_3.Checked = true;
                if (checkNoidung.noidungdanhgiagiaovien_mucdotieuchi15 == "Đạt")
                    txtDatTieuChi2_1.Checked = true;
                if (checkNoidung.noidungdanhgiagiaovien_mucdotieuchi15 == "Khá")
                    txtDatTieuChi2_2.Checked = true;
                if (checkNoidung.noidungdanhgiagiaovien_mucdotieuchi15 == "Tốt")
                    txtDatTieuChi2_3.Checked = true;
            }
        }
    }

    private void setNULL()
    {
        edtnoidung.Html = "";
    }

    protected void btnLuu_Click(object sender, EventArgs e)
    {
        var checkTaiKhoan = (from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value select tk).SingleOrDefault();
        if (checkTaiKhoan != null)
        {
            string tieuchicuatieuchuan1 = "";
            if (txtDatTieuChi1_1.Checked == true)
            {
                tieuchicuatieuchuan1 = "Đạt";
            }
            else if (txtDatTieuChi1_2.Checked == true)
            {
                tieuchicuatieuchuan1 = "Khá";
            }
            else if (txtDatTieuChi1_3.Checked == true)
            {
                tieuchicuatieuchuan1 = "Tốt";
            }
            else
            {
                alert.alert_Error(Page, "Vui lòng chọn mức độ tiêu chi 14 để đánh giá", "");
            }
            string tieuchicuatieuchuan2 = "";
            if (txtDatTieuChi2_1.Checked == true)
            {
                tieuchicuatieuchuan2 = "Đạt";
            }
            else if (txtDatTieuChi2_2.Checked == true)
            {
                tieuchicuatieuchuan2 = "Khá";
            }
            else if (txtDatTieuChi2_3.Checked == true)
            {
                tieuchicuatieuchuan2 = "Tốt";
            }
            else
            {
                alert.alert_Error(Page, "Vui lòng chọn mức độ tiêu chi 15 để đánh giá", "");
            }
            try
            {
                // tiêu chí 1
                if (tieuchicuatieuchuan1 != "" && tieuchicuatieuchuan2 != "")
                {
                    tbNoiDungDanhGiaGiaoVienVietNhat checkNoiDungCuaTaiKhoan = (from tk in db.tbNoiDungDanhGiaGiaoVienVietNhats where tk.taikhoan_id == checkTaiKhoan.username_id select tk).SingleOrDefault();
                    if (checkNoiDungCuaTaiKhoan != null)
                    {
                        checkNoiDungCuaTaiKhoan.noidungdanhgiagiaovien_minhchung5 = edtnoidung.Html;
                        checkNoiDungCuaTaiKhoan.noidungdanhgiagiaovien_dateupdate = DateTime.Now;
                        checkNoiDungCuaTaiKhoan.noidungdanhgiagiaovien_solanupdate = checkNoiDungCuaTaiKhoan.noidungdanhgiagiaovien_solanupdate + 1;
                        checkNoiDungCuaTaiKhoan.noidungdanhgiagiaovien_mucdotieuchi14 = tieuchicuatieuchuan1;
                        checkNoiDungCuaTaiKhoan.noidungdanhgiagiaovien_mucdotieuchi15 = tieuchicuatieuchuan2;
                        checkNoiDungCuaTaiKhoan.taikhoan_id = checkTaiKhoan.username_id;
                        db.SubmitChanges();
                        alert.alert_Success(Page, "Lưu thành công", "");
                    }
                }
            }
            catch (Exception ex)
            {
                alert.alert_Error(Page, "Error", "");
            }
        }
    }
}