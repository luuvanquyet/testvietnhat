﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_KetQuaTongKet : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["UserName"].Value != "admin")
        {
            var list = from l in db.tbNoiDungDanhGiaGiaoVienVietNhats
                       join a in db.admin_Users on l.taikhoan_id equals a.username_id
                       where a.username_username == Request.Cookies["UserName"].Value
                       select new
                       {
                           a.username_id,
                           a.username_fullname,
                           l.noidungdanhgiagiaovien_mucdotieuchi1,
                           l.noidungdanhgiagiaovien_mucdotieuchi2,
                           l.noidungdanhgiagiaovien_mucdotieuchi3,
                           l.noidungdanhgiagiaovien_mucdotieuchi4,
                           l.noidungdanhgiagiaovien_mucdotieuchi5,
                           l.noidungdanhgiagiaovien_mucdotieuchi6,
                           l.noidungdanhgiagiaovien_mucdotieuchi7,
                           l.noidungdanhgiagiaovien_mucdotieuchi8,
                           l.noidungdanhgiagiaovien_mucdotieuchi9,
                           l.noidungdanhgiagiaovien_mucdotieuchi10,
                           l.noidungdanhgiagiaovien_mucdotieuchi11,
                           l.noidungdanhgiagiaovien_mucdotieuchi12,
                           l.noidungdanhgiagiaovien_mucdotieuchi13,
                           l.noidungdanhgiagiaovien_mucdotieuchi14,
                           l.noidungdanhgiagiaovien_mucdotieuchi15,
                       };
            if (list.Count()>0)
            {
               // dvThem.Visible = false;
            }
            rpList.DataSource = list;
            rpList.DataBind();
        }
        else
        {
            var list = from l in db.tbNoiDungDanhGiaGiaoVienVietNhats
                       join a in db.admin_Users on l.taikhoan_id equals a.username_id
                       select new
                       {
                           a.username_id,
                           a.username_fullname,
                           l.noidungdanhgiagiaovien_mucdotieuchi1,
                           l.noidungdanhgiagiaovien_mucdotieuchi2,
                           l.noidungdanhgiagiaovien_mucdotieuchi3,
                           l.noidungdanhgiagiaovien_mucdotieuchi4,
                           l.noidungdanhgiagiaovien_mucdotieuchi5,
                           l.noidungdanhgiagiaovien_mucdotieuchi6,
                           l.noidungdanhgiagiaovien_mucdotieuchi7,
                           l.noidungdanhgiagiaovien_mucdotieuchi8,
                           l.noidungdanhgiagiaovien_mucdotieuchi9,
                           l.noidungdanhgiagiaovien_mucdotieuchi10,
                           l.noidungdanhgiagiaovien_mucdotieuchi11,
                           l.noidungdanhgiagiaovien_mucdotieuchi12,
                           l.noidungdanhgiagiaovien_mucdotieuchi13,
                           l.noidungdanhgiagiaovien_mucdotieuchi14,
                           l.noidungdanhgiagiaovien_mucdotieuchi15,
                       };
            rpList.DataSource = list;
            rpList.DataBind();
          //  dvThem.Visible = false;
        }

    }
}