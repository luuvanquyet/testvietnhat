﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_QuanLyNoiDung : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    public string hanhdong;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["UserName"].Value != "bld01")
        {
            dv_BoPhan.Visible = false;
            btnXem.Visible = false;
            // Danh sách dành cho giáo viên
            var checkTruongBoPhan = from tk in db.admin_Users where tk.username_username == Request.Cookies["UserName"].Value && tk.username_truongbophan == true select tk;
            if (checkTruongBoPhan.Count() > 0)
            {
                var listTuTruongBoPhan = from l in db.tbNoiDungDanhGiaGiaoVienVietNhats
                                         join a in db.admin_Users on l.taikhoan_id equals a.username_id
                                         where a.username_username == Request.Cookies["UserName"].Value
                                         select new
                                         {
                                             a.username_id,
                                             a.username_fullname,
                                             l.mucdo1_truongbophan,
                                             l.mucdo2_truongbophan,
                                             l.mucdo3_truongbophan,
                                             l.mucdo4_truongbophan,
                                             l.mucdo5_truongbophan,
                                             l.mucdo6_truongbophan,
                                             l.mucdo7_truongbophan,
                                             l.mucdo8_truongbophan,
                                             l.mucdo9_truongbophan,
                                             l.mucdo10_truongbophan,
                                             l.mucdo11_truongbophan,
                                             l.mucdo12_truongbophan,
                                             l.mucdo13_truongbophan,
                                             l.mucdo14_truongbophan,
                                             l.mucdo15_truongbophan,
                                             l.noidungdanhgiagiaovien_ketquatongket,
                                         };
                if (listTuTruongBoPhan.Count() > 0)
                {
                    dvThem.Visible = false;
                    block_3.Visible = false;
                }
                rpTongKetTuTruongBoPhan.DataSource = listTuTruongBoPhan;
                rpTongKetTuTruongBoPhan.DataBind();
            }
            var list1 = (from l in db.tbNoiDungDanhGiaGiaoVienVietNhats
                         join a in db.admin_Users on l.taikhoan_id equals a.username_id
                         where a.username_username == Request.Cookies["UserName"].Value && l.active == null
                         orderby l.noidungdanhgiagiaovien_id descending
                         select new
                         {
                             a.username_id,
                             a.username_fullname,
                             l.noidungdanhgiagiaovien_mucdotieuchi1,
                             l.noidungdanhgiagiaovien_mucdotieuchi2,
                             l.noidungdanhgiagiaovien_mucdotieuchi3,
                             l.noidungdanhgiagiaovien_mucdotieuchi4,
                             l.noidungdanhgiagiaovien_mucdotieuchi5,
                             l.noidungdanhgiagiaovien_mucdotieuchi6,
                             l.noidungdanhgiagiaovien_mucdotieuchi7,
                             l.noidungdanhgiagiaovien_mucdotieuchi8,
                             l.noidungdanhgiagiaovien_mucdotieuchi9,
                             l.noidungdanhgiagiaovien_mucdotieuchi10,
                             l.noidungdanhgiagiaovien_mucdotieuchi11,
                             l.noidungdanhgiagiaovien_mucdotieuchi12,
                             l.noidungdanhgiagiaovien_mucdotieuchi13,
                             l.noidungdanhgiagiaovien_mucdotieuchi14,
                             l.noidungdanhgiagiaovien_mucdotieuchi15,
                             active = l.active == true ? "HT" : "Xem",
                         }).Take(1);
            if (list1.Count() > 0)
            {
                dvThem.Visible = false;
                block_2.Visible = false;
                block_3.Visible = false;
            }
            else
            {
                dvThem.Visible = true;
                block_2.Visible = false;
                block_3.Visible = false;
            }
            rpList.DataSource = list1;
            rpList.DataBind();

        }
        else
        {
            if (!IsPostBack)
            {
                var listNV = from nv in db.tbBoPhans where nv.hidden == true select nv;
                ddlBoPhan.Items.Clear();
                ddlBoPhan.Items.Insert(0, "Chọn bộ phận");
                ddlBoPhan.AppendDataBoundItems = true;
                ddlBoPhan.DataTextField = "bophan_name";
                ddlBoPhan.DataValueField = "bophan_id";
                ddlBoPhan.DataSource = listNV;
                ddlBoPhan.DataBind();
            }
            var listTuGiaoVien = from l in db.tbNoiDungDanhGiaGiaoVienVietNhats
                                 join a in db.admin_Users on l.taikhoan_id equals a.username_id
                                 select new
                                 {
                                     a.username_id,
                                     a.username_fullname,
                                     l.noidungdanhgiagiaovien_mucdotieuchi1,
                                     l.noidungdanhgiagiaovien_mucdotieuchi2,
                                     l.noidungdanhgiagiaovien_mucdotieuchi3,
                                     l.noidungdanhgiagiaovien_mucdotieuchi4,
                                     l.noidungdanhgiagiaovien_mucdotieuchi5,
                                     l.noidungdanhgiagiaovien_mucdotieuchi6,
                                     l.noidungdanhgiagiaovien_mucdotieuchi7,
                                     l.noidungdanhgiagiaovien_mucdotieuchi8,
                                     l.noidungdanhgiagiaovien_mucdotieuchi9,
                                     l.noidungdanhgiagiaovien_mucdotieuchi10,
                                     l.noidungdanhgiagiaovien_mucdotieuchi11,
                                     l.noidungdanhgiagiaovien_mucdotieuchi12,
                                     l.noidungdanhgiagiaovien_mucdotieuchi13,
                                     l.noidungdanhgiagiaovien_mucdotieuchi14,
                                     l.noidungdanhgiagiaovien_mucdotieuchi15,
                                     active = l.active == true ? "HT" : "Xem",
                                 };
            rpList.DataSource = listTuGiaoVien;
            rpList.DataBind();
            dvThem.Visible = false;
            // Xem kết quả tổng kết
            var list = from l in db.tbNoiDungDanhGiaGiaoVienVietNhats
                       join a in db.admin_Users on l.taikhoan_id equals a.username_id
                       select new
                       {
                           a.username_id,
                           a.username_fullname,
                           l.mucdo1_truongbophan,
                           l.mucdo2_truongbophan,
                           l.mucdo3_truongbophan,
                           l.mucdo4_truongbophan,
                           l.mucdo5_truongbophan,
                           l.mucdo6_truongbophan,
                           l.mucdo7_truongbophan,
                           l.mucdo8_truongbophan,
                           l.mucdo9_truongbophan,
                           l.mucdo10_truongbophan,
                           l.mucdo11_truongbophan,
                           l.mucdo12_truongbophan,
                           l.mucdo13_truongbophan,
                           l.mucdo14_truongbophan,
                           l.mucdo15_truongbophan,
                           l.noidungdanhgiagiaovien_ketquatongket,
                       };
            rpTongKetTuTruongBoPhan.DataSource = list;
            rpTongKetTuTruongBoPhan.DataBind();
            var listTongKet = from l in db.tbTongKetDanhGiaTieuChuans
                              join a in db.admin_Users on l.taikhoan_id equals a.username_id
                              where l.tongketdanhgia_status == true
                              select new
                              {
                                  a.username_id,
                                  a.username_fullname,
                                  l.tongketdanhgia_mucdotieuchi1,
                                  l.tongketdanhgia_mucdotieuchi2,
                                  l.tongketdanhgia_mucdotieuchi3,
                                  l.tongketdanhgia_mucdotieuchi4,
                                  l.tongketdanhgia_mucdotieuchi5,
                                  l.tongketdanhgia_mucdotieuchi6,
                                  l.tongketdanhgia_mucdotieuchi7,
                                  l.tongketdanhgia_mucdotieuchi8,
                                  l.tongketdanhgia_mucdotieuchi9,
                                  l.tongketdanhgia_mucdotieuchi10,
                                  l.tongketdanhgia_mucdotieuchi11,
                                  l.tongketdanhgia_mucdotieuchi12,
                                  l.tongketdanhgia_mucdotieuchi13,
                                  l.tongketdanhgia_mucdotieuchi14,
                                  l.tongketdanhgia_mucdotieuchi15,
                                  l.tongketdanhgia_xeploai,
                              };
            rpTongKet.DataSource = listTongKet;
            rpTongKet.DataBind();

            block_2.Visible = false;
            block_3.Visible = false;
        }

    }

    protected void btnThem_Click(object sender, EventArgs e)
    {
        Session["_id"] = 0;
        Response.Redirect("admin-noi-dung-bai-viet-tieu-chuan-1");
    }
    protected void btnShow_ServerClick(object sender, EventArgs e)
    {
        if (Request.Cookies["UserName"].Value == "admin")
        {
            Response.Redirect("admin-xem-bai-viet-tu-giao-vien-" + txtUserName.Value);
        }
        Response.Redirect("admin-noi-dung-bai-viet-tieu-chuan-1");
    }

    protected void btnXem_Click(object sender, EventArgs e)
    {
        if (ddlBoPhan.SelectedValue.ToString() == "Chọn bộ phận")
        {
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu để xem!", "");
        }
        else
        {
            var listTuGiaoVien = from l in db.tbNoiDungDanhGiaGiaoVienVietNhats
                                 join a in db.admin_Users on l.taikhoan_id equals a.username_id
                                 join bp in db.tbBoPhans on a.bophan_id equals bp.bophan_id
                                 where bp.bophan_id == Convert.ToInt16(ddlBoPhan.SelectedValue.ToString())
                                 select new
                                 {
                                     a.username_id,
                                     a.username_fullname,
                                     l.noidungdanhgiagiaovien_mucdotieuchi1,
                                     l.noidungdanhgiagiaovien_mucdotieuchi2,
                                     l.noidungdanhgiagiaovien_mucdotieuchi3,
                                     l.noidungdanhgiagiaovien_mucdotieuchi4,
                                     l.noidungdanhgiagiaovien_mucdotieuchi5,
                                     l.noidungdanhgiagiaovien_mucdotieuchi6,
                                     l.noidungdanhgiagiaovien_mucdotieuchi7,
                                     l.noidungdanhgiagiaovien_mucdotieuchi8,
                                     l.noidungdanhgiagiaovien_mucdotieuchi9,
                                     l.noidungdanhgiagiaovien_mucdotieuchi10,
                                     l.noidungdanhgiagiaovien_mucdotieuchi11,
                                     l.noidungdanhgiagiaovien_mucdotieuchi12,
                                     l.noidungdanhgiagiaovien_mucdotieuchi13,
                                     l.noidungdanhgiagiaovien_mucdotieuchi14,
                                     l.noidungdanhgiagiaovien_mucdotieuchi15,
                                     active = l.active == true ? "HT" : "Xem",
                                 };
            rpList.DataSource = listTuGiaoVien;
            rpList.DataBind();
            dvThem.Visible = false;
            // Xem kết quả tổng kết
            var list = from l in db.tbNoiDungDanhGiaGiaoVienVietNhats
                       join a in db.admin_Users on l.taikhoan_id equals a.username_id
                       join bp in db.tbBoPhans on a.bophan_id equals bp.bophan_id
                       where bp.bophan_id == Convert.ToInt16(ddlBoPhan.SelectedValue.ToString())
                       select new
                       {
                           a.username_id,
                           a.username_fullname,
                           l.mucdo1_truongbophan,
                           l.mucdo2_truongbophan,
                           l.mucdo3_truongbophan,
                           l.mucdo4_truongbophan,
                           l.mucdo5_truongbophan,
                           l.mucdo6_truongbophan,
                           l.mucdo7_truongbophan,
                           l.mucdo8_truongbophan,
                           l.mucdo9_truongbophan,
                           l.mucdo10_truongbophan,
                           l.mucdo11_truongbophan,
                           l.mucdo12_truongbophan,
                           l.mucdo13_truongbophan,
                           l.mucdo14_truongbophan,
                           l.mucdo15_truongbophan,
                           l.noidungdanhgiagiaovien_ketquatongket,
                       };
            rpTongKetTuTruongBoPhan.DataSource = list;
            rpTongKetTuTruongBoPhan.DataBind();
            var listTongKet = from l in db.tbTongKetDanhGiaTieuChuans
                              join a in db.admin_Users on l.taikhoan_id equals a.username_id
                              join bp in db.tbBoPhans on a.bophan_id equals bp.bophan_id
                              where bp.bophan_id == Convert.ToInt16(ddlBoPhan.SelectedValue.ToString()) && l.tongketdanhgia_status == true
                              select new
                              {
                                  a.username_id,
                                  a.username_fullname,
                                  l.tongketdanhgia_mucdotieuchi1,
                                  l.tongketdanhgia_mucdotieuchi2,
                                  l.tongketdanhgia_mucdotieuchi3,
                                  l.tongketdanhgia_mucdotieuchi4,
                                  l.tongketdanhgia_mucdotieuchi5,
                                  l.tongketdanhgia_mucdotieuchi6,
                                  l.tongketdanhgia_mucdotieuchi7,
                                  l.tongketdanhgia_mucdotieuchi8,
                                  l.tongketdanhgia_mucdotieuchi9,
                                  l.tongketdanhgia_mucdotieuchi10,
                                  l.tongketdanhgia_mucdotieuchi11,
                                  l.tongketdanhgia_mucdotieuchi12,
                                  l.tongketdanhgia_mucdotieuchi13,
                                  l.tongketdanhgia_mucdotieuchi14,
                                  l.tongketdanhgia_mucdotieuchi15,
                                  l.tongketdanhgia_xeploai,
                              };
            rpTongKet.DataSource = listTongKet;
            rpTongKet.DataBind();
        }
    }
}