﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_QuanLyNoiDungBoPhan.aspx.cs" Inherits="admin_page_module_function_module_QuanLyNoiDungBoPhan" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v17.1" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <script>
        function myNoiDungCuaGiaoVien(account_id) {
            document.getElementById("<%=txtUserName.ClientID%>").value = account_id;
            document.getElementById("<%=btnShow.ClientID%>").click();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div id="block_1" runat="server" class="card card-block">
        <table class="table table-borderless" style="">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th colspan="2" scope="col">Tiêu Chuẩn 1</th>
                    <th colspan="5" scope="col">Tiêu Chuẩn 2</th>
                    <th colspan="3" scope="col">Tiêu Chuẩn 3</th>
                    <th colspan="3" scope="col">Tiêu Chuẩn 4</th>
                    <th colspan="2" scope="col">Tiêu Chuẩn 5</th>
                    <th scope="col">#</th>
                </tr>
            </thead>
            <tbody>
                <tr style="text-align: center">
                    <th scope="row">Giáo viên</th>
                    <td>TC1</td>
                    <td>TC2</td>
                    <td>TC3</td>
                    <td>TC4</td>
                    <td>TC5</td>
                    <td>TC6</td>
                    <td>TC7</td>
                    <td>TC8</td>
                    <td>TC9</td>
                    <td>TC10</td>
                    <td>TC11</td>
                    <td>TC12</td>
                    <td>TC13</td>
                    <td>TC14</td>
                    <td>TC15</td>
                    <th scope="row">Hành động</th>
                </tr>
                <asp:Repeater ID="rpList" runat="server">
                    <ItemTemplate>
                        <tr style="text-align: center">
                            <th scope="row"><%#Eval("username_fullname") %></th>
                            <td><%#Eval("noidungdanhgiagiaovien_mucdotieuchi1") %></td>
                            <td><%#Eval("noidungdanhgiagiaovien_mucdotieuchi2") %></td>
                            <td><%#Eval("noidungdanhgiagiaovien_mucdotieuchi3") %></td>
                            <td><%#Eval("noidungdanhgiagiaovien_mucdotieuchi4") %></td>
                            <td><%#Eval("noidungdanhgiagiaovien_mucdotieuchi5") %></td>
                            <td><%#Eval("noidungdanhgiagiaovien_mucdotieuchi6") %></td>
                            <td><%#Eval("noidungdanhgiagiaovien_mucdotieuchi7") %></td>
                            <td><%#Eval("noidungdanhgiagiaovien_mucdotieuchi8") %></td>
                            <td><%#Eval("noidungdanhgiagiaovien_mucdotieuchi9") %></td>
                            <td><%#Eval("noidungdanhgiagiaovien_mucdotieuchi10") %></td>
                            <td><%#Eval("noidungdanhgiagiaovien_mucdotieuchi11") %></td>
                            <td><%#Eval("noidungdanhgiagiaovien_mucdotieuchi12") %></td>
                            <td><%#Eval("noidungdanhgiagiaovien_mucdotieuchi13") %></td>
                            <td><%#Eval("noidungdanhgiagiaovien_mucdotieuchi14") %></td>
                            <td><%#Eval("noidungdanhgiagiaovien_mucdotieuchi15") %></td>
                            <td>
                                <a id="btnXem" style="color: white" onclick='myNoiDungCuaGiaoVien(<%#Eval("username_id") %>)' class="btn btn-primary">Xem</a>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
        </table>
    </div>
    <div style="display: none">
        <input type="text" id="txtUserName" runat="server" />
        <a id="btnShow" runat="server" onserverclick="btnShow_ServerClick"></a>
    </div>
    <div id="block_2" runat="server" class="card card-block">
        <table class="table table-borderless" style="">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th colspan="2" scope="col">Tiêu Chuẩn 1</th>
                    <th colspan="5" scope="col">Tiêu Chuẩn 2</th>
                    <th colspan="3" scope="col">Tiêu Chuẩn 3</th>
                    <th colspan="3" scope="col">Tiêu Chuẩn 4</th>
                    <th colspan="2" scope="col">Tiêu Chuẩn 5</th>
                    <th scope="col">Tổng kết</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th scope="row">Giáo viên</th>
                    <td>TC1</td>
                    <td>TC2</td>
                    <td>TC3</td>
                    <td>TC4</td>
                    <td>TC5</td>
                    <td>TC6</td>
                    <td>TC7</td>
                    <td>TC8</td>
                    <td>TC9</td>
                    <td>TC10</td>
                    <td>TC11</td>
                    <td>TC12</td>
                    <td>TC13</td>
                    <td>TC14</td>
                    <td>TC15</td>
                    <td>Kết quả</td>
                </tr>
                <asp:Repeater ID="rpTongKet" runat="server">
                    <ItemTemplate>
                        <tr>
                            <th scope="row"><%#Eval("username_fullname") %></th>
                            <td><%#Eval("mucdo1_truongbophan") %></td>
                            <td><%#Eval("mucdo2_truongbophan") %></td>
                            <td><%#Eval("mucdo3_truongbophan") %></td>
                            <td><%#Eval("mucdo4_truongbophan") %></td>
                            <td><%#Eval("mucdo5_truongbophan") %></td>
                            <td><%#Eval("mucdo6_truongbophan") %></td>
                            <td><%#Eval("mucdo7_truongbophan") %></td>
                            <td><%#Eval("mucdo8_truongbophan") %></td>
                            <td><%#Eval("mucdo9_truongbophan") %></td>
                            <td><%#Eval("mucdo10_truongbophan") %></td>
                            <td><%#Eval("mucdo11_truongbophan") %></td>
                            <td><%#Eval("mucdo12_truongbophan") %></td>
                            <td><%#Eval("mucdo13_truongbophan") %></td>
                            <td><%#Eval("mucdo14_truongbophan") %></td>
                            <td><%#Eval("mucdo15_truongbophan") %></td>
                            <td><%#Eval("noidungdanhgiagiaovien_ketquatongket") %> </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

