﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_QuanLyNoiDungBoPhan : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;

    protected void Page_Load(object sender, EventArgs e)
    {
        var checkTruongBoPhan = (from u in db.admin_Users where u.username_username == Request.Cookies["UserName"].Value select u).SingleOrDefault();
        var list = from l in db.tbNoiDungDanhGiaGiaoVienVietNhats
                   join a in db.admin_Users on l.taikhoan_id equals a.username_id
                   where a.bophan_id == checkTruongBoPhan.bophan_id
                   select new
                   {
                       a.username_id,
                       a.username_fullname,
                       l.noidungdanhgiagiaovien_mucdotieuchi1,
                       l.noidungdanhgiagiaovien_mucdotieuchi2,
                       l.noidungdanhgiagiaovien_mucdotieuchi3,
                       l.noidungdanhgiagiaovien_mucdotieuchi4,
                       l.noidungdanhgiagiaovien_mucdotieuchi5,
                       l.noidungdanhgiagiaovien_mucdotieuchi6,
                       l.noidungdanhgiagiaovien_mucdotieuchi7,
                       l.noidungdanhgiagiaovien_mucdotieuchi8,
                       l.noidungdanhgiagiaovien_mucdotieuchi9,
                       l.noidungdanhgiagiaovien_mucdotieuchi10,
                       l.noidungdanhgiagiaovien_mucdotieuchi11,
                       l.noidungdanhgiagiaovien_mucdotieuchi12,
                       l.noidungdanhgiagiaovien_mucdotieuchi13,
                       l.noidungdanhgiagiaovien_mucdotieuchi14,
                       l.noidungdanhgiagiaovien_mucdotieuchi15,
                   };
        rpList.DataSource = list;
        rpList.DataBind();
        //// Xem kết quả tổng kết
        var list1 = from l in db.tbNoiDungDanhGiaGiaoVienVietNhats
                    join a in db.admin_Users on l.taikhoan_id equals a.username_id
                    where a.bophan_id == checkTruongBoPhan.bophan_id && l.active_truongbophan == true
                    select new
                    {
                        a.username_id,
                        a.username_fullname,
                        l.mucdo1_truongbophan,
                        l.mucdo2_truongbophan,
                        l.mucdo3_truongbophan,
                        l.mucdo4_truongbophan,
                        l.mucdo5_truongbophan,
                        l.mucdo6_truongbophan,
                        l.mucdo7_truongbophan,
                        l.mucdo8_truongbophan,
                        l.mucdo9_truongbophan,
                        l.mucdo10_truongbophan,
                        l.mucdo11_truongbophan,
                        l.mucdo12_truongbophan,
                        l.mucdo13_truongbophan,
                        l.mucdo14_truongbophan,
                        l.mucdo15_truongbophan,
                        l.noidungdanhgiagiaovien_ketquatongket
                    };
        rpTongKet.DataSource = list1;
        rpTongKet.DataBind();
    }
    protected void btnShow_ServerClick(object sender, EventArgs e)
    {
        Response.Redirect("admin-xem-bai-viet-cua-giao-vien-tu-truong-bo-phan-" + txtUserName.Value);
    }
}