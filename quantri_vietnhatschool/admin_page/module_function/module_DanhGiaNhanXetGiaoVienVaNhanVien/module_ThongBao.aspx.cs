﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_ThongBao : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {
        edtnoidung.Toolbars.Add(HtmlEditorToolbar.CreateStandardToolbar1());
        //if (logedMember.groupuser_id == 3)
        //    Response.Redirect("/user-home");
        if (!IsPostBack)
        {
            Session["_id"] = 0;

        }
        loadData();
    }
    private void loadData()
    {
        var getData = from n in db.tbThongBaos
                      orderby n.thongbao_id descending
                      select n;
        grvList.DataSource = getData;
        grvList.DataBind();
    }
    private void setNULL()
    {
        txttensanpham.Text = "";
        edtnoidung.Html = "";
    }
    protected void btnThem_Click(object sender, EventArgs e)
    {
        Session["_id"] = 0;
        setNULL();
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupControl.Show();", true);
    }
    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "thongbao_id" }));
        Session["_id"] = _id;
        var getData = (from n in db.tbThongBaos
                       where n.thongbao_id == _id
                       select n).Single();
        txttensanpham.Text = getData.thongbao_name;
        edtnoidung.Html = getData.thongbao_content;

        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupControl.Show();", true);
    }
    protected void btnXoa_Click(object sender, EventArgs e)
    {
        cls_ThongBao cls;
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "thongbao_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                cls = new cls_ThongBao();
                tbThongBao checkImage = (from i in db.tbThongBaos where i.thongbao_id == Convert.ToInt32(item) select i).SingleOrDefault();
                if (cls.Linq_Xoa(Convert.ToInt32(item)))
                {
                    alert.alert_Success(Page, "Xóa thành công", "");
                }
                else
                    alert.alert_Error(Page, "Xóa thất bại", "");
            }
        }
        else
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
    }

    public bool checknull()
    {
        if (txttensanpham.Text != "" || edtnoidung.Html != "")
            return true;
        else return false;
    }


    protected void btnLuu_Click(object sender, EventArgs e)
    {
        cls_ThongBao cls = new cls_ThongBao();
        if (checknull() == false)
            alert.alert_Warning(Page, "Hãy nhập đầy đủ thông tin!", "");
        else
        {
            if (Session["_id"].ToString() == "0")
            {

                if (cls.Linq_Them(txttensanpham.Text, edtnoidung.Html))
                    alert.alert_Success(Page, "Thêm thành công", "");
                else alert.alert_Error(Page, "Thêm thất bại", "");

            }
            else
            {
                if (cls.Linq_Sua(Convert.ToInt32(Session["_id"].ToString()), txttensanpham.Text, edtnoidung.Html))
                    alert.alert_Success(Page, "Cập nhật thành công", "");
                else alert.alert_Error(Page, "Cập nhật thất bại", "");
            }
            //popupControl.ShowOnPageLoad = false;
        }
    }

    protected void btnGiaoVien_Click(object sender, EventArgs e)
    {
        string getDataEmail = (from mail in db.tbDanhSachEmails where mail.email_id == 1 select mail).SingleOrDefault().email_content;
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "thongbao_id" });

        if (selectedKey.Count > 0)
        {
            SendMail(getDataEmail);
            ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Đã gửi email thành công!','','success').then(function(){grvList.UnselectRows();})", true);
        }
        else
        {
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu để gửi!", "");
        }
    }

    protected void btnNhanVien_Click(object sender, EventArgs e)
    {
        string getDataEmail = (from mail in db.tbDanhSachEmails where mail.email_id == 2 select mail).SingleOrDefault().email_content;
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "thongbao_id" });

        if (selectedKey.Count > 0)
        {
            SendMail(getDataEmail);
            ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Đã gửi email thành công!','','success').then(function(){grvList.UnselectRows();})", true);
        }
        else
        {
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu để gửi!", "");
        }
    }

    protected void btnTruong_Click(object sender, EventArgs e)
    {
        string getDataEmail = (from mail in db.tbDanhSachEmails where mail.email_id == 2 select mail).SingleOrDefault().email_content;
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "thongbao_id" });

        if (selectedKey.Count > 0)
        {
            SendMail(getDataEmail);
            ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Đã gửi email thành công!','','success').then(function(){grvList.UnselectRows();})", true);
        }
        else
        {
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu để gửi!", "");
        }
    }
    private bool SendMail(string email)
    {

        if (email != "")
        {
            try
            {
                var fromAddress = "thongbaovietnhatschool@gmail.com";//  Email Address from where you send the mail 
                var toAddress = email;
                const string fromPassword = "neiabcekdjluofid";
                string subject, title;
                title = "Thông báo";
                subject = "<!DOCTYPE html><html><head><title></title></head><body ><div>" +
                "<h3 style=\"margin-top:0px; text-align:center; color:#029ada\">Bạn có thông báo mới từ lịch công tác tuần. Xem chi tiết <a href='http://quantri.vietnhatschool.edu.vn/admin-xem-lich-cong-tac-hang-tuan'>tại đây.</a></h3>" +
                "</div></body></html>";
                var smtp = new System.Net.Mail.SmtpClient();
                {
                    smtp.Host = "smtp.gmail.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    smtp.Credentials = new NetworkCredential(fromAddress, fromPassword);
                    smtp.Timeout = 20000;
                }
                MailMessage mm = new MailMessage();
                mm.From = new MailAddress(fromAddress, "Trường Liên cấp Việt Nhật");
                mm.Subject = title;
                mm.To.Add(toAddress);
                mm.IsBodyHtml = true;
                mm.Body = subject;
                smtp.Send(mm);
                return true;
            }
            catch
            {
                return false;
            }
        }
        else
            return false;
    }

}