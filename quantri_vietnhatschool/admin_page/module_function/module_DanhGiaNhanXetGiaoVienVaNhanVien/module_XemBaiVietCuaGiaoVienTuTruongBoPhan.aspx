﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_XemBaiVietCuaGiaoVienTuTruongBoPhan.aspx.cs" Inherits="admin_page_module_function_module_XemBaiVietCuaGiaoVienTuTruongBoPhan" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v17.1" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="card card-block">
        <asp:UpdatePanel ID="udPopup" runat="server">
            <ContentTemplate>
                <div class="popup-main">
                    <%-- Start tiêu chuẩn 1--%>
                    <div class="div_content col-12">
                        <div class="col-12">
                            <div class="col-12 form-group">
                                <label>Tiêu chuẩn 1</label>
                                <hr />
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="col-12 form-group">
                                <label class="col-12 form-control-label">Minh chứng 1:</label>
                            </div>
                            <div class="col-12 form-group">
                                <div class="col-12">
                                    <asp:Repeater ID="rpMinhChung1" runat="server">
                                        <ItemTemplate>
                                            <%#Eval("noidungdanhgiagiaovien_minhchung1") %>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <label>Mức độ đạt được ở tiêu chí 1</label>
                            <div class="col-12 form-group">
                                <label class="label-checkbox font-weight-normal">
                                    Đạt
                                        <input type="radio" id="txtDatTieuChi1_1" value="Đạt" runat="server" name="imgavatart" />
                                    <span class="checkmark"></span>
                                </label>
                                <label class="label-checkbox font-weight-normal">
                                    Khá
                                        <input type="radio" id="txtDatTieuChi1_2" value="Khá" runat="server" name="imgavatart" />
                                    <span class="checkmark"></span>
                                </label>
                                <label class="label-checkbox font-weight-normal">
                                    Tốt
                                        <input type="radio" id="txtDatTieuChi1_3" value="Tốt" runat="server" name="imgavatart" />
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-12">
                            <label>Mức độ đạt được ở tiêu chí 2</label>
                            <div class="col-12 form-group">
                                <label class="label-checkbox font-weight-normal">
                                    Đạt
                                        <input type="radio" id="txtDatTieuChi2_1" value="Đạt" runat="server" name="imgavatart2" />
                                    <span class="checkmark"></span>
                                </label>
                                <label class="label-checkbox font-weight-normal">
                                    Khá
                                        <input type="radio" id="txtDatTieuChi2_2" value="Khá" runat="server" name="imgavatart2" />
                                    <span class="checkmark"></span>
                                </label>
                                <label class="label-checkbox font-weight-normal">
                                    Tốt
                                        <input type="radio" id="txtDatTieuChi2_3" value="Tốt" runat="server" name="imgavatart2" />
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 form-group">
                        <label>Nội dung đánh giá tiêu chuẩn 1</label>
                    </div>
                    <textarea id="txtDanhGiaTieuChuan1" runat="server" rows="7" style="width: 100%"></textarea>
                    <a></a>
                    <%-- End tiêu chuẩn 1--%>
                    <%-- Start tiêu chuẩn 2--%>
                    <div class="div_content col-12">
                        <div class="col-12">
                            <div class="col-12 form-group">
                                <label>Tiêu chuẩn 2</label>
                                <hr />
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="col-12 form-group">
                                <label class="col-12 form-control-label">Minh chứng 2:</label>
                            </div>
                            <div class="col-12 form-group">
                                <div class="col-12">
                                    <asp:Repeater ID="rpMinhChung2" runat="server">
                                        <ItemTemplate>
                                            <%#Eval("noidungdanhgiagiaovien_minhchung2") %>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <label>Mức độ đạt được ở tiêu chí 3</label>
                            <div class="col-12 form-group">
                                <label class="label-checkbox font-weight-normal">
                                    Đạt
                                        <input type="radio" id="txtDatTieuChi3_1" value="Đạt" runat="server" name="imgavatart3" />
                                    <span class="checkmark"></span>
                                </label>
                                <label class="label-checkbox font-weight-normal">
                                    Khá
                                        <input type="radio" id="txtDatTieuChi3_2" value="Khá" runat="server" name="imgavatart3" />
                                    <span class="checkmark"></span>
                                </label>
                                <label class="label-checkbox font-weight-normal">
                                    Tốt
                                        <input type="radio" id="txtDatTieuChi3_3" value="Tốt" runat="server" name="imgavatart3" />
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-12">
                            <label>Mức độ đạt được ở tiêu chí 4</label>
                            <div class="col-12 form-group">
                                <label class="label-checkbox font-weight-normal">
                                    Đạt
                                        <input type="radio" id="txtDatTieuChi4_1" value="Đạt" runat="server" name="imgavatart4" />
                                    <span class="checkmark"></span>
                                </label>
                                <label class="label-checkbox font-weight-normal">
                                    Khá
                                        <input type="radio" id="txtDatTieuChi4_2" value="Khá" runat="server" name="imgavatart4" />
                                    <span class="checkmark"></span>
                                </label>
                                <label class="label-checkbox font-weight-normal">
                                    Tốt
                                        <input type="radio" id="txtDatTieuChi4_3" value="Tốt" runat="server" name="imgavatart4" />
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-12">
                            <label>Mức độ đạt được ở tiêu chí 5</label>
                            <div class="col-12 form-group">
                                <label class="label-checkbox font-weight-normal">
                                    Đạt
                                        <input type="radio" id="txtDatTieuChi5_1" value="Đạt" runat="server" name="imgavatart5" />
                                    <span class="checkmark"></span>
                                </label>
                                <label class="label-checkbox font-weight-normal">
                                    Khá
                                        <input type="radio" id="txtDatTieuChi5_2" value="Khá" runat="server" name="imgavatart5" />
                                    <span class="checkmark"></span>
                                </label>
                                <label class="label-checkbox font-weight-normal">
                                    Tốt
                                        <input type="radio" id="txtDatTieuChi5_3" value="Tốt" runat="server" name="imgavatart5" />
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-12">
                            <label>Mức độ đạt được ở tiêu chí 6</label>
                            <div class="col-12 form-group">
                                <label class="label-checkbox font-weight-normal">
                                    Đạt
                                        <input type="radio" id="txtDatTieuChi6_1" value="Đạt" runat="server" name="imgavatart6" />
                                    <span class="checkmark"></span>
                                </label>
                                <label class="label-checkbox font-weight-normal">
                                    Khá
                                        <input type="radio" id="txtDatTieuChi6_2" value="Khá" runat="server" name="imgavatart6" />
                                    <span class="checkmark"></span>
                                </label>
                                <label class="label-checkbox font-weight-normal">
                                    Tốt
                                        <input type="radio" id="txtDatTieuChi6_3" value="Tốt" runat="server" name="imgavatart6" />
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-12">
                            <label>Mức độ đạt được ở tiêu chí 7</label>
                            <div class="col-12 form-group">
                                <label class="label-checkbox font-weight-normal">
                                    Đạt
                                        <input type="radio" id="txtDatTieuChi7_1" value="Đạt" runat="server" name="imgavatart7" />
                                    <span class="checkmark"></span>
                                </label>
                                <label class="label-checkbox font-weight-normal">
                                    Khá
                                        <input type="radio" id="txtDatTieuChi7_2" value="Khá" runat="server" name="imgavatart7" />
                                    <span class="checkmark"></span>
                                </label>
                                <label class="label-checkbox font-weight-normal">
                                    Tốt
                                        <input type="radio" id="txtDatTieuChi7_3" value="Tốt" runat="server" name="imgavatart7" />
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 form-group">
                        <label>Nội dung đánh giá tiêu chuẩn 2</label>
                    </div>
                    <textarea id="txtDanhGiaTieuChuan2" runat="server" rows="7" style="width: 100%"></textarea>
                  <%--  <asp:Button ID="btnLuuTieuChuan2" runat="server" ClientIDMode="Static" Text="Lưu" CssClass="btn btn-primary" />--%>
                    <%-- End tiêu chuẩn 2--%>
                    <%-- Start tiêu chuẩn 3--%>
                    <div class="div_content col-12">
                        <div class="col-12">
                            <div class="col-12 form-group">
                                <label>Tiêu chuẩn 3</label>
                                <hr />
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="col-12 form-group">
                                <label class="col-12 form-control-label">Minh chứng 3:</label>
                            </div>
                            <div class="col-12 form-group">
                                <div class="col-12">
                                    <asp:Repeater ID="rpMinhChung3" runat="server">
                                        <ItemTemplate>
                                            <%#Eval("noidungdanhgiagiaovien_minhchung3") %>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <label>Mức độ đạt được ở tiêu chí 8</label>
                            <div class="col-12 form-group">
                                <label class="label-checkbox font-weight-normal">
                                    Đạt
                                        <input type="radio" id="txtDatTieuChi8_1" value="Đạt" runat="server" name="imgavatart8" />
                                    <span class="checkmark"></span>
                                </label>
                                <label class="label-checkbox font-weight-normal">
                                    Khá
                                        <input type="radio" id="txtDatTieuChi8_2" value="Khá" runat="server" name="imgavatart8" />
                                    <span class="checkmark"></span>
                                </label>
                                <label class="label-checkbox font-weight-normal">
                                    Tốt
                                        <input type="radio" id="txtDatTieuChi8_3" value="Tốt" runat="server" name="imgavatart8" />
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-12">
                            <label>Mức độ đạt được ở tiêu chí 9</label>
                            <div class="col-12 form-group">
                                <label class="label-checkbox font-weight-normal">
                                    Đạt
                                        <input type="radio" id="txtDatTieuChi9_1" value="Đạt" runat="server" name="imgavatart9" />
                                    <span class="checkmark"></span>
                                </label>
                                <label class="label-checkbox font-weight-normal">
                                    Khá
                                        <input type="radio" id="txtDatTieuChi9_2" value="Khá" runat="server" name="imgavatart9" />
                                    <span class="checkmark"></span>
                                </label>
                                <label class="label-checkbox font-weight-normal">
                                    Tốt
                                        <input type="radio" id="txtDatTieuChi9_3" value="Tốt" runat="server" name="imgavatart9" />
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-12">
                            <label>Mức độ đạt được ở tiêu chí 10</label>
                            <div class="col-12 form-group">
                                <label class="label-checkbox font-weight-normal">
                                    Đạt
                                        <input type="radio" id="txtDatTieuChi10_1" value="Đạt" runat="server" name="imgavatart10" />
                                    <span class="checkmark"></span>
                                </label>
                                <label class="label-checkbox font-weight-normal">
                                    Khá
                                        <input type="radio" id="txtDatTieuChi10_2" value="Khá" runat="server" name="imgavatart10" />
                                    <span class="checkmark"></span>
                                </label>
                                <label class="label-checkbox font-weight-normal">
                                    Tốt
                                        <input type="radio" id="txtDatTieuChi10_3" value="Tốt" runat="server" name="imgavatart10" />
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 form-group">
                        <label>Nội dung đánh giá tiêu chuẩn 3</label>
                    </div>
                    <textarea id="txtDanhGiaTieuChuan3" runat="server" rows="7" style="width: 100%"></textarea>
                    <%--<asp:Button ID="btnLuuTieuChuan3" runat="server" ClientIDMode="Static" Text="Lưu" CssClass="btn btn-primary" />--%>
                    <%-- End tiêu chuẩn 3--%>
                    <%-- Start tiêu chuẩn 4--%>
                    <div class="div_content col-12">
                        <div class="col-12">
                            <div class="col-12 form-group">
                                <label>Tiêu chuẩn 4</label>
                                <hr />
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="col-12 form-group">
                                <label class="col-12 form-control-label">Minh chứng 4:</label>
                            </div>
                            <div class="col-12 form-group">
                                <div class="col-12">
                                    <asp:Repeater ID="rpMinhChung4" runat="server">
                                        <ItemTemplate>
                                            <%#Eval("noidungdanhgiagiaovien_minhchung4") %>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <label>Mức độ đạt được ở tiêu chí 11</label>
                            <div class="col-12 form-group">
                                <label class="label-checkbox font-weight-normal">
                                    Đạt
                                        <input type="radio" id="txtDatTieuChi11_1" value="Đạt" runat="server" name="imgavatart11" />
                                    <span class="checkmark"></span>
                                </label>
                                <label class="label-checkbox font-weight-normal">
                                    Khá
                                        <input type="radio" id="txtDatTieuChi11_2" value="Khá" runat="server" name="imgavatart11" />
                                    <span class="checkmark"></span>
                                </label>
                                <label class="label-checkbox font-weight-normal">
                                    Tốt
                                        <input type="radio" id="txtDatTieuChi11_3" value="Tốt" runat="server" name="imgavatart11" />
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-12">
                            <label>Mức độ đạt được ở tiêu chí 12</label>
                            <div class="col-12 form-group">
                                <label class="label-checkbox font-weight-normal">
                                    Đạt
                                        <input type="radio" id="txtDatTieuChi12_1" value="Đạt" runat="server" name="imgavatart12" />
                                    <span class="checkmark"></span>
                                </label>
                                <label class="label-checkbox font-weight-normal">
                                    Khá
                                        <input type="radio" id="txtDatTieuChi12_2" value="Khá" runat="server" name="imgavatart12" />
                                    <span class="checkmark"></span>
                                </label>
                                <label class="label-checkbox font-weight-normal">
                                    Tốt
                                        <input type="radio" id="txtDatTieuChi12_3" value="Tốt" runat="server" name="imgavatart12" />
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-12">
                            <label>Mức độ đạt được ở tiêu chí 13</label>
                            <div class="col-12 form-group">
                                <label class="label-checkbox font-weight-normal">
                                    Đạt
                                        <input type="radio" id="txtDatTieuChi13_1" value="Đạt" runat="server" name="imgavatart13" />
                                    <span class="checkmark"></span>
                                </label>
                                <label class="label-checkbox font-weight-normal">
                                    Khá
                                        <input type="radio" id="txtDatTieuChi13_2" value="Khá" runat="server" name="imgavatart13" />
                                    <span class="checkmark"></span>
                                </label>
                                <label class="label-checkbox font-weight-normal">
                                    Tốt
                                        <input type="radio" id="txtDatTieuChi13_3" value="Tốt" runat="server" name="imgavatart13" />
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 form-group">
                        <label>Nội dung đánh giá tiêu chuẩn 4</label>
                    </div>
                    <textarea id="txtDanhGiaTieuChuan4" runat="server" rows="7" style="width: 100%"></textarea>
                    <%--<asp:Button ID="btnLuuTieuChuan4" runat="server" ClientIDMode="Static" Text="Lưu" CssClass="btn btn-primary" />--%>
                    <%-- End tiêu chuẩn 4--%>
                    <%-- Start tiêu chuẩn 5--%>
                    <div class="div_content col-12">
                        <div class="col-12">
                            <div class="col-12 form-group">
                                <label>Tiêu chuẩn 5</label>
                                <hr />
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="col-12 form-group">
                                <label class="col-12 form-control-label">Minh chứng 5:</label>
                            </div>
                            <div class="col-12 form-group">
                                <div class="col-12">
                                    <asp:Repeater ID="rpMinhChung5" runat="server">
                                        <ItemTemplate>
                                            <%#Eval("noidungdanhgiagiaovien_minhchung5") %>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <label>Mức độ đạt được ở tiêu chí 14</label>
                            <div class="col-12 form-group">
                                <label class="label-checkbox font-weight-normal">
                                    Đạt
                                        <input type="radio" id="txtDatTieuChi14_1" value="Đạt" runat="server" name="imgavatart14" />
                                    <span class="checkmark"></span>
                                </label>
                                <label class="label-checkbox font-weight-normal">
                                    Khá
                                        <input type="radio" id="txtDatTieuChi14_2" value="Khá" runat="server" name="imgavatart14" />
                                    <span class="checkmark"></span>
                                </label>
                                <label class="label-checkbox font-weight-normal">
                                    Tốt
                                        <input type="radio" id="txtDatTieuChi14_3" value="Tốt" runat="server" name="imgavatart14" />
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-12">
                            <label>Mức độ đạt được ở tiêu chí 15</label>
                            <div class="col-12 form-group">
                                <label class="label-checkbox font-weight-normal">
                                    Đạt
                                        <input type="radio" id="txtDatTieuChi15_1" value="Đạt" runat="server" name="imgavatart15" />
                                    <span class="checkmark"></span>
                                </label>
                                <label class="label-checkbox font-weight-normal">
                                    Khá
                                        <input type="radio" id="txtDatTieuChi15_2" value="Khá" runat="server" name="imgavatart15" />
                                    <span class="checkmark"></span>
                                </label>
                                <label class="label-checkbox font-weight-normal">
                                    Tốt
                                        <input type="radio" id="txtDatTieuChi15_3" value="Tốt" runat="server" name="imgavatart15" />
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 form-group">
                        <label>Nội dung đánh giá tiêu chuẩn 5</label>
                    </div>
                    <textarea id="txtDanhGiaTieuChuan5" runat="server" rows="7" style="width: 100%"></textarea>
                   <%-- <asp:Button ID="btnLuuTieuChuan5" runat="server" ClientIDMode="Static" Text="Lưu" CssClass="btn btn-primary" />--%>
                    <%-- End tiêu chuẩn 5--%>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="mar_but button">
            <asp:UpdatePanel ID="pnLuu" runat="server">
                <ContentTemplate>
                    <asp:Button ID="btnLuu" runat="server" ClientIDMode="Static" Text="Hoàn thành" CssClass="btn btn-primary" OnClick="btnLuu_Click" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

