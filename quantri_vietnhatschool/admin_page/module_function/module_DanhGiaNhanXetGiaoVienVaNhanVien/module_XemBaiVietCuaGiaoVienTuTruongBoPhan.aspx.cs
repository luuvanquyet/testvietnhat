﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_XemBaiVietCuaGiaoVienTuTruongBoPhan : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            // Kiểm tra trưởng bộ phận
            int taikhoan = Convert.ToInt32(RouteData.Values["id"].ToString());
            var checkTruongBoPhan = (from tbp in db.tbNoiDungDanhGiaGiaoVienVietNhats
                                    join u in db.admin_Users on tbp.taikhoan_id equals u.username_id
                                    where u.username_id == taikhoan && tbp.active_truongbophan ==true
                                    orderby tbp.noidungdanhgiagiaovien_id descending
                                    select tbp).Take(1);
            if (checkTruongBoPhan.Count()>0)
            {
                txtDanhGiaTieuChuan1.Value = checkTruongBoPhan.Single().nhanxettieuchuan1;
                txtDanhGiaTieuChuan2.Value = checkTruongBoPhan.Single().nhanxettieuchuan2;
                txtDanhGiaTieuChuan3.Value = checkTruongBoPhan.Single().nhanxettieuchuan3;
                txtDanhGiaTieuChuan4.Value = checkTruongBoPhan.Single().nhanxettieuchuan4;
                txtDanhGiaTieuChuan5.Value = checkTruongBoPhan.Single().nhanxettieuchuan5;
                rpMinhChung1.DataSource = checkTruongBoPhan;
                rpMinhChung1.DataBind();
                rpMinhChung2.DataSource = checkTruongBoPhan;
                rpMinhChung2.DataBind();
                rpMinhChung3.DataSource = checkTruongBoPhan;
                rpMinhChung3.DataBind();
                rpMinhChung4.DataSource = checkTruongBoPhan;
                rpMinhChung4.DataBind();
                rpMinhChung5.DataSource = checkTruongBoPhan;
                rpMinhChung5.DataBind();
                if (checkTruongBoPhan.SingleOrDefault().mucdo1_truongbophan == "Đạt")
                    txtDatTieuChi1_1.Checked = true;
                if (checkTruongBoPhan.SingleOrDefault().mucdo1_truongbophan == "Khá")
                    txtDatTieuChi1_2.Checked = true;
                if (checkTruongBoPhan.SingleOrDefault().mucdo1_truongbophan == "Tốt")
                    txtDatTieuChi1_3.Checked = true;

                if (checkTruongBoPhan.SingleOrDefault().mucdo2_truongbophan == "Đạt")
                    txtDatTieuChi2_1.Checked = true;
                if (checkTruongBoPhan.SingleOrDefault().mucdo2_truongbophan == "Khá")
                    txtDatTieuChi2_2.Checked = true;
                if (checkTruongBoPhan.SingleOrDefault().mucdo2_truongbophan == "Tốt")
                    txtDatTieuChi2_3.Checked = true;

                if (checkTruongBoPhan.SingleOrDefault().mucdo3_truongbophan == "Đạt")
                    txtDatTieuChi3_1.Checked = true;
                if (checkTruongBoPhan.SingleOrDefault().mucdo3_truongbophan == "Khá")
                    txtDatTieuChi3_2.Checked = true;
                if (checkTruongBoPhan.SingleOrDefault().mucdo3_truongbophan == "Tốt")
                    txtDatTieuChi3_3.Checked = true;

                if (checkTruongBoPhan.SingleOrDefault().mucdo4_truongbophan == "Đạt")
                    txtDatTieuChi4_1.Checked = true;
                if (checkTruongBoPhan.SingleOrDefault().mucdo4_truongbophan == "Khá")
                    txtDatTieuChi4_2.Checked = true;
                if (checkTruongBoPhan.SingleOrDefault().mucdo4_truongbophan == "Tốt")
                    txtDatTieuChi4_3.Checked = true;

                if (checkTruongBoPhan.SingleOrDefault().mucdo5_truongbophan == "Đạt")
                    txtDatTieuChi5_1.Checked = true;
                if (checkTruongBoPhan.SingleOrDefault().mucdo5_truongbophan == "Khá")
                    txtDatTieuChi5_2.Checked = true;
                if (checkTruongBoPhan.SingleOrDefault().mucdo5_truongbophan == "Tốt")
                    txtDatTieuChi5_3.Checked = true;

                if (checkTruongBoPhan.SingleOrDefault().mucdo6_truongbophan == "Đạt")
                    txtDatTieuChi6_1.Checked = true;
                if (checkTruongBoPhan.SingleOrDefault().mucdo6_truongbophan == "Khá")
                    txtDatTieuChi6_2.Checked = true;
                if (checkTruongBoPhan.SingleOrDefault().mucdo6_truongbophan == "Tốt")
                    txtDatTieuChi6_3.Checked = true;

                if (checkTruongBoPhan.SingleOrDefault().mucdo7_truongbophan == "Đạt")
                    txtDatTieuChi7_1.Checked = true;
                if (checkTruongBoPhan.SingleOrDefault().mucdo7_truongbophan == "Khá")
                    txtDatTieuChi7_2.Checked = true;
                if (checkTruongBoPhan.SingleOrDefault().mucdo7_truongbophan == "Tốt")
                    txtDatTieuChi7_3.Checked = true;

                if (checkTruongBoPhan.SingleOrDefault().mucdo8_truongbophan == "Đạt")
                    txtDatTieuChi8_1.Checked = true;
                if (checkTruongBoPhan.SingleOrDefault().mucdo8_truongbophan == "Khá")
                    txtDatTieuChi8_2.Checked = true;
                if (checkTruongBoPhan.SingleOrDefault().mucdo8_truongbophan == "Tốt")
                    txtDatTieuChi8_3.Checked = true;

                if (checkTruongBoPhan.SingleOrDefault().mucdo9_truongbophan == "Đạt")
                    txtDatTieuChi9_1.Checked = true;
                if (checkTruongBoPhan.SingleOrDefault().mucdo9_truongbophan == "Khá")
                    txtDatTieuChi9_2.Checked = true;
                if (checkTruongBoPhan.SingleOrDefault().mucdo9_truongbophan == "Tốt")
                    txtDatTieuChi9_3.Checked = true;

                if (checkTruongBoPhan.SingleOrDefault().mucdo10_truongbophan == "Đạt")
                    txtDatTieuChi10_1.Checked = true;
                if (checkTruongBoPhan.SingleOrDefault().mucdo10_truongbophan == "Khá")
                    txtDatTieuChi10_2.Checked = true;
                if (checkTruongBoPhan.SingleOrDefault().mucdo10_truongbophan == "Tốt")
                    txtDatTieuChi10_3.Checked = true;

                if (checkTruongBoPhan.SingleOrDefault().mucdo11_truongbophan == "Đạt")
                    txtDatTieuChi11_1.Checked = true;
                if (checkTruongBoPhan.SingleOrDefault().mucdo11_truongbophan == "Khá")
                    txtDatTieuChi11_2.Checked = true;
                if (checkTruongBoPhan.SingleOrDefault().mucdo11_truongbophan == "Tốt")
                    txtDatTieuChi11_3.Checked = true;

                if (checkTruongBoPhan.SingleOrDefault().mucdo12_truongbophan == "Đạt")
                    txtDatTieuChi12_1.Checked = true;
                if (checkTruongBoPhan.SingleOrDefault().mucdo12_truongbophan == "Khá")
                    txtDatTieuChi12_2.Checked = true;
                if (checkTruongBoPhan.SingleOrDefault().mucdo12_truongbophan == "Tốt")
                    txtDatTieuChi12_3.Checked = true;

                if (checkTruongBoPhan.SingleOrDefault().mucdo13_truongbophan == "Đạt")
                    txtDatTieuChi13_1.Checked = true;
                if (checkTruongBoPhan.SingleOrDefault().mucdo13_truongbophan == "Khá")
                    txtDatTieuChi13_2.Checked = true;
                if (checkTruongBoPhan.SingleOrDefault().mucdo13_truongbophan == "Tốt")
                    txtDatTieuChi13_3.Checked = true;

                if (checkTruongBoPhan.SingleOrDefault().mucdo14_truongbophan == "Đạt")
                    txtDatTieuChi14_1.Checked = true;
                if (checkTruongBoPhan.SingleOrDefault().mucdo14_truongbophan == "Khá")
                    txtDatTieuChi14_2.Checked = true;
                if (checkTruongBoPhan.SingleOrDefault().mucdo14_truongbophan == "Tốt")
                    txtDatTieuChi14_3.Checked = true;

                if (checkTruongBoPhan.SingleOrDefault().mucdo15_truongbophan == "Đạt")
                    txtDatTieuChi15_1.Checked = true;
                if (checkTruongBoPhan.SingleOrDefault().mucdo15_truongbophan == "Khá")
                    txtDatTieuChi15_2.Checked = true;
                if (checkTruongBoPhan.SingleOrDefault().mucdo15_truongbophan == "Tốt")
                    txtDatTieuChi15_3.Checked = true;
            }
            else
            {
                var checkNoidung = (from nd in db.tbNoiDungDanhGiaGiaoVienVietNhats
                                    join tk in db.admin_Users on nd.taikhoan_id equals tk.username_id
                                    where tk.username_id == taikhoan
                                    orderby nd.noidungdanhgiagiaovien_id descending
                                    select nd).Take(1);
                if (checkNoidung.Count()>0)
                {
                    txtDanhGiaTieuChuan1.Value = checkNoidung.Single().nhanxettieuchuan1;
                    txtDanhGiaTieuChuan2.Value = checkNoidung.Single().nhanxettieuchuan2;
                    txtDanhGiaTieuChuan3.Value = checkNoidung.Single().nhanxettieuchuan3;
                    txtDanhGiaTieuChuan4.Value = checkNoidung.Single().nhanxettieuchuan4;
                    txtDanhGiaTieuChuan5.Value = checkNoidung.Single().nhanxettieuchuan5;
                    rpMinhChung1.DataSource = checkNoidung;
                    rpMinhChung1.DataBind();
                    rpMinhChung2.DataSource = checkNoidung;
                    rpMinhChung2.DataBind();
                    rpMinhChung3.DataSource = checkNoidung;
                    rpMinhChung3.DataBind();
                    rpMinhChung4.DataSource = checkNoidung;
                    rpMinhChung4.DataBind();
                    rpMinhChung5.DataSource = checkNoidung;
                    rpMinhChung5.DataBind();
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi1 == "Đạt")
                        txtDatTieuChi1_1.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi1 == "Khá")
                        txtDatTieuChi1_2.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi1 == "Tốt")
                        txtDatTieuChi1_3.Checked = true;

                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi2 == "Đạt")
                        txtDatTieuChi2_1.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi2 == "Khá")
                        txtDatTieuChi2_2.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi2 == "Tốt")
                        txtDatTieuChi2_3.Checked = true;

                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi3 == "Đạt")
                        txtDatTieuChi3_1.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi3 == "Khá")
                        txtDatTieuChi3_2.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi3 == "Tốt")
                        txtDatTieuChi3_3.Checked = true;

                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi4 == "Đạt")
                        txtDatTieuChi4_1.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi4 == "Khá")
                        txtDatTieuChi4_2.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi4 == "Tốt")
                        txtDatTieuChi4_3.Checked = true;

                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi5 == "Đạt")
                        txtDatTieuChi5_1.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi5 == "Khá")
                        txtDatTieuChi5_2.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi5 == "Tốt")
                        txtDatTieuChi5_3.Checked = true;

                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi6 == "Đạt")
                        txtDatTieuChi6_1.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi6 == "Khá")
                        txtDatTieuChi6_2.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi6 == "Tốt")
                        txtDatTieuChi6_3.Checked = true;

                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi7 == "Đạt")
                        txtDatTieuChi7_1.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi7 == "Khá")
                        txtDatTieuChi7_2.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi7 == "Tốt")
                        txtDatTieuChi7_3.Checked = true;

                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi8 == "Đạt")
                        txtDatTieuChi8_1.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi8 == "Khá")
                        txtDatTieuChi8_2.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi8 == "Tốt")
                        txtDatTieuChi8_3.Checked = true;

                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi9 == "Đạt")
                        txtDatTieuChi9_1.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi9 == "Khá")
                        txtDatTieuChi9_2.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi9 == "Tốt")
                        txtDatTieuChi9_3.Checked = true;

                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi10 == "Đạt")
                        txtDatTieuChi10_1.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi10 == "Khá")
                        txtDatTieuChi10_2.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi10 == "Tốt")
                        txtDatTieuChi10_3.Checked = true;

                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi11 == "Đạt")
                        txtDatTieuChi11_1.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi11 == "Khá")
                        txtDatTieuChi11_2.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi11 == "Tốt")
                        txtDatTieuChi11_3.Checked = true;

                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi12 == "Đạt")
                        txtDatTieuChi12_1.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi12 == "Khá")
                        txtDatTieuChi12_2.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi12 == "Tốt")
                        txtDatTieuChi12_3.Checked = true;

                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi13 == "Đạt")
                        txtDatTieuChi13_1.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi13 == "Khá")
                        txtDatTieuChi13_2.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi13 == "Tốt")
                        txtDatTieuChi13_3.Checked = true;

                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi14 == "Đạt")
                        txtDatTieuChi14_1.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi14 == "Khá")
                        txtDatTieuChi14_2.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi14 == "Tốt")
                        txtDatTieuChi14_3.Checked = true;

                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi15 == "Đạt")
                        txtDatTieuChi15_1.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi15 == "Khá")
                        txtDatTieuChi15_2.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi15 == "Tốt")
                        txtDatTieuChi15_3.Checked = true;
                }
            }
        }
    }

    protected void btnLuu_Click(object sender, EventArgs e)
    {
        var checkTaiKhoan = (from tk in db.admin_Users where tk.username_id == Convert.ToInt32(RouteData.Values["id"].ToString()) select tk).SingleOrDefault();
        if (checkTaiKhoan != null)
        {
            string tieuchicuatieuchuan1 = "";
            if (txtDatTieuChi1_1.Checked == true)
            {
                tieuchicuatieuchuan1 = "Đạt";
            }
            else if (txtDatTieuChi1_2.Checked == true)
            {
                tieuchicuatieuchuan1 = "Khá";
            }
            else if (txtDatTieuChi1_3.Checked == true)
            {
                tieuchicuatieuchuan1 = "Tốt";
            }
            else
            {
                alert.alert_Error(Page, "Vui lòng chọn mức độ tiêu chi 1 để đánh giá", "");
            }
            string tieuchicuatieuchuan2 = "";
            if (txtDatTieuChi2_1.Checked == true)
            {
                tieuchicuatieuchuan2 = "Đạt";
            }
            else if (txtDatTieuChi2_2.Checked == true)
            {
                tieuchicuatieuchuan2 = "Khá";
            }
            else if (txtDatTieuChi2_3.Checked == true)
            {
                tieuchicuatieuchuan2 = "Tốt";
            }
            else
            {
                alert.alert_Error(Page, "Vui lòng chọn mức độ tiêu chi 2 để đánh giá", "");
            }
            string tieuchicuatieuchuan3 = "";
            if (txtDatTieuChi3_1.Checked == true)
            {
                tieuchicuatieuchuan3 = "Đạt";
            }
            else if (txtDatTieuChi3_2.Checked == true)
            {
                tieuchicuatieuchuan3 = "Khá";
            }
            else if (txtDatTieuChi3_3.Checked == true)
            {
                tieuchicuatieuchuan3 = "Tốt";
            }
            else
            {
                alert.alert_Error(Page, "Vui lòng chọn mức độ tiêu chi 3 để đánh giá", "");
            }
            string tieuchicuatieuchuan4 = "";
            if (txtDatTieuChi4_1.Checked == true)
            {
                tieuchicuatieuchuan4 = "Đạt";
            }
            else if (txtDatTieuChi4_2.Checked == true)
            {
                tieuchicuatieuchuan4 = "Khá";
            }
            else if (txtDatTieuChi4_3.Checked == true)
            {
                tieuchicuatieuchuan4 = "Tốt";
            }
            else
            {
                alert.alert_Error(Page, "Vui lòng chọn mức độ tiêu chi 4 để đánh giá", "");
            }
            string tieuchicuatieuchuan5 = "";
            if (txtDatTieuChi5_1.Checked == true)
            {
                tieuchicuatieuchuan5 = "Đạt";
            }
            else if (txtDatTieuChi5_2.Checked == true)
            {
                tieuchicuatieuchuan5 = "Khá";
            }
            else if (txtDatTieuChi5_3.Checked == true)
            {
                tieuchicuatieuchuan5 = "Tốt";
            }
            else
            {
                alert.alert_Error(Page, "Vui lòng chọn mức độ tiêu chi 5 để đánh giá", "");
            }
            string tieuchicuatieuchuan6 = "";
            if (txtDatTieuChi6_1.Checked == true)
            {
                tieuchicuatieuchuan6 = "Đạt";
            }
            else if (txtDatTieuChi6_2.Checked == true)
            {
                tieuchicuatieuchuan6 = "Khá";
            }
            else if (txtDatTieuChi6_3.Checked == true)
            {
                tieuchicuatieuchuan6 = "Tốt";
            }
            else
            {
                alert.alert_Error(Page, "Vui lòng chọn mức độ tiêu chi 6 để đánh giá", "");
            }
            string tieuchicuatieuchuan7 = "";
            if (txtDatTieuChi7_1.Checked == true)
            {
                tieuchicuatieuchuan7 = "Đạt";
            }
            else if (txtDatTieuChi7_2.Checked == true)
            {
                tieuchicuatieuchuan7 = "Khá";
            }
            else if (txtDatTieuChi7_3.Checked == true)
            {
                tieuchicuatieuchuan7 = "Tốt";
            }
            else
            {
                alert.alert_Error(Page, "Vui lòng chọn mức độ tiêu chi 7 để đánh giá", "");
            }
            string tieuchicuatieuchuan8 = "";
            if (txtDatTieuChi8_1.Checked == true)
            {
                tieuchicuatieuchuan8 = "Đạt";
            }
            else if (txtDatTieuChi8_2.Checked == true)
            {
                tieuchicuatieuchuan8 = "Khá";
            }
            else if (txtDatTieuChi8_3.Checked == true)
            {
                tieuchicuatieuchuan8 = "Tốt";
            }
            else
            {
                alert.alert_Error(Page, "Vui lòng chọn mức độ tiêu chi 8 để đánh giá", "");
            }
            string tieuchicuatieuchuan9 = "";
            if (txtDatTieuChi9_1.Checked == true)
            {
                tieuchicuatieuchuan9 = "Đạt";
            }
            else if (txtDatTieuChi9_2.Checked == true)
            {
                tieuchicuatieuchuan9 = "Khá";
            }
            else if (txtDatTieuChi9_3.Checked == true)
            {
                tieuchicuatieuchuan9 = "Tốt";
            }
            else
            {
                alert.alert_Error(Page, "Vui lòng chọn mức độ tiêu chi 9 để đánh giá", "");
            }
            string tieuchicuatieuchuan10 = "";
            if (txtDatTieuChi10_1.Checked == true)
            {
                tieuchicuatieuchuan10 = "Đạt";
            }
            else if (txtDatTieuChi10_2.Checked == true)
            {
                tieuchicuatieuchuan10 = "Khá";
            }
            else if (txtDatTieuChi10_3.Checked == true)
            {
                tieuchicuatieuchuan10 = "Tốt";
            }
            else
            {
                alert.alert_Error(Page, "Vui lòng chọn mức độ tiêu chi 10 để đánh giá", "");
            }
            string tieuchicuatieuchuan11 = "";
            if (txtDatTieuChi11_1.Checked == true)
            {
                tieuchicuatieuchuan11 = "Đạt";
            }
            else if (txtDatTieuChi11_2.Checked == true)
            {
                tieuchicuatieuchuan11 = "Khá";
            }
            else if (txtDatTieuChi11_3.Checked == true)
            {
                tieuchicuatieuchuan11 = "Tốt";
            }
            else
            {
                alert.alert_Error(Page, "Vui lòng chọn mức độ tiêu chi 11 để đánh giá", "");
            }
            string tieuchicuatieuchuan12 = "";
            if (txtDatTieuChi12_1.Checked == true)
            {
                tieuchicuatieuchuan12 = "Đạt";
            }
            else if (txtDatTieuChi12_2.Checked == true)
            {
                tieuchicuatieuchuan12 = "Khá";
            }
            else if (txtDatTieuChi12_3.Checked == true)
            {
                tieuchicuatieuchuan12 = "Tốt";
            }
            else
            {
                alert.alert_Error(Page, "Vui lòng chọn mức độ tiêu chi 12 để đánh giá", "");
            }
            string tieuchicuatieuchuan13 = "";
            if (txtDatTieuChi13_1.Checked == true)
            {
                tieuchicuatieuchuan13 = "Đạt";
            }
            else if (txtDatTieuChi13_2.Checked == true)
            {
                tieuchicuatieuchuan13 = "Khá";
            }
            else if (txtDatTieuChi13_3.Checked == true)
            {
                tieuchicuatieuchuan13 = "Tốt";
            }
            else
            {
                alert.alert_Error(Page, "Vui lòng chọn mức độ tiêu chi 13 để đánh giá", "");
            }
            string tieuchicuatieuchuan14 = "";
            if (txtDatTieuChi14_1.Checked == true)
            {
                tieuchicuatieuchuan14 = "Đạt";
            }
            else if (txtDatTieuChi14_2.Checked == true)
            {
                tieuchicuatieuchuan14 = "Khá";
            }
            else if (txtDatTieuChi14_3.Checked == true)
            {
                tieuchicuatieuchuan14 = "Tốt";
            }
            else
            {
                alert.alert_Error(Page, "Vui lòng chọn mức độ tiêu chi 14 để đánh giá", "");
            }
            string tieuchicuatieuchuan15 = "";
            if (txtDatTieuChi15_1.Checked == true)
            {
                tieuchicuatieuchuan15 = "Đạt";
            }
            else if (txtDatTieuChi15_2.Checked == true)
            {
                tieuchicuatieuchuan15 = "Khá";
            }
            else if (txtDatTieuChi15_3.Checked == true)
            {
                tieuchicuatieuchuan15 = "Tốt";
            }
            else
            {
                alert.alert_Error(Page, "Vui lòng chọn mức độ tiêu chi 15 để đánh giá", "");
            }

            try
            {
                // tiêu chí 1
                if (tieuchicuatieuchuan1 != "" && tieuchicuatieuchuan2 != "" && tieuchicuatieuchuan3 != "" && tieuchicuatieuchuan4 != "" && tieuchicuatieuchuan5 != "" && tieuchicuatieuchuan6 != "" && tieuchicuatieuchuan7 != "" && tieuchicuatieuchuan8 != "" && tieuchicuatieuchuan9 != "" && tieuchicuatieuchuan10 != "" && tieuchicuatieuchuan11 != "" && tieuchicuatieuchuan12 != "" && tieuchicuatieuchuan13 != "" && tieuchicuatieuchuan14 != "" && tieuchicuatieuchuan15 != "")
                {
                    var checkTaiKhoanDaCoDanhGiaLanDauChua = (from dg in db.tbNoiDungDanhGiaGiaoVienVietNhats where dg.taikhoan_id == Convert.ToInt32(RouteData.Values["id"].ToString()) orderby dg.noidungdanhgiagiaovien_id descending select dg).FirstOrDefault();
                    {
                        // trường hợp này đã insert vào lần đầu rồi
                        tbNoiDungDanhGiaGiaoVienVietNhat update = (from up in db.tbNoiDungDanhGiaGiaoVienVietNhats where up.taikhoan_id == Convert.ToInt32(RouteData.Values["id"].ToString()) orderby up.noidungdanhgiagiaovien_id descending select up).First();
                        update.mucdo1_truongbophan = tieuchicuatieuchuan1;
                        update.mucdo2_truongbophan = tieuchicuatieuchuan2;
                        update.mucdo3_truongbophan = tieuchicuatieuchuan3;
                        update.mucdo4_truongbophan = tieuchicuatieuchuan4;
                        update.mucdo5_truongbophan = tieuchicuatieuchuan5;
                        update.mucdo6_truongbophan = tieuchicuatieuchuan6;
                        update.mucdo7_truongbophan = tieuchicuatieuchuan7;
                        update.mucdo8_truongbophan = tieuchicuatieuchuan8;
                        update.mucdo9_truongbophan = tieuchicuatieuchuan9;
                        update.mucdo10_truongbophan = tieuchicuatieuchuan10;
                        update.mucdo11_truongbophan = tieuchicuatieuchuan11;
                        update.mucdo12_truongbophan = tieuchicuatieuchuan12;
                        update.mucdo13_truongbophan = tieuchicuatieuchuan13;
                        update.mucdo14_truongbophan = tieuchicuatieuchuan14;
                        update.mucdo15_truongbophan = tieuchicuatieuchuan15;
                        update.active_truongbophan = true;
                        update.hidden = false;
                        db.SubmitChanges();

                    }
                    checkKetQua(Convert.ToInt32(RouteData.Values["id"].ToString()));
                }
            }
            catch (Exception ex)
            {
                alert.alert_Error(Page, "Error", "");
            }
        }
    }
    protected void checkKetQua(int account_id)
    {
        // check tài khoản đã có đánh giá lần đâu chưa

        int countTot = 0;
        var checkDataAccount = (from nd in db.tbNoiDungDanhGiaGiaoVienVietNhats where nd.taikhoan_id == account_id orderby nd.noidungdanhgiagiaovien_id descending select nd).FirstOrDefault();
        // Mức tốt : tiêu chuẩn 2 phải tốt, trên 2/3 các tiêu chí phải tốt, ko có bất cừ trường hợp nào chỉ đạt
        if (checkDataAccount != null)
        {
            if (checkDataAccount.mucdo3_truongbophan == "Tốt"
                && checkDataAccount.mucdo4_truongbophan == "Tốt"
                && checkDataAccount.mucdo5_truongbophan == "Tốt"
                && checkDataAccount.mucdo6_truongbophan == "Tốt"
                && checkDataAccount.mucdo7_truongbophan == "Tốt")
            {
                countTot = 5;
                if (checkDataAccount.mucdo1_truongbophan == "Tốt")
                    countTot = countTot + 1;
                if (checkDataAccount.mucdo2_truongbophan == "Tốt")
                    countTot = countTot + 1;
                if (checkDataAccount.mucdo8_truongbophan == "Tốt")
                    countTot = countTot + 1;
                if (checkDataAccount.mucdo9_truongbophan == "Tốt")
                    countTot = countTot + 1;
                if (checkDataAccount.mucdo10_truongbophan == "Tốt")
                    countTot = countTot + 1;
                if (checkDataAccount.mucdo11_truongbophan == "Tốt")
                    countTot = countTot + 1;
                if (checkDataAccount.mucdo12_truongbophan == "Tốt")
                    countTot = countTot + 1;
                if (checkDataAccount.mucdo13_truongbophan == "Tốt")
                    countTot = countTot + 1;
                if (checkDataAccount.mucdo14_truongbophan == "Tốt")
                    countTot = countTot + 1;
                if (checkDataAccount.mucdo15_truongbophan == "Tốt")
                    countTot = countTot + 1;
                if (countTot >= 10)
                {
                    if (checkDataAccount.mucdo1_truongbophan == "Đạt" ||
                        checkDataAccount.mucdo2_truongbophan == "Đạt" ||
                        checkDataAccount.mucdo8_truongbophan == "Đạt" ||
                        checkDataAccount.mucdo9_truongbophan == "Đạt" ||
                        checkDataAccount.mucdo10_truongbophan == "Đạt" ||
                        checkDataAccount.mucdo11_truongbophan == "Đạt" ||
                        checkDataAccount.mucdo12_truongbophan == "Đạt" ||
                        checkDataAccount.mucdo13_truongbophan == "Đạt" ||
                        checkDataAccount.mucdo14_truongbophan == "Đạt" ||
                        checkDataAccount.mucdo15_truongbophan == "Đạt"
                        )
                    {
                        checkDataAccount.noidungdanhgiagiaovien_ketquatongket = "Khá";
                    }
                    else
                    {
                        checkDataAccount.noidungdanhgiagiaovien_ketquatongket = "Tốt";
                    }
                }
                else
                {
                    checkDataAccount.noidungdanhgiagiaovien_ketquatongket = "Khá";
                }
            }
            else
            {
                checkDataAccount.noidungdanhgiagiaovien_ketquatongket = "Khá";
                if (checkDataAccount.mucdo3_truongbophan == "Khá"
                && checkDataAccount.mucdo4_truongbophan == "Khá"
                && checkDataAccount.mucdo5_truongbophan == "Khá"
                && checkDataAccount.mucdo6_truongbophan == "Khá"
                && checkDataAccount.mucdo7_truongbophan == "Khá")
                {
                    countTot = 5;
                    if (checkDataAccount.mucdo1_truongbophan == "Khá")
                        countTot = countTot + 1;
                    if (checkDataAccount.mucdo2_truongbophan == "Khá")
                        countTot = countTot + 1;
                    if (checkDataAccount.mucdo8_truongbophan == "Khá")
                        countTot = countTot + 1;
                    if (checkDataAccount.mucdo9_truongbophan == "Khá")
                        countTot = countTot + 1;
                    if (checkDataAccount.mucdo10_truongbophan == "Khá")
                        countTot = countTot + 1;
                    if (checkDataAccount.mucdo11_truongbophan == "Khá")
                        countTot = countTot + 1;
                    if (checkDataAccount.mucdo12_truongbophan == "Khá")
                        countTot = countTot + 1;
                    if (checkDataAccount.mucdo13_truongbophan == "Khá")
                        countTot = countTot + 1;
                    if (checkDataAccount.mucdo14_truongbophan == "Khá")
                        countTot = countTot + 1;
                    if (checkDataAccount.mucdo15_truongbophan == "Khá")
                        countTot = countTot + 1;
                    if (countTot >= 10)
                    {
                        checkDataAccount.noidungdanhgiagiaovien_ketquatongket = "Khá";
                    }
                    else
                    {
                        checkDataAccount.noidungdanhgiagiaovien_ketquatongket = "Đạt";
                    }
                }
            }
            checkDataAccount.nhanxettieuchuan1 = txtDanhGiaTieuChuan1.Value;
            checkDataAccount.nhanxettieuchuan2 = txtDanhGiaTieuChuan2.Value;
            checkDataAccount.nhanxettieuchuan3 = txtDanhGiaTieuChuan3.Value;
            checkDataAccount.nhanxettieuchuan4 = txtDanhGiaTieuChuan4.Value;
            checkDataAccount.nhanxettieuchuan5 = txtDanhGiaTieuChuan5.Value;
            checkDataAccount.active_truongbophan = true;
            db.SubmitChanges();
            alert.alert_Success(Page, "Đã hoàn thành việc đánh giá giáo viên", "");
        }
    }
}