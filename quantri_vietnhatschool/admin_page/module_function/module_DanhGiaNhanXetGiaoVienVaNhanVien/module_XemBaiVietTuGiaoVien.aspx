﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_XemBaiVietTuGiaoVien.aspx.cs" Inherits="admin_page_module_function_module_XemBaiVietTuGiaoVien" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v17.1" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <style>
        .label-checkbox {
            display: inline-block;
            position: relative;
            padding-left: 28px;
            margin-bottom: 12px;
            cursor: pointer;
            font-size: 16px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            line-height: 23px;
        }

            .label-checkbox:nth-child(2) {
                margin: 0 15px;
            }

            .label-checkbox input[type="radio"] {
                position: absolute;
                opacity: 0;
                cursor: pointer;
                height: 0;
                width: 0;
                vertical-align: middle;
            }

            .label-checkbox .checkmark {
                position: absolute;
                top: 0;
                left: 0;
                height: 20px;
                width: 20px;
                background-color: #eee;
            }

            .label-checkbox input[type="radio"]:checked ~ .checkmark {
                background-color: #2196F3;
            }

            .label-checkbox .checkmark:after {
                content: "";
                position: absolute;
                display: none;
            }

            .label-checkbox input[type="radio"]:checked ~ .checkmark:after {
                display: block;
            }

            .label-checkbox .checkmark:after {
                left: 9px;
                top: 5px;
                width: 5px;
                height: 10px;
                border: solid white;
                border-width: 0 3px 3px 0;
                -webkit-transform: rotate(45deg);
                -ms-transform: rotate(45deg);
                transform: rotate(45deg);
            }
    </style>
    <div class="card card-block">
        <asp:UpdatePanel ID="udPopup" runat="server">
            <ContentTemplate>
                <div class="popup-main">
                    <%-- Start tiêu chuẩn 1--%>
                    <div>
                        <button type="button" class="btn btn-success btn-tcpopup" data-toggle="modal" data-target="#tieuchuan">
                            Phản Hồi
                        </button>
                        <!-- Modal -->
                        <div class="modal fade" id="tieuchuan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Phản Hồi
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                        </h5>
                                    </div>
                                    <div class="modal-body">
                                        <label class="tieuchuan__label">Nhập phản hồi tại đây :</label>
                                        <br />
                                        <textarea id="txtPhanHoi" rows="3" runat="server" style="width: 100%"></textarea>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                                        <asp:Button ID="btnPhanHoi" runat="server" CssClass="btn btn-primary" Text="Gửi" OnClick="btnPhanHoi_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="div_content col-12">
                        <div class="col-12">
                            <div class="col-12 form-group">
                                <label>Tiêu chuẩn 1</label>
                                <hr />
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="col-12 form-group">
                                <label class="col-12 form-control-label">Minh chứng 1:</label>
                            </div>
                            <div class="col-12 form-group">
                                <div class="col-12">
                                    <asp:Repeater ID="rpMinhChung1" runat="server">
                                        <ItemTemplate>
                                            <%#Eval("noidungdanhgiagiaovien_minhchung1") %>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Đánh giá từ giáo viên</th>
                                    <th>Đánh giá từ tổ bộ phận</th>
                                    <th>Đánh giá từ hiệu trưởng</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <div class="col-12">
                                            <label>Mức độ đạt được ở tiêu chí 1</label>
                                            <div class="col-12 form-group">
                                                <label class="label-checkbox font-weight-normal">
                                                    Đạt
                                        <input type="radio" id="txtDatTieuChi1_1" value="Đạt" runat="server" name="gvdanhgia1" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Khá
                                        <input type="radio" id="txtDatTieuChi1_2" value="Khá" runat="server" name="gvdanhgia1" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Tốt
                                        <input type="radio" id="txtDatTieuChi1_3" value="Tốt" runat="server" name="gvdanhgia1" />
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-12">
                                            <label>Mức độ đạt được ở tiêu chí 1</label>
                                            <div class="col-12 form-group">
                                                <label class="label-checkbox font-weight-normal">
                                                    Đạt
                                        <input type="radio" id="txtBoPhan1_1" value="Đạt" runat="server" name="todanhgia1" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Khá
                                        <input type="radio" id="txtBoPhan1_2" value="Khá" runat="server" name="todanhgia1" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Tốt
                                        <input type="radio" id="txtBoPhan1_3" value="Tốt" runat="server" name="todanhgia1" />
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-12">
                                            <label>Mức độ đạt được ở tiêu chí 1</label>
                                            <div class="col-12 form-group">
                                                <label class="label-checkbox font-weight-normal">
                                                    Đạt
                                        <input type="radio" id="txtTongDanhGia1_1" value="Đạt" runat="server" name="tongdanhgia1" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Khá
                                        <input type="radio" id="txtTongDanhGia1_2" value="Khá" runat="server" name="tongdanhgia1" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Tốt
                                        <input type="radio" id="txtTongDanhGia1_3" value="Tốt" runat="server" name="tongdanhgia1" />
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="col-12">
                                            <label>Mức độ đạt được ở tiêu chí 2</label>
                                            <div class="col-12 form-group">
                                                <label class="label-checkbox font-weight-normal">
                                                    Đạt
                                        <input type="radio" id="txtDatTieuChi2_1" value="Đạt" runat="server" name="gvdanhgia2" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Khá
                                        <input type="radio" id="txtDatTieuChi2_2" value="Khá" runat="server" name="gvdanhgia2" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Tốt
                                        <input type="radio" id="txtDatTieuChi2_3" value="Tốt" runat="server" name="gvdanhgia2" />
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-12">
                                            <label>Mức độ đạt được ở tiêu chí 2</label>
                                            <div class="col-12 form-group">
                                                <label class="label-checkbox font-weight-normal">
                                                    Đạt
                                        <input type="radio" id="txtBoPhan2_1" value="Đạt" runat="server" name="todanhgia2" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Khá
                                        <input type="radio" id="txtBoPhan2_2" value="Khá" runat="server" name="todanhgia2" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Tốt
                                        <input type="radio" id="txtBoPhan2_3" value="Tốt" runat="server" name="todanhgia2" />
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-12">
                                            <label>Mức độ đạt được ở tiêu chí 2</label>
                                            <div class="col-12 form-group">
                                                <label class="label-checkbox font-weight-normal">
                                                    Đạt
                                        <input type="radio" id="txtTongDanhGia2_1" value="Đạt" runat="server" name="tongdanhgia2" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Khá
                                        <input type="radio" id="txtTongDanhGia2_2" value="Khá" runat="server" name="tongdanhgia2" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Tốt
                                        <input type="radio" id="txtTongDanhGia2_3" value="Tốt" runat="server" name="tongdanhgia2" />
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Nội dung đánh giá tiêu chuẩn 1 từ tổ bộ phận</th>
                                <th>Nội dung đánh giá tiêu chuẩn 1 từ hiệu trưởng</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="width: 50%">
                                    <asp:Repeater ID="rpNhanXetTC1" runat="server">
                                        <ItemTemplate>
                                            <%#Eval("nhanxettieuchuan1") %>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </td>
                                <td style="width: 50%">
                                    <textarea id="txtDanhGiaTieuChuan1" runat="server" rows="5" style="width: 100%"></textarea>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <%-- End tiêu chuẩn 1--%>
                    <%-- Start tiêu chuẩn 2--%>
                    <div class="div_content col-12">
                        <div class="col-12">
                            <div class="col-12 form-group">
                                <label>Tiêu chuẩn 2</label>
                                <hr />
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="col-12 form-group">
                                <label class="col-12 form-control-label">Minh chứng 2:</label>
                            </div>
                            <div class="col-12 form-group">
                                <div class="col-12">
                                    <asp:Repeater ID="rpMinhChung2" runat="server">
                                        <ItemTemplate>
                                            <%#Eval("noidungdanhgiagiaovien_minhchung2") %>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Đánh giá từ giáo viên</th>
                                    <th>Đánh giá từ tổ bộ phận</th>
                                    <th>Đánh giá từ hiệu trưởng</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <div class="col-12">
                                            <label>Mức độ đạt được ở tiêu chí 3</label>
                                            <div class="col-12 form-group">
                                                <label class="label-checkbox font-weight-normal">
                                                    Đạt
                                        <input type="radio" id="txtDatTieuChi3_1" value="Đạt" runat="server" name="gvdanhgia3" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Khá
                                        <input type="radio" id="txtDatTieuChi3_2" value="Khá" runat="server" name="gvdanhgia3" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Tốt
                                        <input type="radio" id="txtDatTieuChi3_3" value="Tốt" runat="server" name="gvdanhgia3" />
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-12">
                                            <label>Mức độ đạt được ở tiêu chí 3</label>
                                            <div class="col-12 form-group">
                                                <label class="label-checkbox font-weight-normal">
                                                    Đạt
                                        <input type="radio" id="txtBoPhan3_1" value="Đạt" runat="server" name="bpdanhgia3" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Khá
                                        <input type="radio" id="txtBoPhan3_2" value="Khá" runat="server" name="bpdanhgia3" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Tốt
                                        <input type="radio" id="txtBoPhan3_3" value="Tốt" runat="server" name="bpdanhgia3" />
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-12">
                                            <label>Mức độ đạt được ở tiêu chí 3</label>
                                            <div class="col-12 form-group">
                                                <label class="label-checkbox font-weight-normal">
                                                    Đạt
                                        <input type="radio" id="txtTongDanhGia3_1" value="Đạt" runat="server" name="tongdanhgia3" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Khá
                                        <input type="radio" id="txtTongDanhGia3_2" value="Khá" runat="server" name="tongdanhgia3" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Tốt
                                        <input type="radio" id="txtTongDanhGia3_3" value="Tốt" runat="server" name="tongdanhgia3" />
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="col-12">
                                            <label>Mức độ đạt được ở tiêu chí 4</label>
                                            <div class="col-12 form-group">
                                                <label class="label-checkbox font-weight-normal">
                                                    Đạt
                                        <input type="radio" id="txtDatTieuChi4_1" value="Đạt" runat="server" name="gvdanhgia4" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Khá
                                        <input type="radio" id="txtDatTieuChi4_2" value="Khá" runat="server" name="gvdanhgia4" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Tốt
                                        <input type="radio" id="txtDatTieuChi4_3" value="Tốt" runat="server" name="gvdanhgia4" />
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-12">
                                            <label>Mức độ đạt được ở tiêu chí 4</label>
                                            <div class="col-12 form-group">
                                                <label class="label-checkbox font-weight-normal">
                                                    Đạt
                                        <input type="radio" id="txtBoPhan4_1" value="Đạt" runat="server" name="bpdanhgia4" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Khá
                                        <input type="radio" id="txtBoPhan4_2" value="Khá" runat="server" name="bpdanhgia4" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Tốt
                                        <input type="radio" id="txtBoPhan4_3" value="Tốt" runat="server" name="bpdanhgia4" />
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-12">
                                            <label>Mức độ đạt được ở tiêu chí 4</label>
                                            <div class="col-12 form-group">
                                                <label class="label-checkbox font-weight-normal">
                                                    Đạt
                                        <input type="radio" id="txtTongDanhGia4_1" value="Đạt" runat="server" name="tongdanhgia4" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Khá
                                        <input type="radio" id="txtTongDanhGia4_2" value="Khá" runat="server" name="tongdanhgia4" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Tốt
                                        <input type="radio" id="txtTongDanhGia4_3" value="Tốt" runat="server" name="tongdanhgia4" />
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="col-12">
                                            <label>Mức độ đạt được ở tiêu chí 5</label>
                                            <div class="col-12 form-group">
                                                <label class="label-checkbox font-weight-normal">
                                                    Đạt
                                        <input type="radio" id="txtDatTieuChi5_1" value="Đạt" runat="server" name="gvdanhgia5" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Khá
                                        <input type="radio" id="txtDatTieuChi5_2" value="Khá" runat="server" name="gvdanhgia5" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Tốt
                                        <input type="radio" id="txtDatTieuChi5_3" value="Tốt" runat="server" name="gvdanhgia5" />
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-12">
                                            <label>Mức độ đạt được ở tiêu chí 5</label>
                                            <div class="col-12 form-group">
                                                <label class="label-checkbox font-weight-normal">
                                                    Đạt
                                        <input type="radio" id="txtBoPhan5_1" value="Đạt" runat="server" name="bpdanhgia5" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Khá
                                        <input type="radio" id="txtBoPhan5_2" value="Khá" runat="server" name="bpdanhgia5" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Tốt
                                        <input type="radio" id="txtBoPhan5_3" value="Tốt" runat="server" name="bpdanhgia5" />
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-12">
                                            <label>Mức độ đạt được ở tiêu chí 5</label>
                                            <div class="col-12 form-group">
                                                <label class="label-checkbox font-weight-normal">
                                                    Đạt
                                        <input type="radio" id="txtTongDanhGia5_1" value="Đạt" runat="server" name="tongdanhgia5" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Khá
                                        <input type="radio" id="txtTongDanhGia5_2" value="Khá" runat="server" name="tongdanhgia5" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Tốt
                                        <input type="radio" id="txtTongDanhGia5_3" value="Tốt" runat="server" name="tongdanhgia5" />
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="col-12">
                                            <label>Mức độ đạt được ở tiêu chí 6</label>
                                            <div class="col-12 form-group">
                                                <label class="label-checkbox font-weight-normal">
                                                    Đạt
                                        <input type="radio" id="txtDatTieuChi6_1" value="Đạt" runat="server" name="gvdanhgia6" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Khá
                                        <input type="radio" id="txtDatTieuChi6_2" value="Khá" runat="server" name="gvdanhgia6" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Tốt
                                        <input type="radio" id="txtDatTieuChi6_3" value="Tốt" runat="server" name="gvdanhgia6" />
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-12">
                                            <label>Mức độ đạt được ở tiêu chí 6</label>
                                            <div class="col-12 form-group">
                                                <label class="label-checkbox font-weight-normal">
                                                    Đạt
                                        <input type="radio" id="txtBoPhan6_1" value="Đạt" runat="server" name="bpdanhgia6" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Khá
                                        <input type="radio" id="txtBoPhan6_2" value="Khá" runat="server" name="bpdanhgia6" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Tốt
                                        <input type="radio" id="txtBoPhan6_3" value="Tốt" runat="server" name="bpdanhgia6" />
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-12">
                                            <label>Mức độ đạt được ở tiêu chí 6</label>
                                            <div class="col-12 form-group">
                                                <label class="label-checkbox font-weight-normal">
                                                    Đạt
                                        <input type="radio" id="txtTongDanhGia6_1" value="Đạt" runat="server" name="tongdanhgia6" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Khá
                                        <input type="radio" id="txtTongDanhGia6_2" value="Khá" runat="server" name="tongdanhgia6" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Tốt
                                        <input type="radio" id="txtTongDanhGia6_3" value="Tốt" runat="server" name="tongdanhgia6" />
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="col-12">
                                            <label>Mức độ đạt được ở tiêu chí 7</label>
                                            <div class="col-12 form-group">
                                                <label class="label-checkbox font-weight-normal">
                                                    Đạt
                                        <input type="radio" id="txtDatTieuChi7_1" value="Đạt" runat="server" name="gvdanhgia7" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Khá
                                        <input type="radio" id="txtDatTieuChi7_2" value="Khá" runat="server" name="gvdanhgia7" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Tốt
                                        <input type="radio" id="txtDatTieuChi7_3" value="Tốt" runat="server" name="gvdanhgia7" />
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-12">
                                            <label>Mức độ đạt được ở tiêu chí 7</label>
                                            <div class="col-12 form-group">
                                                <label class="label-checkbox font-weight-normal">
                                                    Đạt
                                        <input type="radio" id="txtBoPhan7_1" value="Đạt" runat="server" name="bpdanhgia7" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Khá
                                        <input type="radio" id="txtBoPhan7_2" value="Khá" runat="server" name="bpdanhgia7" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Tốt
                                        <input type="radio" id="txtBoPhan7_3" value="Tốt" runat="server" name="bpdanhgia7" />
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-12">
                                            <label>Mức độ đạt được ở tiêu chí 7</label>
                                            <div class="col-12 form-group">
                                                <label class="label-checkbox font-weight-normal">
                                                    Đạt
                                        <input type="radio" id="txtTongDanhGia7_1" value="Đạt" runat="server" name="tongdanhgia7" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Khá
                                        <input type="radio" id="txtTongDanhGia7_2" value="Khá" runat="server" name="tongdanhgia7" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Tốt
                                        <input type="radio" id="txtTongDanhGia7_3" value="Tốt" runat="server" name="tongdanhgia7" />
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Nội dung đánh giá tiêu chuẩn 2 từ tổ bộ phận</th>
                                <th>Nội dung đánh giá tiêu chuẩn 2 từ hiệu trưởng</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="width: 50%">
                                    <asp:Repeater ID="rpNhanXetTC2" runat="server">
                                        <ItemTemplate>
                                            <%#Eval("nhanxettieuchuan2") %>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </td>
                                <td style="width: 50%">
                                    <textarea id="txtDanhGiaTieuChuan2" runat="server" rows="5" style="width: 100%"></textarea>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <%--  <asp:Button ID="btnLuuTieuChuan2" runat="server" ClientIDMode="Static" Text="Lưu" CssClass="btn btn-primary" />--%>
                    <%-- End tiêu chuẩn 2--%>
                    <%-- Start tiêu chuẩn 3--%>
                    <div class="div_content col-12">
                        <div class="col-12">
                            <div class="col-12 form-group">
                                <label>Tiêu chuẩn 3</label>
                                <hr />
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="col-12 form-group">
                                <label class="col-12 form-control-label">Minh chứng 3:</label>
                            </div>
                            <div class="col-12 form-group">
                                <div class="col-12">
                                    <asp:Repeater ID="rpMinhChung3" runat="server">
                                        <ItemTemplate>
                                            <%#Eval("noidungdanhgiagiaovien_minhchung3") %>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Đánh giá từ giáo viên</th>
                                    <th>Đánh giá từ tổ bộ phận</th>
                                    <th>Đánh giá từ hiệu trưởng</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <div class="col-12">
                                            <label>Mức độ đạt được ở tiêu chí 8</label>
                                            <div class="col-12 form-group">
                                                <label class="label-checkbox font-weight-normal">
                                                    Đạt
                                        <input type="radio" id="txtDatTieuChi8_1" value="Đạt" runat="server" name="gvdanhgia8" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Khá
                                        <input type="radio" id="txtDatTieuChi8_2" value="Khá" runat="server" name="gvdanhgia8" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Tốt
                                        <input type="radio" id="txtDatTieuChi8_3" value="Tốt" runat="server" name="gvdanhgia8" />
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-12">
                                            <label>Mức độ đạt được ở tiêu chí 8</label>
                                            <div class="col-12 form-group">
                                                <label class="label-checkbox font-weight-normal">
                                                    Đạt
                                        <input type="radio" id="txtBoPhan8_1" value="Đạt" runat="server" name="bpdanhgia8" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Khá
                                        <input type="radio" id="txtBoPhan8_2" value="Khá" runat="server" name="bpdanhgia8" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Tốt
                                        <input type="radio" id="txtBoPhan8_3" value="Tốt" runat="server" name="bpdanhgia8" />
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-12">
                                            <label>Mức độ đạt được ở tiêu chí 8</label>
                                            <div class="col-12 form-group">
                                                <label class="label-checkbox font-weight-normal">
                                                    Đạt
                                        <input type="radio" id="txtTongDanhGia8_1" value="Đạt" runat="server" name="tongdanhgia8" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Khá
                                        <input type="radio" id="txtTongDanhGia8_2" value="Khá" runat="server" name="tongdanhgia8" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Tốt
                                        <input type="radio" id="txtTongDanhGia8_3" value="Tốt" runat="server" name="tongdanhgia8" />
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="col-12">
                                            <label>Mức độ đạt được ở tiêu chí 9</label>
                                            <div class="col-12 form-group">
                                                <label class="label-checkbox font-weight-normal">
                                                    Đạt
                                        <input type="radio" id="txtDatTieuChi9_1" value="Đạt" runat="server" name="gvdanhgia9" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Khá
                                        <input type="radio" id="txtDatTieuChi9_2" value="Khá" runat="server" name="gvdanhgia9" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Tốt
                                        <input type="radio" id="txtDatTieuChi9_3" value="Tốt" runat="server" name="gvdanhgia9" />
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-12">
                                            <label>Mức độ đạt được ở tiêu chí 9</label>
                                            <div class="col-12 form-group">
                                                <label class="label-checkbox font-weight-normal">
                                                    Đạt
                                        <input type="radio" id="txtBoPhan9_1" value="Đạt" runat="server" name="bpdanhgia9" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Khá
                                        <input type="radio" id="txtBoPhan9_2" value="Khá" runat="server" name="bpdanhgia9" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Tốt
                                        <input type="radio" id="txtBoPhan9_3" value="Tốt" runat="server" name="bpdanhgia9" />
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-12">
                                            <label>Mức độ đạt được ở tiêu chí 9</label>
                                            <div class="col-12 form-group">
                                                <label class="label-checkbox font-weight-normal">
                                                    Đạt
                                        <input type="radio" id="txtTongDanhGia9_1" value="Đạt" runat="server" name="tongdanhgia9" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Khá
                                        <input type="radio" id="txtTongDanhGia9_2" value="Khá" runat="server" name="tongdanhgia9" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Tốt
                                        <input type="radio" id="txtTongDanhGia9_3" value="Tốt" runat="server" name="tongdanhgia9" />
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="col-12">
                                            <label>Mức độ đạt được ở tiêu chí 10</label>
                                            <div class="col-12 form-group">
                                                <label class="label-checkbox font-weight-normal">
                                                    Đạt
                                        <input type="radio" id="txtDatTieuChi10_1" value="Đạt" runat="server" name="gvdanhgia10" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Khá
                                        <input type="radio" id="txtDatTieuChi10_2" value="Khá" runat="server" name="gvdanhgia10" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Tốt
                                        <input type="radio" id="txtDatTieuChi10_3" value="Tốt" runat="server" name="gvdanhgia10" />
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-12">
                                            <label>Mức độ đạt được ở tiêu chí 10</label>
                                            <div class="col-12 form-group">
                                                <label class="label-checkbox font-weight-normal">
                                                    Đạt
                                        <input type="radio" id="txtBoPhan10_1" value="Đạt" runat="server" name="bpdanhgia10" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Khá
                                        <input type="radio" id="txtBoPhan10_2" value="Khá" runat="server" name="bpdanhgia10" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Tốt
                                        <input type="radio" id="txtBoPhan10_3" value="Tốt" runat="server" name="bpdanhgia10" />
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-12">
                                            <label>Mức độ đạt được ở tiêu chí 10</label>
                                            <div class="col-12 form-group">
                                                <label class="label-checkbox font-weight-normal">
                                                    Đạt
                                        <input type="radio" id="txtTongDanhGia10_1" value="Đạt" runat="server" name="tongdanhgia10" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Khá
                                        <input type="radio" id="txtTongDanhGia10_2" value="Khá" runat="server" name="tongdanhgia10" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Tốt
                                        <input type="radio" id="txtTongDanhGia10_3" value="Tốt" runat="server" name="tongdanhgia10" />
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Nội dung đánh giá tiêu chuẩn 3 từ tổ bộ phận</th>
                                <th>Nội dung đánh giá tiêu chuẩn 3 từ hiệu trưởng</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="width: 50%">
                                    <asp:Repeater ID="rpNhanXetTC3" runat="server">
                                        <ItemTemplate>
                                            <%#Eval("nhanxettieuchuan3") %>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </td>
                                <td style="width: 50%">
                                    <textarea id="txtDanhGiaTieuChuan3" runat="server" rows="5" style="width: 100%"></textarea>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <%--<asp:Button ID="btnLuuTieuChuan3" runat="server" ClientIDMode="Static" Text="Lưu" CssClass="btn btn-primary" />--%>
                    <%-- End tiêu chuẩn 3--%>
                    <%-- Start tiêu chuẩn 4--%>
                    <div class="div_content col-12">
                        <div class="col-12">
                            <div class="col-12 form-group">
                                <label>Tiêu chuẩn 4</label>
                                <hr />
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="col-12 form-group">
                                <label class="col-12 form-control-label">Minh chứng 4:</label>
                            </div>
                            <div class="col-12 form-group">
                                <div class="col-12">
                                    <asp:Repeater ID="rpMinhChung4" runat="server">
                                        <ItemTemplate>
                                            <%#Eval("noidungdanhgiagiaovien_minhchung4") %>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Đánh giá từ giáo viên</th>
                                    <th>Đánh giá từ tổ bộ phận</th>
                                    <th>Đánh giá từ hiệu trưởng</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <div class="col-12">
                                            <label>Mức độ đạt được ở tiêu chí 11</label>
                                            <div class="col-12 form-group">
                                                <label class="label-checkbox font-weight-normal">
                                                    Đạt
                                        <input type="radio" id="txtDatTieuChi11_1" value="Đạt" runat="server" name="gvdanhgia11" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Khá
                                        <input type="radio" id="txtDatTieuChi11_2" value="Khá" runat="server" name="gvdanhgia11" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Tốt
                                        <input type="radio" id="txtDatTieuChi11_3" value="Tốt" runat="server" name="gvdanhgia11" />
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-12">
                                            <label>Mức độ đạt được ở tiêu chí 11</label>
                                            <div class="col-12 form-group">
                                                <label class="label-checkbox font-weight-normal">
                                                    Đạt
                                        <input type="radio" id="txtBoPhan11_1" value="Đạt" runat="server" name="bpdanhgia11" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Khá
                                        <input type="radio" id="txtBoPhan11_2" value="Khá" runat="server" name="bpdanhgia11" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Tốt
                                        <input type="radio" id="txtBoPhan11_3" value="Tốt" runat="server" name="bpdanhgia11" />
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-12">
                                            <label>Mức độ đạt được ở tiêu chí 11</label>
                                            <div class="col-12 form-group">
                                                <label class="label-checkbox font-weight-normal">
                                                    Đạt
                                        <input type="radio" id="txtTongDanhGia11_1" value="Đạt" runat="server" name="tongdanhgia11" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Khá
                                        <input type="radio" id="txtTongDanhGia11_2" value="Khá" runat="server" name="tongdanhgia11" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Tốt
                                        <input type="radio" id="txtTongDanhGia11_3" value="Tốt" runat="server" name="tongdanhgia11" />
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="col-12">
                                            <label>Mức độ đạt được ở tiêu chí 12</label>
                                            <div class="col-12 form-group">
                                                <label class="label-checkbox font-weight-normal">
                                                    Đạt
                                        <input type="radio" id="txtDatTieuChi12_1" value="Đạt" runat="server" name="gvdanhgia12" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Khá
                                        <input type="radio" id="txtDatTieuChi12_2" value="Khá" runat="server" name="gvdanhgia12" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Tốt
                                        <input type="radio" id="txtDatTieuChi12_3" value="Tốt" runat="server" name="gvdanhgia12" />
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-12">
                                            <label>Mức độ đạt được ở tiêu chí 12</label>
                                            <div class="col-12 form-group">
                                                <label class="label-checkbox font-weight-normal">
                                                    Đạt
                                        <input type="radio" id="txtBoPhan12_1" value="Đạt" runat="server" name="bpdanhgia12" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Khá
                                        <input type="radio" id="txtBoPhan12_2" value="Khá" runat="server" name="bpdanhgia12" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Tốt
                                        <input type="radio" id="txtBoPhan12_3" value="Tốt" runat="server" name="bpdanhgia12" />
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-12">
                                            <label>Mức độ đạt được ở tiêu chí 12</label>
                                            <div class="col-12 form-group">
                                                <label class="label-checkbox font-weight-normal">
                                                    Đạt
                                        <input type="radio" id="txtTongDanhGia12_1" value="Đạt" runat="server" name="tongdanhgia12" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Khá
                                        <input type="radio" id="txtTongDanhGia12_2" value="Khá" runat="server" name="tongdanhgia12" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Tốt
                                        <input type="radio" id="txtTongDanhGia12_3" value="Tốt" runat="server" name="tongdanhgia12" />
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="col-12">
                                            <label>Mức độ đạt được ở tiêu chí 13</label>
                                            <div class="col-12 form-group">
                                                <label class="label-checkbox font-weight-normal">
                                                    Đạt
                                        <input type="radio" id="txtDatTieuChi13_1" value="Đạt" runat="server" name="gvdanhgia13" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Khá
                                        <input type="radio" id="txtDatTieuChi13_2" value="Khá" runat="server" name="gvdanhgia13" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Tốt
                                        <input type="radio" id="txtDatTieuChi13_3" value="Tốt" runat="server" name="gvdanhgia13" />
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-12">
                                            <label>Mức độ đạt được ở tiêu chí 13</label>
                                            <div class="col-12 form-group">
                                                <label class="label-checkbox font-weight-normal">
                                                    Đạt
                                        <input type="radio" id="txtBoPhan13_1" value="Đạt" runat="server" name="bpdanhgia13" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Khá
                                        <input type="radio" id="txtBoPhan13_2" value="Khá" runat="server" name="bpdanhgia13" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Tốt
                                        <input type="radio" id="txtBoPhan13_3" value="Tốt" runat="server" name="bpdanhgia13" />
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-12">
                                            <label>Mức độ đạt được ở tiêu chí 13</label>
                                            <div class="col-12 form-group">
                                                <label class="label-checkbox font-weight-normal">
                                                    Đạt
                                        <input type="radio" id="txtTongDanhGia13_1" value="Đạt" runat="server" name="tongdanhgia13" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Khá
                                        <input type="radio" id="txtTongDanhGia13_2" value="Khá" runat="server" name="tongdanhgia13" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Tốt
                                        <input type="radio" id="txtTongDanhGia13_3" value="Tốt" runat="server" name="tongdanhgia13" />
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Nội dung đánh giá tiêu chuẩn 4 từ tổ bộ phận</th>
                                <th>Nội dung đánh giá tiêu chuẩn 4 từ hiệu trưởng</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="width: 50%">
                                    <asp:Repeater ID="rpNhanXetTC4" runat="server">
                                        <ItemTemplate>
                                            <%#Eval("nhanxettieuchuan4") %>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </td>
                                <td style="width: 50%">
                                    <textarea id="txtDanhGiaTieuChuan4" runat="server" rows="5" style="width: 100%"></textarea>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <%--<asp:Button ID="btnLuuTieuChuan4" runat="server" ClientIDMode="Static" Text="Lưu" CssClass="btn btn-primary" />--%>
                    <%-- End tiêu chuẩn 4--%>
                    <%-- Start tiêu chuẩn 5--%>
                    <div class="div_content col-12">
                        <div class="col-12">
                            <div class="col-12 form-group">
                                <label>Tiêu chuẩn 5</label>
                                <hr />
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="col-12 form-group">
                                <label class="col-12 form-control-label">Minh chứng 5:</label>
                            </div>
                            <div class="col-12 form-group">
                                <div class="col-12">
                                    <asp:Repeater ID="rpMinhChung5" runat="server">
                                        <ItemTemplate>
                                            <%#Eval("noidungdanhgiagiaovien_minhchung5") %>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Đánh giá từ giáo viên</th>
                                    <th>Đánh giá từ tổ bộ phận</th>
                                    <th>Đánh giá từ hiệu trưởng</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <div class="col-12">
                                            <label>Mức độ đạt được ở tiêu chí 14</label>
                                            <div class="col-12 form-group">
                                                <label class="label-checkbox font-weight-normal">
                                                    Đạt
                                        <input type="radio" id="txtDatTieuChi14_1" value="Đạt" runat="server" name="gvdanhgia14" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Khá
                                        <input type="radio" id="txtDatTieuChi14_2" value="Khá" runat="server" name="gvdanhgia14" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Tốt
                                        <input type="radio" id="txtDatTieuChi14_3" value="Tốt" runat="server" name="gvdanhgia14" />
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-12">
                                            <label>Mức độ đạt được ở tiêu chí 14</label>
                                            <div class="col-12 form-group">
                                                <label class="label-checkbox font-weight-normal">
                                                    Đạt
                                        <input type="radio" id="txtBoPhan14_1" value="Đạt" runat="server" name="bpdanhgia14" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Khá
                                        <input type="radio" id="txtBoPhan14_2" value="Khá" runat="server" name="bpdanhgia14" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Tốt
                                        <input type="radio" id="txtBoPhan14_3" value="Tốt" runat="server" name="bpdanhgia14" />
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-12">
                                            <label>Mức độ đạt được ở tiêu chí 14</label>
                                            <div class="col-12 form-group">
                                                <label class="label-checkbox font-weight-normal">
                                                    Đạt
                                        <input type="radio" id="txtTongDanhGia14_1" value="Đạt" runat="server" name="tongdanhgia14" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Khá
                                        <input type="radio" id="txtTongDanhGia14_2" value="Khá" runat="server" name="tongdanhgia14" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Tốt
                                        <input type="radio" id="txtTongDanhGia14_3" value="Tốt" runat="server" name="tongdanhgia14" />
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="col-12">
                                            <label>Mức độ đạt được ở tiêu chí 15</label>
                                            <div class="col-12 form-group">
                                                <label class="label-checkbox font-weight-normal">
                                                    Đạt
                                        <input type="radio" id="txtDatTieuChi15_1" value="Đạt" runat="server" name="gvdanhgia15" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Khá
                                        <input type="radio" id="txtDatTieuChi15_2" value="Khá" runat="server" name="gvdanhgia15" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Tốt
                                        <input type="radio" id="txtDatTieuChi15_3" value="Tốt" runat="server" name="gvdanhgia15" />
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="col-12">
                                            <label>Mức độ đạt được ở tiêu chí 15</label>
                                            <div class="col-12 form-group">
                                                <label class="label-checkbox font-weight-normal">
                                                    Đạt
                                        <input type="radio" id="txtBoPhan15_1" value="Đạt" runat="server" name="bpdanhgia15" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Khá
                                        <input type="radio" id="txtBoPhan15_2" value="Khá" runat="server" name="bpdanhgia15" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Tốt
                                        <input type="radio" id="txtBoPhan15_3" value="Tốt" runat="server" name="bpdanhgia15" />
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                    <td>

                                        <div class="col-12">
                                            <label>Mức độ đạt được ở tiêu chí 15</label>
                                            <div class="col-12 form-group">
                                                <label class="label-checkbox font-weight-normal">
                                                    Đạt
                                        <input type="radio" id="txtTongDanhGia15_1" value="Đạt" runat="server" name="tongdanhgia15" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Khá
                                        <input type="radio" id="txtTongDanhGia15_2" value="Khá" runat="server" name="tongdanhgia15" />
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="label-checkbox font-weight-normal">
                                                    Tốt
                                        <input type="radio" id="txtTongDanhGia15_3" value="Tốt" runat="server" name="tongdanhgia15" />
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>


                    </div>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Nội dung đánh giá tiêu chuẩn 5 từ tổ bộ phận</th>
                                <th>Nội dung đánh giá tiêu chuẩn 5 từ hiệu trưởng</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="width: 50%">
                                    <asp:Repeater ID="rpNhanXetTC5" runat="server">
                                        <ItemTemplate>
                                            <%#Eval("nhanxettieuchuan5") %>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </td>
                                <td style="width: 50%">
                                    <textarea id="txtDanhGiaTieuChuan5" runat="server" rows="5" style="width: 100%"></textarea>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <%-- <asp:Button ID="btnLuuTieuChuan5" runat="server" ClientIDMode="Static" Text="Lưu" CssClass="btn btn-primary" />--%>
                    <%-- End tiêu chuẩn 5--%>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="mar_but button">
            <asp:UpdatePanel ID="pnLuu" runat="server">
                <ContentTemplate>
                    <asp:Button ID="btnLuu" runat="server" ClientIDMode="Static" Text="Hoàn thành" CssClass="btn btn-primary" OnClick="btnLuu_Click" />
                    <a href="/admin-xem-danh-gia" class="btn btn-primary">Quay lại</a>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

