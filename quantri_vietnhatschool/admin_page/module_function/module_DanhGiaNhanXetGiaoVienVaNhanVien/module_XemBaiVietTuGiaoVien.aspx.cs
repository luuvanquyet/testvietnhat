﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_XemBaiVietTuGiaoVien : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            int taikhoan = Convert.ToInt32(RouteData.Values["id"].ToString());
            var checkTongKetDanhGia = (from tket in db.tbTongKetDanhGiaTieuChuans
                                       join tk in db.admin_Users on tket.taikhoan_id equals tk.username_id
                                       where tk.username_id == taikhoan
                                       orderby tket.tongketdanhgia_id descending
                                       select tket).Take(1);
            var checkNoidung = (from nd in db.tbNoiDungDanhGiaGiaoVienVietNhats
                                join tk in db.admin_Users on nd.taikhoan_id equals tk.username_id
                                where tk.username_id == taikhoan
                                orderby nd.noidungdanhgiagiaovien_id descending
                                select nd).Take(1);
            if(checkNoidung.Count()>0)
            {
                //đổ dữ liệu của đánh giá từ tổ bộ phận
                if (checkNoidung.SingleOrDefault().mucdo1_truongbophan == "Đạt")
                    txtBoPhan1_1.Checked = true;
                if (checkNoidung.SingleOrDefault().mucdo1_truongbophan == "Khá")
                    txtBoPhan1_2.Checked = true;
                if (checkNoidung.SingleOrDefault().mucdo1_truongbophan == "Tốt")
                    txtBoPhan1_3.Checked = true;

                if (checkNoidung.SingleOrDefault().mucdo2_truongbophan == "Đạt")
                    txtBoPhan2_1.Checked = true;
                if (checkNoidung.SingleOrDefault().mucdo2_truongbophan == "Khá")
                    txtBoPhan2_2.Checked = true;
                if (checkNoidung.SingleOrDefault().mucdo2_truongbophan == "Tốt")
                    txtBoPhan2_3.Checked = true;

                if (checkNoidung.SingleOrDefault().mucdo3_truongbophan == "Đạt")
                    txtBoPhan3_1.Checked = true;
                if (checkNoidung.SingleOrDefault().mucdo3_truongbophan == "Khá")
                    txtBoPhan3_2.Checked = true;
                if (checkNoidung.SingleOrDefault().mucdo3_truongbophan == "Tốt")
                    txtBoPhan3_3.Checked = true;

                if (checkNoidung.SingleOrDefault().mucdo4_truongbophan == "Đạt")
                    txtBoPhan4_1.Checked = true;
                if (checkNoidung.SingleOrDefault().mucdo4_truongbophan == "Khá")
                    txtBoPhan4_2.Checked = true;
                if (checkNoidung.SingleOrDefault().mucdo4_truongbophan == "Tốt")
                    txtBoPhan4_3.Checked = true;

                if (checkNoidung.SingleOrDefault().mucdo5_truongbophan == "Đạt")
                    txtBoPhan5_1.Checked = true;
                if (checkNoidung.SingleOrDefault().mucdo5_truongbophan == "Khá")
                    txtBoPhan5_2.Checked = true;
                if (checkNoidung.SingleOrDefault().mucdo5_truongbophan == "Tốt")
                    txtBoPhan5_3.Checked = true;

                if (checkNoidung.SingleOrDefault().mucdo6_truongbophan == "Đạt")
                    txtBoPhan6_1.Checked = true;
                if (checkNoidung.SingleOrDefault().mucdo6_truongbophan == "Khá")
                    txtBoPhan6_2.Checked = true;
                if (checkNoidung.SingleOrDefault().mucdo6_truongbophan == "Tốt")
                    txtBoPhan6_3.Checked = true;

                if (checkNoidung.SingleOrDefault().mucdo7_truongbophan == "Đạt")
                    txtBoPhan7_1.Checked = true;
                if (checkNoidung.SingleOrDefault().mucdo7_truongbophan == "Khá")
                    txtBoPhan7_2.Checked = true;
                if (checkNoidung.SingleOrDefault().mucdo7_truongbophan == "Tốt")
                    txtBoPhan7_3.Checked = true;

                if (checkNoidung.SingleOrDefault().mucdo8_truongbophan == "Đạt")
                    txtBoPhan8_1.Checked = true;
                if (checkNoidung.SingleOrDefault().mucdo8_truongbophan == "Khá")
                    txtBoPhan8_2.Checked = true;
                if (checkNoidung.SingleOrDefault().mucdo8_truongbophan == "Tốt")
                    txtBoPhan8_3.Checked = true;

                if (checkNoidung.SingleOrDefault().mucdo9_truongbophan == "Đạt")
                    txtBoPhan9_1.Checked = true;
                if (checkNoidung.SingleOrDefault().mucdo9_truongbophan == "Khá")
                    txtBoPhan9_2.Checked = true;
                if (checkNoidung.SingleOrDefault().mucdo9_truongbophan == "Tốt")
                    txtBoPhan9_3.Checked = true;

                if (checkNoidung.SingleOrDefault().mucdo10_truongbophan == "Đạt")
                    txtBoPhan10_1.Checked = true;
                if (checkNoidung.SingleOrDefault().mucdo10_truongbophan == "Khá")
                    txtBoPhan10_2.Checked = true;
                if (checkNoidung.SingleOrDefault().mucdo10_truongbophan == "Tốt")
                    txtBoPhan10_3.Checked = true;

                if (checkNoidung.SingleOrDefault().mucdo11_truongbophan == "Đạt")
                    txtBoPhan11_1.Checked = true;
                if (checkNoidung.SingleOrDefault().mucdo11_truongbophan == "Khá")
                    txtBoPhan11_2.Checked = true;
                if (checkNoidung.SingleOrDefault().mucdo11_truongbophan == "Tốt")
                    txtBoPhan11_3.Checked = true;

                if (checkNoidung.SingleOrDefault().mucdo12_truongbophan == "Đạt")
                    txtBoPhan12_1.Checked = true;
                if (checkNoidung.SingleOrDefault().mucdo12_truongbophan == "Khá")
                    txtBoPhan12_2.Checked = true;
                if (checkNoidung.SingleOrDefault().mucdo12_truongbophan == "Tốt")
                    txtBoPhan12_3.Checked = true;

                if (checkNoidung.SingleOrDefault().mucdo13_truongbophan == "Đạt")
                    txtBoPhan13_1.Checked = true;
                if (checkNoidung.SingleOrDefault().mucdo13_truongbophan == "Khá")
                    txtBoPhan13_2.Checked = true;
                if (checkNoidung.SingleOrDefault().mucdo13_truongbophan == "Tốt")
                    txtBoPhan13_3.Checked = true;

                if (checkNoidung.SingleOrDefault().mucdo14_truongbophan == "Đạt")
                    txtBoPhan14_1.Checked = true;
                if (checkNoidung.SingleOrDefault().mucdo14_truongbophan == "Khá")
                    txtBoPhan14_2.Checked = true;
                if (checkNoidung.SingleOrDefault().mucdo14_truongbophan == "Tốt")
                    txtBoPhan14_3.Checked = true;

                if (checkNoidung.SingleOrDefault().mucdo15_truongbophan == "Đạt")
                    txtBoPhan15_1.Checked = true;
                if (checkNoidung.SingleOrDefault().mucdo15_truongbophan == "Khá")
                    txtBoPhan15_2.Checked = true;
                if (checkNoidung.SingleOrDefault().mucdo15_truongbophan == "Tốt")
                    txtBoPhan15_3.Checked = true;
            }
            if (checkTongKetDanhGia.Count() > 0)
            {
                // đổ dữ liệu từ đánh giá của hiệu trưởng
                txtDanhGiaTieuChuan1.Value = checkTongKetDanhGia.Single().tongketdanhgia_nhanxettieuchi1;
                txtDanhGiaTieuChuan2.Value = checkTongKetDanhGia.Single().tongketdanhgia_nhanxettieuchi2;
                txtDanhGiaTieuChuan3.Value = checkTongKetDanhGia.Single().tongketdanhgia_nhanxettieuchi3;
                txtDanhGiaTieuChuan4.Value = checkTongKetDanhGia.Single().tongketdanhgia_nhanxettieuchi4;
                txtDanhGiaTieuChuan5.Value = checkTongKetDanhGia.Single().tongketdanhgia_nhanxettieuchi5;
                if (checkTongKetDanhGia.SingleOrDefault().tongketdanhgia_mucdotieuchi1 == "Đạt")
                    txtTongDanhGia1_1.Checked = true;
                if (checkTongKetDanhGia.SingleOrDefault().tongketdanhgia_mucdotieuchi1 == "Khá")
                    txtTongDanhGia1_2.Checked = true;
                if (checkTongKetDanhGia.SingleOrDefault().tongketdanhgia_mucdotieuchi1 == "Tốt")
                    txtTongDanhGia1_3.Checked = true;

                if (checkTongKetDanhGia.SingleOrDefault().tongketdanhgia_mucdotieuchi2 == "Đạt")
                    txtTongDanhGia2_1.Checked = true;
                if (checkTongKetDanhGia.SingleOrDefault().tongketdanhgia_mucdotieuchi2 == "Khá")
                    txtTongDanhGia2_2.Checked = true;
                if (checkTongKetDanhGia.SingleOrDefault().tongketdanhgia_mucdotieuchi2 == "Tốt")
                    txtTongDanhGia2_3.Checked = true;

                if (checkTongKetDanhGia.SingleOrDefault().tongketdanhgia_mucdotieuchi3 == "Đạt")
                    txtTongDanhGia3_1.Checked = true;
                if (checkTongKetDanhGia.SingleOrDefault().tongketdanhgia_mucdotieuchi3 == "Khá")
                    txtTongDanhGia3_2.Checked = true;
                if (checkTongKetDanhGia.SingleOrDefault().tongketdanhgia_mucdotieuchi3 == "Tốt")
                    txtTongDanhGia3_3.Checked = true;

                if (checkTongKetDanhGia.SingleOrDefault().tongketdanhgia_mucdotieuchi4 == "Đạt")
                    txtTongDanhGia4_1.Checked = true;
                if (checkTongKetDanhGia.SingleOrDefault().tongketdanhgia_mucdotieuchi4 == "Khá")
                    txtTongDanhGia4_2.Checked = true;
                if (checkTongKetDanhGia.SingleOrDefault().tongketdanhgia_mucdotieuchi4 == "Tốt")
                    txtTongDanhGia4_3.Checked = true;

                if (checkTongKetDanhGia.SingleOrDefault().tongketdanhgia_mucdotieuchi5 == "Đạt")
                    txtTongDanhGia5_1.Checked = true;
                if (checkTongKetDanhGia.SingleOrDefault().tongketdanhgia_mucdotieuchi5 == "Khá")
                    txtTongDanhGia5_2.Checked = true;
                if (checkTongKetDanhGia.SingleOrDefault().tongketdanhgia_mucdotieuchi5 == "Tốt")
                    txtTongDanhGia5_3.Checked = true;

                if (checkTongKetDanhGia.SingleOrDefault().tongketdanhgia_mucdotieuchi6 == "Đạt")
                    txtTongDanhGia6_1.Checked = true;
                if (checkTongKetDanhGia.SingleOrDefault().tongketdanhgia_mucdotieuchi6 == "Khá")
                    txtTongDanhGia6_2.Checked = true;
                if (checkTongKetDanhGia.SingleOrDefault().tongketdanhgia_mucdotieuchi6 == "Tốt")
                    txtTongDanhGia6_3.Checked = true;

                if (checkTongKetDanhGia.SingleOrDefault().tongketdanhgia_mucdotieuchi7 == "Đạt")
                    txtTongDanhGia7_1.Checked = true;
                if (checkTongKetDanhGia.SingleOrDefault().tongketdanhgia_mucdotieuchi7 == "Khá")
                    txtTongDanhGia7_2.Checked = true;
                if (checkTongKetDanhGia.SingleOrDefault().tongketdanhgia_mucdotieuchi7 == "Tốt")
                    txtTongDanhGia7_3.Checked = true;

                if (checkTongKetDanhGia.SingleOrDefault().tongketdanhgia_mucdotieuchi8 == "Đạt")
                    txtTongDanhGia8_1.Checked = true;
                if (checkTongKetDanhGia.SingleOrDefault().tongketdanhgia_mucdotieuchi8 == "Khá")
                    txtTongDanhGia8_2.Checked = true;
                if (checkTongKetDanhGia.SingleOrDefault().tongketdanhgia_mucdotieuchi8 == "Tốt")
                    txtTongDanhGia8_3.Checked = true;

                if (checkTongKetDanhGia.SingleOrDefault().tongketdanhgia_mucdotieuchi9 == "Đạt")
                    txtTongDanhGia9_1.Checked = true;
                if (checkTongKetDanhGia.SingleOrDefault().tongketdanhgia_mucdotieuchi9 == "Khá")
                    txtTongDanhGia9_2.Checked = true;
                if (checkTongKetDanhGia.SingleOrDefault().tongketdanhgia_mucdotieuchi9 == "Tốt")
                    txtTongDanhGia9_3.Checked = true;

                if (checkTongKetDanhGia.SingleOrDefault().tongketdanhgia_mucdotieuchi10 == "Đạt")
                    txtTongDanhGia10_1.Checked = true;
                if (checkTongKetDanhGia.SingleOrDefault().tongketdanhgia_mucdotieuchi10 == "Khá")
                    txtTongDanhGia10_2.Checked = true;
                if (checkTongKetDanhGia.SingleOrDefault().tongketdanhgia_mucdotieuchi10 == "Tốt")
                    txtTongDanhGia10_3.Checked = true;

                if (checkTongKetDanhGia.SingleOrDefault().tongketdanhgia_mucdotieuchi11 == "Đạt")
                    txtTongDanhGia11_1.Checked = true;
                if (checkTongKetDanhGia.SingleOrDefault().tongketdanhgia_mucdotieuchi11 == "Khá")
                    txtTongDanhGia11_2.Checked = true;
                if (checkTongKetDanhGia.SingleOrDefault().tongketdanhgia_mucdotieuchi11 == "Tốt")
                    txtTongDanhGia11_3.Checked = true;

                if (checkTongKetDanhGia.SingleOrDefault().tongketdanhgia_mucdotieuchi12 == "Đạt")
                    txtTongDanhGia12_1.Checked = true;
                if (checkTongKetDanhGia.SingleOrDefault().tongketdanhgia_mucdotieuchi12 == "Khá")
                    txtTongDanhGia12_2.Checked = true;
                if (checkTongKetDanhGia.SingleOrDefault().tongketdanhgia_mucdotieuchi12 == "Tốt")
                    txtTongDanhGia12_3.Checked = true;

                if (checkTongKetDanhGia.SingleOrDefault().tongketdanhgia_mucdotieuchi13 == "Đạt")
                    txtTongDanhGia13_1.Checked = true;
                if (checkTongKetDanhGia.SingleOrDefault().tongketdanhgia_mucdotieuchi13 == "Khá")
                    txtTongDanhGia13_2.Checked = true;
                if (checkTongKetDanhGia.SingleOrDefault().tongketdanhgia_mucdotieuchi13 == "Tốt")
                    txtTongDanhGia13_3.Checked = true;

                if (checkTongKetDanhGia.SingleOrDefault().tongketdanhgia_mucdotieuchi14 == "Đạt")
                    txtTongDanhGia14_1.Checked = true;
                if (checkTongKetDanhGia.SingleOrDefault().tongketdanhgia_mucdotieuchi14 == "Khá")
                    txtTongDanhGia14_2.Checked = true;
                if (checkTongKetDanhGia.SingleOrDefault().tongketdanhgia_mucdotieuchi14 == "Tốt")
                    txtTongDanhGia14_3.Checked = true;

                if (checkTongKetDanhGia.SingleOrDefault().tongketdanhgia_mucdotieuchi15 == "Đạt")
                    txtTongDanhGia15_1.Checked = true;
                if (checkTongKetDanhGia.SingleOrDefault().tongketdanhgia_mucdotieuchi15 == "Khá")
                    txtTongDanhGia15_2.Checked = true;
                if (checkTongKetDanhGia.SingleOrDefault().tongketdanhgia_mucdotieuchi15 == "Tốt")
                    txtTongDanhGia15_3.Checked = true;
                //đổ dữ liệu của đánh giá từ giáo viên
                if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi1 == "Đạt")
                    txtDatTieuChi1_1.Checked = true;
                if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi1 == "Khá")
                    txtDatTieuChi1_2.Checked = true;
                if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi1 == "Tốt")
                    txtDatTieuChi1_3.Checked = true;

                if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi2 == "Đạt")
                    txtDatTieuChi2_1.Checked = true;
                if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi2 == "Khá")
                    txtDatTieuChi2_2.Checked = true;
                if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi2 == "Tốt")
                    txtDatTieuChi2_3.Checked = true;

                if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi3 == "Đạt")
                    txtDatTieuChi3_1.Checked = true;
                if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi3 == "Khá")
                    txtDatTieuChi3_2.Checked = true;
                if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi3 == "Tốt")
                    txtDatTieuChi3_3.Checked = true;

                if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi4 == "Đạt")
                    txtDatTieuChi4_1.Checked = true;
                if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi4 == "Khá")
                    txtDatTieuChi4_2.Checked = true;
                if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi4 == "Tốt")
                    txtDatTieuChi4_3.Checked = true;

                if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi5 == "Đạt")
                    txtDatTieuChi5_1.Checked = true;
                if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi5 == "Khá")
                    txtDatTieuChi5_2.Checked = true;
                if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi5 == "Tốt")
                    txtDatTieuChi5_3.Checked = true;

                if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi6 == "Đạt")
                    txtDatTieuChi6_1.Checked = true;
                if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi6 == "Khá")
                    txtDatTieuChi6_2.Checked = true;
                if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi6 == "Tốt")
                    txtDatTieuChi6_3.Checked = true;

                if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi7 == "Đạt")
                    txtDatTieuChi7_1.Checked = true;
                if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi7 == "Khá")
                    txtDatTieuChi7_2.Checked = true;
                if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi7 == "Tốt")
                    txtDatTieuChi7_3.Checked = true;

                if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi8 == "Đạt")
                    txtDatTieuChi8_1.Checked = true;
                if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi8 == "Khá")
                    txtDatTieuChi8_2.Checked = true;
                if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi8 == "Tốt")
                    txtDatTieuChi8_3.Checked = true;

                if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi9 == "Đạt")
                    txtDatTieuChi9_1.Checked = true;
                if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi9 == "Khá")
                    txtDatTieuChi9_2.Checked = true;
                if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi9 == "Tốt")
                    txtDatTieuChi9_3.Checked = true;

                if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi10 == "Đạt")
                    txtDatTieuChi10_1.Checked = true;
                if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi10 == "Khá")
                    txtDatTieuChi10_2.Checked = true;
                if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi10 == "Tốt")
                    txtDatTieuChi10_3.Checked = true;

                if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi11 == "Đạt")
                    txtDatTieuChi11_1.Checked = true;
                if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi11 == "Khá")
                    txtDatTieuChi11_2.Checked = true;
                if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi11 == "Tốt")
                    txtDatTieuChi11_3.Checked = true;

                if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi12 == "Đạt")
                    txtDatTieuChi12_1.Checked = true;
                if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi12 == "Khá")
                    txtDatTieuChi12_2.Checked = true;
                if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi12 == "Tốt")
                    txtDatTieuChi12_3.Checked = true;

                if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi13 == "Đạt")
                    txtDatTieuChi13_1.Checked = true;
                if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi13 == "Khá")
                    txtDatTieuChi13_2.Checked = true;
                if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi13 == "Tốt")
                    txtDatTieuChi13_3.Checked = true;

                if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi14 == "Đạt")
                    txtDatTieuChi14_1.Checked = true;
                if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi14 == "Khá")
                    txtDatTieuChi14_2.Checked = true;
                if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi14 == "Tốt")
                    txtDatTieuChi14_3.Checked = true;

                if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi15 == "Đạt")
                    txtDatTieuChi15_1.Checked = true;
                if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi15 == "Khá")
                    txtDatTieuChi15_2.Checked = true;
                if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi15 == "Tốt")
                    txtDatTieuChi15_3.Checked = true;
                
            }
            else
            {

                if (checkNoidung.Count() > 0)
                {
                    //txtDanhGiaTieuChuan1.Value = checkNoidung.Single().nhanxettieuchuan1;
                    //txtDanhGiaTieuChuan2.Value = checkNoidung.Single().nhanxettieuchuan2;
                    //txtDanhGiaTieuChuan3.Value = checkNoidung.Single().nhanxettieuchuan3;
                    //txtDanhGiaTieuChuan4.Value = checkNoidung.Single().nhanxettieuchuan4;
                    //txtDanhGiaTieuChuan5.Value = checkNoidung.Single().nhanxettieuchuan5;

                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi1 == "Đạt")
                        txtDatTieuChi1_1.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi1 == "Khá")
                        txtDatTieuChi1_2.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi1 == "Tốt")
                        txtDatTieuChi1_3.Checked = true;

                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi2 == "Đạt")
                        txtDatTieuChi2_1.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi2 == "Khá")
                        txtDatTieuChi2_2.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi2 == "Tốt")
                        txtDatTieuChi2_3.Checked = true;

                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi3 == "Đạt")
                        txtDatTieuChi3_1.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi3 == "Khá")
                        txtDatTieuChi3_2.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi3 == "Tốt")
                        txtDatTieuChi3_3.Checked = true;

                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi4 == "Đạt")
                        txtDatTieuChi4_1.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi4 == "Khá")
                        txtDatTieuChi4_2.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi4 == "Tốt")
                        txtDatTieuChi4_3.Checked = true;

                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi5 == "Đạt")
                        txtDatTieuChi5_1.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi5 == "Khá")
                        txtDatTieuChi5_2.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi5 == "Tốt")
                        txtDatTieuChi5_3.Checked = true;

                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi6 == "Đạt")
                        txtDatTieuChi6_1.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi6 == "Khá")
                        txtDatTieuChi6_2.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi6 == "Tốt")
                        txtDatTieuChi6_3.Checked = true;

                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi7 == "Đạt")
                        txtDatTieuChi7_1.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi7 == "Khá")
                        txtDatTieuChi7_2.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi7 == "Tốt")
                        txtDatTieuChi7_3.Checked = true;

                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi8 == "Đạt")
                        txtDatTieuChi8_1.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi8 == "Khá")
                        txtDatTieuChi8_2.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi8 == "Tốt")
                        txtDatTieuChi8_3.Checked = true;

                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi9 == "Đạt")
                        txtDatTieuChi9_1.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi9 == "Khá")
                        txtDatTieuChi9_2.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi9 == "Tốt")
                        txtDatTieuChi9_3.Checked = true;

                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi10 == "Đạt")
                        txtDatTieuChi10_1.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi10 == "Khá")
                        txtDatTieuChi10_2.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi10 == "Tốt")
                        txtDatTieuChi10_3.Checked = true;

                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi11 == "Đạt")
                        txtDatTieuChi11_1.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi11 == "Khá")
                        txtDatTieuChi11_2.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi11 == "Tốt")
                        txtDatTieuChi11_3.Checked = true;

                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi12 == "Đạt")
                        txtDatTieuChi12_1.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi12 == "Khá")
                        txtDatTieuChi12_2.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi12 == "Tốt")
                        txtDatTieuChi12_3.Checked = true;

                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi13 == "Đạt")
                        txtDatTieuChi13_1.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi13 == "Khá")
                        txtDatTieuChi13_2.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi13 == "Tốt")
                        txtDatTieuChi13_3.Checked = true;

                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi14 == "Đạt")
                        txtDatTieuChi14_1.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi14 == "Khá")
                        txtDatTieuChi14_2.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi14 == "Tốt")
                        txtDatTieuChi14_3.Checked = true;

                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi15 == "Đạt")
                        txtDatTieuChi15_1.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi15 == "Khá")
                        txtDatTieuChi15_2.Checked = true;
                    if (checkNoidung.SingleOrDefault().noidungdanhgiagiaovien_mucdotieuchi15 == "Tốt")
                        txtDatTieuChi15_3.Checked = true;
                }
            }
            rpMinhChung1.DataSource = checkNoidung;
            rpMinhChung1.DataBind();
            rpMinhChung2.DataSource = checkNoidung;
            rpMinhChung2.DataBind();
            rpMinhChung3.DataSource = checkNoidung;
            rpMinhChung3.DataBind();
            rpMinhChung4.DataSource = checkNoidung;
            rpMinhChung4.DataBind();
            rpMinhChung5.DataSource = checkNoidung;
            rpMinhChung5.DataBind();
            rpNhanXetTC1.DataSource = checkNoidung;
            rpNhanXetTC1.DataBind();
            rpNhanXetTC2.DataSource = checkNoidung;
            rpNhanXetTC2.DataBind();
            rpNhanXetTC3.DataSource = checkNoidung;
            rpNhanXetTC3.DataBind();
            rpNhanXetTC4.DataSource = checkNoidung;
            rpNhanXetTC4.DataBind();
            rpNhanXetTC5.DataSource = checkNoidung;
            rpNhanXetTC5.DataBind();
            Disable();
        }
    }

    protected void btnLuu_Click(object sender, EventArgs e)
    {
        var checkTaiKhoan = (from tk in db.admin_Users where tk.username_id == Convert.ToInt32(RouteData.Values["id"].ToString()) select tk).SingleOrDefault();
        if (checkTaiKhoan != null)
        {
            string tieuchicuatieuchuan1 = "";
            if (txtTongDanhGia1_1.Checked == true)
            {
                tieuchicuatieuchuan1 = "Đạt";
            }
            else if (txtTongDanhGia1_2.Checked == true)
            {
                tieuchicuatieuchuan1 = "Khá";
            }
            else if (txtTongDanhGia1_3.Checked == true)
            {
                tieuchicuatieuchuan1 = "Tốt";
            }
            else
            {
                alert.alert_Error(Page, "Vui lòng chọn mức độ tiêu chi 1 để đánh giá", "");
            }
            string tieuchicuatieuchuan2 = "";
            if (txtTongDanhGia2_1.Checked == true)
            {
                tieuchicuatieuchuan2 = "Đạt";
            }
            else if (txtTongDanhGia2_2.Checked == true)
            {
                tieuchicuatieuchuan2 = "Khá";
            }
            else if (txtTongDanhGia2_3.Checked == true)
            {
                tieuchicuatieuchuan2 = "Tốt";
            }
            else
            {
                alert.alert_Error(Page, "Vui lòng chọn mức độ tiêu chi 2 để đánh giá", "");
            }
            string tieuchicuatieuchuan3 = "";
            if (txtTongDanhGia3_1.Checked == true)
            {
                tieuchicuatieuchuan3 = "Đạt";
            }
            else if (txtTongDanhGia3_2.Checked == true)
            {
                tieuchicuatieuchuan3 = "Khá";
            }
            else if (txtTongDanhGia3_3.Checked == true)
            {
                tieuchicuatieuchuan3 = "Tốt";
            }
            else
            {
                alert.alert_Error(Page, "Vui lòng chọn mức độ tiêu chi 3 để đánh giá", "");
            }
            string tieuchicuatieuchuan4 = "";
            if (txtTongDanhGia4_1.Checked == true)
            {
                tieuchicuatieuchuan4 = "Đạt";
            }
            else if (txtTongDanhGia4_2.Checked == true)
            {
                tieuchicuatieuchuan4 = "Khá";
            }
            else if (txtTongDanhGia4_3.Checked == true)
            {
                tieuchicuatieuchuan4 = "Tốt";
            }
            else
            {
                alert.alert_Error(Page, "Vui lòng chọn mức độ tiêu chi 4 để đánh giá", "");
            }
            string tieuchicuatieuchuan5 = "";
            if (txtTongDanhGia5_1.Checked == true)
            {
                tieuchicuatieuchuan5 = "Đạt";
            }
            else if (txtTongDanhGia5_2.Checked == true)
            {
                tieuchicuatieuchuan5 = "Khá";
            }
            else if (txtTongDanhGia5_3.Checked == true)
            {
                tieuchicuatieuchuan5 = "Tốt";
            }
            else
            {
                alert.alert_Error(Page, "Vui lòng chọn mức độ tiêu chi 5 để đánh giá", "");
            }
            string tieuchicuatieuchuan6 = "";
            if (txtTongDanhGia6_1.Checked == true)
            {
                tieuchicuatieuchuan6 = "Đạt";
            }
            else if (txtTongDanhGia6_2.Checked == true)
            {
                tieuchicuatieuchuan6 = "Khá";
            }
            else if (txtTongDanhGia6_3.Checked == true)
            {
                tieuchicuatieuchuan6 = "Tốt";
            }
            else
            {
                alert.alert_Error(Page, "Vui lòng chọn mức độ tiêu chi 6 để đánh giá", "");
            }
            string tieuchicuatieuchuan7 = "";
            if (txtTongDanhGia7_1.Checked == true)
            {
                tieuchicuatieuchuan7 = "Đạt";
            }
            else if (txtTongDanhGia7_2.Checked == true)
            {
                tieuchicuatieuchuan7 = "Khá";
            }
            else if (txtTongDanhGia7_3.Checked == true)
            {
                tieuchicuatieuchuan7 = "Tốt";
            }
            else
            {
                alert.alert_Error(Page, "Vui lòng chọn mức độ tiêu chi 7 để đánh giá", "");
            }
            string tieuchicuatieuchuan8 = "";
            if (txtTongDanhGia8_1.Checked == true)
            {
                tieuchicuatieuchuan8 = "Đạt";
            }
            else if (txtTongDanhGia8_2.Checked == true)
            {
                tieuchicuatieuchuan8 = "Khá";
            }
            else if (txtTongDanhGia8_3.Checked == true)
            {
                tieuchicuatieuchuan8 = "Tốt";
            }
            else
            {
                alert.alert_Error(Page, "Vui lòng chọn mức độ tiêu chi 8 để đánh giá", "");
            }
            string tieuchicuatieuchuan9 = "";
            if (txtTongDanhGia9_1.Checked == true)
            {
                tieuchicuatieuchuan9 = "Đạt";
            }
            else if (txtTongDanhGia9_2.Checked == true)
            {
                tieuchicuatieuchuan9 = "Khá";
            }
            else if (txtTongDanhGia9_3.Checked == true)
            {
                tieuchicuatieuchuan9 = "Tốt";
            }
            else
            {
                alert.alert_Error(Page, "Vui lòng chọn mức độ tiêu chi 9 để đánh giá", "");
            }
            string tieuchicuatieuchuan10 = "";
            if (txtTongDanhGia10_1.Checked == true)
            {
                tieuchicuatieuchuan10 = "Đạt";
            }
            else if (txtTongDanhGia10_2.Checked == true)
            {
                tieuchicuatieuchuan10 = "Khá";
            }
            else if (txtTongDanhGia10_3.Checked == true)
            {
                tieuchicuatieuchuan10 = "Tốt";
            }
            else
            {
                alert.alert_Error(Page, "Vui lòng chọn mức độ tiêu chi 10 để đánh giá", "");
            }
            string tieuchicuatieuchuan11 = "";
            if (txtTongDanhGia11_1.Checked == true)
            {
                tieuchicuatieuchuan11 = "Đạt";
            }
            else if (txtTongDanhGia11_2.Checked == true)
            {
                tieuchicuatieuchuan11 = "Khá";
            }
            else if (txtTongDanhGia11_3.Checked == true)
            {
                tieuchicuatieuchuan11 = "Tốt";
            }
            else
            {
                alert.alert_Error(Page, "Vui lòng chọn mức độ tiêu chi 11 để đánh giá", "");
            }
            string tieuchicuatieuchuan12 = "";
            if (txtTongDanhGia12_1.Checked == true)
            {
                tieuchicuatieuchuan12 = "Đạt";
            }
            else if (txtTongDanhGia12_2.Checked == true)
            {
                tieuchicuatieuchuan12 = "Khá";
            }
            else if (txtTongDanhGia12_3.Checked == true)
            {
                tieuchicuatieuchuan12 = "Tốt";
            }
            else
            {
                alert.alert_Error(Page, "Vui lòng chọn mức độ tiêu chi 12 để đánh giá", "");
            }
            string tieuchicuatieuchuan13 = "";
            if (txtTongDanhGia13_1.Checked == true)
            {
                tieuchicuatieuchuan13 = "Đạt";
            }
            else if (txtTongDanhGia13_2.Checked == true)
            {
                tieuchicuatieuchuan13 = "Khá";
            }
            else if (txtTongDanhGia13_3.Checked == true)
            {
                tieuchicuatieuchuan13 = "Tốt";
            }
            else
            {
                alert.alert_Error(Page, "Vui lòng chọn mức độ tiêu chi 13 để đánh giá", "");
            }
            string tieuchicuatieuchuan14 = "";
            if (txtTongDanhGia14_1.Checked == true)
            {
                tieuchicuatieuchuan14 = "Đạt";
            }
            else if (txtTongDanhGia14_2.Checked == true)
            {
                tieuchicuatieuchuan14 = "Khá";
            }
            else if (txtTongDanhGia14_3.Checked == true)
            {
                tieuchicuatieuchuan14 = "Tốt";
            }
            else
            {
                alert.alert_Error(Page, "Vui lòng chọn mức độ tiêu chi 14 để đánh giá", "");
            }
            string tieuchicuatieuchuan15 = "";
            if (txtTongDanhGia15_1.Checked == true)
            {
                tieuchicuatieuchuan15 = "Đạt";
            }
            else if (txtTongDanhGia15_2.Checked == true)
            {
                tieuchicuatieuchuan15 = "Khá";
            }
            else if (txtTongDanhGia15_3.Checked == true)
            {
                tieuchicuatieuchuan15 = "Tốt";
            }
            else
            {
                alert.alert_Error(Page, "Vui lòng chọn mức độ tiêu chi 15 để đánh giá", "");
            }

            try
            {
                // tiêu chí 1
                if (tieuchicuatieuchuan1 != "" && tieuchicuatieuchuan2 != "" && tieuchicuatieuchuan3 != "" && tieuchicuatieuchuan4 != "" && tieuchicuatieuchuan5 != "" && tieuchicuatieuchuan6 != "" && tieuchicuatieuchuan7 != "" && tieuchicuatieuchuan8 != "" && tieuchicuatieuchuan9 != "" && tieuchicuatieuchuan10 != "" && tieuchicuatieuchuan11 != "" && tieuchicuatieuchuan12 != "" && tieuchicuatieuchuan13 != "" && tieuchicuatieuchuan14 != "" && tieuchicuatieuchuan15 != "")
                {
                    var checkTaiKhoanDaCoDanhGiaLanDauChua = (from dg in db.tbNoiDungDanhGiaGiaoVienVietNhats where dg.taikhoan_id == Convert.ToInt32(RouteData.Values["id"].ToString()) orderby dg.noidungdanhgiagiaovien_id descending select dg).FirstOrDefault();
                    {
                        if (checkTaiKhoanDaCoDanhGiaLanDauChua.active == true)
                        {
                            // trường hợp này đã insert vào lần đầu rồi
                            tbTongKetDanhGiaTieuChuan update = (from up in db.tbTongKetDanhGiaTieuChuans where up.taikhoan_id == Convert.ToInt32(RouteData.Values["id"].ToString()) orderby up.tongketdanhgia_id descending select up).First();
                            update.tongketdanhgia_mucdotieuchi1 = tieuchicuatieuchuan1;
                            update.tongketdanhgia_mucdotieuchi2 = tieuchicuatieuchuan2;
                            update.tongketdanhgia_mucdotieuchi3 = tieuchicuatieuchuan3;
                            update.tongketdanhgia_mucdotieuchi4 = tieuchicuatieuchuan4;
                            update.tongketdanhgia_mucdotieuchi5 = tieuchicuatieuchuan5;
                            update.tongketdanhgia_mucdotieuchi6 = tieuchicuatieuchuan6;
                            update.tongketdanhgia_mucdotieuchi7 = tieuchicuatieuchuan7;
                            update.tongketdanhgia_mucdotieuchi8 = tieuchicuatieuchuan8;
                            update.tongketdanhgia_mucdotieuchi9 = tieuchicuatieuchuan9;
                            update.tongketdanhgia_mucdotieuchi10 = tieuchicuatieuchuan10;
                            update.tongketdanhgia_mucdotieuchi11 = tieuchicuatieuchuan11;
                            update.tongketdanhgia_mucdotieuchi12 = tieuchicuatieuchuan12;
                            update.tongketdanhgia_mucdotieuchi13 = tieuchicuatieuchuan13;
                            update.tongketdanhgia_mucdotieuchi14 = tieuchicuatieuchuan14;
                            update.tongketdanhgia_mucdotieuchi15 = tieuchicuatieuchuan15;
                            update.tongketdanhgia_ngaydanhgia = DateTime.Now;
                            update.tongketdanhgia_status = true;
                            update.tongketdanhgia_hidden = false;
                            db.SubmitChanges();
                        }
                        else
                        {
                            // trường hợp chưa insert vào 
                            tbTongKetDanhGiaTieuChuan insertquantri = new tbTongKetDanhGiaTieuChuan();
                            insertquantri.tongketdanhgia_mucdotieuchi1 = tieuchicuatieuchuan1;
                            insertquantri.tongketdanhgia_mucdotieuchi2 = tieuchicuatieuchuan2;
                            insertquantri.tongketdanhgia_mucdotieuchi3 = tieuchicuatieuchuan3;
                            insertquantri.tongketdanhgia_mucdotieuchi4 = tieuchicuatieuchuan4;
                            insertquantri.tongketdanhgia_mucdotieuchi5 = tieuchicuatieuchuan5;
                            insertquantri.tongketdanhgia_mucdotieuchi6 = tieuchicuatieuchuan6;
                            insertquantri.tongketdanhgia_mucdotieuchi7 = tieuchicuatieuchuan7;
                            insertquantri.tongketdanhgia_mucdotieuchi8 = tieuchicuatieuchuan8;
                            insertquantri.tongketdanhgia_mucdotieuchi9 = tieuchicuatieuchuan9;
                            insertquantri.tongketdanhgia_mucdotieuchi10 = tieuchicuatieuchuan10;
                            insertquantri.tongketdanhgia_mucdotieuchi11 = tieuchicuatieuchuan11;
                            insertquantri.tongketdanhgia_mucdotieuchi12 = tieuchicuatieuchuan12;
                            insertquantri.tongketdanhgia_mucdotieuchi13 = tieuchicuatieuchuan13;
                            insertquantri.tongketdanhgia_mucdotieuchi14 = tieuchicuatieuchuan14;
                            insertquantri.tongketdanhgia_mucdotieuchi15 = tieuchicuatieuchuan15;
                            insertquantri.taikhoan_id = Convert.ToInt32(RouteData.Values["id"].ToString());
                            insertquantri.tongketdanhgia_status = true;
                            insertquantri.tongketdanhgia_ngaydanhgia = DateTime.Now;
                            db.tbTongKetDanhGiaTieuChuans.InsertOnSubmit(insertquantri);
                            checkTaiKhoanDaCoDanhGiaLanDauChua.active = true;
                            db.SubmitChanges();
                        }
                    }
                    checkKetQua(Convert.ToInt32(RouteData.Values["id"].ToString()));
                }
            }
            catch (Exception ex)
            {
                alert.alert_Error(Page, "Error", "");
            }
        }
    }
    protected void checkKetQua(int account_id)
    {
        // check tài khoản đã có đánh giá lần đâu chưa

        int countTot = 0;
        var checkDataAccount = (from nd in db.tbTongKetDanhGiaTieuChuans where nd.taikhoan_id == account_id orderby nd.tongketdanhgia_id descending select nd).FirstOrDefault();
        // Mức tốt : tiêu chuẩn 2 phải tốt, trên 2/3 các tiêu chí phải tốt, ko có bất cừ trường hợp nào chỉ đạt
        if (checkDataAccount != null)
        {
            if (checkDataAccount.tongketdanhgia_mucdotieuchi3 == "Tốt"
                && checkDataAccount.tongketdanhgia_mucdotieuchi4 == "Tốt"
                && checkDataAccount.tongketdanhgia_mucdotieuchi5 == "Tốt"
                && checkDataAccount.tongketdanhgia_mucdotieuchi6 == "Tốt"
                && checkDataAccount.tongketdanhgia_mucdotieuchi7 == "Tốt")
            {
                countTot = 5;
                if (checkDataAccount.tongketdanhgia_mucdotieuchi1 == "Tốt")
                    countTot = countTot + 1;
                if (checkDataAccount.tongketdanhgia_mucdotieuchi2 == "Tốt")
                    countTot = countTot + 1;
                if (checkDataAccount.tongketdanhgia_mucdotieuchi8 == "Tốt")
                    countTot = countTot + 1;
                if (checkDataAccount.tongketdanhgia_mucdotieuchi9 == "Tốt")
                    countTot = countTot + 1;
                if (checkDataAccount.tongketdanhgia_mucdotieuchi10 == "Tốt")
                    countTot = countTot + 1;
                if (checkDataAccount.tongketdanhgia_mucdotieuchi11 == "Tốt")
                    countTot = countTot + 1;
                if (checkDataAccount.tongketdanhgia_mucdotieuchi12 == "Tốt")
                    countTot = countTot + 1;
                if (checkDataAccount.tongketdanhgia_mucdotieuchi13 == "Tốt")
                    countTot = countTot + 1;
                if (checkDataAccount.tongketdanhgia_mucdotieuchi14 == "Tốt")
                    countTot = countTot + 1;
                if (checkDataAccount.tongketdanhgia_mucdotieuchi15 == "Tốt")
                    countTot = countTot + 1;
                if (countTot >= 10)
                {
                    if (checkDataAccount.tongketdanhgia_mucdotieuchi1 == "Đạt" ||
                        checkDataAccount.tongketdanhgia_mucdotieuchi2 == "Đạt" ||
                        checkDataAccount.tongketdanhgia_mucdotieuchi8 == "Đạt" ||
                        checkDataAccount.tongketdanhgia_mucdotieuchi9 == "Đạt" ||
                        checkDataAccount.tongketdanhgia_mucdotieuchi10 == "Đạt" ||
                        checkDataAccount.tongketdanhgia_mucdotieuchi11 == "Đạt" ||
                        checkDataAccount.tongketdanhgia_mucdotieuchi12 == "Đạt" ||
                        checkDataAccount.tongketdanhgia_mucdotieuchi13 == "Đạt" ||
                        checkDataAccount.tongketdanhgia_mucdotieuchi14 == "Đạt" ||
                        checkDataAccount.tongketdanhgia_mucdotieuchi15 == "Đạt"
                        )
                    {
                        checkDataAccount.tongketdanhgia_xeploai = "Khá";
                    }
                    else
                    {
                        checkDataAccount.tongketdanhgia_xeploai = "Tốt";
                    }
                }
                else
                {
                    checkDataAccount.tongketdanhgia_xeploai = "Khá";
                }
            }
            else
            {
                checkDataAccount.tongketdanhgia_xeploai = "Khá";
                if (checkDataAccount.tongketdanhgia_mucdotieuchi3 == "Khá"
                && checkDataAccount.tongketdanhgia_mucdotieuchi4 == "Khá"
                && checkDataAccount.tongketdanhgia_mucdotieuchi5 == "Khá"
                && checkDataAccount.tongketdanhgia_mucdotieuchi6 == "Khá"
                && checkDataAccount.tongketdanhgia_mucdotieuchi7 == "Khá")
                {
                    countTot = 5;
                    if (checkDataAccount.tongketdanhgia_mucdotieuchi1 == "Khá")
                        countTot = countTot + 1;
                    if (checkDataAccount.tongketdanhgia_mucdotieuchi2 == "Khá")
                        countTot = countTot + 1;
                    if (checkDataAccount.tongketdanhgia_mucdotieuchi8 == "Khá")
                        countTot = countTot + 1;
                    if (checkDataAccount.tongketdanhgia_mucdotieuchi9 == "Khá")
                        countTot = countTot + 1;
                    if (checkDataAccount.tongketdanhgia_mucdotieuchi10 == "Khá")
                        countTot = countTot + 1;
                    if (checkDataAccount.tongketdanhgia_mucdotieuchi11 == "Khá")
                        countTot = countTot + 1;
                    if (checkDataAccount.tongketdanhgia_mucdotieuchi12 == "Khá")
                        countTot = countTot + 1;
                    if (checkDataAccount.tongketdanhgia_mucdotieuchi13 == "Khá")
                        countTot = countTot + 1;
                    if (checkDataAccount.tongketdanhgia_mucdotieuchi14 == "Khá")
                        countTot = countTot + 1;
                    if (checkDataAccount.tongketdanhgia_mucdotieuchi15 == "Khá")
                        countTot = countTot + 1;
                    if (countTot >= 10)
                    {
                        checkDataAccount.tongketdanhgia_xeploai = "Khá";

                    }
                    else
                    {
                        checkDataAccount.tongketdanhgia_xeploai = "Đạt";
                    }
                }
            }
            checkDataAccount.tongketdanhgia_nhanxettieuchi1 = txtDanhGiaTieuChuan1.Value;
            checkDataAccount.tongketdanhgia_nhanxettieuchi2 = txtDanhGiaTieuChuan2.Value;
            checkDataAccount.tongketdanhgia_nhanxettieuchi3 = txtDanhGiaTieuChuan3.Value;
            checkDataAccount.tongketdanhgia_nhanxettieuchi4 = txtDanhGiaTieuChuan4.Value;
            checkDataAccount.tongketdanhgia_nhanxettieuchi5 = txtDanhGiaTieuChuan5.Value;
            checkDataAccount.tongketdanhgia_status = true;
            db.SubmitChanges();
            alert.alert_Success(Page, "Đã hoàn thành việc đánh giá giáo viên", "");
        }
    }
    protected void Disable()
    {
        txtDatTieuChi1_1.Disabled = true;
        txtDatTieuChi1_2.Disabled = true;
        txtDatTieuChi1_3.Disabled = true;
        txtDatTieuChi2_1.Disabled = true;
        txtDatTieuChi2_2.Disabled = true;
        txtDatTieuChi2_3.Disabled = true;
        txtDatTieuChi3_1.Disabled = true;
        txtDatTieuChi3_2.Disabled = true;
        txtDatTieuChi3_3.Disabled = true;
        txtDatTieuChi4_1.Disabled = true;
        txtDatTieuChi4_2.Disabled = true;
        txtDatTieuChi4_3.Disabled = true;
        txtDatTieuChi5_1.Disabled = true;
        txtDatTieuChi5_2.Disabled = true;
        txtDatTieuChi5_3.Disabled = true;
        txtDatTieuChi6_1.Disabled = true;
        txtDatTieuChi6_2.Disabled = true;
        txtDatTieuChi6_3.Disabled = true;
        txtDatTieuChi7_1.Disabled = true;
        txtDatTieuChi7_2.Disabled = true;
        txtDatTieuChi7_3.Disabled = true;
        txtDatTieuChi8_1.Disabled = true;
        txtDatTieuChi8_2.Disabled = true;
        txtDatTieuChi8_3.Disabled = true;
        txtDatTieuChi9_1.Disabled = true;
        txtDatTieuChi9_2.Disabled = true;
        txtDatTieuChi9_3.Disabled = true;
        txtDatTieuChi10_1.Disabled = true;
        txtDatTieuChi10_2.Disabled = true;
        txtDatTieuChi10_3.Disabled = true;
        txtDatTieuChi11_1.Disabled = true;
        txtDatTieuChi11_2.Disabled = true;
        txtDatTieuChi11_3.Disabled = true;
        txtDatTieuChi12_1.Disabled = true;
        txtDatTieuChi12_2.Disabled = true;
        txtDatTieuChi12_3.Disabled = true;
        txtDatTieuChi13_1.Disabled = true;
        txtDatTieuChi13_2.Disabled = true;
        txtDatTieuChi13_3.Disabled = true;
        txtDatTieuChi14_1.Disabled = true;
        txtDatTieuChi14_2.Disabled = true;
        txtDatTieuChi14_3.Disabled = true;
        txtDatTieuChi15_1.Disabled = true;
        txtDatTieuChi15_2.Disabled = true;
        txtDatTieuChi15_3.Disabled = true;
        txtBoPhan1_1.Disabled = true;
        txtBoPhan1_2.Disabled = true;
        txtBoPhan1_3.Disabled = true;
        txtBoPhan2_1.Disabled = true;
        txtBoPhan2_2.Disabled = true;
        txtBoPhan2_3.Disabled = true;
        txtBoPhan3_1.Disabled = true;
        txtBoPhan3_2.Disabled = true;
        txtBoPhan3_3.Disabled = true;
        txtBoPhan4_1.Disabled = true;
        txtBoPhan4_2.Disabled = true;
        txtBoPhan4_3.Disabled = true;
        txtBoPhan5_1.Disabled = true;
        txtBoPhan5_2.Disabled = true;
        txtBoPhan5_3.Disabled = true;
        txtBoPhan6_1.Disabled = true;
        txtBoPhan6_2.Disabled = true;
        txtBoPhan6_3.Disabled = true;
        txtBoPhan7_1.Disabled = true;
        txtBoPhan7_2.Disabled = true;
        txtBoPhan7_3.Disabled = true;
        txtBoPhan8_1.Disabled = true;
        txtBoPhan8_2.Disabled = true;
        txtBoPhan8_3.Disabled = true;
        txtBoPhan9_1.Disabled = true;
        txtBoPhan9_2.Disabled = true;
        txtBoPhan9_3.Disabled = true;
        txtBoPhan10_1.Disabled = true;
        txtBoPhan10_2.Disabled = true;
        txtBoPhan10_3.Disabled = true;
        txtBoPhan11_1.Disabled = true;
        txtBoPhan11_2.Disabled = true;
        txtBoPhan11_3.Disabled = true;
        txtBoPhan12_1.Disabled = true;
        txtBoPhan12_2.Disabled = true;
        txtBoPhan12_3.Disabled = true;
        txtBoPhan13_1.Disabled = true;
        txtBoPhan13_2.Disabled = true;
        txtBoPhan13_3.Disabled = true;
        txtBoPhan14_1.Disabled = true;
        txtBoPhan14_2.Disabled = true;
        txtBoPhan14_3.Disabled = true;
        txtBoPhan15_1.Disabled = true;
        txtBoPhan15_2.Disabled = true;
        txtBoPhan15_3.Disabled = true;

    }
   
    protected void btnPhanHoi_Click(object sender, EventArgs e)
    {
        int taikhoan = Convert.ToInt32(RouteData.Values["id"].ToString());
        var user = (from u in db.admin_Users where u.username_id == taikhoan select u).SingleOrDefault(); 
        SendMail(user.username_email);
        alert.alert_Success(Page, "Tin phản hồi đã gửi về cho giáo viên", "");
    }
    private bool SendMail(string email)
    {

        if (email != "")
        {
            try
            {
                var fromAddress = "thongbaovietnhatschool@gmail.com";//  Email Address from where you send the mail 
                var toAddress = email;
                const string fromPassword = "neiabcekdjluofid";
                string subject, title;
                title = "Thông báo từ Thầy Hiệu Trưởng";
                subject = "<!DOCTYPE html><html><head><title></title></head><body ><div>" + txtPhanHoi.Value +"</div></body></html>";
                var smtp = new System.Net.Mail.SmtpClient();
                {
                    smtp.Host = "smtp.gmail.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    smtp.Credentials = new NetworkCredential(fromAddress, fromPassword);
                    smtp.Timeout = 20000;
                }
                MailMessage mm = new MailMessage();
                mm.From = new MailAddress(fromAddress, "Trường Liên cấp Việt Nhật");
                mm.Subject = title;
                mm.To.Add(toAddress);
                mm.IsBodyHtml = true;
                mm.Body = subject;
                smtp.Send(mm);
                return true;
            }
            catch
            {
                return false;
            }
        }
        else
            return false;
    }
}