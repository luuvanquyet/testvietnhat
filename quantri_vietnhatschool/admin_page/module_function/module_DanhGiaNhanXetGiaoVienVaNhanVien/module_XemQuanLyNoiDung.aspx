﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="module_XemQuanLyNoiDung.aspx.cs" Inherits="admin_page_module_function_module_DanhGiaNhanXetGiaoVienVaNhanVien_module_XemQuanLyNoiDung" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Administrator</title>
    <link href="../../../admin_css/vendor.css" rel="stylesheet" />
</head>
<body>
    <style>
        div.title h3.title, h3.title {
            text-align: center;
            text-transform: uppercase;
            padding: 20px;
            top: 20px;
            left: 35%;
        }

        .table {
            cursor: default;
        }

            .table tr.tieuchuan th, .table thead tr th, .table tr.bophan td {
                text-align: center;
                background: #cdcfd2;
            }

            .table tr th.fullname {
                display: block;
                width: 165px;
                height: 60px;
                position: sticky;
                left: 0;
                background-color: #cdcfd2;
            }

            .table tr.context {
                height: 50px;
            }

        /*div.title {
            width: 100%;
            height: 150px;
            position: fixed;
            top: 0;
            left: 0;
            display: block;
            float: left;
        }*/

        /*div.content {
            display:none;
            position: absolute;
            top: 150px;
            left: 0;
        }*/

        a.Comback {
            position: absolute;
            top: 74px;
            right: 34px;
        }

        #box-modal {
            width: 836px;
            height: 254px;
            display: block;
            /* position: fixed; */
            /* top: 20%; */
            /* left: 28%; */
            /* box-shadow: 7px 6px 16px burlywood; */
            /* border-radius: 16px; */
            animation-name: modal;
            animation-duration: 2s;
            z-index: 1000;
            background-color: #fff;
            outline: none;
            border-radius: .3rem;
        }

            #box-modal .name {
                position: absolute;
                top: 10%;
                left: 10%;
                font-size: 30px;
                font-weight: bold;
                color: #1f58c1;
            }

            #box-modal .thongke {
                position: absolute;
                top: 35%;
                left: 2%;
                width: 95%;
                text-align: center;
            }

            #box-modal #btnTieptuc {
                position: absolute;
                right: 7%;
                bottom: 13%;
                display: inline-block;
                font-weight: 400;
                text-align: center;
                white-space: nowrap;
                vertical-align: middle;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
                border: 1px solid transparent;
                padding: .375rem .75rem;
                font-size: 1rem;
                line-height: 1.5;
                border-radius: .25rem;
                background-color: #545b62;
                border-color: #4e555b;
            }


        @keyframes modal {
            from {
                top: 0%;
                left: 0%;
            }

            to {
                top: 0%;
                left: 0%;
            }
        }

        .loading {
            display: none;
            width: 100%;
            height: 100%;
            z-index: 1;
            background-color: rgba(0,0,0,0.6);
            position: fixed;
             animation-name: modal;
            animation-duration: 2s;
        }

        .loading__img {
            position: fixed;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }

        .box {
            display: block;
            float: left;
            width: 205px;
            height: 103px;
            background: #c1e6a5;
            margin: 0 10px;
            text-align: center;
            font-size: 30px;
            border: 1px dotted gray;
            font-family: initial;
        }

        .box__left {
            margin-left: 190px;
        }

        .box a {
            width: 100%;
            height: auto;
            /* background: #ffdc20; */
            padding: 0 5px;
            /* border: 14.2rem; */
            text-decoration: none;
            font-weight: 600;
            font-size: 35px;
            cursor: default;
        }

        .loading__img span {
            font-size: 20px;
            color: black;
            margin-left: 5px;
        }
    </style>
    <script>
        function myNoiDungCuaGiaoVien(account_id) {
            document.getElementById("<%=txtUserName.ClientID%>").value = account_id;
            document.getElementById("<%=btnShow.ClientID%>").click();
        }
        //show modal
        function showDetail() {
            document.getElementById("img-loading-icon").style.display = "block";
            document.getElementById("btnTieptuc").addEventListener("click", function () {
                document.getElementById("img-loading-icon").style.display = "none";
            });
        }
    </script>
    <div class="loading" id="img-loading-icon">
        <div class="loading__img">
            <div id="box-modal">
                <p class="name">Thống kê đánh giá chuẩn nghề nghiệp giáo viên</p>
                <div class="thongke">
                    <div class="box box__left">
                        Xếp loại tốt
                        <a href="#"><%=XepLoaiTot %> <span>(tỉ lệ: <%=TiLeTot %>%)</span> </a>

                    </div>
                    <div class="box">
                        Xếp loại khá
                        <a href="#"><%=XepLoaiKha %><span>(tỉ lệ: <%=TiLeKha %>%)</span></a>
                    </div>
                </div>
                <a id="btnTieptuc" href="javascript:void(0)" class="btn btn-primary">Đóng</a>
            </div>
        </div>
    </div>
    <form id="form1" runat="server">
        <div>
            <div class="title">
                <h3 class="title">Đánh giá nội dung</h3>
                <div>
                    Bộ phận:
                <asp:DropDownList ID="ddlBoPhan" runat="server" CssClass=""></asp:DropDownList>
                    <asp:Button ID="btnXem" runat="server" Text="Xem" CssClass="btn btn-primary" OnClick="btnXem_Click" />
                </div>
                <a href="/admin-home" class="btn btn-primary Comback">Quay lại</a>
            </div>
            <div class="listdanhgia">
                <table class="table table-borderless" style="">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th colspan="2" scope="col">Tiêu Chuẩn 1</th>
                            <th colspan="5" scope="col">Tiêu Chuẩn 2</th>
                            <th colspan="3" scope="col">Tiêu Chuẩn 3</th>
                            <th colspan="3" scope="col">Tiêu Chuẩn 4</th>
                            <th colspan="2" scope="col">Tiêu Chuẩn 5</th>
                            <th scope="col">#</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr style="text-align: center">
                            <th scope="row">Giáo viên</th>
                            <td>TC1</td>
                            <td>TC2</td>
                            <td>TC3</td>
                            <td>TC4</td>
                            <td>TC5</td>
                            <td>TC6</td>
                            <td>TC7</td>
                            <td>TC8</td>
                            <td>TC9</td>
                            <td>TC10</td>
                            <td>TC11</td>
                            <td>TC12</td>
                            <td>TC13</td>
                            <td>TC14</td>
                            <td>TC15</td>
                            <th scope="row">Hành động</th>
                        </tr>
                        <asp:Repeater ID="rpListDanhGiaGiaoVien" runat="server">
                            <ItemTemplate>
                                <tr style="text-align: center">
                                    <th scope="row"><%#Eval("username_fullname") %></th>
                                    <td><%#Eval("noidungdanhgiagiaovien_mucdotieuchi1") %></td>
                                    <td><%#Eval("noidungdanhgiagiaovien_mucdotieuchi2") %></td>
                                    <td><%#Eval("noidungdanhgiagiaovien_mucdotieuchi3") %></td>
                                    <td><%#Eval("noidungdanhgiagiaovien_mucdotieuchi4") %></td>
                                    <td><%#Eval("noidungdanhgiagiaovien_mucdotieuchi5") %></td>
                                    <td><%#Eval("noidungdanhgiagiaovien_mucdotieuchi6") %></td>
                                    <td><%#Eval("noidungdanhgiagiaovien_mucdotieuchi7") %></td>
                                    <td><%#Eval("noidungdanhgiagiaovien_mucdotieuchi8") %></td>
                                    <td><%#Eval("noidungdanhgiagiaovien_mucdotieuchi9") %></td>
                                    <td><%#Eval("noidungdanhgiagiaovien_mucdotieuchi10") %></td>
                                    <td><%#Eval("noidungdanhgiagiaovien_mucdotieuchi11") %></td>
                                    <td><%#Eval("noidungdanhgiagiaovien_mucdotieuchi12") %></td>
                                    <td><%#Eval("noidungdanhgiagiaovien_mucdotieuchi13") %></td>
                                    <td><%#Eval("noidungdanhgiagiaovien_mucdotieuchi14") %></td>
                                    <td><%#Eval("noidungdanhgiagiaovien_mucdotieuchi15") %></td>
                                    <td>
                                        <a id="btnXem" style="color: white" onclick='myNoiDungCuaGiaoVien(<%#Eval("username_id") %>)' class="btn btn-primary"><%#Eval("active") %></a>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>
            <div class="content">
                <h3 class="title">KẾT QUẢ ĐÁNH GIÁ GIÁO VIÊN</h3>
                <div style="text-align: center">
                    <a class="btn btn-primary" href="javascript:void(0)" onclick="showDetail()">Thống kê</a>
                </div>
                <table class="table table-borderless table-hover" style="">
                    <thead>
                        <tr>
                            <th class="fullname" scope="col">#</th>
                            <th colspan="6" scope="col">Tiêu Chuẩn 1</th>
                            <th colspan="15" scope="col">Tiêu Chuẩn 2</th>
                            <th colspan="9" scope="col">Tiêu Chuẩn 3</th>
                            <th colspan="9" scope="col">Tiêu Chuẩn 4</th>
                            <th colspan="6" scope="col">Tiêu Chuẩn 5</th>
                            <th colspan="3" scope="col">Tổng kết</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="tieuchuan">
                            <th rowspan="2" style="position: sticky; left: 0; background-color: #cdcfd2">Giáo viên</th>
                            <th colspan="3">TC1</th>
                            <th colspan="3">TC2</th>
                            <th colspan="3">TC3</th>
                            <th colspan="3">TC4</th>
                            <th colspan="3">TC5</th>
                            <th colspan="3">TC6</th>
                            <th colspan="3">TC7</th>
                            <th colspan="3">TC8</th>
                            <th colspan="3">TC9</th>
                            <th colspan="3">TC10</th>
                            <th colspan="3">TC11</th>
                            <th colspan="3">TC12</th>
                            <th colspan="3">TC13</th>
                            <th colspan="3">TC14</th>
                            <th colspan="3">TC15</th>
                            <th colspan="3">Kết quả</th>
                        </tr>
                        <tr class="bophan">
                            <td>GV</td>
                            <td>Tổ</td>
                            <td>HT</td>
                            <td>GV</td>
                            <td>Tổ</td>
                            <td>HT</td>
                            <td>GV</td>
                            <td>Tổ</td>
                            <td>HT</td>
                            <td>GV</td>
                            <td>Tổ</td>
                            <td>HT</td>
                            <td>GV</td>
                            <td>Tổ</td>
                            <td>HT</td>
                            <td>GV</td>
                            <td>Tổ</td>
                            <td>HT</td>
                            <td>GV</td>
                            <td>Tổ</td>
                            <td>HT</td>
                            <td>GV</td>
                            <td>Tổ</td>
                            <td>HT</td>
                            <td>GV</td>
                            <td>Tổ</td>
                            <td>HT</td>
                            <td>GV</td>
                            <td>Tổ</td>
                            <td>HT</td>
                            <td>GV</td>
                            <td>Tổ</td>
                            <td>HT</td>
                            <td>GV</td>
                            <td>Tổ</td>
                            <td>HT</td>
                            <td>GV</td>
                            <td>Tổ</td>
                            <td>HT</td>
                            <td>GV</td>
                            <td>Tổ</td>
                            <td>HT</td>
                            <td>GV</td>
                            <td>Tổ</td>
                            <td>HT</td>
                            <td>GV</td>
                            <td>Tổ</td>
                            <td>HT</td>
                        </tr>
                        <asp:Repeater ID="rpList" runat="server">
                            <ItemTemplate>
                                <tr class="context">
                                    <th class="fullname" style="height: 54px; line-height: 34px"><%#Eval("username_fullname") %></th>
                                    <td><%#Eval("noidungdanhgiagiaovien_mucdotieuchi1") %></td>
                                    <td><%#Eval("mucdo1_truongbophan") %></td>
                                    <td><%#Eval("tongketdanhgia_mucdotieuchi1") %></td>
                                    <td><%#Eval("noidungdanhgiagiaovien_mucdotieuchi2") %></td>
                                    <td><%#Eval("mucdo2_truongbophan") %></td>
                                    <td><%#Eval("tongketdanhgia_mucdotieuchi2") %></td>
                                    <td><%#Eval("noidungdanhgiagiaovien_mucdotieuchi3") %></td>
                                    <td><%#Eval("mucdo3_truongbophan") %></td>
                                    <td><%#Eval("tongketdanhgia_mucdotieuchi3") %></td>
                                    <td><%#Eval("noidungdanhgiagiaovien_mucdotieuchi4") %></td>
                                    <td><%#Eval("mucdo4_truongbophan") %></td>
                                    <td><%#Eval("tongketdanhgia_mucdotieuchi4") %></td>
                                    <td><%#Eval("noidungdanhgiagiaovien_mucdotieuchi5") %></td>
                                    <td><%#Eval("mucdo5_truongbophan") %></td>
                                    <td><%#Eval("tongketdanhgia_mucdotieuchi5") %></td>
                                    <td><%#Eval("noidungdanhgiagiaovien_mucdotieuchi6") %></td>
                                    <td><%#Eval("mucdo6_truongbophan") %></td>
                                    <td><%#Eval("tongketdanhgia_mucdotieuchi6") %></td>
                                    <td><%#Eval("noidungdanhgiagiaovien_mucdotieuchi7") %></td>
                                    <td><%#Eval("mucdo7_truongbophan") %></td>
                                    <td><%#Eval("tongketdanhgia_mucdotieuchi7") %></td>
                                    <td><%#Eval("noidungdanhgiagiaovien_mucdotieuchi8") %></td>
                                    <td><%#Eval("mucdo8_truongbophan") %></td>
                                    <td><%#Eval("tongketdanhgia_mucdotieuchi8") %></td>
                                    <td><%#Eval("noidungdanhgiagiaovien_mucdotieuchi9") %></td>
                                    <td><%#Eval("mucdo9_truongbophan") %></td>
                                    <td><%#Eval("tongketdanhgia_mucdotieuchi9") %></td>
                                    <td><%#Eval("noidungdanhgiagiaovien_mucdotieuchi10") %></td>
                                    <td><%#Eval("mucdo10_truongbophan") %></td>
                                    <td><%#Eval("tongketdanhgia_mucdotieuchi10") %></td>
                                    <td><%#Eval("noidungdanhgiagiaovien_mucdotieuchi11") %></td>
                                    <td><%#Eval("mucdo11_truongbophan") %></td>
                                    <td><%#Eval("tongketdanhgia_mucdotieuchi11") %></td>
                                    <td><%#Eval("noidungdanhgiagiaovien_mucdotieuchi12") %></td>
                                    <td><%#Eval("mucdo12_truongbophan") %></td>
                                    <td><%#Eval("tongketdanhgia_mucdotieuchi12") %></td>
                                    <td><%#Eval("noidungdanhgiagiaovien_mucdotieuchi13") %></td>
                                    <td><%#Eval("mucdo13_truongbophan") %></td>
                                    <td><%#Eval("tongketdanhgia_mucdotieuchi13") %></td>
                                    <td><%#Eval("noidungdanhgiagiaovien_mucdotieuchi14") %></td>
                                    <td><%#Eval("mucdo14_truongbophan") %></td>
                                    <td><%#Eval("tongketdanhgia_mucdotieuchi14") %></td>
                                    <td><%#Eval("noidungdanhgiagiaovien_mucdotieuchi15") %></td>
                                    <td><%#Eval("mucdo15_truongbophan") %></td>
                                    <td><%#Eval("tongketdanhgia_mucdotieuchi15") %></td>
                                    <td><%#Eval("giaovien_ketquatongket") %></td>
                                    <td><%#Eval("noidungdanhgiagiaovien_ketquatongket") %> </td>
                                    <td><%#Eval("tongketdanhgia_xeploai") %> </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>
        </div>
        <div style="display: none">
            <input type="text" id="txtUserName" runat="server" />
            <a id="btnShow" runat="server" onserverclick="btnShow_ServerClick"></a>
        </div>

    </form>
</body>
</html>
