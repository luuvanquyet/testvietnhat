﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_DanhGiaNhanXetGiaoVienVaNhanVien_module_XemQuanLyNoiDung : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    public string xeploai, XepLoaiTot, XepLoaiKha, TiLeTot, TiLeKha;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            var listNV = from nv in db.tbBoPhans where nv.hidden == true orderby nv.bophan_position select nv;
            ddlBoPhan.Items.Clear();
            //ddlBoPhan.Items.Insert(0, "Chọn bộ phận");
            ddlBoPhan.AppendDataBoundItems = true;
            ddlBoPhan.DataTextField = "bophan_name";
            ddlBoPhan.DataValueField = "bophan_id";
            ddlBoPhan.DataSource = listNV;
            ddlBoPhan.DataBind();
            HttpCookie ck = new HttpCookie("Select");
            string s = ck.Value;
            ck.Value = ddlBoPhan.SelectedValue.ToString();
            ck.Expires = DateTime.Now.AddDays(-365);
            Response.Cookies.Add(ck);
            ddlBoPhan.SelectedValue = Request.Cookies["Select"].Value;
        }
        try
        {
            if (Request.Cookies["UserName"].Value == "")
            {
                alert.alert_Error(Page, "Lỗi tài khoản vui lòng login lại", "");
            }
            else
            {
                var getdata = from tk in db.tbTongKetDanhGiaTieuChuans
                              select tk;
                int countTot = 0, countKha = 0;
                int count = getdata.Count();
                foreach (var item in getdata)
                {
                    if (item.tongketdanhgia_xeploai == "Tốt")
                    {
                        countTot = countTot + 1;
                    }
                    else if (item.tongketdanhgia_xeploai == "Khá")
                    {
                        countKha = countKha + 1;
                    }
                }
                XepLoaiTot = countTot + "/" + getdata.Count();
                XepLoaiKha = countKha + "/" + getdata.Count();
                float percentTot = ((float)countTot / (float)count);
                float percentKha = ((float)countKha / (float)count);
                TiLeTot = percentTot * 100 + "";
                TiLeKha = percentKha * 100 + "";
                // hiện list đánh giá với select option được chọn từ trước khi ấn quay lại từ trang xem đánh giá từ giáo viên
                if (Request.Cookies["Select"] != null)
                {
                    //ddlBoPhan.SelectedValue = Request.Cookies["Select"].Value;
                    var list1 = from l in db.tbNoiDungDanhGiaGiaoVienVietNhats
                                join a in db.admin_Users on l.taikhoan_id equals a.username_id
                                join tk in db.tbTongKetDanhGiaTieuChuans on l.taikhoan_id equals tk.taikhoan_id
                                join bp in db.tbBoPhans on a.bophan_id equals bp.bophan_id
                                where bp.bophan_id == Convert.ToInt16(ddlBoPhan.SelectedValue.ToString())
                                select new
                                {
                                    a.username_id,
                                    a.username_fullname,
                                    l.noidungdanhgiagiaovien_mucdotieuchi1,
                                    l.noidungdanhgiagiaovien_mucdotieuchi2,
                                    l.noidungdanhgiagiaovien_mucdotieuchi3,
                                    l.noidungdanhgiagiaovien_mucdotieuchi4,
                                    l.noidungdanhgiagiaovien_mucdotieuchi5,
                                    l.noidungdanhgiagiaovien_mucdotieuchi6,
                                    l.noidungdanhgiagiaovien_mucdotieuchi7,
                                    l.noidungdanhgiagiaovien_mucdotieuchi8,
                                    l.noidungdanhgiagiaovien_mucdotieuchi9,
                                    l.noidungdanhgiagiaovien_mucdotieuchi10,
                                    l.noidungdanhgiagiaovien_mucdotieuchi11,
                                    l.noidungdanhgiagiaovien_mucdotieuchi12,
                                    l.noidungdanhgiagiaovien_mucdotieuchi13,
                                    l.noidungdanhgiagiaovien_mucdotieuchi14,
                                    l.noidungdanhgiagiaovien_mucdotieuchi15,
                                    l.noidungdanhgiagiaovien_ketquatongket,
                                    l.mucdo1_truongbophan,
                                    l.mucdo2_truongbophan,
                                    l.mucdo3_truongbophan,
                                    l.mucdo4_truongbophan,
                                    l.mucdo5_truongbophan,
                                    l.mucdo6_truongbophan,
                                    l.mucdo7_truongbophan,
                                    l.mucdo8_truongbophan,
                                    l.mucdo9_truongbophan,
                                    l.mucdo10_truongbophan,
                                    l.mucdo11_truongbophan,
                                    l.mucdo12_truongbophan,
                                    l.mucdo13_truongbophan,
                                    l.mucdo14_truongbophan,
                                    l.mucdo15_truongbophan,
                                    l.giaovien_ketquatongket,
                                    tk.tongketdanhgia_mucdotieuchi1,
                                    tk.tongketdanhgia_mucdotieuchi2,
                                    tk.tongketdanhgia_mucdotieuchi3,
                                    tk.tongketdanhgia_mucdotieuchi4,
                                    tk.tongketdanhgia_mucdotieuchi5,
                                    tk.tongketdanhgia_mucdotieuchi6,
                                    tk.tongketdanhgia_mucdotieuchi7,
                                    tk.tongketdanhgia_mucdotieuchi8,
                                    tk.tongketdanhgia_mucdotieuchi9,
                                    tk.tongketdanhgia_mucdotieuchi10,
                                    tk.tongketdanhgia_mucdotieuchi11,
                                    tk.tongketdanhgia_mucdotieuchi12,
                                    tk.tongketdanhgia_mucdotieuchi13,
                                    tk.tongketdanhgia_mucdotieuchi14,
                                    tk.tongketdanhgia_mucdotieuchi15,
                                    tk.tongketdanhgia_xeploai
                                };
                    rpList.DataSource = list1;
                    rpList.DataBind();
                    //foreach trong list1 để đếm tổng số gv xếp loại

                    var listTuGiaoVien1 = from l in db.tbNoiDungDanhGiaGiaoVienVietNhats
                                          join a in db.admin_Users on l.taikhoan_id equals a.username_id
                                          join bp in db.tbBoPhans on a.bophan_id equals bp.bophan_id
                                          where bp.bophan_id == Convert.ToInt16(ddlBoPhan.SelectedValue.ToString())
                                          select new
                                          {
                                              a.username_id,
                                              a.username_fullname,
                                              l.noidungdanhgiagiaovien_mucdotieuchi1,
                                              l.noidungdanhgiagiaovien_mucdotieuchi2,
                                              l.noidungdanhgiagiaovien_mucdotieuchi3,
                                              l.noidungdanhgiagiaovien_mucdotieuchi4,
                                              l.noidungdanhgiagiaovien_mucdotieuchi5,
                                              l.noidungdanhgiagiaovien_mucdotieuchi6,
                                              l.noidungdanhgiagiaovien_mucdotieuchi7,
                                              l.noidungdanhgiagiaovien_mucdotieuchi8,
                                              l.noidungdanhgiagiaovien_mucdotieuchi9,
                                              l.noidungdanhgiagiaovien_mucdotieuchi10,
                                              l.noidungdanhgiagiaovien_mucdotieuchi11,
                                              l.noidungdanhgiagiaovien_mucdotieuchi12,
                                              l.noidungdanhgiagiaovien_mucdotieuchi13,
                                              l.noidungdanhgiagiaovien_mucdotieuchi14,
                                              l.noidungdanhgiagiaovien_mucdotieuchi15,
                                              active = l.active == true ? "HT" : "Xem",
                                          };
                    rpListDanhGiaGiaoVien.DataSource = listTuGiaoVien1;
                    rpListDanhGiaGiaoVien.DataBind();
                }
                else
                {

                    var getData = from l in db.tbNoiDungDanhGiaGiaoVienVietNhats
                                  join a in db.admin_Users on l.taikhoan_id equals a.username_id
                                  join tk in db.tbTongKetDanhGiaTieuChuans on l.taikhoan_id equals tk.taikhoan_id
                                  where l.active == true
                                  select l;
                    foreach (var item in getData)
                    {
                        checkKetQua(Convert.ToInt32(item.taikhoan_id));
                    }
                    var list = from l in db.tbNoiDungDanhGiaGiaoVienVietNhats
                               join a in db.admin_Users on l.taikhoan_id equals a.username_id
                               join tk in db.tbTongKetDanhGiaTieuChuans on l.taikhoan_id equals tk.taikhoan_id
                               select new
                               {
                                   a.username_id,
                                   a.username_fullname,
                                   l.noidungdanhgiagiaovien_mucdotieuchi1,
                                   l.noidungdanhgiagiaovien_mucdotieuchi2,
                                   l.noidungdanhgiagiaovien_mucdotieuchi3,
                                   l.noidungdanhgiagiaovien_mucdotieuchi4,
                                   l.noidungdanhgiagiaovien_mucdotieuchi5,
                                   l.noidungdanhgiagiaovien_mucdotieuchi6,
                                   l.noidungdanhgiagiaovien_mucdotieuchi7,
                                   l.noidungdanhgiagiaovien_mucdotieuchi8,
                                   l.noidungdanhgiagiaovien_mucdotieuchi9,
                                   l.noidungdanhgiagiaovien_mucdotieuchi10,
                                   l.noidungdanhgiagiaovien_mucdotieuchi11,
                                   l.noidungdanhgiagiaovien_mucdotieuchi12,
                                   l.noidungdanhgiagiaovien_mucdotieuchi13,
                                   l.noidungdanhgiagiaovien_mucdotieuchi14,
                                   l.noidungdanhgiagiaovien_mucdotieuchi15,
                                   l.noidungdanhgiagiaovien_ketquatongket,
                                   active = l.active == true ? "HT" : "Xem",
                                   l.mucdo1_truongbophan,
                                   l.mucdo2_truongbophan,
                                   l.mucdo3_truongbophan,
                                   l.mucdo4_truongbophan,
                                   l.mucdo5_truongbophan,
                                   l.mucdo6_truongbophan,
                                   l.mucdo7_truongbophan,
                                   l.mucdo8_truongbophan,
                                   l.mucdo9_truongbophan,
                                   l.mucdo10_truongbophan,
                                   l.mucdo11_truongbophan,
                                   l.mucdo12_truongbophan,
                                   l.mucdo13_truongbophan,
                                   l.mucdo14_truongbophan,
                                   l.mucdo15_truongbophan,
                                   l.giaovien_ketquatongket,
                                   tk.tongketdanhgia_mucdotieuchi1,
                                   tk.tongketdanhgia_mucdotieuchi2,
                                   tk.tongketdanhgia_mucdotieuchi3,
                                   tk.tongketdanhgia_mucdotieuchi4,
                                   tk.tongketdanhgia_mucdotieuchi5,
                                   tk.tongketdanhgia_mucdotieuchi6,
                                   tk.tongketdanhgia_mucdotieuchi7,
                                   tk.tongketdanhgia_mucdotieuchi8,
                                   tk.tongketdanhgia_mucdotieuchi9,
                                   tk.tongketdanhgia_mucdotieuchi10,
                                   tk.tongketdanhgia_mucdotieuchi11,
                                   tk.tongketdanhgia_mucdotieuchi12,
                                   tk.tongketdanhgia_mucdotieuchi13,
                                   tk.tongketdanhgia_mucdotieuchi14,
                                   tk.tongketdanhgia_mucdotieuchi15,
                                   tk.tongketdanhgia_xeploai
                               };
                    rpList.DataSource = list;
                    rpList.DataBind();
                    var listTuGiaoVien = from l in db.tbNoiDungDanhGiaGiaoVienVietNhats
                                         join a in db.admin_Users on l.taikhoan_id equals a.username_id
                                         select new
                                         {
                                             a.username_id,
                                             a.username_fullname,
                                             l.noidungdanhgiagiaovien_mucdotieuchi1,
                                             l.noidungdanhgiagiaovien_mucdotieuchi2,
                                             l.noidungdanhgiagiaovien_mucdotieuchi3,
                                             l.noidungdanhgiagiaovien_mucdotieuchi4,
                                             l.noidungdanhgiagiaovien_mucdotieuchi5,
                                             l.noidungdanhgiagiaovien_mucdotieuchi6,
                                             l.noidungdanhgiagiaovien_mucdotieuchi7,
                                             l.noidungdanhgiagiaovien_mucdotieuchi8,
                                             l.noidungdanhgiagiaovien_mucdotieuchi9,
                                             l.noidungdanhgiagiaovien_mucdotieuchi10,
                                             l.noidungdanhgiagiaovien_mucdotieuchi11,
                                             l.noidungdanhgiagiaovien_mucdotieuchi12,
                                             l.noidungdanhgiagiaovien_mucdotieuchi13,
                                             l.noidungdanhgiagiaovien_mucdotieuchi14,
                                             l.noidungdanhgiagiaovien_mucdotieuchi15,
                                             active = l.active == true ? "HT" : "Xem",
                                         };
                    rpListDanhGiaGiaoVien.DataSource = listTuGiaoVien;
                    rpListDanhGiaGiaoVien.DataBind();
                }
            }
        }
        catch (Exception ex)
        {
            alert.alert_Error(Page, "Đã xảy ra lỗi, vui lòng liên hệ IT", "");
        }
    }
    protected void btnShow_ServerClick(object sender, EventArgs e)
    {
        if (Request.Cookies["UserName"].Value == "admin")
        {
            Response.Redirect("admin-xem-bai-viet-tu-giao-vien-" + txtUserName.Value);
        }
        Response.Redirect("admin-noi-dung-bai-viet-tieu-chuan-1");
    }


    protected void btnXem_Click(object sender, EventArgs e)
    {
        try
        {
            if (Request.Cookies["UserName"].Value == "")
            {
                alert.alert_Error(Page, "Lỗi tài khoản vui lòng login lại", "");
            }
            else
            {
                if (ddlBoPhan.SelectedValue.ToString() == "Chọn bộ phận")
                {
                    alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu để xem!", "");
                }
                else
                {
                    if (Request.Cookies["Select"] == null)
                    {
                        HttpCookie ck = new HttpCookie("Select");
                        //string s = ck.Value;
                        //ck.Value = ddlBoPhan.SelectedValue.ToString();
                        //ck.Expires = DateTime.Now.AddDays(365);
                        //Response.Cookies.Add(ck);
                        var list = from l in db.tbNoiDungDanhGiaGiaoVienVietNhats
                                   join a in db.admin_Users on l.taikhoan_id equals a.username_id
                                   join tk in db.tbTongKetDanhGiaTieuChuans on l.taikhoan_id equals tk.taikhoan_id
                                   join bp in db.tbBoPhans on a.bophan_id equals bp.bophan_id
                                   where bp.bophan_id == Convert.ToInt16(ddlBoPhan.SelectedValue.ToString())
                                   select new
                                   {
                                       a.username_id,
                                       a.username_fullname,
                                       l.noidungdanhgiagiaovien_mucdotieuchi1,
                                       l.noidungdanhgiagiaovien_mucdotieuchi2,
                                       l.noidungdanhgiagiaovien_mucdotieuchi3,
                                       l.noidungdanhgiagiaovien_mucdotieuchi4,
                                       l.noidungdanhgiagiaovien_mucdotieuchi5,
                                       l.noidungdanhgiagiaovien_mucdotieuchi6,
                                       l.noidungdanhgiagiaovien_mucdotieuchi7,
                                       l.noidungdanhgiagiaovien_mucdotieuchi8,
                                       l.noidungdanhgiagiaovien_mucdotieuchi9,
                                       l.noidungdanhgiagiaovien_mucdotieuchi10,
                                       l.noidungdanhgiagiaovien_mucdotieuchi11,
                                       l.noidungdanhgiagiaovien_mucdotieuchi12,
                                       l.noidungdanhgiagiaovien_mucdotieuchi13,
                                       l.noidungdanhgiagiaovien_mucdotieuchi14,
                                       l.noidungdanhgiagiaovien_mucdotieuchi15,
                                       l.noidungdanhgiagiaovien_ketquatongket,
                                       l.mucdo1_truongbophan,
                                       l.mucdo2_truongbophan,
                                       l.mucdo3_truongbophan,
                                       l.mucdo4_truongbophan,
                                       l.mucdo5_truongbophan,
                                       l.mucdo6_truongbophan,
                                       l.mucdo7_truongbophan,
                                       l.mucdo8_truongbophan,
                                       l.mucdo9_truongbophan,
                                       l.mucdo10_truongbophan,
                                       l.mucdo11_truongbophan,
                                       l.mucdo12_truongbophan,
                                       l.mucdo13_truongbophan,
                                       l.mucdo14_truongbophan,
                                       l.mucdo15_truongbophan,
                                       l.giaovien_ketquatongket,
                                       tk.tongketdanhgia_mucdotieuchi1,
                                       tk.tongketdanhgia_mucdotieuchi2,
                                       tk.tongketdanhgia_mucdotieuchi3,
                                       tk.tongketdanhgia_mucdotieuchi4,
                                       tk.tongketdanhgia_mucdotieuchi5,
                                       tk.tongketdanhgia_mucdotieuchi6,
                                       tk.tongketdanhgia_mucdotieuchi7,
                                       tk.tongketdanhgia_mucdotieuchi8,
                                       tk.tongketdanhgia_mucdotieuchi9,
                                       tk.tongketdanhgia_mucdotieuchi10,
                                       tk.tongketdanhgia_mucdotieuchi11,
                                       tk.tongketdanhgia_mucdotieuchi12,
                                       tk.tongketdanhgia_mucdotieuchi13,
                                       tk.tongketdanhgia_mucdotieuchi14,
                                       tk.tongketdanhgia_mucdotieuchi15,
                                       tk.tongketdanhgia_xeploai
                                   };
                        rpList.DataSource = list;
                        rpList.DataBind();
                        var listTuGiaoVien = from l in db.tbNoiDungDanhGiaGiaoVienVietNhats
                                             join a in db.admin_Users on l.taikhoan_id equals a.username_id
                                             join bp in db.tbBoPhans on a.bophan_id equals bp.bophan_id
                                             where bp.bophan_id == Convert.ToInt16(ddlBoPhan.SelectedValue.ToString())
                                             select new
                                             {
                                                 a.username_id,
                                                 a.username_fullname,
                                                 l.noidungdanhgiagiaovien_mucdotieuchi1,
                                                 l.noidungdanhgiagiaovien_mucdotieuchi2,
                                                 l.noidungdanhgiagiaovien_mucdotieuchi3,
                                                 l.noidungdanhgiagiaovien_mucdotieuchi4,
                                                 l.noidungdanhgiagiaovien_mucdotieuchi5,
                                                 l.noidungdanhgiagiaovien_mucdotieuchi6,
                                                 l.noidungdanhgiagiaovien_mucdotieuchi7,
                                                 l.noidungdanhgiagiaovien_mucdotieuchi8,
                                                 l.noidungdanhgiagiaovien_mucdotieuchi9,
                                                 l.noidungdanhgiagiaovien_mucdotieuchi10,
                                                 l.noidungdanhgiagiaovien_mucdotieuchi11,
                                                 l.noidungdanhgiagiaovien_mucdotieuchi12,
                                                 l.noidungdanhgiagiaovien_mucdotieuchi13,
                                                 l.noidungdanhgiagiaovien_mucdotieuchi14,
                                                 l.noidungdanhgiagiaovien_mucdotieuchi15,
                                                 active = l.active == true ? "HT" : "Xem",
                                             };
                        rpListDanhGiaGiaoVien.DataSource = listTuGiaoVien;
                        rpListDanhGiaGiaoVien.DataBind();
                        string s1 = ck.Value;
                        ck.Value = ddlBoPhan.SelectedValue.ToString();
                        ck.Expires = DateTime.Now.AddDays(1);
                        Response.Cookies.Add(ck);
                    }
                    else
                    {
                        HttpCookie ck = new HttpCookie("Select");
                        string s = ck.Value;
                        ck.Value = ddlBoPhan.SelectedValue.ToString();
                        ck.Expires = DateTime.Now.AddDays(-365);
                        Response.Cookies.Add(ck);
                        var list = from l in db.tbNoiDungDanhGiaGiaoVienVietNhats
                                   join a in db.admin_Users on l.taikhoan_id equals a.username_id
                                   join tk in db.tbTongKetDanhGiaTieuChuans on l.taikhoan_id equals tk.taikhoan_id
                                   join bp in db.tbBoPhans on a.bophan_id equals bp.bophan_id
                                   where bp.bophan_id == Convert.ToInt16(ddlBoPhan.SelectedValue.ToString())
                                   select new
                                   {
                                       a.username_id,
                                       a.username_fullname,
                                       l.noidungdanhgiagiaovien_mucdotieuchi1,
                                       l.noidungdanhgiagiaovien_mucdotieuchi2,
                                       l.noidungdanhgiagiaovien_mucdotieuchi3,
                                       l.noidungdanhgiagiaovien_mucdotieuchi4,
                                       l.noidungdanhgiagiaovien_mucdotieuchi5,
                                       l.noidungdanhgiagiaovien_mucdotieuchi6,
                                       l.noidungdanhgiagiaovien_mucdotieuchi7,
                                       l.noidungdanhgiagiaovien_mucdotieuchi8,
                                       l.noidungdanhgiagiaovien_mucdotieuchi9,
                                       l.noidungdanhgiagiaovien_mucdotieuchi10,
                                       l.noidungdanhgiagiaovien_mucdotieuchi11,
                                       l.noidungdanhgiagiaovien_mucdotieuchi12,
                                       l.noidungdanhgiagiaovien_mucdotieuchi13,
                                       l.noidungdanhgiagiaovien_mucdotieuchi14,
                                       l.noidungdanhgiagiaovien_mucdotieuchi15,
                                       l.noidungdanhgiagiaovien_ketquatongket,
                                       l.mucdo1_truongbophan,
                                       l.mucdo2_truongbophan,
                                       l.mucdo3_truongbophan,
                                       l.mucdo4_truongbophan,
                                       l.mucdo5_truongbophan,
                                       l.mucdo6_truongbophan,
                                       l.mucdo7_truongbophan,
                                       l.mucdo8_truongbophan,
                                       l.mucdo9_truongbophan,
                                       l.mucdo10_truongbophan,
                                       l.mucdo11_truongbophan,
                                       l.mucdo12_truongbophan,
                                       l.mucdo13_truongbophan,
                                       l.mucdo14_truongbophan,
                                       l.mucdo15_truongbophan,
                                       l.giaovien_ketquatongket,
                                       tk.tongketdanhgia_mucdotieuchi1,
                                       tk.tongketdanhgia_mucdotieuchi2,
                                       tk.tongketdanhgia_mucdotieuchi3,
                                       tk.tongketdanhgia_mucdotieuchi4,
                                       tk.tongketdanhgia_mucdotieuchi5,
                                       tk.tongketdanhgia_mucdotieuchi6,
                                       tk.tongketdanhgia_mucdotieuchi7,
                                       tk.tongketdanhgia_mucdotieuchi8,
                                       tk.tongketdanhgia_mucdotieuchi9,
                                       tk.tongketdanhgia_mucdotieuchi10,
                                       tk.tongketdanhgia_mucdotieuchi11,
                                       tk.tongketdanhgia_mucdotieuchi12,
                                       tk.tongketdanhgia_mucdotieuchi13,
                                       tk.tongketdanhgia_mucdotieuchi14,
                                       tk.tongketdanhgia_mucdotieuchi15,
                                       tk.tongketdanhgia_xeploai
                                   };
                        rpList.DataSource = list;
                        rpList.DataBind();
                        var listTuGiaoVien = from l in db.tbNoiDungDanhGiaGiaoVienVietNhats
                                             join a in db.admin_Users on l.taikhoan_id equals a.username_id
                                             join bp in db.tbBoPhans on a.bophan_id equals bp.bophan_id
                                             where bp.bophan_id == Convert.ToInt16(ddlBoPhan.SelectedValue.ToString())
                                             select new
                                             {
                                                 a.username_id,
                                                 a.username_fullname,
                                                 l.noidungdanhgiagiaovien_mucdotieuchi1,
                                                 l.noidungdanhgiagiaovien_mucdotieuchi2,
                                                 l.noidungdanhgiagiaovien_mucdotieuchi3,
                                                 l.noidungdanhgiagiaovien_mucdotieuchi4,
                                                 l.noidungdanhgiagiaovien_mucdotieuchi5,
                                                 l.noidungdanhgiagiaovien_mucdotieuchi6,
                                                 l.noidungdanhgiagiaovien_mucdotieuchi7,
                                                 l.noidungdanhgiagiaovien_mucdotieuchi8,
                                                 l.noidungdanhgiagiaovien_mucdotieuchi9,
                                                 l.noidungdanhgiagiaovien_mucdotieuchi10,
                                                 l.noidungdanhgiagiaovien_mucdotieuchi11,
                                                 l.noidungdanhgiagiaovien_mucdotieuchi12,
                                                 l.noidungdanhgiagiaovien_mucdotieuchi13,
                                                 l.noidungdanhgiagiaovien_mucdotieuchi14,
                                                 l.noidungdanhgiagiaovien_mucdotieuchi15,
                                                 active = l.active == true ? "HT" : "Xem",
                                             };
                        rpListDanhGiaGiaoVien.DataSource = listTuGiaoVien;
                        rpListDanhGiaGiaoVien.DataBind();
                        string s1 = ck.Value;
                        ck.Value = ddlBoPhan.SelectedValue.ToString();
                        ck.Expires = DateTime.Now.AddDays(1);
                        Response.Cookies.Add(ck);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            alert.alert_Error(Page, "Đã xảy ra lỗi, vui lòng liên hệ IT", "");
        }
    }
    protected void checkKetQua(int account_id)
    {
        // check tài khoản đã có đánh giá lần đâu chưa

        int countTot = 0;
        var checkDataAccount = (from nd in db.tbNoiDungDanhGiaGiaoVienVietNhats where nd.taikhoan_id == account_id orderby nd.noidungdanhgiagiaovien_id descending select nd).FirstOrDefault();
        // Mức tốt : tiêu chuẩn 2 phải tốt, trên 2/3 các tiêu chí phải tốt, ko có bất cừ trường hợp nào chỉ đạt
        if (checkDataAccount != null)
        {
            if (checkDataAccount.noidungdanhgiagiaovien_mucdotieuchi3 == "Tốt"
                && checkDataAccount.noidungdanhgiagiaovien_mucdotieuchi4 == "Tốt"
                && checkDataAccount.noidungdanhgiagiaovien_mucdotieuchi5 == "Tốt"
                && checkDataAccount.noidungdanhgiagiaovien_mucdotieuchi6 == "Tốt"
                && checkDataAccount.noidungdanhgiagiaovien_mucdotieuchi7 == "Tốt")
            {
                countTot = 5;
                if (checkDataAccount.noidungdanhgiagiaovien_mucdotieuchi1 == "Tốt")
                    countTot = countTot + 1;
                if (checkDataAccount.noidungdanhgiagiaovien_mucdotieuchi2 == "Tốt")
                    countTot = countTot + 1;
                if (checkDataAccount.noidungdanhgiagiaovien_mucdotieuchi8 == "Tốt")
                    countTot = countTot + 1;
                if (checkDataAccount.noidungdanhgiagiaovien_mucdotieuchi9 == "Tốt")
                    countTot = countTot + 1;
                if (checkDataAccount.noidungdanhgiagiaovien_mucdotieuchi10 == "Tốt")
                    countTot = countTot + 1;
                if (checkDataAccount.noidungdanhgiagiaovien_mucdotieuchi11 == "Tốt")
                    countTot = countTot + 1;
                if (checkDataAccount.noidungdanhgiagiaovien_mucdotieuchi12 == "Tốt")
                    countTot = countTot + 1;
                if (checkDataAccount.noidungdanhgiagiaovien_mucdotieuchi13 == "Tốt")
                    countTot = countTot + 1;
                if (checkDataAccount.noidungdanhgiagiaovien_mucdotieuchi14 == "Tốt")
                    countTot = countTot + 1;
                if (checkDataAccount.noidungdanhgiagiaovien_mucdotieuchi15 == "Tốt")
                    countTot = countTot + 1;
                if (countTot >= 10)
                {
                    if (checkDataAccount.noidungdanhgiagiaovien_mucdotieuchi1 == "Đạt" ||
                        checkDataAccount.noidungdanhgiagiaovien_mucdotieuchi2 == "Đạt" ||
                        checkDataAccount.noidungdanhgiagiaovien_mucdotieuchi8 == "Đạt" ||
                        checkDataAccount.noidungdanhgiagiaovien_mucdotieuchi9 == "Đạt" ||
                        checkDataAccount.noidungdanhgiagiaovien_mucdotieuchi10 == "Đạt" ||
                        checkDataAccount.noidungdanhgiagiaovien_mucdotieuchi11 == "Đạt" ||
                        checkDataAccount.noidungdanhgiagiaovien_mucdotieuchi12 == "Đạt" ||
                        checkDataAccount.noidungdanhgiagiaovien_mucdotieuchi13 == "Đạt" ||
                        checkDataAccount.noidungdanhgiagiaovien_mucdotieuchi14 == "Đạt" ||
                        checkDataAccount.noidungdanhgiagiaovien_mucdotieuchi15 == "Đạt"
                        )
                    {
                        checkDataAccount.giaovien_ketquatongket = "Khá";
                    }
                    else
                    {
                        checkDataAccount.giaovien_ketquatongket = "Tốt";
                    }
                }
                else
                {
                    checkDataAccount.giaovien_ketquatongket = "Khá";
                }
            }
            else
            {
                checkDataAccount.giaovien_ketquatongket = "Khá";
                if (checkDataAccount.noidungdanhgiagiaovien_mucdotieuchi3 == "Khá"
                && checkDataAccount.noidungdanhgiagiaovien_mucdotieuchi4 == "Khá"
                && checkDataAccount.noidungdanhgiagiaovien_mucdotieuchi5 == "Khá"
                && checkDataAccount.noidungdanhgiagiaovien_mucdotieuchi6 == "Khá"
                && checkDataAccount.noidungdanhgiagiaovien_mucdotieuchi7 == "Khá")
                {
                    countTot = 5;
                    if (checkDataAccount.noidungdanhgiagiaovien_mucdotieuchi1 == "Khá")
                        countTot = countTot + 1;
                    if (checkDataAccount.noidungdanhgiagiaovien_mucdotieuchi2 == "Khá")
                        countTot = countTot + 1;
                    if (checkDataAccount.noidungdanhgiagiaovien_mucdotieuchi8 == "Khá")
                        countTot = countTot + 1;
                    if (checkDataAccount.noidungdanhgiagiaovien_mucdotieuchi9 == "Khá")
                        countTot = countTot + 1;
                    if (checkDataAccount.noidungdanhgiagiaovien_mucdotieuchi10 == "Khá")
                        countTot = countTot + 1;
                    if (checkDataAccount.noidungdanhgiagiaovien_mucdotieuchi11 == "Khá")
                        countTot = countTot + 1;
                    if (checkDataAccount.noidungdanhgiagiaovien_mucdotieuchi12 == "Khá")
                        countTot = countTot + 1;
                    if (checkDataAccount.noidungdanhgiagiaovien_mucdotieuchi13 == "Khá")
                        countTot = countTot + 1;
                    if (checkDataAccount.noidungdanhgiagiaovien_mucdotieuchi14 == "Khá")
                        countTot = countTot + 1;
                    if (checkDataAccount.noidungdanhgiagiaovien_mucdotieuchi15 == "Khá")
                        countTot = countTot + 1;
                    if (countTot >= 10)
                    {
                        checkDataAccount.giaovien_ketquatongket = "Khá";
                    }
                    else
                    {
                        checkDataAccount.giaovien_ketquatongket = "Đạt";
                    }
                }
            }
            db.SubmitChanges();
        }
    }

}