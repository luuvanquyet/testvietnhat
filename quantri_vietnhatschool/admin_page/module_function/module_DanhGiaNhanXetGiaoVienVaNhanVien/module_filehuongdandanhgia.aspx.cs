﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_filehuongdandanhgia : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {

        rpFileHuongDan.DataSource = from lct in db.tbThongBaos where lct.thongbao_id== Convert.ToInt32(RouteData.Values["id"]) select lct;
        rpFileHuongDan.DataBind();
       
    }
    protected void btnQuayLai_Click(object sender, EventArgs e)
    {
        Response.Redirect("/admin-home");
    }

}