﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_Lop : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {
        // Kiểm trả session login nếu khác null thì vào form xử lý
        if (Session["AdminLogined"] != null)
        {

            admin_User logedMember = Session["AdminLogined"] as admin_User;
            //if (logedMember.groupuser_id == 3)
            //    Response.Redirect("/user-home");
            if (!IsPostBack)
            {
                Session["_id"] = 0;
            }
            loadData();
        }
        // nếu session = null thì trả về trang login
        else
        {
            Response.Redirect("/admin-login");
        }
    }
    private void loadData()
    {
        //// load data đổ vào var danh sách
        //var getData = from nc in db.tbLops
        //              //join gv in db.admin_Users on nc.username_id equals gv.username_id
        //              //join gv in db.tbGiaoVienTrongLops on nc.lop_id equals gv.lop_id
        //              //join user in db.admin_Users on gv.username_id equals user.username_id
        //              //join gr in db.admin_GroupUsers on user.groupuser_id equals gr.groupuser_id
        //              where nc.hidden == false
        //              orderby nc.lop_id descending
        //              select new
        //              {
        //                  nc.lop_id,
        //                  nc.lop_name,
        //                  username_idEn = (from n in db.tbLops
        //                                   join gv in db.tbGiaoVienTrongLops on nc.lop_id equals gv.lop_id
        //                                   where n.lop_id == nc.lop_id && n.username_idEn == gv.username_id
        //                                   select gv.username_fullname).FirstOrDefault(),
        //                  username_idVn = (from n in db.tbLops
        //                                   join gv in db.tbGiaoVienTrongLops on nc.lop_id equals gv.lop_id
        //                                   where n.lop_id == nc.lop_id && n.username_idVn == gv.username_id
        //                                   select gv.username_fullname).FirstOrDefault(),
        //                  siso = (from ss in db.tbhocsinhtronglops where ss.lop_id == nc.lop_id select ss).Count()
        //              };
        //// đẩy dữ liệu vào gridivew
        //grvList.DataSource = getData;
        //grvList.DataBind();
    }
    private void setNULL()
    {
        txttensanpham.Text = "";
    }
    protected void btnThem_Click(object sender, EventArgs e)
    {
        // Khi nhấn nút thêm thì mật định session id = 0 để thêm mới
        Session["_id"] = 0;
        // gọi hàm setNull để trả toàn bộ các control về rỗng
        setNULL();
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupControl.Show();", true);
    }
    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        // get value từ việc click vào gridview
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "lop_id" }));
        // đẩy id vào session
        Session["_id"] = _id;
        var getData = (from nc in db.tbLops
                       where nc.lop_id == _id
                       select nc).Single();
        txttensanpham.Text = getData.lop_name;
        //txtGvEn.Text = (from n in db.tbGiaoVienTrongLops
        //                where n.lop_id == getData.lop_id && n.username_id == getData.username_idEn
        //                select n.username_fullname).FirstOrDefault();
        //txtGvVn.Text= (from n in db.tbGiaoVienTrongLops
        //               where n.lop_id == getData.lop_id && n.username_id == getData.username_idVn
        //               select n.username_fullname).FirstOrDefault();
        //txtSiso.Text = (from ss in db.tbhocsinhtronglops where ss.lop_id == getData.lop_id select ss).Count().ToString();
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupControl.Show();", true);
    }
    protected void btnXoa_Click(object sender, EventArgs e)
    {

        cls_Lop cls;
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "lop_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                cls = new cls_Lop();
                if (cls.Linq_Xoa(Convert.ToInt32(item)))
                    alert.alert_Success(Page, "Xóa thành công", "");
                else
                    alert.alert_Error(Page, "Xóa thất bại", "");
            }
        }
        else
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
    }

    public bool checknull()
    {
        if (txttensanpham.Text != "")
            return true;
        else return false;
    }


    protected void btnLuu_Click(object sender, EventArgs e)
    {
        cls_Lop cls = new cls_Lop();
        if (checknull() == false)
            alert.alert_Warning(Page, "Nhập đầy đủ thông tin!", "");
        else
        {
            if (Session["_id"].ToString() == "0")
            {
                if (cls.Linq_Them(txttensanpham.Text))
                {
                    ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Thêm thành công','','success').then(function(){grvList.Refresh();})", true);
                    loadData();
                }
                else alert.alert_Error(Page, "Thêm thất bại", "");
            }
            else
            {
                if (cls.Linq_Sua(Convert.ToInt32(Session["_id"].ToString()), txttensanpham.Text))
                {
                    alert.alert_Success(Page, "Cập nhật thành công", "");
                    loadData();
                }
                else alert.alert_Error(Page, "Cập nhật thất bại", "");
            }
        }
        popupControl.ShowOnPageLoad = false;
    }
    protected void btnDuyet_Click(object sender, EventArgs e)
    {
        //List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "account_id" });
        //if (selectedKey.Count > 0)
        //{
        //    foreach (var item in selectedKey)
        //    {
        //        tbAccount check = (from c in db.tbAccounts where c.account_id == Convert.ToInt32(item) select c).SingleOrDefault();
        //        if (check != null)
        //        {
        //            check.account_acctive = true;
        //            db.SubmitChanges();
        //            var dsBaiTap = from ds in db.tbBaiTaps select ds;
        //            foreach (var item1 in dsBaiTap)
        //            {
        //                tbBaiTapTaiKhoan insert = new tbBaiTapTaiKhoan();
        //                insert.account_id = Convert.ToInt32(item);
        //                insert.baitap_id = Convert.ToInt32(item1.baitap_id);
        //                insert.hidden = false;
        //                insert.link_seo = item1.link_seo;
        //                insert.baitap_name = item1.baitap_name;
        //                insert.baitap_image = item1.baitap_image;
        //                db.tbBaiTapTaiKhoans.InsertOnSubmit(insert);
        //                db.SubmitChanges();
        //            }
        //            alert.alert_Success(Page, "Đã duyệt lớp", "");

        //        }

        //        else
        //            alert.alert_Error(Page, "Thất bại", "");
        //    }
        //}
        //else
        //    alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
    }
}