﻿<%@ Page Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_NhapDiemHocSinh.aspx.cs" Inherits="admin_page_module_function_module_Diem_module_NhapDiemHocSinh" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v17.1" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <style>
        /*.diemmieng, .diem15phut {
            width: 240px;
        }

        .diem1iet {
            width: 165px;
        }

        .diemcuoiki {
            width: 165px;
        }*/
        .nhapdiem {
            margin: 0 50px;
        }

        .row .score input {
            width: 50px;
            text-align: center;
            text-align: center;
            border: none;
            border-bottom: 1px solid #e4e0e0;
            margin: 0 5px 0 0;
        }
    </style>
    <div class="card card-block">
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <div class="container">
                    <div class="row">
                        <div class="form-group col-6">
                            <label class="col-2 form-control-label">Chọn lớp:</label>
                            <div class="col-10">
                                <dx:ASPxComboBox ID="ddllop" runat="server" OnSelectedIndexChanged="ddllop_SelectedIndexChanged" AutoPostBack="true" ValueType="System.Int32" TextField="lop_name" ValueField="lop_id" ClientInstanceName="ddllop" CssClass="" Width="95%"></dx:ASPxComboBox>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div>
                        <asp:Repeater ID="rpChitiet" runat="server">
                            <ItemTemplate>
                                <div id="modaldemo<%#Eval("diem_id") %>" class="modal">
                                    <div class="modal-dialog " role="document">
                                        <div class="modal-content modal-content-demo">
                                            <div class="modal-header">
                                                <h6 class="modal-title">Nhập điểm chi tiết</h6>
                                                <button type="button" onclick="Clear()" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="nhapdiem">
                                                    <p>
                                                        Họ tên:
                                                <label><%#Eval("hocsinh_name") %></label>
                                                    </p>
                                                    <div class="row">
                                                        <div class="col-3">
                                                            <label>Điểm miệng:</label>
                                                        </div>
                                                        <div class="col-9 score">
                                                            <input id="txtDiemMiengLan1" value="<%#Eval("diem_diemmienglan1") %>" />
                                                            <input id="txtDiemMiengLan2" value="<%#Eval("diem_diemmienglan2") %>" />
                                                            <input id="txtDiemMiengLan3" value="<%#Eval("diem_diemmienglan3") %>" />
                                                            <input id="txtDiemMiengLan4" value="<%#Eval("diem_diemmienglan4") %>" />
                                                            <input id="txtDiemMiengLan5" value="<%#Eval("diem_diemmienglan5") %>" />
                                                            <input id="txtDiemMiengLan6" value="<%#Eval("diem_diemmienglan6") %>" />
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-3">
                                                            <label>Điểm 15 phút:</label>
                                                        </div>
                                                        <div class="col-9 score">
                                                            <input id="txtDiem15PLan1" value="<%#Eval("diem_diem15phutlan1") %>" />
                                                            <input id="txtDiem15PLan2" value="<%#Eval("diem_diem15phutlan2") %>" />
                                                            <input id="txtDiem15PLan3" value="<%#Eval("diem_diem15phutlan3") %>" />
                                                            <input id="txtDiem15PLan4" value="<%#Eval("diem_diem15phutlan4") %>" />
                                                            <input id="txtDiem15PLan5" value="<%#Eval("diem_diem15phutlan5") %>" />
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-3">
                                                            <label>Điểm 1 tiết:</label>
                                                        </div>
                                                        <div class="col-9 score">
                                                            <input id="txtDiem1TLan1" value="<%#Eval("diem_diem1tietlan1") %>" />
                                                            <input id="txtDiem1TLan2" value="<%#Eval("diem_diem1tietlan2") %>" />
                                                            <input id="txtDiem1TLan3" value="<%#Eval("diem_diem1tietlan3") %>" />
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-3">
                                                            <label>Điểm cuối kỳ:</label>
                                                        </div>
                                                        <div class="col-9 score">
                                                            <input id="txtDiemCuoiKy" value="<%#Eval("diem_diemcuoiky") %>" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <a id="btnCapNhat1" href="javascript:void(0);" onclick="capnhat();" class="btn btn-primary btn-block">Cập nhật</a>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Họ tên</th>
                                <th scope="col">Điểm miệng</th>
                                <th scope="col">Điểm 15 phút</th>
                                <th scope="col">Điểm 1 tiết</th>
                                <th scope="col">Điểm cuối kì</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Repeater ID="rpHocSinh" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <th scope="row" style="width: 50px"><%=STT++ %></th>
                                        <td><%#Eval("hocsinh_name") %>
                                        </td>
                                        <td class="diemmieng">
                                            <label><%#Eval("diem_diemmienglan1") %></label>
                                            <label><%#Eval("diem_diemmienglan2") %></label>
                                            <label><%#Eval("diem_diemmienglan3") %></label>
                                            <label><%#Eval("diem_diemmienglan4") %></label>
                                            <label><%#Eval("diem_diemmienglan5") %></label>
                                            <label><%#Eval("diem_diemmienglan6") %></label>
                                        </td>
                                        <td class="diem15phut">
                                            <label><%#Eval("diem_diem15phutlan1") %></label>
                                            <label><%#Eval("diem_diem15phutlan2") %></label>
                                            <label><%#Eval("diem_diem15phutlan3") %></label>
                                            <label><%#Eval("diem_diem15phutlan4") %></label>
                                            <label><%#Eval("diem_diem15phutlan5") %></label>
                                        </td>
                                        <td class="diem1iet">
                                            <label><%#Eval("diem_diem1tietlan1") %></label>
                                            <label><%#Eval("diem_diem1tietlan2") %></label>
                                            <label><%#Eval("diem_diem1tietlan3") %></label>
                                        </td>
                                        <td class="diemcuoiki">
                                            <label><%#Eval("diem_diemcuoiky") %></label>
                                        </td>
                                        <td>
                                            <a href="javascript:void(0);" data-toggle="modal" data-target="#modaldemo<%#Eval("diem_id") %>" onclick="getIDDiem(<%#Eval("diem_id") %>);">Edit</a>
                                        </td>
                                    </tr>

                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div >
         <input id="txtIddiem" runat="server" type="text" placeholder="iddiem" />
        <input runat="server" id="txtDiemMiengLan1" type="text" />
        <input runat="server" id="txtDiemMiengLan2" type="text" />
        <input runat="server" id="txtDiemMiengLan3" type="text" />
        <input runat="server" id="txtDiemMiengLan4" type="text" />
        <input runat="server" id="txtDiemMiengLan5" type="text" />
        <input runat="server" id="txtDiemMiengLan6" type="text" />
        <input runat="server" id="txtDiem15pLan1" type="text" />
        <input runat="server" id="txtDiem15pLan2" type="text" />
        <input runat="server" id="txtDiem15pLan3" type="text" />
        <input runat="server" id="txtDiem15pLan4" type="text" />
        <input runat="server" id="txtDiem15pLan5" type="text" />
        <input runat="server" id="txtDiem1tLan1" type="text" />
        <input runat="server" id="txtDiem1tLan2" type="text" />
        <input runat="server" id="txtDiem1tLan3" type="text" />
        <input runat="server" id="txtDiemCuoiKi" type="text" />
        <a href="javascript:void(0);" id="btncapNhat" runat="server" onserverclick="btnCapNhat_ServerClick"></a>
    </div>
    <%--<div>
        <input runat="server" id="txtid" />
        <input runat="server" id="txtDiemMiengLan1" />
        <input runat="server" id="txtDiemMiengLan2" />
        <input runat="server" id="txtDiemMiengLan3" />
        <input runat="server" id="txtDiemMiengLan4" />
        <input runat="server" id="txtDiemMiengLan5" />
        <input runat="server" id="txtDiemMiengLan6" />
        <input runat="server" id="txtDiem15pLan1" />
        <input runat="server" id="txtDiem15pLan2" />
        <input runat="server" id="txtDiem15pLan3" />
        <input runat="server" id="txtDiem15pLan4" />
        <input runat="server" id="txtDiem15pLan5" />
        <input runat="server" id="txtDiem1tLan1" />
        <input runat="server" id="txtDiem1tLan2" />
        <input runat="server" id="txtDiem1tLan3" />
        <input runat="server" id="txtDiemCuoiKi" />
    </div>--%>
    <script>
        function getIDDiem(id) {
            document.getElementById("<%= txtIddiem.ClientID %>").value = id;
        }
        function capnhat() {
            document.getElementById("<%= txtDiemMiengLan1.ClientID %>").value = document.getElementById("txtDiemMiengLan1").value;
            document.getElementById("<%= txtDiemMiengLan2.ClientID %>").value = document.getElementById("txtDiemMiengLan2").value;
            document.getElementById("<%= txtDiemMiengLan3.ClientID %>").value = document.getElementById("txtDiemMiengLan3").value;
            document.getElementById("<%= txtDiemMiengLan4.ClientID %>").value = document.getElementById("txtDiemMiengLan4").value;
            document.getElementById("<%= txtDiemMiengLan5.ClientID %>").value = document.getElementById("txtDiemMiengLan5").value;
            document.getElementById("<%= txtDiemMiengLan6.ClientID %>").value = document.getElementById("txtDiemMiengLan6").value;
            document.getElementById("<%= txtDiem15pLan1.ClientID %>").value = document.getElementById("txtDiem15PLan1").value;
            document.getElementById("<%= txtDiem15pLan2.ClientID %>").value = document.getElementById("txtDiem15PLan2").value;
            document.getElementById("<%= txtDiem15pLan3.ClientID %>").value = document.getElementById("txtDiem15PLan3").value;
            document.getElementById("<%= txtDiem15pLan4.ClientID %>").value = document.getElementById("txtDiem15PLan4").value;
            document.getElementById("<%= txtDiem15pLan5.ClientID %>").value = document.getElementById("txtDiem15PLan5").value;
            document.getElementById("<%= txtDiem1tLan1.ClientID %>").value = document.getElementById("txtDiem1TLan1").value;
            document.getElementById("<%= txtDiem1tLan2.ClientID %>").value = document.getElementById("txtDiem1TLan2").value;
            document.getElementById("<%= txtDiem1tLan3.ClientID %>").value = document.getElementById("txtDiem1TLan3").value;
            document.getElementById("<%= txtDiemCuoiKi.ClientID %>").value = document.getElementById("txtDiemCuoiKy").value;
            document.getElementById("<%= btncapNhat.ClientID %>").click();
        }
        <%--  function getidhs(id) {
            document.getElementById("<%=txtid.ClientID%>").value = id;
            document.getElementById("<%=txtDiemMiengLan1.ClientID%>").value = document.getElementById("txtDiemmienglan1").value;
            document.getElementById("<%=txtDiemMiengLan2.ClientID%>").value = document.getElementById("txtDiemmienglan2").value;
            document.getElementById("<%=txtDiemMiengLan3.ClientID%>").value = document.getElementById("txtDiemmienglan3").value;
            document.getElementById("<%=txtDiemMiengLan4.ClientID%>").value = document.getElementById("txtDiemmienglan4").value;
            document.getElementById("<%=txtDiemMiengLan5.ClientID%>").value = document.getElementById("txtDiemmienglan5").value;
            document.getElementById("<%=txtDiemMiengLan6.ClientID%>").value = document.getElementById("txtDiemmienglan6").value;
            document.getElementById("<%=txtDiem15pLan1.ClientID%>").value = document.getElementById("txtDiem15phutlan1").value;
            document.getElementById("<%=txtDiem15pLan2.ClientID%>").value = document.getElementById("txtDiem15phutlan2").value;
            document.getElementById("<%=txtDiem15pLan3.ClientID%>").value = document.getElementById("txtDiem15phutlan3").value;
            document.getElementById("<%=txtDiem15pLan4.ClientID%>").value = document.getElementById("txtDiem15phutlan4").value;
            document.getElementById("<%=txtDiem15pLan5.ClientID%>").value = document.getElementById("txtDiem15phutlan5").value;
            document.getElementById("<%=txtDiem1tLan1.ClientID%>").value = document.getElementById("txtDiem1tietlan1").value;
            document.getElementById("<%=txtDiem1tLan2.ClientID%>").value = document.getElementById("txtDiem1tietlan2").value;
            document.getElementById("<%=txtDiem1tLan3.ClientID%>").value = document.getElementById("txtDiem1tietlan3").value;
            document.getElementById("<%=txtDiemCuoiKi.ClientID%>").value = document.getElementById("txtDiemcuoiky").values;
        }--%>
</script>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

