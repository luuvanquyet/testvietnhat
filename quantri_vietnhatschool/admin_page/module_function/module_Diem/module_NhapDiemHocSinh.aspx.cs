﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_Diem_module_NhapDiemHocSinh : System.Web.UI.Page
{
    public int STT = 1;
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();

    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (!IsPostBack)
        {
            loadDiemChiTiet();
            loadData();
        }
        Session["idDiem"] = 0;
    }
    private void loadData()
    {
        ddllop.DataSource = from l in db.tbLops
                            where l.hidden == false
                            select l;
        ddllop.DataBind();
    }
    private void loadDiemChiTiet()
    {
        var listhsct = from hstl in db.tbHocSinhTrongLops
                       join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                       join d in db.tbDiems on hs.hocsinh_id equals d.hocsinh_id
                       select new
                       {
                           hstl.hstl_id,
                           hs.hocsinh_id,
                           hs.hocsinh_name,
                           d.diem_id,
                           d.diem_diemmienglan1,
                           d.diem_diemmienglan2,
                           d.diem_diemmienglan3,
                           d.diem_diemmienglan4,
                           d.diem_diemmienglan5,
                           d.diem_diemmienglan6,
                           d.diem_diem15phutlan1,
                           d.diem_diem15phutlan2,
                           d.diem_diem15phutlan3,
                           d.diem_diem15phutlan4,
                           d.diem_diem15phutlan5,
                           d.diem_diem1tietlan1,
                           d.diem_diem1tietlan2,
                           d.diem_diem1tietlan3,
                           d.diem_diemcuoiky
                       };
        rpChitiet.DataSource = listhsct;
        rpChitiet.DataBind();
    }
    protected void ddllop_SelectedIndexChanged(object sender, EventArgs e)
    {
        Session["idDiem"] = Convert.ToInt32(ddllop.SelectedItem.Value);
        var lisths = from hstl in db.tbHocSinhTrongLops
                     join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                     join d in db.tbDiems on hs.hocsinh_id equals d.hocsinh_id
                     where hstl.lop_id == Convert.ToInt32(ddllop.SelectedItem.Value)
                     select new
                     {
                         hstl.hstl_id,
                         hs.hocsinh_id,
                         hs.hocsinh_name,
                         d.diem_id,
                         d.diem_diemmienglan1,
                         d.diem_diemmienglan2,
                         d.diem_diemmienglan3,
                         d.diem_diemmienglan4,
                         d.diem_diemmienglan5,
                         d.diem_diemmienglan6,
                         d.diem_diem15phutlan1,
                         d.diem_diem15phutlan2,
                         d.diem_diem15phutlan3,
                         d.diem_diem15phutlan4,
                         d.diem_diem15phutlan5,
                         d.diem_diem1tietlan1,
                         d.diem_diem1tietlan2,
                         d.diem_diem1tietlan3,
                         d.diem_diemcuoiky
                     };
        rpHocSinh.DataSource = lisths;
        rpHocSinh.DataBind();
    }

    protected void btnCapNhat_ServerClick(object sender, EventArgs e)
    {
        tbDiem update = db.tbDiems.Where(x => x.diem_id == Convert.ToInt32(txtIddiem.Value)).FirstOrDefault();
        update.diem_diemmienglan1 = Convert.ToDouble(txtDiemMiengLan1.Value);
        update.diem_diemmienglan2 = Convert.ToDouble(txtDiemMiengLan2.Value);
        update.diem_diemmienglan3 = Convert.ToDouble(txtDiemMiengLan3.Value);
        update.diem_diemmienglan4 = Convert.ToDouble(txtDiemMiengLan4.Value);
        update.diem_diemmienglan5 = Convert.ToDouble(txtDiemMiengLan5.Value);
        update.diem_diemmienglan6 = Convert.ToDouble(txtDiemMiengLan6.Value);
        update.diem_diem15phutlan1 = Convert.ToDouble(txtDiem15pLan1.Value);
        update.diem_diem15phutlan2 = Convert.ToDouble(txtDiem15pLan2.Value);
        update.diem_diem15phutlan3 = Convert.ToDouble(txtDiem15pLan3.Value);
        update.diem_diem15phutlan4 = Convert.ToDouble(txtDiem15pLan4.Value);
        update.diem_diem15phutlan5 = Convert.ToDouble(txtDiem15pLan5.Value);
        update.diem_diem1tietlan1 = Convert.ToDouble(txtDiem1tLan1.Value);
        update.diem_diem1tietlan2 = Convert.ToDouble(txtDiem1tLan2.Value);
        update.diem_diem1tietlan3 = Convert.ToDouble(txtDiem1tLan3.Value);
        update.diem_diemcuoiky = Convert.ToDouble(txtDiemCuoiKi.Value);
        //tính điểm trung bình
        double DTB = (double)(Convert.ToDouble(txtDiemMiengLan1.Value) + Convert.ToDouble(txtDiemMiengLan2.Value) + Convert.ToDouble(txtDiemMiengLan3.Value) + Convert.ToDouble(txtDiemMiengLan4.Value) + Convert.ToDouble(txtDiemMiengLan5.Value) + Convert.ToDouble(txtDiemMiengLan6.Value) + Convert.ToDouble(txtDiem15pLan1.Value) + Convert.ToDouble(txtDiem15pLan2.Value) + Convert.ToDouble(txtDiem15pLan3.Value) + Convert.ToDouble(txtDiem15pLan4.Value) + Convert.ToDouble(txtDiem15pLan5.Value) + (Convert.ToDouble(txtDiem1tLan1.Value) * 2) + (Convert.ToDouble(txtDiem1tLan2.Value) * 2) + (Convert.ToDouble(txtDiem1tLan3.Value) * 2) + (Convert.ToDouble(txtDiemCuoiKi.Value) * 3)) / 20;
        //lưu vào data
        update.diem_diemtrungbinh = DTB;
        //xếp loại
        if (DTB < 4)
            update.diem_xeploai = "Kém";
        else
        {
            if (DTB < 5)
                update.diem_xeploai = "Yếu";
            else
            {
                if (DTB < 7)
                    update.diem_xeploai = "Trung bình";
                else
                {
                    if (DTB < 8)
                        update.diem_xeploai = "Khá";
                    else
                    {
                        if (DTB < 9)
                            update.diem_xeploai = "Giỏi";
                        else
                            update.diem_xeploai = "Xuất sắc";
                    }
                }
            }
        }
        try
        {
            db.SubmitChanges();
            var lisths = from hstl in db.tbHocSinhTrongLops
                         join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                         join d in db.tbDiems on hs.hocsinh_id equals d.hocsinh_id
                         where hstl.lop_id == Convert.ToInt32(ddllop.SelectedItem.Value)
                         //group hstl by hstl.hocsinh_id into g
                         select new
                         {
                             //hocsinh_name = (from hs in db.tbHocSinhs where hs.hocsinh_id == g.Key select hs).FirstOrDefault().hocsinh_name,
                             hstl.hstl_id,
                             hs.hocsinh_id,
                             hs.hocsinh_name,
                             d.diem_id,
                             d.diem_diemmienglan1,
                             d.diem_diemmienglan2,
                             d.diem_diemmienglan3,
                             d.diem_diemmienglan4,
                             d.diem_diemmienglan5,
                             d.diem_diemmienglan6,
                             d.diem_diem15phutlan1,
                             d.diem_diem15phutlan2,
                             d.diem_diem15phutlan3,
                             d.diem_diem15phutlan4,
                             d.diem_diem15phutlan5,
                             d.diem_diem1tietlan1,
                             d.diem_diem1tietlan2,
                             d.diem_diem1tietlan3,
                             d.diem_diemcuoiky
                         };
            rpHocSinh.DataSource = lisths;
            rpHocSinh.DataBind();
            ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Cập nhật thành công!','','success').then(function(){this.Page.Refresh();})", true);
            //alert.alert_Success(Page,"Cập nhật thành công!","");
        }
        catch
        {
        }
        txtIddiem.Value = "";
        txtDiemMiengLan1.Value = "";
        txtDiemMiengLan2.Value = "";
        txtDiemMiengLan3.Value = "";
        txtDiemMiengLan4.Value = "";
        txtDiemMiengLan5.Value = "";
        txtDiemMiengLan6.Value = "";
        txtDiem15pLan1.Value = "";
        txtDiem15pLan2.Value = "";
        txtDiem15pLan3.Value = "";
        txtDiem15pLan4.Value = "";
        txtDiem15pLan5.Value = "";
        txtDiem1tLan1.Value = "";
        txtDiem1tLan2.Value = "";
        txtDiem1tLan3.Value = "";
        txtDiemCuoiKi.Value = "";
    }

}