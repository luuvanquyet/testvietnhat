﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_TaoHocsinhtronglop : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {

        //if (Session["AdminLogined"] != null)
        //{
        //    admin_User logedMember = Session["AdminLogined"] as admin_User;
        //    //if (logedMember.groupuser_id == 3)
        //    //    Response.Redirect("/user-home");
        //    if (!IsPostBack)
        //    {
        //        Session["_id"] = 0;

        //    }
            loadData();
        //}
        //else
        //{
        //    Response.Redirect("/admin-login");
        //}
    }
    private void loadData()
    {

        var getdata = from ac in db.tbHocSinhs
                      //where ac.account_acctive==true && ac.hidden==false
                      orderby ac.hocsinh_id descending
                      select ac;
        grvList.DataSource = getdata;
        grvList.DataBind();
        ddllop.DataSource = from l in db.tbLops
                            select l;
        ddllop.DataBind();
        ddlmh.DataSource = from mh in db.tbMonHocs
                           select mh;
        ddlmh.DataBind();

    }
    private bool setNULL()
    {
        if (ddllop.SelectedItem == null || ddlmh.SelectedItem == null)
        {
            return true;
        }
        else return false;

    }
    //public bool checknull()
    //{
    //    if (grvList.ToString() != "")
    //        return true;
    //    else return false;
    //}
    protected void btnLuu_Click(object sender, EventArgs e)
    {
        cls_TaoHocsinhtronglop cls = new cls_TaoHocsinhtronglop();
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "hocsinh_id" });
        if (selectedKey.Count > 0)
        {
            if (setNULL() == true)
            {
                alert.alert_Warning(Page,"Chưa chọn lớp hoặc môn học!","");
            }
            else
            {
                foreach (var item in selectedKey)
                {
                    if (cls.Linq_Luu(Convert.ToInt32(item), Convert.ToInt32(ddllop.Value.ToString()), Convert.ToInt32(ddlmh.Value.ToString())))
                    {
                        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Thêm thành công','','success').then(function(){grvList.UnselectRows();})", true);
                        loadData();
                    }
                    else
                        alert.alert_Error(Page, "Thêm thất bại", "");
                }
            }
        }
        else
        {
            alert.alert_Warning(Page, "Vui lòng chọn học sinh để thêm vào lớp", "");
        }
    }
}