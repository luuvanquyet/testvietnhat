﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_DiemDanh_module_Nhap_DanhSachHocSinhAnSang : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            var listNV = from l in db.tbLops
                         join gvtl in db.tbGiaoVienTrongLops on l.lop_id equals gvtl.lop_id
                         orderby l.khoi_id
                         select l;
            ddlLop.Items.Clear();
            ddlLop.Items.Insert(0, "Chọn Lớp");
            ddlLop.AppendDataBoundItems = true;
            ddlLop.DataTextField = "lop_name";
            ddlLop.DataValueField = "lop_id";
            ddlLop.DataSource = listNV;
            ddlLop.DataBind();
            Session["_id"] = 0;
        }
        loadData();

    }
    private void loadData()
    {
        var checkNamHoc = (from nh in db.tbHoctap_NamHocs orderby nh.namhoc_id descending select nh).FirstOrDefault();
        if (ddlLop.SelectedValue == "0" || ddlLop.SelectedValue == "Chọn Lớp")
        {

        }
        else
        {
            var getData = from hs in db.tbHocSinhs
                          join hstl in db.tbHocSinhTrongLops on hs.hocsinh_id equals hstl.hocsinh_id
                          join l in db.tbLops on hstl.lop_id equals l.lop_id
                          where hstl.namhoc_id == checkNamHoc.namhoc_id && l.lop_id == Convert.ToInt16(ddlLop.SelectedValue)
                          select new
                          {
                              hs.hocsinh_name,
                              l.lop_name,
                              hstl.hstl_id
                          };
            var getDataBiLock = from hs in db.tbHocSinhs
                                join hstl in db.tbHocSinhTrongLops on hs.hocsinh_id equals hstl.hocsinh_id
                                join l in db.tbLops on hstl.lop_id equals l.lop_id
                                join a in db.tbQuanLyLop_DiemDanh_DanhSachHocSinh_AnSangs on hstl.hstl_id equals a.hstl_id
                                where hstl.namhoc_id == checkNamHoc.namhoc_id && l.lop_id == Convert.ToInt16(ddlLop.SelectedValue) && a.ansang_hidden==true
                                select new
                                {
                                    hs.hocsinh_name,
                                    l.lop_name,
                                    hstl.hstl_id
                                };
            var list = getData.Except(getDataBiLock);
            grvList.DataSource = list;
            grvList.DataBind();
        }
    }
    protected void btnLuu_ServerClick(object sender, EventArgs e)
    {
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "hstl_id" });
           if (selectedKey.Count > 0)
           {
               foreach (var item in selectedKey)
               {
                   var checkHocSinh = from ck in db.tbQuanLyLop_DiemDanh_DanhSachHocSinh_AnSangs
                                      where ck.hstl_id == Convert.ToInt32(item)
                                      select ck;
                   tbQuanLyLop_DiemDanh_DanhSachHocSinh_AnSang insert = new tbQuanLyLop_DiemDanh_DanhSachHocSinh_AnSang();
                   insert.ansang_datecreate = Convert.ToDateTime(dteNgayBatDau.Value);
                   if(checkHocSinh.Count()>0)
                   {
                       insert.ansang_dot = checkHocSinh.Count() + 1;
                   }
                   else
                   {
                       insert.ansang_dot = 1;
                   }
                   insert.hstl_id = Convert.ToInt32(item);
                   db.tbQuanLyLop_DiemDanh_DanhSachHocSinh_AnSangs.InsertOnSubmit(insert);
                   db.SubmitChanges();
               }
               ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Đã hoàn thành!','','success').then(function(){grvList.UnselectRows();})", true);
           }
    }
}