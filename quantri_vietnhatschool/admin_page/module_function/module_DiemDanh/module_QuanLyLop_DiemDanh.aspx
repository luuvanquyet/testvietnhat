﻿<%@ Page Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_QuanLyLop_DiemDanh.aspx.cs" Inherits="admin_page_module_function_module_WebSite_module_QuanLyLop_DiemDanh" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v17.1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <script>
        function myLuu(id) {
            document.getElementById("<%=txtMaHocSinh.ClientID%>").value = id;
            // alert(document.getElementById("txtghiChu" + id).value)
            document.getElementById("<%=txtGhiChuHocSinh.ClientID%>").value = document.getElementById("txtghiChu" + id).value;

            document.getElementById("<%=btnSave.ClientID%>").click();
        }
        function myPhep(id) {
            document.getElementById("<%=txtPhep.ClientID%>").value = id;
        }
        function mySuatAn(id) {
            document.getElementById("<%=txtSuatAn.ClientID%>").value = id;
        }
        function check(id) {
            var value = id.options[id.selectedIndex].value;
            alert(value);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="main-omt">
        <div class="omt-header">
            <i class="fa fa-user omt__icon" aria-hidden="true"></i>
            <h4 class="header-title">Điểm Danh &nbsp;</h4>
        </div>
        <div class="card card-block">
            <div class="form-group row">
                <div class="form-group search">
                    <div class="col-3" style="margin-left: 10px;">
                        <input id="dteNgay" type="date" runat="server" class="form-control" />
                    </div>
                    <div class="col-sm-3">
                        <asp:DropDownList ID="ddlLop" runat="server" AutoPostBack="true" CssClass="form-control"></asp:DropDownList>
                    </div>
                    <div class="col-sm-3" <%--style="margin-left: 170px"--%>>
                        <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control" autocomplete="off" placeholder="Họ và tên"></asp:TextBox>
                    </div>
                    <div class="col-sm-2">
                        <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" CssClass="btn btn-primary" Text="Tìm kiếm" />
                    </div>
                </div>
            </div>
            <div class="form-group table-responsive">
                <table class="table table-bordered">
                    <thead class="thead-omt">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Mã học sinh</th>
                            <th scope="col">Họ và Tên</th>
                            <th scope="col">Trạng thái</th>
                            <%--<th scope="col">Ăn sáng</th>--%>
                            <th scope="col">Ăn trưa</th>
                            <th scope="col">Ghi chú</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rpLop" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <th><%=STT++ %></th>
                                    <td><%#Eval("hocsinh_code") %></td>
                                    <td><%#Eval("hocsinh_name") %></td>
                                    <td>
                                        <%--<asp:DropDownList ID="ddlTrangThaiDiHoc" runat="server" CssClass="form-control" DataValueField="trangthai_id" DataTextField="trangthai_name"></asp:DropDownList>--%>
                                        <select id="ddlPhep" onchange="myPhep(this.value)">
                                            <option value="1">Đúng giờ</option>
                                            <option value="2">Đến muộn</option>
                                            <option value="3">Vắng có phép</option>
                                            <option value="4">Vắng không phép</option>
                                        </select>
                                    </td>
                                    <td>
                                        <%--<asp:DropDownList ID="ddlGhiChuSuatAn" onchange="check(hibodywrapper_rpLop_ddlGhiChuSuatAn_0)" runat="server" CssClass="form-control" DataValueField="ghichu_id" DataTextField="ghichu_name"></asp:DropDownList>--%>
                                        <select id="ddlSuatAn" onchange="mySuatAn(this.value)">
                                            <option value="1">Có ăn trưa</option>
                                            <option value="2">Không có ăn trưa</option>
                                        </select>
                                    </td>
                                    <td>
                                        <input id="txtghiChu<%#Eval("hstl_id") %>" type="text" style="width: 100%" />
                                    </td>
                                    <td>
                                        <a href="#" class="btn btn-primary" id="btnLuu" onclick="myLuu(<%#Eval("hstl_id") %>)">Lưu</a>
                                    </td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>
            <div style="display: none">
                <asp:UpdatePanel ID="btnUpdate" runat="server">
                    <ContentTemplate>
                        <input id="txtMaHocSinh" type="text" runat="server" />
                        <input id="txtGhiChuHocSinh" type="text" runat="server" />
                        <input id="txtPhep" runat="server" type="text" />
                        <input id="txtSuatAn" runat="server" type="text" />
                        <a href="#" id="btnSave" runat="server" onserverclick="btnSave_ServerClick"></a>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>

    <style>
        .main-omt {
            border: 1px solid #32c5d2;
            background-color: #fff;
        }

            .main-omt .omt-header {
                background-color: #32c5d2;
                padding: 4px 7px;
                display: flex;
            }

        .omt-header .header-title {
            padding: 10px 10px;
            color: white;
        }

        .omt-header .omt__icon {
            font-size: 35px;
            padding: 5px 10px;
            color: white;
        }
    </style>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

