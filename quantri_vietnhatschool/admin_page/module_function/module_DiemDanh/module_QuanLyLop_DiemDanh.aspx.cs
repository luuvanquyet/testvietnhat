﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_WebSite_module_QuanLyLop_DiemDanh : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    public int STT = 1;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["_id"] = 0;
            var listNV = from l in db.tbLops
                         join gvtl in db.tbGiaoVienTrongLops on l.lop_id equals gvtl.lop_id
                         select l;
            ddlLop.Items.Clear();
            ddlLop.Items.Insert(0, "Chọn Lớp");
            ddlLop.AppendDataBoundItems = true;
            ddlLop.DataTextField = "lop_name";
            ddlLop.DataValueField = "lop_id";
            ddlLop.DataSource = listNV;
            ddlLop.DataBind();
        }
        loadData();
    }
    private void loadData()
    {
        var checkNamHoc = (from nh in db.tbHoctap_NamHocs orderby nh.namhoc_id descending select nh).FirstOrDefault();
        if (ddlLop.SelectedValue == "0" || ddlLop.SelectedValue == "Chọn Lớp")
        {

        }
        else
        {

            // load data đổ vào var danh sách
            var getData = from l in db.tbLops
                          join hstl in db.tbHocSinhTrongLops on l.lop_id equals hstl.lop_id
                          join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                          where l.lop_id == Convert.ToInt32(ddlLop.SelectedValue)
                          && hstl.namhoc_id == checkNamHoc.namhoc_id
                          orderby hstl.position
                          select new
                          {
                              STT = STT + 1,
                              hs.hocsinh_code,
                              hs.hocsinh_name,
                              hstl.hstl_id
                          };
            // đẩy dữ liệu vào gridivew
            rpLop.DataSource = getData;
            rpLop.DataBind();
            var checkData = from at in db.tbDiemDanh_HocSinhs
                            join hstl in db.tbHocSinhTrongLops on at.hstl_id equals hstl.hstl_id
                            where hstl.lop_id == Convert.ToInt32(ddlLop.SelectedValue)
                            && at.diemdanh_ngay.Value.Date == Convert.ToDateTime(dteNgay.Value).Date
                            && at.diemdanh_ngay.Value.Month == Convert.ToDateTime(dteNgay.Value).Month
                            && at.diemdanh_ngay.Value.Year == Convert.ToDateTime(dteNgay.Value).Year
                            select at;
            //nếu đã có danh sách điểm danh thì get đúng trạng thái
            if (checkData.Count() > 0)
            {

            }
            else
            {
                foreach (var item in getData)
                {
                    tbDiemDanh_HocSinh insert = new tbDiemDanh_HocSinh();
                    insert.diemdanh_ngay = Convert.ToDateTime(dteNgay.Value);
                    insert.tinhtrang = 1;
                    insert.diemdanh_lydo = "";
                    insert.hstl_id = item.hstl_id;
                    insert.ghichusuatan = 1;
                    db.tbDiemDanh_HocSinhs.InsertOnSubmit(insert);
                    try
                    {
                        db.SubmitChanges();
                    }
                    catch
                    {

                    }
                }
            }
        }
    }


    protected void btnThem_Click(object sender, EventArgs e)
    {

    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (txtSearch.Text != "")
        {
            var list = from hs in db.tbHocSinhs
                       join hstl in db.tbHocSinhTrongLops on hs.hocsinh_id equals hstl.hocsinh_id
                       where hs.hocsinh_name.Contains(txtSearch.Text)
                       && hstl.lop_id == Convert.ToInt16(ddlLop.SelectedValue)
                       orderby hstl.position
                       select new
                       {
                           STT = STT + 1,
                           hs.hocsinh_code,
                           hs.hocsinh_name,
                           hstl.hstl_id
                       };
            rpLop.DataSource = list;
            rpLop.DataBind();
        }
        else
        {
            alert.alert_Error(Page, "Vùi lòng nhập tên học sinh", "");
        }
    }
    protected void btnSave_ServerClick(object sender, EventArgs e)
    {
        if (txtMaHocSinh.Value != "")
        {
            cls_HocSinh cls = new cls_HocSinh();
            if (txtPhep.Value == "")
                txtPhep.Value = "1";
            if (txtSuatAn.Value == "")
                txtSuatAn.Value = "1";
            tbDiemDanh_HocSinh checkNgayAnTrua = (from an in db.tbDiemDanh_HocSinhs
                                                  where an.diemdanh_ngay.Value.Date == Convert.ToDateTime(dteNgay.Value).Date
                                                      && an.diemdanh_ngay.Value.Month == Convert.ToDateTime(dteNgay.Value).Month
                                                      && an.diemdanh_ngay.Value.Year == Convert.ToDateTime(dteNgay.Value).Year
                                                      && an.hstl_id == Convert.ToInt32(txtMaHocSinh.Value)
                                                  select an).SingleOrDefault();
            if (checkNgayAnTrua != null)
            {
                checkNgayAnTrua.diemdanh_ngay = Convert.ToDateTime(dteNgay.Value);
                checkNgayAnTrua.tinhtrang = Convert.ToInt16(txtPhep.Value);
                checkNgayAnTrua.diemdanh_lydo = txtGhiChuHocSinh.Value;
                checkNgayAnTrua.hstl_id = Convert.ToInt32(txtMaHocSinh.Value);
                checkNgayAnTrua.ghichusuatan = Convert.ToInt16(txtSuatAn.Value);
                db.SubmitChanges();
                txtMaHocSinh.Value = "";
                txtPhep.Value = "";
                txtSuatAn.Value = "";
                txtGhiChuHocSinh.Value = "";
                alert.alert_Success(Page, "Hoàn thành", "");
            }
        }
        else
        {
            alert.alert_Error(Page, "Lỗi", "");
        }
    }
}


