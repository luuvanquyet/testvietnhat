﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_WebSite_module_QuanLyLop_DiemDanh_AnSang : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    public int STT = 1;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["_id"] = 0;
            var listNV = from l in db.tbLops
                         join gvtl in db.tbGiaoVienTrongLops on l.lop_id equals gvtl.lop_id
                         select l;
            ddlLop.Items.Clear();
            ddlLop.Items.Insert(0, "Chọn Lớp");
            ddlLop.AppendDataBoundItems = true;
            ddlLop.DataTextField = "lop_name";
            ddlLop.DataValueField = "lop_id";
            ddlLop.DataSource = listNV;
            ddlLop.DataBind();
        }
        loadData();
    }
    private void loadData()
    {
        if (ddlLop.SelectedValue == "0" || ddlLop.SelectedValue == "Chọn Lớp")
        {

        }
        else
        {
            // load data đổ vào var danh sách
            var getData = from l in db.tbLops
                          join hstl in db.tbHocSinhTrongLops on l.lop_id equals hstl.lop_id
                          join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                          where l.lop_id == Convert.ToInt32(ddlLop.SelectedValue)
                          select new
                          {
                              STT = STT + 1,
                              hs.hocsinh_name,
                              hstl.hstl_id
                          };
            // đẩy dữ liệu vào gridivew
            rpLop.DataSource = getData;
            rpLop.DataBind();
        }
    }


    protected void btnThem_Click(object sender, EventArgs e)
    {

    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (txtSearch.Text != "")
        {
            var list = from hs in db.tbHocSinhs
                       join hstl in db.tbHocSinhTrongLops on hs.hocsinh_id equals hstl.hocsinh_id
                       where hs.hocsinh_name.Contains(txtSearch.Text) && hstl.lop_id ==Convert.ToInt16(ddlLop.SelectedValue)
                       select new
                       {
                           STT = STT + 1,
                           hs.hocsinh_name,
                           hstl.hstl_id
                       };
            rpLop.DataSource = list;
            rpLop.DataBind();
        }
        else
        {
            alert.alert_Error(Page, "Vùi lòng nhập tên học sinh", "");
        }
    }
    protected void btnSave_ServerClick(object sender, EventArgs e)
    {
        if (txtMaHocSinh.Value != "")
        {
            cls_HocSinh cls = new cls_HocSinh();
            if (txtPhep.Value == "")
                txtPhep.Value = "1";
            if (txtSuatAn.Value == "")
                txtSuatAn.Value = "1";
            if (cls.Linq_Insert_DanhSach(Convert.ToInt16(txtPhep.Value), Convert.ToInt16(txtSuatAn.Value), txtGhiChuHocSinh.Value, Convert.ToInt32(txtMaHocSinh.Value)))
            {
                alert.alert_Success(Page, "Hoàn thành", "");
                txtMaHocSinh.Value = "";
                txtPhep.Value = "";
                txtSuatAn.Value = "";
                txtGhiChuHocSinh.Value = "";
            }
            else
            {
                alert.alert_Error(Page, "Lỗi", "");
            }
        }
        else
        {
            alert.alert_Error(Page, "Lỗi", "");
        }
    }
}


