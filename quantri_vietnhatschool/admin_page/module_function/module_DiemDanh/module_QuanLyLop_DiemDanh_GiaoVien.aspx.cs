﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_WebSite_module_QuanLyLop_DiemDanh_GiaoVien : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    public int STT = 1;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            var checkuserid = (from u in db.admin_Users where u.username_username == Request.Cookies["UserName"].Value select u).First();
            // int khoi_id = Convert.ToInt32((from l in db.tbGiaoViens where l.giaovien_id == checkuserid.username_id select l).First().khoi_id);
            Session["_id"] = 0;
            var listNV = from l in db.tbLops
                         join gvtl in db.tbGiaoVienTrongLops on l.lop_id equals gvtl.lop_id
                         select l;
            ddlLop.Items.Clear();
            ddlLop.Items.Insert(0, "Chọn Lớp");
            ddlLop.AppendDataBoundItems = true;
            ddlLop.DataTextField = "lop_name";
            ddlLop.DataValueField = "lop_id";
            ddlLop.DataSource = listNV;
            ddlLop.DataBind();
            var listMon = from l in db.tbMonHocs
                          join gvmh in db.tbHocTap_GiaoVien_MonHocs on l.monhoc_id equals gvmh.monhoc_id
                          where gvmh.giaovien_id == checkuserid.username_id
                          select l;
            ddlMon.Items.Clear();
            ddlMon.Items.Insert(0, "Chọn Môn");
            ddlMon.AppendDataBoundItems = true;
            ddlMon.DataTextField = "monhoc_name";
            ddlMon.DataValueField = "monhoc_id";
            ddlMon.DataSource = listMon;
            ddlMon.DataBind();
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
       
        if (ddlLop.SelectedValue == "Chọn Lớp" || ddlMon.SelectedValue == "Chọn Môn")
        {
            alert.alert_Error(Page, "Vui lòng chọn lớp và chọn môn", "");
        }
        else
        {
            var list = from hs in db.tbHocSinhs
                       join hstl in db.tbHocSinhTrongLops on hs.hocsinh_id equals hstl.hocsinh_id
                       where hstl.lop_id == Convert.ToInt16(ddlLop.SelectedValue)
                         && hstl.namhoc_id == 1
                       orderby hstl.position
                       //  && hs.hocsinh_name.Contains(txtSearch.Text)
                       select new
                       {
                           STT = STT + 1,
                           hs.hocsinh_name,
                           hstl.hstl_id
                       };
            var checkHocSinh = from hs in db.tbHocSinhs
                               join hstl in db.tbHocSinhTrongLops on hs.hocsinh_id equals hstl.hocsinh_id
                               join dd in db.tbDiemDanh_HocOnlines on hstl.hstl_id equals dd.hstl_id
                               where hstl.lop_id == Convert.ToInt16(ddlLop.SelectedValue)
                               && dd.monhoc_id == Convert.ToInt16(ddlMon.SelectedValue)
                               && dd.diemdanhhoconline_ngay.Value.Date >= DateTime.Now.Date
                        && dd.diemdanhhoconline_ngay.Value.Month >= DateTime.Now.Month
                        && dd.diemdanhhoconline_ngay.Value.Year >= DateTime.Now.Year
                        && hstl.namhoc_id==1
                        orderby hstl.position
                               select new
                               {
                                   STT = STT + 1,
                                   hs.hocsinh_name,
                                   hstl.hstl_id
                               };
            var ketqua = list.Except(checkHocSinh);
            rpLop.DataSource = ketqua;
            rpLop.DataBind();
        }
    }
    protected void btnSave_ServerClick(object sender, EventArgs e)
    {
        if (txtMaHocSinh.Value != "")
        {
            cls_HocSinh cls = new cls_HocSinh();
            if (txtPhep.Value == "")
                txtPhep.Value = "1";
            if (txtSuatAn.Value == "")
                txtSuatAn.Value = "1";
            var checkuserid = (from u in db.admin_Users where u.username_username == Request.Cookies["UserName"].Value select u).First();
            tbDiemDanh_HocOnline insert = new tbDiemDanh_HocOnline();
            insert.diemdanhhoconline_ngay = DateTime.Now;
            insert.diemdanhhoconline_tinhtrang = Convert.ToInt16(txtPhep.Value);
            insert.diemdanhhoconline_lydo = txtGhiChuHocSinh.Value;
            insert.hstl_id = Convert.ToInt32(txtMaHocSinh.Value);
            insert.monhoc_id = Convert.ToInt32(ddlMon.SelectedValue);
            insert.lop_id = Convert.ToInt32(ddlLop.SelectedValue);
            insert.username_id = checkuserid.username_id;
            db.tbDiemDanh_HocOnlines.InsertOnSubmit(insert);
            try
            {
                db.SubmitChanges();
                txtMaHocSinh.Value = "";
                alert.alert_Success(Page, "Đã hoàn thành", "");
            }
            catch
            {

            }

        }
        else
        {
            alert.alert_Error(Page, "Lỗi", "");
        }
    }
}


