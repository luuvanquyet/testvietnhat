﻿<%@ Page Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_QuanLyLop_DiemDanh_GiaoVien_Gridview.aspx.cs" Inherits="admin_page_module_function_module_WebSite_module_QuanLyLop_DiemDanh_GiaoVien_Gridview" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v17.1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="main-omt">
        <div class="omt-header">
            <i class="fa fa-user omt__icon" aria-hidden="true"></i>
            <h4 class="header-title">Điểm Danh &nbsp; (<%=DateTime.Now %>)</h4>
        </div>
        <div class="card card-block">
            <div class="form-group row">
                <div class="form-group search">
                    <div class="col-sm-3">
                        <asp:DropDownList ID="ddlLop" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlPage_SelectedIndexChanged" CssClass="form-control"></asp:DropDownList>
                    </div>
                    <div class="col-sm-3">
                        <asp:DropDownList ID="ddlMon" runat="server" AutoPostBack="true" CssClass="form-control"></asp:DropDownList>
                    </div>
                    <div class="col-sm-3" <%--style="margin-left: 170px"--%>>
                        <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control" autocomplete="off" placeholder="Họ và tên"></asp:TextBox>
                    </div>
                  <%--  <div class="col-sm-3">
                        <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" CssClass="btn btn-primary" Text="Tìm kiếm" />
                    </div>--%>
                </div>
            </div>
            <div class="form-group table-responsive">
                <asp:GridView ID="grvList" CssClass="table table-striped table-bordered table-hover" runat="server" AutoGenerateColumns="false" DataKeyNames="diemdanhhoconline_id"
                    OnRowDataBound="OnRowDataBound" OnRowEditing="OnRowEditing" OnRowCancelingEdit="OnRowCancelingEdit"
                    OnRowUpdating="OnRowUpdating" Width="100%" EmptyDataText="No records has been added.">
                    <Columns>
                        <asp:TemplateField HeaderText="STT" ItemStyle-Width="" HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lblSTT" runat="server" Text='<%# Eval("diemdanhhoconline_id") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Học sinh" ItemStyle-Width="" HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lblHocSinh" runat="server" Text='<%# Eval("hocsinh_name") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtHocSinh" runat="server" Width="150px" Text='<%# Eval("hocsinh_name") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Tình trạng" ItemStyle-Width="" HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lblTinhTrang" runat="server" Text='<%# Eval("diemdanhhoconline_tinhtrang") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:DropDownList ID="ddlTinhTrang" runat="server" Width="150px" Text='<%# Eval("diemdanhhoconline_tinhtrang") %>'></asp:DropDownList>
                            </EditItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Ghi chú" ItemStyle-Width="" HeaderStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Label ID="lblhanquoc" runat="server" Text='<%# Eval("diemdanhhoconline_lydo") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="txtLyDo" runat="server" Width="150px" Text='<%# Eval("diemdanhhoconline_lydo") %>'></asp:TextBox>
                            </EditItemTemplate>
                        </asp:TemplateField>
                       
                        <asp:CommandField ButtonType="Link" ShowEditButton="true"
                            EditText="Edit"
                            CancelText="Cancel"
                            UpdateText="Update"
                            HeaderText="Action"
                            ItemStyle-Width="" />
                    </Columns>
                </asp:GridView>
            </div>

        </div>
    </div>

    <style>
        .main-omt {
            border: 1px solid #32c5d2;
            background-color: #fff;
        }

            .main-omt .omt-header {
                background-color: #32c5d2;
                padding: 4px 7px;
                display: flex;
            }

        .omt-header .header-title {
            padding: 10px 10px;
            color: white;
        }

        .omt-header .omt__icon {
            font-size: 35px;
            padding: 5px 10px;
            color: white;
        }
    </style>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

