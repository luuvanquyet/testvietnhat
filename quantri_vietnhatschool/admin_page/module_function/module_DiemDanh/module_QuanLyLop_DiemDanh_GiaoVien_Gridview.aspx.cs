﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_WebSite_module_QuanLyLop_DiemDanh_GiaoVien_Gridview : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
           
            var checkuserid = (from u in db.admin_Users where u.username_username == Request.Cookies["UserName"].Value select u).First();
          // int khoi_id = Convert.ToInt32((from l in db.tbGiaoViens where l.giaovien_id == checkuserid.username_id select l).First().khoi_id);
            var listNV = from l in db.tbLops
                         join gvtl in db.tbGiaoVienTrongLops on l.lop_id equals gvtl.lop_id
                         select l;
            ddlLop.Items.Clear();
            ddlLop.Items.Insert(0, "Chọn Lớp");
            ddlLop.AppendDataBoundItems = true;
            ddlLop.DataTextField = "lop_name";
            ddlLop.DataValueField = "lop_id";
            ddlLop.DataSource = listNV;
            ddlLop.DataBind();
            var listMon = from l in db.tbMonHocs
                          join gvmh in db.tbHocTap_GiaoVien_MonHocs on l.monhoc_id equals gvmh.monhoc_id
                          where gvmh.giaovien_id == checkuserid.username_id
                          select l;
            ddlMon.Items.Clear();
            ddlMon.Items.Insert(0, "Chọn Môn");
            ddlMon.AppendDataBoundItems = true;
            ddlMon.DataTextField = "monhoc_name";
            ddlMon.DataValueField = "monhoc_id";
            ddlMon.DataSource = listMon;
            ddlMon.DataBind();
            this.BindGrid();
        }
       
    }
    protected void PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        grvList.PageIndex = e.NewPageIndex;
        BindGrid();
    }
    private void BindGrid()
    {
        var checkNamHoc = (from nh in db.tbHoctap_NamHocs orderby nh.namhoc_id descending select nh).FirstOrDefault();
        // sử dụng LINQ lấy data từ csdl
       var list = from l in db.tbLops
                             join hstl in db.tbHocSinhTrongLops on l.lop_id equals hstl.lop_id
                             join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                             join d in db.tbDiemDanh_HocOnlines on l.lop_id equals d.lop_id
                             join tt in db.tbTinhTrangHocOnlines on d.diemdanhhoconline_tinhtrang equals tt.tinhtranghoconline_id
                             where l.lop_id == Convert.ToInt32(ddlLop.SelectedValue) && d.monhoc_id == Convert.ToInt32(ddlMon.SelectedValue) && hstl.namhoc_id == checkNamHoc.namhoc_id
                             select new
                             {
                                 d.diemdanhhoconline_id,
                                 hs.hocsinh_name,
                                 hstl.hstl_id,
                                 tt.tinhtranghoconline_name,
                                 d.diemdanhhoconline_lydo,
                             };
       grvList.DataSource = list;
        // fill vào gridview
        grvList.DataBind();
    }
    protected void OnRowDataBound(object sender, GridViewRowEventArgs e)
    {

    }
    protected void OnRowEditing(object sender, GridViewEditEventArgs e)
    {
        var checkNamHoc = (from nh in db.tbHoctap_NamHocs orderby nh.namhoc_id descending select nh).FirstOrDefault();
        // edit row
        grvList.EditIndex = e.NewEditIndex;
        string id_Lop = (ddlLop.SelectedValue.ToString());
        string id_Mon = (ddlMon.SelectedValue.ToString());
        if (id_Lop != "Chọn Lớp")
        {
            grvList.DataSource = from l in db.tbLops
                                 join hstl in db.tbHocSinhTrongLops on l.lop_id equals hstl.lop_id
                                 join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                                 join d in db.tbDiemDanh_HocOnlines on l.lop_id equals d.lop_id
                                 join tt in db.tbTinhTrangHocOnlines on d.diemdanhhoconline_tinhtrang equals tt.tinhtranghoconline_id
                                 where l.lop_id == Convert.ToInt32(ddlLop.SelectedValue) && d.monhoc_id == Convert.ToInt32(ddlMon.SelectedValue) && hstl.namhoc_id == checkNamHoc.namhoc_id
                                 select new
                                 {
                                     hs.hocsinh_name,
                                     hstl.hstl_id,
                                     tt.tinhtranghoconline_name,
                                 };
            // fill vào gridview
            grvList.DataBind();
        }
        else
        {
            BindGrid();
        }
    }
    protected void OnRowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var checkNamHoc = (from nh in db.tbHoctap_NamHocs orderby nh.namhoc_id descending select nh).FirstOrDefault();
        // lấy id của row đang edit
        GridViewRow row = grvList.Rows[e.RowIndex];
        int languagepage_id = Convert.ToInt32(grvList.DataKeys[e.RowIndex].Values[0].ToString());
        // string pagetitle = (row.FindControl("txtenglish") as TextBox).Text;
        //string key = (row.FindControl("txtenglish") as TextBox).Text;
        string hocsinh = (row.FindControl("txtHocSinh") as TextBox).Text;
        string tinhtrang = (row.FindControl("ddlTinhTrang") as DropDownList).Text;
        string lydo = (row.FindControl("txtLyDo") as TextBox).Text;
        tbDiemDanh_HocOnline update = (from c in db.tbDiemDanh_HocOnlines
                            where c.diemdanhhoconline_id == languagepage_id
                            select c).FirstOrDefault();
        //update.MaGiaoVien = name;
        update.diemdanhhoconline_id = languagepage_id;
        update.hstl_id = Convert.ToInt32(hocsinh);
        update.diemdanhhoconline_tinhtrang =  Convert.ToInt32(tinhtrang);
        update.diemdanhhoconline_lydo = lydo;
        // thực hiện thay đổi
        db.SubmitChanges();
        grvList.EditIndex = -1;
        // load lại danh sách
        string id = (ddlLop.SelectedValue.ToString());
        if (id != "Chọn Lớp")
        {
            grvList.DataSource = from l in db.tbLops
                                 join hstl in db.tbHocSinhTrongLops on l.lop_id equals hstl.lop_id
                                 join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                                 join d in db.tbDiemDanh_HocOnlines on l.lop_id equals d.lop_id
                                 join tt in db.tbTinhTrangHocOnlines on d.diemdanhhoconline_tinhtrang equals tt.tinhtranghoconline_id
                                 where l.lop_id == Convert.ToInt32(ddlLop.SelectedValue) && d.monhoc_id == Convert.ToInt32(ddlMon.SelectedValue) && hstl.namhoc_id == checkNamHoc.namhoc_id
                                 select new
                                 {
                                     hs.hocsinh_name,
                                     hstl.hstl_id,
                                     tt.tinhtranghoconline_name,
                                 };
            // fill vào gridview
            grvList.DataBind();
        }
        else
        {
            BindGrid();
        }
    }
    protected void OnRowCancelingEdit(object sender, EventArgs e)
    {
        // cancel edit row
        grvList.EditIndex = -1;
        this.BindGrid();
    }
    protected void ddlPage_SelectedIndexChanged(object sender, EventArgs e)
    {
        var checkNamHoc = (from nh in db.tbHoctap_NamHocs orderby nh.namhoc_id descending select nh).FirstOrDefault();
        string id = (ddlLop.SelectedValue.ToString());
        if (id != "Chọn Lớp")
        {
            grvList.DataSource = from l in db.tbLops
                                 join hstl in db.tbHocSinhTrongLops on l.lop_id equals hstl.lop_id
                                 join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                                 join d in db.tbDiemDanh_HocOnlines on l.lop_id equals d.lop_id
                                 join tt in db.tbTinhTrangHocOnlines on d.diemdanhhoconline_tinhtrang equals tt.tinhtranghoconline_id
                                 where l.lop_id == Convert.ToInt32(ddlLop.SelectedValue) && d.monhoc_id == Convert.ToInt32(ddlMon.SelectedValue) && hstl.namhoc_id == checkNamHoc.namhoc_id
                                 select new
                                 {
                                     hs.hocsinh_name,
                                     hstl.hstl_id,
                                     tt.tinhtranghoconline_name,
                                 };
            // fill vào gridview
            grvList.DataBind();
        }
        else
        {
            BindGrid();
        }
    }
}


