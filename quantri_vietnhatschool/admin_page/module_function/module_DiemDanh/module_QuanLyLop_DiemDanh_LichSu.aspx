﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_QuanLyLop_DiemDanh_LichSu.aspx.cs" Inherits="admin_page_module_function_module_HocTap_module_QuanLyLop_DiemDanh_LichSu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="main-omt">
        <div class="omt-header">
            <i class="fa fa-user omt__icon" aria-hidden="true"></i>
            <h4 class="header-title">Lịch sử điểm Danh</h4>
        </div>
        <%--  <div class="omt-top">
        </div>--%>
        <div class=" container form-group">
            <div class="row">
                <div class="col-3">
                    Chọn lớp
                 <asp:DropDownList ID="ddlLop" runat="server" Width="90%" class="form-control"></asp:DropDownList>
                </div>
                <div class="col-3">
                    Từ ngày:<input type="date" id="txtTuNgay" runat="server" class="form-control" style="width: 90%" />
                </div>
                <div class="col-3">
                    Đến ngày:<input type="date" id="txtDenNgay" runat="server" class="form-control" style="width: 90%" />
                </div>
                <a href="#" id="btnSearch" runat="server" class="btn btn-primary btn__search" onserverclick="btnSearch_ServerClick">Search</a>
            </div>
        </div>
        <div class="table-omt omt-title-bot" style="overflow-x: auto">
            <table class="table table-bordered">
                <thead class="thead-omt">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Họ và Tên</th>
                        <th scope="col">Không phép</th>
                        <th scope="col">Có phép</th>
                        <th scope="col">Tổng</th>
                        <th scope="col">Xem chi tiết</th>
                        <%--<th scope="col">tình trạng</th>
                        <th scope="col">Ghi chú suất ăn</th>
                        <th scope="col">Ghi chú</th>
                        <th scope="col">Ngày</th>
                        <th scope="col">Phụ trách</th>--%>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater ID="rpLop" runat="server">
                        <ItemTemplate>
                            <tr>
                                <th style="text-align: center"><%=STT++ %></th>
                                <td><%#Eval("hocsinh_name") %></td>
                                <%--<td><%#Eval("tinhtrang") %></td>
                                <td><%#Eval("ghichusuatan") %></td>
                                <td><%#Eval("diemdanh_lydo") %></td>
                                <td><%#Eval("diemdanh_ngay") %></td>--%>
                                <td style="text-align: center"><%#Eval("countkhongphep") %></td>
                                <td style="text-align: center"><%#Eval("countcophep") %></td>
                                <td style="text-align: center"><%#Eval("tong") %></td>
                                <td style="text-align: center">
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-<%#Eval("hocsinh_id") %>">Xem</button>
                                    <%--<a href="javascript:void(0)" id="box-modal-<%#Eval("hocsinh_id") %>" onclick="myDetail(this.id)">Xem</a>--%>
                                </td>
                                <%-- <td>Phùng Đức</td>--%>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
        </div>
    </div>
    <%--modal--%>
    <asp:Repeater ID="rpPopup" runat="server" OnItemDataBound="rpPopup_ItemDataBound">
        <ItemTemplate>
            <!-- Modal -->
            <div class="modal fade" id="modal-<%#Eval("hocsinh_id") %>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">CHI TIẾT VẮNG HỌC</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <table class="table table-hover">
                                <tr>
                                    <th style="text-align: center">Họ tên</th>
                                    <th style="text-align: center">Ngày vắng học</th>
                                    <th style="text-align: center">Hình thức vắng</th>
                                </tr>
                                <asp:Repeater runat="server" ID="rpChiTietVangHoc">
                                    <ItemTemplate>
                                        <tr style="text-align: center">
                                            <td><%#Eval("hocsinh_name") %></td>
                                            <td><%#Eval("diemdanh_ngay", "{0: dd/MM/yyyy}") %></td>
                                            <td><%#Eval("tinhtrang")  %></td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Đóng</button>
                        </div>
                    </div>
                </div>
            </div>
        </ItemTemplate>
    </asp:Repeater>
    <style>
        .modal-title {
            text-align: center;
            margin: 0;
            padding: 0;
            padding-top: 20px;
            line-height: 1px;
        }

        .modal-body {
            position: relative;
            padding: 15px;
            background: #c7c9cc;
        }

        .main-omt {
            border: 1px solid #32c5d2;
            background-color: #fff;
        }

            .main-omt .omt-header {
                background-color: #32c5d2;
                padding: 4px 7px;
                display: flex;
            }

        .omt-header .header-title {
            padding: 10px 10px;
            color: white;
        }

        .omt-header .omt__icon {
            font-size: 35px;
            padding: 5px 10px;
            color: white;
        }

        .omt-top {
            display: flex;
            padding: 0 20px;
        }

        .form-omt {
            width: 10%;
            height: 38px;
            margin-right: 15px;
            margin-top: 10px;
        }

        .form-group__omt {
            margin-top: 10px;
            width: 35%;
        }

        .omt-title-top {
            padding: 20px;
            background-color: #fbe1e3;
            margin: 0 20px;
            border-radius: 2px;
        }

            .omt-title-top .omt-title-top-title {
                font-size: 12px;
                color: #e73d4a;
                font-weight: 600;
            }

            .omt-title-top .omt-title-top-p {
                font-size: 12px;
                color: #e73d4a;
            }

        .omt-title-bot {
            padding: 20px;
            /*background-color: #faeaa9;*/
            margin: 15px 20px;
            border-radius: 2px;
        }

            .omt-title-bot .omt-title-top-p {
                margin: 0;
                font-size: 12px;
                font-weight: 600;
            }

        .header-th {
            width: 25px;
        }

        .thead-omt {
            background-color: #3598dc;
            color: white;
            text-align: center;
        }

        .table thead th {
            vertical-align: middle;
        }

        .table-omt__icon {
            font-size: 50px;
            padding-left: 23px;
            cursor: pointer;
        }

        .btn__search {
            top: 20px;
            position: relative;
        }
    </style>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

