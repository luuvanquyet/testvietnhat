﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_HocTap_module_QuanLyLop_DiemDanh_LichSu : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    public int STT = 1;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["_id"] = 0;
            var listNV = from l in db.tbLops
                         join gvtl in db.tbGiaoVienTrongLops on l.lop_id equals gvtl.lop_id
                         select l;
            ddlLop.Items.Clear();
            ddlLop.Items.Insert(0, "Chọn Lớp");
            ddlLop.AppendDataBoundItems = true;
            ddlLop.DataTextField = "lop_name";
            ddlLop.DataValueField = "lop_id";
            ddlLop.DataSource = listNV;
            ddlLop.DataBind();
        }
    }
    private void loadData()
    {
        //try
        //{

        if (ddlLop.SelectedValue == "0" || ddlLop.SelectedValue == "Chọn Lớp")
        {
            alert.alert_Error(Page, "vui lòng chọn Lớp", "");
        }
        else if (txtTuNgay.Value == "")
        {
            alert.alert_Error(Page, "vui lòng chọn ngày bắt đầu tìm kiếm", "");
        }
        else if (txtDenNgay.Value == "")
        {
            alert.alert_Error(Page, "vui lòng chọn ngày kết thúc tìm kiếm", "");
        }
        else
        {
            // load data đổ vào var danh sách
            var getData = from l in db.tbLops
                          join hstl in db.tbHocSinhTrongLops on l.lop_id equals hstl.lop_id
                          join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                          join d in db.tbDiemDanh_HocSinhs on hstl.hstl_id equals d.hstl_id
                          where l.lop_id == Convert.ToInt32(ddlLop.SelectedValue)
                          && d.diemdanh_ngay >= Convert.ToDateTime(txtTuNgay.Value)
                          && d.diemdanh_ngay <= Convert.ToDateTime(txtDenNgay.Value)
                          group hs by hs.hocsinh_id into g
                          select new
                          {
                              //STT = STT + 1,
                              g.Key,
                              hocsinh_name = g.First().hocsinh_name,
                              hocsinh_id = g.Key,
                              countkhongphep = (from d in db.tbDiemDanh_HocSinhs
                                                join hstl in db.tbHocSinhTrongLops on d.hstl_id equals hstl.hstl_id
                                                join l in db.tbLops on hstl.lop_id equals l.lop_id
                                                join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                                                where l.lop_id == Convert.ToInt32(ddlLop.SelectedValue)
                                                && d.diemdanh_ngay >= Convert.ToDateTime(txtTuNgay.Value)
                                                && d.diemdanh_ngay <= Convert.ToDateTime(txtDenNgay.Value)
                                                && d.tinhtrang == 4
                                                && hs.hocsinh_id == g.Key
                                                select d).Count(),
                              countcophep = (from d in db.tbDiemDanh_HocSinhs
                                             join hstl in db.tbHocSinhTrongLops on d.hstl_id equals hstl.hstl_id
                                             join l in db.tbLops on hstl.lop_id equals l.lop_id
                                             join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                                             where l.lop_id == Convert.ToInt32(ddlLop.SelectedValue)
                                                && d.diemdanh_ngay >= Convert.ToDateTime(txtTuNgay.Value)
                                                && d.diemdanh_ngay <= Convert.ToDateTime(txtDenNgay.Value)
                                                && d.tinhtrang == 3
                                                && hs.hocsinh_id == g.Key
                                             select d).Count(),
                                             tong= (from d in db.tbDiemDanh_HocSinhs
                                                    join hstl in db.tbHocSinhTrongLops on d.hstl_id equals hstl.hstl_id
                                                    join l in db.tbLops on hstl.lop_id equals l.lop_id
                                                    join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                                                    where l.lop_id == Convert.ToInt32(ddlLop.SelectedValue)
                                                    && d.diemdanh_ngay >= Convert.ToDateTime(txtTuNgay.Value)
                                                    && d.diemdanh_ngay <= Convert.ToDateTime(txtDenNgay.Value)
                                                    && d.tinhtrang >=3
                                                    && hs.hocsinh_id == g.Key
                                                    select d).Count()

                              //count = (Convert.ToInt32(countkhongphep) + Convert.ToInt32(countcophep))+"",
                              //hs.hocsinh_name,
                              //hstl.hstl_id,
                              //d.diemdanh_ngay,
                              //ghichusuatan = d.ghichusuatan == 1 ? "Có ăn trưa" : "Không có ăn trưa",
                              //d.diemdanh_lydo,
                              //tinhtrang = d.tinhtrang == 1 ? "Đúng giờ" : d.tinhtrang == 2 ? "Đi muộn" : d.tinhtrang == 3 ? "Vắng có phép" : "Vắng không phép"
                          };
            // đẩy dữ liệu vào gridivew
            rpLop.DataSource = getData;
            rpLop.DataBind();
            //var getDetail = from d in db.tbDiemDanh_HocSinhs
            //                join hstl in db.tbHocSinhTrongLops on d.hstl_id equals hstl.hstl_id
            //                join l in db.tbLops on hstl.lop_id equals l.lop_id
            //                join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
            //                where l.lop_id == Convert.ToInt32(ddlLop.SelectedValue)
            //                && d.diemdanh_ngay >= Convert.ToDateTime(txtTuNgay.Value)
            //                && d.diemdanh_ngay <= Convert.ToDateTime(txtDenNgay.Value)
            //                //&& d.tinhtrang == 3 || d.tinhtrang == 4
            //                && hs.hocsinh_id == (from s in getData
            //                                    where hs.hocsinh_id == s.Key
            //                                    select s).First().Key
            //                select new
            //                {
            //                    hs.hocsinh_id,
            //                    hs.hocsinh_name,
            //                    d.diemdanh_ngay,
            //                    tinhtrang = d.tinhtrang == 3 ? "Vắng có phép" : "Vắng không phép"

            //                };
            var getDetail = from hs in db.tbHocSinhs
                            join x in getData on hs.hocsinh_id equals x.Key
                            select hs;
            rpPopup.DataSource = getDetail;
            rpPopup.DataBind();
        }
        //}
        //catch (Exception ex)
        //{
        //    alert.alert_Error(Page, "Đã xảy ra lỗi, vui lòng liên hệ IT", "");
        //}
    }

    protected void btnSearch_ServerClick(object sender, EventArgs e)
    {
        loadData();
    }

    protected void rpPopup_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Repeater rpChiTietVangHoc = e.Item.FindControl("rpChiTietVangHoc") as Repeater;
        int _id = int.Parse(DataBinder.Eval(e.Item.DataItem, "hocsinh_id").ToString());
        var getDetail = from d in db.tbDiemDanh_HocSinhs
                        join hstl in db.tbHocSinhTrongLops on d.hstl_id equals hstl.hstl_id
                        //join l in db.tbLops on hstl.lop_id equals l.lop_id
                        join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                        where hstl.lop_id == Convert.ToInt32(ddlLop.SelectedValue)
                        && d.diemdanh_ngay >= Convert.ToDateTime(txtTuNgay.Value)
                        && d.diemdanh_ngay <= Convert.ToDateTime(txtDenNgay.Value)
                        && hs.hocsinh_id == _id
                        && d.tinhtrang >2
                        select new
                        {
                            hs.hocsinh_id,
                            hs.hocsinh_name,
                            d.diemdanh_ngay,
                            tinhtrang = d.tinhtrang == 3 ? "Vắng có phép" : "Vắng không phép"
                        };
        rpChiTietVangHoc.DataSource = getDetail;
        rpChiTietVangHoc.DataBind();
    }
}