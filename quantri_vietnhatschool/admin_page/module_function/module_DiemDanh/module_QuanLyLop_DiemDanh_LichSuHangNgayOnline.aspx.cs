﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_HocTap_module_QuanLyLop_DiemDanh_LichSuHangNgayOnline : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    public int STT = 1;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["_id"] = 0;
            var listNV = from l in db.tbLops
                         join gvtl in db.tbGiaoVienTrongLops on l.lop_id equals gvtl.lop_id
                         select l;
            ddlLop.Items.Clear();
            ddlLop.Items.Insert(0, "Chọn Lớp");
            ddlLop.AppendDataBoundItems = true;
            ddlLop.DataTextField = "lop_name";
            ddlLop.DataValueField = "lop_id";
            ddlLop.DataSource = listNV;
            ddlLop.DataBind();
        }
    }
    private void loadData()
    {
        if (ddlLop.SelectedValue == "0" || ddlLop.SelectedValue == "Chọn Lớp")
        {
            alert.alert_Error(Page, "vui lòng chọn Lớp", "");
        }
        else
        {
            // load data đổ vào var danh sách
            var getData = from l in db.tbLops
                          join hstl in db.tbHocSinhTrongLops on l.lop_id equals hstl.lop_id
                          join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                          join d in db.tbDiemDanh_HocOnlines on hstl.hstl_id equals d.hstl_id
                          join m in db.tbMonHocs on d.monhoc_id equals m.monhoc_id
                          where l.lop_id == Convert.ToInt32(ddlLop.SelectedValue)
                          && d.diemdanhhoconline_ngay >= Convert.ToDateTime(txtTuNgay.Value)
                          && d.diemdanhhoconline_ngay <= Convert.ToDateTime(txtDenNgay.Value)
                          select new
                          {
                              STT = STT + 1,
                              l.lop_name,
                              m.monhoc_name,
                              hs.hocsinh_name,
                              hstl.hstl_id,
                              d.diemdanhhoconline_ngay,
                              d.diemdanhhoconline_lydo,
                              tinhtrang = d.diemdanhhoconline_tinhtrang == 1 ? "Đúng giờ" : d.diemdanhhoconline_tinhtrang == 2 ? "Vắng có phép" : "Vắng không phép"
                          };
            // đẩy dữ liệu vào gridivew
            rpLop.DataSource = getData;
            rpLop.DataBind();
        }
    }

    protected void btnSearch_ServerClick(object sender, EventArgs e)
    {
        loadData();
    }
}