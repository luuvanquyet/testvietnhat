﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_QuanLyLop_DiemDanh_Thongke.aspx.cs" Inherits="admin_page_module_function_module_HocTap_module_QuanLyLop_DiemDanh_Thongke" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="main-omt">
        <div class="omt-header">
            <i class="fa fa-user omt__icon" aria-hidden="true"></i>
            <h4 class="header-title">Thống kê điểm Danh</h4>
        </div>
        <%--  <div class="omt-top">
        </div>--%>
        <div class="container">
            <a class="omt-search" href="#" style="text-decoration: none" id="btnSeachHomNay" runat="server" onserverclick="btnSeachHomnay_ServerClick">
                <div class="basicBox">
                    <p>HÔM NAY</p>
                    <p><%=soluonghomnay %></p>
                </div>
            </a>
             <a class="omt-search" href="#" style="text-decoration: none" id="A3" runat="server" onserverclick="btnSearchTuanNay_ServerClick">
                <div class="basicBox">
                    <p>TUẦN NÀY</p>
                    <p><%=soluongtuannay %></p>
                </div>
            </a>
            <a class="omt-search" href="#" style="text-decoration: none" id="btnSearchThangNay" runat="server" onserverclick="btnSearchThangNay_ServerClick">
                <div class="basicBox">
                    <p>THÁNG NÀY</p>
                    <p><%=soluongthangnay %></p>

                </div>
            </a>
        </div>
        <%--<div class=" container form-group row">
            <div class="col-3">
                Chọn lớp
                 <asp:DropDownList ID="ddlLop" runat="server" Width="90%" class="form-control"></asp:DropDownList>
            </div>
            <div class="col-3">
                Từ ngày:
            <input type="date" id="txtTuNgay" runat="server" class="form-control" style="width: 90%" />
            </div>
            <div class="col-3">
                Đến ngày:
            <input type="date" id="txtDenNgay" runat="server" class="form-control" style="width: 90%" />
            </div>
            <div>
                <a href="#" id="btnSearch" runat="server" class="btn btn-primary" onserverclick="btnSearch_ServerClick">Search</a>
            </div>
        </div>--%>

        <div class="table-omt omt-title-bot">
            <table class="table table-bordered">
                <thead class="thead-omt">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Lớp</th>
                        <th scope="col">Họ và Tên</th>
                        <th scope="col">tình trạng</th>
                        <th scope="col">Ghi chú suất ăn</th>
                        <th scope="col">Ghi chú</th>
                        <th scope="col">Ngày</th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater ID="rpLop" runat="server">
                        <ItemTemplate>
                            <tr>
                                <th><%=STT++ %></th>
                                <td><%#Eval("lop_name") %></td>
                                <td><%#Eval("hocsinh_name") %></td>
                                <td><%#Eval("tinhtrang") %></td>
                                <td><%#Eval("ghichusuatan") %></td>
                                <td><%#Eval("diemdanh_lydo") %></td>
                                <td><%#Eval("diemdanh_ngay", "{0: dd/MM/yyyy}") %></td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>

                </tbody>
            </table>

        </div>
    </div>
    <style>
        .container {
            display: flex;
            justify-content: space-around;
        }

        .omt-search {
            margin-left: 60px;
        }

        .basicBox {
            width: 150px;
            height: 80px;
            background-color: #737171;
            margin-top: 20px;
            font-weight: bold;
            font-family: arial;
            font-size: 20px;
            padding: 10px;
            float: left;
            -webkit-transition: -webkit-transform 1s, opacity 1s, background 1s, width 1s, height 1s,font-size 1s;
            -webkit-border-radius: 5px;
            -o-transition-property: width, height, -o-transform, background, font-size, opacity;
            -o-transition-duration: 1s,1s,1s,1s,1s,1s;
            -moz-transition-property: width, height, -o-transform, background, font-size, opacity;
            -moz-transition-duration: 1s,1s,1s,1s,1s,1s;
            transition-property: width, height, transform, background, font-size, opacity;
            transition-duration: 1s, 1s, 1s, 1s, 1s, 1s;
            border-radius: 5px;
            opacity: 0.7;
            box-shadow:  2px 3px #c5c5ba;
        }

            .basicBox p {
                font-size: 15px;
                text-align: center;
                margin: 0;
                padding-top: 10px;
                color: white;
                border-radius: 3px;
            }

            .basicBox:hover {
                opacity: 1;
                background: #12a583;
                font-size: 40px;
            }







        .main-omt {
            border: 1px solid #32c5d2;
            background-color: #fff;
        }

            .main-omt .omt-header {
                background-color: #32c5d2;
                padding: 4px 7px;
                display: flex;
            }

        .omt-header .header-title {
            padding: 10px 10px;
            color: white;
        }

        .omt-header .omt__icon {
            font-size: 35px;
            padding: 5px 10px;
            color: white;
        }

        .omt-top {
            display: flex;
            padding: 0 20px;
        }

        .form-omt {
            width: 10%;
            height: 38px;
            margin-right: 15px;
            margin-top: 10px;
        }

        .form-group__omt {
            margin-top: 10px;
            width: 35%;
        }

        .omt-title-top {
            padding: 20px;
            background-color: #fbe1e3;
            margin: 0 20px;
            border-radius: 2px;
        }

            .omt-title-top .omt-title-top-title {
                font-size: 12px;
                color: #e73d4a;
                font-weight: 600;
            }

            .omt-title-top .omt-title-top-p {
                font-size: 12px;
                color: #e73d4a;
            }

        .omt-title-bot {
            padding: 20px;
            /*background-color: #faeaa9;*/
            margin: 15px 20px;
            border-radius: 2px;
        }

            .omt-title-bot .omt-title-top-p {
                margin: 0;
                font-size: 12px;
                font-weight: 600;
            }

        .header-th {
            width: 25px;
        }

        .thead-omt {
            background-color: #3598dc;
            color: white;
            text-align: center;
        }

        .table thead th {
            vertical-align: middle;
        }

        .table-omt__icon {
            font-size: 50px;
            padding-left: 23px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

