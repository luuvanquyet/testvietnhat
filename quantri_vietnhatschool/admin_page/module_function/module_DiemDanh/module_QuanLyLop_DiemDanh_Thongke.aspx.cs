﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_HocTap_module_QuanLyLop_DiemDanh_Thongke : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    public int soluonghomnay, soluongtuannay, soluongthangnay;
    public int STT = 1;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {


        }
        soluonghomnay = (from ds in db.tbDiemDanh_HocSinhs
                         where ds.diemdanh_ngay.Value.Day == DateTime.Now.Day
                         && ds.diemdanh_ngay.Value.Month == DateTime.Now.Month
                         && ds.diemdanh_ngay.Value.Year == DateTime.Now.Year
                         select ds).Count();

        //khởi tạo biến ngày thứ 2 và thứ 7
        DateTime dayThu2 = DateTime.Now;
        DateTime dayThu7 = DateTime.Now;
        //lấy ra thứ hiện tại
        var dayOfWeek = DateTime.Now.DayOfWeek;
        if (dayOfWeek == DayOfWeek.Sunday)
        {
            //nếu là chủ nhật thì ngày t2 sẽ là giảm đi 6
            //và ngày thứ 7 sẽ cộng thêm 5
            dayThu2 = dayThu2.AddDays(-6);
            dayThu7 = dayThu2.AddDays(5);
        }
        else
        {
            // nếu không phải thứ 2 thì lùi ngày lại cho đến thứ 2  
            int offset = dayOfWeek - DayOfWeek.Monday;
            dayThu2 = dayThu2.AddDays(-offset);
            dayThu7 = dayThu2.AddDays(5);
        }
        //lấy data đổ và danh sách từ t2 đến t7;
        soluongtuannay = (from l in db.tbLops
                          join hstl in db.tbHocSinhTrongLops on l.lop_id equals hstl.lop_id
                          join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                          join d in db.tbDiemDanh_HocSinhs on hstl.hstl_id equals d.hstl_id
                          where d.diemdanh_ngay >= dayThu2 && d.diemdanh_ngay <= dayThu7
                          select new
                          {
                              STT = STT + 1,
                              hs.hocsinh_name,
                              hstl.hstl_id,
                              d.diemdanh_ngay,
                              ghichusuatan = d.ghichusuatan == 1 ? "Có ăn trưa" : "Không có ăn trưa",
                              d.diemdanh_lydo,
                              tinhtrang = d.tinhtrang == 1 ? "Đúng giờ" : d.tinhtrang == 2 ? "Đi muộn" : d.tinhtrang == 3 ? "Vắng có phép" : "Vắng không phép"

                          }).Count();
        soluongthangnay = (from ds in db.tbDiemDanh_HocSinhs
                           where ds.diemdanh_ngay.Value.Month == Convert.ToDateTime(DateTime.Now).Month
                           select ds).Count();
    }

    //load data theo tuần hiện tại

    private void loadDataTuan()
    {
        //khởi tạo biến ngày thứ 2 và thứ 7
        DateTime dayThu2 = DateTime.Now;
        DateTime dayThu7 = DateTime.Now;
        //lấy ra thứ hiện tại
        var dayOfWeek = DateTime.Now.DayOfWeek;
        if (dayOfWeek == DayOfWeek.Sunday)
        {
            //nếu là chủ nhật thì ngày t2 sẽ là giảm đi 6
            //và ngày thứ 7 sẽ cộng thêm 5
            dayThu2 = dayThu2.AddDays(-6);
            dayThu7 = dayThu2.AddDays(5);
        }
        else
        {
            // nếu không phải thứ 2 thì lùi ngày lại cho đến thứ 2  
            int offset = dayOfWeek - DayOfWeek.Monday;
            dayThu2 = dayThu2.AddDays(-offset);
            dayThu7 = dayThu2.AddDays(5);
        }
        //lấy data đổ và danh sách từ t2 đến t7;
        var getData = from l in db.tbLops
                      join hstl in db.tbHocSinhTrongLops on l.lop_id equals hstl.lop_id
                      join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                      join d in db.tbDiemDanh_HocSinhs on hstl.hstl_id equals d.hstl_id
                      where d.diemdanh_ngay >= dayThu2 && d.diemdanh_ngay <= dayThu7
                      select new
                      {
                          STT = STT + 1,
                          l.lop_name,
                          hs.hocsinh_name,
                          hstl.hstl_id,
                          d.diemdanh_ngay,
                          ghichusuatan = d.ghichusuatan == 1 ? "Có ăn trưa" : "Không có ăn trưa",
                          d.diemdanh_lydo,
                          tinhtrang = d.tinhtrang == 1 ? "Đúng giờ" : d.tinhtrang == 2 ? "Đi muộn" : d.tinhtrang == 3 ? "Vắng có phép" : "Vắng không phép"

                      };
        // đẩy dữ liệu vào gridivew
        rpLop.DataSource = getData;
        rpLop.DataBind();
    }


    protected void btnSeachHomnay_ServerClick(object sender, EventArgs e)
    {


        // load data đổ vào var danh sách
        var getData = from l in db.tbLops
                      join hstl in db.tbHocSinhTrongLops on l.lop_id equals hstl.lop_id
                      join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                      join d in db.tbDiemDanh_HocSinhs on hstl.hstl_id equals d.hstl_id
                      where d.diemdanh_ngay.Value.Day == DateTime.Now.Day
                       && d.diemdanh_ngay.Value.Month ==DateTime.Now.Month
                       && d.diemdanh_ngay.Value.Year == DateTime.Now.Year
                      select new
                      {
                          l.lop_name,
                          STT = STT + 1,
                          hs.hocsinh_name,
                          hstl.hstl_id,
                          d.diemdanh_ngay,
                          ghichusuatan = d.ghichusuatan == 1 ? "Có ăn trưa" : "Không có ăn trưa",
                          d.diemdanh_lydo,
                          tinhtrang = d.tinhtrang == 1 ? "Đúng giờ" : d.tinhtrang == 2 ? "Đi muộn" : d.tinhtrang == 3 ? "Vắng có phép" : "Vắng không phép"

                      };
        // đẩy dữ liệu vào gridivew
        rpLop.DataSource = getData;
        rpLop.DataBind();


    }
    protected void btnSearchTuanNay_ServerClick(object sender, EventArgs e)
    {
        loadDataTuan();
    }
    protected void btnSearchThangNay_ServerClick(object sender, EventArgs e)
    {
        var getData = from l in db.tbLops
                      join hstl in db.tbHocSinhTrongLops on l.lop_id equals hstl.lop_id
                      join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                      join d in db.tbDiemDanh_HocSinhs on hstl.hstl_id equals d.hstl_id
                      where d.diemdanh_ngay.Value.Month == DateTime.Now.Month
                       && d.diemdanh_ngay.Value.Year ==DateTime.Now.Year
                      select new
                      {
                          STT = STT + 1,
                          l.lop_name,
                          hs.hocsinh_name,
                          hstl.hstl_id,
                          d.diemdanh_ngay,
                          ghichusuatan = d.ghichusuatan == 1 ? "Có ăn trưa" : "Không có ăn trưa",
                          d.diemdanh_lydo,
                          tinhtrang = d.tinhtrang == 1 ? "Đúng giờ" : d.tinhtrang == 2 ? "Đi muộn" : d.tinhtrang == 3 ? "Vắng có phép" : "Vắng không phép"

                      };
        // đẩy dữ liệu vào gridivew
        rpLop.DataSource = getData;
        rpLop.DataBind();
    }
}