﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_HocTap_module_QuanLyLop_DiemDanh_Thongke_AnSang : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    public int soluongtuannay, soluongthangnay;
    public int STT = 1;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            lblTongSo.Text = (from asang in db.tbQuanLyLop_DiemDanh_DanhSachHocSinh_AnSangs
                              where asang.ansang_tinhtrang == null
                              select asang).Count() + "";
            lblHomNay.Text = (from asang in db.tbDiemDanh_HocSinhs
                              where asang.diemdanh_ansang == 2
                              && asang.diemdanh_ngay.Value.Date == Convert.ToDateTime(DateTime.Now).Date
                              && asang.diemdanh_ngay.Value.Month == Convert.ToDateTime(DateTime.Now).Month
                              && asang.diemdanh_ngay.Value.Year == Convert.ToDateTime(DateTime.Now).Year
                              select asang).Count() + "";
            var homnay = (from asang in db.tbDiemDanh_HocSinhs
                          where asang.diemdanh_ansang == 2
                          && asang.diemdanh_ngay.Value.Date == Convert.ToDateTime(DateTime.Now).Date
                          && asang.diemdanh_ngay.Value.Month == Convert.ToDateTime(DateTime.Now).Month
                          && asang.diemdanh_ngay.Value.Year == Convert.ToDateTime(DateTime.Now).Year
                          select asang);
            var homqua = (from asang in db.tbDiemDanh_HocSinhs
                          where asang.diemdanh_ansang == 2
                          && asang.diemdanh_ngay.Value.Date == Convert.ToDateTime(DateTime.Now).Date.AddDays(-1)
                          && asang.diemdanh_ngay.Value.Month == Convert.ToDateTime(DateTime.Now).Month
                          && asang.diemdanh_ngay.Value.Year == Convert.ToDateTime(DateTime.Now).Year
                          select asang);
            lbl2NgayLienTiep.Text = (from a in homnay
                                     join b in homqua on a.hstl_id equals b.hstl_id
                                     select a).Count() + "";
            var listngayhomqua = from asang in db.tbDiemDanh_HocSinhs
                                 where asang.diemdanh_ansang == 2
                                 group asang by asang.hstl_id into playerGroup
                                 select new
                                 {
                                     Team = playerGroup.Key,
                                     Count = playerGroup.Count(),
                                 };
            var listngayhomnay = from asang in db.tbDiemDanh_HocSinhs
                                 where asang.diemdanh_ansang == 2
                                 && asang.diemdanh_ngay.Value.Day == Convert.ToDateTime(DateTime.Now).Day
                                 && asang.diemdanh_ngay.Value.Month == Convert.ToDateTime(DateTime.Now).Month
                                 && asang.diemdanh_ngay.Value.Year == Convert.ToDateTime(DateTime.Now).Year
                                 select asang;
        }
    }

    //load data theo tuần hiện tại




    //protected void btnSeachHomnay_ServerClick(object sender, EventArgs e)
    //{
    //    // load data đổ vào var danh sách
    //    var getData = from l in db.tbLops
    //                  join hstl in db.tbHocSinhTrongLops on l.lop_id equals hstl.lop_id
    //                  join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
    //                  join d in db.tbDiemDanh_HocSinhs on hstl.hstl_id equals d.hstl_id
    //                  where d.diemdanh_ngay.Value.Day == Convert.ToDateTime(DateTime.Now).Day
    //                   && d.diemdanh_ngay.Value.Month == Convert.ToDateTime(DateTime.Now).Month
    //                   && d.diemdanh_ngay.Value.Year == Convert.ToDateTime(DateTime.Now).Year
    //                   && d.diemdanh_ansang != null
    //                  orderby l.lop_position
    //                  select new
    //                  {
    //                      l.lop_name,
    //                      STT = STT + 1,
    //                      hs.hocsinh_name,
    //                      hstl.hstl_id,
    //                      d.diemdanh_ngay,
    //                      tinhtrang = d.diemdanh_ansang == 1 ? "Có ăn sáng" : "Không ăn sáng"
    //                  };
    //    // đẩy dữ liệu vào gridivew
    //    rpLop.DataSource = getData;
    //    rpLop.DataBind();


    //}

    protected void btnSearch_ServerClick(object sender, EventArgs e)
    {
        var getDanhSachHocSinhAnSang = from ds in db.tbQuanLyLop_DiemDanh_DanhSachHocSinh_AnSangs
                                       join hstl in db.tbHocSinhTrongLops on ds.hstl_id equals hstl.hstl_id
                                       join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                                       join l in db.tbLops on hstl.lop_id equals l.lop_id
                                       orderby l.lop_position
                                       select new
                                       {

                                           l.lop_name,
                                           hs.hocsinh_name,

                                       };
        rpLop.DataSource = getDanhSachHocSinhAnSang;
        rpLop.DataBind();
    }

    protected void btnHomNay_ServerClick(object sender, EventArgs e)
    {
        var getDanhSachHocSinhAnSang = from ds in db.tbQuanLyLop_DiemDanh_DanhSachHocSinh_AnSangs
                                       join hstl in db.tbHocSinhTrongLops on ds.hstl_id equals hstl.hstl_id
                                       join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                                       join l in db.tbLops on hstl.lop_id equals l.lop_id
                                       join ans in db.tbDiemDanh_HocSinhs on hstl.hstl_id equals ans.hstl_id
                                       where ans.diemdanh_ngay.Value.Date == DateTime.Now.Date
                                       && ans.diemdanh_ngay.Value.Month == DateTime.Now.Month
                                       && ans.diemdanh_ngay.Value.Year == DateTime.Now.Year
                                       && ans.diemdanh_ansang == 2
                                       orderby l.lop_position
                                       select new
                                       {

                                           l.lop_name,
                                           hs.hocsinh_name,

                                       };
        rpLop.DataSource = getDanhSachHocSinhAnSang;
        rpLop.DataBind();
    }
    protected void btnLienTiep_ServerClick(object sender, EventArgs e)
    {
        var homnay = (from asang in db.tbDiemDanh_HocSinhs
                      where asang.diemdanh_ansang == 2
                      && asang.diemdanh_ngay.Value.Date == Convert.ToDateTime(DateTime.Now).Date
                      && asang.diemdanh_ngay.Value.Month == Convert.ToDateTime(DateTime.Now).Month
                      && asang.diemdanh_ngay.Value.Year == Convert.ToDateTime(DateTime.Now).Year
                      select asang);
        var homqua = (from asang in db.tbDiemDanh_HocSinhs
                      where asang.diemdanh_ansang == 2
                      && asang.diemdanh_ngay.Value.Date == Convert.ToDateTime(DateTime.Now).Date.AddDays(-1)
                      && asang.diemdanh_ngay.Value.Month == Convert.ToDateTime(DateTime.Now).Month
                      && asang.diemdanh_ngay.Value.Year == Convert.ToDateTime(DateTime.Now).Year
                      select asang);
        var getLienTiep = from a in homnay
                          join b in homqua on a.hstl_id equals b.hstl_id
                          join hstl in db.tbHocSinhTrongLops on a.hstl_id equals hstl.hstl_id
                          join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                          join l in db.tbLops on hstl.lop_id equals l.lop_id
                          orderby l.lop_position
                          select new
                          {
                              l.lop_name,
                              hs.hocsinh_name,
                          };
        rpLop.DataSource = getLienTiep;
        rpLop.DataBind();
    }
}