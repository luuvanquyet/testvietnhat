﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_HocTap_module_QuanLyLop_DiemDanh_Thongke_DiemDanh : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    public int soluongtuannay, soluongthangnay;
    public int STT = 1;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            var check = from dd in db.tbDiemDanh_HocSinhs
                        where dd.tinhtrang >= 3
                        && dd.diemdanh_ngay.Value.Date == Convert.ToDateTime(DateTime.Now).Date
                        && dd.diemdanh_ngay.Value.Month == Convert.ToDateTime(DateTime.Now).Month
                        && dd.diemdanh_ngay.Value.Year == Convert.ToDateTime(DateTime.Now).Year
                        select dd;
            lblHomNay.Text = (from dd in db.tbDiemDanh_HocSinhs
                              where dd.tinhtrang >= 3
                              && dd.diemdanh_ngay.Value.Date == Convert.ToDateTime(DateTime.Now).Date
                              && dd.diemdanh_ngay.Value.Month == Convert.ToDateTime(DateTime.Now).Month
                              && dd.diemdanh_ngay.Value.Year == Convert.ToDateTime(DateTime.Now).Year
                              select dd).Count() + "";
        }
    }

    //load data theo tuần hiện tại
    protected void btnHomNay_ServerClick(object sender, EventArgs e)
    {
        var getDanhSachHocSinhAnSang = from hstl in db.tbHocSinhTrongLops
                                       join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                                       join l in db.tbLops on hstl.lop_id equals l.lop_id
                                       join ans in db.tbDiemDanh_HocSinhs on hstl.hstl_id equals ans.hstl_id
                                       where ans.diemdanh_ngay.Value.Date == DateTime.Now.Date
                                       && ans.diemdanh_ngay.Value.Month == DateTime.Now.Month
                                       && ans.diemdanh_ngay.Value.Year == DateTime.Now.Year
                                       && ans.tinhtrang >= 3
                                       orderby l.lop_position
                                       select new
                                       {
                                           l.lop_name,
                                           hs.hocsinh_name,
                                           hsvang=1
                                       };
        rpLop.DataSource = getDanhSachHocSinhAnSang;
        rpLop.DataBind();
    }
    protected void btnLienTiep_ServerClick(object sender, EventArgs e)
    {
        int songhay = Convert.ToInt32(txtNgayVang.Value);

        var checkHocSinh = from hstl in db.tbHocSinhTrongLops
                           join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                           join ans in db.tbDiemDanh_HocSinhs on hstl.hstl_id equals ans.hstl_id
                           where ans.tinhtrang >= 3
                           group ans by ans.hstl_id into item
                           where item.Count()> songhay
                           select new
                           {
                               item.Key,
                               lop_name= (from l in db.tbLops 
                                         join hstl in db.tbHocSinhTrongLops on l.lop_id equals hstl.lop_id
                                         where hstl.hstl_id == item.Key select l.lop_name).First(),
                               hocsinh_name = (from hs in db.tbHocSinhs
                                               join hstl in db.tbHocSinhTrongLops on hs.hocsinh_id equals hstl.hocsinh_id
                                               where hstl.hstl_id == item.Key
                                               select hs.hocsinh_name).First(),
                               hsvang = item.Count()
                           };
     
        rpLop.DataSource = checkHocSinh;
        rpLop.DataBind();
    }

 
}