﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_WebSite_module_QuanLyLop_DiemDanh_TongHocSinh_TrongLop : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    public int STT = 1;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //var checkuserid = (from u in db.admin_Users where u.username_username == Request.Cookies["UserName"].Value select u).First();
            //Session["_id"] = 0;
            //var listNV = from l in db.tbLops
            //             join gvtl in db.tbGiaoVienTrongLops on l.lop_id equals gvtl.lop_id
            //             select l;
            //ddlLop.Items.Clear();
            //ddlLop.Items.Insert(0, "Chọn Lớp");
            //ddlLop.AppendDataBoundItems = true;
            //ddlLop.DataTextField = "lop_name";
            //ddlLop.DataValueField = "lop_id";
            //ddlLop.DataSource = listNV;
            //ddlLop.DataBind();
        }
        loadData();
    }
    private void loadData()
    {
        var checkNamHoc = (from nh in db.tbHoctap_NamHocs orderby nh.namhoc_id descending select nh).FirstOrDefault();
        //if (ddlLop.SelectedValue == "0" || ddlLop.SelectedValue == "Chọn Lớp")
        //{

        //}
        //else
        //{
        var checkuserid = (from u in db.admin_Users where u.username_username == Request.Cookies["UserName"].Value select u).First();
        var checkGiaoVienDayTrongLop = (from gvtl in db.tbGiaoVienTrongLops where gvtl.taikhoan_id == checkuserid.username_id select gvtl).FirstOrDefault();
        // load data đổ vào var danh sách
        var getData = from l in db.tbLops
                      join hstl in db.tbHocSinhTrongLops on l.lop_id equals hstl.lop_id
                      join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                      //join online in db.tbDanhSachHocOnLines on l.lop_id equals online.lop_id
                      where l.lop_id == Convert.ToInt32(checkGiaoVienDayTrongLop.lop_id) && hstl.namhoc_id == checkNamHoc.namhoc_id
                     orderby hstl.position
                      select new
                      {
                          STT = STT + 1,
                          hs.hocsinh_name,
                          hstl.hstl_id,
                          hstl.position
                      };
        var checkData1 = from hstl in db.tbHocSinhTrongLops
                         join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                         join online in db.tbDanhSachHocOnLines on hstl.hstl_id equals online.hstl_id
                         where online.lop_id == Convert.ToInt32(checkGiaoVienDayTrongLop.lop_id) && online.tinhtrang == 2
                         orderby hstl.position
                         select new
                         {
                             STT = STT + 1,
                             hs.hocsinh_name,
                             hstl.hstl_id,
                             hstl.position
                         };
        // đẩy dữ liệu vào gridivew
        var list = getData.Except(checkData1);
        rpLop.DataSource = list.OrderBy(x=>x.position);
        rpLop.DataBind();
        var checkData = from at in db.tbDanhSachHocOnLines
                        join hstl in db.tbHocSinhTrongLops on at.hstl_id equals hstl.hstl_id
                        where hstl.lop_id == Convert.ToInt32(checkGiaoVienDayTrongLop.lop_id)
                        select at;
        if (checkData.Count() > 0)
        {

        }
        else
        {
            // var checkuserid = (from u in db.admin_Users where u.username_username == Request.Cookies["UserName"].Value select u).First();

            foreach (var item in getData)
            {
                tbDanhSachHocOnLine insert = new tbDanhSachHocOnLine();
                insert.ngay = DateTime.Now;
                insert.tinhtrang = 1;
                insert.dshoconline_lydo = "";
                insert.hstl_id = item.hstl_id;
                insert.lop_id = Convert.ToInt32(checkGiaoVienDayTrongLop.lop_id);
                //insert.username_id = checkuserid.username_id;
                db.tbDanhSachHocOnLines.InsertOnSubmit(insert);
                try
                {
                    db.SubmitChanges();
                }
                catch
                {

                }
            }
        }
        //}
    }


    protected void btnThem_Click(object sender, EventArgs e)
    {

    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (txtSearch.Text != "")
        {
            var checkuserid = (from u in db.admin_Users where u.username_username == Request.Cookies["UserName"].Value select u).First();
            var checkGiaoVienDayTrongLop = (from gvtl in db.tbGiaoVienTrongLops where gvtl.taikhoan_id == checkuserid.username_id select gvtl).FirstOrDefault();
            var list = from hs in db.tbHocSinhs
                       join hstl in db.tbHocSinhTrongLops on hs.hocsinh_id equals hstl.hocsinh_id
                       where hs.hocsinh_name.Contains(txtSearch.Text) && hstl.lop_id == Convert.ToInt32(checkGiaoVienDayTrongLop.lop_id)
                       select new
                       {
                           STT = STT + 1,
                           hs.hocsinh_name,
                           hstl.hstl_id
                       };
            rpLop.DataSource = list;
            rpLop.DataBind();
        }
        else
        {
            alert.alert_Error(Page, "Vùi lòng nhập tên học sinh", "");
        }
    }
    protected void btnSave_ServerClick(object sender, EventArgs e)
    {
        if (txtMaHocSinh.Value != "")
        {
            cls_HocSinh cls = new cls_HocSinh();
            if (txtPhep.Value == "")
                txtPhep.Value = "1";
            tbDanhSachHocOnLine checkNgayAnSang = (from an in db.tbDanhSachHocOnLines
                                                   where an.hstl_id == Convert.ToInt32(txtMaHocSinh.Value)
                                                   select an).SingleOrDefault();
            if (checkNgayAnSang != null)
            {
                checkNgayAnSang.tinhtrang = Convert.ToInt16(txtPhep.Value);
                checkNgayAnSang.dshoconline_lydo = txtGhiChuHocSinh.Value;
                checkNgayAnSang.hstl_id = Convert.ToInt32(txtMaHocSinh.Value);
                db.SubmitChanges();
                txtMaHocSinh.Value = "";
                txtPhep.Value = "";
                txtGhiChuHocSinh.Value = "";
                alert.alert_Success(Page, "Hoàn thành", "");
            }
            //else
            //{
            //    if (cls.Linq_Insert_DanhSach(Convert.ToInt16(txtPhep.Value), Convert.ToInt16(txtSuatAn.Value), txtGhiChuHocSinh.Value, Convert.ToInt32(txtMaHocSinh.Value)))
            //    {
            //        alert.alert_Success(Page, "Hoàn thành", "");
            //        txtMaHocSinh.Value = "";
            //        txtPhep.Value = "";
            //        txtSuatAn.Value = "";
            //        txtGhiChuHocSinh.Value = "";
            //    }
            //    else
            //    {
            //        alert.alert_Error(Page, "Lỗi", "");
            //    }
            //}
        }
        else
        {
            alert.alert_Error(Page, "Lỗi", "");
        }
    }
}


