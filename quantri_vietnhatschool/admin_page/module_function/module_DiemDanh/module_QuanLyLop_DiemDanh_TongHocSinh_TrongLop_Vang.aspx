﻿<%@ Page Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_QuanLyLop_DiemDanh_TongHocSinh_TrongLop_Vang.aspx.cs" Inherits="admin_page_module_function_module_WebSite_module_QuanLyLop_DiemDanh_TongHocSinh_TrongLop_Vang" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v17.1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <script>
        function myLuu(id) {
            document.getElementById("<%=txtMaHocSinh.ClientID%>").value = id;
            // alert(document.getElementById("txtghiChu" + id).value)
            document.getElementById("<%=btnSave.ClientID%>").click();
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="main-omt">
        <div class="omt-header">
            <i class="fa fa-user omt__icon" aria-hidden="true"></i>
            <h4 class="header-title">Điểm Danh &nbsp; (<%=DateTime.Now %>)</h4>
        </div>
        <div class="card card-block">
            <div class="form-group row">
                <div class="form-group search">
                    <%--<div class="col-sm-3">
                        <asp:DropDownList ID="ddlLop" runat="server" AutoPostBack="true" CssClass="form-control"></asp:DropDownList>
                    </div>--%>
                    <%-- <div class="col-sm-3">
                        <asp:DropDownList ID="ddlMon" runat="server" AutoPostBack="true" CssClass="form-control"></asp:DropDownList>
                    </div>--%>
                    <div class="col-sm-3" <%--style="margin-left: 170px"--%>>
                        <asp:TextBox ID="txtSearch" runat="server" CssClass="form-control" autocomplete="off" placeholder="Họ và tên"></asp:TextBox>
                    </div>
                    <div class="col-sm-3">
                        <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" CssClass="btn btn-primary" Text="Tìm kiếm" />
                    </div>
                </div>
            </div>
            <asp:UpdatePanel ID="btnUpdate" runat="server">
                <ContentTemplate>
                    <div class="form-group table-responsive">
                        <table class="table table-bordered">
                            <thead class="thead-omt">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Họ và Tên</th>
                                    <th scope="col">Trạng thái</th>
                                    <th scope="col">Ghi chú</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="rpLop" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <th><%=STT++ %></th>
                                            <td><%#Eval("hocsinh_name") %></td>
                                            <td>
                                                <%#Eval("tinhtrang") %>
                                            </td>
                                            <td>
                                                <%#Eval("dshoconline_lydo") %></td>
                                            <td>
                                                <a href="#" class="btn btn-primary" id="btnLuu" onclick="myLuu(<%#Eval("hstl_id") %>)">đi học lại</a></td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </tbody>
                        </table>
                    </div>
                    <div style="display: none">

                        <input id="txtMaHocSinh" type="text" runat="server" />

                        <a href="#" id="btnSave" runat="server" onserverclick="btnSave_ServerClick"></a>
                    </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>

        <style>
            .main-omt {
                border: 1px solid #32c5d2;
                background-color: #fff;
            }

                .main-omt .omt-header {
                    background-color: #32c5d2;
                    padding: 4px 7px;
                    display: flex;
                }

            .omt-header .header-title {
                padding: 10px 10px;
                color: white;
            }

            .omt-header .omt__icon {
                font-size: 35px;
                padding: 5px 10px;
                color: white;
            }
        </style>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

