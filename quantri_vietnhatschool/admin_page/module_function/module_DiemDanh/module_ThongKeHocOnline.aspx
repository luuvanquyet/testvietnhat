﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_ThongKeHocOnline.aspx.cs" Inherits="admin_page_module_function_module_DiemDanh_module_ThongKeHocOnline" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <style>
        .vn-khoi {
            margin-top: 40px;
            background-color: #a9a8a8;
            padding: 15px 10px;
            margin-left: 6px;
        }

            .vn-khoi a {
                color: white;
                font-size: 14px;
                font-weight: bold;
                text-decoration: none;
                text-align: center;
            }

                .vn-khoi a:hover {
                    background-color: red;
                    color: white;
                    text-decoration: none;
                    /*padding: 15px 10px;*/
                    display: block;
                }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">

    <div class="col-12">
        <input id="dteNgay" type="date" runat="server" />
        <asp:Button ID="btnXem" runat="server" Text="Xem" CssClass="btn btn-primary" />
    </div>
    <%-- <div class="col-1 vn-khoi">
        <a href="#">
            <div>Lớp 1/1</div>
            <div>Toán</div>
            <div>20/28</div>
        </a>
    </div>
   <div class="col-1 vn-khoi">
        <a href="#">
            <div>Lớp 1/2</div>
            <div>20/28</div>
        </a>
    </div>
    <div class="col-1 vn-khoi">
        <a href="#">
            <div>Lớp 1/3</div>
            <div>20/28</div>
        </a>
    </div>
    <div class="col-1 vn-khoi">
        <a href="#">
            <div>Lớp 1/4</div>
            <div>20/28</div>
        </a>
    </div>
    <div class="col-1 vn-khoi">
        <a href="#">
            <div>Lớp 2/1</div>
            <div>20/28</div>
        </a>
    </div>
    <div class="col-1 vn-khoi">
        <a href="#">
            <div>Lớp 2/2</div>
            <div>20/28</div>
        </a>
    </div>
    <div class="col-1 vn-khoi">
        <a href="#">
            <div>Lớp 2/3</div>
            <div>20/28</div>
        </a>
    </div>
    <div class="col-1 vn-khoi">
        <a href="#">
            <div>Lớp 2/4</div>
            <div>20/28</div>
        </a>
    </div>
    <div class="col-1 vn-khoi">
        <a href="#">
            <div>Lớp 3/1</div>
            <div>20/28</div>
        </a>
    </div>
    <div class="col-1 vn-khoi">
        <a href="#">
            <div>Lớp 3/2</div>
            <div>20/28</div>
        </a>
    </div>
    <div class="col-1 vn-khoi">
        <a href="#">
            <div>Lớp 3/3</div>
            <div>20/28</div>
        </a>
    </div>
    <div class="col-1 vn-khoi">
        <a href="#">
            <div>Lớp 3/4</div>
            <div>20/28</div>
        </a>
    </div>
    <div class="col-1 vn-khoi">
        <a href="#">
            <div>Lớp 3/5</div>
            <div>20/28</div>
        </a>
    </div>
    <div class="col-1 vn-khoi">
        <a href="#">
            <div>Lớp 4/1</div>
            <div>20/28</div>
        </a>
    </div>
    <div class="col-1 vn-khoi">
        <a href="#">
            <div>Lớp 4/2</div>
            <div>20/28</div>
        </a>
    </div>
    <div class="col-1 vn-khoi">
        <a href="#">
            <div>Lớp 5/1</div>
            <div>20/28</div>
        </a>
    </div>
    <div class="col-1 vn-khoi">
        <a href="#">
            <div>Lớp 6/1</div>
            <div>20/28</div>
        </a>
    </div>
    <div class="col-1 vn-khoi">
        <a href="#">
            <div>Lớp 7/1</div>
            <div>20/28</div>
        </a>
    </div>
    <div class="col-1 vn-khoi">
        <a href="#">
            <div>Lớp 7/2</div>
            <div>20/28</div>
        </a>
    </div>
    <div class="col-1 vn-khoi">
        <a href="#">
            <div>Lớp 8/1</div>
            <div>20/28</div>
        </a>
    </div>
    <div class="col-1 vn-khoi">
        <a href="#">
            <div>Lớp 10/1</div>
            <div>20/28</div>
        </a>
    </div>
    <div class="col-1 vn-khoi">
        <a href="#">
            <div>Lớp 11/1</div>
            <div>20/28</div>
        </a>
    </div>--%>
    <br />
    <div>
        <table class="table" style="background-color: white">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Lớp</th>
                     <th scope="col">Môn</th>
                    <th scope="col">Họ tên</th>
                    <th scope="col">Lý do</th>
                </tr>
            </thead>
            <tbody>
                <asp:Repeater ID="rpDanhSach" runat="server">
                    <ItemTemplate>
                        <tr>
                            <th scope="row" style="text-align: center"><%=STT++ %></th>
                            <td><%#Eval("lop_name") %></td>
                             <td><%#Eval("monhoc_name") %></td>
                            <td><%#Eval("hocsinh_name") %></td>
                            <th scope="col"><%#Eval("dshoconline_lydo") %></th>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

