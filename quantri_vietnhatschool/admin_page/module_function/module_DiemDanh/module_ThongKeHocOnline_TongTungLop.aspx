﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_ThongKeHocOnline_TongTungLop.aspx.cs" Inherits="admin_page_module_function_module_DiemDanh_module_ThongKeHocOnline_TongTungLop" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <style>
        .tk-title{
            display:block;
            text-align:center;
            font-weight:bold;
            font-size:18px;
            color:red;
        }
        .vn-khoi {
            margin-top: 10px;
            background-color: #a9a8a8;
            padding: 15px 10px;
            margin-left: 6px;
        }

            .vn-khoi a {
                color: white;
                font-size: 14px;
                font-weight: bold;
                text-decoration: none;
                text-align: center;
            }

                .vn-khoi a:hover {
                    background-color: red;
                    color: white;
                    text-decoration: none;
                    /*padding: 15px 10px;*/
                    display: block;
                }

        .percent-khoi {
         margin-top:10px;
            color: blue;
            font-size: 14px;
            font-weight: bold;
        }
          .percent-khoi1 {
            margin-top:15px;
            color: blue;
            font-size: 14px;
            font-weight: bold;
        }

        .block-khoi {
            margin-bottom: 20px;
        }

        @media (max-width:767px) {
            .wrap-thongke__online .col-1 {
                width: 17.333333%;
            }
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
     <span class="tk-title">THỐNG KÊ DANH SÁCH HỌC SINH HỌC ONLINE NĂM 2020 - 2021</span>
    <div class="wrap-thongke__online">
        <div class="col-12 block-khoi">
            <div class="col-2 percent-khoi">Khối 1: <%=percentkhoi1 %>%</div>
            <div class="col-2 percent-khoi">Khối 2: <%=percentkhoi2 %>%</div>
            <div class="col-2 percent-khoi">Khối 3: <%=percentkhoi3 %>%</div>
            <div class="col-2 percent-khoi">Khối 4: <%=percentkhoi4 %>%</div>
            <div class="col-2 percent-khoi">Khối 5: <%=percentkhoi5 %>%</div>
        </div>

        <div class="col-12">
            <div class="col-2 percent-khoi">Khối 6: <%=percentkhoi6 %>%</div>
            <div class="col-2 percent-khoi">Khối 7: <%=percentkhoi7 %>%</div>
            <div class="col-2 percent-khoi">Khối 8: <%=percentkhoi8 %>%</div>
            <div class="col-2 percent-khoi">Khối 10: <%=percentkhoi10 %>%</div>
            <div class="col-2 percent-khoi">Khối 11: <%=percentkhoi11 %>%</div>
        </div>
         <div class="col-12 percent-khoi1"> Số lượng học sinh tham gia học Online</div>
        <div class="col-1 vn-khoi">
            <a href="javascript:void(0)" id="btnLop11" runat="server" >
                <div>Lớp 1/1</div>
                <div><%=soluonghocsinh1_1%>/<%=tongsohocsinh1_1%></div>
            </a>
        </div>
        <div class="col-1 vn-khoi">
            <a href="javascript:void(0)" id="btnLop12" runat="server" >
                <div>Lớp 1/2</div>
                <div><%=soluonghocsinh1_2%>/<%=tongsohocsinh1_2%></div>
            </a>
        </div>

        <div class="col-1 vn-khoi">
            <a href="javascript:void(0)" id="btnLop13" runat="server">
                <div>Lớp 1/3</div>
                <div><%=soluonghocsinh13%>/<%=tongsohocsinh13%></div>
            </a>
        </div>
        <div class="col-1 vn-khoi">
            <a href="javascript:void(0)" id="btnLop14" runat="server" >
                <div>Lớp 1/4</div>
                <div><%=soluonghocsinh14%>/<%=tongsohocsinh14%></div>
            </a>
        </div>
        <div class="col-1 vn-khoi">
            <a href="javascript:void(0)" id="btnLop21" runat="server" >
                <div>Lớp 2/1</div>
                <div><%=soluonghocsinh21%>/<%=tongsohocsinh21%></div>
            </a>
        </div>
        <div class="col-1 vn-khoi">
            <a href="javascript:void(0)" id="btnLop22" runat="server">
                <div>Lớp 2/2</div>
                <div><%=soluonghocsinh22%>/<%=tongsohocsinh22%></div>
            </a>
        </div>
        <div class="col-1 vn-khoi">
            <a href="javascript:void(0)" id="btnLop23" runat="server" >
                <div>Lớp 2/3</div>
                <div><%=soluonghocsinh23%>/<%=tongsohocsinh23%></div>
            </a>
        </div>
        <div class="col-1 vn-khoi">
            <a href="javascript:void(0)" id="btnLop24" runat="server" >
                <div>Lớp 2/4</div>
                <div><%=soluonghocsinh24%>/<%=tongsohocsinh24%></div>
            </a>
        </div>
        <div class="col-1 vn-khoi">
            <a href="javascript:void(0)" id="btnLop31" runat="server" >
                <div>Lớp 3/1</div>
                <div><%=soluonghocsinh31%>/<%=tongsohocsinh31%></div>
            </a>
        </div>
        <div class="col-1 vn-khoi">
            <a href="javascript:void(0)" id="btnLop32" runat="server">
                <div>Lớp 3/2</div>
                <div><%=soluonghocsinh32%>/<%=tongsohocsinh32%></div>
            </a>
        </div>
        <div class="col-1 vn-khoi">
            <a href="javascript:void(0)" id="btnLop33" runat="server">
                <div>Lớp 3/3</div>
                <div><%=soluonghocsinh33%>/<%=tongsohocsinh33%></div>
            </a>
        </div>
        <div class="col-1 vn-khoi">
            <a href="javascript:void(0)" id="btnLop34" runat="server" >
                <div>Lớp 3/4</div>
                <div><%=soluonghocsinh34%>/<%=tongsohocsinh34%></div>
            </a>
        </div>
        <div class="col-1 vn-khoi">
            <a href="javascript:void(0)" id="btnLop35" runat="server">
                <div>Lớp 3/5</div>
                <div><%=soluonghocsinh35%>/<%=tongsohocsinh35%></div>
            </a>
        </div>
        <div class="col-1 vn-khoi">
            <a href="javascript:void(0)" id="btnLop41" runat="server">
                <div>Lớp 4/1</div>
                <div><%=soluonghocsinh41%>/<%=tongsohocsinh41%></div>
            </a>
        </div>
        <div class="col-1 vn-khoi">
            <a href="javascript:void(0)" id="btnLop42" runat="server">
                <div>Lớp 4/2</div>
                <div><%=soluonghocsinh42%>/<%=tongsohocsinh42%></div>
            </a>
        </div>
        <div class="col-1 vn-khoi">
            <a href="javascript:void(0)" id="btnLop51" runat="server">
                <div>Lớp 5/1</div>
                <div><%=soluonghocsinh51%>/<%=tongsohocsinh51%></div>
            </a>
        </div>
        <div class="col-1 vn-khoi">
            <a href="javascript:void(0)" id="btnLop61" runat="server">
                <div>Lớp 6/1</div>
                <div><%=soluonghocsinh61%>/<%=tongsohocsinh61%></div>
            </a>
        </div>
        <div class="col-1 vn-khoi">
            <a href="javascript:void(0)" id="btnLop71" runat="server">
                <div>Lớp 7/1</div>
                <div><%=soluonghocsinh71%>/<%=tongsohocsinh71%></div>
            </a>
        </div>
        <div class="col-1 vn-khoi">
            <a href="javascript:void(0)" id="btnLop72" runat="server">
                <div>Lớp 7/2</div>
                <div><%=soluonghocsinh72%>/<%=tongsohocsinh72%></div>
            </a>
        </div>
        <div class="col-1 vn-khoi">

            <a href="javascript:void(0)" id="btnLop81" runat="server">
                <div>Lớp 8/1</div>
                <div><%=soluonghocsinh81%>/<%=tongsohocsinh81%></div>
            </a>
        </div>
        <div class="col-1 vn-khoi">
            <a href="javascript:void(0)" id="btnLop101" runat="server">
                <div>Lớp 10/1</div>
                <div><%=soluonghocsinh101%>/<%=tongsohocsinh101%></div>
            </a>
        </div>
        <div class="col-1 vn-khoi block-khoi">
            <a href="javascript:void(0)" id="btnLop111" runat="server">
                <div>Lớp 11/1</div>
                <div><%=soluonghocsinh111%>/<%=tongsohocsinh111%></div>
            </a>
        </div>
        <div>
                 
            <div class="col-12 percent-khoi"> Danh sách học sinh không tham gia học Online</div>
            <table class="table" style="background-color: white">

                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Lớp</th>
                        <th scope="col">Họ tên</th>
                        <th scope="col">Tình trạng</th>
                        <th scope="col">Lý do</th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater ID="rpDanhSach" runat="server">
                        <ItemTemplate>
                            <tr>
                                <th scope="row" style="text-align: center"><%=STT++ %></th>
                                <td><%#Eval("lop_name") %></td>
                                <td><%#Eval("hocsinh_name") %></td>
                                <td><%#Eval("tinhtrang") %></td>
                                <th scope="col"><%#Eval("dshoconline_lydo") %></th>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

