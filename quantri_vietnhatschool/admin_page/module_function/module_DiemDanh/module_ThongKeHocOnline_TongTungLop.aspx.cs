﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_DiemDanh_module_ThongKeHocOnline_TongTungLop : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public int soluonghocsinh1_1, tongsohocsinh1_1, soluonghocsinh1_2, tongsohocsinh1_2, soluonghocsinh13, tongsohocsinh13, soluonghocsinh14, tongsohocsinh14, soluonghocsinh21, tongsohocsinh21, soluonghocsinh22, tongsohocsinh22, soluonghocsinh23, tongsohocsinh23, soluonghocsinh24, tongsohocsinh24, soluonghocsinh31, tongsohocsinh31, soluonghocsinh32, tongsohocsinh32, soluonghocsinh33, tongsohocsinh33, soluonghocsinh34, tongsohocsinh34, soluonghocsinh35, tongsohocsinh35, soluonghocsinh41, tongsohocsinh41, soluonghocsinh42, tongsohocsinh42, soluonghocsinh51, tongsohocsinh51, soluonghocsinh61, tongsohocsinh61, soluonghocsinh71, tongsohocsinh71, soluonghocsinh72, tongsohocsinh72, soluonghocsinh81, tongsohocsinh81, soluonghocsinh101, tongsohocsinh101, soluonghocsinh111, tongsohocsinh111;
    public int percentkhoi1, percentkhoi2, percentkhoi3, percentkhoi4, percentkhoi5, percentkhoi6, percentkhoi7, percentkhoi8, percentkhoi10, percentkhoi11;
    public int STT = 1;
    protected void Page_Load(object sender, EventArgs e)
    {

        //lớp 1_1
        var listDanhSach1_1 = from hol in db.tbDanhSachHocOnLines
                              join hstl in db.tbHocSinhTrongLops on hol.hstl_id equals hstl.hstl_id
                              where hol.lop_id == 1 && hol.tinhtrang == 1 && hstl.namhoc_id==1
                              orderby hstl.position
                              select hol;
        var listTongDanhSach1_1 = from hol in db.tbDanhSachHocOnLines
                                  join hstl in db.tbHocSinhTrongLops on hol.hstl_id equals hstl.hstl_id
                                  where hstl.namhoc_id == 1
                                  orderby hstl.position
                                  where hol.lop_id == 1
                                  select hol;
        soluonghocsinh1_1 = listDanhSach1_1.Count();
        tongsohocsinh1_1 = listTongDanhSach1_1.Count();

        //lớp 1_2
        var listDanhSach1_2 = from hol in db.tbDanhSachHocOnLines
                              join hstl in db.tbHocSinhTrongLops on hol.hstl_id equals hstl.hstl_id
                              where  hstl.namhoc_id == 1
                              orderby hstl.position
                              where hol.lop_id == 2 && hol.tinhtrang == 1
                              select hol;
        var listTongDanhSach1_2 = from hol in db.tbDanhSachHocOnLines
                                  join hstl in db.tbHocSinhTrongLops on hol.hstl_id equals hstl.hstl_id
                                  where hstl.namhoc_id == 1
                                  orderby hstl.position
                                  where hol.lop_id == 2
                                  select hol;
        soluonghocsinh1_2 = listDanhSach1_2.Count();
        tongsohocsinh1_2 = listTongDanhSach1_2.Count();

        //lớp 1_3
        var listDanhSach13 = from hol in db.tbDanhSachHocOnLines
                             join hstl in db.tbHocSinhTrongLops on hol.hstl_id equals hstl.hstl_id
                             where hstl.namhoc_id == 1
                             orderby hstl.position
                             where hol.lop_id == 3 && hol.tinhtrang == 1
                             select hol;
        var listTongDanhSach13 = from hol in db.tbDanhSachHocOnLines
                                 join hstl in db.tbHocSinhTrongLops on hol.hstl_id equals hstl.hstl_id
                                 where hstl.namhoc_id == 1
                                 orderby hstl.position
                                 where hol.lop_id == 3
                                 select hol;
        soluonghocsinh13 = listDanhSach13.Count();
        tongsohocsinh13 = listTongDanhSach13.Count();
        //lớp 1_4
        var listDanhSach14 = from hol in db.tbDanhSachHocOnLines
                             join hstl in db.tbHocSinhTrongLops on hol.hstl_id equals hstl.hstl_id
                             where  hstl.namhoc_id == 1
                             orderby hstl.position
                             where hol.lop_id == 4 && hol.tinhtrang == 1
                             select hol;
        var listTongDanhSach14 = from hol in db.tbDanhSachHocOnLines
                                 join hstl in db.tbHocSinhTrongLops on hol.hstl_id equals hstl.hstl_id
                                 where hstl.namhoc_id == 1
                                 orderby hstl.position
                                 where hol.lop_id == 4
                                 select hol;
        soluonghocsinh14 = listDanhSach14.Count();
        tongsohocsinh14 = listTongDanhSach14.Count();
        // Phần trăm học sinh khối 1;
        int tongHocSinhKhoi1 = tongsohocsinh1_1 + tongsohocsinh1_2 + tongsohocsinh13 + tongsohocsinh14;
        int soluonghocsinhkhoi1 = soluonghocsinh1_1 + soluonghocsinh1_2 + soluonghocsinh13 + soluonghocsinh14;
        percentkhoi1 = (soluonghocsinhkhoi1 * 100) / tongHocSinhKhoi1;
        //lớp 2_1
        var listDanhSach21 = from hol in db.tbDanhSachHocOnLines
                             join hstl in db.tbHocSinhTrongLops on hol.hstl_id equals hstl.hstl_id
                             where hstl.namhoc_id == 1
                             orderby hstl.position
                             where hol.lop_id == 5 && hol.tinhtrang == 1
                             select hol;
        var listTongDanhSach21 = from hol in db.tbDanhSachHocOnLines
                                 join hstl in db.tbHocSinhTrongLops on hol.hstl_id equals hstl.hstl_id
                                 where  hstl.namhoc_id == 1
                                 orderby hstl.position
                                 where hol.lop_id == 5
                                 select hol;
        soluonghocsinh21 = listDanhSach21.Count();
        tongsohocsinh21 = listTongDanhSach21.Count();
        //lớp 2_2
        var listDanhSach22 = from hol in db.tbDanhSachHocOnLines
                             join hstl in db.tbHocSinhTrongLops on hol.hstl_id equals hstl.hstl_id
                             where hstl.namhoc_id == 1
                             orderby hstl.position
                             where hol.lop_id == 6 && hol.tinhtrang == 1
                             select hol;
        var listTongDanhSach22 = from hol in db.tbDanhSachHocOnLines
                                 join hstl in db.tbHocSinhTrongLops on hol.hstl_id equals hstl.hstl_id
                                 where  hstl.namhoc_id == 1
                                 orderby hstl.position
                                 where hol.lop_id == 6
                                 select hol;
        soluonghocsinh22 = listDanhSach22.Count();
        tongsohocsinh22 = listTongDanhSach22.Count();
        //lớp 2_3
        var listDanhSach23 = from hol in db.tbDanhSachHocOnLines
                             join hstl in db.tbHocSinhTrongLops on hol.hstl_id equals hstl.hstl_id
                             where hstl.namhoc_id == 1
                             orderby hstl.position
                             where hol.lop_id == 7 && hol.tinhtrang == 1
                             select hol;
        var listTongDanhSach23 = from hol in db.tbDanhSachHocOnLines
                                 join hstl in db.tbHocSinhTrongLops on hol.hstl_id equals hstl.hstl_id
                                 where hstl.namhoc_id == 1
                                 orderby hstl.position
                                 where hol.lop_id == 7
                                 select hol;
        soluonghocsinh23 = listDanhSach23.Count();
        tongsohocsinh23 = listTongDanhSach23.Count();
        //lớp 2_4
        var listDanhSach24 = from hol in db.tbDanhSachHocOnLines
                             join hstl in db.tbHocSinhTrongLops on hol.hstl_id equals hstl.hstl_id
                             where hstl.namhoc_id == 1
                             orderby hstl.position
                             where hol.lop_id == 8 && hol.tinhtrang == 1
                             select hol;
        var listTongDanhSach24 = from hol in db.tbDanhSachHocOnLines
                                 where hol.lop_id == 8
                                 select hol;
        soluonghocsinh24 = listDanhSach24.Count();
        tongsohocsinh24 = listTongDanhSach24.Count();
        // Phần trăm học sinh khối 2;
        int tongHocSinhKhoi2 = tongsohocsinh21 + tongsohocsinh22 + tongsohocsinh23 + tongsohocsinh24;
        int soluonghocsinhkhoi2 = soluonghocsinh21 + soluonghocsinh22 + soluonghocsinh23 + soluonghocsinh24;
        percentkhoi2 = (soluonghocsinhkhoi2 * 100) / tongHocSinhKhoi2;
        //lớp 3_1
        var listDanhSach31 = from hol in db.tbDanhSachHocOnLines
                             join hstl in db.tbHocSinhTrongLops on hol.hstl_id equals hstl.hstl_id
                             where hstl.namhoc_id == 1
                             orderby hstl.position
                             where hol.lop_id == 10 && hol.tinhtrang == 1
                             select hol;
        var listTongDanhSach31 = from hol in db.tbDanhSachHocOnLines
                                 join hstl in db.tbHocSinhTrongLops on hol.hstl_id equals hstl.hstl_id
                                 where hstl.namhoc_id == 1
                                 orderby hstl.position
                                 where hol.lop_id == 10
                                 select hol;
        soluonghocsinh31 = listDanhSach31.Count();
        tongsohocsinh31 = listTongDanhSach31.Count();
        //lớp 3_2
        var listDanhSach32 = from hol in db.tbDanhSachHocOnLines
                             join hstl in db.tbHocSinhTrongLops on hol.hstl_id equals hstl.hstl_id
                             where  hstl.namhoc_id == 1
                             orderby hstl.position
                             where hol.lop_id == 11 && hol.tinhtrang == 1
                             select hol;
        var listTongDanhSach32 = from hol in db.tbDanhSachHocOnLines
                                 join hstl in db.tbHocSinhTrongLops on hol.hstl_id equals hstl.hstl_id
                                 where hstl.namhoc_id == 1
                                 orderby hstl.position
                                 where hol.lop_id == 11
                                 select hol;
        soluonghocsinh32 = listDanhSach32.Count();
        tongsohocsinh32 = listTongDanhSach32.Count();
        //lớp 3_3
        var listDanhSach33 = from hol in db.tbDanhSachHocOnLines
                             join hstl in db.tbHocSinhTrongLops on hol.hstl_id equals hstl.hstl_id
                             where hstl.namhoc_id == 1
                             orderby hstl.position
                             where hol.lop_id == 18 && hol.tinhtrang == 1
                             select hol;
        var listTongDanhSach33 = from hol in db.tbDanhSachHocOnLines
                                 join hstl in db.tbHocSinhTrongLops on hol.hstl_id equals hstl.hstl_id
                                 where hstl.namhoc_id == 1
                                 orderby hstl.position
                                 where hol.lop_id == 18
                                 select hol;
        soluonghocsinh33 = listDanhSach33.Count();
        tongsohocsinh33 = listTongDanhSach33.Count();
        //lớp 3_4
        var listDanhSach34 = from hol in db.tbDanhSachHocOnLines
                             join hstl in db.tbHocSinhTrongLops on hol.hstl_id equals hstl.hstl_id
                             where hstl.namhoc_id == 1
                             orderby hstl.position
                             where hol.lop_id == 19 && hol.tinhtrang == 1
                             select hol;
        var listTongDanhSach34 = from hol in db.tbDanhSachHocOnLines
                                 join hstl in db.tbHocSinhTrongLops on hol.hstl_id equals hstl.hstl_id
                                 where hstl.namhoc_id == 1
                                 orderby hstl.position
                                 where hol.lop_id == 19
                                 select hol;
        soluonghocsinh34 = listDanhSach34.Count();
        tongsohocsinh34 = listTongDanhSach34.Count();
        //lớp 3_5
        var listDanhSach35 = from hol in db.tbDanhSachHocOnLines
                             join hstl in db.tbHocSinhTrongLops on hol.hstl_id equals hstl.hstl_id
                             where  hstl.namhoc_id == 1
                             orderby hstl.position
                             where hol.lop_id == 20 && hol.tinhtrang == 1
                             select hol;
        var listTongDanhSach35 = from hol in db.tbDanhSachHocOnLines
                                 join hstl in db.tbHocSinhTrongLops on hol.hstl_id equals hstl.hstl_id
                                 where hstl.namhoc_id == 1
                                 orderby hstl.position
                                 where hol.lop_id == 20
                                 select hol;
        soluonghocsinh35 = listDanhSach35.Count();
        tongsohocsinh35 = listTongDanhSach35.Count();
        // Phần trăm học sinh khối 3;
        int tongHocSinhKhoi3 = tongsohocsinh31 + tongsohocsinh32 + tongsohocsinh33 + tongsohocsinh34 + tongsohocsinh35;
        int soluonghocsinhkhoi3 = soluonghocsinh31 + soluonghocsinh32 + soluonghocsinh33 + soluonghocsinh34 + soluonghocsinh35;
        percentkhoi3 = (soluonghocsinhkhoi3 * 100) / tongHocSinhKhoi3;
        //lớp 4_1
        var listDanhSach41 = from hol in db.tbDanhSachHocOnLines
                             join hstl in db.tbHocSinhTrongLops on hol.hstl_id equals hstl.hstl_id
                             where hstl.namhoc_id == 1
                             orderby hstl.position
                             where hol.lop_id == 12 && hol.tinhtrang == 1
                             select hol;
        var listTongDanhSach41 = from hol in db.tbDanhSachHocOnLines
                                 join hstl in db.tbHocSinhTrongLops on hol.hstl_id equals hstl.hstl_id
                                 where hstl.namhoc_id == 1
                                 orderby hstl.position
                                 where hol.lop_id == 12
                                 select hol;
        soluonghocsinh41 = listDanhSach41.Count();
        tongsohocsinh41 = listTongDanhSach41.Count();
        //lớp 4_2
        var listDanhSach42 = from hol in db.tbDanhSachHocOnLines
                             join hstl in db.tbHocSinhTrongLops on hol.hstl_id equals hstl.hstl_id
                             where hstl.namhoc_id == 1
                             orderby hstl.position
                             where hol.lop_id == 21 && hol.tinhtrang == 1
                             select hol;
        var listTongDanhSach42 = from hol in db.tbDanhSachHocOnLines
                                 join hstl in db.tbHocSinhTrongLops on hol.hstl_id equals hstl.hstl_id
                                 where hstl.namhoc_id == 1
                                 orderby hstl.position
                                 where hol.lop_id == 21
                                 select hol;
        soluonghocsinh42 = listDanhSach42.Count();
        tongsohocsinh42 = listTongDanhSach42.Count();
        // Phần trăm học sinh khối 4;
        int tongHocSinhKhoi4 = tongsohocsinh41 + tongsohocsinh42;
        int soluonghocsinhkhoi4 = soluonghocsinh41 + soluonghocsinh42;
        percentkhoi4 = (soluonghocsinhkhoi4 * 100) / tongHocSinhKhoi4;
        //lớp 5_1
        var listDanhSach51 = from hol in db.tbDanhSachHocOnLines
                             join hstl in db.tbHocSinhTrongLops on hol.hstl_id equals hstl.hstl_id
                             where hstl.namhoc_id == 1
                             orderby hstl.position
                             where hol.lop_id == 13 && hol.tinhtrang == 1
                             select hol;
        var listTongDanhSach51 = from hol in db.tbDanhSachHocOnLines
                                 join hstl in db.tbHocSinhTrongLops on hol.hstl_id equals hstl.hstl_id
                                 where hstl.namhoc_id == 1
                                 orderby hstl.position
                                 where hol.lop_id == 13
                                 select hol;
        soluonghocsinh51 = listDanhSach51.Count();
        tongsohocsinh51 = listTongDanhSach51.Count();
        // Phần trăm học sinh khối 5;
        int tongHocSinhKhoi5 = tongsohocsinh51;
        int soluonghocsinhkhoi5 = soluonghocsinh51;
        percentkhoi5 = (soluonghocsinhkhoi5 * 100) / tongHocSinhKhoi5;
        //lớp 6_1
        var listDanhSach61 = from hol in db.tbDanhSachHocOnLines
                             join hstl in db.tbHocSinhTrongLops on hol.hstl_id equals hstl.hstl_id
                             where hstl.namhoc_id == 1
                             orderby hstl.position
                             where hol.lop_id == 14 && hol.tinhtrang == 1
                             select hol;
        var listTongDanhSach61 = from hol in db.tbDanhSachHocOnLines
                                 join hstl in db.tbHocSinhTrongLops on hol.hstl_id equals hstl.hstl_id
                                 where hstl.namhoc_id == 1
                                 orderby hstl.position
                                 where hol.lop_id == 14
                                 select hol;
        soluonghocsinh61 = listDanhSach61.Count();
        tongsohocsinh61 = listTongDanhSach61.Count();
        // Phần trăm học sinh khối 6;
        int tongHocSinhKhoi6 = tongsohocsinh61;
        int soluonghocsinhkhoi6 = soluonghocsinh61;
        percentkhoi6 = (soluonghocsinhkhoi6 * 100) / tongHocSinhKhoi6;
        //lớp 7_1
        var listDanhSach71 = from hol in db.tbDanhSachHocOnLines
                             join hstl in db.tbHocSinhTrongLops on hol.hstl_id equals hstl.hstl_id
                             where hstl.namhoc_id == 1
                             orderby hstl.position
                             where hol.lop_id == 16 && hol.tinhtrang == 1
                             select hol;
        var listTongDanhSach71 = from hol in db.tbDanhSachHocOnLines
                                 join hstl in db.tbHocSinhTrongLops on hol.hstl_id equals hstl.hstl_id
                                 where hstl.namhoc_id == 1
                                 orderby hstl.position
                                 where hol.lop_id == 16
                                 select hol;
        soluonghocsinh71 = listDanhSach71.Count();
        tongsohocsinh71 = listTongDanhSach71.Count();
        //lớp 7_2
        var listDanhSach72 = from hol in db.tbDanhSachHocOnLines
                             join hstl in db.tbHocSinhTrongLops on hol.hstl_id equals hstl.hstl_id
                             where hstl.namhoc_id == 1
                             orderby hstl.position
                             where hol.lop_id == 22 && hol.tinhtrang == 1
                             select hol;
        var listTongDanhSach72 = from hol in db.tbDanhSachHocOnLines
                                 join hstl in db.tbHocSinhTrongLops on hol.hstl_id equals hstl.hstl_id
                                 where hstl.namhoc_id == 1
                                 orderby hstl.position
                                 where hol.lop_id == 22
                                 select hol;
        soluonghocsinh72 = listDanhSach72.Count();
        tongsohocsinh72 = listTongDanhSach72.Count();
        // Phần trăm học sinh khối 7;
        int tongHocSinhKhoi7 = tongsohocsinh71 + tongsohocsinh72;
        int soluonghocsinhkhoi7 = soluonghocsinh71 + soluonghocsinh72;
        percentkhoi7 = (soluonghocsinhkhoi7 * 100) / tongHocSinhKhoi7;
        //lớp 8
        var listDanhSach81 = from hol in db.tbDanhSachHocOnLines
                             join hstl in db.tbHocSinhTrongLops on hol.hstl_id equals hstl.hstl_id
                             where hstl.namhoc_id == 1
                             orderby hstl.position
                             where hol.lop_id == 23 && hol.tinhtrang == 1
                             select hol;
        var listTongDanhSach81 = from hol in db.tbDanhSachHocOnLines
                                 join hstl in db.tbHocSinhTrongLops on hol.hstl_id equals hstl.hstl_id
                                 where hstl.namhoc_id == 1
                                 orderby hstl.position
                                 where hol.lop_id == 23
                                 select hol;
        soluonghocsinh81 = listDanhSach81.Count();
        tongsohocsinh81 = listTongDanhSach81.Count();
        // Phần trăm học sinh khối 8;
        int tongHocSinhKhoi8 = tongsohocsinh81;
        int soluonghocsinhkhoi8 = soluonghocsinh81;
        percentkhoi8 = (soluonghocsinhkhoi8 * 100) / tongHocSinhKhoi8;
        //lớp 10
        var listDanhSach101 = from hol in db.tbDanhSachHocOnLines
                              join hstl in db.tbHocSinhTrongLops on hol.hstl_id equals hstl.hstl_id
                              where hstl.namhoc_id == 1
                              orderby hstl.position
                              where hol.lop_id == 17 && hol.tinhtrang == 1
                              select hol;
        var listTongDanhSach101 = from hol in db.tbDanhSachHocOnLines
                                  join hstl in db.tbHocSinhTrongLops on hol.hstl_id equals hstl.hstl_id
                                  where hstl.namhoc_id == 1
                                  orderby hstl.position
                                  where hol.lop_id == 17
                                  select hol;
        soluonghocsinh101 = listDanhSach101.Count();
        tongsohocsinh101 = listTongDanhSach101.Count();
        // Phần trăm học sinh khối 10;
        int tongHocSinhKhoi10 = tongsohocsinh101;
        int soluonghocsinhkhoi10 = soluonghocsinh101;
        percentkhoi10 = (soluonghocsinhkhoi10 * 100) / tongHocSinhKhoi10;
        //lớp 11
        var listDanhSach111 = from hol in db.tbDanhSachHocOnLines
                              join hstl in db.tbHocSinhTrongLops on hol.hstl_id equals hstl.hstl_id
                              where hstl.namhoc_id == 1
                              orderby hstl.position
                              where hol.lop_id == 24 && hol.tinhtrang == 1
                              select hol;
        var listTongDanhSach111 = from hol in db.tbDanhSachHocOnLines
                                  join hstl in db.tbHocSinhTrongLops on hol.hstl_id equals hstl.hstl_id
                                  where hstl.namhoc_id == 1
                                  orderby hstl.position
                                  where hol.lop_id == 24
                                  select hol;
        soluonghocsinh111 = listDanhSach111.Count();
        tongsohocsinh111 = listTongDanhSach111.Count();
        // Phần trăm học sinh khối 11;
        int tongHocSinhKhoi11 = tongsohocsinh111;
        int soluonghocsinhkhoi11 = soluonghocsinh111;
        percentkhoi11 = (soluonghocsinhkhoi11 * 100) / tongHocSinhKhoi11;
        rpDanhSach.DataSource = from ds in db.tbDanhSachHocOnLines
                                join l in db.tbLops on ds.lop_id equals l.lop_id
                                join hstl in db.tbHocSinhTrongLops on ds.hstl_id equals hstl.hstl_id
                                join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                                where ds.tinhtrang ==2
                                orderby l.khoi_id 
                                select new
                                {
                                    ds.dshoconline_lydo,
                                    hs.hocsinh_name,
                                    tinhtrang = ds.tinhtrang == 2 ? "Không học" : "Có học",
                                    l.lop_name

                                };
        rpDanhSach.DataBind();
    }

    protected void btnLop11_ServerClick(object sender, EventArgs e)
    {
        rpDanhSach.DataSource = from ds in db.tbDanhSachHocOnLines
                                join l in db.tbLops on ds.lop_id equals l.lop_id
                                join hstl in db.tbHocSinhTrongLops on ds.hstl_id equals hstl.hstl_id
                                join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                                where l.lop_id == 1
                                orderby ds.tinhtrang descending
                                select new
                                {
                                    ds.dshoconline_lydo,
                                    tinhtrang = ds.tinhtrang == 2 ? "Không học" : "Có học",
                                    hs.hocsinh_name,
                                    l.lop_name

                                };
        rpDanhSach.DataBind();
    }
    protected void btnLop12_ServerClick(object sender, EventArgs e)
    {
        rpDanhSach.DataSource = from ds in db.tbDanhSachHocOnLines
                                join l in db.tbLops on ds.lop_id equals l.lop_id
                                join hstl in db.tbHocSinhTrongLops on ds.hstl_id equals hstl.hstl_id
                                join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                                where l.lop_id == 2
                                orderby ds.tinhtrang descending
                                select new
                                {
                                    ds.dshoconline_lydo,
                                    hs.hocsinh_name,
                                    tinhtrang = ds.tinhtrang == 2 ? "Không học" : "Có học",
                                    l.lop_name

                                };
        rpDanhSach.DataBind();
    }
    protected void btnLop13_ServerClick(object sender, EventArgs e)
    {
        rpDanhSach.DataSource = from ds in db.tbDanhSachHocOnLines
                                join l in db.tbLops on ds.lop_id equals l.lop_id
                                join hstl in db.tbHocSinhTrongLops on ds.hstl_id equals hstl.hstl_id
                                join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                                where l.lop_id == 3
                                orderby ds.tinhtrang descending
                                select new
                                {
                                    ds.dshoconline_lydo,
                                    hs.hocsinh_name,
                                    tinhtrang = ds.tinhtrang == 2 ? "Không học" : "Có học",
                                    l.lop_name

                                };
        rpDanhSach.DataBind();
    }
    protected void btnLop14_ServerClick(object sender, EventArgs e)
    {
        rpDanhSach.DataSource = from ds in db.tbDanhSachHocOnLines
                                join l in db.tbLops on ds.lop_id equals l.lop_id
                                join hstl in db.tbHocSinhTrongLops on ds.hstl_id equals hstl.hstl_id
                                join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                                where l.lop_id == 4
                                orderby ds.tinhtrang descending
                                select new
                                {
                                    ds.dshoconline_lydo,
                                    hs.hocsinh_name,
                                    tinhtrang = ds.tinhtrang == 2 ? "Không học" : "Có học",
                                    l.lop_name

                                };
        rpDanhSach.DataBind();
    }
    protected void btnLop21_ServerClick(object sender, EventArgs e)
    {
        rpDanhSach.DataSource = from ds in db.tbDanhSachHocOnLines
                                join l in db.tbLops on ds.lop_id equals l.lop_id
                                join hstl in db.tbHocSinhTrongLops on ds.hstl_id equals hstl.hstl_id
                                join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                                where l.lop_id == 5
                                orderby ds.tinhtrang descending
                                select new
                                {
                                    ds.dshoconline_lydo,
                                    hs.hocsinh_name,
                                    tinhtrang = ds.tinhtrang == 2 ? "Không học" : "Có học",
                                    l.lop_name

                                };
        rpDanhSach.DataBind();
    }
    protected void btnLop22_ServerClick(object sender, EventArgs e)
    {
        rpDanhSach.DataSource = from ds in db.tbDanhSachHocOnLines
                                join l in db.tbLops on ds.lop_id equals l.lop_id
                                join hstl in db.tbHocSinhTrongLops on ds.hstl_id equals hstl.hstl_id
                                join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                                where l.lop_id == 6
                                orderby ds.tinhtrang descending
                                select new
                                {
                                    ds.dshoconline_lydo,
                                    hs.hocsinh_name,
                                    tinhtrang = ds.tinhtrang == 2 ? "Không học" : "Có học",
                                    l.lop_name

                                };
        rpDanhSach.DataBind();
    }
    protected void btnLop23_ServerClick(object sender, EventArgs e)
    {
        rpDanhSach.DataSource = from ds in db.tbDanhSachHocOnLines
                                join l in db.tbLops on ds.lop_id equals l.lop_id
                                join hstl in db.tbHocSinhTrongLops on ds.hstl_id equals hstl.hstl_id
                                join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                                where l.lop_id == 7
                                orderby ds.tinhtrang descending
                                select new
                                {
                                    ds.dshoconline_lydo,
                                    hs.hocsinh_name,
                                    tinhtrang = ds.tinhtrang == 2 ? "Không học" : "Có học",
                                    l.lop_name

                                };
        rpDanhSach.DataBind();
    }
    protected void btnLop24_ServerClick(object sender, EventArgs e)
    {
        rpDanhSach.DataSource = from ds in db.tbDanhSachHocOnLines
                                join l in db.tbLops on ds.lop_id equals l.lop_id
                                join hstl in db.tbHocSinhTrongLops on ds.hstl_id equals hstl.hstl_id
                                join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                                where l.lop_id == 8
                                orderby ds.tinhtrang descending
                                select new
                                {
                                    ds.dshoconline_lydo,
                                    hs.hocsinh_name,
                                    tinhtrang = ds.tinhtrang == 2 ? "Không học" : "Có học",
                                    l.lop_name

                                };
        rpDanhSach.DataBind();
    }
    protected void btnLop31_ServerClick(object sender, EventArgs e)
    {
        rpDanhSach.DataSource = from ds in db.tbDanhSachHocOnLines
                                join l in db.tbLops on ds.lop_id equals l.lop_id
                                join hstl in db.tbHocSinhTrongLops on ds.hstl_id equals hstl.hstl_id
                                join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                                where l.lop_id == 10
                                orderby ds.tinhtrang descending
                                select new
                                {
                                    ds.dshoconline_lydo,
                                    hs.hocsinh_name,
                                    tinhtrang = ds.tinhtrang == 2 ? "Không học" : "Có học",
                                    l.lop_name

                                };
        rpDanhSach.DataBind();
    }
    protected void btnLop32_ServerClick(object sender, EventArgs e)
    {
        rpDanhSach.DataSource = from ds in db.tbDanhSachHocOnLines
                                join l in db.tbLops on ds.lop_id equals l.lop_id
                                join hstl in db.tbHocSinhTrongLops on ds.hstl_id equals hstl.hstl_id
                                join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                                where l.lop_id == 11
                                orderby ds.tinhtrang descending
                                select new
                                {
                                    ds.dshoconline_lydo,
                                    hs.hocsinh_name,
                                    tinhtrang = ds.tinhtrang == 2 ? "Không học" : "Có học",
                                    l.lop_name

                                };
        rpDanhSach.DataBind();
    }
    protected void btnLop33_ServerClick(object sender, EventArgs e)
    {
        rpDanhSach.DataSource = from ds in db.tbDanhSachHocOnLines
                                join l in db.tbLops on ds.lop_id equals l.lop_id
                                join hstl in db.tbHocSinhTrongLops on ds.hstl_id equals hstl.hstl_id
                                join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                                where l.lop_id == 18
                                orderby ds.tinhtrang descending
                                select new
                                {
                                    ds.dshoconline_lydo,
                                    hs.hocsinh_name,
                                    tinhtrang = ds.tinhtrang == 2 ? "Không học" : "Có học",
                                    l.lop_name

                                };
        rpDanhSach.DataBind();
    }
    protected void btnLop34_ServerClick(object sender, EventArgs e)
    {
        rpDanhSach.DataSource = from ds in db.tbDanhSachHocOnLines
                                join l in db.tbLops on ds.lop_id equals l.lop_id
                                join hstl in db.tbHocSinhTrongLops on ds.hstl_id equals hstl.hstl_id
                                join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                                where l.lop_id == 19
                                orderby ds.tinhtrang descending
                                select new
                                {
                                    ds.dshoconline_lydo,
                                    hs.hocsinh_name,
                                    tinhtrang = ds.tinhtrang == 2 ? "Không học" : "Có học",
                                    l.lop_name

                                };
        rpDanhSach.DataBind();
    }
    protected void btnLop35_ServerClick(object sender, EventArgs e)
    {
        rpDanhSach.DataSource = from ds in db.tbDanhSachHocOnLines
                                join l in db.tbLops on ds.lop_id equals l.lop_id
                                join hstl in db.tbHocSinhTrongLops on ds.hstl_id equals hstl.hstl_id
                                join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                                where l.lop_id == 20
                                orderby ds.tinhtrang descending
                                select new
                                {
                                    ds.dshoconline_lydo,
                                    hs.hocsinh_name,
                                    tinhtrang = ds.tinhtrang == 2 ? "Không học" : "Có học",
                                    l.lop_name

                                };
        rpDanhSach.DataBind();
    }
    protected void btnLop41_ServerClick(object sender, EventArgs e)
    {
        rpDanhSach.DataSource = from ds in db.tbDanhSachHocOnLines
                                join l in db.tbLops on ds.lop_id equals l.lop_id
                                join hstl in db.tbHocSinhTrongLops on ds.hstl_id equals hstl.hstl_id
                                join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                                where l.lop_id == 12
                                orderby ds.tinhtrang descending
                                select new
                                {
                                    ds.dshoconline_lydo,
                                    hs.hocsinh_name,
                                    tinhtrang = ds.tinhtrang == 2 ? "Không học" : "Có học",
                                    l.lop_name

                                };
        rpDanhSach.DataBind();
    }
    protected void btnLop42_ServerClick(object sender, EventArgs e)
    {
        rpDanhSach.DataSource = from ds in db.tbDanhSachHocOnLines
                                join l in db.tbLops on ds.lop_id equals l.lop_id
                                join hstl in db.tbHocSinhTrongLops on ds.hstl_id equals hstl.hstl_id
                                join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                                where l.lop_id == 21
                                orderby ds.tinhtrang descending
                                select new
                                {
                                    ds.dshoconline_lydo,
                                    hs.hocsinh_name,
                                    tinhtrang = ds.tinhtrang == 2 ? "Không học" : "Có học",
                                    l.lop_name

                                };
        rpDanhSach.DataBind();
    }
    protected void btnLop51_ServerClick(object sender, EventArgs e)
    {
        rpDanhSach.DataSource = from ds in db.tbDanhSachHocOnLines
                                join l in db.tbLops on ds.lop_id equals l.lop_id
                                join hstl in db.tbHocSinhTrongLops on ds.hstl_id equals hstl.hstl_id
                                join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                                where l.lop_id == 13
                                orderby ds.tinhtrang descending
                                select new
                                {
                                    ds.dshoconline_lydo,
                                    hs.hocsinh_name,
                                    tinhtrang = ds.tinhtrang == 2 ? "Không học" : "Có học",
                                    l.lop_name

                                };
        rpDanhSach.DataBind();
    }
    protected void btnLop61_ServerClick(object sender, EventArgs e)
    {
        rpDanhSach.DataSource = from ds in db.tbDanhSachHocOnLines
                                join l in db.tbLops on ds.lop_id equals l.lop_id
                                join hstl in db.tbHocSinhTrongLops on ds.hstl_id equals hstl.hstl_id
                                join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                                where l.lop_id == 14
                                orderby ds.tinhtrang descending
                                select new
                                {
                                    ds.dshoconline_lydo,
                                    hs.hocsinh_name,
                                    tinhtrang = ds.tinhtrang == 2 ? "Không học" : "Có học",
                                    l.lop_name

                                };
        rpDanhSach.DataBind();
    }
    protected void btnLop71_ServerClick(object sender, EventArgs e)
    {
        rpDanhSach.DataSource = from ds in db.tbDanhSachHocOnLines
                                join l in db.tbLops on ds.lop_id equals l.lop_id
                                join hstl in db.tbHocSinhTrongLops on ds.hstl_id equals hstl.hstl_id
                                join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                                where l.lop_id == 16
                                orderby ds.tinhtrang descending
                                select new
                                {
                                    ds.dshoconline_lydo,
                                    hs.hocsinh_name,
                                    tinhtrang = ds.tinhtrang == 2 ? "Không học" : "Có học",
                                    l.lop_name

                                };
        rpDanhSach.DataBind();
    }
    protected void btnLop72_ServerClick(object sender, EventArgs e)
    {
        rpDanhSach.DataSource = from ds in db.tbDanhSachHocOnLines
                                join l in db.tbLops on ds.lop_id equals l.lop_id
                                join hstl in db.tbHocSinhTrongLops on ds.hstl_id equals hstl.hstl_id
                                join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                                where l.lop_id == 22
                                orderby ds.tinhtrang descending
                                select new
                                {
                                    ds.dshoconline_lydo,
                                    hs.hocsinh_name,
                                    tinhtrang = ds.tinhtrang == 2 ? "Không học" : "Có học",
                                    l.lop_name

                                };
        rpDanhSach.DataBind();
    }
    protected void btnLop81_ServerClick(object sender, EventArgs e)
    {
        rpDanhSach.DataSource = from ds in db.tbDanhSachHocOnLines
                                join l in db.tbLops on ds.lop_id equals l.lop_id
                                join hstl in db.tbHocSinhTrongLops on ds.hstl_id equals hstl.hstl_id
                                join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                                where l.lop_id == 23
                                orderby ds.tinhtrang descending
                                select new
                                {
                                    ds.dshoconline_lydo,
                                    hs.hocsinh_name,
                                    tinhtrang = ds.tinhtrang == 2 ? "Không học" : "Có học",
                                    l.lop_name

                                };
        rpDanhSach.DataBind();
    }
    protected void btnLop101_ServerClick(object sender, EventArgs e)
    {
        rpDanhSach.DataSource = from ds in db.tbDanhSachHocOnLines
                                join l in db.tbLops on ds.lop_id equals l.lop_id
                                join hstl in db.tbHocSinhTrongLops on ds.hstl_id equals hstl.hstl_id
                                join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                                where l.lop_id == 17
                                orderby ds.tinhtrang descending
                                select new
                                {
                                    ds.dshoconline_lydo,
                                    hs.hocsinh_name,
                                    tinhtrang = ds.tinhtrang == 2 ? "Không học" : "Có học",
                                    l.lop_name

                                };
        rpDanhSach.DataBind();
    }
    protected void btnLop111_ServerClick(object sender, EventArgs e)
    {
        rpDanhSach.DataSource = from ds in db.tbDanhSachHocOnLines
                                join l in db.tbLops on ds.lop_id equals l.lop_id
                                join hstl in db.tbHocSinhTrongLops on ds.hstl_id equals hstl.hstl_id
                                join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                                where l.lop_id == 24
                                orderby ds.tinhtrang descending
                                select new
                                {
                                    ds.dshoconline_lydo,
                                    hs.hocsinh_name,
                                    tinhtrang = ds.tinhtrang == 2 ? "Không học" : "Có học",
                                    l.lop_name

                                };
        rpDanhSach.DataBind();
    }
}