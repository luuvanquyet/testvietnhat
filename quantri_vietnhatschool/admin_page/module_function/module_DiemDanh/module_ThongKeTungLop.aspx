﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_ThongKeTungLop.aspx.cs" Inherits="admin_page_module_function_module_DiemDanh_module_ThongKeTungLop" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div>
        <%-- <asp:DropDownList ID="ddlLop" runat="server" AutoPostBack="true" CssClass="form-control">
        </asp:DropDownList>--%>
        <a href="#" id="btnLop1_1" runat="server" class="btn btn-primary" onserverclick="btnLop1_1_ServerClick">Lớp 1/1</a>
        <a href="#" id="btnLop1_2" runat="server" class="btn btn-primary" onserverclick="btnLop1_2_ServerClick">Lớp 1/2</a>
        <a href="#" id="btnLop1_3" runat="server" class="btn btn-primary" onserverclick="btnLop1_3_ServerClick">Lớp 1/3</a>
        <a href="#" id="btnLop1_4" runat="server" class="btn btn-primary" onserverclick="btnLop1_4_ServerClick">Lớp 1/4</a>
        <a href="#" id="btnLop2_1" runat="server" class="btn btn-primary" onserverclick="btnLop2_1_ServerClick">Lớp 2/1</a>
        <a href="#" id="btnLop2_2" runat="server" class="btn btn-primary" onserverclick="btnLop2_2_ServerClick">Lớp 2/2</a>
        <a href="#" id="btnLop2_3" runat="server" class="btn btn-primary" onserverclick="btnLop2_3_ServerClick">Lớp 2/3</a>
        <a href="#" id="btnLop2_4" runat="server" class="btn btn-primary" onserverclick="btnLop2_4_ServerClick">Lớp 2/4</a>
        <a href="#" id="btnLop3_1" runat="server" class="btn btn-primary" onserverclick="btnLop3_1_ServerClick">Lớp 3/1</a>
        <a href="#" id="btnLop3_2" runat="server" class="btn btn-primary" onserverclick="btnLop3_2_ServerClick">Lớp 3/2</a>
        <a href="#" id="btnLop3_3" runat="server" class="btn btn-primary" onserverclick="btnLop3_3_ServerClick">Lớp 3/3</a>
        <a href="#" id="btnLop3_4" runat="server" class="btn btn-primary" onserverclick="btnLop3_4_ServerClick">Lớp 3/4</a>
        <a href="#" id="btnLop3_5" runat="server" class="btn btn-primary" onserverclick="btnLop3_5_ServerClick">Lớp 3/5</a>
        <a href="#" id="btnLop4_1" runat="server" class="btn btn-primary" onserverclick="btnLop4_1_ServerClick">Lớp 4/1</a>
        <a href="#" id="btnLop4_2" runat="server" class="btn btn-primary" onserverclick="btnLop4_2_ServerClick">Lớp 4/2</a>
        <a href="#" id="btnLop5_1" runat="server" class="btn btn-primary" onserverclick="btnLop5_1_ServerClick">Lớp 5/1</a>
        <a href="#" id="btnLop6_1" runat="server" class="btn btn-primary" onserverclick="btnLop6_1_ServerClick">Lớp 6/1</a>
        <a href="#" id="btnLop7_1" runat="server" class="btn btn-primary" onserverclick="btnLop7_1_ServerClick">Lớp 7/1</a>
        <a href="#" id="btnLop7_2" runat="server" class="btn btn-primary" onserverclick="btnLop7_2_ServerClick">Lớp 7/2</a>
        <a href="#" id="btnLop8_1" runat="server" class="btn btn-primary" onserverclick="btnLop8_1_ServerClick">Lớp 8/1</a>
        <a href="#" id="btnLop10_1" runat="server" class="btn btn-primary" onserverclick="btnLop10_1_ServerClick">Lớp 10/1</a>
        <a href="#" id="btnLop11_1" runat="server" class="btn btn-primary" onserverclick="btnLop11_1_ServerClick">Lớp 11/1</a>
    </div>
    <br />
    <div>

        <table class="table" style="background-color: white">

            <thead>
                <tr>
                    <th scope="col">#</th>
                     <th scope="col">Lớp</th>
                    <th scope="col">Mã học sinh</th>
                    <th scope="col">Họ tên</th>
                    <asp:Repeater ID="rpParent" runat="server">
                        <ItemTemplate>
                            <th scope="col"><%# Eval("ngay", "{0:dd}") %></th>
                        </ItemTemplate>
                    </asp:Repeater>
                </tr>
            </thead>
            <tbody>
                <asp:Repeater ID="rpDanhSach" runat="server" OnItemDataBound="rpDanhSach_ItemDataBound">
                    <ItemTemplate>
                        <tr>
                            <th scope="row" style="text-align: center"><%=STT++ %></th>
                             <td><%#Eval("lop_name") %></td>
                            <td><%#Eval("hocsinh_code") %></td>
                            <td><%#Eval("hocsinh_name") %></td>
                            <asp:Repeater ID="rpDanhSachChild" runat="server">
                                <ItemTemplate>
                                    <td style="text-align: center"><%#Eval("ghichusuatan") %></td>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

