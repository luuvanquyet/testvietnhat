﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_DiemDanh_module_ThongKeTungLop : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public int STT = 1;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //var listNV = from l in db.tbLops
            //             join gvtl in db.tbGiaoVienTrongLops on l.lop_id equals gvtl.lop_id
            //             orderby l.khoi_id
            //             select l;
            //ddlLop.Items.Clear();
            //ddlLop.Items.Insert(0, "Chọn Lớp");
            //ddlLop.AppendDataBoundItems = true;
            //ddlLop.DataTextField = "lop_name";
            //ddlLop.DataValueField = "lop_id";
            //ddlLop.DataSource = listNV;
            //ddlLop.DataBind();
        }
        //if (ddlLop.SelectedValue == "0" || ddlLop.SelectedValue == "Chọn Lớp")
        //{
        //}
        //else
        //{
          
       // }
    }
    protected void rpDanhSach_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Repeater rpDanhSachChild = e.Item.FindControl("rpDanhSachChild") as Repeater;
        int hstl_id = int.Parse(DataBinder.Eval(e.Item.DataItem, "hstl_id").ToString());
        var listsuatan = from sa in db.tbDiemDanh_HocSinhs
                         where sa.hstl_id == hstl_id
                         && sa.diemdanh_ngay.Value.Month == DateTime.Now.Month
                         && sa.diemdanh_ngay.Value.Year == DateTime.Now.Year
                         select new {
                             ghichusuatan = sa.ghichusuatan==2?"X":""
                         };
        rpDanhSachChild.DataSource = listsuatan;
        rpDanhSachChild.DataBind();
         //ghichusuatan =(from ng in db.tbDiemDanh_HocSinhs
         //                                     where ng.hstl_id == hstl.hstl_id 
         //                                     && ng.diemdanh_ngay.Value.Month == DateTime.Now.Month 
         //                                     && ng.diemdanh_ngay.Value.Year == DateTime.Now.Year select ng).Count()>0?"X":""
    }

    protected void getData(int lop)
    {
        var checkNamHoc = (from nh in db.tbHoctap_NamHocs orderby nh.namhoc_id descending select nh).FirstOrDefault();
        var listParent = from ng in db.tbDiemDanh_HocSinhs
                         join hstl in db.tbHocSinhTrongLops on ng.hstl_id equals hstl.hstl_id
                         join l in db.tbLops on hstl.lop_id equals l.lop_id
                         where l.lop_id == lop && ng.diemdanh_ngay.Value.Month == DateTime.Now.Month
                         && ng.diemdanh_ngay.Value.Year == DateTime.Now.Year && hstl.namhoc_id == 1
                         group ng by ng.diemdanh_ngay.Value.Date into g
                         select new
                         {
                             ngay = g.First().diemdanh_ngay.Value.Date
                         };
        rpParent.DataSource = listParent;
        rpParent.DataBind();
        var dsHocSinh = from hstl in db.tbHocSinhTrongLops
                        join l in db.tbLops on hstl.lop_id equals l.lop_id
                        join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                        where l.lop_id == lop && hstl.namhoc_id == checkNamHoc.namhoc_id
                        select new
                        {
                            hstl.hstl_id,
                            hs.hocsinh_code,
                            hs.hocsinh_name,
                            l.lop_name
                        };
        rpDanhSach.DataSource = dsHocSinh;
        rpDanhSach.DataBind();
    }
    protected void btnLop1_1_ServerClick(object sender, EventArgs e)
    {
        getData(1);
    }
    protected void btnLop1_2_ServerClick(object sender, EventArgs e)
    {
        getData(2);
    }
    protected void btnLop1_3_ServerClick(object sender, EventArgs e)
    {
        getData(3);
    }
    protected void btnLop1_4_ServerClick(object sender, EventArgs e)
    {
        getData(4);
    }
    protected void btnLop2_1_ServerClick(object sender, EventArgs e)
    {
        getData(5);
    }
    protected void btnLop2_2_ServerClick(object sender, EventArgs e)
    {
        getData(6);
    }
    protected void btnLop2_3_ServerClick(object sender, EventArgs e)
    {
        getData(7);
    }
    protected void btnLop2_4_ServerClick(object sender, EventArgs e)
    {
        getData(8);
    }
    protected void btnLop3_1_ServerClick(object sender, EventArgs e)
    {
        getData(10);
    }
    protected void btnLop3_2_ServerClick(object sender, EventArgs e)
    {
        getData(11);
    }
    protected void btnLop3_3_ServerClick(object sender, EventArgs e)
    {
        getData(18);
    }
    protected void btnLop3_4_ServerClick(object sender, EventArgs e)
    {
        getData(19);
    }
    protected void btnLop3_5_ServerClick(object sender, EventArgs e)
    {
        getData(20);
    }
    protected void btnLop4_1_ServerClick(object sender, EventArgs e)
    {
        getData(12);
    }
    protected void btnLop4_2_ServerClick(object sender, EventArgs e)
    {
        getData(21);
    }
    protected void btnLop5_1_ServerClick(object sender, EventArgs e)
    {
        getData(13);
    }
    protected void btnLop6_1_ServerClick(object sender, EventArgs e)
    {
        getData(14);
    }
    protected void btnLop7_1_ServerClick(object sender, EventArgs e)
    {
        getData(16);
    }
    protected void btnLop7_2_ServerClick(object sender, EventArgs e)
    {
        getData(22);
    }
    
    protected void btnLop8_1_ServerClick(object sender, EventArgs e)
    {
        getData(23);
    }
    protected void btnLop10_1_ServerClick(object sender, EventArgs e)
    {
        getData(17);
    }
    protected void btnLop11_1_ServerClick(object sender, EventArgs e)
    {
        getData(24);
    }
}