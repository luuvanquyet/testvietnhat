﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_DownloadFile.aspx.cs" Inherits="admin_page_module_function_module_DownloadFile" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v17.1" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <script>
        function myNoiDungCuaGiaoVien(account_id) {
            document.getElementById("<%=txtUserName.ClientID%>").value = account_id;
             document.getElementById("<%=btnShow.ClientID%>").click();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div id="block_1" runat="server" class="card card-block">
        <h3>Đánh giá từ giáo viên</h3>
        <table class="table table-borderless" style="">
            <thead>
                <tr>
                    <th scope="col">STT</th>
                    <th scope="col">Nội dung file</th>
                    <th scope="col">Download</th>
                </tr>
            </thead>
            <tbody>
                <asp:Repeater ID="rpList" runat="server">
                    <ItemTemplate>
                        <tr style="text-align: center">
                            <th scope="row"><%#STT++ %></th>
                            <td><%#Eval("fildownload_content") %></td>
                            <td>
                                <a id="btnXem" style="color: white" download="<%#Eval("filedownload_name") %>"  class="btn btn-primary" href="#">Tải xuống</a>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
        </table>
    </div>
    <div style="display: none">
        <input type="text" id="txtUserName" runat="server" />
        <a id="btnShow" runat="server"></a>
    </div>
    
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

