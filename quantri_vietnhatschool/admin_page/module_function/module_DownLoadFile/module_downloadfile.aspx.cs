﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_DownloadFile : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    public int STT = 1;

    protected void Page_Load(object sender, EventArgs e)
    {
        rpList.DataSource = from l in db.tbFileDownLoads select l;
        rpList.DataBind();
        
    }
}