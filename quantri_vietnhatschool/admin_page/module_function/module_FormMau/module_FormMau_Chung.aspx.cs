﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_FormMau_module_FormMau_Chung : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    string image;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["_id"] = 0;
        }
        loadData();

    }
    private void loadData()
    {
        var getData = from n in db.tbQuanTri_FormMaus
                      where n.formau_loai=="file chung"
                      orderby n.formmau_id descending
                      select n;
        grvList.DataSource = getData;
        grvList.DataBind();
        //ddlloaisanpham.DataSource = from tb in db.tbWebsite_NewsCates
        //                            select tb;
        //ddlloaisanpham.DataBind();

    }
    private void setNULL()
    {
        txtTieuDe.Text = "";
       // txtGhiChu.Text = "";
        //imgPreview.Src = "";
    }
    protected void btnThem_Click(object sender, EventArgs e)
    {
        Session["_id"] = 0;
        setNULL();
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupControl.Show();", true);
    }
    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "formmau_id" }));
        Session["_id"] = _id;
        var getData = (from n in db.tbQuanTri_FormMaus
                       where n.formmau_id == _id
                       select n).Single();
        txtTieuDe.Text = getData.formmau_title;
       // txtGhiChu.Text = getData.formau_ghichu;
        if (getData.formau_file == null)
            ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupControl.Show();showImg1_1('" + "/admin_images/Preview-icon.png" + "'); ", true);
        else
            image = getData.formau_file;
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupControl.Show();showImg1_1('" + getData.formau_file + "'); ", true);
        // loadData();
    }
    protected void btnXoa_Click(object sender, EventArgs e)
    {
        cls_formmau cls;
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "formmau_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                cls = new cls_formmau();
                tbQuanTri_FormMau checkImage = (from i in db.tbQuanTri_FormMaus where i.formmau_id == Convert.ToInt32(item) select i).SingleOrDefault();
                if (cls.Linq_Xoa(Convert.ToInt32(item)))
                {
                    alert.alert_Success(Page, "Xóa thành công", "");
                    ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "grvList.Refresh(); ", true);
                }
                else
                    alert.alert_Error(Page, "Xóa thất bại", "");
            }
        }
        else
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
      
    }
    public bool checknull()
    {
        if (txtTieuDe.Text != "")
            return true;
        else return false;
    }
    protected void btnLuu_Click(object sender, EventArgs e)
    {

        if (Page.IsValid && FileUpload1.HasFile)
        {
            String folderUser = Server.MapPath("~/uploadimages/file-mau/chung/");
            if (!Directory.Exists(folderUser))
            {
                Directory.CreateDirectory(folderUser);
            }
            //string filename;
            string ulr = "/uploadimages/file-mau/chung/";
            HttpFileCollection hfc = Request.Files;
            //string filename = Path.GetRandomFileName() + Path.GetExtension(FileUpload1.FileName);
            //string fileName_save = Path.Combine(Server.MapPath("~/uploadimages/file-mau"), filename);
            string filename = Path.GetFileName(FileUpload1.FileName);
            string fileName_save = Path.Combine(Server.MapPath("~/uploadimages/file-mau/chung/"), filename);
            FileUpload1.SaveAs(fileName_save);
            image = ulr + filename;
        }
        cls_formmau cls = new cls_formmau();
        if (checknull() == false)
            alert.alert_Warning(Page, "Hãy nhập đầy đủ thông tin!", "");
        else
        {
            if (Session["_id"].ToString() == "0")
            {
              
                if (image == null)
                {
                    image = "/images/790x525.jpg";
                }
                else
                {
                }
                if (cls.Linq_Them(txtTieuDe.Text, image))
                    alert.alert_Success(Page, "Thêm thành công", "");
                else alert.alert_Error(Page, "Thêm thất bại", "");

            }
            else
            {
                if (cls.Linq_Sua(Convert.ToInt32(Session["_id"].ToString()), txtTieuDe.Text, image))
                    alert.alert_Success(Page, "Cập nhật thành công", "");
                else alert.alert_Error(Page, "Cập nhật thất bại", "");
            }
           // popupControl.ShowOnPageLoad = false;
            loadData();
        }
    }

    //protected void btnDownload_Click(object sender, EventArgs e)
    //{
    //    cls_formmau cls;
    //    List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "formmau_id" });
    //    if (selectedKey.Count > 0)
    //    {
    //        foreach (var item in selectedKey)
    //        {
    //            tbQuanTri_FormMau dow = (from d in db.tbQuanTri_FormMaus where d.formmau_id == Convert.ToInt32(item) select d).Single();
    //            if (dow != null)
    //            {
    //                Response.ContentType = "Application/pdf";
    //                Response.AppendHeader("Content-Disposition", "attachment; filename=formmau.pdf");
    //                Response.TransmitFile(Server.MapPath(dow.formau_file));
    //                Response.End();
    //                //alert.alert_Success(Page, "Xóa thành công", "");
    //            }
    //        }
    //    }
    //    else
    //        alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
    //}
}