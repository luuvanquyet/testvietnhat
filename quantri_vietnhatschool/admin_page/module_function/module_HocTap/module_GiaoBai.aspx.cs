﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_GiaoBai : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    public string image;

    protected void Page_Load(object sender, EventArgs e)
    {
        edtnoidung.Toolbars.Add(HtmlEditorToolbar.CreateStandardToolbar1());
        if (!IsPostBack)
        {
            if (Request.Cookies["UserName"].Value != "")
            {
                var listNV = from nv in db.tbMonHocs select nv;
                ddlMonHoc.Items.Clear();
                ddlMonHoc.Items.Insert(0, "Chọn môn học");
                ddlMonHoc.AppendDataBoundItems = true;
                ddlMonHoc.DataTextField = "monhoc_name";
                ddlMonHoc.DataValueField = "monhoc_id";
                ddlMonHoc.DataSource = listNV;
                ddlMonHoc.DataBind();
            }
        }
    }
    protected void btnLuu_Click(object sender, EventArgs e)
    {
        try
        {
            if (Page.IsValid && FileUpload1.HasFile)
            {
                String folderUser = Server.MapPath("~/FileUploads/");
                if (!Directory.Exists(folderUser))
                {
                    Directory.CreateDirectory(folderUser);
                }
                //string filename;
                string ulr = "/FileUploads/";
                HttpFileCollection hfc = Request.Files;
                string filename = Path.GetRandomFileName() + Path.GetExtension(FileUpload1.FileName);
                string fileName_save = Path.Combine(Server.MapPath("~/FileUploads"), filename);
                FileUpload1.SaveAs(fileName_save);
                image = ulr + filename;
            }
            var giaovien = (from ad in db.admin_Users
                            join gvl in db.tbGiaoVienTrongLops on ad.username_id equals gvl.taikhoan_id
                            where ad.username_username == Request.Cookies["UserName"].Value
                            select new
                                {
                                    gvl.lop_id,
                                    ad.username_id
                                }).SingleOrDefault();
            tbHoSo_GiaoBaiTap insert = new tbHoSo_GiaoBaiTap();
            insert.giaovien_id = giaovien.username_id;
            insert.monhoc_id = Convert.ToInt32(ddlMonHoc.SelectedValue);
            insert.lop_id = giaovien.lop_id;
            insert.giaobaitap_content = edtnoidung.Html;
            insert.giaobaitap_createdate = DateTime.Now;
            insert.giaobaitap_image = image;
            db.tbHoSo_GiaoBaiTaps.InsertOnSubmit(insert);
            db.SubmitChanges();
            alert.alert_Success(Page, "Lưu thành công", "");
        }
        catch(Exception ex)
        {
            alert.alert_Error(Page, "Lỗi", "");
        }
    }
}