﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_GiaoBaiTap.aspx.cs" Inherits="admin_page_module_function_module_GiaoBaiTapOnline_module_GiaoBaiTap" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <script src="../../../admin_js/sweetalert.min.js"></script>
    <style>
        .section-info {
            margin-top: 25px;
        }

            .section-info .title-info {
                font-size: 30px;
                margin-bottom: 20px;
                font-weight: 600;
                color: var(--color-primary);
            }

        .form-group__title {
            min-width: 130px;
            font-size: 16px;
        }

        .section-info .form-group {
            display: flex;
            margin-bottom: 20px;
            flex-direction: column;
        }

            .section-info .form-group input {
                flex: 1;
                background-color: transparent;
                outline: none;
                border: none;
                border-bottom: 1px solid #ccc;
            }

                .section-info .form-group input[type="date"] {
                    width: 150px;
                }

        .section-info .nav-pills .nav-link.active, .section-info .nav-pills .show > .nav-link {
            margin-bottom: 10px;
            display: block;
            text-indent: 10px;
            background-color: var(--color-primary);
            color: #fff !important;
        }

        .send-btn {
            display: flex;
            margin-bottom: 20px;
        }

            .send-btn .btn--modifier {
                margin: 0 20px;
            }

        .form--modifier {
            width: 30%;
            margin-right: 20px;
        }

        .bt-content {
            text-align: center;
            background-color: #fff;
        }

        .seen-content {
            border: 1px solid #ccc;
            padding: 20px;
        }

            .seen-content .seen--label {
                display: block;
                /*text-indent:15px;*/
            }

        .custom-file .custom-file-label {
            width: 30%;
        }

        .send-btn input[type="text"].dxeEditArea_Moderno, .send-btn input[type="password"].dxeEditArea_Moderno {
            margin-top: -5px;
            margin-bottom: 1px;
        }

        .modal-header .close {
            margin-top: -25px;
        }
    </style>
    <script> function showPreview1(input) {
            if (input.files && input.files[0]) {
                var filerdr = new FileReader();
                filerdr.onload = function (e) {
                    $('#imgPreview1').attr('src', e.target.result);
                }
                filerdr.readAsDataURL(input.files[0]);
            }
        }</script>
    <script type="text/javascript">
        function clickavatar1() {
            $("#up1 input[type=file]").click();
        }
    </script>
    <h1 style="text-align: center; margin-bottom: 20px;">Giao Bài Tập</h1>
    <asp:UpdatePanel runat="server" ID="upCbb">
        <ContentTemplate>
    <div class="send-btn">
        <asp:DropDownList CssClass="form-control form--modifier" ID="ddlKhoiLop" runat="server" AutoPostBack="true" DataValueField="khoi_id" DataTextField="khoi_name" NullText="Chọn Khối"></asp:DropDownList>
        <dx:ASPxComboBox CssClass="form-control form--modifier" ID="ddlMonHoc" runat="server" AutoPostBack="true" ValueField="monhoc_id" TextField="monhoc_name" NullText="Chọn Môn Học"></dx:ASPxComboBox>
    </div>
          
    <div class="col-12">
        <div class="col-12 form-group">
            <div class="colum-5 form-group">
                <label class="form-control-label">File upload :</label>
                <div id="up1" class="">
                    <asp:FileUpload CssClass="hidden-xs-up" ID="FileUpload1" runat="server" onchange="showPreview1(this)" />
                    <button type="button" class="btn-chang" onclick="clickavatar1()">
                        <img id="imgPreview1" src="/admin_images/up-img.png" style="max-width: 100%; height: 200px" />
                    </button>
                </div>
            </div>
        </div>
    </div>
              </ContentTemplate>
    </asp:UpdatePanel>
    <asp:CheckBox Text="Đây là bài tập thêm" ID="cbox_bt" runat="server" />
    <h3>Nhập nội dung bài tập</h3>

    <div class="form-group row">
        <div class="col-12 form-group">
            <div class="col-12">
                <dx:ASPxHtmlEditor ID="edtnoidung" ClientInstanceName="edtnoidung" runat="server" Width="100%" Height="400px" Border-BorderStyle="Solid" Border-BorderWidth="1px" Border-BorderColor="#dddddd">
                    <SettingsHtmlEditing AllowIFrames="true" AllowYouTubeVideoIFrames="True" EnablePasteOptions="true" />
                    <Settings AllowHtmlView="true" AllowContextMenu="Default" />
                    <settingsimageupload uploadfolder="~/editorimages"></settingsimageupload>
                    <Toolbars>
                        <dx:HtmlEditorToolbar>
                            <Items>
                                <dx:ToolbarCustomCssEdit Width="120px">
                                    <Items>
                                        <dx:ToolbarCustomCssListEditItem TagName="" Text="Clear Style" CssClass="" />
                                        <dx:ToolbarCustomCssListEditItem TagName="H1" Text="Title" CssClass="CommonTitle">
                                            <PreviewStyle CssClass="CommonTitlePreview" />
                                        </dx:ToolbarCustomCssListEditItem>
                                        <dx:ToolbarCustomCssListEditItem TagName="H3" Text="Header1" CssClass="CommonHeader1">
                                            <PreviewStyle CssClass="CommonHeader1Preview" />
                                        </dx:ToolbarCustomCssListEditItem>
                                        <dx:ToolbarCustomCssListEditItem TagName="H4" Text="Header2" CssClass="CommonHeader2">
                                            <PreviewStyle CssClass="CommonHeader2Preview" />
                                        </dx:ToolbarCustomCssListEditItem>
                                        <dx:ToolbarCustomCssListEditItem TagName="Div" Text="Content" CssClass="CommonContent">
                                            <PreviewStyle CssClass="CommonContentPreview" />
                                        </dx:ToolbarCustomCssListEditItem>
                                        <dx:ToolbarCustomCssListEditItem TagName="Strong" Text="Features" CssClass="CommonFeatures">
                                            <PreviewStyle CssClass="CommonFeaturesPreview" />
                                        </dx:ToolbarCustomCssListEditItem>
                                        <dx:ToolbarCustomCssListEditItem TagName="Div" Text="Footer" CssClass="CommonFooter">
                                            <PreviewStyle CssClass="CommonFooterPreview" />
                                        </dx:ToolbarCustomCssListEditItem>
                                        <dx:ToolbarCustomCssListEditItem TagName="" Text="Link" CssClass="Link">
                                            <PreviewStyle CssClass="LinkPreview" />
                                        </dx:ToolbarCustomCssListEditItem>
                                        <dx:ToolbarCustomCssListEditItem TagName="EM" Text="ImageTitle" CssClass="ImageTitle">
                                            <PreviewStyle CssClass="ImageTitlePreview" />
                                        </dx:ToolbarCustomCssListEditItem>
                                        <dx:ToolbarCustomCssListEditItem TagName="" Text="ImageMargin" CssClass="ImageMargin">
                                            <PreviewStyle CssClass="ImageMarginPreview" />
                                        </dx:ToolbarCustomCssListEditItem>
                                    </Items>
                                </dx:ToolbarCustomCssEdit>
                                <dx:ToolbarParagraphFormattingEdit>
                                    <Items>
                                        <dx:ToolbarListEditItem Text="Normal" Value="p" />
                                        <dx:ToolbarListEditItem Text="Heading  1" Value="h1" />
                                        <dx:ToolbarListEditItem Text="Heading  2" Value="h2" />
                                        <dx:ToolbarListEditItem Text="Heading  3" Value="h3" />
                                        <dx:ToolbarListEditItem Text="Heading  4" Value="h4" />
                                        <dx:ToolbarListEditItem Text="Heading  5" Value="h5" />
                                        <dx:ToolbarListEditItem Text="Heading  6" Value="h6" />
                                        <dx:ToolbarListEditItem Text="Address" Value="address" />
                                        <dx:ToolbarListEditItem Text="Normal (DIV)" Value="div" />
                                    </Items>
                                </dx:ToolbarParagraphFormattingEdit>
                                <dx:ToolbarFontNameEdit>
                                    <Items>
                                        <dx:ToolbarListEditItem Value="Times New Roman" Text="Times New Roman"></dx:ToolbarListEditItem>
                                        <dx:ToolbarListEditItem Value="Tahoma" Text="Tahoma"></dx:ToolbarListEditItem>
                                        <dx:ToolbarListEditItem Value="Verdana" Text="Verdana"></dx:ToolbarListEditItem>
                                        <dx:ToolbarListEditItem Value="Arial" Text="Arial"></dx:ToolbarListEditItem>
                                        <dx:ToolbarListEditItem Value="MS Sans Serif" Text="MS Sans Serif"></dx:ToolbarListEditItem>
                                        <dx:ToolbarListEditItem Value="Courier" Text="Courier"></dx:ToolbarListEditItem>
                                        <dx:ToolbarListEditItem Value="bodoni MT" Text="bodoni MT"></dx:ToolbarListEditItem>
                                    </Items>
                                </dx:ToolbarFontNameEdit>
                                <dx:ToolbarFontSizeEdit>
                                    <Items>
                                        <dx:ToolbarListEditItem Value="1" Text="1 (8pt)"></dx:ToolbarListEditItem>
                                        <dx:ToolbarListEditItem Value="2" Text="2 (10pt)"></dx:ToolbarListEditItem>
                                        <dx:ToolbarListEditItem Value="3" Text="3 (12pt)"></dx:ToolbarListEditItem>
                                        <dx:ToolbarListEditItem Value="4" Text="4 (14pt)"></dx:ToolbarListEditItem>
                                        <dx:ToolbarListEditItem Value="5" Text="5 (18pt)"></dx:ToolbarListEditItem>
                                        <dx:ToolbarListEditItem Value="6" Text="6 (24pt)"></dx:ToolbarListEditItem>
                                        <dx:ToolbarListEditItem Value="7" Text="7 (36pt)"></dx:ToolbarListEditItem>
                                    </Items>
                                </dx:ToolbarFontSizeEdit>
                                <dx:ToolbarBoldButton BeginGroup="True" />
                                <dx:ToolbarItalicButton />
                                <dx:ToolbarUnderlineButton />
                                <dx:ToolbarStrikethroughButton />
                                <dx:ToolbarJustifyLeftButton BeginGroup="True" />
                                <dx:ToolbarJustifyCenterButton />
                                <dx:ToolbarJustifyRightButton />
                                <dx:ToolbarJustifyFullButton />
                                <dx:ToolbarBackColorButton BeginGroup="True" />
                                <dx:ToolbarFontColorButton />
                                <dx:ToolbarInsertYouTubeVideoDialogButton>
                                </dx:ToolbarInsertYouTubeVideoDialogButton>
                            </Items>
                        </dx:HtmlEditorToolbar>
                    </Toolbars>
                </dx:ASPxHtmlEditor>
            </div>
        </div>
    </div>
    <%--<asp:UpdatePanel runat="server" ID="udSave">
        <ContentTemplate>--%>
    <div class="mar_but button">
        <asp:Button ID="btnLuu" runat="server" ClientIDMode="Static" Text="Lưu" CssClass="btn btn-primary" OnClientClick="return checkNULL()" OnClick="btnLuu_Click" />
    </div>
          <%--  </ContentTemplate>
    </asp:UpdatePanel>--%>
    <%--  <div class="bt-content">
        <label> Nhập Nội Dung Bài Tập</label>
        
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nộp Bài</th>
                    <th scope="col">Ngày</th>
                    <th scope="col">#</th>
                    <th scope="col">Điểm</th>
                    <th scope="col">Edit</th>
                    <th scope="col">Phản Hồi Phụ Huynh</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th scope="row">1</th>
                    <td>Nộp Bài</td>
                    <td>13/02/2019</td>
                    <td>download</td>
                    <td>
                        <input type="text" name="name" value="" />
                    </td>
                    <td><a href="#" class="btn btn-primary">Lưu </a></td>
                    <td><a href="#" class="btn btn-success" data-toggle="modal" data-target="#seenPH">Xem </a></td>
                </tr>
                <tr>
                    <th scope="row">2</th>
                    <td>Nộp Bài</td>
                    <td>13/02/2019</td>
                    <td>download</td>
                    <td>
                        <input type="text" name="name" value="" />
                    </td>
                    <td><a href="#" class="btn btn-primary">Lưu </a></td>
                    <td><a href="#" class="btn btn-success">Xem </a></td>
                </tr>
                <tr>
                    <th scope="row">3</th>
                    <td>Nộp Bài</td>
                    <td>13/02/2019</td>
                    <td>download</td>
                    <td>
                        <input type="text" name="name" value="" />
                    </td>
                    <td><a href="#" class="btn btn-primary">Lưu </a></td>
                    <td><a href="#" class="btn btn-success">Xem </a></td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="modal fade" id="seenPH" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Phản Hồi Phụ Huynh</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="seen-content">
                        <label class="seen--label">Nội Dung</label>


                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Lưu</button>
                </div>
            </div>
        </div>
    </div>--%>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

