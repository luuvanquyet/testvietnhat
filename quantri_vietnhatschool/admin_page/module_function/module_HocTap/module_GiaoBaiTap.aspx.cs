﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_GiaoBaiTapOnline_module_GiaoBaiTap : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public string image;
    cls_Alert alert = new cls_Alert();
    protected void Page_Load(object sender, EventArgs e)
    {
        edtnoidung.Toolbars.Add(HtmlEditorToolbar.CreateStandardToolbar1());
        string username = Request.Cookies["UserName"].Value;
        var giaovien = from user in db.admin_Users
                       where user.username_username == username
                       select user;
        if (giaovien.Single().username_username == "root")
        {
            ddlKhoiLop.DataSource = from kh in db.tbKhois
                                    select kh;
            ddlKhoiLop.DataBind();
        }
        else
        {
            
            var giaovien_Khoi = from gv in db.tbGiaoViens
                              join k in db.tbKhois on gv.khoi_id equals k.khoi_id
                              where gv.username_id == giaovien.Single().username_id
                              select new {
                                  k.khoi_name,
                                  k.khoi_id,
                              };
            ddlKhoiLop.DataSource = giaovien_Khoi;
            ddlKhoiLop.DataBind();
        }
        ddlMonHoc.DataSource = from mh in db.tbMonHocs
                               select mh;
        ddlMonHoc.DataBind();

    }

    protected void btnLuu_Click(object sender, EventArgs e)
    {
      
        string username = Request.Cookies["UserName"].Value;
       
        var giaovien = from user in db.admin_Users
                       where user.username_username == username
                       select user;
        var giaoVien_Id = from gv in db.tbGiaoViens
                          where gv.username_id == giaovien.Single().username_id
                          select gv;
        if (Page.IsValid && FileUpload1.HasFile)
        {
            String folderUser = Server.MapPath("~/FileUploads/");
            if (!Directory.Exists(folderUser))
            {
                Directory.CreateDirectory(folderUser);
            }
            //string filename;
            string url_ = HttpContext.Current.Request.Url.AbsoluteUri;
            string ulr = "http://quantri.vietnhatschool.edu.vn/admin-home/FileUploads/";
            HttpFileCollection hfc = Request.Files;
            string filename = Path.GetRandomFileName() + Path.GetExtension(FileUpload1.FileName);
            string fileName_save = Path.Combine(Server.MapPath("~/FileUploads"), filename);
            FileUpload1.SaveAs(fileName_save);
            image = ulr + filename;
            tbHoSo_GiaoBaiTap ins = new tbHoSo_GiaoBaiTap();
            ins.giaobaitap_filebaitap = image;
            ins.giaobaitap_content = edtnoidung.Html;
            if (cbox_bt.Checked == true)
            {
                ins.giaobaitap_chinhorphu = false;
            }
            else
            {
                ins.giaobaitap_chinhorphu = true;
            }
            ins.giaovien_id = giaoVien_Id.Single().giaovien_id ;
            ins.giaobaitap_createdate = DateTime.Now;
            ins.monhoc_id = Convert.ToInt32(ddlMonHoc.SelectedItem.Value);
            ins.khoi_id = Convert.ToInt32(ddlKhoiLop.SelectedValue);
            db.tbHoSo_GiaoBaiTaps.InsertOnSubmit(ins);
            try
            {
                db.SubmitChanges();
                alert.alert_Success(Page, "Thao Tác Thành Công!", "");
            }
            catch { }

        }
        else
        {
            alert.alert_Error(Page, "thêm file không được", "");
        }

    }
}