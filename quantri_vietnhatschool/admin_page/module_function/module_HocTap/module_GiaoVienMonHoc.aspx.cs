﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_GiaoVienMonHoc : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {

       
            if (!IsPostBack)
            {
                Session["_id"] = 0;
            }
            loadData();

    }
    private void loadData()
    {
        var getData = from tb in db.admin_Users
                      join u in db.tbHocTap_GiaoVien_MonHocs on tb.username_id equals u.giaovien_id
                      join m in db.tbMonHocs on u.monhoc_id equals m.monhoc_id
                      select new { 
                      tb.username_fullname,
                      u.gvmh_id,
                      m.monhoc_name
                      };
        grvList.DataSource = getData;
        grvList.DataBind();
        ddlGiaoVien.DataSource = from tb in db.admin_Users
                                 where tb.groupuser_id==3
                                    select tb;
        ddlGiaoVien.DataBind();
        ddlMonHoc.DataSource = from tb in db.tbMonHocs
                                 select tb;
        ddlMonHoc.DataBind();
    }
    private void setNULL()
    {
       
    }
   
    protected void btnThem_Click(object sender, EventArgs e)
    {
        Session["_id"] = 0;
        setNULL();
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupControl.Show();", true);
    }
    protected void btnChiTiet_Click(object sender, EventArgs e)
    {

        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "gvmh_id" }));
        Session["_id"] = _id;

        var getData = (from tb in db.admin_Users
                       join u in db.tbHocTap_GiaoVien_MonHocs on tb.username_id equals u.giaovien_id
                       join m in db.tbMonHocs on u.monhoc_id equals m.monhoc_id
                       where u.gvmh_id == _id
                       select new
                       {
                           tb.username_fullname,
                           u.gvmh_id,
                           m.monhoc_name
                       }).Single();
        ddlGiaoVien.Text = getData.username_fullname;
        ddlMonHoc.Value = getData.monhoc_name;
       
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupControl.Show();", true);
    }
    protected void btnXoa_Click(object sender, EventArgs e)
    {
        cls_GiaoVienMonHoc cls;
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "gvmh_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                cls = new cls_GiaoVienMonHoc();
                if (cls.Linq_Xoa(Convert.ToInt32(item)))
                    alert.alert_Success(Page, "Xóa thành công", "");
                else
                    alert.alert_Error(Page, "Xóa thất bại", "");
            }
        }
        else
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
    }
    protected void btnLuu_Click(object sender, EventArgs e)
    {
        cls_GiaoVienMonHoc cls = new cls_GiaoVienMonHoc();
            if (Session["_id"].ToString() == "0")
            {
                if (cls.Linq_Them(Convert.ToInt32(ddlGiaoVien.Value),Convert.ToInt32(ddlMonHoc.Value)))
                    alert.alert_Success(Page, "Thêm thành công", "");
                else alert.alert_Error(Page, "Thêm thất bại", "");
            }
            else
            {
                if (cls.Linq_Sua(Convert.ToInt32(Session["_id"].ToString()), Convert.ToInt32(ddlGiaoVien.Value),Convert.ToInt32(ddlMonHoc.Value)))
                    alert.alert_Success(Page, "Cập nhật thành công", "");
                else alert.alert_Error(Page, "Cập nhật thất bại", "");
            }
    }
}