﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_WebSite_module_QuanLyBoPhan : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["_id"] = 0;
            var listNV = from nv in db.tbBoPhans where nv.hidden==true select nv;
            ddlBoPhan.Items.Clear();
            ddlBoPhan.Items.Insert(0, "Chọn bộ phận");
            ddlBoPhan.AppendDataBoundItems = true;
            ddlBoPhan.DataTextField = "bophan_name";
            ddlBoPhan.DataValueField = "bophan_id";
            ddlBoPhan.DataSource = listNV;
            ddlBoPhan.DataBind();
        }
        loadData();
    }
    private void loadData()
    {
        if (ddlBoPhan.SelectedValue == "0" || ddlBoPhan.SelectedValue == "Chọn bộ phận")
        {

        }
        else
        {
            // load data đổ vào var danh sách
            var getData = from nc in db.admin_Users
                          where nc.bophan_id == Convert.ToInt32(ddlBoPhan.SelectedValue)
                          select nc;
            // đẩy dữ liệu vào gridivew
            grvList.DataSource = getData;
            grvList.DataBind();
        }
    }
  
}