﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_WebSite_module_QuanLyHocTapHocSinh : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["_id"] = 0;
            var listNV = from hs in db.tbHocSinhs
                         join hstl in db.tbHocSinhTrongLops on hs.hocsinh_id equals hstl.hocsinh_id
                         join l in db.tbLops on hstl.lop_id equals l.lop_id
                         join gvtl in db.tbGiaoVienTrongLops on l.lop_id equals gvtl.lop_id
                         join a in db.admin_Users on gvtl.taikhoan_id equals a.username_id
                         where a.username_username == Request.Cookies["UserName"].Value
                         select hs;
            ddlHocSinh.Items.Clear();
            ddlHocSinh.Items.Insert(0, "Chọn Học sinh");
            ddlHocSinh.AppendDataBoundItems = true;
            ddlHocSinh.DataTextField = "hocsinh_name";
            ddlHocSinh.DataValueField = "hocsinh_code";
            ddlHocSinh.DataSource = listNV;
            ddlHocSinh.DataBind();
        }
        loadData();
    }
    private void loadData()
    {
        if (ddlHocSinh.SelectedValue == "0" || ddlHocSinh.SelectedValue == "Chọn Lớp")
        {

        }
        else
        {
            // load data đổ vào var danh sách
            var getData = from kq in db.tbTracNghiem_TestResults
                          join hs in db.tbHocSinhs on kq.student_code equals hs.hocsinh_code
                          where kq.student_code == (ddlHocSinh.SelectedValue)
                          select new { 
                           hs.hocsinh_name,
                           kq.result_id,
                           kq.result_date,
                           kq.result_all,
                           kq.baitap_name,
                           kq.result_time
                          };
            // đẩy dữ liệu vào gridivew
            grvList.DataSource = getData;
            grvList.DataBind();
        }
    }

}