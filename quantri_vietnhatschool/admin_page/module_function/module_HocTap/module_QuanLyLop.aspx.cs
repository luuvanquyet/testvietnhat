﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_WebSite_module_QuanLyLop : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["_id"] = 0;
            //lấy thông tin của tk đang nhập
            var getuser = (from u in db.admin_Users
                           where u.username_username == Request.Cookies["UserName"].Value
                           select u).FirstOrDefault();
            if (getuser.username_id == 1)
            {
                var list = from l in db.tbLops orderby l.khoi_id 
                           select l;
                ddlLop.Items.Clear();
                ddlLop.Items.Insert(0, "Chọn Lớp");
                ddlLop.AppendDataBoundItems = true;
                ddlLop.DataTextField = "lop_name";
                ddlLop.DataValueField = "lop_id";
                ddlLop.DataSource = list;
                ddlLop.DataBind();
            }
            else
            {
                var listNV = from l in db.tbLops
                             join gvtl in db.tbGiaoVienTrongLops on l.lop_id equals gvtl.lop_id
                             join a in db.admin_Users on gvtl.taikhoan_id equals a.username_id
                             where a.username_id == getuser.username_id
                             select l;
                ddlLop.Items.Clear();
                ddlLop.Items.Insert(0, "Chọn Lớp");
                ddlLop.AppendDataBoundItems = true;
                ddlLop.DataTextField = "lop_name";
                ddlLop.DataValueField = "lop_id";
                ddlLop.DataSource = listNV;
                ddlLop.DataBind();
            }
            var listlenlop = from l in db.tbLops
                       select l;
            ddlLenLop.Items.Clear();
            ddlLenLop.Items.Insert(0, "Chọn Lớp");
            ddlLenLop.AppendDataBoundItems = true;
            ddlLenLop.DataTextField = "lop_name";
            ddlLenLop.DataValueField = "lop_id";
            ddlLenLop.DataSource = listlenlop;
            ddlLenLop.DataBind();
        }
        loadData();
    }
    private void loadData()
    {
        var checkNamHoc = (from nh in db.tbHoctap_NamHocs orderby nh.namhoc_id descending select nh).FirstOrDefault();
        if (ddlLop.SelectedValue == "0" || ddlLop.SelectedValue == "Chọn Lớp")
        {
            //alert.alert_Warning(Page,"Vui lòng chọn lớp trước","");
        }
        else
        {
            // load data đổ vào var danh sách
            var getData = from l in db.tbLops
                          join hstl in db.tbHocSinhTrongLops on l.lop_id equals hstl.lop_id
                          join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                          where l.lop_id == Convert.ToInt32(ddlLop.SelectedValue)
                           && hstl.namhoc_id == checkNamHoc.namhoc_id
                          orderby hstl.position
                          select new { 
                          hs.hocsinh_id,
                          hs.hocsinh_name,
                          hs.hocsinh_code,
                          hstl.position
                          };
            // đẩy dữ liệu vào gridivew
            grvList.DataSource = getData;
            grvList.DataBind();
            lblTongHocSinh.Text = getData.Count() + "";
        }
    }


    protected void btnLenLop_Click(object sender, EventArgs e)
    {
        //duyệt qua ds hs và thêm vào tbhocsinhtronglop với năm học sau
        var checkNamHoc = (from nh in db.tbHoctap_NamHocs orderby nh.namhoc_id descending select nh).First();
      
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "hocsinh_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                
                
                //cập nhật lại trạng thái active = false đối với hs
                //tức là hs đã học xong lớp đó rồi thì ẩn đi
                //tbHocSinhTrongLop update = db.tbHocSinhTrongLops.Where(x => x.hstl_id == getNamHoc.hstl_id).FirstOrDefault();
                //update.active = false;
                //db.SubmitChanges();
                //thêm học sinh đó với lớp mới (lên lớp)
                tbHocSinhTrongLop insert = new tbHocSinhTrongLop();
                insert.hocsinh_id = Convert.ToInt32(item);
                insert.lop_id =Convert.ToInt32(ddlLenLop.SelectedValue);
                insert.namhoc_id = checkNamHoc.namhoc_id;
                //insert.kyhoc = 1;
                //insert.namhoc_id = getNamHoc.namhoc_id + 1;
                //insert.ngaybatdauhoc = DateTime.Now;
                //insert.active = true;
                db.tbHocSinhTrongLops.InsertOnSubmit(insert);
                db.SubmitChanges();
            }
            ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Đã cho học sinh lên lớp!','','success').then(function(){grvList.UnselectRows();})", true);
        }
        else
        {
            alert.alert_Error(Page, "Bạn chưa chọn dữ liệu", "");
        }
    }

    protected void btnRutHoSo_Click(object sender, EventArgs e)
    {
        //ẩn ds hs được chọn
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "hocsinh_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var hs in selectedKey)
            {
                var getData = from hstl in db.tbHocSinhTrongLops
                              where hstl.hocsinh_id == Convert.ToInt32(hs)
                              && hstl.lop_id == Convert.ToInt32(ddlLop.SelectedValue)
                              select hstl;
                foreach (var item in getData)
                {
                    tbHocSinhTrongLop update = db.tbHocSinhTrongLops.Where(x => x.hstl_id == item.hstl_id).FirstOrDefault();
                    //update.active = false;
                    update.hidden = true;
                    db.SubmitChanges();
                }
            }
            ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Rút hồ sơ học sinh thành công!','','success').then(function(){grvList.UnselectRows();})", true);
        }
        else
        {
            alert.alert_Error(Page, "Bạn chưa chọn dữ liệu", "");
        }
    }
}