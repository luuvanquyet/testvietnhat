﻿<%@ Page Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_QuanLyLop_GiaoVien.aspx.cs" Inherits="admin_page_module_function_module_WebSite_module_QuanLyLop_GiaoVien" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v17.1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <script type="text/javascript">
        function func() {
            grvList.Refresh();
        }
    </script>
    <div class="card card-block">
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <div class="form-group row">
                    Tổng học sinh trong <%=Lop %> là:
                        <asp:Label ID="lblTongHocSinh" runat="server"></asp:Label>
                </div>

                <div class="form-group table-responsive">
                    <dx:ASPxGridView ID="grvList" runat="server" ClientInstanceName="grvList" KeyFieldName="hocsinh_id" Width="100%">
                        <Columns>
                            <dx:GridViewCommandColumn ShowSelectCheckbox="True" SelectAllCheckboxMode="Page" VisibleIndex="0" Width="5%">
                            </dx:GridViewCommandColumn>
                            <dx:GridViewDataColumn Caption="Tài khoản" FieldName="hocsinh_code" HeaderStyle-HorizontalAlign="Center" Width="20%"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn Caption="Tên học sinh" FieldName="hocsinh_name" HeaderStyle-HorizontalAlign="Center" Width="20%"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn Caption="Tên ba" FieldName="hocsinh_tenba" HeaderStyle-HorizontalAlign="Center" Width="20%"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn Caption="SĐT ba" FieldName="hocsinh_sdtba" HeaderStyle-HorizontalAlign="Center" Width="20%"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn Caption="Tên mẹ" FieldName="hocsinh_tenme" HeaderStyle-HorizontalAlign="Center" Width="20%"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn Caption="SĐT mẹ" FieldName="hocsinh_sdtme" HeaderStyle-HorizontalAlign="Center" Width="20%"></dx:GridViewDataColumn>
                        </Columns>
                        <SettingsBehavior AllowFocusedRow="true" />
                        <SettingsText EmptyDataRow="Không có dữ liệu" SearchPanelEditorNullText="Gỏ từ cần tìm kiếm và enter..." />
                        <SettingsLoadingPanel Text="Đang tải..." />
                        <SettingsPager PageSize="50" Summary-Text="Trang {0} / {1} ({2} trang)"></SettingsPager>
                    </dx:ASPxGridView>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

