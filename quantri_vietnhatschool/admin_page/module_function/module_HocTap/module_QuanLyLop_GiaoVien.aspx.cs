﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_WebSite_module_QuanLyLop_GiaoVien : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public string Lop;
    cls_Alert alert = new cls_Alert();
    private int _id;

    protected void Page_Load(object sender, EventArgs e)
    {
            
            var getuser = (from u in db.admin_Users
                           where u.username_username == Request.Cookies["UserName"].Value
                           select u).FirstOrDefault();
            var checkNamHoc = (from nh in db.tbHoctap_NamHocs orderby nh.namhoc_id descending select nh).FirstOrDefault();

            // load data đổ vào var danh sách
            var getData = from l in db.tbLops
                          join hstl in db.tbHocSinhTrongLops on l.lop_id equals hstl.lop_id
                          join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                          join gvtl in db.tbGiaoVienTrongLops on l.lop_id equals gvtl.lop_id
                          where gvtl.taikhoan_id == getuser.username_id
                           && hstl.namhoc_id == checkNamHoc.namhoc_id
                          orderby hstl.position
                          select new
                          {
                              hs.hocsinh_id,
                              hs.hocsinh_name,
                              hs.hocsinh_code,
                              hs.hocsinh_tenba,
                              hs.hocsinh_sdtba,
                              hs.hocsinh_tenme,
                              hs.hocsinh_sdtme,
                              l.lop_name
                          };
            // đẩy dữ liệu vào gridivew
            grvList.DataSource = getData;
            grvList.DataBind();
            lblTongHocSinh.Text = getData.Count() + "";
            Lop = getData.First().lop_name;
    }
}