﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_WebSite_module_TongHocSinhCuaTruong : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    
    cls_Alert alert = new cls_Alert();
    private int _id;
    public string image;
    protected void Page_Load(object sender, EventArgs e)
    {
        // load data đổ vào var danh sách
        loadData();
        //lblTongHocSinh.Text = getData.Count() + "";
    }
    public void loadData()
    {
        var checkNamHoc = (from nh in db.tbHoctap_NamHocs orderby nh.namhoc_id descending select nh).FirstOrDefault();
        var getData = from l in db.tbLops
                      join hstl in db.tbHocSinhTrongLops on l.lop_id equals hstl.lop_id
                      join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                      join gvtl in db.tbGiaoVienTrongLops on l.lop_id equals gvtl.lop_id
                      where hstl.namhoc_id == 1
                      orderby hstl.position
                      select new
                      {
                          hs.hocsinh_id,
                          hs.hocsinh_name,
                          hs.hocsinh_code,
                          hs.hocsinh_tenba,
                          hs.hocsinh_sdtba,
                          hs.hocsinh_tenme,
                          hs.hocsinh_sdtme,
                          hs.hocsinh_image,
                          l.lop_name
                      };
        // đẩy dữ liệu vào gridivew
        grvList.DataSource = getData;
        grvList.DataBind();
        lblTongHocSinh.Text = getData.Count() + "";
    }
    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        // get value từ việc click vào gridview
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "hocsinh_id" }));
        // đẩy id vào session
        Session["_id"] = _id;
        var getData = (from hs in db.tbHocSinhs where hs.hocsinh_id == _id select hs).SingleOrDefault();
        txtHoTenHocSinh.Text = getData.hocsinh_name;
        txtHoTenBa.Text = getData.hocsinh_tenba;
        txtSDTBa.Text = getData.hocsinh_sdtba;
        txtHoTenMe.Text = getData.hocsinh_tenme;
        txtSDTMe.Text = getData.hocsinh_sdtme;

        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupControl.Show();", true);
        //ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupControl.Show();showImg1_1('" + getData.intro_image + "'); ", true);
        loadData();
    }
    protected void btnLuu_Click(object sender, EventArgs e)
    {
        tbHocSinh update = db.tbHocSinhs.Where(x => x.hocsinh_id == Convert.ToInt32(Session["_id"].ToString())).FirstOrDefault();
        
        update.hocsinh_tenba = txtHoTenBa.Text;
        update.hocsinh_sdtba = txtSDTBa.Text;
        update.hocsinh_tenme = txtHoTenMe.Text;
        update.hocsinh_sdtme = txtSDTMe.Text;
        try
        {
            db.SubmitChanges();
        }
        catch( Exception ex)
        {
            alert.alert_Error(Page, "Lỗi", "");
        }
    }
    protected void btnCapNhatHinhAnh_Click(object sender, EventArgs e)
    {
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "hocsinh_id" }));
        Session["_id"] = _id;
        var getData = (from hs in db.tbHocSinhs where hs.hocsinh_id == _id select hs).SingleOrDefault();
        txtHoTenHocSinh.Text = getData.hocsinh_name;
        dv_Ba.Visible = false;
        dv_sdtBa.Visible = false;
        dv_Me.Visible = false;
        dv_sdtMe.Visible = false;
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupControl.Show();showImg1_1('" + getData.hocsinh_image + "'); ", true);
    }
    protected void btnLuuHinh_Click(object sender, EventArgs e)
    {
        try
        {
            if (Page.IsValid && FileUpload1.HasFile)
            {

                String folderUser = Server.MapPath("~/uploadimages/avatar-hocsinh/");
                if (!Directory.Exists(folderUser))
                {
                    Directory.CreateDirectory(folderUser);
                }
                //string filename;
                string ulr = "/uploadimages/avatar-hocsinh/";
                HttpFileCollection hfc = Request.Files;
                string filename = Path.GetRandomFileName() + Path.GetExtension(FileUpload1.FileName);
                string fileName_save = Path.Combine(Server.MapPath("~/uploadimages/avatar-hocsinh"), filename);
                FileUpload1.SaveAs(fileName_save);
                image = ulr + filename;
                tbHocSinh update = (from hs in db.tbHocSinhs where hs.hocsinh_id == Convert.ToInt32(Session["_id"].ToString()) select hs).SingleOrDefault();
                update.hocsinh_image = image;
                db.SubmitChanges();
                alert.alert_Success(Page, "Đã lưu avatar học sinh", "");
            }
        }
        catch(Exception ex)
        {
            alert.alert_Error(Page, "Lỗi", "");
        }
    }
}