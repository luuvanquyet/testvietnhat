﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_TraBaiTap.aspx.cs" Inherits="admin_page_module_function_module_GiaoBaiTapOnline_module_TraBaiTap" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <style>
        .section-info {
            margin-top: 25px;
        }

            .section-info .title-info {
                font-size: 30px;
                margin-bottom: 20px;
                font-weight: 600;
                color: var(--color-primary);
            }

        .form-group__title {
            min-width: 130px;
            font-size: 16px;
        }

        .section-info .form-group {
            display: flex;
            margin-bottom: 20px;
            flex-direction: column;
        }

            .section-info .form-group input {
                flex: 1;
                background-color: transparent;
                outline: none;
                border: none;
                border-bottom: 1px solid #ccc;
            }

                .section-info .form-group input[type="date"] {
                    width: 150px;
                }

        .section-info .nav-pills .nav-link.active, .section-info .nav-pills .show > .nav-link {
            margin-bottom: 10px;
            display: block;
            text-indent: 10px;
            background-color: var(--color-primary);
            color: #fff !important;
        }

        .send-btn {
            display: flex;
            margin-bottom: 20px;
        }

            .send-btn .btn--modifier {
                margin: 0 20px;
            }

        .form--modifier {
            width: 30%;
            margin-right: 20px;
        }

        .bt-content {
            text-align: center;
            background-color: #fff;
        }

        .seen-content {
            border: 1px solid #ccc;
            padding: 20px;
        }

            .seen-content .seen--label {
                display: block;
                /*text-indent:15px;*/
            }

        .custom-file .custom-file-label {
            width: 30%;
        }

        .send-btn input[type="text"].dxeEditArea_Moderno, .send-btn input[type="password"].dxeEditArea_Moderno {
            margin-top: -5px;
            margin-bottom: 1px;
        }

        .seen-content__img {
            width: 100%;
            height: 200px;
        }
    </style>

    <h1 style="text-align: center; margin-bottom: 20px;">Trả Bài Tập</h1>
    <asp:UpdatePanel runat="server" ID="udButton">
        <ContentTemplate>
            <div class="send-btn">
                <dx:ASPxComboBox CssClass="form-control form--modifier" ID="ddlKhoiLop" TextField="khoi_name" ValueField="khoi_id" AutoPostBack="true" runat="server" NullText="Chọn Khối" OnSelectedIndexChanged="ddlKhoiLop_SelectedIndexChanged"></dx:ASPxComboBox>
                <a class="btn btn-primary btn--modifier" id="btn_Toan" runat="server" onserverclick="btn_Toan_ServerClick">Toán</a>
                <a class="btn btn-primary btn--modifier" id="btn_TiengViet" runat="server" onserverclick="btn_TiengViet_ServerClick">Tiếng Việt</a>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel runat="server" ID="udContent">
        <ContentTemplate>
            <div class="bt-content">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Tài Khoản Học Sinh</th>
                            <th scope="col">Ngày</th>
                            <th scope="col">Bài Nộp</th>
                            <th scope="col">Nhận Xét Và Bài Chấm Của Giáo Viên</th>
                            <%-- <th scope="col">Edit</th>
                            <th scope="col">Gửi Bài</th>--%>
                            <th scope="col">Phản Hồi Phụ Huynh</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater runat="server" ID="rpTraBaiTap">
                            <ItemTemplate>
                                <tr>
                                    <th scope="row"><%#STT++ %></th>
                                    <td><%#Eval("hocsinh_name") %></td>
                                    <td><%#Eval("trabaitap_createdate") %></td>
                                    <td><a href="#" class="btn btn-success" data-toggle="modal" data-target="#baitapHS<%#Eval("trabaitap_id") %>">Bài Nộp</a></td>
                                    <%--<td>
                                        <input type="text" id="btn_Diem_<%#Eval("trabaitap_id")%>" class="cssDiem" name="name" value="<%#Eval("trabaitap_diem") %>" />
                                    </td>
                                    <td><a href="#" class="btn btn-primary" id="" onclick="getid(<%#Eval("trabaitap_id")%>)">Lưu
                                        </a></td>--%>
                                    <td><a href="#" class="btn btn-success" data-toggle="modal" data-target="#nhanxetGV<%#Eval("trabaitap_id") %>" onclick="getid(<%#Eval("trabaitap_id") %>)">Gửi Kết Quả</a></td>
                                    <td><a href="#" class="btn btn-success" data-toggle="modal" data-target="#seenPH<%#Eval("trabaitap_id") %>" onclick="getid(<%#Eval("trabaitap_id") %>)"><%#Eval("xem")%></a></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Repeater runat="server" ID="rpDownload" OnItemDataBound="rpDownload_ItemDataBound">
        <ItemTemplate>
            <div class="modal fade" id="baitapHS<%#Eval("trabaitap_id") %>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Trả Bài Tập</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="seen-content">
                                <div class="container">
                                    <div class="row">
                                        <asp:Repeater runat="server" ID="rpImageKetQua">
                                            <ItemTemplate>
                                                <div class="col-lg-3">
                                                    <img class="seen-content__img" src="<%#Eval("imgketqua_image") %>" style="width: 200px; height: 300px;" alt="Alternate Text">
                                                    <a href="#" download="<%#Eval("imgketqua_image") %>">Download</a>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ItemTemplate>
    </asp:Repeater>
    <asp:Repeater runat="server" ID="rpPhanHoi">
        <ItemTemplate>
            <div class="modal fade" id="seenPH<%#Eval("trabaitap_id") %>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Phản Hồi</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="seen-content">
                                <label class="seen--label">Nhận Xét Phụ Huynh :</label>
                                <p><%# Eval("trabaitap_nhanxetphuhuynh") %></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ItemTemplate>
    </asp:Repeater>
    <asp:Repeater runat="server" ID="rpNhanXetGV">
        <ItemTemplate>
            <div class="modal fade" id="nhanxetGV<%#Eval("trabaitap_id") %>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Nhận Xét</h5>
                            <a href="#" type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </a>
                        </div>
                        <asp:UpdatePanel runat="server" ID="udPH">
                            <ContentTemplate>
                                <div class="modal-body">
                                    <div class="seen-content">
                                        <asp:UpdatePanel ID="upUpload" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
                                            <ContentTemplate>
                                                <span class="button-addimg fileinput-button">
                                                    <label>Lựa chọn ảnh</label>
                                                    <%--<div class="text-color1"><i>Kích thước ảnh tiêu chuẩn 1000x1000</i></div>--%>
                                                    <div id="upLoad_<%#Eval("trabaitap_id") %>" class="">
                                                        <asp:FileUpload CssClass="hidden-xs-up" ID="files" name="files[]" ClientIDMode="Static" runat="server" multiple accept="image/jpeg, image/png, image/gif," />
                                                        <button type="button" id="btnClick" class="btn-chang" onclick="clickavatar1(<%#Eval("trabaitap_id") %>)">
                                                            <img src="/admin_images/up-img.png" style="max-width: 100%; height: 150px" />
                                                        </button>
                                                    </div>

                                                </span>
                                                <output id="Filelist" class="clearfix"></output>
                                                <br />
                                                <div class="form-group d-flex align-items-center justify-content-space-between">
                                                    <asp:Button ID="btnUpload" CssClass="btn btn-primary fileinput-button" runat="server" Text="Lưu tất cả" OnClick="btn_GuiBai_ServerClick" />
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="btnUpload" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                        <label class="seen--label">Nhận Xét Giáo Viên :</label>
                                        <textarea rows="4" cols="50" id="txtNhanXet<%#Eval("trabaitap_id") %>" onchange="getid()"><%# Eval("trabaitap_diem") %></textarea>

                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary" onclick="getid(<%#Eval("trabaitap_id") %>)">Lưu</button>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </ItemTemplate>
    </asp:Repeater>
    <asp:UpdatePanel runat="server" ID="udXem">
        <ContentTemplate>
            <div style="display: none;">
                <input type="text" name="name" value="" id="txtScore" runat="server" />
                <input type="text" name="name" value="" id="txtIdhs" runat="server" />
                <a href="#" id="btn_Xem" onserverclick="btnXemm_ServerClick" runat="server">aa</a>
                <a class="btn btn-primary" id="btn_Luu" runat="server" onserverclick="btn_Luu_ServerClick">Gửi Bài</a>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script>
        function getid(id) {
            document.getElementById("<%=txtIdhs.ClientID%>").value = id;
            document.getElementById("<%=txtScore.ClientID%>").value = document.getElementById("txtNhanXet" + id).value;
            document.getElementById("<%=btn_Xem.ClientID%>").click();
            document.getElementById("<%=btn_Luu.ClientID%>").click();

        }
    </script>
    <script type="text/javascript">
        function clickavatar1(id) {
            //document.getElementById("upLoad_" + id).click();
            $("#upLoad input[type=file]").click();
        }
        //}
        //Tôi đã thêm trình xử lý sự kiện cho điều khiển tải lên tệp để truy cập các thuộc tính tệp.
        document.addEventListener("DOMContentLoaded", init, false);

        //Để lưu một loạt các tệp đính kèm
        var AttachmentArray = [];

        //bộ đếm cho mảng đính kèm
        var arrCounter = 0;

        //để đảm bảo thông báo lỗi cho số lượng tệp sẽ chỉ được hiển thị một lần.
        var filesCounterAlertStatus = false;

        //hủy danh sách để giữ hình thu nhỏ
        var ul = document.createElement('ul');
        ul.className = ("thumb-Images");
        ul.id = "imgList";

        function init() {
            //thêm trình xử lý javascript cho sự kiện tải lên tệp
            document.querySelector('#files').addEventListener('change', handleFileSelect, false);
        }

        //xử lý cho sự kiện tải lên tập tin
        function handleFileSelect(e) {

            //để đảm bảo người dùng chọn tập tin / tập tin
            if (!e.target.files) return;

            //Để có được một tài liệu tham khảo
            var files = e.target.files;
            if (files.length >= 1) {
                // Lặp lại thông qua FileList và sau đó để hiển thị các tệp hình ảnh dưới dạng hình thu nhỏ.
                for (var i = 0, f; f = files[i]; i++) {

                    //khởi tạo một đối tượng FileReader để đọc nội dung của nó vào bộ nhớ
                    var fileReader = new FileReader();

                    // Đóng để nắm bắt thông tin tập tin và áp dụng xác nhận.
                    fileReader.onload = (function (readerEvt) {
                        return function (e) {

                            //Áp dụng các quy tắc xác thực cho tải lên tệp đính kèm
                            ApplyFileValidationRules(readerEvt)

                            //Kết xuất hình đính kèm hình thu nhỏ.
                            RenderThumbnail(e, readerEvt);
                            //Điền vào mảng đính kèm
                            FillAttachmentArray(e, readerEvt)

                        };
                    })(f);

                    // Đọc trong tệp hình ảnh dưới dạng URL dữ liệu.
                    // readAsDataURL: Thuộc tính kết quả sẽ chứa dữ liệu của tệp / blob được mã hóa dưới dạng URL dữ liệu.
                    // More info about Data URI scheme https://en.wikipedia.org/wiki/Data_URI_scheme
                    fileReader.readAsDataURL(f);
                }

                document.getElementById('files').addEventListener('change', handleFileSelect, false);
                document.getElementById("<%=btnUpload.ClientID%>").style.display = 'block';
                      // document.getElementById("<%=btnUpload.ClientID%>").click();

            }
            else {
                var user = getCookie("language");
                if (user == "1") {
                    cls_Alert.alert_SuccessRs(Page, "Vui lòng nhập tối thiểu 10 hình ảnh", "");
                }
                else {
                    cls_Alert.alert_SuccessRs('Please enter a minimum of 10 images');
                }

            }
        }

        //Áp dụng các quy tắc xác thực cho tải lên tệp đính kèm
        function ApplyFileValidationRules(readerEvt) {
            //Để kiểm tra loại tệp theo điều kiện tải lên
            if (CheckFileType(readerEvt.type) == false) {
                alert("The file (" + readerEvt.name + ") does not match the upload conditions, You can only upload jpg/png/gif files");
                e.preventDefault();
                return;
            }

            //Để kiểm tra Kích thước tệp theo điều kiện tải lên
            if (CheckFileSize(readerEvt.size) == false) {
                alert("The file (" + readerEvt.name + ") does not match the upload conditions, The maximum file size for uploads should not exceed 4MB");
                e.preventDefault();
                return;
            }
            if (CheckFilesCount(AttachmentArray) == false) {

                if (!filesCounterAlertStatus) {
                    filesCounterAlertStatus = true;
                    alert("You have added more than 10 files. According to upload conditions you can upload 100 files maximum");
                }
                e.preventDefault();
                return;
            }
            //Để kiểm tra số lượng tập tin theo điều kiện tải lên

        }

        //Để kiểm tra loại tệp theo điều kiện tải lên
        function CheckFileType(fileType) {
            if (fileType == "image/jpeg") {
                return true;
            }
            else if (fileType == "image/png") {
                return true;
            }
            else if (fileType == "image/gif") {
                return true;
            }
            else {
                return false;
            }
            return true;
        }

        //Để kiểm tra Kích thước tệp theo điều kiện tải lên
        function CheckFileSize(fileSize) {
            if (fileSize < 15000000) {
                return true;
            }
            else {
                return false;
            }
            return true;
        }

        //Để kiểm tra số lượng tập tin theo điều kiện tải lên
        function CheckFilesCount(AttachmentArray) {
            //Vì AttachmentArray.length trả về chỉ mục có sẵn tiếp theo trong mảng,
            //Tôi đã sử dụng vòng lặp để có được chiều dài thực
            var len = 0;
            var count = 0;
            for (var i = 0; i < AttachmentArray.length; i++) {
                if (AttachmentArray[i] !== undefined) {
                    len++;

                }

            }
            //Để kiểm tra độ dài không vượt quá 100 tệp
            if (len > 100) {
                return false;
            }
            else {
                return true;
            }
        }
        function CheckFilesCountMin() {
            //Vì AttachmentArray.length trả về chỉ mục có sẵn tiếp theo trong mảng,
            //Tôi đã sử dụng vòng lặp để có được chiều dài thực
            var len = AttachmentArray.length;
            if (len > 1)
                return true;
            else {
                return false;
            }

            //To check the length does not exceed 10 files maximum
        }
        //Render attachments thumbnails.
        function RenderThumbnail(e, readerEvt) {
            var li = document.createElement('li');
            ul.appendChild(li);
            li.innerHTML = ['<div class="img-wrap">' +
                '<img class="thumb" src="', e.target.result, '" title="', escape(readerEvt.name), '" data-id="',
            readerEvt.name, '"/>' + '</div>'].join('');

            var div = document.createElement('div');
            div.className = "FileNameCaptionStyle";
            li.appendChild(div);
            div.innerHTML = [readerEvt.name].join('');
            document.getElementById('Filelist').insertBefore(ul, null);
        }

        //Fill the array of attachment
        function FillAttachmentArray(e, readerEvt) {
            AttachmentArray[arrCounter] =
                {
                    AttachmentType: 1,
                    ObjectType: 1,
                    FileName: readerEvt.name,
                    FileDescription: "Attachment",
                    NoteText: "",
                    MimeType: readerEvt.type,
                    Content: e.target.result.split("base64,")[1],
                    FileSizeInBytes: readerEvt.size,
                };
            arrCounter = arrCounter + 1;
        }
    </script>
  <%--  <script type="text/javascript">
        function clickavatar1() {
            $("#upLoad input[type=file]").click();
        }

    </script>--%>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>
