﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_GiaoBaiTapOnline_module_TraBaiTap : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public int STT = 1;
    public string image;
    cls_Alert alert = new cls_Alert();
    protected void Page_Load(object sender, EventArgs e)
    {

        string username = Request.Cookies["UserName"].Value;
        var giaovien = from user in db.admin_Users
                       where user.username_username == username
                       select user;
        if (giaovien.Single().username_username == "root")
        {
           var khoiLop = from kh in db.tbKhois
                                    select kh;
          
            ddlKhoiLop.DataSource = khoiLop;
            ddlKhoiLop.DataBind();
        }
        else
        {

            var giaovien_Khoi = from gv in db.tbGiaoViens
                                join k in db.tbKhois on gv.khoi_id equals k.khoi_id
                                where gv.username_id == giaovien.Single().username_id
                                select new
                                {
                                    k.khoi_name,
                                    k.khoi_id,
                                };
            ddlKhoiLop.DataSource = giaovien_Khoi;
            ddlKhoiLop.DataBind();
        }
        var nhanxetGv = from nx in db.tbHoSo_TraBaiTaps
                        select nx;
        rpNhanXetGV.DataSource = nhanxetGv;
        rpNhanXetGV.DataBind();
        rpPhanHoi.DataSource = nhanxetGv;
        rpPhanHoi.DataBind();
        rpDownload.DataSource = nhanxetGv;
        rpDownload.DataBind();
    }

    protected void ddlKhoiLop_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Session["MonHoc"] != null)
        {
            // Hiện thị danh sách học sinh của từng khối
            var traBaiTap = from bt in db.tbHoSo_TraBaiTaps
                            join gbt in db.tbHoSo_GiaoBaiTaps on bt.giaobaitap_id equals gbt.giaobaitap_id
                            join l in db.tbLops on gbt.lop_id equals l.lop_id
                            join k in db.tbKhois on l.khoi_id equals k.khoi_id
                            join mh in db.tbMonHocs on gbt.monhoc_id equals mh.monhoc_id
                            join hs in db.tbHocSinhs on bt.hocsinh_id equals hs.hocsinh_id
                            where (mh.monhoc_name == Session["MonHoc"].ToString() && k.khoi_id == Convert.ToInt32(ddlKhoiLop.SelectedItem.Value))
                            select new
                            {
                                bt.trabaitap_id,
                                bt.trabaitap_createdate,
                                bt.trabaitap_diem,
                                bt.trabaitap_filename,
                                bt.trabaitap_nhanxettugiaovien,
                                xem = bt.trabaitap_active == true ? "Đã Xem" :"Xem",
                                hs.hocsinh_name,
                                hs.hocsinh_code,
                                mh.monhoc_name,
                                bt.trabaitap_nhanxetphuhuynh,
                            };
            rpTraBaiTap.DataSource = traBaiTap;
            rpTraBaiTap.DataBind();
            
            rpNhanXetGV.DataSource = traBaiTap;
            rpNhanXetGV.DataBind();
            rpPhanHoi.DataSource = traBaiTap;
            rpPhanHoi.DataBind();
            var imageBT = from bt in db.tbHoSo_TraBaiTaps
                          join img in db.tbImageKetQuaHocSinhs on bt.trabaitap_id equals img.trabaitap_id
                          select new
                          {
                              bt.trabaitap_id,
                              img.imgketqua_id,
                              img.imgketqua_image,
                          };
            rpDownload.DataSource = imageBT;
            rpDownload.DataBind();
            loaddata();
        }
    }

    protected void btn_Toan_ServerClick(object sender, EventArgs e)
    {
        var traBaiTap = from bt in db.tbHoSo_TraBaiTaps
                        join gbt in db.tbHoSo_GiaoBaiTaps on bt.giaobaitap_id equals gbt.giaobaitap_id
                        join l in db.tbLops on gbt.lop_id equals l.lop_id
                        join k in db.tbKhois on l.khoi_id equals k.khoi_id
                        join mh in db.tbMonHocs on gbt.monhoc_id equals mh.monhoc_id
                        join hs in db.tbHocSinhs on bt.hocsinh_id equals hs.hocsinh_id
                        where (mh.monhoc_name == "Toán" && k.khoi_id == Convert.ToInt32(ddlKhoiLop.SelectedItem.Value))
                        select new
                        {
                            bt.trabaitap_id,
                            bt.trabaitap_createdate,
                            bt.trabaitap_diem,
                            bt.trabaitap_filename,
                            bt.trabaitap_nhanxettugiaovien,
                            xem = bt.trabaitap_active == true ? "Đã Xem" : "Xem",
                            hs.hocsinh_name,
                            hs.hocsinh_code,
                            bt.trabaitap_nhanxetphuhuynh,
                            mh.monhoc_name,
                        };
        Session["MonHoc"] = "Toán";
        rpTraBaiTap.DataSource = traBaiTap;
        rpTraBaiTap.DataBind();
        rpNhanXetGV.DataSource = traBaiTap;
        rpNhanXetGV.DataBind();
        rpPhanHoi.DataSource = traBaiTap;
        rpPhanHoi.DataBind();
        var imageBT = from bt in db.tbHoSo_TraBaiTaps
                      join img in db.tbImageKetQuaHocSinhs on bt.trabaitap_id equals img.trabaitap_id
                      select new
                      {
                          bt.trabaitap_id,
                          img.imgketqua_id,
                          img.imgketqua_image,
                      };
        rpDownload.DataSource = imageBT;
        rpDownload.DataBind();
        loaddata();
    }

    protected void btn_TiengViet_ServerClick(object sender, EventArgs e)
    {
        var traBaiTap = from bt in db.tbHoSo_TraBaiTaps
                        join gbt in db.tbHoSo_GiaoBaiTaps on bt.giaobaitap_id equals gbt.giaobaitap_id
                        join l in db.tbLops on gbt.lop_id equals l.lop_id
                        join k in db.tbKhois on l.khoi_id equals k.khoi_id
                        join mh in db.tbMonHocs on gbt.monhoc_id equals mh.monhoc_id
                        join hs in db.tbHocSinhs on bt.hocsinh_id equals hs.hocsinh_id
                        where (mh.monhoc_name == "Tiếng Việt" && k.khoi_id == Convert.ToInt32(ddlKhoiLop.SelectedItem.Value))
                        select new
                        {
                            bt.trabaitap_id,
                            bt.trabaitap_createdate,
                            bt.trabaitap_diem,
                            bt.trabaitap_filename,
                            xem = bt.trabaitap_active == true ? "Đã Xem" : "Xem",
                            bt.trabaitap_nhanxettugiaovien,
                            hs.hocsinh_name,
                            hs.hocsinh_code,
                            bt.trabaitap_nhanxetphuhuynh,
                            mh.monhoc_name
                        };
        Session["MonHoc"] = "Tiếng Việt";
        rpTraBaiTap.DataSource = traBaiTap;
        rpTraBaiTap.DataBind();
        rpNhanXetGV.DataSource = traBaiTap;
        rpNhanXetGV.DataBind();
        rpPhanHoi.DataSource = traBaiTap;
        rpPhanHoi.DataBind();
        var imageBT = from bt in db.tbHoSo_TraBaiTaps
                      join img in db.tbImageKetQuaHocSinhs on bt.trabaitap_id equals img.trabaitap_id
                      select new
                      {
                          bt.trabaitap_id,
                          img.imgketqua_id,
                          img.imgketqua_image,
                      };
        rpDownload.DataSource = imageBT;
        rpDownload.DataBind();
        loaddata();
    }
    public void loaddata()
    {
        var phanhoi = from ph in db.tbHoSo_TraBaiTaps
                      select ph;
        rpPhanHoi.DataSource = phanhoi;
        rpPhanHoi.DataBind();
    }


    protected void btnXemm_ServerClick(object sender, EventArgs e)
    {
        string id = txtIdhs.Value;
        tbHoSo_TraBaiTap update = db.tbHoSo_TraBaiTaps.Where(x => x.trabaitap_id == Convert.ToInt32(id)).Single();
        update.trabaitap_active = true;
        db.SubmitChanges();
    }

    protected void btn_GuiBai_ServerClick(object sender, EventArgs e)
    {
        cls_AddImages insert = new cls_AddImages();
        string id = txtIdhs.Value;
        String folderUser = Server.MapPath("~/uploadimages/thuvienanh/");
        if (!Directory.Exists(folderUser))
        {
            Directory.CreateDirectory(folderUser);
        }
        string filename;
        string ulr = "/uploadimages/thuvienanh/";
        HttpFileCollection hfc = Request.Files;
        for (int i = 0; i < hfc.Count; i++)
        {
            HttpPostedFile hpf = hfc[i];
            if (hpf.ContentLength > 0)
            {
                //string filename = Path.GetRandomFileName() + Path.GetExtension(FileUpload1.FileName);
                //filename = ulr + System.IO.Path.GetFileName(cls_ToAscii.ToAscii(hpf.FileName).ToLower());
                filename = ulr + Path.GetRandomFileName() + Path.GetExtension(hpf.FileName);
                string path = HttpContext.Current.Server.MapPath("~/" + filename);
                hpf.SaveAs(path);
                VaryQualityLevel(System.Drawing.Image.FromStream(hpf.InputStream), path);
                insert.InsertImageOneProduct(filename, Convert.ToInt32(id));
            }
        }

        //foreach (RepeaterItem item in rpImageListing.Items)
        //{
        //    HtmlGenericControl control = item.FindControl("divSub") as HtmlGenericControl;
        //    if (control != null)
        //    {
        //        //set the control value here.
        //    }
        //}
        //divMes.InnerHtml = mes;
        //Session["re_id"] = null;
       // cls_Alert.alert_Success(Page, "Đã lưu thành công", "");
        //loadData();
        
        tbHoSo_TraBaiTap upd = db.tbHoSo_TraBaiTaps.Where(x => x.trabaitap_id == Convert.ToInt32(id)).Single();
        if (txtScore.Value == "" && image == null)
        {
            upd.trabaitap_fileketqua = image;
            upd.trabaitap_diem = txtScore.Value;
        }
        else
        {
            if (txtScore.Value != "")
            {
                upd.trabaitap_diem = upd.trabaitap_diem;
            }
            if (upd.trabaitap_fileketqua == null)
            {
                upd.trabaitap_fileketqua = image;
            }
        }
        try
        {
            db.SubmitChanges();
        }
        catch { }
    }

    protected void btn_Luu_ServerClick(object sender, EventArgs e)
    {

    }

    protected void rpDownload_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

        Repeater rpImageKetQua = e.Item.FindControl("rpImageKetQua") as Repeater;
        int trabaitap_id = int.Parse(DataBinder.Eval(e.Item.DataItem, "trabaitap_id").ToString());
        var image = from bt in db.tbHoSo_TraBaiTaps
                    join img in db.tbImageKetQuaHocSinhs on bt.trabaitap_id equals img.trabaitap_id
                    where bt.trabaitap_id == trabaitap_id
                    select new
                    {
                        bt.trabaitap_id,
                        img.imgketqua_image,
                    };
        rpImageKetQua.DataSource = image;
        rpImageKetQua.DataBind();
    }
    private void SaveResizeImage(System.Drawing.Image img, int width, int height, string path)
    {
        try
        {
            int resizedW = width;
            int resizedH = height;
            Bitmap b = new Bitmap(resizedW, resizedH);
            Graphics g = Graphics.FromImage((System.Drawing.Image)b);
            g.InterpolationMode = InterpolationMode.Bicubic;
            g.DrawImage(img, 0, 0, resizedW, resizedH);

            ImageCodecInfo jpgEncoder = GetEncoder(ImageFormat.Jpeg);
            System.Drawing.Imaging.Encoder myEncoder = System.Drawing.Imaging.Encoder.Quality;
            EncoderParameters myEncoderParameters = new EncoderParameters(1);

            EncoderParameter myEncoderParameter = new EncoderParameter(myEncoder, 50L);
            myEncoderParameters.Param[0] = myEncoderParameter;

            g.Dispose();
            b.Save(path, jpgEncoder, myEncoderParameters);
        }
        catch
        {
        }
    }
    private void VaryQualityLevel(System.Drawing.Image img, string path)
    {
        using (Bitmap bmp1 = new Bitmap(img))
        {
            ImageCodecInfo jpgEncoder = GetEncoder(ImageFormat.Jpeg);
            System.Drawing.Imaging.Encoder myEncoder = System.Drawing.Imaging.Encoder.Quality;
            EncoderParameters myEncoderParameters = new EncoderParameters(1);

            EncoderParameter myEncoderParameter = new EncoderParameter(myEncoder, 50L);
            myEncoderParameters.Param[0] = myEncoderParameter;
            bmp1.Save(path, jpgEncoder, myEncoderParameters);
        }
    }

    private ImageCodecInfo GetEncoder(ImageFormat format)
    {
        ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();
        foreach (ImageCodecInfo codec in codecs)
        {
            if (codec.FormatID == format.Guid)
            {
                return codec;
            }
        }
        return null;
    }
}