﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_HocTap_module_TuanHoc : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["_id"] = 0;

        }
        loadData();
    }
    private void loadData()
    {
        var getData = from t in db.tbHocTap_Tuans
                      join n in db.tbHoctap_NamHocs on t.namhoc_id equals n.namhoc_id
                      where t.tuan_hidden == false
                      orderby t.tuan_id descending
                      select new
                      {
                          t.tuan_id,
                          t.tuan_name,
                          n.namhoc_id,
                          n.namhoc_hocky,
                          n.namhoc_nienkhoa
                      };
        grvList.DataSource = getData;
        grvList.DataBind();
        ddlNienKhoa.DataSource = from n in db.tbHoctap_NamHocs
                                 select n;
        ddlNienKhoa.DataBind();
        //ddlKyHoc.DataSource = from n in db.tbHoctap_NamHocs
        //                         select n;
        //ddlKyHoc.DataBind();
    }
    private void setNULL()
    {
        txtName.Text = "";
        txtTuNgay.Value = "";
        txtDenNgay.Value = "";
        ddlNienKhoa.Text = "";
    }
    protected void btnThem_Click(object sender, EventArgs e)
    {
        Session["_id"] = 0;
        setNULL();
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupControl.Show();", true);
    }

    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "tuan_id" }));
        Session["_id"] = _id;
        var getData = (from t in db.tbHocTap_Tuans
                       join n in db.tbHoctap_NamHocs on t.namhoc_id equals n.namhoc_id
                       where t.tuan_id == _id
                       select new
                       {
                          t.tuan_id,
                          t.tuan_name,
                          t.tuan_tungay,
                          t.tuan_denngay,
                          n.namhoc_id,
                          n.namhoc_nienkhoa,
                          n.namhoc_hocky
                       }).Single();
        txtName.Text = getData.tuan_name;
        //s.Text = getData.namhoc_hocky;
        slKyHoc.Value = getData.namhoc_hocky;
        ddlNienKhoa.Text = getData.namhoc_nienkhoa;
        txtTuNgay.Value = getData.tuan_tungay.Value.ToString("yyyy-MM-dd").Replace(' ', 'T');
        txtDenNgay.Value = getData.tuan_denngay.Value.ToString("yyyy-MM-dd").Replace(' ', 'T');
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupControl.Show(); ", true);
    }
    public bool checknull()
    {
        if (txtTuNgay.Value != "" && txtDenNgay.Value != "" && ddlNienKhoa.Text!="")
            return true;
        else return false;
    }
    protected void btnLuu_Click(object sender, EventArgs e)
    {
        cls_TuanHoc cls = new cls_TuanHoc();
        if (checknull() == false)
            alert.alert_Warning(Page, "Hãy nhập đầy đủ thông tin!", "");
        else
        {
            if (Session["_id"].ToString() == "0")
            {
                if (cls.Linq_Them(txtName.Text, Convert.ToInt32(ddlNienKhoa.SelectedItem.Value), Convert.ToDateTime(txtTuNgay.Value), Convert.ToDateTime(txtDenNgay.Value)))
                    ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Thêm thành công!','','success').then(function(){grvList.UnselectRows();})", true);
                else alert.alert_Error(Page, "Thêm thất bại", "");

            }
            else
            {
                if (cls.Linq_Sua(Convert.ToInt32(Session["_id"].ToString()), txtName.Text, Convert.ToInt32(ddlNienKhoa.SelectedItem.Value), Convert.ToDateTime(txtTuNgay.Value), Convert.ToDateTime(txtDenNgay.Value)))
                    ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Cập nhật thành công!','','success').then(function(){grvList.UnselectRows();})", true);
                else alert.alert_Error(Page, "Cập nhật thất bại", "");
            }
            popupControl.ShowOnPageLoad = false;
            loadData();
        }
    }

    protected void btnXoa_Click(object sender, EventArgs e)
    {
        cls_TuanHoc cls;
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "tuan_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                cls = new cls_TuanHoc();
                tbHocTap_Tuan checkImage = (from i in db.tbHocTap_Tuans where i.tuan_id == Convert.ToInt32(item) select i).SingleOrDefault();
                if (cls.Linq_Xoa(Convert.ToInt32(item)))
                {
                    ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Xóa thành công!','','success').then(function(){grvList.Refresh();})", true);
                }
                else
                    ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Xóa thất bại!','','error').then(function(){grvList.UnselectRows();})", true);
            }
        }
        else
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
        popupControl.ShowOnPageLoad = false;
        loadData();
    }
}