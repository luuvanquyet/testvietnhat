﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_KhoTuLieu_admin_KhoTuLieu_KhoiTrungHoc_ChiTiet : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    protected void Page_Load(object sender, EventArgs e)
    {
        var getData = (from fm in db.tbQuanTri_FormMaus where fm.formmau_id == Convert.ToInt32(RouteData.Values["id"]) select fm).Single();
        string embed = "<object data=\"{0}\" type=\"application/pdf\" width=\"100%\" height=\"700px\">";
        embed += "If you are unable to view file, you can download from <a href = \"{0}\">here</a>";
        embed += " or download <a target = \"_blank\" href = \"http://get.adobe.com/reader/\">Adobe PDF Reader</a> to view the file.";
        embed += "</object>";
        ltEmbed.Text = string.Format(embed, ResolveUrl(getData.formau_file));
    }
}