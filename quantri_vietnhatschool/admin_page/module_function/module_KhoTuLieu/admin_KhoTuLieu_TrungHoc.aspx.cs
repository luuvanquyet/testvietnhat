﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_KhoTuLieu_admin_KhoTuLieu_TrungHoc : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    protected void Page_Load(object sender, EventArgs e)
    {
        var list = from l in db.tbQuanTri_FormMaus
                   where l.formau_loai == "file trung hoc" 
                   orderby l.formau_ngaycapnhat descending
                   select new
                   {
                       l.formmau_id,
                       l.formmau_title,
                       l.formau_loai,
                       l.formau_hidden,
                       l.formau_ngaycapnhat,
                       l.formau_file,
                       file_images = l.formau_file.Contains("pdf") ? "/images/icon_PDF.png" : "/images/icon-word.png"
                   };
        grvList.DataSource = list;
        grvList.DataBind();
    }

    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        int _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "formmau_id" }));
        Response.Redirect("kho-tu-lieu-khoi-trung-hoc-" + _id);
    }

}