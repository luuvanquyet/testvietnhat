﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_DanhSachKeHoachChuNhiemThang : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (RouteData.Values["id"].ToString() != "0")
            {
                var checkLichCongTacThang = (from lct in db.tbkeHoachChuNhiems
                                             where lct.kehoachchunhiem_id == Convert.ToInt32(RouteData.Values["id"])
                                             select new
                                             {
                                                 lct.kehoachchunhiem_title,
                                                 lct.kehoachchunhiem_content,
                                                 lct.kehoachchunhiem_thang,
                                             }).SingleOrDefault();
                ddlThang.Value = checkLichCongTacThang.kehoachchunhiem_thang +"";
                txtTieuDe.Text = checkLichCongTacThang.kehoachchunhiem_title;
                edtnoidung.Html = checkLichCongTacThang.kehoachchunhiem_content;
            }
        }
    }
    protected void btnLuu_Click(object sender, EventArgs e)
    {
        try
        {
            var getuser = (from u in db.admin_Users
                           where u.username_username == Request.Cookies["UserName"].Value
                           select u).FirstOrDefault();
            if (RouteData.Values["id"].ToString() == "0")
            {
                tbkeHoachChuNhiem insert = new tbkeHoachChuNhiem();
                insert.kehoachchunhiem_title = txtTieuDe.Text;
                insert.kehoachchunhiem_thang = Convert.ToInt16(ddlThang.Value);
                insert.kehoachchunhiem_content = edtnoidung.Html;
                insert.kehoachchunhiem_nam = DateTime.Now.Year;
                insert.username_id = getuser.username_id;
                db.tbkeHoachChuNhiems.InsertOnSubmit(insert);
                db.SubmitChanges();
                alert.alert_Success(Page, "Hoàn thành kế hoạch tháng: " + insert.kehoachchunhiem_thang, "");

            }
            else
            {
                tbkeHoachChuNhiem update = (from lct in db.tbkeHoachChuNhiems where lct.kehoachchunhiem_id == Convert.ToInt32(RouteData.Values["id"].ToString()) select lct).SingleOrDefault();
                update.kehoachchunhiem_title = txtTieuDe.Text;
                update.kehoachchunhiem_content = edtnoidung.Html;
                update.kehoachchunhiem_nam = DateTime.Now.Year;
                update.kehoachchunhiem_thang = Convert.ToInt16(ddlThang.Value);
                update.username_id = getuser.username_id;
                db.SubmitChanges();
                alert.alert_Success(Page, "Đã cập nhật lại kế hoạch tháng: " + update.kehoachchunhiem_thang, "");
            }

        }
        catch (Exception)
        {
            alert.alert_Error(Page, "Lỗi, vui lòng liên hệ IT!", "");
        }
    }

    protected void btnQuayLai_Click(object sender, EventArgs e)
    {
        Response.Redirect("admin-danh-sach-ke-hoach-chu-nhiem");
    }
}