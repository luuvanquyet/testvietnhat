﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_DanhSachKeHoachChuNhiem_Tong : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    protected void Page_Load(object sender, EventArgs e)
    {
      
        loadData();
    }
    private void loadData()
    {
        var getData = from tb in db.tbkeHoachChuNhiems
                      join u in db.admin_Users on tb.username_id equals u.username_id
                      orderby tb.kehoachchunhiem_id descending
                      where tb.kehoachchunhiem_thang == Convert.ToInt16(ddlThang.SelectedValue)
                      select new
                      {
                          tb.kehoachchunhiem_id,
                          tb.kehoachchunhiem_thang,
                          tb.kehoachchunhiem_title,
                          u.username_fullname
                      };
        rpGiaoVien.DataSource = getData;
        rpGiaoVien.DataBind();
        //grvList.DataSource = getData;
        //grvList.DataBind();
    }
    protected void btnXem_Click(object sender, EventArgs e)
    {

    }
}