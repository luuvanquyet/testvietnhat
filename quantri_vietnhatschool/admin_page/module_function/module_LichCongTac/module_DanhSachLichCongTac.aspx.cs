﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_NewsCate : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["_id"] = 0;
        }
        loadData();
    }
    private void loadData()
    {
        var getData = from tb in db.tbLichCongTacs
                      orderby tb.lichcongtac_id descending
                      where tb.lichcongtac_hidden == null
                      select tb;
        grvList.DataSource = getData;
        grvList.DataBind();
    }

    protected void btnThem_Click(object sender, EventArgs e)
    {
        Response.Redirect("admin-lich-cong-tac-chi-tiet-hang-tuan-1");
    }
    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "lichcongtac_id" }));
        Response.Redirect("admin-lich-cong-tac-chi-tiet-hang-tuan-" + _id);
    }
    protected void btnXoa_Click(object sender, EventArgs e)
    {
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "lichcongtac_id" }));
        tbLichCongTac del = (from d in db.tbLichCongTacs where d.lichcongtac_id == _id select d).SingleOrDefault();
        db.tbLichCongTacs.DeleteOnSubmit(del);
        db.SubmitChanges();
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Đã xóa dữ liệu!','','success').then(function(){grvList.UnselectRows();})", true);
    }
    private bool SendMail(string email)
    {

        if (email != "")
        {
            try
            {
                var fromAddress = "thongbaovietnhatschool@gmail.com";//  Email Address from where you send the mail 
                var toAddress = email;
                const string fromPassword = "neiabcekdjluofid";
                string subject, title;
                title = "Thông báo";
                subject = "<!DOCTYPE html><html><head><title></title></head><body ><div>" +
                "<h3 style=\"margin-top:0px; text-align:center; color:#029ada\">Bạn có thông báo mới từ lịch công tác tuần. Xem chi tiết <a href='http://quantri.vietnhatschool.edu.vn/admin-xem-lich-cong-tac-hang-tuan'>tại đây.</a></h3>" +
                "</div></body></html>";
                var smtp = new System.Net.Mail.SmtpClient();
                {
                    smtp.Host = "smtp.gmail.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    smtp.Credentials = new NetworkCredential(fromAddress, fromPassword);
                    smtp.Timeout = 20000;
                }
                MailMessage mm = new MailMessage();
                mm.From = new MailAddress(fromAddress, "Trường Liên cấp Việt Nhật");
                mm.Subject = title;
                mm.To.Add(toAddress);
                mm.IsBodyHtml = true;
                mm.Body = subject;
                smtp.Send(mm);
                return true;
            }
            catch
            {
                return false;
            }
        }
        else
            return false;
    }
    protected void btnSenMail_Click(object sender, EventArgs e)
    {
        string getDataEmail = (from mail in db.tbDanhSachEmails where mail.email_id == 1 select mail).SingleOrDefault().email_content;
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "lichcongtac_id" });

        if (selectedKey.Count > 0)
        {
            SendMail(getDataEmail);
            ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Đã gửi email thành công!','','success').then(function(){grvList.UnselectRows();})", true);
        }
        else
        {
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu để gửi!", "");
        }
    }
}