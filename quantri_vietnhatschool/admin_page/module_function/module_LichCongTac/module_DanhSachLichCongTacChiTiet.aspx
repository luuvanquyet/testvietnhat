﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_DanhSachLichCongTacChiTiet.aspx.cs" Inherits="admin_page_module_function_module_NewsCate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="card card-block">

        <div class="form-group table-responsive">
            <asp:Button ID="btnQuayLai" runat="server" Text="Quay lại" CssClass="btn btn-primary" OnClick="btnQuayLai_Click" />
            <br />
            <b>SỞ GIÁO DỤC & ĐÀO TẠO ĐÀ NẴNG</b>
            <br />
            <b>TRƯỜNG TH-THCS & THPT VIỆT NHẬT</b>
            <br />
            <br />
            <div style="text-align: center">
                <h3><b>LỊCH CÔNG TÁC HÀNG TUẦN NĂM 2021-2022 </b></h3>
            </div>
            <br />
            <%-- <div class="row">--%>
            <div class="col-3" style="margin-left: 20px;">
                <span>Tuần</span>
                <dx:ASPxComboBox ID="ddlTuan" runat="server" ValueType="System.Int32" TextField="image_link" ValueField="image_lich" ClientInstanceName="ddlThang" CssClass="" Width="100%"></dx:ASPxComboBox>
            </div>
            <div class="col-3" style="margin-left: 20px;">
                <span>Từ ngày</span>
                <input type="date" id="txtTuNgay" class="form-control" style="width: 100%" runat="server" />
            </div>
            <div class="col-3" style="margin-left: 20px;">
                <span>đến ngày</span>
                <input id="txtDenNgay" type="date" class="form-control" style="width: 100%" runat="server" />
            </div>
            <%-- </div>--%>
        </div>
        <div>
            <table>
                <tr>
                    <th style="text-align: center">Thứ/Ngày</th>
                    <th style="text-align: center">Nội dung công việc</th>
                    <th style="text-align: center">Ghi chú</th>
                </tr>
                <tr>
                    <td>Thứ 2
                         <br />
                        <input type="text" value="" placeholder="dd/mm" runat="server" id="txtThu2" style="width: 60px" />
                    </td>

                    <td>
                        <textarea id="txtNoiDung1" runat="server" rows="3" cols="50"></textarea></td>
                    <td>
                        <textarea id="txtGhiChu1" runat="server" rows="3" cols="50"></textarea></td>
                </tr>
                <tr>
                    <td>Thứ 3
                        <br />
                        <input type="text" value="" placeholder="dd/mm" runat="server" id="txtThu3" style="width: 60px" />
                    </td>
                    <td>
                        <textarea id="txtNoiDung2" runat="server" rows="3" cols="50"></textarea></td>
                    <td>
                        <textarea id="txtGhiChu2" runat="server" rows="3" cols="50"></textarea></td>
                </tr>
                <tr>
                    <td>Thứ 4
                        <br />
                        <input type="text" value="" placeholder="dd/mm" runat="server" id="txtThu4" style="width: 60px" />
                    </td>
                    <td>
                        <textarea id="txtNoiDung3" runat="server" rows="3" cols="50"></textarea></td>
                    <td>
                        <textarea id="txtGhiChu3" runat="server" rows="3" cols="50"></textarea></td>
                </tr>
                <tr>
                    <td>Thứ 5 
                        <br />
                        <input type="text" value="" placeholder="dd/mm" runat="server" id="txtThu5" style="width: 60px" />
                    </td>
                    <td>
                        <textarea id="txtNoiDung4" runat="server" rows="3" cols="50"></textarea></td>
                    <td>
                        <textarea id="txtGhiChu4" runat="server" rows="3" cols="50"></textarea></td>
                </tr>
                <tr>
                    <td>Thứ 6
                        <br />
                        <input type="text" value="" placeholder="dd/mm" runat="server" id="txtThu6" style="width: 60px" />
                    </td>
                    <td>
                        <textarea id="txtNoiDung5" runat="server" rows="3" cols="50"></textarea></td>
                    <td>
                        <textarea id="txtGhiChu5" runat="server" rows="3" cols="50"></textarea></td>
                </tr>
                <tr>
                    <td>Thứ 7 
                        <br />
                        <input type="text" value="" placeholder="dd/mm" runat="server" id="txtThu7" style="width: 60px" />
                    </td>
                    <td>
                        <textarea id="txtNoiDung6" runat="server" rows="3" cols="50"></textarea></td>
                    <td>
                        <textarea id="txtGhiChu6" runat="server" rows="3" cols="50"></textarea></td>
                </tr>
            </table>
            <div class="form-group row" style="display:flex">
                <label style="display:flex">
                    <input class="checkbox" type="checkbox" id="chkHopTo" runat="server" />
                    <span></span>
                    <p class="pr-name" style="padding-top: 6px;">Họp tổ</p>
                </label>
                <label style="margin-left:100px;display:flex">
                    <input class="checkbox" type="checkbox" id="chkHopNhom" runat="server" />
                    <span></span>
                    <p class="pr-name"style="padding-top: 6px;">Họp nhóm</p>
                </label>
            </div>
        </div>
        <br />
        <div class="form-group row">
            <div class="col-sm-10">
                <asp:UpdatePanel ID="udButton" runat="server">
                    <ContentTemplate>

                        <asp:Button ID="btnLuu" runat="server" Text="Lưu" CssClass="btn btn-primary" OnClick="btnLuu_Click" />
                        <%--<asp:Button ID="btnChiTiet" runat="server" Text="Chi tiết" CssClass="btn btn-primary" OnClick="btnChiTiet_Click" />
                        <input type="submit" class="btn btn-primary" value="Xóa" onclick="confirmDel()" />
                        <asp:Button ID="btnXoa" runat="server" CssClass="invisible" OnClick="btnXoa_Click" />--%>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

