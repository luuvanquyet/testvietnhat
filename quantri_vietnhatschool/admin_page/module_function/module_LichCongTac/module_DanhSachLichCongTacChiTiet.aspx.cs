﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_NewsCate : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            var listNV = from nv in db.tbImage_Liches where nv.image_group == 3 select nv;
            ddlTuan.DataSource = listNV;
            ddlTuan.DataBind();
            if (RouteData.Values["id"].ToString() != "1")
            {
                var checkLichCongTac = (from lct in db.tbLichCongTacs
                                        join i in db.tbImage_Liches on lct.image_lich equals i.image_lich
                                        where lct.lichcongtac_id == Convert.ToInt32(RouteData.Values["id"])
                                        select new
                                        {
                                            lct.lichcongtac_tuan,
                                            lct.lichcongtac_tungay,
                                            lct.lichcongtac_denngay,
                                            i.image_link,
                                            lct.lichcongtac_cuochopnhom,
                                            lct.lichcongtac_cuochopto,

                                        }).SingleOrDefault();

                ddlTuan.Text = checkLichCongTac.image_link;
                txtTuNgay.Value = checkLichCongTac.lichcongtac_tungay.Value.ToString("yyyy-MM-dd").Replace(' ', 'T');
                txtDenNgay.Value = checkLichCongTac.lichcongtac_denngay.Value.ToString("yyyy-MM-dd").Replace(' ', 'T');
                if (checkLichCongTac.lichcongtac_cuochopto == true)
                    chkHopTo.Checked = true;
                else
                    chkHopTo.Checked = false;
                if (checkLichCongTac.lichcongtac_cuochopnhom == true)
                    chkHopNhom.Checked = true;
                else
                    chkHopNhom.Checked = false;
                var checkLichCongTacChiTiet = from lctct in db.tbLichCongtacChiTiets
                                              where lctct.lichcongtac_id == Convert.ToInt32(RouteData.Values["id"])
                                              select lctct;
                txtThu2.Value = checkLichCongTacChiTiet.First().lichcongtacchitiet_ngay;
                txtThu3.Value = checkLichCongTacChiTiet.Skip(1).Take(1).Single().lichcongtacchitiet_ngay;
                txtThu4.Value = checkLichCongTacChiTiet.Skip(2).Take(1).Single().lichcongtacchitiet_ngay;
                txtThu5.Value = checkLichCongTacChiTiet.Skip(3).Take(1).Single().lichcongtacchitiet_ngay;
                txtThu6.Value = checkLichCongTacChiTiet.Skip(4).Take(1).Single().lichcongtacchitiet_ngay;
                txtThu7.Value = checkLichCongTacChiTiet.Skip(5).Take(1).Single().lichcongtacchitiet_ngay;

                txtNoiDung1.Value = checkLichCongTacChiTiet.First().lichcongtacchitiet_noidung;
                txtNoiDung2.Value = checkLichCongTacChiTiet.Skip(1).Take(1).Single().lichcongtacchitiet_noidung;
                txtNoiDung3.Value = checkLichCongTacChiTiet.Skip(2).Take(1).Single().lichcongtacchitiet_noidung;
                txtNoiDung4.Value = checkLichCongTacChiTiet.Skip(3).Take(1).Single().lichcongtacchitiet_noidung;
                txtNoiDung5.Value = checkLichCongTacChiTiet.Skip(4).Take(1).Single().lichcongtacchitiet_noidung;
                txtNoiDung6.Value = checkLichCongTacChiTiet.Skip(5).Take(1).Single().lichcongtacchitiet_noidung;

                txtGhiChu1.Value = checkLichCongTacChiTiet.First().lichcongtacchitiet_ghichu;
                txtGhiChu2.Value = checkLichCongTacChiTiet.Skip(1).Take(1).Single().lichcongtacchitiet_ghichu;
                txtGhiChu3.Value = checkLichCongTacChiTiet.Skip(2).Take(1).Single().lichcongtacchitiet_ghichu;
                txtGhiChu4.Value = checkLichCongTacChiTiet.Skip(3).Take(1).Single().lichcongtacchitiet_ghichu;
                txtGhiChu5.Value = checkLichCongTacChiTiet.Skip(4).Take(1).Single().lichcongtacchitiet_ghichu;
                txtGhiChu6.Value = checkLichCongTacChiTiet.Skip(5).Take(1).Single().lichcongtacchitiet_ghichu;
            }
        }
    }
    protected void btnLuu_Click(object sender, EventArgs e)
    {
        try
        {

            if (RouteData.Values["id"].ToString() == "1")
            {
                if (ddlTuan.Text == "" && txtTuNgay.Value == "" && txtDenNgay.Value == "")
                {
                    alert.alert_Warning(Page, "Vui lòng nhập đầy đủ thông tin", "");
                }
                else
                {
                    tbLichCongTac insert = new tbLichCongTac();
                    insert.image_lich = Convert.ToInt16(ddlTuan.Value);
                    insert.lichcongtac_tungay = Convert.ToDateTime(txtTuNgay.Value);
                    insert.lichcongtac_denngay = Convert.ToDateTime(txtDenNgay.Value);
                    insert.lichcongtac_tuan = ddlTuan.Text;
                    insert.lichcongtac_title = "Lịch công tác hàng tuần";
                    if (chkHopTo.Checked == true)
                        insert.lichcongtac_cuochopto = true;
                    else
                        insert.lichcongtac_cuochopto = false;
                    if (chkHopNhom.Checked == true)
                        insert.lichcongtac_cuochopnhom = true;
                    else
                        insert.lichcongtac_cuochopnhom = false;
                    db.tbLichCongTacs.InsertOnSubmit(insert);
                    db.SubmitChanges();

                    tbLichCongtacChiTiet insertDetail2 = new tbLichCongtacChiTiet();
                    insertDetail2.lichcongtacchitiet_thu = "Thứ 2";
                    insertDetail2.lichcongtacchitiet_noidung = txtNoiDung1.Value;
                    insertDetail2.lichcongtacchitiet_ghichu = txtGhiChu1.Value;
                    insertDetail2.lichcongtac_id = insert.lichcongtac_id;
                    db.tbLichCongtacChiTiets.InsertOnSubmit(insertDetail2);

                    tbLichCongtacChiTiet insertDetail3 = new tbLichCongtacChiTiet();
                    insertDetail3.lichcongtacchitiet_thu = "Thứ 3";
                    insertDetail3.lichcongtacchitiet_noidung = txtNoiDung2.Value;
                    insertDetail3.lichcongtacchitiet_ghichu = txtGhiChu2.Value;
                    insertDetail3.lichcongtac_id = insert.lichcongtac_id;
                    db.tbLichCongtacChiTiets.InsertOnSubmit(insertDetail3);

                    tbLichCongtacChiTiet insertDetail4 = new tbLichCongtacChiTiet();
                    insertDetail4.lichcongtacchitiet_thu = "Thứ 4";
                    insertDetail4.lichcongtacchitiet_noidung = txtNoiDung3.Value;
                    insertDetail4.lichcongtacchitiet_ghichu = txtGhiChu3.Value;
                    insertDetail4.lichcongtac_id = insert.lichcongtac_id;
                    db.tbLichCongtacChiTiets.InsertOnSubmit(insertDetail4);

                    tbLichCongtacChiTiet insertDetail5 = new tbLichCongtacChiTiet();
                    insertDetail5.lichcongtacchitiet_thu = "Thứ 5";
                    insertDetail5.lichcongtacchitiet_noidung = txtNoiDung4.Value;
                    insertDetail5.lichcongtacchitiet_ghichu = txtGhiChu4.Value;
                    insertDetail5.lichcongtac_id = insert.lichcongtac_id;
                    db.tbLichCongtacChiTiets.InsertOnSubmit(insertDetail5);

                    tbLichCongtacChiTiet insertDetail6 = new tbLichCongtacChiTiet();
                    insertDetail6.lichcongtacchitiet_thu = "Thứ 6";
                    insertDetail6.lichcongtacchitiet_noidung = txtNoiDung5.Value;
                    insertDetail6.lichcongtacchitiet_ghichu = txtGhiChu5.Value;
                    insertDetail6.lichcongtac_id = insert.lichcongtac_id;
                    db.tbLichCongtacChiTiets.InsertOnSubmit(insertDetail6);

                    tbLichCongtacChiTiet insertDetail7 = new tbLichCongtacChiTiet();
                    insertDetail7.lichcongtacchitiet_thu = "Thứ 7";
                    insertDetail7.lichcongtacchitiet_noidung = txtNoiDung6.Value;
                    insertDetail7.lichcongtacchitiet_ghichu = txtGhiChu6.Value;
                    insertDetail7.lichcongtac_id = insert.lichcongtac_id;
                    db.tbLichCongtacChiTiets.InsertOnSubmit(insertDetail7);
                    db.SubmitChanges();
                    alert.alert_Success(Page, "Hoàn thành tạo lịch", "");
                }
            }
            else
            {
                tbLichCongTac updateLichCongTac = (from lct in db.tbLichCongTacs
                                                   where lct.lichcongtac_id == Convert.ToInt32(RouteData.Values["id"])
                                                   select lct).SingleOrDefault();
                updateLichCongTac.image_lich = Convert.ToInt16(ddlTuan.Value);
                updateLichCongTac.lichcongtac_tuan = ddlTuan.Text;
                if (txtTuNgay.Value == "" && txtDenNgay.Value == "")
                {

                }
                else
                {
                    updateLichCongTac.lichcongtac_tungay = Convert.ToDateTime(txtTuNgay.Value);
                    updateLichCongTac.lichcongtac_denngay = Convert.ToDateTime(txtDenNgay.Value);
                }
                if (chkHopTo.Checked == true)
                    updateLichCongTac.lichcongtac_cuochopto = true;
                else
                    updateLichCongTac.lichcongtac_cuochopto = false;
                if (chkHopNhom.Checked == true)
                    updateLichCongTac.lichcongtac_cuochopnhom = true;
                else
                    updateLichCongTac.lichcongtac_cuochopnhom = false;
                db.SubmitChanges();
                var checkChiTiet = from lctct in db.tbLichCongtacChiTiets
                                   where lctct.lichcongtac_id == Convert.ToInt32(RouteData.Values["id"])
                                   select lctct;
                tbLichCongtacChiTiet updateChiTiet1 = checkChiTiet.First();
                updateChiTiet1.lichcongtacchitiet_noidung = txtNoiDung1.Value;
                updateChiTiet1.lichcongtacchitiet_ghichu = txtGhiChu1.Value;
                db.SubmitChanges();
                tbLichCongtacChiTiet updateChiTiet2 = checkChiTiet.Skip(1).Take(1).Single();
                updateChiTiet2.lichcongtacchitiet_noidung = txtNoiDung2.Value;
                updateChiTiet2.lichcongtacchitiet_ghichu = txtGhiChu2.Value;
                db.SubmitChanges();
                tbLichCongtacChiTiet updateChiTiet3 = checkChiTiet.Skip(2).Take(1).Single();
                updateChiTiet3.lichcongtacchitiet_noidung = txtNoiDung3.Value;
                updateChiTiet3.lichcongtacchitiet_ghichu = txtGhiChu3.Value;
                db.SubmitChanges();
                tbLichCongtacChiTiet updateChiTiet4 = checkChiTiet.Skip(3).Take(1).Single();
                updateChiTiet4.lichcongtacchitiet_noidung = txtNoiDung4.Value;
                updateChiTiet4.lichcongtacchitiet_ghichu = txtGhiChu4.Value;
                db.SubmitChanges();
                tbLichCongtacChiTiet updateChiTiet5 = checkChiTiet.Skip(4).Take(1).Single();
                updateChiTiet5.lichcongtacchitiet_noidung = txtNoiDung5.Value;
                updateChiTiet5.lichcongtacchitiet_ghichu = txtGhiChu5.Value;
                db.SubmitChanges();
                tbLichCongtacChiTiet updateChiTiet6 = checkChiTiet.Skip(5).Take(1).Single();
                updateChiTiet6.lichcongtacchitiet_noidung = txtNoiDung6.Value;
                updateChiTiet6.lichcongtacchitiet_ghichu = txtGhiChu6.Value;
                db.SubmitChanges();
                alert.alert_Success(Page, "Đã cập nhật lại lịch công tác", "");
            }

        }
        catch (Exception)
        {
            alert.alert_Error(Page, "Lỗi, vui lòng liên hệ IT!", "");
        }
    }

    protected void btnQuayLai_Click(object sender, EventArgs e)
    {
        Response.Redirect("admin-lich-cong-tac-hang-tuan");
    }
}