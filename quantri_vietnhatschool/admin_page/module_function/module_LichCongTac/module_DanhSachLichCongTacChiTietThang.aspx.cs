﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_functionmodule_DanhSachLichCongTacChiTietThang : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    string image;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ddlThang.DataSource = from t in db.tbImage_Liches where t.image_group == 2 select t;
            ddlThang.DataBind();
            if (RouteData.Values["id"].ToString() != "1")
            {
                var checkLichCongTacThang = (from lct in db.tbLichCongTac_Thangs
                                             join i in db.tbImage_Liches on lct.image_id equals i.image_lich
                                             where lct.lichcongtac_thang_id == Convert.ToInt32(RouteData.Values["id"])
                                             select new
                                             {
                                                 lct.lichcongtac_thang_title,
                                                 lct.lichcongtac_thang_content,
                                                i.image_link,
                                             }).SingleOrDefault();
                txtTieuDe.Text = checkLichCongTacThang.lichcongtac_thang_title;
                ddlThang.Text = checkLichCongTacThang.image_link;
                //edtnoidung.Html = checkLichCongTacThang.lichcongtac_thang_content;
            }
        }
    }
    protected void btnLuu_Click(object sender, EventArgs e)
    {
        try
        {
            if (Page.IsValid && FileUpload1.HasFile)
            {
                String folderUser = Server.MapPath("~/uploadimages/file-lich-cong-tac-thang/");
                if (!Directory.Exists(folderUser))
                {
                    Directory.CreateDirectory(folderUser);
                }
                //string filename;
                string ulr = "/uploadimages/file-lich-cong-tac-thang/";
                HttpFileCollection hfc = Request.Files;
                //string filename = Path.GetRandomFileName() + Path.GetExtension(FileUpload1.FileName);
                //string fileName_save = Path.Combine(Server.MapPath("~/uploadimages/file-mau"), filename);
                string filename = Path.GetFileName(FileUpload1.FileName);
                string fileName_save = Path.Combine(Server.MapPath("~/uploadimages/file-lich-cong-tac-thang/"), filename);
                FileUpload1.SaveAs(fileName_save);
                image = ulr + filename;
            }
            if (RouteData.Values["id"].ToString() == "1")
            {
                tbLichCongTac_Thang insert = new tbLichCongTac_Thang();
                insert.lichcongtac_thang_title = txtTieuDe.Text;
                insert.image_id = Convert.ToInt16(ddlThang.Value);
                insert.lichcongtac_thang_content = image;
                db.tbLichCongTac_Thangs.InsertOnSubmit(insert);
                db.SubmitChanges();
                alert.alert_Success(Page, "Hoàn thành tạo lịch", "");

            }
            else
            {
                tbLichCongTac_Thang update = (from lct in db.tbLichCongTac_Thangs where lct.lichcongtac_thang_id == Convert.ToInt32(RouteData.Values["id"].ToString()) select lct).SingleOrDefault();
                update.lichcongtac_thang_title = txtTieuDe.Text;
                update.lichcongtac_thang_content = image;
                update.image_id = Convert.ToInt16(ddlThang.Value);
                db.SubmitChanges();
                alert.alert_Success(Page, "Đã cập nhật lại lịch công tác", "");
            }

        }
        catch (Exception)
        {
            alert.alert_Error(Page, "Lỗi, vui lòng liên hệ IT!", "");
        }
    }

    protected void btnQuayLai_Click(object sender, EventArgs e)
    {
        Response.Redirect("admin-danh-sach-lich-cong-tac-thang");
    }
}