﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_LichBaoGiangBoMon.aspx.cs" Inherits="admin_page_module_function_module_WebSite_module_LichBaoGiangBoMon" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <script>
        function confirmDel() {
            swal("Bạn có thực sự muốn xóa?",
                "Nếu xóa, dữ liệu sẽ không thể khôi phục.",
                "warning",
                {
                    buttons: true,
                    dangerMode: true
                }).then(function (value) {
                    if (value == true) {
                        var xoa = document.getElementById('<%=btnXoa.ClientID%>');
                        xoa.click();
                    }
                });
        }
    </script>

    <div class="card card-block">
        <div class="form-group row">
            <div class="col-sm-12">
                <asp:UpdatePanel ID="udButton" runat="server">
                    <ContentTemplate>
                        <div class="col-lg-8">
                            <asp:Button ID="btnThem" runat="server" Text="Thêm" CssClass="btn btn-primary" OnClick="btnThem_Click" />
                            <asp:Button ID="btnXem" runat="server" Text="Xem và thêm mới" CssClass="btn btn-primary" OnClick="btnXem_Click" />
                            <asp:Button ID="btnChiTiet" runat="server" Text="Cập nhật" CssClass="btn btn-primary" OnClick="btnChiTiet_Click" />
                            <input type="submit" class="btn btn-primary" value="Xóa" onclick="confirmDel()" />
                            <asp:Button ID="btnXoa" runat="server" CssClass="invisible" OnClick="btnXoa_Click" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div class="col-lg-4">
                <%-- <span style="padding: 3% 0 1% 23%;font-size: 17px;">Năm học</span>--%>
                <asp:DropDownList ID="ddlNamHoc" runat="server" CssClass="form-control col-lg-4" Width="44%" OnSelectedIndexChanged="ddlNamHoc_SelectedIndexChanged" AutoPostBack="true" Style="float: right;">
                    <asp:ListItem Value="3">2021-2022</asp:ListItem>
                    <asp:ListItem Value="1">2020-2021</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>

    </div>
    <div class="form-group table-responsive">
        <dx:ASPxGridView ID="grvList" runat="server" CssClass="table-hover" ClientInstanceName="grvList" KeyFieldName="lichbaogiang_id" Width="100%">
            <Columns>
                <dx:GridViewCommandColumn ShowSelectCheckbox="True" SelectAllCheckboxMode="Page" VisibleIndex="0" Width="5%"></dx:GridViewCommandColumn>
                <dx:GridViewDataColumn Caption="Tuần lễ" FieldName="tuan_name" HeaderStyle-HorizontalAlign="Center" Width="20%"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn Caption="Từ ngày" FieldName="lichbaogiang_tungay" HeaderStyle-HorizontalAlign="Center" Width="20%"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn Caption="Đến ngày" FieldName="lichbaogiang_denngay" HeaderStyle-HorizontalAlign="Center" Width="20%"></dx:GridViewDataColumn>
                <dx:GridViewDataColumn Caption="Khối" FieldName="khoi_name" HeaderStyle-HorizontalAlign="Center" Width="20%"></dx:GridViewDataColumn>
            </Columns>
            <SettingsSearchPanel Visible="true" />
            <SettingsBehavior AllowFocusedRow="true" />
            <SettingsText EmptyDataRow="Trống" SearchPanelEditorNullText="Gỏ từ cần tìm kiếm và enter..." />
            <SettingsLoadingPanel Text="Đang tải..." />
            <SettingsPager PageSize="10" Summary-Text="Trang {0} / {1} ({2} trang)"></SettingsPager>
        </dx:ASPxGridView>
    </div>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

