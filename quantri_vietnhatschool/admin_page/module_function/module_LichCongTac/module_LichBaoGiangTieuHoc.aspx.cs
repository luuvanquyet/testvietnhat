﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_WebSite_module_LichBaoGiangTieuHoc : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {
        //lấy thông tin của tk đang nhập
        var getuser = (from u in db.admin_Users
                       where u.username_username == Request.Cookies["UserName"].Value
                       select u).FirstOrDefault();
        //đổ dữu liệu lên gridview
        var getData = from lbg in db.tbLichBaoGiangs
                      join k in db.tbKhois on lbg.khoi_id equals k.khoi_id
                      join t in db.tbHocTap_Tuans on lbg.tuan_id equals t.tuan_id
                      where lbg.username_id == getuser.username_id 
                      && lbg.hidden==false
                      orderby lbg.lichbaogiang_id descending
                      select new
                      {
                          lbg.lichbaogiang_id,
                          lbg.lichbaogiang_tungay,
                          lbg.lichbaogiang_denngay,
                          k.khoi_id,
                          k.khoi_name,
                          t.tuan_id, 
                          t.tuan_name
                      };
        grvList.DataSource = getData;
        grvList.DataBind();
        if (getData.Count() > 0)
        {
            btnThem.Visible = false;
            btnXem.Visible = true;
        }
        else
        {
            btnThem.Visible = true;
            btnXem.Visible = false;
        }
    }

    protected void btnThem_Click(object sender, EventArgs e)
    {
        _id = 1;
        Response.Redirect("/admin-them-lich-bao-giang-khoi-tieu-hoc-" + 1);
    }

    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "lichbaogiang_id" }));
        if (_id == 0)
        {
            alert.alert_Error(Page, "Bạn chưa chọn dữ liệu để cập nhật!", "");
        }
        else
        {
            Session["idlichbaogiang"] = _id;
            Response.Redirect("/admin-them-lich-bao-giang-khoi-tieu-hoc-" + _id);
        }
    }

    protected void btnXoa_Click(object sender, EventArgs e)
    {
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "lichbaogiang_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                tbLichBaoGiang delete = db.tbLichBaoGiangs.Where(x => x.lichbaogiang_id == Convert.ToInt32(item)).FirstOrDefault();
                db.tbLichBaoGiangs.DeleteOnSubmit(delete);
                try
                {
                    db.SubmitChanges();
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "AlertBox", "swal('Xóa thành công!', '','success').then(function(){window.location = '/admin-lich-bao-giang-khoi-tieu-hoc';})", true);
                }
                catch
                {
                }
            }
        }
        else
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
    }

    protected void btnXem_Click(object sender, EventArgs e)
    {
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "lichbaogiang_id" }));
        if (_id == 0)
        {
            alert.alert_Error(Page, "Bạn chưa chọn dữ liệu để xem!", "");
        }
        else
        {
            Session["idlichbaogiang"] = _id;
            Response.Redirect("/admin-them-lich-bao-giang-khoi-tieu-hoc/formart-" + _id);
        }
    }
}