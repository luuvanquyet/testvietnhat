﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_ThemLichBaoGiang_Formart.aspx.cs" Inherits="admin_page_module_function_module_WebSite_module_ThemLichBaoGiang_Formart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <style>
        .inputmon {
            width: 100%;
            padding-left: 5px;
        }

        .inputtextarea {
            width: 100%;
        }

        .table th, .table td {
            border: 1px solid #a6a9ab;
        }

        .table tr.tbheader {
            background: #a3a7a199;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <script type="text/javascript"> 
        <%--function checkNULL() {
            var CityName = document.getElementById('<%= txtTuan.ClientID%>');

            if (CityName.value.trim() == "") {
                swal('Tuần học không được để trống!', '', 'warning').then(function () { CityName.focus(); });
                return false;
            }
            return true;
        }--%>
    </script>
    <div class="card card-block">
        <div class="form-group row">
            <asp:UpdatePanel ID="udButton" runat="server">
                <ContentTemplate>
                    <div class="col-12">
                        <asp:Button ID="btnQuayLai" runat="server" Text="Quay lại trang danh sách" CssClass="btn btn-primary" OnClick="btnQuayLai_Click" />
                    </div>
                    <div class="col-12">
                        <div class="col-2">
                            <div class="col-12 form-group">
                                <label class="col-5 form-control-label">Khối học:</label>
                                <dx:ASPxComboBox ID="ddlKhoiHoc" runat="server" ValueType="System.Int32" TextField="khoi_name" ValueField="khoi_id" NullText="Chọn khối học" ClientInstanceName="ddlKhoiHoc" CssClass="" Width="100%"></dx:ASPxComboBox>
                            </div>
                        </div>
                        <div class="col-2" style="margin-left: 30px;">
                            <div class="col-12 form-group">
                                <label class="col-5 form-control-label">Tuần:</label>
                                <dx:ASPxComboBox ID="ddlTuanHoc" runat="server" ValueType="System.Int32" TextField="tuan_name" ValueField="tuan_id" NullText="Chọn tuần học" ClientInstanceName="ddlTuanHoc" CssClass="" Width="100%"></dx:ASPxComboBox>
                            </div>
                        </div>
                        <%--<div class="col-2" style="margin-left: 30px;">
                            <div class="col-12 form-group">
                                <label class="col-5 form-control-label">Từ ngày:</label>
                                <input type="date" value="" runat="server" id="txtTuNgay" style="line-height: 30px;" />
                            </div>
                        </div>
                        <div class="col-2" style="margin-left: 30px;">
                            <div class="col-12 form-group">
                                <label class="col-5 form-control-label">Đến ngày:</label>
                                <input type="date" value="" runat="server" id="txtDenNgay" style="line-height: 30px;" />
                            </div>
                        </div>--%>
                    </div>
                    <div runat="server" id="dvThu2">
                        <table class="table table-hover">
                            <tr class="tbheader">
                                <th style="text-align: center">Thứ</th>
                                <th style="text-align: center">Buổi</th>
                                <th style="text-align: center">Tiết</th>
                                <th style="text-align: center; width: 10%">Môn</th>
                                <th style="text-align: center; width: 7%">Tiết
                                <br />
                                    (PPCT)</th>
                                <th style="text-align: center">Tên bài giảng</th>
                                <th style="text-align: center">Ghi chú</th>
                            </tr>
                            <%--thứ 2--%>
                            <tr>
                                <td rowspan="9">Thứ 2</td>
                                <td style="text-align: center" class="a" runat="server" id="txtThu2_sang">Sáng</td>
                                <td style="text-align: center" runat="server" id="Thu2_row1_td1">1</td>
                                <td style="text-align: center" runat="server" id="Thu2_row1_td2">
                                    <input class="inputmon" runat="server" id="txtThu2Tiet1_Mon" />
                                </td>
                                <td style="text-align: center" runat="server" id="Thu2_row1_td4">
                                    <textarea class="inputtextarea" runat="server" id="txtThu2Tiet1_TCT" cols="10" rows="2"></textarea>
                                </td>
                                <td runat="server" id="Thu2_row1_td5">
                                    <textarea class="inputtextarea" runat="server" id="txtThu2Tiet1_TenBaiGiang" cols="15" rows="2"></textarea>
                                </td>
                                <td runat="server" id="Thu2_row1_td6">
                                    <textarea class="inputtextarea" runat="server" id="txtThu2Tiet1_GhiChu" cols="15" rows="2"></textarea>
                                </td>
                            </tr>
                            <tr runat="server" id="Thu2_row2">
                                <td class="a"></td>
                                <td style="text-align: center">2</td>
                                <td style="text-align: center">
                                    <input class="inputmon" runat="server" id="txtThu2Tiet2_Mon" />
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu2Tiet2_TCT" cols="10" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu2Tiet2_TenBaiGiang" cols="15" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu2Tiet2_GhiChu" cols="15" rows="2"></textarea>
                                </td>
                            </tr>
                            <tr runat="server" id="Thu2_row3">
                                <td class="a"></td>
                                <td style="text-align: center">3</td>
                                <td style="text-align: center">
                                    <input class="inputmon" runat="server" id="txtThu2Tiet3_Mon" />
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu2Tiet3_TCT" cols="10" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu2Tiet3_TenBaiGiang" cols="15" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu2Tiet3_GhiChu" cols="15" rows="2"></textarea>
                                </td>
                            </tr>
                            <tr runat="server" id="Thu2_row4">
                                <td></td>
                                <td style="text-align: center">4</td>
                                <td style="text-align: center">
                                    <input class="inputmon" runat="server" id="txtThu2Tiet4_Mon" />
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu2Tiet4_TCT" cols="10" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu2Tiet4_TenBaiGiang" cols="15" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu2Tiet4_GhiChu" cols="15" rows="2"></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center" class="a" runat="server" id="txtThu2_chieu">Chiều</td>
                                <td style="text-align: center" runat="server" id="Thu2_row5_td1">5</td>
                                <td style="text-align: center" runat="server" id="Thu2_row5_td2">
                                    <input class="inputmon" runat="server" id="txtThu2Tiet5_Mon" />
                                </td>
                                <td runat="server" id="Thu2_row5_td4">
                                    <textarea class="inputtextarea" runat="server" id="txtThu2Tiet5_TCT" cols="10" rows="2"></textarea>
                                </td>
                                <td runat="server" id="Thu2_row5_td5">
                                    <textarea class="inputtextarea" runat="server" id="txtThu2Tiet5_TenBaiGiang" cols="15" rows="2"></textarea>
                                </td>
                                <td runat="server" id="Thu2_row5_td6">
                                    <textarea class="inputtextarea" runat="server" id="txtThu2Tiet5_GhiChu" cols="15" rows="2"></textarea>
                                </td>
                            </tr>
                            <tr runat="server" id="Thu2_row6">
                                <td class="a"></td>
                                <td style="text-align: center">6</td>
                                <td style="text-align: center">
                                    <input class="inputmon" runat="server" id="txtThu2Tiet6_Mon" />
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu2Tiet6_TCT" cols="10" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu2Tiet6_TenBaiGiang" cols="15" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu2Tiet6_GhiChu" cols="15" rows="2"></textarea>
                                </td>
                            </tr>
                            <tr runat="server" id="Thu2_row7">
                                <td class="a"></td>
                                <td style="text-align: center">7</td>
                                <td style="text-align: center">
                                    <input class="inputmon" runat="server" id="txtThu2Tiet7_Mon" />
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu2Tiet7_TCT" cols="10" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu2Tiet7_TenBaiGiang" cols="15" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu2Tiet7_GhiChu" cols="15" rows="2"></textarea>
                                </td>
                            </tr>
                            <tr runat="server" id="Thu2_row8">
                                <td></td>
                                <td style="text-align: center">8</td>
                                <td style="text-align: center">
                                    <input class="inputmon" runat="server" id="txtThu2Tiet8_Mon" />
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu2Tiet8_TCT" cols="10" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu2Tiet8_TenBaiGiang" cols="15" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu2Tiet8_GhiChu" cols="15" rows="2"></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="7" style="text-align: center">
                                    <asp:Button ID="btnLuuThu2" runat="server" Text="Lưu và sang thứ 3" CssClass="btn btn-primary" OnClientClick="return checkNULL()" OnClick="btnLuuThu2_Click" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div runat="server" id="dvThu3">
                        <table class="table table-hover">
                            <tr class="tbheader">
                                <th style="text-align: center">Thứ</th>
                                <th style="text-align: center">Buổi</th>
                                <th style="text-align: center">Tiết</th>
                                <th style="text-align: center; width: 10%">Môn</th>
                                <th style="text-align: center; width: 7%">Tiết
                                <br />
                                    (PPCT)</th>
                                <th style="text-align: center">Tên bài giảng</th>
                                <th style="text-align: center">Ghi chú</th>
                            </tr>
                            <%--thứ 3--%>
                            <tr>
                                <td rowspan="9">Thứ 3</td>
                                <td style="text-align: center" class="a" runat="server" id="txtThu3_sang">Sáng</td>
                                <td style="text-align: center" runat="server" id="Thu3_row1_td1" class="a">1</td>
                                <td style="text-align: center" runat="server" id="Thu3_row1_td2">
                                    <input class="inputmon" runat="server" id="txtThu3Tiet1_Mon" />
                                </td>
                                <td runat="server" id="Thu3_row1_td4">
                                    <textarea class="inputtextarea" runat="server" id="txtThu3Tiet1_TCT" cols="10" rows="2"></textarea>
                                </td>
                                <td runat="server" id="Thu3_row1_td5">
                                    <textarea class="inputtextarea" runat="server" id="txtThu3Tiet1_TenBaiGiang" cols="15" rows="2"></textarea>
                                </td>
                                <td runat="server" id="Thu3_row1_td6">
                                    <textarea class="inputtextarea" runat="server" id="txtThu3Tiet1_GhiChu" cols="15" rows="2"></textarea>
                                </td>
                            </tr>
                            <tr runat="server" id="Thu3_row2">
                                <td class="a"></td>
                                <td style="text-align: center">2</td>
                                <td style="text-align: center">
                                    <input class="inputmon" runat="server" id="txtThu3Tiet2_Mon" />
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu3Tiet2_TCT" cols="10" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu3Tiet2_TenBaiGiang" cols="15" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu3Tiet2_GhiChu" cols="15" rows="2"></textarea>
                                </td>
                            </tr>
                            <tr runat="server" id="Thu3_row3">
                                <td class="a"></td>
                                <td style="text-align: center">3</td>
                                <td style="text-align: center">
                                    <input class="inputmon" runat="server" id="txtThu3Tiet3_Mon" />
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu3Tiet3_TCT" cols="10" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu3Tiet3_TenBaiGiang" cols="15" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu3Tiet3_GhiChu" cols="15" rows="2"></textarea>
                                </td>
                            </tr>
                            <tr runat="server" id="Thu3_row4">
                                <td></td>
                                <td style="text-align: center">4</td>
                                <td style="text-align: center">
                                    <input class="inputmon" runat="server" id="txtThu3Tiet4_Mon" />
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu3Tiet4_TCT" cols="10" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu3Tiet4_TenBaiGiang" cols="15" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu3Tiet4_GhiChu" cols="15" rows="2"></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center" class="a" runat="server" id="txtThu3_chieu">Chiều</td>
                                <td style="text-align: center" runat="server" id="Thu3_row5_td1">5</td>
                                <td style="text-align: center" runat="server" id="Thu3_row5_td2">
                                    <input class="inputmon" runat="server" id="txtThu3Tiet5_Mon" />
                                </td>
                                <td runat="server" id="Thu3_row5_td4">
                                    <textarea class="inputtextarea" runat="server" id="txtThu3Tiet5_TCT" cols="10" rows="2"></textarea>
                                </td>
                                <td runat="server" id="Thu3_row5_td5">
                                    <textarea class="inputtextarea" runat="server" id="txtThu3Tiet5_TenBaiGiang" cols="15" rows="2"></textarea>
                                </td>
                                <td runat="server" id="Thu3_row5_td6">
                                    <textarea class="inputtextarea" runat="server" id="txtThu3Tiet5_GhiChu" cols="15" rows="2"></textarea>
                                </td>
                            </tr>
                            <tr runat="server" id="Thu3_row6">
                                <td class="a"></td>
                                <td style="text-align: center">6</td>
                                <td style="text-align: center">
                                    <input class="inputmon" runat="server" id="txtThu3Tiet6_Mon" />
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu3Tiet6_TCT" cols="10" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu3Tiet6_TenBaiGiang" cols="15" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu3Tiet6_GhiChu" cols="15" rows="2"></textarea>
                                </td>
                            </tr>
                            <tr runat="server" id="Thu3_row7">
                                <td class="a"></td>
                                <td style="text-align: center">7</td>
                                <td style="text-align: center">
                                    <input class="inputmon" runat="server" id="txtThu3Tiet7_Mon" />
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu3Tiet7_TCT" cols="10" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu3Tiet7_TenBaiGiang" cols="15" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu3Tiet7_GhiChu" cols="15" rows="2"></textarea>
                                </td>
                            </tr>
                            <tr runat="server" id="Thu3_row8">
                                <td></td>
                                <td style="text-align: center">8</td>
                                <td style="text-align: center">
                                    <input class="inputmon" runat="server" id="txtThu3Tiet8_Mon" />
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu3Tiet8_TCT" cols="10" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu3Tiet8_TenBaiGiang" cols="15" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu3Tiet8_GhiChu" cols="15" rows="2"></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="7" style="text-align: center">
                                    <asp:Button ID="btnLuuThu3" runat="server" Text="Lưu và sang thứ 4" CssClass="btn btn-primary" OnClick="btnLuuThu3_Click" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div runat="server" id="dvThu4">
                        <table class="table table-hover">
                            <tr class="tbheader">
                                <th style="text-align: center">Thứ</th>
                                <th style="text-align: center">Buổi</th>
                                <th style="text-align: center">Tiết</th>
                                <th style="text-align: center; width: 10%">Môn</th>
                                <th style="text-align: center; width: 7%">Tiết
                                <br />
                                    (PPCT)</th>
                                <th style="text-align: center">Tên bài giảng</th>
                                <th style="text-align: center">Ghi chú</th>
                            </tr>
                            <%--thứ 4--%>
                            <tr>
                                <td rowspan="9">Thứ 4</td>
                                <td style="text-align: center" class="a" runat="server" id="txtThu4_sang">Sáng</td>
                                <td style="text-align: center" runat="server" id="Thu4_row1_td1">1</td>
                                <td style="text-align: center" runat="server" id="Thu4_row1_td2">
                                    <input class="inputmon" runat="server" id="txtThu4Tiet1_Mon" />
                                </td>
                                <td runat="server" id="Thu4_row1_td4">
                                    <textarea class="inputtextarea" runat="server" id="txtThu4Tiet1_TCT" cols="10" rows="2"></textarea>
                                </td>
                                <td runat="server" id="Thu4_row1_td5">
                                    <textarea class="inputtextarea" runat="server" id="txtThu4Tiet1_TenBaiGiang" cols="15" rows="2"></textarea>
                                </td>
                                <td runat="server" id="Thu4_row1_td6">
                                    <textarea class="inputtextarea" runat="server" id="txtThu4Tiet1_GhiChu" cols="15" rows="2"></textarea>
                                </td>
                            </tr>
                            <tr runat="server" id="Thu4_row2">
                                <td class="a"></td>
                                <td style="text-align: center">2</td>
                                <td style="text-align: center">
                                    <input class="inputmon" runat="server" id="txtThu4Tiet2_Mon" />
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu4Tiet2_TCT" cols="10" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu4Tiet2_TenBaiGiang" cols="15" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu4Tiet2_GhiChu" cols="15" rows="2"></textarea>
                                </td>
                            </tr>
                            <tr runat="server" id="Thu4_row3">
                                <td class="a"></td>
                                <td style="text-align: center">3</td>
                                <td style="text-align: center">
                                    <input class="inputmon" runat="server" id="txtThu4Tiet3_Mon" />
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu4Tiet3_TCT" cols="10" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu4Tiet3_TenBaiGiang" cols="15" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu4Tiet3_GhiChu" cols="15" rows="2"></textarea>
                                </td>
                            </tr>
                            <tr runat="server" id="Thu4_row4">
                                <td></td>
                                <td style="text-align: center">4</td>
                                <td style="text-align: center">
                                    <input class="inputmon" runat="server" id="txtThu4Tiet4_Mon" />
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu4Tiet4_TCT" cols="10" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu4Tiet4_TenBaiGiang" cols="15" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu4Tiet4_GhiChu" cols="15" rows="2"></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center" class="a" runat="server" id="txtThu4_chieu">Chiều</td>
                                <td style="text-align: center" runat="server" id="Thu4_row5_td1">5</td>
                                <td style="text-align: center" runat="server" id="Thu4_row5_td2">
                                    <input class="inputmon" runat="server" id="txtThu4Tiet5_Mon" />
                                </td>
                                <td runat="server" id="Thu4_row5_td4">
                                    <textarea class="inputtextarea" runat="server" id="txtThu4Tiet5_TCT" cols="10" rows="2"></textarea>
                                </td>
                                <td runat="server" id="Thu4_row5_td5">
                                    <textarea class="inputtextarea" runat="server" id="txtThu4Tiet5_TenBaiGiang" cols="15" rows="2"></textarea>
                                </td>
                                <td runat="server" id="Thu4_row5_td6">
                                    <textarea class="inputtextarea" runat="server" id="txtThu4Tiet5_GhiChu" cols="15" rows="2"></textarea>
                                </td>
                            </tr>
                            <tr runat="server" id="Thu4_row6">
                                <td class="a"></td>
                                <td style="text-align: center">6</td>
                                <td style="text-align: center">
                                    <input class="inputmon" runat="server" id="txtThu4Tiet6_Mon" />
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu4Tiet6_TCT" cols="10" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu4Tiet6_TenBaiGiang" cols="15" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu4Tiet6_GhiChu" cols="15" rows="2"></textarea>
                                </td>
                            </tr>
                            <tr runat="server" id="Thu4_row7">
                                <td class="a"></td>
                                <td style="text-align: center">7</td>
                                <td style="text-align: center">
                                    <input class="inputmon" runat="server" id="txtThu4Tiet7_Mon" />
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu4Tiet7_TCT" cols="10" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu4Tiet7_TenBaiGiang" cols="15" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu4Tiet7_GhiChu" cols="15" rows="2"></textarea>
                                </td>
                            </tr>
                            <tr runat="server" id="Thu4_row8">
                                <td></td>
                                <td style="text-align: center">8</td>
                                <td style="text-align: center">
                                    <input class="inputmon" runat="server" id="txtThu4Tiet8_Mon" />
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu4Tiet8_TCT" cols="10" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu4Tiet8_TenBaiGiang" cols="15" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu4Tiet8_GhiChu" cols="15" rows="2"></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="7" style="text-align: center">
                                    <asp:Button ID="btnLuuThu4" runat="server" Text="Lưu và sang thứ 5" CssClass="btn btn-primary" OnClick="btnLuuThu4_Click" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div runat="server" id="dvThu5">
                        <table class="table table-hover">
                            <tr class="tbheader">
                                <th style="text-align: center">Thứ</th>
                                <th style="text-align: center">Buổi</th>
                                <th style="text-align: center">Tiết</th>
                                <th style="text-align: center; width: 10%">Môn</th>
                                <th style="text-align: center; width: 7%">Tiết
                                <br />
                                    (PPCT)</th>
                                <th style="text-align: center">Tên bài giảng</th>
                                <th style="text-align: center">Ghi chú</th>
                            </tr>
                            <%--thứ 5--%>
                            <tr>
                                <td rowspan="9">Thứ 5</td>
                                <td style="text-align: center" class="a" runat="server" id="txtThu5_sang">Sáng</td>
                                <td style="text-align: center" runat="server" id="Thu5_row1_td1">1</td>
                                <td style="text-align: center" runat="server" id="Thu5_row1_td2">
                                    <input class="inputmon" runat="server" id="txtThu5Tiet1_Mon" />
                                </td>
                                <td runat="server" id="Thu5_row1_td4">
                                    <textarea class="inputtextarea" runat="server" id="txtThu5Tiet1_TCT" cols="10" rows="2"></textarea>
                                </td>
                                <td runat="server" id="Thu5_row1_td5">
                                    <textarea class="inputtextarea" runat="server" id="txtThu5Tiet1_TenBaiGiang" cols="15" rows="2"></textarea>
                                </td>
                                <td runat="server" id="Thu5_row1_td6">
                                    <textarea class="inputtextarea" runat="server" id="txtThu5Tiet1_GhiChu" cols="15" rows="2"></textarea>
                                </td>
                            </tr>
                            <tr runat="server" id="Thu5_row2">
                                <td class="a"></td>
                                <td style="text-align: center">2</td>
                                <td style="text-align: center">
                                    <input class="inputmon" runat="server" id="txtThu5Tiet2_Mon" />
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu5Tiet2_TCT" cols="10" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu5Tiet2_TenBaiGiang" cols="15" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu5Tiet2_GhiChu" cols="15" rows="2"></textarea>
                                </td>
                            </tr>
                            <tr runat="server" id="Thu5_row3">
                                <td class="a"></td>
                                <td style="text-align: center">3</td>
                                <td style="text-align: center">
                                    <input class="inputmon" runat="server" id="txtThu5Tiet3_Mon" />
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu5Tiet3_TCT" cols="10" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu5Tiet3_TenBaiGiang" cols="15" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu5Tiet3_GhiChu" cols="15" rows="2"></textarea>
                                </td>
                            </tr>
                            <tr runat="server" id="Thu5_row4">
                                <td></td>
                                <td style="text-align: center">4</td>
                                <td style="text-align: center">
                                    <input class="inputmon" runat="server" id="txtThu5Tiet4_Mon" />
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu5Tiet4_TCT" cols="10" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu5Tiet4_TenBaiGiang" cols="15" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu5Tiet4_GhiChu" cols="15" rows="2"></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center" class="a" runat="server" id="txtThu5_chieu">Chiều</td>
                                <td style="text-align: center" runat="server" id="Thu5_row5_td1">5</td>
                                <td style="text-align: center" runat="server" id="Thu5_row5_td2">
                                    <input class="inputmon" runat="server" id="txtThu5Tiet5_Mon" />
                                </td>
                                <td runat="server" id="Thu5_row5_td4">
                                    <textarea class="inputtextarea" runat="server" id="txtThu5Tiet5_TCT" cols="10" rows="2"></textarea>
                                </td>
                                <td runat="server" id="Thu5_row5_td5">
                                    <textarea class="inputtextarea" runat="server" id="txtThu5Tiet5_TenBaiGiang" cols="15" rows="2"></textarea>
                                </td>
                                <td runat="server" id="Thu5_row5_td6">
                                    <textarea class="inputtextarea" runat="server" id="txtThu5Tiet5_GhiChu" cols="15" rows="2"></textarea>
                                </td>
                            </tr>
                            <tr runat="server" id="Thu5_row6">
                                <td class="a"></td>
                                <td style="text-align: center">6</td>
                                <td style="text-align: center">
                                    <input class="inputmon" runat="server" id="txtThu5Tiet6_Mon" />
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu5Tiet6_TCT" cols="10" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu5Tiet6_TenBaiGiang" cols="15" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu5Tiet6_GhiChu" cols="15" rows="2"></textarea>
                                </td>
                            </tr>
                            <tr runat="server" id="Thu5_row7">
                                <td class="a"></td>
                                <td style="text-align: center">7</td>
                                <td style="text-align: center">
                                    <input class="inputmon" runat="server" id="txtThu5Tiet7_Mon" />
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu5Tiet7_TCT" cols="10" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu5Tiet7_TenBaiGiang" cols="15" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu5Tiet7_GhiChu" cols="15" rows="2"></textarea>
                                </td>
                            </tr>
                            <tr runat="server" id="Thu5_row8">
                                <td></td>
                                <td style="text-align: center">8</td>
                                <td style="text-align: center">
                                    <input class="inputmon" runat="server" id="txtThu5Tiet8_Mon" />
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu5Tiet8_TCT" cols="10" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu5Tiet8_TenBaiGiang" cols="15" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu5Tiet8_GhiChu" cols="15" rows="2"></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="7" style="text-align: center">
                                    <asp:Button ID="btnLuuThu5" runat="server" Text="Lưu và sang thứ 6" CssClass="btn btn-primary" OnClick="btnLuuThu5_Click" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div runat="server" id="dvThu6">
                        <table class="table table-hover">
                            <tr class="tbheader">
                                <th style="text-align: center">Thứ</th>
                                <th style="text-align: center">Buổi</th>
                                <th style="text-align: center">Tiết</th>
                                <th style="text-align: center; width: 10%">Môn</th>
                                <th style="text-align: center; width: 7%">Tiết
                                <br />
                                    (PPCT)</th>
                                <th style="text-align: center">Tên bài giảng</th>
                                <th style="text-align: center">Ghi chú</th>
                            </tr>
                            <%--thứ 6--%>
                            <tr>
                                <td rowspan="9">Thứ 6</td>
                                <td style="text-align: center" class="a" runat="server" id="txtThu6_sang">Sáng</td>
                                <td style="text-align: center" runat="server" id="Thu6_row1_td1">1</td>
                                <td style="text-align: center" runat="server" id="Thu6_row1_td2">
                                    <input class="inputmon" runat="server" id="txtThu6Tiet1_Mon" />
                                </td>
                                <td runat="server" id="Thu6_row1_td4">
                                    <textarea class="inputtextarea" runat="server" id="txtThu6Tiet1_TCT" cols="10" rows="2"></textarea>
                                </td>
                                <td runat="server" id="Thu6_row1_td5">
                                    <textarea class="inputtextarea" runat="server" id="txtThu6Tiet1_TenBaiGiang" cols="15" rows="2"></textarea>
                                </td>
                                <td runat="server" id="Thu6_row1_td6">
                                    <textarea class="inputtextarea" runat="server" id="txtThu6Tiet1_GhiChu" cols="15" rows="2"></textarea>
                                </td>
                            </tr>
                            <tr runat="server" id="Thu6_row2">
                                <td class="a"></td>
                                <td style="text-align: center">2</td>
                                <td style="text-align: center">
                                    <input class="inputmon" runat="server" id="txtThu6Tiet2_Mon" />
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu6Tiet2_TCT" cols="10" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu6Tiet2_TenBaiGiang" cols="15" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu6Tiet2_GhiChu" cols="15" rows="2"></textarea>
                                </td>
                            </tr>
                            <tr runat="server" id="Thu6_row3">
                                <td class="a"></td>
                                <td style="text-align: center">3</td>
                                <td style="text-align: center">
                                    <input class="inputmon" runat="server" id="txtThu6Tiet3_Mon" />
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu6Tiet3_TCT" cols="10" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu6Tiet3_TenBaiGiang" cols="15" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu6Tiet3_GhiChu" cols="15" rows="2"></textarea>
                                </td>
                            </tr>
                            <tr runat="server" id="Thu6_row4">
                                <td></td>
                                <td style="text-align: center">4</td>
                                <td style="text-align: center">
                                    <input class="inputmon" runat="server" id="txtThu6Tiet4_Mon" />
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu6Tiet4_TCT" cols="10" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu6Tiet4_TenBaiGiang" cols="15" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu6Tiet4_GhiChu" cols="15" rows="2"></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center" class="a" runat="server" id="txtThu6_chieu">Chiều</td>
                                <td style="text-align: center" runat="server" id="Thu6_row5_td1">5</td>
                                <td style="text-align: center" runat="server" id="Thu6_row5_td2">
                                    <input class="inputmon" runat="server" id="txtThu6Tiet5_Mon" />
                                </td>
                                <td runat="server" id="Thu6_row5_td4">
                                    <textarea class="inputtextarea" runat="server" id="txtThu6Tiet5_TCT" cols="10" rows="2"></textarea>
                                </td>
                                <td runat="server" id="Thu6_row5_td5">
                                    <textarea class="inputtextarea" runat="server" id="txtThu6Tiet5_TenBaiGiang" cols="15" rows="2"></textarea>
                                </td>
                                <td runat="server" id="Thu6_row5_td6">
                                    <textarea class="inputtextarea" runat="server" id="txtThu6Tiet5_GhiChu" cols="15" rows="2"></textarea>
                                </td>
                            </tr>
                            <tr runat="server" id="Thu6_row6">
                                <td class="a"></td>
                                <td style="text-align: center">6</td>
                                <td style="text-align: center">
                                    <input class="inputmon" runat="server" id="txtThu6Tiet6_Mon" />
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu6Tiet6_TCT" cols="10" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu6Tiet6_TenBaiGiang" cols="15" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu6Tiet6_GhiChu" cols="15" rows="2"></textarea>
                                </td>
                            </tr>
                            <tr runat="server" id="Thu6_row7">
                                <td class="a"></td>
                                <td style="text-align: center">7</td>
                                <td style="text-align: center">
                                    <input class="inputmon" runat="server" id="txtThu6Tiet7_Mon" />
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu6Tiet7_TCT" cols="10" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu6Tiet7_TenBaiGiang" cols="15" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu6Tiet7_GhiChu" cols="15" rows="2"></textarea>
                                </td>
                            </tr>
                            <tr runat="server" id="Thu6_row8">
                                <td></td>
                                <td style="text-align: center">8</td>
                                <td style="text-align: center">
                                    <input class="inputmon" runat="server" id="txtThu6Tiet8_Mon" />
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu6Tiet8_TCT" cols="10" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu6Tiet8_TenBaiGiang" cols="15" rows="2"></textarea>
                                </td>
                                <td>
                                    <textarea class="inputtextarea" runat="server" id="txtThu6Tiet8_GhiChu" cols="15" rows="2"></textarea>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="7" style="text-align: center">
                                    <asp:Button ID="btnLuuThu6" runat="server" Text="Lưu và hoàn thành" CssClass="btn btn-primary" OnClick="btnLuuThu6_Click" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-12">
                        <%--<asp:Button ID="btnQuayLai1" runat="server" Text="Quay lại" CssClass="btn btn-primary" OnClick="btnQuayLai1_Click" />--%>
                        <%--<asp:Button ID="btnLuu" runat="server" Text="Lưu" CssClass="btn btn-primary" OnClientClick="return checkNULL()" OnClick="btnLuu_Click" />--%>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

