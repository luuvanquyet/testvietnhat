﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_WebSite_module_ThemLichBaoGiang_Formart : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //đổ danh sách khối học ra
            var dsKhoi = from k in db.tbKhois where k.khoi_id <= 5 select k;
            ddlKhoiHoc.DataSource = dsKhoi;
            ddlKhoiHoc.DataBind();
            //đổ ra ds tuần học
            var dsTuan = from t in db.tbHocTap_Tuans where t.tuan_hidden == false select t;
            ddlTuanHoc.DataSource = dsTuan;
            ddlTuanHoc.DataBind();
            loadData();
        }
        dvThu3.Visible = false;
        dvThu4.Visible = false;
        dvThu5.Visible = false;
        dvThu6.Visible = false;
    }
    private void loadData()
    {
        try
        {
            //lấy thông tin của tk đang nhập
            var getuser = (from u in db.admin_Users
                           where u.username_username == Request.Cookies["UserName"].Value
                           select u).FirstOrDefault();
            _id = Convert.ToInt32(RouteData.Values["id"]);
            //get tên khối vào combobox
            var getKhoi = (from lbg in db.tbLichBaoGiangs
                           join k in db.tbKhois on lbg.khoi_id equals k.khoi_id
                           join t in db.tbHocTap_Tuans on lbg.tuan_id equals t.tuan_id
                           where lbg.lichbaogiang_id == _id && lbg.username_id == getuser.username_id
                           select new
                           {
                               k.khoi_id,
                               k.khoi_name,
                               t.tuan_id,
                               t.tuan_name,
                               lbg.lichbaogiang_id,
                               lbg.lichbaogiang_tungay,
                               lbg.lichbaogiang_denngay,
                           }).First();
            ddlKhoiHoc.Text = getKhoi.khoi_name;
            ddlTuanHoc.Text = getKhoi.tuan_name;
            // get thông tin từ bảng tblichbaogiangtheotuan để lấy ngày của các thứ
            var getThu = from t in db.tbLichBaoGiangTheoTuans
                         where t.lichbaogiang_id == _id
                         select t;
            //get thông tin chi tiết của từng ngày học
            //lấy thông tin của thứ 2 từ bảng chi tiết
            var getChiTiet = from lbgct in db.tbLichBaoGiangChiTiets
                             where lbgct.lichbaogiangtheotuan_id == getThu.Take(1).Single().lichbaogiangtheotuan_id
                             select lbgct;
            //thông tin của các tiết trong buổi học thứ 2
            txtThu2Tiet1_Mon.Value = getChiTiet.Take(1).Single().lichbaogiangchitiet_monhoc;
            txtThu2Tiet2_Mon.Value = getChiTiet.Skip(1).Take(1).Single().lichbaogiangchitiet_monhoc;
            txtThu2Tiet3_Mon.Value = getChiTiet.Skip(2).Take(1).Single().lichbaogiangchitiet_monhoc;
            txtThu2Tiet4_Mon.Value = getChiTiet.Skip(3).Take(1).Single().lichbaogiangchitiet_monhoc;
            txtThu2Tiet5_Mon.Value = getChiTiet.Skip(4).Take(1).Single().lichbaogiangchitiet_monhoc;
            txtThu2Tiet6_Mon.Value = getChiTiet.Skip(5).Take(1).Single().lichbaogiangchitiet_monhoc;
            txtThu2Tiet7_Mon.Value = getChiTiet.Skip(6).Take(1).Single().lichbaogiangchitiet_monhoc;
            txtThu2Tiet8_Mon.Value = getChiTiet.Skip(7).Take(1).Single().lichbaogiangchitiet_monhoc;
            //lấy thông tin của thứ 3 từ bảng chi tiết
            var getChiTiet1 = from lbgct in db.tbLichBaoGiangChiTiets
                              where lbgct.lichbaogiangtheotuan_id == getThu.Skip(1).Take(1).Single().lichbaogiangtheotuan_id
                              select lbgct;
            //thông tin của các tiết trong buổi học thứ 3
            txtThu3Tiet1_Mon.Value = getChiTiet1.Take(1).Single().lichbaogiangchitiet_monhoc;
            txtThu3Tiet2_Mon.Value = getChiTiet1.Skip(1).Take(1).Single().lichbaogiangchitiet_monhoc;
            txtThu3Tiet3_Mon.Value = getChiTiet1.Skip(2).Take(1).Single().lichbaogiangchitiet_monhoc;
            txtThu3Tiet4_Mon.Value = getChiTiet1.Skip(3).Take(1).Single().lichbaogiangchitiet_monhoc;
            txtThu3Tiet5_Mon.Value = getChiTiet1.Skip(4).Take(1).Single().lichbaogiangchitiet_monhoc;
            txtThu3Tiet6_Mon.Value = getChiTiet1.Skip(5).Take(1).Single().lichbaogiangchitiet_monhoc;
            txtThu3Tiet7_Mon.Value = getChiTiet1.Skip(6).Take(1).Single().lichbaogiangchitiet_monhoc;
            txtThu3Tiet8_Mon.Value = getChiTiet1.Skip(7).Take(1).Single().lichbaogiangchitiet_monhoc;
            //lấy thông tin của thứ 4 từ bảng chi tiết
            var getChiTiet2 = from lbgct in db.tbLichBaoGiangChiTiets
                              where lbgct.lichbaogiangtheotuan_id == getThu.Skip(2).Take(1).Single().lichbaogiangtheotuan_id
                              select lbgct;
            //thông tin của các tiết trong buổi học thứ 4
            txtThu4Tiet1_Mon.Value = getChiTiet2.Take(1).Single().lichbaogiangchitiet_monhoc;
            txtThu4Tiet2_Mon.Value = getChiTiet2.Skip(1).Take(1).Single().lichbaogiangchitiet_monhoc;
            txtThu4Tiet3_Mon.Value = getChiTiet2.Skip(2).Take(1).Single().lichbaogiangchitiet_monhoc;
            txtThu4Tiet4_Mon.Value = getChiTiet2.Skip(3).Take(1).Single().lichbaogiangchitiet_monhoc;
            txtThu4Tiet5_Mon.Value = getChiTiet2.Skip(4).Take(1).Single().lichbaogiangchitiet_monhoc;
            txtThu4Tiet6_Mon.Value = getChiTiet2.Skip(5).Take(1).Single().lichbaogiangchitiet_monhoc;
            txtThu4Tiet7_Mon.Value = getChiTiet2.Skip(6).Take(1).Single().lichbaogiangchitiet_monhoc;
            txtThu4Tiet8_Mon.Value = getChiTiet2.Skip(7).Take(1).Single().lichbaogiangchitiet_monhoc;
            //lấy thông tin của thứ 5 từ bảng chi tiết
            var getChiTiet3 = from lbgct in db.tbLichBaoGiangChiTiets
                              where lbgct.lichbaogiangtheotuan_id == getThu.Skip(3).Take(1).Single().lichbaogiangtheotuan_id
                              select lbgct;
            //thông tin của các tiết trong buổi học thứ 5
            txtThu5Tiet1_Mon.Value = getChiTiet3.Take(1).Single().lichbaogiangchitiet_monhoc;
            txtThu5Tiet2_Mon.Value = getChiTiet3.Skip(1).Take(1).Single().lichbaogiangchitiet_monhoc;
            txtThu5Tiet3_Mon.Value = getChiTiet3.Skip(2).Take(1).Single().lichbaogiangchitiet_monhoc;
            txtThu5Tiet4_Mon.Value = getChiTiet3.Skip(3).Take(1).Single().lichbaogiangchitiet_monhoc;
            txtThu5Tiet5_Mon.Value = getChiTiet3.Skip(4).Take(1).Single().lichbaogiangchitiet_monhoc;
            txtThu5Tiet6_Mon.Value = getChiTiet3.Skip(5).Take(1).Single().lichbaogiangchitiet_monhoc;
            txtThu5Tiet7_Mon.Value = getChiTiet3.Skip(6).Take(1).Single().lichbaogiangchitiet_monhoc;
            txtThu5Tiet8_Mon.Value = getChiTiet3.Skip(7).Take(1).Single().lichbaogiangchitiet_monhoc;
            //lấy thông tin của thứ 6 từ bảng chi tiết
            var getChiTiet4 = from lbgct in db.tbLichBaoGiangChiTiets
                              where lbgct.lichbaogiangtheotuan_id == getThu.Skip(4).Take(1).Single().lichbaogiangtheotuan_id
                              select lbgct;
            //thông tin của các tiết trong buổi học thứ 6
            txtThu6Tiet1_Mon.Value = getChiTiet4.Take(1).Single().lichbaogiangchitiet_monhoc;
            txtThu6Tiet2_Mon.Value = getChiTiet4.Skip(1).Take(1).Single().lichbaogiangchitiet_monhoc;
            txtThu6Tiet3_Mon.Value = getChiTiet4.Skip(2).Take(1).Single().lichbaogiangchitiet_monhoc;
            txtThu6Tiet4_Mon.Value = getChiTiet4.Skip(3).Take(1).Single().lichbaogiangchitiet_monhoc;
            txtThu6Tiet5_Mon.Value = getChiTiet4.Skip(4).Take(1).Single().lichbaogiangchitiet_monhoc;
            txtThu6Tiet6_Mon.Value = getChiTiet4.Skip(5).Take(1).Single().lichbaogiangchitiet_monhoc;
            txtThu6Tiet7_Mon.Value = getChiTiet4.Skip(6).Take(1).Single().lichbaogiangchitiet_monhoc;
            txtThu6Tiet8_Mon.Value = getChiTiet4.Skip(7).Take(1).Single().lichbaogiangchitiet_monhoc;
            setFromChiTiet();
        }
        catch (Exception ex)
        {
            alert.alert_Error(Page, "Đã xảy ra lỗi, vui lòng liên hệ IT", "");
        }
    }

    protected void btnQuayLai_Click(object sender, EventArgs e)
    {
        Response.Redirect("/admin-lich-bao-giang-khoi-tieu-hoc");
    }

    //protected void btnQuayLai1_Click(object sender, EventArgs e)
    //{
    //    Response.Redirect("/admin-lich-bao-giang-khoi-tieu-hoc");
    //}

    protected void btnLuuThu2_Click(object sender, EventArgs e)
    {
        try
        {
            if (Request.Cookies["UserName"].Value == "")
            {
                alert.alert_Error(Page, "Lỗi tài khoản vui lòng login lại", "");
            }
            else
            {
                //lấy thông tin của tk đang nhập
                var getuser = (from u in db.admin_Users
                               where u.username_username == Request.Cookies["UserName"].Value
                               select u).FirstOrDefault();
                //nếu id=1 thì lưu mới, ngược lại thì cập nhật
                _id = Convert.ToInt32(RouteData.Values["id"]);
                //kiểm tra đã nhập đủ dữ liệu chưa
                if (ddlKhoiHoc.Text == "")
                {
                    alert.alert_Warning(Page, "Vui lòng chọn bộ môn!", "");
                }
                else
                {
                    //lấy thời gian học của tuần để tính ngày học luôn
                    var getDate = (from t in db.tbHocTap_Tuans
                                   where t.tuan_hidden == false && t.tuan_id == Convert.ToInt32(ddlTuanHoc.SelectedItem.Value)
                                   select t).SingleOrDefault();
                    //lưu vào bảng báo giảng
                    tbLichBaoGiang insert = new tbLichBaoGiang();
                    insert.tuan_id = Convert.ToInt32(ddlTuanHoc.SelectedItem.Value);
                    insert.lichbaogiang_tungay = getDate.tuan_tungay;
                    insert.lichbaogiang_denngay = getDate.tuan_denngay;
                    insert.khoi_id = Convert.ToInt32(ddlKhoiHoc.SelectedItem.Value);
                    //insert.lichbaogiang_mon = txtMon.Value;
                    insert.username_id = getuser.username_id;
                    insert.lichbaogiang_active = true;
                    insert.hidden = false;
                    insert.lichbaogiang_ngaytao = DateTime.Now;
                    db.tbLichBaoGiangs.InsertOnSubmit(insert);
                    db.SubmitChanges();
                    //lưu lại id của lịch báo giảng mới
                    Session["idBaoGiang"] = insert.lichbaogiang_id;
                    //lưu thông tin của thứ  hai vào lịch báo giảng theo tuần
                    tbLichBaoGiangTheoTuan insert_thu = new tbLichBaoGiangTheoTuan();
                    insert_thu.lichbaogiang_id = insert.lichbaogiang_id;
                    insert_thu.lichbaogiangtheotuan_thuhoc = "Thứ 2";
                    insert_thu.lichbaogiangtheotuan_ngayhoc = getDate.tuan_tungay;
                    insert_thu.username_id = getuser.username_id;
                    db.tbLichBaoGiangTheoTuans.InsertOnSubmit(insert_thu);
                    db.SubmitChanges();
                    // lưu vào bảng báo giảng chi tiết
                    for (int j = 1; j <= 8; j++)
                    {
                        tbLichBaoGiangChiTiet ins = new tbLichBaoGiangChiTiet();
                        ins.lichbaogiangtheotuan_id = insert_thu.lichbaogiangtheotuan_id;
                        ins.lichbaogiangchitiet_tiethoc = "Tiết " + j;
                        //lấy dữ liệu từ các thẻ input của thứ 2
                        if (j == 1)
                        {
                            //nếu j = 1 thì insert dữ liệu của tiết 1
                            ins.lichbaogiangchitiet_monhoc = txtThu2Tiet1_Mon.Value;
                            ins.lichbaogiangchitiet_tietchuongtrinh = txtThu2Tiet1_TCT.Value;
                            ins.lichbaogiangchitiet_tenbaigiang = txtThu2Tiet1_TenBaiGiang.Value;
                            ins.lichbaogiangchitiet_ghichu = txtThu2Tiet1_GhiChu.Value;
                        }
                        else if (j == 2)
                        {
                            //nếu j = 2 thì insert dữ liệu của tiết 2
                            ins.lichbaogiangchitiet_monhoc = txtThu2Tiet2_Mon.Value;
                            ins.lichbaogiangchitiet_tietchuongtrinh = txtThu2Tiet2_TCT.Value;
                            ins.lichbaogiangchitiet_tenbaigiang = txtThu2Tiet2_TenBaiGiang.Value;
                            ins.lichbaogiangchitiet_ghichu = txtThu2Tiet2_GhiChu.Value;
                        }
                        else if (j == 3)
                        {
                            //nếu j = 3 thì insert dữ liệu của tiết 3
                            ins.lichbaogiangchitiet_monhoc = txtThu2Tiet3_Mon.Value;
                            ins.lichbaogiangchitiet_tietchuongtrinh = txtThu2Tiet3_TCT.Value;
                            ins.lichbaogiangchitiet_tenbaigiang = txtThu2Tiet3_TenBaiGiang.Value;
                            ins.lichbaogiangchitiet_ghichu = txtThu2Tiet3_GhiChu.Value;
                        }
                        else if (j == 4)
                        {
                            //nếu j = 4 thì insert dữ liệu của tiết 4
                            ins.lichbaogiangchitiet_monhoc = txtThu2Tiet4_Mon.Value;
                            ins.lichbaogiangchitiet_tietchuongtrinh = txtThu2Tiet4_TCT.Value;
                            ins.lichbaogiangchitiet_tenbaigiang = txtThu2Tiet4_TenBaiGiang.Value;
                            ins.lichbaogiangchitiet_ghichu = txtThu2Tiet4_GhiChu.Value;
                        }
                        else if (j == 5)
                        {
                            //nếu j = 5 thì insert dữ liệu của tiết 5
                            ins.lichbaogiangchitiet_monhoc = txtThu2Tiet5_Mon.Value;
                            ins.lichbaogiangchitiet_tietchuongtrinh = txtThu2Tiet5_TCT.Value;
                            ins.lichbaogiangchitiet_tenbaigiang = txtThu2Tiet5_TenBaiGiang.Value;
                            ins.lichbaogiangchitiet_ghichu = txtThu2Tiet5_GhiChu.Value;
                        }
                        else if (j == 6)
                        {
                            //nếu j = 6 thì insert dữ liệu của tiết 6
                            ins.lichbaogiangchitiet_monhoc = txtThu2Tiet6_Mon.Value;
                            ins.lichbaogiangchitiet_tietchuongtrinh = txtThu2Tiet6_TCT.Value;
                            ins.lichbaogiangchitiet_tenbaigiang = txtThu2Tiet6_TenBaiGiang.Value;
                            ins.lichbaogiangchitiet_ghichu = txtThu2Tiet6_GhiChu.Value;
                        }
                        else if (j == 7)
                        {
                            //nếu j = 7 thì insert dữ liệu của tiết 7
                            ins.lichbaogiangchitiet_monhoc = txtThu2Tiet7_Mon.Value;
                            ins.lichbaogiangchitiet_tietchuongtrinh = txtThu2Tiet7_TCT.Value;
                            ins.lichbaogiangchitiet_tenbaigiang = txtThu2Tiet7_TenBaiGiang.Value;
                            ins.lichbaogiangchitiet_ghichu = txtThu2Tiet7_GhiChu.Value;
                        }
                        else
                        {
                            // j = 8 thì insert dữ liệu của tiết 8
                            ins.lichbaogiangchitiet_monhoc = txtThu2Tiet8_Mon.Value;
                            ins.lichbaogiangchitiet_tietchuongtrinh = txtThu2Tiet8_TCT.Value;
                            ins.lichbaogiangchitiet_tenbaigiang = txtThu2Tiet8_TenBaiGiang.Value;
                            ins.lichbaogiangchitiet_ghichu = txtThu2Tiet8_GhiChu.Value;
                        }
                        if (j <= 4)
                            ins.lichbaogiangchitiet_buoihoc = "Sáng ";
                        else
                            ins.lichbaogiangchitiet_buoihoc = "Chiều ";
                        ins.username_id = getuser.username_id;
                        db.tbLichBaoGiangChiTiets.InsertOnSubmit(ins);
                        db.SubmitChanges();
                    }
                    alert.alert_Success(Page, "Lưu thành công!", "");
                    btnQuayLai.Visible = false;
                    dvThu2.Visible = false;
                    dvThu3.Visible = true;
                }
            }
        }
        catch (Exception ex)
        {
            alert.alert_Error(Page, "Đã xảy ra lỗi, vui lòng liên hệ IT", "");
        }
    }
    protected void btnLuuThu3_Click(object sender, EventArgs e)
    {
        try
        {
            if (Request.Cookies["UserName"].Value == "")
            {
                alert.alert_Error(Page, "Lỗi tài khoản vui lòng login lại", "");
            }
            else
            {
                //lấy thông tin của tk đang nhập
                var getuser = (from u in db.admin_Users
                               where u.username_username == Request.Cookies["UserName"].Value
                               select u).FirstOrDefault();
                //nếu id=1 thì lưu mới, ngược lại thì cập nhật
                _id = Convert.ToInt32(RouteData.Values["id"]);
                //kiểm tra đã nhập đủ dữ liệu chưa
                if (ddlKhoiHoc.Text == "")
                {
                    alert.alert_Warning(Page, "Vui lòng chọn bộ môn!", "");
                }
                else
                {
                    //lấy thời gian học của tuần để tính ngày học luôn
                    var getDate = (from t in db.tbHocTap_Tuans
                                   where t.tuan_hidden == false && t.tuan_id == Convert.ToInt32(ddlTuanHoc.SelectedItem.Value)
                                   select t).SingleOrDefault();
                    //lưu thông tin của thứ  hai vào lịch báo giảng theo tuần
                    tbLichBaoGiangTheoTuan insert_thu = new tbLichBaoGiangTheoTuan();
                    insert_thu.lichbaogiang_id = Convert.ToInt32(Session["idBaoGiang"].ToString());
                    insert_thu.lichbaogiangtheotuan_thuhoc = "Thứ 3";
                    insert_thu.lichbaogiangtheotuan_ngayhoc = Convert.ToDateTime(getDate.tuan_tungay).AddDays(1);
                    insert_thu.username_id = getuser.username_id;
                    db.tbLichBaoGiangTheoTuans.InsertOnSubmit(insert_thu);
                    db.SubmitChanges();
                    // lưu vào bảng báo giảng chi tiết
                    for (int j = 1; j <= 8; j++)
                    {
                        tbLichBaoGiangChiTiet ins = new tbLichBaoGiangChiTiet();
                        ins.lichbaogiangtheotuan_id = insert_thu.lichbaogiangtheotuan_id;
                        ins.lichbaogiangchitiet_tiethoc = "Tiết " + j;
                        //lấy dữ liệu từ các thẻ input của thứ 3
                        if (j == 1)
                        {
                            //nếu j = 1 thì insert dữ liệu của tiết 1
                            ins.lichbaogiangchitiet_monhoc = txtThu3Tiet1_Mon.Value;
                            ins.lichbaogiangchitiet_tietchuongtrinh = txtThu3Tiet1_TCT.Value;
                            ins.lichbaogiangchitiet_tenbaigiang = txtThu3Tiet1_TenBaiGiang.Value;
                            ins.lichbaogiangchitiet_ghichu = txtThu3Tiet1_GhiChu.Value;
                        }
                        else if (j == 2)
                        {
                            //nếu j = 2 thì insert dữ liệu của tiết 2
                            ins.lichbaogiangchitiet_monhoc = txtThu2Tiet2_Mon.Value;
                            ins.lichbaogiangchitiet_tietchuongtrinh = txtThu2Tiet2_TCT.Value;
                            ins.lichbaogiangchitiet_tenbaigiang = txtThu2Tiet2_TenBaiGiang.Value;
                            ins.lichbaogiangchitiet_ghichu = txtThu2Tiet2_GhiChu.Value;
                        }
                        else if (j == 3)
                        {
                            //nếu j = 3 thì insert dữ liệu của tiết 3
                            ins.lichbaogiangchitiet_monhoc = txtThu3Tiet3_Mon.Value;
                            ins.lichbaogiangchitiet_tietchuongtrinh = txtThu3Tiet3_TCT.Value;
                            ins.lichbaogiangchitiet_tenbaigiang = txtThu3Tiet3_TenBaiGiang.Value;
                            ins.lichbaogiangchitiet_ghichu = txtThu3Tiet3_GhiChu.Value;
                        }
                        else if (j == 4)
                        {
                            //nếu j = 4 thì insert dữ liệu của tiết 4
                            ins.lichbaogiangchitiet_monhoc = txtThu3Tiet4_Mon.Value;
                            ins.lichbaogiangchitiet_tietchuongtrinh = txtThu3Tiet4_TCT.Value;
                            ins.lichbaogiangchitiet_tenbaigiang = txtThu3Tiet4_TenBaiGiang.Value;
                            ins.lichbaogiangchitiet_ghichu = txtThu3Tiet4_GhiChu.Value;
                        }
                        else if (j == 5)
                        {
                            //nếu j = 5 thì insert dữ liệu của tiết 5
                            ins.lichbaogiangchitiet_monhoc = txtThu3Tiet5_Mon.Value;
                            ins.lichbaogiangchitiet_tietchuongtrinh = txtThu3Tiet5_TCT.Value;
                            ins.lichbaogiangchitiet_tenbaigiang = txtThu3Tiet5_TenBaiGiang.Value;
                            ins.lichbaogiangchitiet_ghichu = txtThu3Tiet5_GhiChu.Value;
                        }
                        else if (j == 6)
                        {
                            //nếu j = 6 thì insert dữ liệu của tiết 6
                            ins.lichbaogiangchitiet_monhoc = txtThu3Tiet6_Mon.Value;
                            ins.lichbaogiangchitiet_tietchuongtrinh = txtThu3Tiet6_TCT.Value;
                            ins.lichbaogiangchitiet_tenbaigiang = txtThu3Tiet6_TenBaiGiang.Value;
                            ins.lichbaogiangchitiet_ghichu = txtThu3Tiet6_GhiChu.Value;
                        }
                        else if (j == 7)
                        {
                            //nếu j = 7 thì insert dữ liệu của tiết 7
                            ins.lichbaogiangchitiet_monhoc = txtThu3Tiet7_Mon.Value;
                            ins.lichbaogiangchitiet_tietchuongtrinh = txtThu3Tiet7_TCT.Value;
                            ins.lichbaogiangchitiet_tenbaigiang = txtThu3Tiet7_TenBaiGiang.Value;
                            ins.lichbaogiangchitiet_ghichu = txtThu3Tiet7_GhiChu.Value;
                        }
                        else
                        {
                            // j = 8 thì insert dữ liệu của tiết 8
                            ins.lichbaogiangchitiet_monhoc = txtThu3Tiet8_Mon.Value;
                            ins.lichbaogiangchitiet_tietchuongtrinh = txtThu3Tiet8_TCT.Value;
                            ins.lichbaogiangchitiet_tenbaigiang = txtThu3Tiet8_TenBaiGiang.Value;
                            ins.lichbaogiangchitiet_ghichu = txtThu3Tiet8_GhiChu.Value;
                        }
                        if (j <= 4)
                            ins.lichbaogiangchitiet_buoihoc = "Sáng ";
                        else
                            ins.lichbaogiangchitiet_buoihoc = "Chiều ";
                        ins.username_id = getuser.username_id;
                        db.tbLichBaoGiangChiTiets.InsertOnSubmit(ins);
                        db.SubmitChanges();
                    }
                    alert.alert_Success(Page, "Lưu thành công!", "");
                    dvThu3.Visible = false;
                    dvThu4.Visible = true;
                }
            }
        }
        catch (Exception ex)
        {
            alert.alert_Error(Page, "Đã xảy ra lỗi, vui lòng liên hệ IT", "");
        }
    }

    protected void btnLuuThu4_Click(object sender, EventArgs e)
    {
        try
        {
            if (Request.Cookies["UserName"].Value == "")
            {
                alert.alert_Error(Page, "Lỗi tài khoản vui lòng login lại", "");
            }
            else
            {
                //lấy thông tin của tk đang nhập
                var getuser = (from u in db.admin_Users
                               where u.username_username == Request.Cookies["UserName"].Value
                               select u).FirstOrDefault();
                //nếu id=1 thì lưu mới, ngược lại thì cập nhật
                _id = Convert.ToInt32(RouteData.Values["id"]);
                //kiểm tra đã nhập đủ dữ liệu chưa
                if (ddlKhoiHoc.Text == "")
                {
                    alert.alert_Warning(Page, "Vui lòng chọn bộ môn!", "");
                }
                else
                {
                    //lấy thời gian học của tuần để tính ngày học luôn
                    var getDate = (from t in db.tbHocTap_Tuans
                                   where t.tuan_hidden == false && t.tuan_id == Convert.ToInt32(ddlTuanHoc.SelectedItem.Value)
                                   select t).SingleOrDefault();
                    //lưu thông tin của thứ  hai vào lịch báo giảng theo tuần
                    tbLichBaoGiangTheoTuan insert_thu = new tbLichBaoGiangTheoTuan();
                    insert_thu.lichbaogiang_id = Convert.ToInt32(Session["idBaoGiang"].ToString());
                    insert_thu.lichbaogiangtheotuan_thuhoc = "Thứ 4";
                    insert_thu.lichbaogiangtheotuan_ngayhoc = Convert.ToDateTime(getDate.tuan_tungay).AddDays(2);
                    insert_thu.username_id = getuser.username_id;
                    db.tbLichBaoGiangTheoTuans.InsertOnSubmit(insert_thu);
                    db.SubmitChanges();
                    // lưu vào bảng báo giảng chi tiết
                    for (int j = 1; j <= 8; j++)
                    {
                        tbLichBaoGiangChiTiet ins = new tbLichBaoGiangChiTiet();
                        ins.lichbaogiangtheotuan_id = insert_thu.lichbaogiangtheotuan_id;
                        ins.lichbaogiangchitiet_tiethoc = "Tiết " + j;
                        //lấy dữ liệu từ các thẻ input của thứ 2
                        if (j == 1)
                        {
                            //nếu j = 1 thì insert dữ liệu của tiết 1
                            ins.lichbaogiangchitiet_monhoc = txtThu4Tiet1_Mon.Value;
                            ins.lichbaogiangchitiet_tietchuongtrinh = txtThu4Tiet1_TCT.Value;
                            ins.lichbaogiangchitiet_tenbaigiang = txtThu4Tiet1_TenBaiGiang.Value;
                            ins.lichbaogiangchitiet_ghichu = txtThu4Tiet1_GhiChu.Value;
                        }
                        else if (j == 2)
                        {
                            //nếu j = 2 thì insert dữ liệu của tiết 2
                            ins.lichbaogiangchitiet_monhoc = txtThu4Tiet2_Mon.Value;
                            ins.lichbaogiangchitiet_tietchuongtrinh = txtThu4Tiet2_TCT.Value;
                            ins.lichbaogiangchitiet_tenbaigiang = txtThu4Tiet2_TenBaiGiang.Value;
                            ins.lichbaogiangchitiet_ghichu = txtThu4Tiet2_GhiChu.Value;
                        }
                        else if (j == 3)
                        {
                            //nếu j = 3 thì insert dữ liệu của tiết 3
                            ins.lichbaogiangchitiet_monhoc = txtThu4Tiet3_Mon.Value;
                            ins.lichbaogiangchitiet_tietchuongtrinh = txtThu4Tiet3_TCT.Value;
                            ins.lichbaogiangchitiet_tenbaigiang = txtThu4Tiet3_TenBaiGiang.Value;
                            ins.lichbaogiangchitiet_ghichu = txtThu4Tiet3_GhiChu.Value;
                        }
                        else if (j == 4)
                        {
                            //nếu j = 4 thì insert dữ liệu của tiết 4
                            ins.lichbaogiangchitiet_monhoc = txtThu4Tiet4_Mon.Value;
                            ins.lichbaogiangchitiet_tietchuongtrinh = txtThu4Tiet4_TCT.Value;
                            ins.lichbaogiangchitiet_tenbaigiang = txtThu4Tiet4_TenBaiGiang.Value;
                            ins.lichbaogiangchitiet_ghichu = txtThu4Tiet4_GhiChu.Value;
                        }
                        else if (j == 5)
                        {
                            //nếu j = 5 thì insert dữ liệu của tiết 5
                            ins.lichbaogiangchitiet_monhoc = txtThu4Tiet5_Mon.Value;
                            ins.lichbaogiangchitiet_tietchuongtrinh = txtThu4Tiet5_TCT.Value;
                            ins.lichbaogiangchitiet_tenbaigiang = txtThu4Tiet5_TenBaiGiang.Value;
                            ins.lichbaogiangchitiet_ghichu = txtThu4Tiet5_GhiChu.Value;
                        }
                        else if (j == 6)
                        {
                            //nếu j = 6 thì insert dữ liệu của tiết 6
                            ins.lichbaogiangchitiet_monhoc = txtThu4Tiet6_Mon.Value;
                            ins.lichbaogiangchitiet_tietchuongtrinh = txtThu4Tiet6_TCT.Value;
                            ins.lichbaogiangchitiet_tenbaigiang = txtThu4Tiet6_TenBaiGiang.Value;
                            ins.lichbaogiangchitiet_ghichu = txtThu4Tiet6_GhiChu.Value;
                        }
                        else if (j == 7)
                        {
                            //nếu j = 7 thì insert dữ liệu của tiết 7
                            ins.lichbaogiangchitiet_monhoc = txtThu4Tiet7_Mon.Value;
                            ins.lichbaogiangchitiet_tietchuongtrinh = txtThu4Tiet7_TCT.Value;
                            ins.lichbaogiangchitiet_tenbaigiang = txtThu4Tiet7_TenBaiGiang.Value;
                            ins.lichbaogiangchitiet_ghichu = txtThu4Tiet7_GhiChu.Value;
                        }
                        else
                        {
                            // j = 8 thì insert dữ liệu của tiết 8
                            ins.lichbaogiangchitiet_monhoc = txtThu4Tiet8_Mon.Value;
                            ins.lichbaogiangchitiet_tietchuongtrinh = txtThu4Tiet8_TCT.Value;
                            ins.lichbaogiangchitiet_tenbaigiang = txtThu4Tiet8_TenBaiGiang.Value;
                            ins.lichbaogiangchitiet_ghichu = txtThu4Tiet8_GhiChu.Value;
                        }
                        if (j <= 4)
                            ins.lichbaogiangchitiet_buoihoc = "Sáng ";
                        else
                            ins.lichbaogiangchitiet_buoihoc = "Chiều ";
                        ins.username_id = getuser.username_id;
                        db.tbLichBaoGiangChiTiets.InsertOnSubmit(ins);
                        db.SubmitChanges();
                    }
                    alert.alert_Success(Page, "Lưu thành công!", "");
                    dvThu4.Visible = false;
                    dvThu5.Visible = true;
                }
            }
        }
        catch (Exception ex)
        {
            alert.alert_Error(Page, "Đã xảy ra lỗi, vui lòng liên hệ IT", "");
        }
    }

    protected void btnLuuThu5_Click(object sender, EventArgs e)
    {
        try
        {
            if (Request.Cookies["UserName"].Value == "")
            {
                alert.alert_Error(Page, "Lỗi tài khoản vui lòng login lại", "");
            }
            else
            {
                //lấy thông tin của tk đang nhập
                var getuser = (from u in db.admin_Users
                               where u.username_username == Request.Cookies["UserName"].Value
                               select u).FirstOrDefault();
                //nếu id=1 thì lưu mới, ngược lại thì cập nhật
                _id = Convert.ToInt32(RouteData.Values["id"]);
                //kiểm tra đã nhập đủ dữ liệu chưa
                if (ddlKhoiHoc.Text == "")
                {
                    alert.alert_Warning(Page, "Vui lòng chọn bộ môn!", "");
                }
                else
                {
                    //lấy thời gian học của tuần để tính ngày học luôn
                    var getDate = (from t in db.tbHocTap_Tuans
                                   where t.tuan_hidden == false && t.tuan_id == Convert.ToInt32(ddlTuanHoc.SelectedItem.Value)
                                   select t).SingleOrDefault();
                    //lưu thông tin của thứ  hai vào lịch báo giảng theo tuần
                    tbLichBaoGiangTheoTuan insert_thu = new tbLichBaoGiangTheoTuan();
                    insert_thu.lichbaogiang_id = Convert.ToInt32(Session["idBaoGiang"].ToString());
                    insert_thu.lichbaogiangtheotuan_thuhoc = "Thứ 5";
                    insert_thu.lichbaogiangtheotuan_ngayhoc = Convert.ToDateTime(getDate.tuan_tungay).AddDays(3);
                    insert_thu.username_id = getuser.username_id;
                    db.tbLichBaoGiangTheoTuans.InsertOnSubmit(insert_thu);
                    db.SubmitChanges();
                    // lưu vào bảng báo giảng chi tiết
                    for (int j = 1; j <= 8; j++)
                    {
                        tbLichBaoGiangChiTiet ins = new tbLichBaoGiangChiTiet();
                        ins.lichbaogiangtheotuan_id = insert_thu.lichbaogiangtheotuan_id;
                        ins.lichbaogiangchitiet_tiethoc = "Tiết " + j;
                        //lấy dữ liệu từ các thẻ input của thứ 2
                        if (j == 1)
                        {
                            //nếu j = 1 thì insert dữ liệu của tiết 1
                            ins.lichbaogiangchitiet_monhoc = txtThu5Tiet1_Mon.Value;
                            ins.lichbaogiangchitiet_tietchuongtrinh = txtThu5Tiet1_TCT.Value;
                            ins.lichbaogiangchitiet_tenbaigiang = txtThu5Tiet1_TenBaiGiang.Value;
                            ins.lichbaogiangchitiet_ghichu = txtThu5Tiet1_GhiChu.Value;
                        }
                        else if (j == 2)
                        {
                            //nếu j = 2 thì insert dữ liệu của tiết 2
                            ins.lichbaogiangchitiet_monhoc = txtThu5Tiet2_Mon.Value;
                            ins.lichbaogiangchitiet_tietchuongtrinh = txtThu5Tiet2_TCT.Value;
                            ins.lichbaogiangchitiet_tenbaigiang = txtThu5Tiet2_TenBaiGiang.Value;
                            ins.lichbaogiangchitiet_ghichu = txtThu5Tiet2_GhiChu.Value;
                        }
                        else if (j == 3)
                        {
                            //nếu j = 3 thì insert dữ liệu của tiết 3
                            ins.lichbaogiangchitiet_monhoc = txtThu5Tiet3_Mon.Value;
                            ins.lichbaogiangchitiet_tietchuongtrinh = txtThu5Tiet3_TCT.Value;
                            ins.lichbaogiangchitiet_tenbaigiang = txtThu5Tiet3_TenBaiGiang.Value;
                            ins.lichbaogiangchitiet_ghichu = txtThu5Tiet3_GhiChu.Value;
                        }
                        else if (j == 4)
                        {
                            //nếu j = 4 thì insert dữ liệu của tiết 4
                            ins.lichbaogiangchitiet_monhoc = txtThu5Tiet4_Mon.Value;
                            ins.lichbaogiangchitiet_tietchuongtrinh = txtThu5Tiet4_TCT.Value;
                            ins.lichbaogiangchitiet_tenbaigiang = txtThu5Tiet4_TenBaiGiang.Value;
                            ins.lichbaogiangchitiet_ghichu = txtThu5Tiet4_GhiChu.Value;
                        }
                        else if (j == 5)
                        {
                            //nếu j = 5 thì insert dữ liệu của tiết 5
                            ins.lichbaogiangchitiet_monhoc = txtThu5Tiet5_Mon.Value;
                            ins.lichbaogiangchitiet_tietchuongtrinh = txtThu5Tiet5_TCT.Value;
                            ins.lichbaogiangchitiet_tenbaigiang = txtThu5Tiet5_TenBaiGiang.Value;
                            ins.lichbaogiangchitiet_ghichu = txtThu5Tiet5_GhiChu.Value;
                        }
                        else if (j == 6)
                        {
                            //nếu j = 6 thì insert dữ liệu của tiết 6
                            ins.lichbaogiangchitiet_monhoc = txtThu5Tiet6_Mon.Value;
                            ins.lichbaogiangchitiet_tietchuongtrinh = txtThu5Tiet6_TCT.Value;
                            ins.lichbaogiangchitiet_tenbaigiang = txtThu5Tiet6_TenBaiGiang.Value;
                            ins.lichbaogiangchitiet_ghichu = txtThu5Tiet6_GhiChu.Value;
                        }
                        else if (j == 7)
                        {
                            //nếu j = 7 thì insert dữ liệu của tiết 7
                            ins.lichbaogiangchitiet_monhoc = txtThu5Tiet7_Mon.Value;
                            ins.lichbaogiangchitiet_tietchuongtrinh = txtThu5Tiet7_TCT.Value;
                            ins.lichbaogiangchitiet_tenbaigiang = txtThu5Tiet7_TenBaiGiang.Value;
                            ins.lichbaogiangchitiet_ghichu = txtThu5Tiet7_GhiChu.Value;
                        }
                        else
                        {
                            // j = 8 thì insert dữ liệu của tiết 8
                            ins.lichbaogiangchitiet_monhoc = txtThu5Tiet8_Mon.Value;
                            ins.lichbaogiangchitiet_tietchuongtrinh = txtThu5Tiet8_TCT.Value;
                            ins.lichbaogiangchitiet_tenbaigiang = txtThu5Tiet8_TenBaiGiang.Value;
                            ins.lichbaogiangchitiet_ghichu = txtThu5Tiet8_GhiChu.Value;
                        }
                        if (j <= 4)
                            ins.lichbaogiangchitiet_buoihoc = "Sáng ";
                        else
                            ins.lichbaogiangchitiet_buoihoc = "Chiều ";
                        ins.username_id = getuser.username_id;
                        db.tbLichBaoGiangChiTiets.InsertOnSubmit(ins);
                        db.SubmitChanges();
                    }
                    alert.alert_Success(Page, "Lưu thành công!", "");
                    dvThu5.Visible = false;
                    dvThu6.Visible = true;
                }
            }
        }
        catch (Exception ex)
        {
            alert.alert_Error(Page, "Đã xảy ra lỗi, vui lòng liên hệ IT", "");
        }
    }

    protected void btnLuuThu6_Click(object sender, EventArgs e)
    {
        try
        {
            if (Request.Cookies["UserName"].Value == "")
            {
                alert.alert_Error(Page, "Lỗi tài khoản vui lòng login lại", "");
            }
            else
            {
                //lấy thông tin của tk đang nhập
                var getuser = (from u in db.admin_Users
                               where u.username_username == Request.Cookies["UserName"].Value
                               select u).FirstOrDefault();
                //nếu id=1 thì lưu mới, ngược lại thì cập nhật
                _id = Convert.ToInt32(RouteData.Values["id"]);
                //kiểm tra đã nhập đủ dữ liệu chưa
                if (ddlKhoiHoc.Text == "")
                {
                    alert.alert_Warning(Page, "Vui lòng chọn bộ môn!", "");
                }
                else
                {
                    //lấy thời gian học của tuần để tính ngày học luôn
                    var getDate = (from t in db.tbHocTap_Tuans
                                   where t.tuan_hidden == false && t.tuan_id == Convert.ToInt32(ddlTuanHoc.SelectedItem.Value)
                                   select t).SingleOrDefault();
                    //lưu thông tin của thứ  hai vào lịch báo giảng theo tuần
                    tbLichBaoGiangTheoTuan insert_thu = new tbLichBaoGiangTheoTuan();
                    insert_thu.lichbaogiang_id = Convert.ToInt32(Session["idBaoGiang"].ToString());
                    insert_thu.lichbaogiangtheotuan_thuhoc = "Thứ 6";
                    insert_thu.lichbaogiangtheotuan_ngayhoc = Convert.ToDateTime(getDate.tuan_tungay).AddDays(4);
                    insert_thu.username_id = getuser.username_id;
                    db.tbLichBaoGiangTheoTuans.InsertOnSubmit(insert_thu);
                    db.SubmitChanges();
                    // lưu vào bảng báo giảng chi tiết
                    for (int j = 1; j <= 8; j++)
                    {
                        tbLichBaoGiangChiTiet ins = new tbLichBaoGiangChiTiet();
                        ins.lichbaogiangtheotuan_id = insert_thu.lichbaogiangtheotuan_id;
                        ins.lichbaogiangchitiet_tiethoc = "Tiết " + j;
                        //lấy dữ liệu từ các thẻ input của thứ 6
                        if (j == 1)
                        {
                            //nếu j = 1 thì insert dữ liệu của tiết 1
                            ins.lichbaogiangchitiet_monhoc = txtThu6Tiet1_Mon.Value;
                            ins.lichbaogiangchitiet_tietchuongtrinh = txtThu6Tiet1_TCT.Value;
                            ins.lichbaogiangchitiet_tenbaigiang = txtThu6Tiet1_TenBaiGiang.Value;
                            ins.lichbaogiangchitiet_ghichu = txtThu6Tiet1_GhiChu.Value;
                        }
                        else if (j == 2)
                        {
                            //nếu j = 2 thì insert dữ liệu của tiết 2
                            ins.lichbaogiangchitiet_monhoc = txtThu6Tiet2_Mon.Value;
                            ins.lichbaogiangchitiet_tietchuongtrinh = txtThu6Tiet2_TCT.Value;
                            ins.lichbaogiangchitiet_tenbaigiang = txtThu6Tiet2_TenBaiGiang.Value;
                            ins.lichbaogiangchitiet_ghichu = txtThu6Tiet2_GhiChu.Value;
                        }
                        else if (j == 3)
                        {
                            //nếu j = 3 thì insert dữ liệu của tiết 3
                            ins.lichbaogiangchitiet_monhoc = txtThu6Tiet3_Mon.Value;
                            ins.lichbaogiangchitiet_tietchuongtrinh = txtThu6Tiet3_TCT.Value;
                            ins.lichbaogiangchitiet_tenbaigiang = txtThu6Tiet3_TenBaiGiang.Value;
                            ins.lichbaogiangchitiet_ghichu = txtThu6Tiet3_GhiChu.Value;
                        }
                        else if (j == 4)
                        {
                            //nếu j = 4 thì insert dữ liệu của tiết 4
                            ins.lichbaogiangchitiet_monhoc = txtThu6Tiet4_Mon.Value;
                            ins.lichbaogiangchitiet_tietchuongtrinh = txtThu6Tiet4_TCT.Value;
                            ins.lichbaogiangchitiet_tenbaigiang = txtThu6Tiet4_TenBaiGiang.Value;
                            ins.lichbaogiangchitiet_ghichu = txtThu6Tiet4_GhiChu.Value;
                        }
                        else if (j == 5)
                        {
                            //nếu j = 5 thì insert dữ liệu của tiết 5
                            ins.lichbaogiangchitiet_monhoc = txtThu6Tiet5_Mon.Value;
                            ins.lichbaogiangchitiet_tietchuongtrinh = txtThu6Tiet5_TCT.Value;
                            ins.lichbaogiangchitiet_tenbaigiang = txtThu6Tiet5_TenBaiGiang.Value;
                            ins.lichbaogiangchitiet_ghichu = txtThu6Tiet5_GhiChu.Value;
                        }
                        else if (j == 6)
                        {
                            //nếu j = 6 thì insert dữ liệu của tiết 6
                            ins.lichbaogiangchitiet_monhoc = txtThu6Tiet6_Mon.Value;
                            ins.lichbaogiangchitiet_tietchuongtrinh = txtThu6Tiet6_TCT.Value;
                            ins.lichbaogiangchitiet_tenbaigiang = txtThu6Tiet6_TenBaiGiang.Value;
                            ins.lichbaogiangchitiet_ghichu = txtThu6Tiet6_GhiChu.Value;
                        }
                        else if (j == 7)
                        {
                            //nếu j = 7 thì insert dữ liệu của tiết 7
                            ins.lichbaogiangchitiet_monhoc = txtThu6Tiet7_Mon.Value;
                            ins.lichbaogiangchitiet_tietchuongtrinh = txtThu6Tiet7_TCT.Value;
                            ins.lichbaogiangchitiet_tenbaigiang = txtThu6Tiet7_TenBaiGiang.Value;
                            ins.lichbaogiangchitiet_ghichu = txtThu6Tiet7_GhiChu.Value;
                        }
                        else
                        {
                            // j = 8 thì insert dữ liệu của tiết 8
                            ins.lichbaogiangchitiet_monhoc = txtThu6Tiet8_Mon.Value;
                            ins.lichbaogiangchitiet_tietchuongtrinh = txtThu6Tiet8_TCT.Value;
                            ins.lichbaogiangchitiet_tenbaigiang = txtThu6Tiet8_TenBaiGiang.Value;
                            ins.lichbaogiangchitiet_ghichu = txtThu6Tiet8_GhiChu.Value;
                        }
                        if (j <= 4)
                            ins.lichbaogiangchitiet_buoihoc = "Sáng ";
                        else
                            ins.lichbaogiangchitiet_buoihoc = "Chiều ";
                        ins.username_id = getuser.username_id;
                        db.tbLichBaoGiangChiTiets.InsertOnSubmit(ins);
                        db.SubmitChanges();
                    }
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "AlertBox", "swal('Lưu thành công!', '','success').then(function(){window.location = '/admin-lich-bao-giang-khoi-tieu-hoc';})", true);
                }
            }
        }
        catch (Exception ex)
        {
            alert.alert_Error(Page, "Đã xảy ra lỗi, vui lòng liên hệ IT", "");
        }
    }
    private void setFromChiTiet()
    {
        //ẩn những dòng trong thứ 2 mà giáo viên không có tiết dạy
        if (txtThu2Tiet1_Mon.Value == "")
        {
            Thu2_row1_td1.Visible = false;
            Thu2_row1_td2.Visible = false;
            Thu2_row1_td4.Visible = false;
            Thu2_row1_td5.Visible = false;
            Thu2_row1_td6.Visible = false;
        }
        else
        {
            Thu2_row1_td1.Visible = true;
            Thu2_row1_td2.Visible = true;
            Thu2_row1_td4.Visible = true;
            Thu2_row1_td5.Visible = true;
            Thu2_row1_td6.Visible = true;
        }
        if (txtThu2Tiet2_Mon.Value == "")
            Thu2_row2.Visible = false;
        else
            Thu2_row2.Visible = true;
        if (txtThu2Tiet3_Mon.Value == "")
            Thu2_row3.Visible = false;
        else
            Thu2_row3.Visible = true;
        if (txtThu2Tiet4_Mon.Value == "")
            Thu2_row4.Visible = false;
        else
            Thu2_row4.Visible = true;
        if (txtThu2Tiet5_Mon.Value == "")
        {
            Thu2_row5_td1.Visible = false;
            Thu2_row5_td2.Visible = false;
            Thu2_row5_td4.Visible = false;
            Thu2_row5_td5.Visible = false;
            Thu2_row5_td6.Visible = false;
        }
        else
        {
            Thu2_row5_td1.Visible = true;
            Thu2_row5_td2.Visible = true;
            Thu2_row5_td4.Visible = true;
            Thu2_row5_td5.Visible = true;
            Thu2_row5_td6.Visible = true;
        }
        if (txtThu2Tiet6_Mon.Value == "")
            Thu2_row6.Visible = false;
        else
            Thu2_row6.Visible = true;
        if (txtThu2Tiet7_Mon.Value == "")
            Thu2_row7.Visible = false;
        else
            Thu2_row7.Visible = true;
        if (txtThu2Tiet8_Mon.Value == "")
            Thu2_row8.Visible = false;
        else
            Thu2_row8.Visible = true;
        //ẩn những dòng trong thứ 3 mà giáo viên không có tiết dạy
        if (txtThu3Tiet1_Mon.Value == "")
        {
            Thu3_row1_td1.Visible = false;
            Thu3_row1_td2.Visible = false;
            Thu3_row1_td4.Visible = false;
            Thu3_row1_td5.Visible = false;
            Thu3_row1_td6.Visible = false;
        }
        else
        {
            Thu3_row1_td1.Visible = true;
            Thu3_row1_td2.Visible = true;
            Thu3_row1_td4.Visible = true;
            Thu3_row1_td5.Visible = true;
            Thu3_row1_td6.Visible = true;
        }
        if (txtThu3Tiet2_Mon.Value == "")
            Thu3_row2.Visible = false;
        else
            Thu3_row2.Visible = true;
        if (txtThu3Tiet3_Mon.Value == "")
            Thu3_row3.Visible = false;
        else
            Thu3_row3.Visible = true;
        if (txtThu3Tiet4_Mon.Value == "")
            Thu3_row4.Visible = false;
        else
            Thu3_row4.Visible = true;
        if (txtThu3Tiet5_Mon.Value == "")
        {
            Thu3_row5_td1.Visible = false;
            Thu3_row5_td2.Visible = false;
            Thu3_row5_td4.Visible = false;
            Thu3_row5_td5.Visible = false;
            Thu3_row5_td6.Visible = false;
        }
        else
        {
            Thu3_row5_td1.Visible = true;
            Thu3_row5_td2.Visible = true;
            Thu3_row5_td4.Visible = true;
            Thu3_row5_td5.Visible = true;
            Thu3_row5_td6.Visible = true;
        }
        if (txtThu3Tiet6_Mon.Value == "")
            Thu3_row6.Visible = false;
        else
            Thu3_row6.Visible = true;
        if (txtThu3Tiet7_Mon.Value == "")
            Thu3_row7.Visible = false;
        else
            Thu3_row7.Visible = true;
        if (txtThu3Tiet8_Mon.Value == "")
            Thu3_row8.Visible = false;
        else
            Thu3_row8.Visible = true;
        //ẩn những dòng trong thứ 4 mà giáo viên không có tiết dạy
        if (txtThu4Tiet1_Mon.Value == "")
        {
            Thu4_row1_td1.Visible = false;
            Thu4_row1_td2.Visible = false;
            Thu4_row1_td4.Visible = false;
            Thu4_row1_td5.Visible = false;
            Thu4_row1_td6.Visible = false;
        }
        else
        {
            Thu4_row1_td1.Visible = true;
            Thu4_row1_td2.Visible = true;
            Thu4_row1_td4.Visible = true;
            Thu4_row1_td5.Visible = true;
            Thu4_row1_td6.Visible = true;
        }
        if (txtThu4Tiet2_Mon.Value == "")
            Thu4_row2.Visible = false;
        else
            Thu4_row2.Visible = true;
        if (txtThu4Tiet3_Mon.Value == "")
            Thu4_row3.Visible = false;
        else
            Thu4_row3.Visible = true;
        if (txtThu4Tiet4_Mon.Value == "")
            Thu4_row4.Visible = false;
        else
            Thu4_row4.Visible = true;
        if (txtThu4Tiet5_Mon.Value == "")
        {
            Thu4_row5_td1.Visible = false;
            Thu4_row5_td2.Visible = false;
            Thu4_row5_td4.Visible = false;
            Thu4_row5_td5.Visible = false;
            Thu4_row5_td6.Visible = false;
        }
        else
        {
            Thu4_row5_td1.Visible = true;
            Thu4_row5_td2.Visible = true;
            Thu4_row5_td4.Visible = true;
            Thu4_row5_td5.Visible = true;
            Thu4_row5_td6.Visible = true;
        }
        if (txtThu4Tiet6_Mon.Value == "")
            Thu4_row6.Visible = false;
        else
            Thu4_row6.Visible = true;
        if (txtThu4Tiet7_Mon.Value == "")
            Thu4_row7.Visible = false;
        else
            Thu4_row7.Visible = true;
        if (txtThu4Tiet8_Mon.Value == "")
            Thu4_row8.Visible = false;
        else
            Thu4_row8.Visible = true;
        //ẩn những dòng trong thứ 5 mà giáo viên không có tiết dạy
        if (txtThu5Tiet1_Mon.Value == "")
        {
            Thu5_row1_td1.Visible = false;
            Thu5_row1_td2.Visible = false;
            Thu5_row1_td4.Visible = false;
            Thu5_row1_td5.Visible = false;
            Thu5_row1_td6.Visible = false;
        }
        else
        {
            Thu5_row1_td1.Visible = true;
            Thu5_row1_td2.Visible = true;
            Thu5_row1_td4.Visible = true;
            Thu5_row1_td5.Visible = true;
            Thu5_row1_td6.Visible = true;
        }
        if (txtThu5Tiet2_Mon.Value == "")
            Thu5_row2.Visible = false;
        else
            Thu5_row2.Visible = true;
        if (txtThu5Tiet3_Mon.Value == "")
            Thu5_row3.Visible = false;
        else
            Thu5_row3.Visible = true;
        if (txtThu5Tiet4_Mon.Value == "")
            Thu5_row4.Visible = false;
        else
            Thu5_row4.Visible = true;
        if (txtThu5Tiet5_Mon.Value == "")
        {
            Thu5_row5_td1.Visible = false;
            Thu5_row5_td2.Visible = false;
            Thu5_row5_td4.Visible = false;
            Thu5_row5_td5.Visible = false;
            Thu5_row5_td6.Visible = false;
        }
        else
        {
            Thu5_row5_td1.Visible = true;
            Thu5_row5_td2.Visible = true;
            Thu5_row5_td4.Visible = true;
            Thu5_row5_td5.Visible = true;
            Thu5_row5_td6.Visible = true;
        }
        if (txtThu5Tiet6_Mon.Value == "")
            Thu5_row6.Visible = false;
        else
            Thu5_row6.Visible = true;
        if (txtThu5Tiet7_Mon.Value == "")
            Thu5_row7.Visible = false;
        else
            Thu5_row7.Visible = true;
        if (txtThu5Tiet8_Mon.Value == "")
            Thu5_row8.Visible = false;
        else
            Thu5_row8.Visible = true;
        //ẩn những dòng trong thứ 6 mà giáo viên không có tiết dạy
        if (txtThu6Tiet1_Mon.Value == "")
        {
            Thu6_row1_td1.Visible = false;
            Thu6_row1_td2.Visible = false;
            Thu6_row1_td4.Visible = false;
            Thu6_row1_td5.Visible = false;
            Thu6_row1_td6.Visible = false;
        }
        else
        {
            Thu6_row1_td1.Visible = true;
            Thu6_row1_td2.Visible = true;
            Thu6_row1_td4.Visible = true;
            Thu6_row1_td5.Visible = true;
            Thu6_row1_td6.Visible = true;
        }
        if (txtThu6Tiet2_Mon.Value == "")
            Thu6_row2.Visible = false;
        else
            Thu6_row2.Visible = true;
        if (txtThu6Tiet3_Mon.Value == "")
            Thu6_row3.Visible = false;
        else
            Thu6_row3.Visible = true;
        if (txtThu6Tiet4_Mon.Value == "")
            Thu6_row4.Visible = false;
        else
            Thu6_row4.Visible = true;
        if (txtThu6Tiet5_Mon.Value == "")
        {
            Thu6_row5_td1.Visible = false;
            Thu6_row5_td2.Visible = false;
            Thu6_row5_td4.Visible = false;
            Thu6_row5_td5.Visible = false;
            Thu6_row5_td6.Visible = false;
        }
        else
        {
            Thu6_row5_td1.Visible = true;
            Thu6_row5_td2.Visible = true;
            Thu6_row5_td4.Visible = true;
            Thu6_row5_td5.Visible = true;
            Thu6_row5_td6.Visible = true;
        }
        if (txtThu6Tiet6_Mon.Value == "")
            Thu6_row6.Visible = false;
        else
            Thu6_row6.Visible = true;
        if (txtThu6Tiet7_Mon.Value == "")
            Thu6_row7.Visible = false;
        else
            Thu6_row7.Visible = true;
        if (txtThu6Tiet8_Mon.Value == "")
            Thu6_row8.Visible = false;
        else
            Thu6_row8.Visible = true;

        //
        if (txtThu2Tiet1_Mon.Value == ""
            && txtThu2Tiet2_Mon.Value == ""
            && txtThu2Tiet3_Mon.Value == ""
            && txtThu2Tiet4_Mon.Value == "")
            txtThu2_sang.Visible = false;
        else
            txtThu2_sang.Visible = true;
        if (txtThu2Tiet5_Mon.Value == ""
            && txtThu2Tiet6_Mon.Value == ""
            && txtThu2Tiet7_Mon.Value == ""
            && txtThu2Tiet8_Mon.Value == "")
            txtThu2_chieu.Visible = false;
        else
            txtThu2_chieu.Visible = true;
        if (txtThu3Tiet1_Mon.Value == ""
           && txtThu3Tiet2_Mon.Value == ""
           && txtThu3Tiet3_Mon.Value == ""
           && txtThu3Tiet4_Mon.Value == "")
            txtThu3_sang.Visible = false;
        else
            txtThu3_sang.Visible = true;
        if (txtThu3Tiet5_Mon.Value == ""
            && txtThu3Tiet6_Mon.Value == ""
            && txtThu3Tiet7_Mon.Value == ""
            && txtThu3Tiet8_Mon.Value == "")
            txtThu3_chieu.Visible = false;
        else
            txtThu3_chieu.Visible = true;
        if (txtThu4Tiet1_Mon.Value == ""
           && txtThu4Tiet2_Mon.Value == ""
           && txtThu4Tiet3_Mon.Value == ""
           && txtThu4Tiet4_Mon.Value == "")
            txtThu4_sang.Visible = false;
        else
            txtThu4_sang.Visible = true;
        if (txtThu4Tiet5_Mon.Value == ""
            && txtThu4Tiet6_Mon.Value == ""
            && txtThu4Tiet7_Mon.Value == ""
            && txtThu4Tiet8_Mon.Value == "")
            txtThu4_chieu.Visible = false;
        else
            txtThu4_chieu.Visible = true;
        if (txtThu5Tiet1_Mon.Value == ""
           && txtThu5Tiet2_Mon.Value == ""
           && txtThu5Tiet3_Mon.Value == ""
           && txtThu5Tiet4_Mon.Value == "")
            txtThu5_sang.Visible = false;
        else
            txtThu5_sang.Visible = true;
        if (txtThu5Tiet5_Mon.Value == ""
            && txtThu5Tiet6_Mon.Value == ""
            && txtThu5Tiet7_Mon.Value == ""
            && txtThu5Tiet8_Mon.Value == "")
            txtThu5_chieu.Visible = false;
        else
            txtThu5_chieu.Visible = true;
        if (txtThu6Tiet1_Mon.Value == ""
           && txtThu6Tiet2_Mon.Value == ""
           && txtThu6Tiet3_Mon.Value == ""
           && txtThu6Tiet4_Mon.Value == "")
            txtThu6_sang.Visible = false;
        else
            txtThu6_sang.Visible = true;
        if (txtThu6Tiet5_Mon.Value == ""
            && txtThu6Tiet6_Mon.Value == ""
            && txtThu6Tiet7_Mon.Value == ""
            && txtThu6Tiet8_Mon.Value == "")
            txtThu6_chieu.Visible = false;
        else
            txtThu6_chieu.Visible = true;
    }
}
