﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_ThongKeGiaoVienDuGio.aspx.cs" Inherits="admin_page_module_function_module_LichCongTac_module_ThongKeGiaoVienDuGio" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <style>
        .nav-tabs .nav-link {
            text-decoration: none;
            font-weight: bold;
            font-size: 16px;
            text-align: center;
            color: #000;
        }

        a.active {
            background: #52bcd3 !important;
            color: #fff !important;
        }
    </style>
    <script>
        function xemDetail(id) {
            console.log(id)
        }
        function xem(id) {
            console.log(id)
        }
    </script>
    <div class="card card-block">
        <div class="container">
            <h3>THỐNG KÊ DỰ GIỜ</h3>
            <%-- <asp:UpdatePanel runat="server">
                <ContentTemplate>--%>
            <div class="search">
                <div class="col-3" style="margin-right: 20px">
                    Từ ngày: 
                <input type="date" runat="server" id="dteTuNgay" value="" class="form-control" />
                </div>
                <div class="col-3">
                    đến ngày: 
                <input type="date" runat="server" id="dteDenNgay" value="" class="form-control" />
                </div>
                <div class="col-3" style="margin-left:20px; margin-top:22px">
                    <a href="javascript:void(0)" class="btn btn-primary" runat="server" id="btnXem" onserverclick="btnXem_ServerClick">Xem</a>
                </div>
            </div>
           
           
                <div class="col-xs-12 ">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" href="#profile" role="tab" data-toggle="tab">Danh sách Giáo viên tham gia dự giờ</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#buzz" role="tab" data-toggle="tab">Danh sách Giáo viên được đăng kí dự giờ</a>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="profile">
                            <h3 class="m-2">DANH SÁCH GIÁO VIÊN THAM GIA ĐĂNG KÍ DỰ GIỜ</h3>
                            <div class="col-6">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr class="head_table">
                                            <th>STT</th>
                                            <th>Họ tên</th>
                                            <th>Số lần tham gia</th>
                                            <th>#</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater runat="server" ID="rpGVThamGiaDuGio">
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="text-align: center"><%#Container.ItemIndex+1 %></td>
                                                    <td class="tiet"><%#Eval("username_fullname") %></td>
                                                    <td class="tiet text-sm-center"><%#Eval("count") %></td>
                                                    <td class="tiet"><a href="#" class="btn btn-primary" id="<%#Eval("Key") %>" onclick="xemDetail(this.id)" data-toggle="modal" data-target="#exampleModalCenter-<%#Eval("Key") %>">Xem</a></td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <tr runat="server" id="txtNote1">
                                            <td colspan="3" class="text-danger text-sm-center font-weight-bold">Không có dữ liệu</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="buzz">
                            <h3 class="m-2">DANH SÁCH GIÁO VIÊN THAM ĐƯỢC ĐĂNG KÍ DỰ GIỜ</h3>
                            <div class="col-6">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr class="head_table">
                                            <th>STT</th>
                                            <th>Họ tên</th>
                                            <th>Số lần được đăng kí</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater runat="server" ID="rpGVBiDuGio">
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="text-align: center"><%#Container.ItemIndex+1 %></td>
                                                    <td class="tiet"><%#Eval("username_fullname") %></td>
                                                    <td class="tiet"><%#Eval("count") %></td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <tr runat="server" id="txtNote2">
                                            <td colspan="3" class="text-danger text-sm-center font-weight-bold">Không có dữ liệu</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            
            <%--  </ContentTemplate>
            </asp:UpdatePanel>--%>
            <input type="text" runat="server" id="txtTuthang" hidden />
            <input type="text" runat="server" id="txtDenthang" hidden />
        </div>
        <%--modal chi tiết--%>
        <asp:Repeater runat="server" ID="rpChiTiet" OnItemDataBound="rpChiTiet_ItemDataBound">
            <ItemTemplate>
                <div class="modal fade" id="exampleModalCenter-<%#Eval("Key") %>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <h3>CHI TIẾT</h3>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr class="head_table">
                                            <th>#</th>
                                            <th>Họ tên</th>
                                            <th>Ngày đăng kí</th>
                                            <th>Lớp dự giờ</th>
                                            <th>Thứ</th>
                                            <th>Môn</th>
                                            <th>Tiết</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <asp:Repeater runat="server" ID="rpItemDetail">
                                            <ItemTemplate>
                                                <tr>
                                                    <td style="text-align: center"><%#Container.ItemIndex+1 %></td>
                                                    <td class="tiet"><%#Eval("username_fullname") %></td>
                                                    <td class="tiet"><%#Eval("dugio_ngaydangky","{0:dd/MM/yyyy}") %></td>
                                                    <td class="tiet"><%#Eval("lichbaogiangchitiet_lop") %></td>
                                                    <td class="tiet"><%#Eval("lichbaogiangtheotuan_thuhoc") %></td>
                                                    <td class="tiet"><%#Eval("lichbaogiangchitiet_monhoc") %></td>
                                                    <td class="tiet"><%#Eval("lichbaogiangchitiet_tiethoc") %></td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tbody>
                                </table>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary text-white" style="background-color: #4f5f6f" data-dismiss="modal">Đóng</button>
                            </div>
                        </div>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
    <script>
        <%--function getValue() {
            var e = document.getElementById("slTuThang");
            var strUser = e.options[e.selectedIndex].value;
            document.getElementById('<%=txtTuthang.ClientID%>').value = strUser;
            var x = document.getElementById("slDenThang");
            var value = x.options[x.selectedIndex].value;
            document.getElementById('<%=txtDenthang.ClientID%>').value = value;
        }
        function setSelected() {
            var a = document.getElementById('<%=txtTuthang.ClientID%>').value;
            var b = document.getElementById('<%=txtDenthang.ClientID%>').value;
            var $option = $('#slTuThang').children('option[value="' + a + '"]');
            $option.attr('selected', true);
            var $option1 = $('#slDenThang').children('option[value="' + b + '"]');
            $option1.attr('selected', true);
        }--%>
    </script>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

