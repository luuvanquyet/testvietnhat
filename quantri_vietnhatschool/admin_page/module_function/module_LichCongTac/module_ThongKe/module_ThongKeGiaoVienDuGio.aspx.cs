﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_LichCongTac_module_ThongKeGiaoVienDuGio : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public int STT = 1;
    public int stt = 1;
    cls_Alert alert = new cls_Alert();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            var getDSGVThamGiaDuGio = (from s in db.tbHocTap_DuGios
                                      join u in db.admin_Users on s.nguoidugio_id equals u.username_id
                                      group u by u.username_id into g
                                      select new
                                      {
                                          g.Key,
                                          username_fullname = g.First().username_fullname,
                                          count = (from s in db.tbHocTap_DuGios
                                                   join u in db.admin_Users on s.nguoidugio_id equals u.username_id
                                                   where s.nguoidugio_id == g.Key
                                                   select s).Count()
                                      }).OrderByDescending(g=>g.count);
            rpGVThamGiaDuGio.DataSource = getDSGVThamGiaDuGio;
            rpGVThamGiaDuGio.DataBind();
            rpChiTiet.DataSource = getDSGVThamGiaDuGio;
            rpChiTiet.DataBind();
            if (getDSGVThamGiaDuGio.Count() == 0)
                txtNote1.Visible = true;
            else
                txtNote1.Visible = false;
            var getDSGVBiDuGio = (from s in db.tbHocTap_DuGios
                                 //join u in db.admin_Users on s.nguoibidugio_id equals u.username_id
                                 group s by s.nguoibidugio_id into g
                                 select new
                                 {
                                     //g.Key,
                                     username_fullname = (from u in db.admin_Users
                                                          where u.username_id == g.First().nguoibidugio_id
                                                          select u.username_fullname).First(),
                                     count = (from dg in db.tbHocTap_DuGios
                                                  //join u in db.admin_Users on s.nguoibidugio_id equals u.username_id
                                              where dg.nguoibidugio_id == g.First().nguoibidugio_id
                                              group dg by new { dg.dugio_ngaydangky.Value.Day, dg.dugio_ngaydangky.Value.Month, dg.dugio_ngaydangky.Value.Year}  into k
                                              select new
                                              {
                                                  k.Key
                                              }).Count(),
                                 }).OrderByDescending(g => g.count);
            rpGVBiDuGio.DataSource = getDSGVBiDuGio;
            rpGVBiDuGio.DataBind();
            if (getDSGVBiDuGio.Count() == 0)
                txtNote2.Visible = true;
            else
                txtNote2.Visible = false;
        }
    }

    protected void btnXem_ServerClick(object sender, EventArgs e)
    {
        if (dteTuNgay.Value == "" || dteDenNgay.Value == "")
        {
            if (dteTuNgay.Value == "")
                alert.alert_Warning(Page, "Vui lòng chọn thời gian bắt đầu", "");
            else
                alert.alert_Warning(Page, "Vui lòng chọn thời gian kết thúc", "");
        }
        else
        {
            var getDSGVThamGiaDuGio = (from s in db.tbHocTap_DuGios
                                      join u in db.admin_Users on s.nguoidugio_id equals u.username_id
                                      where s.dugio_ngaydangky.Value.Day >= Convert.ToDateTime(dteTuNgay.Value).Day
                                          && s.dugio_ngaydangky.Value.Month >= Convert.ToDateTime(dteTuNgay.Value).Month
                                          && s.dugio_ngaydangky.Value.Year >= Convert.ToDateTime(dteTuNgay.Value).Year
                                          && s.dugio_ngaydangky.Value.Day <= Convert.ToDateTime(dteDenNgay.Value).Day
                                          && s.dugio_ngaydangky.Value.Month <= Convert.ToDateTime(dteDenNgay.Value).Month
                                          && s.dugio_ngaydangky.Value.Year <= Convert.ToDateTime(dteDenNgay.Value).Year
                                      group u by u.username_id into g
                                      select new
                                      {
                                          g.Key,
                                          username_fullname = g.First().username_fullname,
                                          count = (from s in db.tbHocTap_DuGios
                                                   join u in db.admin_Users on s.nguoidugio_id equals u.username_id
                                                   where s.nguoidugio_id == g.Key
                                                   && s.dugio_ngaydangky.Value.Day >= Convert.ToDateTime(dteTuNgay.Value).Day
                                                   && s.dugio_ngaydangky.Value.Month >= Convert.ToDateTime(dteTuNgay.Value).Month
                                                   && s.dugio_ngaydangky.Value.Year >= Convert.ToDateTime(dteTuNgay.Value).Year
                                                   && s.dugio_ngaydangky.Value.Day <= Convert.ToDateTime(dteDenNgay.Value).Day
                                                   && s.dugio_ngaydangky.Value.Month <= Convert.ToDateTime(dteDenNgay.Value).Month
                                                   && s.dugio_ngaydangky.Value.Year <= Convert.ToDateTime(dteDenNgay.Value).Year
                                                   select s).Count()
                                      }).OrderByDescending(g => g.count);
            rpGVThamGiaDuGio.DataSource = getDSGVThamGiaDuGio;
            rpGVThamGiaDuGio.DataBind();
            rpChiTiet.DataSource = getDSGVThamGiaDuGio;
            rpChiTiet.DataBind();
            if (getDSGVThamGiaDuGio.Count() == 0)
                txtNote1.Visible = true;
            else
                txtNote1.Visible = false;
            var getDSGVBiDuGio = (from s in db.tbHocTap_DuGios
                                 join u in db.admin_Users on s.nguoibidugio_id equals u.username_id
                                 where s.dugio_ngaydangky.Value.Day >= Convert.ToDateTime(dteTuNgay.Value).Day
                                      && s.dugio_ngaydangky.Value.Month >= Convert.ToDateTime(dteTuNgay.Value).Month
                                      && s.dugio_ngaydangky.Value.Year >= Convert.ToDateTime(dteTuNgay.Value).Year
                                      && s.dugio_ngaydangky.Value.Day <= Convert.ToDateTime(dteDenNgay.Value).Day
                                      && s.dugio_ngaydangky.Value.Month <= Convert.ToDateTime(dteDenNgay.Value).Month
                                      && s.dugio_ngaydangky.Value.Year <= Convert.ToDateTime(dteDenNgay.Value).Year
                                 group u by u.username_id into g
                                 select new
                                 {
                                     g.Key,
                                     username_fullname = g.First().username_fullname,
                                     count = (from s in db.tbHocTap_DuGios
                                              join u in db.admin_Users on s.nguoibidugio_id equals u.username_id
                                              where s.nguoibidugio_id == g.Key
                                              && s.dugio_ngaydangky.Value.Day >= Convert.ToDateTime(dteTuNgay.Value).Day
                                              && s.dugio_ngaydangky.Value.Month >= Convert.ToDateTime(dteTuNgay.Value).Month
                                              && s.dugio_ngaydangky.Value.Year >= Convert.ToDateTime(dteTuNgay.Value).Year
                                              && s.dugio_ngaydangky.Value.Day <= Convert.ToDateTime(dteDenNgay.Value).Day
                                              && s.dugio_ngaydangky.Value.Month <= Convert.ToDateTime(dteDenNgay.Value).Month
                                              && s.dugio_ngaydangky.Value.Year <= Convert.ToDateTime(dteDenNgay.Value).Year
                                              select s).Count()
                                 }).OrderByDescending(g => g.count);
            rpGVBiDuGio.DataSource = getDSGVBiDuGio;
            rpGVBiDuGio.DataBind();
            if (getDSGVBiDuGio.Count() == 0)
                txtNote2.Visible = true;
            else
                txtNote2.Visible = false;
        }
    }

    protected void rpChiTiet_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Repeater rpItemDetail = e.Item.FindControl("rpItemDetail") as Repeater;
        int usename_id = int.Parse(DataBinder.Eval(e.Item.DataItem, "Key").ToString());
        if (dteTuNgay.Value != "" && dteDenNgay.Value != "")
        {
            var getDetail = from d in db.tbHocTap_DuGios
                            join u in db.admin_Users on d.nguoidugio_id equals u.username_id
                            join bg in db.tbLichBaoGiangChiTiets on d.lichbaogiangchitiet_id equals bg.lichbaogiangchitiet_id
                            join bgtt in db.tbLichBaoGiangTheoTuans on bg.lichbaogiangtheotuan_id equals bgtt.lichbaogiangtheotuan_id
                            where d.nguoidugio_id == usename_id
                            && d.dugio_ngaydangky.Value.Day >= Convert.ToDateTime(dteTuNgay.Value).Day
                            && d.dugio_ngaydangky.Value.Month >= Convert.ToDateTime(dteTuNgay.Value).Month
                            && d.dugio_ngaydangky.Value.Year >= Convert.ToDateTime(dteTuNgay.Value).Year
                            && d.dugio_ngaydangky.Value.Day <= Convert.ToDateTime(dteDenNgay.Value).Day
                            && d.dugio_ngaydangky.Value.Month <= Convert.ToDateTime(dteDenNgay.Value).Month
                            && d.dugio_ngaydangky.Value.Year <= Convert.ToDateTime(dteDenNgay.Value).Year
                            select new
                            {
                                u.username_fullname,
                                d.dugio_ngaydangky,
                                bg.lichbaogiangchitiet_lop,
                                bg.lichbaogiangchitiet_tiethoc,
                                bg.lichbaogiangchitiet_monhoc,
                                bgtt.lichbaogiangtheotuan_thuhoc,
                            };
            rpItemDetail.DataSource = getDetail;
            rpItemDetail.DataBind();
        }
        else
        {
            var getDetail = from d in db.tbHocTap_DuGios
                            join u in db.admin_Users on d.nguoidugio_id equals u.username_id
                            join bg in db.tbLichBaoGiangChiTiets on d.lichbaogiangchitiet_id equals bg.lichbaogiangchitiet_id
                            join bgtt in db.tbLichBaoGiangTheoTuans on bg.lichbaogiangtheotuan_id equals bgtt.lichbaogiangtheotuan_id
                            where d.nguoidugio_id == usename_id
                            select new
                            {
                                u.username_fullname,
                                d.dugio_ngaydangky,
                                bg.lichbaogiangchitiet_lop,
                                bg.lichbaogiangchitiet_tiethoc,
                                bg.lichbaogiangchitiet_monhoc,
                                bgtt.lichbaogiangtheotuan_thuhoc,

                            };
            rpItemDetail.DataSource = getDetail;
            rpItemDetail.DataBind();
        }

    }
}