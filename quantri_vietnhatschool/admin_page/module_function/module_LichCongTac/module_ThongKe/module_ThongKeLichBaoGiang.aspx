﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_ThongKeLichBaoGiang.aspx.cs" Inherits="admin_page_module_function_module_LichCongTac_module_ThongKeLichBaoGiang" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="card card-block">
        <div class="form-group row" style="text-align: center">
            <h3 class="text-primary">THỐNG KÊ BÁO GIẢNG</h3>
            <div class="col-sm-10">
                <strong style="display: flex; align-items: center;">Tuần:<asp:DropDownList ID="ddlTuanHoc" CssClass="form-control" runat="server" Width="30%" AutoPostBack="true"></asp:DropDownList>&nbsp;
                </strong>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-5 mr-2">
                <div class="form-group table-responsive">
                    <h3>Danh sách giáo viên đã nhập</h3>
                    <table class="table table-striped table-hover">
                        <tr style="text-align: center">
                            <th scope="col">#</th>
                            <th scope="col">Họ và tên</th>
                            <th scope="col">Bộ phận</th>
                            <th scope="col">Ngày nhập</th>
                        </tr>
                        <asp:Repeater runat="server" ID="rpDSDaNhap">
                            <ItemTemplate>
                                <tr>
                                    <td><%#Container.ItemIndex+1 %></td>
                                    <td><%#Eval("username_fullname") %></td>
                                    <td><%#Eval("bophan_name") %></td>
                                    <td><%#Eval("lichbaogiang_ngaytao") %></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                </div>
            </div>
            <div class="col-5">
                <div class="form-group table-responsive">
                    <h3>Danh sách giáo viên chưa nhập</h3>
                    <table class="table table-striped table-hover">
                        <tr style="text-align: center">
                            <th scope="col">#</th>
                            <th scope="col">Họ và tên</th>
                            <th scope="col">Bộ phận</th>
                        </tr>
                        <asp:Repeater runat="server" ID="rpDSChuaNhap">
                            <ItemTemplate>
                                <tr>
                                    <td><%#Container.ItemIndex+1 %></td>
                                    <td><%#Eval("username_fullname") %></td>
                                    <td><%#Eval("bophan_name") %></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

