﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_LichCongTac_module_ThongKeLichBaoGiang : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["UserName"].Value != null)
        {
            if (!IsPostBack)
            {
                //đổ ra ds tuần học
                var dsTuan = from t in db.tbHocTap_Tuans where t.tuan_hidden == false && t.namhoc_id==3 select t;
                ddlTuanHoc.Items.Clear();
                ddlTuanHoc.Items.Insert(0, "Chọn Tuần học");
                ddlTuanHoc.AppendDataBoundItems = true;
                ddlTuanHoc.DataTextField = "tuan_name";
                ddlTuanHoc.DataValueField = "tuan_id";
                ddlTuanHoc.DataSource = dsTuan;
                ddlTuanHoc.DataBind();

            }
            if (ddlTuanHoc.SelectedValue != "Chọn Tuần học")
            {
                loadData();
            }
        }
        else
        {
            Response.Redirect("/admin-login");
        }
    }
    private void loadData()
    {
        var listUser = from u in db.admin_Users
                       join bp in db.tbBoPhans on u.bophan_id equals bp.bophan_id
                       where u.groupuser_id == 3 && u.bophan_id != 100 && bp.hidden == true
                       select new
                       {
                           u.username_fullname,
                           bp.bophan_name,
                           
                       };
        var listDaNhap = from bg in db.tbLichBaoGiangs
                         join u in db.admin_Users on bg.username_id equals u.username_id
                         join bp in db.tbBoPhans on u.bophan_id equals bp.bophan_id
                         where bg.tuan_id == Convert.ToInt32(ddlTuanHoc.SelectedValue)
                         && u.groupuser_id == 3 && u.bophan_id != 100 
                         select new
                         {
                             u.username_fullname,
                             bp.bophan_name,
                            
                         };
        var listDaNhap2 = from bg in db.tbLichBaoGiangs
                         join u in db.admin_Users on bg.username_id equals u.username_id
                         join bp in db.tbBoPhans on u.bophan_id equals bp.bophan_id
                         where bg.tuan_id == Convert.ToInt32(ddlTuanHoc.SelectedValue)
                         && u.groupuser_id == 3 && u.bophan_id != 100
                         select new
                         {
                             u.username_fullname,
                             bp.bophan_name,
                             bg.lichbaogiang_ngaytao
                         };
        rpDSDaNhap.DataSource = listDaNhap2;
        rpDSDaNhap.DataBind();
        rpDSChuaNhap.DataSource = listUser.Except(listDaNhap);
        rpDSChuaNhap.DataBind();
    }
}