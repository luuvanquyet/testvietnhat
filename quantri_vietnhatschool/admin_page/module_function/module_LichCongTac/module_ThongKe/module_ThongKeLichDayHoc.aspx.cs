﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_LichCongTac_module_ThongKe_module_ThongKeLichDayHoc : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.Cookies["UserName"].Value != null)
        {
            loadData();
        }
        else
        {
            Response.Redirect("/admin-login");
        }
    }
    private void loadData()
    {
        var listUser = from u in db.admin_Users
                       join bp in db.tbBoPhans on u.bophan_id equals bp.bophan_id
                       where u.groupuser_id == 3 && u.bophan_id != 100 && bp.hidden == true
                       select new
                       {
                           u.username_fullname,
                           bp.bophan_name,

                       };
        var listDaNhap = from bg in db.tbQuanTri_KeHoachDayHoc_Version2s
                         join u in db.admin_Users on bg.username_id equals u.username_id
                         join bp in db.tbBoPhans on u.bophan_id equals bp.bophan_id
                         where bg.namhoc_id == 3
                         && u.groupuser_id == 3 && u.bophan_id != 100
                         select new
                         {
                             u.username_fullname,
                             bp.bophan_name,
                         };
        var listDaNhap2 = from bg in db.tbQuanTri_KeHoachDayHoc_Version2s
                          join u in db.admin_Users on bg.username_id equals u.username_id
                          join bp in db.tbBoPhans on u.bophan_id equals bp.bophan_id
                          where bg.namhoc_id == 3
                          && u.groupuser_id == 3 && u.bophan_id != 100
                          group bg by bg.username_id into g
                          select new
                          {
                              //g.Key,
                              username_fullname =(from user in db.admin_Users
                                                  where user.username_id == g.Key
                                                  select user.username_fullname).First(),
                              bophan_name = (from bp in db.tbBoPhans
                                            join u in db.admin_Users on bp.bophan_id equals u.bophan_id
                                            where u.username_id == g.Key
                                            select bp.bophan_name).First(),
                              //bg.lichbaogiang_ngaytao
                          };
        rpDSDaNhap.DataSource = listDaNhap2;
        rpDSDaNhap.DataBind();
        rpDSChuaNhap.DataSource = listUser.Except(listDaNhap);
        rpDSChuaNhap.DataBind();
    }
}