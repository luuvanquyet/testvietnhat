﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_ThongKeNhanXetHocSinhHangTuan.aspx.cs" Inherits="admin_page_module_function_module_OMT_omt_GiaoVien_admin_omt_BGH_ThongKeDanhGiaCuoiTuan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <script>
        function func(id) {
            console.log(id)
        }

    </script>
    <style>
        .wrapper__select {
            margin-top: 10px;
            margin-left: 10px;
            width: 100%;
            display: flex;
        }
    </style>
    <div class="main-omt">
        <div class="omt-header">
            <i class="fa fa-list-alt omt__icon" aria-hidden="true"></i>
            <h4 class="header-title">Thống kê đánh giá cuối tuần</h4>
        </div>
        <div class="omt-top">
            <div class="wrapper__select">
                <div class="col-3" style="margin-right: 10px;">
                    <input type="date" id="dteTuNgay" runat="server" class="form-control" />
                </div>
                <div class="col-3" style="margin-right: 10px;">
                    <input type="date" id="dteDenNgay" runat="server" class="form-control" />
                </div>
                <div class="col-1">
                    <a href="#" id="btnXem" class="btn btn-primary" runat="server" onserverclick="btnXem_ServerClick">Xem</a>
                </div>
            </div>
        </div>
        <div style="display: none">
            <input type="text" id="txtHocSinh" runat="server" />
            <input type="text" id="txtRatting" runat="server" />
            <a href="#" id="btnChiTietHocSinh" runat="server"></a>
        </div>
        <div class="wrapper__select">
            <div style="width: 45%" class="mr-2">
                <h4>DANH SÁCH GV ĐÃ NHẬN XÉT</h4>
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Họ tên</th>
                            <th scope="col">Số lượng</th>
                            <th scope="col">#</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rpListGVDaDanhGia" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td><%#Container.ItemIndex+1 %></td>
                                    <td><%#Eval("username_fullname") %></td>
                                    <td><%#Eval("count_hs") %></td>
                                    <td><a href="#" class="btn btn-primary" id="<%#Eval("Key") %>" data-toggle="modal" data-target="#exampleModalCenter-<%#Eval("Key") %>">Xem</a></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>
            <div style="width: 45%">
                <h4>DANH SÁCH GV CHƯA NHẬN XÉT</h4>
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Họ tên</th>
                            <th scope="col">Bộ phận</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rpListGVChuaDanhGia" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td><%#Container.ItemIndex+1 %></td>
                                    <td><%#Eval("username_fullname") %></td>
                                    <td><%#Eval("bophan_name") %></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <%--modal chi tiết--%>
    <asp:Repeater runat="server" ID="rpChiTiet" OnItemDataBound="rpChiTiet_ItemDataBound">
        <ItemTemplate>
            <div class="modal fade" id="exampleModalCenter-<%#Eval("Key") %>" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <h3>CHI TIẾT</h3>
                            <table class="table table-bordered">
                                <thead>
                                    <tr class="head_table">
                                        <th>#</th>
                                        <th>Họ tên HS</th>
                                        <th>Nội dung đánh giá</th>
                                        <th>Ngày đánh giá</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater runat="server" ID="rpItemDetail">
                                        <ItemTemplate>
                                            <tr>
                                                <td style="text-align: center"><%#Container.ItemIndex+1 %></td>
                                                <td class="tiet"><%#Eval("hocsinh_name") %></td>
                                                <td class="tiet"><%#Eval("nxct_nhanxet") %></td>
                                                <td class="tiet"><%#Eval("nxct_ngaydanhgia","{0:dd/MM/yyyy}") %></td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary text-white" style="background-color: #4f5f6f" data-dismiss="modal">Đóng</button>
                        </div>
                    </div>
                </div>
            </div>
        </ItemTemplate>
    </asp:Repeater>
    <style>
        .main-omt {
            border: 1px solid #32c5d2;
            background-color: #fff;
        }

            .main-omt .omt-header {
                background-color: #32c5d2;
                padding: 4px 7px;
                display: flex;
            }

        .omt-header .header-title {
            font-size: 20px;
            padding: 10px 10px;
            color: white;
        }

        .omt-header .omt__icon {
            font-size: 30px;
            padding: 8px 10px;
            color: white;
        }

        .omt-top {
            display: flex;
            /*padding: 0 20px;*/
        }

            .omt-top .form-omt {
                width: 17%;
                height: 40px !important;
                margin-right: 15px;
                margin-top: 10px;
            }

        .fixed-table-container {
            padding-top: 20px;
        }

        .table-left {
            width: 33.333% !important;
            min-height: 1px;
            padding-left: 15px;
            padding-right: 15px;
        }

        .portlet {
            width: 100% !important;
            border: 1px solid #e7ecf1 !important;
        }

        .portlet-title {
            border-bottom: 1px solid #eef1f5;
            min-height: 48px;
        }

        .caption-title {
            padding: 0;
            display: inline-block;
            margin-left: 5%;
            margin-top: 5px;
            font-size: 25px;
            font-weight: bold;
        }

        .group-tabs ul {
            width: 100%;
        }

            .group-tabs ul li {
                position: relative;
                display: inline-block;
                width: 100%;
                min-height: 20px;
                padding: 10px;
                border-bottom: 1px solid #F4F6F9;
            }

        .click {
            background-color: red;
        }

        .gro p-ta; {
            ackroun F9;
            cu .group tabs text-de oration:;
        }

        u a:hover {
            text-d coration: none;
            .lab l;

        {
            ont-size: 10p;
            -weight: 600;
            d 1px 3px;
            ion: a solut top: 20%;
            i b r: #36 6d3;
            ;
            . ab-content width: 66.666 7% !i min-height: 1px;
            dding-left: 15 x;
            pa 5px;
        }

        . {
            width: 0 min-h ight: .portright;

        {
            width: 1 0% !mporant;
            r id #e7ecf1 !i p margi -botto;
        }

        .po t border b id #eef f5; m x;
        }

        e ainer {
            play: flex;
            .portright- ody 10px 20px 20p;
        }

        po marg; mar
        }

        . 2 border: 1p po ve;
        }

        ody {
            p 5px;
            o;
        }

        y-title {
            z m rgin-top: 15 x ont-weight bold;
            .mt-bod;

        {
            . o 3 font-size: 2 margi -top: 20p ma gin-bo h;
        }

        ve g
        }

        .form-group .table-title p {
            font-size: 0.9rem;
            f6f;
        }

        .table-title {
        }

        .table-raiting {
            position: relative;
            top: -15px;
            margin: 0;
        }

        .wrapper__select {
            margin-top: 10px;
            margin-left: 10px;
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

