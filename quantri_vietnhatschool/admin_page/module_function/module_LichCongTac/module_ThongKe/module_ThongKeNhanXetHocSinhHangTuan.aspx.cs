﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_OMT_omt_GiaoVien_admin_omt_BGH_ThongKeDanhGiaCuoiTuan : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    protected void Page_Load(object sender, EventArgs e)
    {

    }


    protected void btnXem_ServerClick(object sender, EventArgs e)
    {
        if (dteTuNgay.Value == "" || dteDenNgay.Value == "")
        {
            if (dteTuNgay.Value == "")
                alert.alert_Warning(Page, "Vui lòng chọn thời gian bắt đầu", "");
            else
                alert.alert_Warning(Page, "Vui lòng chọn thời gian kết thúc", "");
        }
        else
        {
            //get ds các gv đã tham gia đánh giá hs
            var getDanhgia = from gv in db.tbHocTap_NhanXetCuoiTuans
                             join u in db.admin_Users on gv.giaovien_id equals u.username_id
                             where gv.nxct_ngaydanhgia.Value.Day >= Convert.ToDateTime(dteTuNgay.Value).Day
                             && gv.nxct_ngaydanhgia.Value.Month >= Convert.ToDateTime(dteTuNgay.Value).Month
                             && gv.nxct_ngaydanhgia.Value.Year >= Convert.ToDateTime(dteTuNgay.Value).Year
                             && gv.nxct_ngaydanhgia.Value.Day <= Convert.ToDateTime(dteDenNgay.Value).Day
                             && gv.nxct_ngaydanhgia.Value.Month <= Convert.ToDateTime(dteDenNgay.Value).Month
                             && gv.nxct_ngaydanhgia.Value.Year <= Convert.ToDateTime(dteDenNgay.Value).Year
                             group gv by gv.giaovien_id into g
                             select new
                             {
                                 g.Key,
                                 username_fullname = (from u in db.admin_Users
                                                      where u.username_id == g.Key
                                                      select u).First().username_fullname,
                                 count_hs = (from gv in db.tbHocTap_NhanXetCuoiTuans
                                             where gv.giaovien_id == g.Key
                                             && gv.nxct_ngaydanhgia.Value.Day >= Convert.ToDateTime(dteTuNgay.Value).Day
                             && gv.nxct_ngaydanhgia.Value.Month >= Convert.ToDateTime(dteTuNgay.Value).Month
                             && gv.nxct_ngaydanhgia.Value.Year >= Convert.ToDateTime(dteTuNgay.Value).Year
                             && gv.nxct_ngaydanhgia.Value.Day <= Convert.ToDateTime(dteDenNgay.Value).Day
                             && gv.nxct_ngaydanhgia.Value.Month <= Convert.ToDateTime(dteDenNgay.Value).Month
                             && gv.nxct_ngaydanhgia.Value.Year <= Convert.ToDateTime(dteDenNgay.Value).Year
                                             select new
                                             {
                                                 g.Key
                                             }).Count(),
                             };
            rpListGVDaDanhGia.DataSource = getDanhgia;
            rpListGVDaDanhGia.DataBind();
            rpChiTiet.DataSource = getDanhgia;
            rpChiTiet.DataBind();
            //get ds các gv chưa đánh giá hs
            var getFullGV = from u in db.admin_Users
                            join bp in db.tbBoPhans on u.bophan_id equals bp.bophan_id
                            join gr in db.admin_GroupUsers on u.groupuser_id equals gr.groupuser_id
                            where gr.groupuser_id == 3
                            select new
                            {
                                u.username_id,
                                u.username_fullname,
                                bp.bophan_name,
                            };
            var getGVDaDanhGia = from u in db.admin_Users
                                 join bp in db.tbBoPhans on u.bophan_id equals bp.bophan_id
                                 join gv in db.tbHocTap_NhanXetCuoiTuans on u.username_id equals gv.giaovien_id
                                 where gv.nxct_ngaydanhgia.Value.Day >= Convert.ToDateTime(dteTuNgay.Value).Day
                             && gv.nxct_ngaydanhgia.Value.Month >= Convert.ToDateTime(dteTuNgay.Value).Month
                             && gv.nxct_ngaydanhgia.Value.Year >= Convert.ToDateTime(dteTuNgay.Value).Year
                             && gv.nxct_ngaydanhgia.Value.Day <= Convert.ToDateTime(dteDenNgay.Value).Day
                             && gv.nxct_ngaydanhgia.Value.Month <= Convert.ToDateTime(dteDenNgay.Value).Month
                             && gv.nxct_ngaydanhgia.Value.Year <= Convert.ToDateTime(dteDenNgay.Value).Year
                                 select new
                                 {
                                     u.username_id,
                                     u.username_fullname,
                                     bp.bophan_name,
                                 };
            var getGVChuaDanhGia = getFullGV.Except(getGVDaDanhGia);
            rpListGVChuaDanhGia.DataSource = getGVChuaDanhGia;
            rpListGVChuaDanhGia.DataBind();
        }
    }

    protected void rpChiTiet_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Repeater rpItemDetail = e.Item.FindControl("rpItemDetail") as Repeater;
        int usename_id = int.Parse(DataBinder.Eval(e.Item.DataItem, "Key").ToString());
        var getDetail = from nx in db.tbHocTap_NhanXetCuoiTuans
                        join hstl in db.tbHocSinhTrongLops on nx.mhstl_id equals hstl.hstl_id
                        join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                        where nx.nxct_ngaydanhgia.Value.Day >= Convert.ToDateTime(dteTuNgay.Value).Day
                            && nx.nxct_ngaydanhgia.Value.Month >= Convert.ToDateTime(dteTuNgay.Value).Month
                            && nx.nxct_ngaydanhgia.Value.Year >= Convert.ToDateTime(dteTuNgay.Value).Year
                            && nx.nxct_ngaydanhgia.Value.Day <= Convert.ToDateTime(dteDenNgay.Value).Day
                            && nx.nxct_ngaydanhgia.Value.Month <= Convert.ToDateTime(dteDenNgay.Value).Month
                            && nx.nxct_ngaydanhgia.Value.Year <= Convert.ToDateTime(dteDenNgay.Value).Year
                        && nx.giaovien_id == usename_id
                        select new
                        {
                            hs.hocsinh_name,
                            nx.nxct_ngaydanhgia,
                            nx.nxct_nhanxet
                        };
        rpItemDetail.DataSource = getDetail;
        rpItemDetail.DataBind();
    }
}