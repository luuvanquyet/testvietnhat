﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_ThongKeGiaoVienDuGio.aspx.cs" Inherits="admin_page_module_function_module_LichCongTac_module_ThongKeGiaoVienDuGio" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <style>
        .nav-tabs .nav-link {
            text-decoration: none;
            font-weight: bold;
            font-size: 16px;
            text-align: center;
            color: #000;
        }

        a.active {
            background: #0275d8 !important;
            color: #fff !important;
        }
    </style>
    <div class="card card-block">
        <div class="container">
            <h3>THỐNG KÊ DỰ GIỜ</h3>
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <div class="search">
                        Từ tháng: 
                        <select id="slTuThang" name="cars">
                            <option value="0">---Chọn tháng---</option>
                            <option value="9">Tháng 9/2020</option>
                            <option value="10">Tháng 10/2020</option>
                            <option value="11">Tháng 11/2020</option>
                            <option value="12">Tháng 12/2020</option>
                        </select>
                        đến tháng: 
                        <select id="slDenThang" name="cars">
                            <option value="0">---Chọn tháng---</option>
                            <option value="9">Tháng 9/2020</option>
                            <option value="10">Tháng 10/2020</option>
                            <option value="11">Tháng 11/2020</option>
                            <option value="12">Tháng 12/2020</option>
                        </select>
                        <a href="javascript:void(0)" class="btn btn-primary" runat="server" id="btnXem" onclick="getValue()" onserverclick="btnXem_ServerClick">Xem</a>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 ">
                            <ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#profile" role="tab" data-toggle="tab">Danh sách Giáo viên tham gia dự giờ</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#buzz" role="tab" data-toggle="tab">Danh sách Giáo viên bị dự giờ</a>
                                </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="profile">
                                    <h3 class="m-2">DANH SÁCH GIÁO VIÊN THAM GIA ĐĂNG KÍ DỰ GIỜ</h3>
                                    <div class="col-6">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr class="head_table">
                                                    <th>STT</th>
                                                    <th>Họ tên</th>
                                                    <th>Số lần đăng kí</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater runat="server" ID="rpGVThamGiaDuGio">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td style="text-align: center"><%=STT++ %></td>
                                                            <td class="tiet"><%#Eval("username_fullname") %></td>
                                                            <td class="tiet"><%#Eval("count") %></td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                <tr runat="server" id="txtNote1">
                                                    <td colspan="3" class="text-danger text-sm-center font-weight-bold">Không có dữ liệu</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="buzz">
                                    <h3 class="m-2">DANH SÁCH GIÁO VIÊN THAM BỊ ĐĂNG KÍ DỰ GIỜ</h3>
                                    <div class="col-6">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr class="head_table">
                                                    <th>STT</th>
                                                    <th>Họ tên</th>
                                                    <th>Số lần đăng kí</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <asp:Repeater runat="server" ID="rpGVBiDuGio">
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td style="text-align: center"><%=stt++ %></td>
                                                            <td class="tiet"><%#Eval("username_fullname") %></td>
                                                            <td class="tiet"><%#Eval("count") %></td>
                                                        </tr>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                                <tr runat="server" id="txtNote2">
                                                    <td colspan="3" class="text-danger text-sm-center font-weight-bold">Không có dữ liệu</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <input type="text" runat="server" id="txtTuthang" hidden />
            <input type="text" runat="server" id="txtDenthang" hidden />
        </div>
    </div>
    <script>
        function getValue() {
            var e = document.getElementById("slTuThang");
            var strUser = e.options[e.selectedIndex].value;
            document.getElementById('<%=txtTuthang.ClientID%>').value = strUser;
            var x = document.getElementById("slDenThang");
            var value = x.options[x.selectedIndex].value;
            document.getElementById('<%=txtDenthang.ClientID%>').value = value;
        }
        function setSelected() {
            var a = document.getElementById('<%=txtTuthang.ClientID%>').value;
            var b = document.getElementById('<%=txtDenthang.ClientID%>').value;
            var $option = $('#slTuThang').children('option[value="' + a + '"]');
            $option.attr('selected', true);
            var $option1 = $('#slDenThang').children('option[value="' + b + '"]');
            $option1.attr('selected', true);
        }
    </script>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

