﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_LichCongTac_module_ThongKeGiaoVienDuGio : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public int STT = 1;
    public int stt = 1;
    cls_Alert alert = new cls_Alert();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            var getDSGVThamGiaDuGio = from s in db.tbHocTap_DuGios
                                      join u in db.admin_Users on s.nguoidugio_id equals u.username_id
                                      group u by u.username_id into g
                                      select new
                                      {
                                          g.Key,
                                          username_fullname = g.First().username_fullname,
                                          count = (from s in db.tbHocTap_DuGios
                                                   join u in db.admin_Users on s.nguoidugio_id equals u.username_id
                                                   where s.nguoidugio_id == g.Key
                                                   select s).Count()
                                      };
            rpGVThamGiaDuGio.DataSource = getDSGVThamGiaDuGio;
            rpGVThamGiaDuGio.DataBind();
            if (getDSGVThamGiaDuGio.Count() == 0)
                txtNote1.Visible = true;
            else
                txtNote1.Visible = false;
            var getDSGVBiDuGio = from s in db.tbHocTap_DuGios
                                 join u in db.admin_Users on s.nguoibidugio_id equals u.username_id
                                 group u by u.username_id into g
                                 select new
                                 {
                                     g.Key,
                                     username_fullname = g.First().username_fullname,
                                     count = (from s in db.tbHocTap_DuGios
                                              join u in db.admin_Users on s.nguoibidugio_id equals u.username_id
                                              where s.nguoibidugio_id == g.Key
                                              select s).Count()
                                 };
            rpGVBiDuGio.DataSource = getDSGVBiDuGio;
            rpGVBiDuGio.DataBind();
            if (getDSGVBiDuGio.Count() == 0)
                txtNote2.Visible = true;
            else
                txtNote2.Visible = false;
        }
    }

    protected void btnXem_ServerClick(object sender, EventArgs e)
    {
        if (Convert.ToInt32(txtTuthang.Value) == 0 || Convert.ToInt32(txtDenthang.Value) == 0)
        {
            if (Convert.ToInt32(txtTuthang.Value) == 0)
                alert.alert_Warning(Page, "Vui lòng chọn thời gian bắt đầu", "");
            else
                alert.alert_Warning(Page, "Vui lòng chọn thời gian kết thúc", "");
        }
        else
        {
            var getDSGVThamGiaDuGio = from s in db.tbHocTap_DuGios
                                      join u in db.admin_Users on s.nguoidugio_id equals u.username_id
                                      where s.dugio_ngaydangky.Value.Month >= Convert.ToInt32(txtTuthang.Value)
                                      && s.dugio_ngaydangky.Value.Month <= Convert.ToInt32(txtDenthang.Value)
                                      group u by u.username_id into g
                                      select new
                                      {
                                          g.Key,
                                          username_fullname = g.First().username_fullname,
                                          count = (from s in db.tbHocTap_DuGios
                                                   join u in db.admin_Users on s.nguoidugio_id equals u.username_id
                                                   where s.nguoidugio_id == g.Key && s.dugio_ngaydangky.Value.Month >= Convert.ToInt32(txtTuthang.Value)
                                                    && s.dugio_ngaydangky.Value.Month <= Convert.ToInt32(txtDenthang.Value)
                                                   select s).Count()
                                      };
            rpGVThamGiaDuGio.DataSource = getDSGVThamGiaDuGio;
            rpGVThamGiaDuGio.DataBind();
            if (getDSGVThamGiaDuGio.Count() == 0)
                txtNote1.Visible = true;
            else
                txtNote1.Visible = false;
            var getDSGVBiDuGio = from s in db.tbHocTap_DuGios
                                 join u in db.admin_Users on s.nguoibidugio_id equals u.username_id
                                 where s.dugio_ngaydangky.Value.Month >= Convert.ToInt32(txtTuthang.Value)
                                  && s.dugio_ngaydangky.Value.Month <= Convert.ToInt32(txtDenthang.Value)
                                 group u by u.username_id into g
                                 select new
                                 {
                                     g.Key,
                                     username_fullname = g.First().username_fullname,
                                     count = (from s in db.tbHocTap_DuGios
                                              join u in db.admin_Users on s.nguoibidugio_id equals u.username_id
                                              where s.nguoibidugio_id == g.Key && s.dugio_ngaydangky.Value.Month >= Convert.ToInt32(txtTuthang.Value)
                                               && s.dugio_ngaydangky.Value.Month <= Convert.ToInt32(txtDenthang.Value)
                                              select s).Count()
                                 };
            rpGVBiDuGio.DataSource = getDSGVBiDuGio;
            rpGVBiDuGio.DataBind();
            if (getDSGVBiDuGio.Count() == 0)
                txtNote2.Visible = true;
            else
                txtNote2.Visible = false;
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "text", "setSelected()", true);
        }
    }
}