﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_LichCongTac_module_XemDanhSachDangKyDuGio : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public int STT = 1;
    protected void Page_Load(object sender, EventArgs e)
    {
        loadData();
    }
    private void loadData()
    {
        var getData = from ct in db.tbLichBaoGiangChiTiets
                      join t in db.tbLichBaoGiangTheoTuans on ct.lichbaogiangtheotuan_id equals t.lichbaogiangtheotuan_id
                      join a in db.tbHocTap_DuGios on ct.lichbaogiangchitiet_id equals a.lichbaogiangchitiet_id
                      join u in db.admin_Users on a.nguoidugio_id equals u.username_id
                      where a.hidden == false
                      && u.username_username == Request.Cookies["UserName"].Value
                      select new
                      {
                          ct.lichbaogiangchitiet_id,
                          ct.lichbaogiangchitiet_monhoc,
                          ct.lichbaogiangchitiet_tenbaigiang,
                          ct.lichbaogiangchitiet_tiethoc,
                          ct.lichbaogiangchitiet_lop,
                          t.lichbaogiangtheotuan_thuhoc,
                          t.lichbaogiangtheotuan_ngayhoc
                      };
        grvList.DataSource = getData;
        grvList.DataBind();
    }
}