﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_LichCongTac_module_XemDanhSachDuGio : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    private int count = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        //btnChiTiet.Visible = false;
        loadData();
    }
    private void loadData()
    {
        //lấy thông tin của tk login
        var getuser = (from u in db.admin_Users
                       where u.username_username == Request.Cookies["UserName"].Value
                       select u).FirstOrDefault();
        DataTable dt = new DataTable();
        dt.Columns.Add("lichbaogiangchitiet_id", typeof(int));
        dt.Columns.Add("lichbaogiangtheotuan_thuhoc", typeof(string));
        dt.Columns.Add("lichbaogiangchitiet_tiethoc", typeof(string));
        dt.Columns.Add("lichbaogiangtheotuan_ngayhoc", typeof(string));
        dt.Columns.Add("lichbaogiangchitiet_monhoc", typeof(string));
        dt.Columns.Add("lichbaogiangchitiet_tenbaigiang", typeof(string));
        dt.Columns.Add("songuoidangky", typeof(int));
        var getData = from ct in db.tbLichBaoGiangChiTiets
                      join t in db.tbLichBaoGiangTheoTuans on ct.lichbaogiangtheotuan_id equals t.lichbaogiangtheotuan_id
                      join a in db.tbHocTap_DuGios on ct.lichbaogiangchitiet_id equals a.lichbaogiangchitiet_id
                      where a.hidden == false && a.nguoibidugio_id == getuser.username_id
                      && t.lichbaogiangtheotuan_ngayhoc >= DateTime.Now
                      select new
                      {
                          
                          ct.lichbaogiangchitiet_id,
                          ct.lichbaogiangchitiet_monhoc,
                          ct.lichbaogiangchitiet_tenbaigiang,
                          ct.lichbaogiangchitiet_tiethoc,
                          t.lichbaogiangtheotuan_thuhoc,
                          t.lichbaogiangtheotuan_ngayhoc
                      };
        if (getData.Count()==0)
        {

        }
        else
        {
            foreach (var item in getData)
            {
                //nếu datatable chưa có dòng nào thì thêm
                if (dt.Rows.Count == 0)
                {
                    DataRow row = dt.NewRow();
                    row["lichbaogiangchitiet_id"] = item.lichbaogiangchitiet_id;
                    row["lichbaogiangtheotuan_thuhoc"] = item.lichbaogiangtheotuan_thuhoc;
                    row["lichbaogiangchitiet_tiethoc"] = item.lichbaogiangchitiet_tiethoc;
                    row["lichbaogiangtheotuan_ngayhoc"] = item.lichbaogiangtheotuan_ngayhoc.Value.ToString("dd/MM/yyyy").Replace(' ', 'T');
                    row["lichbaogiangchitiet_monhoc"] = item.lichbaogiangchitiet_monhoc;
                    row["lichbaogiangchitiet_tenbaigiang"] = item.lichbaogiangchitiet_tenbaigiang;
                    row["songuoidangky"] = count + 1;
                    dt.Rows.Add(row);
                }
                //nếu có rồi thì kiểm tra id nếu trùng thì count +1
                else
                {
                    //int lichbaogiangchitiet_id = row["lichbaogiangchitiet_id"].ToString();
                    DataRow row_id = dt.Select("lichbaogiangchitiet_id = '" + item.lichbaogiangchitiet_id + "'").FirstOrDefault();
                    if (row_id != null)
                    {
                        row_id["songuoidangky"] = Convert.ToInt32(row_id["songuoidangky"]) + 1;
                    }
                    else
                    {
                        DataRow row2 = dt.NewRow();
                        row2["lichbaogiangchitiet_id"] = item.lichbaogiangchitiet_id;
                        row2["lichbaogiangtheotuan_thuhoc"] = item.lichbaogiangtheotuan_thuhoc;
                        row2["lichbaogiangchitiet_tiethoc"] = item.lichbaogiangchitiet_tiethoc;
                        row2["lichbaogiangtheotuan_ngayhoc"] = item.lichbaogiangtheotuan_ngayhoc.Value.ToString("dd/MM/yyyy").Replace(' ', 'T');
                        row2["lichbaogiangchitiet_monhoc"] = item.lichbaogiangchitiet_monhoc;
                        row2["lichbaogiangchitiet_tenbaigiang"] = item.lichbaogiangchitiet_tenbaigiang;
                        row2["songuoidangky"] = count + 1;
                        dt.Rows.Add(row2);
                    }
                }
            }
            grvList.DataSource = dt;
            grvList.DataBind();
        }
    }

    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "lichbaogiangchitiet_id" }));
        //alert.alert_Warning(Page,""+_id,"");

        var getData = from ct in db.tbLichBaoGiangChiTiets
                       join a in db.tbHocTap_DuGios on ct.lichbaogiangchitiet_id equals a.lichbaogiangchitiet_id
                       join u in db.admin_Users on a.nguoidugio_id equals u.username_id
                       where ct.lichbaogiangchitiet_id == _id
                       select new
                       {
                           a.dugio_id,
                           ct.lichbaogiangchitiet_tenbaigiang,
                           u.username_fullname
                       };
        txtTenBaiGiang.InnerText = getData.FirstOrDefault().lichbaogiangchitiet_tenbaigiang;
        grvListChiTiet.DataSource = getData;
        grvListChiTiet.DataBind();
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupControl.Show(); ", true);
    }
    protected void btnXoa_Click(object sender, EventArgs e)
    {
        cls_DuGio cls;
        List<object> selectedKey = grvListChiTiet.GetSelectedFieldValues(new string[] { "dugio_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                cls = new cls_DuGio();
                if (cls.Linq_Xoa(Convert.ToInt32(item)))
                    alert.alert_Success(Page, "Xóa thành công", "");
                else
                    alert.alert_Error(Page, "Xóa thất bại", "");
            }
        }
        else
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
        popupControl.ShowOnPageLoad = false;
    }
}