﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_XemDanhSachLichCongTacChiTiet.aspx.cs" Inherits="admin_page_module_function_module_XemDanhSachLichCongTacChiTiet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="card card-block">
        <asp:Repeater ID="rpLichCongtac" runat="server" OnItemDataBound="rpLichCongtac_ItemDataBound">
            <ItemTemplate>
                <div class="form-group table-responsive">
                    <div style="float: right">
                        <asp:Button ID="btnQuayLai" runat="server" Text="Quay lại" CssClass="btn btn-primary" OnClick="btnQuayLai_Click" />
                    </div>
                    <br />
                    <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SỞ GIÁO DỤC & ĐÀO TẠO ĐÀ NẴNG</b>
                    <br />
                    <b>TRƯỜNG TIỂU HỌC - TRUNG HỌC CƠ SỞ & TRUNG HỌC PHỔ THÔNG </b>
                    <br />
                    <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;VIỆT NHẬT</b>
                    <br />
                    <br />
                    <div style="text-align: center">
                        <h3><b>LỊCH CÔNG TÁC HÀNG TUẦN </b></h3>
                        <br />
                        <div style="font-weight: bold;">
                            Tuần
               <%#Eval("lichcongtac_tuan") %>
                        Từ ngày
              <%#Convert.ToDateTime(Eval("lichcongtac_tungay")).ToShortDateString()%>
                        đến ngày
              <%#Convert.ToDateTime(Eval("lichcongtac_denngay")).ToShortDateString()%>
                        </div>
                    </div>
                </div>
                <div>
                    <table>
                        <tr>
                            <th style="text-align: center">Thứ/Ngày</th>
                            <th style="text-align: center">Nội dung công việc</th>
                            <th style="text-align: center">Ghi chú</th>
                        </tr>
                        <asp:Repeater ID="rpLichCongTacChiTiet" runat="server">
                            <ItemTemplate>
                                <tr>
                                    <td style="text-align: center"><%#Eval("lichcongtacchitiet_thu") %><br />
                                        <%#Eval("lichcongtacchitiet_ngay") %>
                                    </td>
                                    <td>
                                        <textarea readonly="readonly" style="width: 100%" rows="4" cols="50"><%#Eval("lichcongtacchitiet_noidung") %></textarea>
                                        <%-- <%#Eval("lichcongtacchitiet_noidung") %>--%>
                                        <td>
                                            <textarea readonly="readonly" style="width: 100%" rows="4" cols="50"><%#Eval("lichcongtacchitiet_ghichu") %></textarea>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                    <%--<table>
                <tr>
                    <th style="text-align: center">Thứ/Ngày</th>
                    <th style="text-align: center">Nội dung công việc</th>
                    <th style="text-align: center">Ghi chú</th>
                </tr>
                <tr>
                    <td>Thứ 2
                         <br />
                        <input type="text" value="" placeholder="dd/mm" runat="server" id="txtThu2" style="width: 60px" />
                    </td>

                    <td>
                        <textarea id="txtNoiDung1" runat="server" rows="3" cols="50"></textarea></td>
                    <td>
                        <textarea id="txtGhiChu1" runat="server" rows="3" cols="50"></textarea></td>
                </tr>
              
            </table>--%>
                </div>
            </ItemTemplate>
        </asp:Repeater>
        <div>
            Xem các tuần trước
            <asp:DropDownList ID="ddlTuanTruoc" runat="server"></asp:DropDownList>
            <asp:Button ID="btnXem" runat="server" Text="Xem" CssClass="btn btn-primary" OnClick="btnXem_Click" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

