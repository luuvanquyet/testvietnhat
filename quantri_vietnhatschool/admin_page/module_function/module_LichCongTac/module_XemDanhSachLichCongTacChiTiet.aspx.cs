﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_XemDanhSachLichCongTacChiTiet : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            var listTuan = from tuan in db.tbLichCongTacs orderby tuan.lichcongtac_id descending where tuan.lichcongtac_hidden==null select tuan;
            ddlTuanTruoc.DataSource = listTuan;
            ddlTuanTruoc.DataTextField = "lichcongtac_tuan";
            ddlTuanTruoc.DataValueField = "lichcongtac_id";
            ddlTuanTruoc.DataBind();
        }
        
        rpLichCongtac.DataSource = from lct in db.tbLichCongTacs where lct.lichcongtac_id== Convert.ToInt32(ddlTuanTruoc.SelectedValue) && lct.lichcongtac_hidden==null select lct;
        rpLichCongtac.DataBind();
       
    }


    protected void rpLichCongtac_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Repeater rpLichCongTacChiTiet = e.Item.FindControl("rpLichCongTacChiTiet") as Repeater;
        int lichcongtac_id = int.Parse(DataBinder.Eval(e.Item.DataItem, "lichcongtac_id").ToString());
        rpLichCongTacChiTiet.DataSource = from lctct in db.tbLichCongtacChiTiets where lctct.lichcongtac_id == lichcongtac_id select lctct;
        rpLichCongTacChiTiet.DataBind();
    }

    protected void btnQuayLai_Click(object sender, EventArgs e)
    {
        Response.Redirect("/admin-home");
    }

    protected void btnXem_Click(object sender, EventArgs e)
    {
        rpLichCongtac.DataSource = from lct in db.tbLichCongTacs where lct.lichcongtac_id == Convert.ToInt32(ddlTuanTruoc.SelectedValue) && lct.lichcongtac_hidden==null select lct;
        rpLichCongtac.DataBind();
    }
}