﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_XemDanhSachLichCongTacThang.aspx.cs" Inherits="admin_page_module_function_module_XemDanhSachLichCongTacThang" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
   
    <div class="card card-block">
        <div style="float: right">
            <a href="/admin-home" class="btn btn-primary">Quay lại</a>
        </div>
        <div class="div_content col-12">
            <asp:Literal ID="ltEmbed" runat="server" />
        </div>

        <%-- </div>
      <asp:Repeater ID="rpLichCongtac" runat="server">
            <ItemTemplate>
                <div>
                    <%#Eval("lichcongtac_thang_content") %>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    <div>--%>
        Xem các tháng trước
            <asp:DropDownList ID="ddlTuanTruoc" runat="server"></asp:DropDownList>
        <asp:Button ID="btnXem" runat="server" Text="Xem" CssClass="btn btn-primary" OnClick="btnXem_Click" />
  </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

