﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_XemDanhSachLichCongTacThang : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            var listTuan = from tuan in db.tbLichCongTac_Thangs orderby tuan.lichcongtac_thang_id descending select tuan;
            ddlTuanTruoc.DataSource = listTuan;
            ddlTuanTruoc.DataTextField = "lichcongtac_thang_title";
            ddlTuanTruoc.DataValueField = "lichcongtac_thang_id";
            ddlTuanTruoc.DataBind();
        }
        var getData = (from lct in db.tbLichCongTac_Thangs where lct.lichcongtac_thang_id == Convert.ToInt32(ddlTuanTruoc.SelectedValue) select lct).Single();
        string embed = "<object data=\"{0}\" type=\"application/pdf\" width=\"100%\" height=\"700px\">";
        embed += "If you are unable to view file, you can download from <a href = \"{0}\">here</a>";
        embed += " or download <a target = \"_blank\" href = \"http://get.adobe.com/reader/\">Adobe PDF Reader</a> to view the file.";
        embed += "</object>";
        ltEmbed.Text = string.Format(embed, ResolveUrl(getData.lichcongtac_thang_content));

    }
    protected void btnQuayLai_Click(object sender, EventArgs e)
    {
        Response.Redirect("/admin-home");
    }

    protected void btnXem_Click(object sender, EventArgs e)
    {
        var getData = (from lct in db.tbLichCongTac_Thangs where lct.lichcongtac_thang_id == Convert.ToInt32(ddlTuanTruoc.SelectedValue) select lct).Single();
        string embed = "<object data=\"{0}\" type=\"application/pdf\" width=\"100%\" height=\"700px\">";
        embed += "If you are unable to view file, you can download from <a href = \"{0}\">here</a>";
        embed += " or download <a target = \"_blank\" href = \"http://get.adobe.com/reader/\">Adobe PDF Reader</a> to view the file.";
        embed += "</object>";
        ltEmbed.Text = string.Format(embed, ResolveUrl(getData.lichcongtac_thang_content));
    }
}