﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_XemLichBaoGiangTuNhien.aspx.cs" Inherits="admin_page_module_function_module_WebSite_module_XemLichBaoGiangTuNhien" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <%--    <style>
        .giaovien {
            margin: 10px 20px;
            border: 1px solid #808080;
         
        }
    </style>--%>
    <script>
        function btnCheck(id) {
            document.getElementById("<%=txtUser_id.ClientID%>").value = id;
            document.getElementById("<%=btnXemBaoGiangGiaoVien.ClientID%>").click();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="card card-block">
        <%-- <div class="col-12">
            <asp:Button ID="btnQuayLai" runat="server" Text="Quay lại" CssClass="btn btn-primary" OnClick="btnQuayLai_Click" />
        </div>--%>
        <asp:Repeater ID="rpGiaoVien" runat="server">
            <ItemTemplate>
                <a class="btn btn-primary" href="#" id="btnCheck" onclick="btnCheck(<%#Eval("username_id") %>)"><%#Eval("username_fullname") %></a>
                <%-- <asp:Button ID="btnXemBaoGiangGiaoVien" runat="server" Text="Xem" CssClass="btn btn-primary" OnClick="btnXemBaoGiangGiaoVien_ServerClick" />--%>
            </ItemTemplate>
        </asp:Repeater>
        <div style="display: none">
            <input id="txtUser_id" runat="server" type="text" />
            <a href="#" id="btnXemBaoGiangGiaoVien" runat="server" onserverclick="btnXemBaoGiangGiaoVien_ServerClick"></a>

        </div>
        <div id="dvKetQua" runat="server">
            <br />
            <br />
            <asp:Repeater ID="rpLichBaoGiang" runat="server">
                <ItemTemplate>
                    <div>
                        <p>
                            <b>SỞ GIÁO DỤC & ĐÀO TẠO ĐÀ NẴNG</b>
                            <br />
                            <b>TRƯỜNG TH-THCS & THPT VIỆT NHẬT</b>
                        </p>
                        <div style="text-align: center">
                            <h3><b>LỊCH BÁO GIẢNG - MÔN: <%#Eval("lichbaogiang_mon") %> </b></h3>
                            <br />
                            <div style="font-weight: bold;">
                                Tuần
               <%#Eval("lichbaigiang_tuan") %>
                        Từ ngày
                      
              <%#Convert.ToDateTime(Eval("lichbaogiang_tungay")).ToShortDateString()%>
                        đến ngày
                        
              <%#Convert.ToDateTime(Eval("lichbaogiang_denngay")).ToShortDateString()%>
                            </div>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
            <div style="overflow-x: auto;">

                <table class="table table-bordered">
                    <thead>
                        <tr class="head_table">
                            <th class="table_header" scope="col">Thứ</th>
                            <th scope="col">Buổi</th>
                            <th scope="col">Tiết</th>
                            <th id="hd_mon" runat="server" scope="col">Môn</th>
                            <th scope="col">Lớp</th>
                            <th scope="col">Tiết
                            <br />
                                (PPCT)</th>
                            <th scope="col">Tên bài dạy</th>
                            <th scope="col">Ghi chú</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td rowspan="8">
                                <span class="table_day">Thứ hai<br />
                                    <span runat="server" id="txtThu2_NgayHoc"></span>
                                </span>
                            </td>
                            <td rowspan="4">Sáng</td>
                            <td class="tiet" style="text-align: center">1</td>
                            <td class="tiet" runat="server" id="txtThu2Tiet1_Mon"></td>
                            <td class="tiet" runat="server" id="txtThu2Tiet1_Lop"></td>
                            <td class="tiet" runat="server" id="txtThu2Tiet1_TCT"></td>
                            <td runat="server" id="txtThu2Tiet1_TenBaiGiang"></td>
                            <td runat="server" id="txtThu2Tiet1_GhiChu"></td>
                        </tr>
                        <tr>
                            <td class="tiet" style="text-align: center">2</td>
                            <td class="tiet" runat="server" id="txtThu2Tiet2_Mon"></td>
                            <td class="tiet" runat="server" id="txtThu2Tiet2_Lop"></td>
                            <td class="tiet" runat="server" id="txtThu2Tiet2_TCT"></td>
                            <td runat="server" id="txtThu2Tiet2_TenBaiGiang"></td>
                            <td runat="server" id="txtThu2Tiet2_GhiChu"></td>
                        </tr>
                        <tr>
                            <td class="tiet" style="text-align: center">3</td>
                            <td class="tiet" runat="server" id="txtThu2Tiet3_Mon"></td>
                            <td class="tiet" runat="server" id="txtThu2Tiet3_Lop"></td>
                            <td class="tiet" runat="server" id="txtThu2Tiet3_TCT"></td>
                            <td runat="server" id="txtThu2Tiet3_TenBaiGiang"></td>
                            <td runat="server" id="txtThu2Tiet3_GhiChu"></td>
                        </tr>
                        <tr>
                            <td class="tiet" style="text-align: center">4</td>
                            <td class="tiet" runat="server" id="txtThu2Tiet4_Mon"></td>
                            <td class="tiet" runat="server" id="txtThu2Tiet4_Lop"></td>
                            <td class="tiet" runat="server" id="txtThu2Tiet4_TCT"></td>
                            <td runat="server" id="txtThu2Tiet4_TenBaiGiang"></td>
                            <td runat="server" id="txtThu2Tiet4_GhiChu"></td>
                        </tr>
                        <tr>
                            <td rowspan="4">Chiều</td>
                            <td class="tiet" style="text-align: center">5</td>
                            <td class="tiet" runat="server" id="txtThu2Tiet5_Mon"></td>
                            <td class="tiet" runat="server" id="txtThu2Tiet5_Lop"></td>
                            <td class="tiet" runat="server" id="txtThu2Tiet5_TCT"></td>
                            <td runat="server" id="txtThu2Tiet5_TenBaiGiang"></td>
                            <td runat="server" id="txtThu2Tiet5_GhiChu"></td>
                        </tr>
                        <tr>
                            <td class="tiet" style="text-align: center">6</td>
                            <td class="tiet" runat="server" id="txtThu2Tiet6_Mon"></td>
                            <td class="tiet" runat="server" id="txtThu2Tiet6_Lop"></td>
                            <td class="tiet" runat="server" id="txtThu2Tiet6_TCT"></td>
                            <td runat="server" id="txtThu2Tiet6_TenBaiGiang"></td>
                            <td runat="server" id="txtThu2Tiet6_GhiChu"></td>
                        </tr>
                        <tr>
                            <td class="tiet" style="text-align: center">7</td>
                            <td class="tiet" runat="server" id="txtThu2Tiet7_Mon"></td>
                            <td class="tiet" runat="server" id="txtThu2Tiet7_Lop"></td>
                            <td class="tiet" runat="server" id="txtThu2Tiet7_TCT"></td>
                            <td runat="server" id="txtThu2Tiet7_TenBaiGiang"></td>
                            <td runat="server" id="txtThu2Tiet7_GhiChu"></td>
                        </tr>
                        <tr>
                            <td class="tiet" style="text-align: center">8</td>
                            <td class="tiet" runat="server" id="txtThu2Tiet8_Mon"></td>
                            <td class="tiet" runat="server" id="txtThu2Tiet8_Lop"></td>
                            <td class="tiet" runat="server" id="txtThu2Tiet8_TCT"></td>
                            <td runat="server" id="txtThu2Tiet8_TenBaiGiang"></td>
                            <td runat="server" id="txtThu2Tiet8_GhiChu"></td>
                        </tr>
                        <tr>
                            <td rowspan="8">
                                <span class="table_day">Thứ Ba<br />
                                    <span runat="server" id="txtThu3_NgayHoc"></span>
                                </span>
                            </td>
                            <td rowspan="4">Sáng</td>
                            <td style="text-align: center">1</td>
                            <td runat="server" id="txtThu3Tiet1_Mon"></td>
                            <td class="tiet" runat="server" id="txtThu3Tiet1_Lop"></td>
                            <td runat="server" id="txtThu3Tiet1_TCT"></td>
                            <td runat="server" id="txtThu3Tiet1_TenBaiGiang"></td>
                            <td runat="server" id="txtThu3Tiet1_GhiChu"></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">2</td>
                            <td runat="server" id="txtThu3Tiet2_Mon"></td>
                            <td class="tiet" runat="server" id="txtThu3Tiet2_Lop"></td>
                            <td runat="server" id="txtThu3Tiet2_TCT"></td>
                            <td runat="server" id="txtThu3Tiet2_TenBaiGiang"></td>
                            <td runat="server" id="txtThu3Tiet2_GhiChu"></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">3</td>
                            <td runat="server" id="txtThu3Tiet3_Mon"></td>
                            <td class="tiet" runat="server" id="txtThu3Tiet3_Lop"></td>
                            <td runat="server" id="txtThu3Tiet3_TCT"></td>
                            <td runat="server" id="txtThu3Tiet3_TenBaiGiang"></td>
                            <td runat="server" id="txtThu3Tiet3_GhiChu"></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">4</td>
                            <td runat="server" id="txtThu3Tiet4_Mon"></td>
                            <td class="tiet" runat="server" id="txtThu3Tiet4_Lop"></td>
                            <td runat="server" id="txtThu3Tiet4_TCT"></td>
                            <td runat="server" id="txtThu3Tiet4_TenBaiGiang"></td>
                            <td runat="server" id="txtThu3Tiet4_GhiChu"></td>
                        </tr>
                        <tr>
                            <td rowspan="4">Chiều</td>
                            <td style="text-align: center">5</td>
                            <td runat="server" id="txtThu3Tiet5_Mon"></td>
                            <td class="tiet" runat="server" id="txtThu3Tiet5_Lop"></td>
                            <td runat="server" id="txtThu3Tiet5_TCT"></td>
                            <td runat="server" id="txtThu3Tiet5_TenBaiGiang"></td>
                            <td runat="server" id="txtThu3Tiet5_GhiChu"></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">6</td>
                            <td runat="server" id="txtThu3Tiet6_Mon"></td>
                            <td class="tiet" runat="server" id="txtThu3Tiet6_Lop"></td>
                            <td runat="server" id="txtThu3Tiet6_TCT"></td>
                            <td runat="server" id="txtThu3Tiet6_TenBaiGiang"></td>
                            <td runat="server" id="txtThu3Tiet6_GhiChu"></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">7</td>
                            <td runat="server" id="txtThu3Tiet7_Mon"></td>
                            <td class="tiet" runat="server" id="txtThu3Tiet7_Lop"></td>
                            <td runat="server" id="txtThu3Tiet7_TCT"></td>
                            <td runat="server" id="txtThu3Tiet7_TenBaiGiang"></td>
                            <td runat="server" id="txtThu3Tiet7_GhiChu"></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">8</td>
                            <td runat="server" id="txtThu3Tiet8_Mon"></td>
                            <td class="tiet" runat="server" id="txtThu3Tiet8_Lop"></td>
                            <td runat="server" id="txtThu3Tiet8_TCT"></td>
                            <td runat="server" id="txtThu3Tiet8_TenBaiGiang"></td>
                            <td runat="server" id="txtThu3Tiet8_GhiChu"></td>
                        </tr>
                        <tr>
                            <td rowspan="8">
                                <span class="table_day">Thứ Tư<br />
                                    <span runat="server" id="txtThu4_NgayHoc"></span>
                                </span>
                            </td>
                            <td rowspan="4">Sáng</td>
                            <td style="text-align: center">1</td>
                            <td runat="server" id="txtThu4Tiet1_Mon"></td>
                            <td class="tiet" runat="server" id="txtThu4Tiet1_Lop"></td>
                            <td runat="server" id="txtThu4Tiet1_TCT"></td>
                            <td runat="server" id="txtThu4Tiet1_TenBaiGiang"></td>
                            <td runat="server" id="txtThu4Tiet1_GhiChu"></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">2</td>
                            <td runat="server" id="txtThu4Tiet2_Mon"></td>
                            <td class="tiet" runat="server" id="txtThu4Tiet2_Lop"></td>
                            <td runat="server" id="txtThu4Tiet2_TCT"></td>
                            <td runat="server" id="txtThu4Tiet2_TenBaiGiang"></td>
                            <td runat="server" id="txtThu4Tiet2_GhiChu"></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">3</td>
                            <td runat="server" id="txtThu4Tiet3_Mon"></td>
                            <td class="tiet" runat="server" id="txtThu4Tiet3_Lop"></td>
                            <td runat="server" id="txtThu4Tiet3_TCT"></td>
                            <td runat="server" id="txtThu4Tiet3_TenBaiGiang"></td>
                            <td runat="server" id="txtThu4Tiet3_GhiChu"></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">4</td>
                            <td runat="server" id="txtThu4Tiet4_Mon"></td>
                            <td class="tiet" runat="server" id="txtThu4Tiet4_Lop"></td>
                            <td runat="server" id="txtThu4Tiet4_TCT"></td>
                            <td runat="server" id="txtThu4Tiet4_TenBaiGiang"></td>
                            <td runat="server" id="txtThu4Tiet4_GhiChu"></td>
                        </tr>
                        <tr>
                            <td rowspan="4">Chiều</td>
                            <td style="text-align: center">5</td>
                            <td runat="server" id="txtThu4Tiet5_Mon"></td>
                            <td class="tiet" runat="server" id="txtThu4Tiet5_Lop"></td>
                            <td runat="server" id="txtThu4Tiet5_TCT"></td>
                            <td runat="server" id="txtThu4Tiet5_TenBaiGiang"></td>
                            <td runat="server" id="txtThu4Tiet5_GhiChu"></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">6</td>
                            <td runat="server" id="txtThu4Tiet6_Mon"></td>
                            <td class="tiet" runat="server" id="txtThu4Tiet6_Lop"></td>
                            <td runat="server" id="txtThu4Tiet6_TCT"></td>
                            <td runat="server" id="txtThu4Tiet6_TenBaiGiang"></td>
                            <td runat="server" id="txtThu4Tiet6_GhiChu"></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">7</td>
                            <td runat="server" id="txtThu4Tiet7_Mon"></td>
                            <td class="tiet" runat="server" id="txtThu4Tiet7_Lop"></td>
                            <td runat="server" id="txtThu4Tiet7_TCT"></td>
                            <td runat="server" id="txtThu4Tiet7_TenBaiGiang"></td>
                            <td runat="server" id="txtThu4Tiet7_GhiChu"></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">8</td>
                            <td runat="server" id="txtThu4Tiet8_Mon"></td>
                            <td class="tiet" runat="server" id="txtThu4Tiet8_Lop"></td>
                            <td runat="server" id="txtThu4Tiet8_TCT"></td>
                            <td runat="server" id="txtThu4Tiet8_TenBaiGiang"></td>
                            <td runat="server" id="txtThu4Tiet8_GhiChu"></td>
                        </tr>
                        <tr>
                            <td rowspan="8">
                                <span class="table_day">Thứ Năm<br />
                                    <span runat="server" id="txtThu5_NgayHoc"></span>
                                </span>
                            </td>
                            <td rowspan="4">Sáng</td>
                            <td style="text-align: center">1</td>
                            <td runat="server" id="txtThu5Tiet1_Mon"></td>
                            <td class="tiet" runat="server" id="txtThu5Tiet1_Lop"></td>
                            <td runat="server" id="txtThu5Tiet1_TCT"></td>
                            <td runat="server" id="txtThu5Tiet1_TenBaiGiang"></td>
                            <td runat="server" id="txtThu5Tiet1_GhiChu"></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">2</td>
                            <td runat="server" id="txtThu5Tiet2_Mon"></td>
                            <td class="tiet" runat="server" id="txtThu5Tiet2_Lop"></td>
                            <td runat="server" id="txtThu5Tiet2_TCT"></td>
                            <td runat="server" id="txtThu5Tiet2_TenBaiGiang"></td>
                            <td runat="server" id="txtThu5Tiet2_GhiChu"></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">3</td>
                            <td runat="server" id="txtThu5Tiet3_Mon"></td>
                            <td class="tiet" runat="server" id="txtThu5Tiet3_Lop"></td>
                            <td runat="server" id="txtThu5Tiet3_TCT"></td>
                            <td runat="server" id="txtThu5Tiet3_TenBaiGiang"></td>
                            <td runat="server" id="txtThu5Tiet3_GhiChu"></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">4</td>
                            <td runat="server" id="txtThu5Tiet4_Mon"></td>
                            <td class="tiet" runat="server" id="txtThu5Tiet4_Lop"></td>
                            <td runat="server" id="txtThu5Tiet4_TCT"></td>
                            <td runat="server" id="txtThu5Tiet4_TenBaiGiang"></td>
                            <td runat="server" id="txtThu5Tiet4_GhiChu"></td>
                        </tr>
                        <tr>
                            <td rowspan="4">Chiều</td>
                            <td style="text-align: center">5</td>
                            <td runat="server" id="txtThu5Tiet5_Mon"></td>
                            <td class="tiet" runat="server" id="txtThu5Tiet5_Lop"></td>
                            <td runat="server" id="txtThu5Tiet5_TCT"></td>
                            <td runat="server" id="txtThu5Tiet5_TenBaiGiang"></td>
                            <td runat="server" id="txtThu5Tiet5_GhiChu"></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">6</td>
                            <td runat="server" id="txtThu5Tiet6_Mon"></td>
                            <td class="tiet" runat="server" id="txtThu5Tiet6_Lop"></td>
                            <td runat="server" id="txtThu5Tiet6_TCT"></td>
                            <td runat="server" id="txtThu5Tiet6_TenBaiGiang"></td>
                            <td runat="server" id="txtThu5Tiet6_GhiChu"></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">7</td>
                            <td runat="server" id="txtThu5Tiet7_Mon"></td>
                            <td class="tiet" runat="server" id="txtThu5Tiet7_Lop"></td>
                            <td runat="server" id="txtThu5Tiet7_TCT"></td>
                            <td runat="server" id="txtThu5Tiet7_TenBaiGiang"></td>
                            <td runat="server" id="txtThu5Tiet7_GhiChu"></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">8</td>
                            <td runat="server" id="txtThu5Tiet8_Mon"></td>
                            <td class="tiet" runat="server" id="txtThu5Tiet8_Lop"></td>
                            <td runat="server" id="txtThu5Tiet8_TCT"></td>
                            <td runat="server" id="txtThu5Tiet8_TenBaiGiang"></td>
                            <td runat="server" id="txtThu5Tiet8_GhiChu"></td>
                        </tr>
                        <tr>
                            <td rowspan="8">
                                <span class="table_day">Thứ Sáu<br />
                                    <span runat="server" id="txtThu6_NgayHoc"></span>
                                </span>
                            </td>
                            <td rowspan="4">Sáng</td>
                            <td style="text-align: center">1</td>
                            <td runat="server" id="txtThu6Tiet1_Mon"></td>
                            <td class="tiet" runat="server" id="txtThu6Tiet1_Lop"></td>
                            <td runat="server" id="txtThu6Tiet1_TCT"></td>
                            <td runat="server" id="txtThu6Tiet1_TenBaiGiang"></td>
                            <td runat="server" id="txtThu6Tiet1_GhiChu"></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">2</td>
                            <td runat="server" id="txtThu6Tiet2_Mon"></td>
                            <td class="tiet" runat="server" id="txtThu6Tiet2_Lop"></td>
                            <td runat="server" id="txtThu6Tiet2_TCT"></td>
                            <td runat="server" id="txtThu6Tiet2_TenBaiGiang"></td>
                            <td runat="server" id="txtThu6Tiet2_GhiChu"></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">3</td>
                            <td runat="server" id="txtThu6Tiet3_Mon"></td>
                            <td class="tiet" runat="server" id="txtThu6Tiet3_Lop"></td>
                            <td runat="server" id="txtThu6Tiet3_TCT"></td>
                            <td runat="server" id="txtThu6Tiet3_TenBaiGiang"></td>
                            <td runat="server" id="txtThu6Tiet3_GhiChu"></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">4</td>
                            <td runat="server" id="txtThu6Tiet4_Mon"></td>
                            <td class="tiet" runat="server" id="txtThu6Tiet4_Lop"></td>
                            <td runat="server" id="txtThu6Tiet4_TCT"></td>
                            <td runat="server" id="txtThu6Tiet4_TenBaiGiang"></td>
                            <td runat="server" id="txtThu6Tiet4_GhiChu"></td>
                        </tr>
                        <tr>
                            <td rowspan="4">Chiều</td>
                            <td style="text-align: center">5</td>
                            <td runat="server" id="txtThu6Tiet5_Mon"></td>
                            <td class="tiet" runat="server" id="txtThu6Tiet5_Lop"></td>
                            <td runat="server" id="txtThu6Tiet5_TCT"></td>
                            <td runat="server" id="txtThu6Tiet5_TenBaiGiang"></td>
                            <td runat="server" id="txtThu6Tiet5_GhiChu"></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">6</td>
                            <td runat="server" id="txtThu6Tiet6_Mon"></td>
                            <td class="tiet" runat="server" id="txtThu6Tiet6_Lop"></td>
                            <td runat="server" id="txtThu6Tiet6_TCT"></td>
                            <td runat="server" id="txtThu6Tiet6_TenBaiGiang"></td>
                            <td runat="server" id="txtThu6Tiet6_GhiChu"></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">7</td>
                            <td runat="server" id="txtThu6Tiet7_Mon"></td>
                            <td class="tiet" runat="server" id="txtThu6Tiet7_Lop"></td>
                            <td runat="server" id="txtThu6Tiet7_TCT"></td>
                            <td runat="server" id="txtThu6Tiet7_TenBaiGiang"></td>
                            <td runat="server" id="txtThu6Tiet7_GhiChu"></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">8</td>
                            <td runat="server" id="txtThu6Tiet8_Mon"></td>
                            <td class="tiet" runat="server" id="txtThu6Tiet8_Lop"></td>
                            <td runat="server" id="txtThu6Tiet8_TCT"></td>
                            <td runat="server" id="txtThu6Tiet8_TenBaiGiang"></td>
                            <td runat="server" id="txtThu6Tiet8_GhiChu"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div>
                Xem các tuần trước
                    <asp:DropDownList ID="ddlLichTuan" runat="server"></asp:DropDownList>
                <asp:Button ID="Button1" runat="server" Text="Xem" CssClass="btn btn-primary" OnClick="btnXem_Click" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

