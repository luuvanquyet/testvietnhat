﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_WebSite_module_XemLichBaoGiangtheDuc : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id = 15;
    int tuanbaoGiang;
    int user_id;

    protected void Page_Load(object sender, EventArgs e)
    {
        //_id = Convert.ToInt32(RouteData.Values["id"]);
        if (!IsPostBack)
        {
            if (txtUser_id.Value != "")
            {
                var listTuan = from tuan in db.tbLichBaoGiangs
                               where tuan.khoi_id == _id && tuan.username_id == Convert.ToInt32(txtUser_id.Value)
                               orderby tuan.lichbaogiang_id descending
                               select tuan;
                if (listTuan.Count() > 0)
                {
                    ddlLichTuan.DataSource = listTuan;
                    ddlLichTuan.DataTextField = "lichbaigiang_tuan";
                    ddlLichTuan.DataValueField = "lichbaogiang_id";
                    ddlLichTuan.DataBind();
                }
            }
            dvKetQua.Visible = false;
            //tuanbaoGiang = Convert.ToInt32(listTuan.First().lichbaigiang_tuan);
            //loadData(_id, tuanbaoGiang);
        }
        var loadGiaoVien = from gv in db.admin_Users
                           join lbg in db.tbLichBaoGiangs on gv.username_id equals lbg.username_id
                           where lbg.khoi_id == _id
                           group gv by gv.username_id into item
                           select new
                           {
                               username_fullname = item.First().username_fullname,
                               username_id = item.Key
                           };
        rpGiaoVien.DataSource = loadGiaoVien;
        rpGiaoVien.DataBind();

    }
    private void loadData(int khoi, int tuan, int magiaovien)
    {
       
        //lấy thông tin của tk đăng nhập
        //var getuser = (from u in db.admin_Users
        //               where u.username_username == Request.Cookies["UserName"].Value
        //               select u).FirstOrDefault();

        //lấy thông tin của lịch báo giảng được chọn
        dvKetQua.Visible = true;
        var getData = (from lbg in db.tbLichBaoGiangs
                       where lbg.khoi_id == _id && lbg.tuan_id == tuan && lbg.username_id == magiaovien
                       select lbg);
        rpLichBaoGiang.DataSource = getData.Take(1);
        rpLichBaoGiang.DataBind();
        var getTheoTuan = (from lbgtt in db.tbLichBaoGiangTheoTuans
                           where lbgtt.lichbaogiang_id == getData.First().lichbaogiang_id
                           select lbgtt);
        //lấy thông tin của thứ 2 từ bảng chi tiết
        var getChiTiet = from lbgct in db.tbLichBaoGiangChiTiets
                         where lbgct.lichbaogiangtheotuan_id == getTheoTuan.Take(1).Single().lichbaogiangtheotuan_id
                         select lbgct;
        //txtThu2_NgayHoc.InnerHtml = getTheoTuan.Take(1).Single().lichbaogiangtheotuan_ngayhoc;
        //txtThu3_NgayHoc.InnerHtml = getTheoTuan.Skip(1).Take(1).Single().lichbaogiangtheotuan_ngayhoc;
        //txtThu4_NgayHoc.InnerHtml = getTheoTuan.Skip(2).Take(1).Single().lichbaogiangtheotuan_ngayhoc;
        //txtThu5_NgayHoc.InnerHtml = getTheoTuan.Skip(3).Take(1).Single().lichbaogiangtheotuan_ngayhoc;
        //txtThu6_NgayHoc.InnerHtml = getTheoTuan.Skip(4).Take(1).Single().lichbaogiangtheotuan_ngayhoc;
        //thông tin của các tiết trong buổi học thứ 2
        txtThu2Tiet1_Mon.InnerHtml = getChiTiet.Take(1).Single().lichbaogiangchitiet_monhoc;
        txtThu2Tiet1_Lop.InnerHtml = getChiTiet.Take(1).Single().lichbaogiangchitiet_lop;
        txtThu2Tiet1_TCT.InnerHtml = getChiTiet.Take(1).Single().lichbaogiangchitiet_tietchuongtrinh;
        txtThu2Tiet1_TenBaiGiang.InnerHtml = getChiTiet.Take(1).Single().lichbaogiangchitiet_tenbaigiang;
        txtThu2Tiet1_GhiChu.InnerHtml = getChiTiet.Take(1).Single().lichbaogiangchitiet_ghichu;

        txtThu2Tiet2_Mon.InnerHtml = getChiTiet.Skip(1).Take(1).Single().lichbaogiangchitiet_monhoc;
        txtThu2Tiet2_Lop.InnerHtml = getChiTiet.Skip(1).Take(1).Single().lichbaogiangchitiet_lop;
        txtThu2Tiet2_TCT.InnerHtml = getChiTiet.Skip(1).Take(1).Single().lichbaogiangchitiet_tietchuongtrinh;
        txtThu2Tiet2_TenBaiGiang.InnerHtml = getChiTiet.Skip(1).Take(1).Single().lichbaogiangchitiet_tenbaigiang;
        txtThu2Tiet2_GhiChu.InnerHtml = getChiTiet.Skip(1).Take(1).Single().lichbaogiangchitiet_ghichu;

        txtThu2Tiet3_Mon.InnerHtml = getChiTiet.Skip(2).Take(1).Single().lichbaogiangchitiet_monhoc;
        txtThu2Tiet3_Lop.InnerHtml = getChiTiet.Skip(2).Take(1).Single().lichbaogiangchitiet_lop;
        txtThu2Tiet3_TCT.InnerHtml = getChiTiet.Skip(2).Take(1).Single().lichbaogiangchitiet_tietchuongtrinh;
        txtThu2Tiet3_TenBaiGiang.InnerHtml = getChiTiet.Skip(2).Take(1).Single().lichbaogiangchitiet_tenbaigiang;
        txtThu2Tiet3_GhiChu.InnerHtml = getChiTiet.Skip(2).Take(1).Single().lichbaogiangchitiet_ghichu;

        txtThu2Tiet4_Mon.InnerHtml = getChiTiet.Skip(3).Take(1).Single().lichbaogiangchitiet_monhoc;
        txtThu2Tiet4_Lop.InnerHtml = getChiTiet.Skip(3).Take(1).Single().lichbaogiangchitiet_lop;
        txtThu2Tiet4_TCT.InnerHtml = getChiTiet.Skip(3).Take(1).Single().lichbaogiangchitiet_tietchuongtrinh;
        txtThu2Tiet4_TenBaiGiang.InnerHtml = getChiTiet.Skip(3).Take(1).Single().lichbaogiangchitiet_tenbaigiang;
        txtThu2Tiet4_GhiChu.InnerHtml = getChiTiet.Skip(3).Take(1).Single().lichbaogiangchitiet_ghichu;

        txtThu2Tiet5_Mon.InnerHtml = getChiTiet.Skip(4).Take(1).Single().lichbaogiangchitiet_monhoc;
        txtThu2Tiet5_Lop.InnerHtml = getChiTiet.Skip(4).Take(1).Single().lichbaogiangchitiet_lop;
        txtThu2Tiet5_TCT.InnerHtml = getChiTiet.Skip(4).Take(1).Single().lichbaogiangchitiet_tietchuongtrinh;
        txtThu2Tiet5_TenBaiGiang.InnerHtml = getChiTiet.Skip(4).Take(1).Single().lichbaogiangchitiet_tenbaigiang;
        txtThu2Tiet5_GhiChu.InnerHtml = getChiTiet.Skip(4).Take(1).Single().lichbaogiangchitiet_ghichu;

        txtThu2Tiet6_Mon.InnerHtml = getChiTiet.Skip(5).Take(1).Single().lichbaogiangchitiet_monhoc;
        txtThu2Tiet6_Lop.InnerHtml = getChiTiet.Skip(5).Take(1).Single().lichbaogiangchitiet_lop;
        txtThu2Tiet6_TCT.InnerHtml = getChiTiet.Skip(5).Take(1).Single().lichbaogiangchitiet_tietchuongtrinh;
        txtThu2Tiet6_TenBaiGiang.InnerHtml = getChiTiet.Skip(5).Take(1).Single().lichbaogiangchitiet_tenbaigiang;
        txtThu2Tiet6_GhiChu.InnerHtml = getChiTiet.Skip(5).Take(1).Single().lichbaogiangchitiet_ghichu;

        txtThu2Tiet7_Mon.InnerHtml = getChiTiet.Skip(6).Take(1).Single().lichbaogiangchitiet_monhoc;
        txtThu2Tiet7_Lop.InnerHtml = getChiTiet.Skip(6).Take(1).Single().lichbaogiangchitiet_lop;
        txtThu2Tiet7_TCT.InnerHtml = getChiTiet.Skip(6).Take(1).Single().lichbaogiangchitiet_tietchuongtrinh;
        txtThu2Tiet7_TenBaiGiang.InnerHtml = getChiTiet.Skip(6).Take(1).Single().lichbaogiangchitiet_tenbaigiang;
        txtThu2Tiet7_GhiChu.InnerHtml = getChiTiet.Skip(6).Take(1).Single().lichbaogiangchitiet_ghichu;

        txtThu2Tiet8_Mon.InnerHtml = getChiTiet.Skip(7).Take(1).Single().lichbaogiangchitiet_monhoc;
        txtThu2Tiet8_Lop.InnerHtml = getChiTiet.Skip(7).Take(1).Single().lichbaogiangchitiet_lop;
        txtThu2Tiet8_TCT.InnerHtml = getChiTiet.Skip(7).Take(1).Single().lichbaogiangchitiet_tietchuongtrinh;
        txtThu2Tiet8_TenBaiGiang.InnerHtml = getChiTiet.Skip(7).Take(1).Single().lichbaogiangchitiet_tenbaigiang;
        txtThu2Tiet8_GhiChu.InnerHtml = getChiTiet.Skip(7).Take(1).Single().lichbaogiangchitiet_ghichu;
        //lấy thông tin của thứ 3 từ bảng chi tiết
        var getChiTiet1 = from lbgct in db.tbLichBaoGiangChiTiets
                          where lbgct.lichbaogiangtheotuan_id == getTheoTuan.Skip(1).Take(1).Single().lichbaogiangtheotuan_id
                          select lbgct;
        //thông tin của các tiết trong buổi học thứ 3
        txtThu3Tiet1_Mon.InnerHtml = getChiTiet1.Take(1).Single().lichbaogiangchitiet_monhoc;
        txtThu3Tiet1_Lop.InnerHtml = getChiTiet1.Take(1).Single().lichbaogiangchitiet_lop;
        txtThu3Tiet1_TCT.InnerHtml = getChiTiet1.Take(1).Single().lichbaogiangchitiet_tietchuongtrinh;
        txtThu3Tiet1_TenBaiGiang.InnerHtml = getChiTiet1.Take(1).Single().lichbaogiangchitiet_tenbaigiang;
        txtThu3Tiet1_GhiChu.InnerHtml = getChiTiet1.Take(1).Single().lichbaogiangchitiet_ghichu;

        txtThu3Tiet2_Mon.InnerHtml = getChiTiet1.Skip(1).Take(1).Single().lichbaogiangchitiet_monhoc;
        txtThu3Tiet2_Lop.InnerHtml = getChiTiet1.Skip(1).Take(1).Single().lichbaogiangchitiet_lop;
        txtThu3Tiet2_TCT.InnerHtml = getChiTiet1.Skip(1).Take(1).Single().lichbaogiangchitiet_tietchuongtrinh;
        txtThu3Tiet2_TenBaiGiang.InnerHtml = getChiTiet1.Skip(1).Take(1).Single().lichbaogiangchitiet_tenbaigiang;
        txtThu3Tiet2_GhiChu.InnerHtml = getChiTiet1.Skip(1).Take(1).Single().lichbaogiangchitiet_ghichu;

        txtThu3Tiet3_Mon.InnerHtml = getChiTiet1.Skip(2).Take(1).Single().lichbaogiangchitiet_monhoc;
        txtThu3Tiet3_Lop.InnerHtml = getChiTiet1.Skip(2).Take(1).Single().lichbaogiangchitiet_lop;
        txtThu3Tiet3_TCT.InnerHtml = getChiTiet1.Skip(2).Take(1).Single().lichbaogiangchitiet_tietchuongtrinh;
        txtThu3Tiet3_TenBaiGiang.InnerHtml = getChiTiet1.Skip(2).Take(1).Single().lichbaogiangchitiet_tenbaigiang;
        txtThu3Tiet3_GhiChu.InnerHtml = getChiTiet1.Skip(2).Take(1).Single().lichbaogiangchitiet_ghichu;

        txtThu3Tiet4_Mon.InnerHtml = getChiTiet1.Skip(3).Take(1).Single().lichbaogiangchitiet_monhoc;
        txtThu3Tiet4_Lop.InnerHtml = getChiTiet1.Skip(3).Take(1).Single().lichbaogiangchitiet_lop;
        txtThu3Tiet4_TCT.InnerHtml = getChiTiet1.Skip(3).Take(1).Single().lichbaogiangchitiet_tietchuongtrinh;
        txtThu3Tiet4_TenBaiGiang.InnerHtml = getChiTiet1.Skip(3).Take(1).Single().lichbaogiangchitiet_tenbaigiang;
        txtThu3Tiet4_GhiChu.InnerHtml = getChiTiet1.Skip(3).Take(1).Single().lichbaogiangchitiet_ghichu;

        txtThu3Tiet5_Mon.InnerHtml = getChiTiet1.Skip(4).Take(1).Single().lichbaogiangchitiet_monhoc;
        txtThu3Tiet5_Lop.InnerHtml = getChiTiet1.Skip(4).Take(1).Single().lichbaogiangchitiet_lop;
        txtThu3Tiet5_TCT.InnerHtml = getChiTiet1.Skip(4).Take(1).Single().lichbaogiangchitiet_tietchuongtrinh;
        txtThu3Tiet5_TenBaiGiang.InnerHtml = getChiTiet1.Skip(4).Take(1).Single().lichbaogiangchitiet_tenbaigiang;
        txtThu3Tiet5_GhiChu.InnerHtml = getChiTiet1.Skip(4).Take(1).Single().lichbaogiangchitiet_ghichu;

        txtThu3Tiet6_Mon.InnerHtml = getChiTiet1.Skip(5).Take(1).Single().lichbaogiangchitiet_monhoc;
        txtThu3Tiet6_Lop.InnerHtml = getChiTiet1.Skip(5).Take(1).Single().lichbaogiangchitiet_lop;
        txtThu3Tiet6_TCT.InnerHtml = getChiTiet1.Skip(5).Take(1).Single().lichbaogiangchitiet_tietchuongtrinh;
        txtThu3Tiet6_TenBaiGiang.InnerHtml = getChiTiet1.Skip(5).Take(1).Single().lichbaogiangchitiet_tenbaigiang;
        txtThu3Tiet6_GhiChu.InnerHtml = getChiTiet1.Skip(5).Take(1).Single().lichbaogiangchitiet_ghichu;

        txtThu3Tiet7_Mon.InnerHtml = getChiTiet1.Skip(6).Take(1).Single().lichbaogiangchitiet_monhoc;
        txtThu3Tiet7_Lop.InnerHtml = getChiTiet1.Skip(6).Take(1).Single().lichbaogiangchitiet_lop;
        txtThu3Tiet7_TCT.InnerHtml = getChiTiet1.Skip(6).Take(1).Single().lichbaogiangchitiet_tietchuongtrinh;
        txtThu3Tiet7_TenBaiGiang.InnerHtml = getChiTiet1.Skip(6).Take(1).Single().lichbaogiangchitiet_tenbaigiang;
        txtThu3Tiet7_GhiChu.InnerHtml = getChiTiet1.Skip(6).Take(1).Single().lichbaogiangchitiet_ghichu;

        txtThu3Tiet8_Mon.InnerHtml = getChiTiet1.Skip(7).Take(1).Single().lichbaogiangchitiet_monhoc;
        txtThu3Tiet8_Lop.InnerHtml = getChiTiet1.Skip(7).Take(1).Single().lichbaogiangchitiet_lop;
        txtThu3Tiet8_TCT.InnerHtml = getChiTiet1.Skip(7).Take(1).Single().lichbaogiangchitiet_tietchuongtrinh;
        txtThu3Tiet8_TenBaiGiang.InnerHtml = getChiTiet1.Skip(7).Take(1).Single().lichbaogiangchitiet_tenbaigiang;
        txtThu3Tiet8_GhiChu.InnerHtml = getChiTiet1.Skip(7).Take(1).Single().lichbaogiangchitiet_ghichu;
        //lấy thông tin của thứ 4 từ bảng chi tiết
        var getChiTiet2 = from lbgct in db.tbLichBaoGiangChiTiets
                          where lbgct.lichbaogiangtheotuan_id == getTheoTuan.Skip(2).Take(1).Single().lichbaogiangtheotuan_id
                          select lbgct;
        //thông tin của các tiết trong buổi học thứ 4
        txtThu4Tiet1_Mon.InnerHtml = getChiTiet2.Take(1).Single().lichbaogiangchitiet_monhoc;
        txtThu4Tiet1_Lop.InnerHtml = getChiTiet2.Take(1).Single().lichbaogiangchitiet_lop;
        txtThu4Tiet1_TCT.InnerHtml = getChiTiet2.Take(1).Single().lichbaogiangchitiet_tietchuongtrinh;
        txtThu4Tiet1_TenBaiGiang.InnerHtml = getChiTiet2.Take(1).Single().lichbaogiangchitiet_tenbaigiang;
        txtThu4Tiet1_GhiChu.InnerHtml = getChiTiet2.Take(1).Single().lichbaogiangchitiet_ghichu;

        txtThu4Tiet2_Mon.InnerHtml = getChiTiet2.Skip(1).Take(1).Single().lichbaogiangchitiet_monhoc;
        txtThu4Tiet2_Lop.InnerHtml = getChiTiet2.Skip(1).Take(1).Single().lichbaogiangchitiet_lop;
        txtThu4Tiet2_TCT.InnerHtml = getChiTiet2.Skip(1).Take(1).Single().lichbaogiangchitiet_tietchuongtrinh;
        txtThu4Tiet2_TenBaiGiang.InnerHtml = getChiTiet2.Skip(1).Take(1).Single().lichbaogiangchitiet_tenbaigiang;
        txtThu4Tiet2_GhiChu.InnerHtml = getChiTiet2.Skip(1).Take(1).Single().lichbaogiangchitiet_ghichu;

        txtThu4Tiet3_Mon.InnerHtml = getChiTiet2.Skip(2).Take(1).Single().lichbaogiangchitiet_monhoc;
        txtThu4Tiet3_Lop.InnerHtml = getChiTiet2.Skip(2).Take(1).Single().lichbaogiangchitiet_lop;
        txtThu4Tiet3_TCT.InnerHtml = getChiTiet2.Skip(2).Take(1).Single().lichbaogiangchitiet_tietchuongtrinh;
        txtThu4Tiet3_TenBaiGiang.InnerHtml = getChiTiet2.Skip(2).Take(1).Single().lichbaogiangchitiet_tenbaigiang;
        txtThu4Tiet3_GhiChu.InnerHtml = getChiTiet2.Skip(2).Take(1).Single().lichbaogiangchitiet_ghichu;

        txtThu4Tiet4_Mon.InnerHtml = getChiTiet2.Skip(3).Take(1).Single().lichbaogiangchitiet_monhoc;
        txtThu4Tiet4_Lop.InnerHtml = getChiTiet2.Skip(3).Take(1).Single().lichbaogiangchitiet_lop;
        txtThu4Tiet4_TCT.InnerHtml = getChiTiet2.Skip(3).Take(1).Single().lichbaogiangchitiet_tietchuongtrinh;
        txtThu4Tiet4_TenBaiGiang.InnerHtml = getChiTiet2.Skip(3).Take(1).Single().lichbaogiangchitiet_tenbaigiang;
        txtThu4Tiet4_GhiChu.InnerHtml = getChiTiet2.Skip(3).Take(1).Single().lichbaogiangchitiet_ghichu;

        txtThu4Tiet5_Mon.InnerHtml = getChiTiet2.Skip(4).Take(1).Single().lichbaogiangchitiet_monhoc;
        txtThu4Tiet5_Lop.InnerHtml = getChiTiet2.Skip(4).Take(1).Single().lichbaogiangchitiet_lop;
        txtThu4Tiet5_TCT.InnerHtml = getChiTiet2.Skip(4).Take(1).Single().lichbaogiangchitiet_tietchuongtrinh;
        txtThu4Tiet5_TenBaiGiang.InnerHtml = getChiTiet2.Skip(4).Take(1).Single().lichbaogiangchitiet_tenbaigiang;
        txtThu4Tiet5_GhiChu.InnerHtml = getChiTiet2.Skip(4).Take(1).Single().lichbaogiangchitiet_ghichu;

        txtThu4Tiet6_Mon.InnerHtml = getChiTiet2.Skip(5).Take(1).Single().lichbaogiangchitiet_monhoc;
        txtThu4Tiet6_Lop.InnerHtml = getChiTiet2.Skip(5).Take(1).Single().lichbaogiangchitiet_lop;
        txtThu4Tiet6_TCT.InnerHtml = getChiTiet2.Skip(5).Take(1).Single().lichbaogiangchitiet_tietchuongtrinh;
        txtThu4Tiet6_TenBaiGiang.InnerHtml = getChiTiet2.Skip(5).Take(1).Single().lichbaogiangchitiet_tenbaigiang;
        txtThu4Tiet6_GhiChu.InnerHtml = getChiTiet2.Skip(5).Take(1).Single().lichbaogiangchitiet_ghichu;

        txtThu4Tiet7_Mon.InnerHtml = getChiTiet2.Skip(6).Take(1).Single().lichbaogiangchitiet_monhoc;
        txtThu4Tiet7_Lop.InnerHtml = getChiTiet2.Skip(6).Take(1).Single().lichbaogiangchitiet_lop;
        txtThu4Tiet7_TCT.InnerHtml = getChiTiet2.Skip(6).Take(1).Single().lichbaogiangchitiet_tietchuongtrinh;
        txtThu4Tiet7_TenBaiGiang.InnerHtml = getChiTiet2.Skip(6).Take(1).Single().lichbaogiangchitiet_tenbaigiang;
        txtThu4Tiet7_GhiChu.InnerHtml = getChiTiet2.Skip(6).Take(1).Single().lichbaogiangchitiet_ghichu;

        txtThu4Tiet8_Mon.InnerHtml = getChiTiet2.Skip(7).Take(1).Single().lichbaogiangchitiet_monhoc;
        txtThu4Tiet8_Lop.InnerHtml = getChiTiet2.Skip(7).Take(1).Single().lichbaogiangchitiet_lop;
        txtThu4Tiet8_TCT.InnerHtml = getChiTiet2.Skip(7).Take(1).Single().lichbaogiangchitiet_tietchuongtrinh;
        txtThu4Tiet8_TenBaiGiang.InnerHtml = getChiTiet2.Skip(7).Take(1).Single().lichbaogiangchitiet_tenbaigiang;
        txtThu4Tiet8_GhiChu.InnerHtml = getChiTiet2.Skip(7).Take(1).Single().lichbaogiangchitiet_ghichu;
        //lấy thông tin của thứ 5 từ bảng chi tiết
        var getChiTiet3 = from lbgct in db.tbLichBaoGiangChiTiets
                          where lbgct.lichbaogiangtheotuan_id == getTheoTuan.Skip(3).Take(1).Single().lichbaogiangtheotuan_id
                          select lbgct;
        //thông tin của các tiết trong buổi học thứ 5
        txtThu5Tiet1_Mon.InnerHtml = getChiTiet3.Take(1).Single().lichbaogiangchitiet_monhoc;
        txtThu5Tiet1_Lop.InnerHtml = getChiTiet3.Take(1).Single().lichbaogiangchitiet_lop;
        txtThu5Tiet1_TCT.InnerHtml = getChiTiet3.Take(1).Single().lichbaogiangchitiet_tietchuongtrinh;
        txtThu5Tiet1_TenBaiGiang.InnerHtml = getChiTiet3.Take(1).Single().lichbaogiangchitiet_tenbaigiang;
        txtThu5Tiet1_GhiChu.InnerHtml = getChiTiet3.Take(1).Single().lichbaogiangchitiet_ghichu;

        txtThu5Tiet2_Mon.InnerHtml = getChiTiet3.Skip(1).Take(1).Single().lichbaogiangchitiet_monhoc;
        txtThu5Tiet2_Lop.InnerHtml = getChiTiet3.Skip(1).Take(1).Single().lichbaogiangchitiet_lop;
        txtThu5Tiet2_TCT.InnerHtml = getChiTiet3.Skip(1).Take(1).Single().lichbaogiangchitiet_tietchuongtrinh;
        txtThu5Tiet2_TenBaiGiang.InnerHtml = getChiTiet3.Skip(1).Take(1).Single().lichbaogiangchitiet_tenbaigiang;
        txtThu5Tiet2_GhiChu.InnerHtml = getChiTiet3.Skip(1).Take(1).Single().lichbaogiangchitiet_ghichu;

        txtThu5Tiet3_Mon.InnerHtml = getChiTiet3.Skip(2).Take(1).Single().lichbaogiangchitiet_monhoc;
        txtThu5Tiet3_Lop.InnerHtml = getChiTiet3.Skip(2).Take(1).Single().lichbaogiangchitiet_lop;
        txtThu5Tiet3_TCT.InnerHtml = getChiTiet3.Skip(2).Take(1).Single().lichbaogiangchitiet_tietchuongtrinh;
        txtThu5Tiet3_TenBaiGiang.InnerHtml = getChiTiet3.Skip(2).Take(1).Single().lichbaogiangchitiet_tenbaigiang;
        txtThu5Tiet3_GhiChu.InnerHtml = getChiTiet3.Skip(2).Take(1).Single().lichbaogiangchitiet_ghichu;

        txtThu5Tiet4_Mon.InnerHtml = getChiTiet3.Skip(3).Take(1).Single().lichbaogiangchitiet_monhoc;
        txtThu5Tiet4_Lop.InnerHtml = getChiTiet3.Skip(3).Take(1).Single().lichbaogiangchitiet_lop;
        txtThu5Tiet4_TCT.InnerHtml = getChiTiet3.Skip(3).Take(1).Single().lichbaogiangchitiet_tietchuongtrinh;
        txtThu5Tiet4_TenBaiGiang.InnerHtml = getChiTiet3.Skip(3).Take(1).Single().lichbaogiangchitiet_tenbaigiang;
        txtThu5Tiet4_GhiChu.InnerHtml = getChiTiet3.Skip(3).Take(1).Single().lichbaogiangchitiet_ghichu;

        txtThu5Tiet5_Mon.InnerHtml = getChiTiet3.Skip(4).Take(1).Single().lichbaogiangchitiet_monhoc;
        txtThu5Tiet5_Lop.InnerHtml = getChiTiet3.Skip(4).Take(1).Single().lichbaogiangchitiet_lop;
        txtThu5Tiet5_TCT.InnerHtml = getChiTiet3.Skip(4).Take(1).Single().lichbaogiangchitiet_tietchuongtrinh;
        txtThu5Tiet5_TenBaiGiang.InnerHtml = getChiTiet3.Skip(4).Take(1).Single().lichbaogiangchitiet_tenbaigiang;
        txtThu5Tiet5_GhiChu.InnerHtml = getChiTiet3.Skip(4).Take(1).Single().lichbaogiangchitiet_ghichu;

        txtThu5Tiet6_Mon.InnerHtml = getChiTiet3.Skip(5).Take(1).Single().lichbaogiangchitiet_monhoc;
        txtThu5Tiet6_Lop.InnerHtml = getChiTiet3.Skip(5).Take(1).Single().lichbaogiangchitiet_lop;
        txtThu5Tiet6_TCT.InnerHtml = getChiTiet3.Skip(5).Take(1).Single().lichbaogiangchitiet_tietchuongtrinh;
        txtThu5Tiet6_TenBaiGiang.InnerHtml = getChiTiet3.Skip(5).Take(1).Single().lichbaogiangchitiet_tenbaigiang;
        txtThu5Tiet6_GhiChu.InnerHtml = getChiTiet3.Skip(5).Take(1).Single().lichbaogiangchitiet_ghichu;

        txtThu5Tiet7_Mon.InnerHtml = getChiTiet3.Skip(6).Take(1).Single().lichbaogiangchitiet_monhoc;
        txtThu5Tiet7_Lop.InnerHtml = getChiTiet3.Skip(6).Take(1).Single().lichbaogiangchitiet_lop;
        txtThu5Tiet7_TCT.InnerHtml = getChiTiet3.Skip(6).Take(1).Single().lichbaogiangchitiet_tietchuongtrinh;
        txtThu5Tiet7_TenBaiGiang.InnerHtml = getChiTiet3.Skip(6).Take(1).Single().lichbaogiangchitiet_tenbaigiang;
        txtThu5Tiet7_GhiChu.InnerHtml = getChiTiet3.Skip(6).Take(1).Single().lichbaogiangchitiet_ghichu;

        txtThu5Tiet8_Mon.InnerHtml = getChiTiet3.Skip(7).Take(1).Single().lichbaogiangchitiet_monhoc;
        txtThu5Tiet8_Lop.InnerHtml = getChiTiet3.Skip(7).Take(1).Single().lichbaogiangchitiet_lop;
        txtThu5Tiet8_TCT.InnerHtml = getChiTiet3.Skip(7).Take(1).Single().lichbaogiangchitiet_tietchuongtrinh;
        txtThu5Tiet8_TenBaiGiang.InnerHtml = getChiTiet3.Skip(7).Take(1).Single().lichbaogiangchitiet_tenbaigiang;
        txtThu5Tiet8_GhiChu.InnerHtml = getChiTiet3.Skip(7).Take(1).Single().lichbaogiangchitiet_ghichu;
        //lấy thông tin của thứ 6 từ bảng chi tiết
        var getChiTiet4 = from lbgct in db.tbLichBaoGiangChiTiets
                          where lbgct.lichbaogiangtheotuan_id == getTheoTuan.Skip(4).Take(1).Single().lichbaogiangtheotuan_id
                          select lbgct;
        //thông tin của các tiết trong buổi học thứ 6
        txtThu6Tiet1_Mon.InnerHtml = getChiTiet4.Take(1).Single().lichbaogiangchitiet_monhoc;
        txtThu6Tiet1_Lop.InnerHtml = getChiTiet4.Take(1).Single().lichbaogiangchitiet_lop;
        txtThu6Tiet1_TCT.InnerHtml = getChiTiet4.Take(1).Single().lichbaogiangchitiet_tietchuongtrinh;
        txtThu6Tiet1_TenBaiGiang.InnerHtml = getChiTiet4.Take(1).Single().lichbaogiangchitiet_tenbaigiang;
        txtThu6Tiet1_GhiChu.InnerHtml = getChiTiet4.Take(1).Single().lichbaogiangchitiet_ghichu;
        txtThu6Tiet2_Mon.InnerHtml = getChiTiet4.Skip(1).Take(1).Single().lichbaogiangchitiet_monhoc;
        txtThu6Tiet2_Lop.InnerHtml = getChiTiet4.Skip(1).Take(1).Single().lichbaogiangchitiet_lop;
        txtThu6Tiet2_TCT.InnerHtml = getChiTiet4.Skip(1).Take(1).Single().lichbaogiangchitiet_tietchuongtrinh;
        txtThu6Tiet2_TenBaiGiang.InnerHtml = getChiTiet4.Skip(1).Take(1).Single().lichbaogiangchitiet_tenbaigiang;
        txtThu6Tiet2_GhiChu.InnerHtml = getChiTiet4.Skip(1).Take(1).Single().lichbaogiangchitiet_ghichu;
        txtThu6Tiet3_Mon.InnerHtml = getChiTiet4.Skip(2).Take(1).Single().lichbaogiangchitiet_monhoc;
        txtThu6Tiet3_Lop.InnerHtml = getChiTiet4.Skip(2).Take(1).Single().lichbaogiangchitiet_lop;
        txtThu6Tiet3_TCT.InnerHtml = getChiTiet4.Skip(2).Take(1).Single().lichbaogiangchitiet_tietchuongtrinh;
        txtThu6Tiet3_TenBaiGiang.InnerHtml = getChiTiet4.Skip(2).Take(1).Single().lichbaogiangchitiet_tenbaigiang;
        txtThu6Tiet3_GhiChu.InnerHtml = getChiTiet4.Skip(2).Take(1).Single().lichbaogiangchitiet_ghichu;
        txtThu6Tiet4_Mon.InnerHtml = getChiTiet4.Skip(3).Take(1).Single().lichbaogiangchitiet_monhoc;
        txtThu6Tiet4_Lop.InnerHtml = getChiTiet4.Skip(3).Take(1).Single().lichbaogiangchitiet_lop;
        txtThu6Tiet4_TCT.InnerHtml = getChiTiet4.Skip(3).Take(1).Single().lichbaogiangchitiet_tietchuongtrinh;
        txtThu6Tiet4_TenBaiGiang.InnerHtml = getChiTiet4.Skip(3).Take(1).Single().lichbaogiangchitiet_tenbaigiang;
        txtThu6Tiet4_GhiChu.InnerHtml = getChiTiet4.Skip(3).Take(1).Single().lichbaogiangchitiet_ghichu;
        txtThu6Tiet5_Mon.InnerHtml = getChiTiet4.Skip(4).Take(1).Single().lichbaogiangchitiet_monhoc;
        txtThu6Tiet5_Lop.InnerHtml = getChiTiet4.Skip(4).Take(1).Single().lichbaogiangchitiet_lop;
        txtThu6Tiet5_TCT.InnerHtml = getChiTiet4.Skip(4).Take(1).Single().lichbaogiangchitiet_tietchuongtrinh;
        txtThu6Tiet5_TenBaiGiang.InnerHtml = getChiTiet4.Skip(4).Take(1).Single().lichbaogiangchitiet_tenbaigiang;
        txtThu6Tiet5_GhiChu.InnerHtml = getChiTiet4.Skip(4).Take(1).Single().lichbaogiangchitiet_ghichu;
        txtThu6Tiet6_Mon.InnerHtml = getChiTiet4.Skip(5).Take(1).Single().lichbaogiangchitiet_monhoc;
        txtThu6Tiet6_Lop.InnerHtml = getChiTiet4.Skip(5).Take(1).Single().lichbaogiangchitiet_lop;
        txtThu6Tiet6_TCT.InnerHtml = getChiTiet4.Skip(5).Take(1).Single().lichbaogiangchitiet_tietchuongtrinh;
        txtThu6Tiet6_TenBaiGiang.InnerHtml = getChiTiet4.Skip(5).Take(1).Single().lichbaogiangchitiet_tenbaigiang;
        txtThu6Tiet6_GhiChu.InnerHtml = getChiTiet4.Skip(5).Take(1).Single().lichbaogiangchitiet_ghichu;
        txtThu6Tiet7_Mon.InnerHtml = getChiTiet4.Skip(6).Take(1).Single().lichbaogiangchitiet_monhoc;
        txtThu6Tiet7_Lop.InnerHtml = getChiTiet4.Skip(6).Take(1).Single().lichbaogiangchitiet_lop;
        txtThu6Tiet7_TCT.InnerHtml = getChiTiet4.Skip(6).Take(1).Single().lichbaogiangchitiet_tietchuongtrinh;
        txtThu6Tiet7_TenBaiGiang.InnerHtml = getChiTiet4.Skip(6).Take(1).Single().lichbaogiangchitiet_tenbaigiang;
        txtThu6Tiet7_GhiChu.InnerHtml = getChiTiet4.Skip(6).Take(1).Single().lichbaogiangchitiet_ghichu;
        txtThu6Tiet8_Mon.InnerHtml = getChiTiet4.Skip(7).Take(1).Single().lichbaogiangchitiet_monhoc;
        txtThu6Tiet8_Lop.InnerHtml = getChiTiet4.Skip(7).Take(1).Single().lichbaogiangchitiet_lop;
        txtThu6Tiet8_TCT.InnerHtml = getChiTiet4.Skip(7).Take(1).Single().lichbaogiangchitiet_tietchuongtrinh;
        txtThu6Tiet8_TenBaiGiang.InnerHtml = getChiTiet4.Skip(7).Take(1).Single().lichbaogiangchitiet_tenbaigiang;
        txtThu6Tiet8_GhiChu.InnerHtml = getChiTiet4.Skip(7).Take(1).Single().lichbaogiangchitiet_ghichu;
    }
    protected void btnQuayLai_Click(object sender, EventArgs e)
    {
        Response.Redirect("/admin-lich-bao-giang");

    }

    protected void btnXem_Click(object sender, EventArgs e)
    {
        user_id = Convert.ToInt32(txtUser_id.Value);
        tuanbaoGiang = Convert.ToInt32(ddlLichTuan.SelectedItem.Text);
        loadData(_id, tuanbaoGiang, user_id);
    }

    protected void btnXemBaoGiangGiaoVien_ServerClick(object sender, EventArgs e)
    {
        if (txtUser_id.Value != "")
        {
            var listTuan = from tuan in db.tbLichBaoGiangs
                           where tuan.khoi_id == _id && tuan.username_id == Convert.ToInt32(txtUser_id.Value)
                           orderby tuan.lichbaogiang_id descending
                           select tuan;
            if (listTuan.Count() > 0)
            {
                ddlLichTuan.DataSource = listTuan;
                ddlLichTuan.DataTextField = "lichbaigiang_tuan";
                ddlLichTuan.DataValueField = "lichbaogiang_id";
                ddlLichTuan.DataBind();
            }
        }
        user_id = Convert.ToInt32(txtUser_id.Value);
        //alert.alert_Success(Page, "thử nghiệm", "");
        tuanbaoGiang = Convert.ToInt32(ddlLichTuan.SelectedItem.Text);
        loadData(_id, tuanbaoGiang, user_id);
    }
    protected void hiddenCotMon()
    {
        txtThu2Tiet1_Mon.Visible = false;
        txtThu2Tiet2_Mon.Visible = false;
        txtThu2Tiet3_Mon.Visible = false;
        txtThu2Tiet4_Mon.Visible = false;
        txtThu2Tiet5_Mon.Visible = false;
        txtThu2Tiet6_Mon.Visible = false;
        txtThu2Tiet7_Mon.Visible = false;
        txtThu2Tiet8_Mon.Visible = false;

        txtThu3Tiet1_Mon.Visible = false;
        txtThu3Tiet2_Mon.Visible = false;
        txtThu3Tiet3_Mon.Visible = false;
        txtThu3Tiet4_Mon.Visible = false;
        txtThu3Tiet5_Mon.Visible = false;
        txtThu3Tiet6_Mon.Visible = false;
        txtThu3Tiet7_Mon.Visible = false;
        txtThu3Tiet8_Mon.Visible = false;

        txtThu4Tiet1_Mon.Visible = false;
        txtThu4Tiet2_Mon.Visible = false;
        txtThu4Tiet3_Mon.Visible = false;
        txtThu4Tiet4_Mon.Visible = false;
        txtThu4Tiet5_Mon.Visible = false;
        txtThu4Tiet6_Mon.Visible = false;
        txtThu4Tiet7_Mon.Visible = false;
        txtThu4Tiet8_Mon.Visible = false;

        txtThu5Tiet1_Mon.Visible = false;
        txtThu5Tiet2_Mon.Visible = false;
        txtThu5Tiet3_Mon.Visible = false;
        txtThu5Tiet4_Mon.Visible = false;
        txtThu5Tiet5_Mon.Visible = false;
        txtThu5Tiet6_Mon.Visible = false;
        txtThu5Tiet7_Mon.Visible = false;
        txtThu5Tiet8_Mon.Visible = false;

        txtThu6Tiet1_Mon.Visible = false;
        txtThu6Tiet2_Mon.Visible = false;
        txtThu6Tiet3_Mon.Visible = false;
        txtThu6Tiet4_Mon.Visible = false;
        txtThu6Tiet5_Mon.Visible = false;
        txtThu6Tiet6_Mon.Visible = false;
        txtThu6Tiet7_Mon.Visible = false;
        txtThu6Tiet8_Mon.Visible = false;

    }
}