﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_XemLichTong.aspx.cs" Inherits="admin_page_module_function_module_WebSite_module_XemLichTong" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="card card-block">
        <div id="dvKetQua" runat="server">
            <br />
            <br />
            <div>
                <p>
                    <b>SỞ GIÁO DỤC & ĐÀO TẠO ĐÀ NẴNG</b>
                    <br />
                    <b>TRƯỜNG TH-THCS & THPT VIỆT NHẬT</b>
                </p>
                <div style="text-align: center">
                    <h3><b>LỊCH TỔNG NĂM 2020 - 2021 </b></h3>
                    <br />

                </div>
            </div>

            <div style="overflow-x: auto;">

                <table class="table table-bordered">
                    <thead>
                        <tr class="head_table">
                            <th class="table_header" scope="col">Thứ</th>
                            <th scope="col">Buổi</th>
                            <th scope="col">Tiết</th>
                            <th>Lớp 1/1</th>
                            <th>Lớp 1/2</th>
                            <th>Lớp 1/3</th>
                            <th>Lớp 1/4</th>
                            <th>Lớp 2/1</th>
                            <th>Lớp 2/2</th>
                            <th>Lớp 2/3</th>
                            <th>Lớp 2/4</th>
                            <th>Lớp 2/5</th>
                            <th>Lớp 3/1</th>
                            <th>Lớp 3/2</th>
                            <th>Lớp 4/1</th>
                            <th>Lớp 5/1</th>
                            <th>Lớp 6/1</th>
                            <th>Lớp 6/2</th>
                            <th>Lớp 7/1</th>
                            <th>Lớp 10/1</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td rowspan="8">
                                <span class="table_day">Thứ hai<br />
                                    <span runat="server" id="txt_Thu2_NgayHoc"></span>
                                </span>
                            </td>
                            <td rowspan="4">Sáng</td>
                            <td class="tiet" style="text-align: center">1</td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet1_Lop1_1_Mon"><a href="#"><%=lbl_Thu2_tiet1_Lop1_1_Mon %></a></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet1_Lop1_2_Mon"><a href="#"><%=lbl_Thu2_tiet1_Lop1_2_Mon %></a></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet1_Lop1_3_Mon"><a href="#"><%=lbl_Thu2_tiet1_Lop1_3_Mon %></a></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet1_Lop1_4_Mon"><a href="#"><%=lbl_Thu2_tiet1_Lop1_4_Mon %></a></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet1_Lop2_1_Mon"><a href="#"><%=lbl_Thu2_tiet1_Lop2_1_Mon %></a></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet1_Lop2_2_Mon"><a href="#"><%=lbl_Thu2_tiet1_Lop2_2_Mon %></a></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet1_Lop2_3_Mon"><a href="#"><%=lbl_Thu2_tiet1_Lop2_3_Mon %></a></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet1_Lop2_4_Mon"><a href="#"><%=lbl_Thu2_tiet1_Lop2_4_Mon %></a></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet1_Lop2_5_Mon"><a href="#"><%=lbl_Thu2_tiet1_Lop2_5_Mon %></a></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet1_Lop3_1_Mon"><a href="#"><%=lbl_Thu2_tiet1_Lop3_1_Mon %></a></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet1_Lop3_2_Mon"><a href="#"><%=lbl_Thu2_tiet1_Lop3_2_Mon %></a></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet1_Lop4_1_Mon"><a href="#"><%=lbl_Thu2_tiet1_Lop4_1_Mon %></a></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet1_Lop5_1_Mon"><a href="#"><%=lbl_Thu2_tiet1_Lop5_1_Mon %></a></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet1_Lop6_1_Mon"><a href="#"><%=lbl_Thu2_tiet1_Lop6_1_Mon %></a></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet1_Lop6_2_Mon"><a href="#"><%=lbl_Thu2_tiet1_Lop6_2_Mon %></a></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet1_Lop7_1_Mon"><a href="#"><%=lbl_Thu2_tiet1_Lop7_1_Mon %></a></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet1_Lop10_1_Mon"><a href="#"><%=lbl_Thu2_tiet1_Lop10_1_Mon %></a></td>
                        </tr>
                        <tr>
                            <td class="tiet" style="text-align: center">2</td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet2_Lop1_1_Mon"><%=lbl_Thu2_tiet2_Lop1_1_Mon %></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet2_Lop1_2_Mon"><%=lbl_Thu2_tiet2_Lop1_2_Mon %></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet2_Lop1_3_Mon"><%=lbl_Thu2_tiet2_Lop1_3_Mon %></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet2_Lop1_4_Mon"><%=lbl_Thu2_tiet2_Lop1_4_Mon %></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet2_Lop2_1_Mon"><%=lbl_Thu2_tiet2_Lop2_1_Mon %></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet2_Lop2_2_Mon"><%=lbl_Thu2_tiet2_Lop2_2_Mon %></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet2_Lop2_3_Mon"><%=lbl_Thu2_tiet2_Lop2_3_Mon %></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet2_Lop2_4_Mon"><%=lbl_Thu2_tiet2_Lop2_4_Mon %></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet2_Lop2_5_Mon"><%=lbl_Thu2_tiet2_Lop2_5_Mon %></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet2_Lop3_1_Mon"><%=lbl_Thu2_tiet2_Lop3_1_Mon %></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet2_Lop3_2_Mon"><%=lbl_Thu2_tiet2_Lop3_2_Mon %></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet2_Lop4_1_Mon"><%=lbl_Thu2_tiet2_Lop4_1_Mon %></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet2_Lop5_1_Mon"><%=lbl_Thu2_tiet2_Lop5_1_Mon %></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet2_Lop6_1_Mon"><%=lbl_Thu2_tiet2_Lop6_1_Mon %></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet2_Lop6_2_Mon"><%=lbl_Thu2_tiet2_Lop6_2_Mon %></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet2_Lop7_1_Mon"><%=lbl_Thu2_tiet2_Lop7_1_Mon %></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet2_Lop10_1_Mon"><%=lbl_Thu2_tiet2_Lop10_1_Mon %></td>
                        </tr>
                        <tr>
                            <td class="tiet" style="text-align: center">3</td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet3_Lop1_1_Mon"><a href="#">Tin</a></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet3_Lop1_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet3_Lop1_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet3_Lop1_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet3_Lop2_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet3_Lop2_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet3_Lop2_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet3_Lop2_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet3_Lop2_5_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet3_Lop3_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet3_Lop3_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet3_Lop4_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet3_Lop5_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet3_Lop6_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet3_Lop6_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet3_Lop7_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet3_Lop10_2_Mon"></td>
                        </tr>
                        <tr>
                            <td class="tiet" style="text-align: center">4</td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet4_Lop1_1_Mon"><a href="#">Toán</a></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet4_Lop1_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet4_Lop1_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet4_Lop1_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet4_Lop2_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet4_Lop2_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet4_Lop2_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet4_Lop2_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet4_Lop2_5_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet4_Lop3_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet4_Lop3_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet4_Lop4_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet4_Lop5_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet4_Lop6_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet4_Lop6_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet4_Lop7_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet4_Lop10_2_Mon"></td>
                        </tr>
                        <tr>
                            <td rowspan="4">Chiều</td>
                            <td class="tiet" style="text-align: center">5</td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet5_Lop1_1_Mon"><a href="#">Mỹ thuật</a></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet5_Lop1_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet5_Lop1_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet5_Lop1_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet5_Lop2_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet5_Lop2_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet5_Lop2_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet5_Lop2_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet5_Lop2_5_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet5_Lop3_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet5_Lop3_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet5_Lop4_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet5_Lop5_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet5_Lop6_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet5_Lop6_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet5_Lop7_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet5_Lop10_2_Mon"></td>
                        </tr>
                        <tr>
                            <td class="tiet" style="text-align: center">6</td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet6_Lop1_1_Mon"><a href="#">Mỹ thuật TC</a></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet6_Lop1_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet6_Lop1_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet6_Lop1_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet6_Lop2_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet6_Lop2_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet6_Lop2_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet6_Lop2_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet6_Lop2_5_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet6_Lop3_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet6_Lop3_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet6_Lop4_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet6_Lop5_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet6_Lop6_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet6_Lop6_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet6_Lop7_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet6_Lop10_2_Mon"></td>
                        </tr>
                        <tr>
                            <td class="tiet" style="text-align: center">7</td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet7_Lop1_1_Mon"><a href="#">TV - TC</a></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet7_Lop1_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet7_Lop1_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet7_Lop1_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet7_Lop2_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet7_Lop2_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet7_Lop2_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet7_Lop2_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet7_Lop2_5_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet7_Lop3_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet7_Lop3_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet7_Lop4_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet7_Lop5_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet7_Lop6_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet7_Lop6_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet7_Lop7_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet7_Lop10_2_Mon"></td>
                        </tr>
                        <tr>
                            <td class="tiet" style="text-align: center">8</td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet8_Lop1_1_Mon"><a href="#"></a></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet8_Lop1_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet8_Lop1_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet8_Lop1_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet8_Lop2_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet8_Lop2_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet8_Lop2_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet8_Lop2_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet8_Lop2_5_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet8_Lop3_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet8_Lop3_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet8_Lop4_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet8_Lop5_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet8_Lop6_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet8_Lop6_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet8_Lop7_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu2_tiet8_Lop10_2_Mon"></td>
                        </tr>
                        <tr>
                            <td rowspan="8">
                                <span class="table_day">Thứ ba<br />
                                    <span runat="server" id="txt_Thu3_NgayHoc"></span>
                                </span>
                            </td>
                            <td rowspan="4">Sáng</td>
                            <td class="tiet" style="text-align: center">1</td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet1_Lop1_1_Mon"><a href="#">Học vần</a></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet1_Lop1_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet1_Lop1_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet1_Lop1_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet1_Lop2_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet1_Lop2_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet1_Lop2_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet1_Lop2_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet1_Lop2_5_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet1_Lop3_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet1_Lop3_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet1_Lop4_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet1_Lop5_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet1_Lop6_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet1_Lop6_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet1_Lop7_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet1_Lop10_2_Mon"></td>
                        </tr>
                        <tr>
                            <td class="tiet" style="text-align: center">2</td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet2_Lop1_1_Mon"><a href="#">Học vần</a></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet2_Lop1_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet2_Lop1_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet2_Lop1_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet2_Lop2_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet2_Lop2_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet2_Lop2_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet2_Lop2_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet2_Lop2_5_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet2_Lop3_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet2_Lop3_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet2_Lop4_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet2_Lop5_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet2_Lop6_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet2_Lop6_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet2_Lop7_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet2_Lop10_2_Mon"></td>
                        </tr>
                        <tr>
                            <td class="tiet" style="text-align: center">3</td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet3_Lop1_1_Mon"><a href="#">Anh văn</a></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet3_Lop1_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet3_Lop1_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet3_Lop1_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet3_Lop2_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet3_Lop2_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet3_Lop2_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet3_Lop2_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet3_Lop2_5_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet3_Lop3_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet3_Lop3_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet3_Lop4_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet3_Lop5_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet3_Lop6_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet3_Lop6_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet3_Lop7_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet3_Lop10_2_Mon"></td>
                        </tr>
                        <tr>
                            <td class="tiet" style="text-align: center">4</td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet4_Lop1_1_Mon"><a href="#">Toán</a></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet4_Lop1_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet4_Lop1_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet4_Lop1_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet4_Lop2_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet4_Lop2_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet4_Lop2_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet4_Lop2_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet4_Lop2_5_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet4_Lop3_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet4_Lop3_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet4_Lop4_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet4_Lop5_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet4_Lop6_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet4_Lop6_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet4_Lop7_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet4_Lop10_2_Mon"></td>
                        </tr>
                        <tr>
                            <td rowspan="4">Chiều</td>
                            <td class="tiet" style="text-align: center">5</td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet5_Lop1_1_Mon"><a href="#">Toán TC</a></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet5_Lop1_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet5_Lop1_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet5_Lop1_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet5_Lop2_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet5_Lop2_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet5_Lop2_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet5_Lop2_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet5_Lop2_5_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet5_Lop3_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet5_Lop3_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet5_Lop4_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet5_Lop5_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet5_Lop6_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet5_Lop6_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet5_Lop7_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet5_Lop10_2_Mon"></td>
                        </tr>
                        <tr>
                            <td class="tiet" style="text-align: center">6</td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet6_Lop1_1_Mon"><a href="#">Thể dục</a></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet6_Lop1_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet6_Lop1_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet6_Lop1_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet6_Lop2_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet6_Lop2_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet6_Lop2_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet6_Lop2_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet6_Lop2_5_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet6_Lop3_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet6_Lop3_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet6_Lop4_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet6_Lop5_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet6_Lop6_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet6_Lop6_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet6_Lop7_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet6_Lop10_2_Mon"></td>
                        </tr>
                        <tr>
                            <td class="tiet" style="text-align: center">7</td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet7_Lop1_1_Mon"><a href="#">Đạo đức</a></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet7_Lop1_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet7_Lop1_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet7_Lop1_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet7_Lop2_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet7_Lop2_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet7_Lop2_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet7_Lop2_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet7_Lop2_5_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet7_Lop3_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet7_Lop3_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet7_Lop4_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet7_Lop5_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet7_Lop6_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet7_Lop6_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet7_Lop7_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet7_Lop10_2_Mon"></td>
                        </tr>
                        <tr>
                            <td class="tiet" style="text-align: center">8</td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet8_Lop1_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet8_Lop1_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet8_Lop1_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet8_Lop1_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet8_Lop2_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet8_Lop2_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet8_Lop2_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet8_Lop2_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet8_Lop2_5_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet8_Lop3_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet8_Lop3_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet8_Lop4_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet8_Lop5_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet8_Lop6_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet8_Lop6_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet8_Lop7_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu3_tiet8_Lop10_2_Mon"></td>
                        </tr>
                        <tr>
                            <td rowspan="8">
                                <span class="table_day">Thứ tư<br />
                                    <span runat="server" id="txt_Thu4_NgayHoc"></span>
                                </span>
                            </td>
                            <td rowspan="4">Sáng</td>
                            <td class="tiet" style="text-align: center">1</td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet1_Lop1_1_Mon"><a href="#">Học vần</a></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet1_Lop1_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet1_Lop1_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet1_Lop1_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet1_Lop2_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet1_Lop2_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet1_Lop2_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet1_Lop2_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet1_Lop2_5_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet1_Lop3_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet1_Lop3_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet1_Lop4_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet1_Lop5_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet1_Lop6_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet1_Lop6_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet1_Lop7_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet1_Lop10_2_Mon"></td>
                        </tr>
                        <tr>
                            <td class="tiet" style="text-align: center">2</td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet2_Lop1_1_Mon"><a href="#">Học vần</a></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet2_Lop1_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet2_Lop1_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet2_Lop1_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet2_Lop2_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet2_Lop2_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet2_Lop2_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet2_Lop2_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet2_Lop2_5_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet2_Lop3_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet2_Lop3_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet2_Lop4_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet2_Lop5_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet2_Lop6_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet2_Lop6_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet2_Lop7_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet2_Lop10_2_Mon"></td>
                        </tr>
                        <tr>
                            <td class="tiet" style="text-align: center">3</td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet3_Lop1_1_Mon"><a href="#">TV-TC</a></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet3_Lop1_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet3_Lop1_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet3_Lop1_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet3_Lop2_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet3_Lop2_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet3_Lop2_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet3_Lop2_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet3_Lop2_5_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet3_Lop3_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet3_Lop3_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet3_Lop4_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet3_Lop5_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet3_Lop6_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet3_Lop6_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet3_Lop7_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet3_Lop10_2_Mon"></td>
                        </tr>
                        <tr>
                            <td class="tiet" style="text-align: center">4</td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet4_Lop1_1_Mon"><a href="#">Toán TC</a></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet4_Lop1_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet4_Lop1_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet4_Lop1_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet4_Lop2_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet4_Lop2_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet4_Lop2_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet4_Lop2_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet4_Lop2_5_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet4_Lop3_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet4_Lop3_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet4_Lop4_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet4_Lop5_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet4_Lop6_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet4_Lop6_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet4_Lop7_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet4_Lop10_2_Mon"></td>
                        </tr>
                        <tr>
                            <td rowspan="4">Chiều</td>
                            <td class="tiet" style="text-align: center">5</td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet5_Lop1_1_Mon"><a href="#">Âm nhạc</a></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet5_Lop1_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet5_Lop1_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet5_Lop1_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet5_Lop2_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet5_Lop2_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet5_Lop2_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet5_Lop2_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet5_Lop2_5_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet5_Lop3_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet5_Lop3_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet5_Lop4_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet5_Lop5_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet5_Lop6_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet5_Lop6_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet5_Lop7_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet5_Lop10_2_Mon"></td>
                        </tr>
                        <tr>
                            <td class="tiet" style="text-align: center">6</td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet6_Lop1_1_Mon"><a href="#">TNXH</a></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet6_Lop1_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet6_Lop1_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet6_Lop1_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet6_Lop2_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet6_Lop2_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet6_Lop2_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet6_Lop2_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet6_Lop2_5_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet6_Lop3_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet6_Lop3_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet6_Lop4_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet6_Lop5_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet6_Lop6_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet6_Lop6_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet6_Lop7_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet6_Lop10_2_Mon"></td>
                        </tr>
                        <tr>
                            <td class="tiet" style="text-align: center">7</td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet7_Lop1_1_Mon"><a href="#">HĐTT</a></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet7_Lop1_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet7_Lop1_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet7_Lop1_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet7_Lop2_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet7_Lop2_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet7_Lop2_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet7_Lop2_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet7_Lop2_5_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet7_Lop3_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet7_Lop3_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet7_Lop4_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet7_Lop5_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet7_Lop6_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet7_Lop6_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet7_Lop7_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet7_Lop10_2_Mon"></td>
                        </tr>
                        <tr>
                            <td class="tiet" style="text-align: center">8</td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet8_Lop1_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet8_Lop1_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet8_Lop1_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet8_Lop1_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet8_Lop2_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet8_Lop2_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet8_Lop2_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet8_Lop2_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet8_Lop2_5_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet8_Lop3_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet8_Lop3_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet8_Lop4_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet8_Lop5_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet8_Lop6_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet8_Lop6_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet8_Lop7_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu4_tiet8_Lop10_2_Mon"></td>
                        </tr>
                        <tr>
                            <td rowspan="8">
                                <span class="table_day">Thứ năm<br />
                                    <span runat="server" id="txt_Thu5_NgayHoc"></span>
                                </span>
                            </td>
                            <td rowspan="4">Sáng</td>
                            <td class="tiet" style="text-align: center">1</td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet1_Lop1_1_Mon"><a href="#">Toán</a></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet1_Lop1_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet1_Lop1_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet1_Lop1_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet1_Lop2_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet1_Lop2_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet1_Lop2_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet1_Lop2_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet1_Lop2_5_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet1_Lop3_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet1_Lop3_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet1_Lop4_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet1_Lop5_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet1_Lop6_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet1_Lop6_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet1_Lop7_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet1_Lop10_2_Mon"></td>
                        </tr>
                        <tr>
                            <td class="tiet" style="text-align: center">2</td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet2_Lop1_1_Mon"><a href="#">Âm nhạc TC</a></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet2_Lop1_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet2_Lop1_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet2_Lop1_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet2_Lop2_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet2_Lop2_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet2_Lop2_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet2_Lop2_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet2_Lop2_5_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet2_Lop3_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet2_Lop3_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet2_Lop4_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet2_Lop5_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet2_Lop6_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet2_Lop6_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet2_Lop7_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet2_Lop10_2_Mon"></td>
                        </tr>
                        <tr>
                            <td class="tiet" style="text-align: center">3</td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet3_Lop1_1_Mon"><a href="#">Học vần</a></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet3_Lop1_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet3_Lop1_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet3_Lop1_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet3_Lop2_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet3_Lop2_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet3_Lop2_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet3_Lop2_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet3_Lop2_5_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet3_Lop3_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet3_Lop3_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet3_Lop4_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet3_Lop5_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet3_Lop6_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet3_Lop6_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet3_Lop7_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet3_Lop10_2_Mon"></td>
                        </tr>
                        <tr>
                            <td class="tiet" style="text-align: center">4</td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet4_Lop1_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet4_Lop1_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet4_Lop1_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet4_Lop1_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet4_Lop2_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet4_Lop2_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet4_Lop2_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet4_Lop2_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet4_Lop2_5_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet4_Lop3_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet4_Lop3_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet4_Lop4_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet4_Lop5_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet4_Lop6_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet4_Lop6_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet4_Lop7_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet4_Lop10_2_Mon"></td>
                        </tr>
                        <tr>
                            <td rowspan="4">Chiều</td>
                            <td class="tiet" style="text-align: center">5</td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet5_Lop1_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet5_Lop1_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet5_Lop1_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet5_Lop1_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet5_Lop2_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet5_Lop2_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet5_Lop2_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet5_Lop2_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet5_Lop2_5_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet5_Lop3_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet5_Lop3_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet5_Lop4_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet5_Lop5_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet5_Lop6_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet5_Lop6_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet5_Lop7_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet5_Lop10_2_Mon"></td>
                        </tr>
                        <tr>
                            <td class="tiet" style="text-align: center">6</td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet6_Lop1_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet6_Lop1_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet6_Lop1_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet6_Lop1_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet6_Lop2_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet6_Lop2_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet6_Lop2_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet6_Lop2_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet6_Lop2_5_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet6_Lop3_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet6_Lop3_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet6_Lop4_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet6_Lop5_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet6_Lop6_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet6_Lop6_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet6_Lop7_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet6_Lop10_2_Mon"></td>
                        </tr>
                        <tr>
                            <td class="tiet" style="text-align: center">7</td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet7_Lop1_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet7_Lop1_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet7_Lop1_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet7_Lop1_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet7_Lop2_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet7_Lop2_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet7_Lop2_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet7_Lop2_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet7_Lop2_5_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet7_Lop3_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet7_Lop3_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet7_Lop4_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet7_Lop5_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet7_Lop6_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet7_Lop6_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet7_Lop7_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet7_Lop10_2_Mon"></td>
                        </tr>
                        <tr>
                            <td class="tiet" style="text-align: center">8</td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet8_Lop1_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet8_Lop1_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet8_Lop1_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet8_Lop1_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet8_Lop2_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet8_Lop2_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet8_Lop2_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet8_Lop2_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet8_Lop2_5_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet8_Lop3_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet8_Lop3_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet8_Lop4_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet8_Lop5_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet8_Lop6_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet8_Lop6_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet8_Lop7_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu5_tiet8_Lop10_2_Mon"></td>
                        </tr>
                        <tr>
                            <td rowspan="8">
                                <span class="table_day">Thứ sáu<br />
                                    <span runat="server" id="txt_Thu6_NgayHoc"></span>
                                </span>
                            </td>
                            <td rowspan="4">Sáng</td>
                            <td class="tiet" style="text-align: center">1</td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet1_Lop1_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet1_Lop1_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet1_Lop1_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet1_Lop1_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet1_Lop2_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet1_Lop2_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet1_Lop2_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet1_Lop2_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet1_Lop2_5_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet1_Lop3_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet1_Lop3_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet1_Lop4_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet1_Lop5_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet1_Lop6_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet1_Lop6_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet1_Lop7_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet1_Lop10_2_Mon"></td>
                        </tr>
                        <tr>
                            <td class="tiet" style="text-align: center">2</td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet2_Lop1_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet2_Lop1_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet2_Lop1_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet2_Lop1_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet2_Lop2_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet2_Lop2_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet2_Lop2_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet2_Lop2_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet2_Lop2_5_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet2_Lop3_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet2_Lop3_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet2_Lop4_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet2_Lop5_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet2_Lop6_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet2_Lop6_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet2_Lop7_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet2_Lop10_2_Mon"></td>
                        </tr>
                        <tr>
                            <td class="tiet" style="text-align: center">3</td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet3_Lop1_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet3_Lop1_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet3_Lop1_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet3_Lop1_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet3_Lop2_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet3_Lop2_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet3_Lop2_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet3_Lop2_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet3_Lop2_5_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet3_Lop3_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet3_Lop3_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet3_Lop4_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet3_Lop5_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet3_Lop6_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet3_Lop6_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet3_Lop7_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet3_Lop10_2_Mon"></td>
                        </tr>
                        <tr>
                            <td class="tiet" style="text-align: center">4</td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet4_Lop1_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet4_Lop1_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet4_Lop1_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet4_Lop1_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet4_Lop2_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet4_Lop2_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet4_Lop2_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet4_Lop2_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet4_Lop2_5_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet4_Lop3_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet4_Lop3_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet4_Lop4_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet4_Lop5_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet4_Lop6_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet4_Lop6_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet4_Lop7_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet4_Lop10_2_Mon"></td>
                        </tr>
                        <tr>
                            <td rowspan="4">Chiều</td>
                            <td class="tiet" style="text-align: center">5</td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet5_Lop1_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet5_Lop1_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet5_Lop1_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet5_Lop1_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet5_Lop2_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet5_Lop2_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet5_Lop2_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet5_Lop2_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet5_Lop2_5_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet5_Lop3_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet5_Lop3_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet5_Lop4_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet5_Lop5_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet5_Lop6_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet5_Lop6_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet5_Lop7_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet5_Lop10_2_Mon"></td>
                        </tr>
                        <tr>
                            <td class="tiet" style="text-align: center">6</td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet6_Lop1_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet6_Lop1_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet6_Lop1_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet6_Lop1_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet6_Lop2_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet6_Lop2_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet6_Lop2_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet6_Lop2_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet6_Lop2_5_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet6_Lop3_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet6_Lop3_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet6_Lop4_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet6_Lop5_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet6_Lop6_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet6_Lop6_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet6_Lop7_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet6_Lop10_2_Mon"></td>
                        </tr>
                        <tr>
                            <td class="tiet" style="text-align: center">7</td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet7_Lop1_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet7_Lop1_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet7_Lop1_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet7_Lop1_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet7_Lop2_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet7_Lop2_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet7_Lop2_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet7_Lop2_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet7_Lop2_5_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet7_Lop3_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet7_Lop3_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet7_Lop4_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet7_Lop5_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet7_Lop6_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet7_Lop6_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet7_Lop7_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet7_Lop10_2_Mon"></td>
                        </tr>
                        <tr>
                            <td class="tiet" style="text-align: center">8</td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet8_Lop1_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet8_Lop1_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet8_Lop1_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet8_Lop1_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet8_Lop2_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet8_Lop2_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet8_Lop2_3_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet8_Lop2_4_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet8_Lop2_5_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet8_Lop3_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet8_Lop3_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet8_Lop4_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet8_Lop5_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet8_Lop6_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet8_Lop6_2_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet8_Lop7_1_Mon"></td>
                            <td class="tiet" runat="server" id="txt_Thu6_tiet8_Lop10_2_Mon"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

