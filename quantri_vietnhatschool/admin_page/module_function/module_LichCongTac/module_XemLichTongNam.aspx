﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="module_XemLichTongNam.aspx.cs" Inherits="admin_page_module_function_module_LichCongTac_module_XemLichTongNam" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../../admin_css/vendor.css" rel="stylesheet" />
    <script src="../../../admin_js/sweetalert.min.js"></script>
    <script src="../../../js/jquery.min.js"></script>
</head>
<body>
    <style>
        #box-modal {
            width: 1000px;
            height: 298px;
            display: block;
            position: fixed;
            top: 20%;
            left: 14%;
            box-shadow: 7px 6px 16px burlywood;
            border-radius: 16px;
            animation-name: modal;
            animation-duration: 2s;
            z-index: 1000;
            background-color: #e3e8e6;
            outline: none;
        }

            #box-modal .name {
                position: absolute;
                top: 10%;
                left: 31%;
                font-size: 30px;
                font-weight: bold;
                color: #1f58c1;
            }

            #box-modal .table {
                position: absolute;
                top: 30%;
                left: 2%;
                width: 95%;
                text-align: center;
            }

            #box-modal #btnTieptuc {
                position: absolute;
                right: 7%;
                bottom: 13%;
                background-color: azure;
                color: green;
                font-weight: bold;
            }

            #box-modal #btnDangKyDuGio {
                position: absolute;
                right: 20%;
                bottom: 13%;
                background-color: azure;
                color: green;
                font-weight: bold;
            }

        @keyframes modal {
            from {
                top: 0%;
                left: 14%;
            }

            to {
                top: 20%;
                left: 14%;
            }
        }

        .table a {
            color: #000;
        }

        #header-fixed {
            position: fixed;
            top: 0px;
            display: none;
            background-color: white;
            width: 97.2%;
        }

            #header-fixed th {
                padding: 0px 12px;
            }

            #header-fixed .head_table th:nth-child(3) {
                padding: 0px 10px;
            }

    </style>
   
    <form id="form1" runat="server">
        <asp:ScriptManager ID="smScriptManager" runat="server"></asp:ScriptManager>
        <div>
            <div class="card card-block">
                <div id="dvKetQua" runat="server">
                    <br />
                    <br />
                    <div>
                        <p>
                            <b>SỞ GIÁO DỤC & ĐÀO TẠO ĐÀ NẴNG</b>
                            <br />
                            <b>TRƯỜNG TH-THCS & THPT VIỆT NHẬT</b>
                            <a href="/admin-xem-lich-bao-giang-giao-vien" class="btn btn-primary" style="float: right">Xem chi tiết giáo viên</a>
                            <a href="/admin-home" class="btn btn-primary" style="float: right">Quay lại</a>
                        </p>

                        <div style="text-align: center">
                            <h3><b>THỜI KHÓA BIỂU NĂM 2020 - 2021 </b>

                            </h3>
                            <br />
                            <p><b><%=tuanhoc %><b></p>
                        </div>
                        <div class="row">
                            <div class="col-3" style="margin: 0 auto; float: unset;">
                                <span style="display: flex; align-items: center;" class="mb-2">Tuần:&nbsp;<dx:ASPxComboBox ID="ddlTuanHoc" runat="server" ValueType="System.Int32" TextField="tuan_name" ValueField="tuan_id" NullText="Chọn tuần học" ClientInstanceName="ddlTuanHoc" CssClass="" Width="100%" OnSelectedIndexChanged="ddlTuanHoc_SelectedIndexChanged" AutoPostBack="true"></dx:ASPxComboBox>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div style="overflow-x: auto;">
                        <table class="table table-bordered" id="table-1" border="1">
                            <thead>
                                <tr class="head_table">
                                    <th class="table_header" scope="col">Thứ</th>
                                    <th scope="col">Buổi</th>
                                    <th scope="col">Tiết</th>
                                    <th>Lớp 1/1</th>
                                    <th>Lớp 1/2</th>
                                    <th>Lớp 1/3</th>
                                    <th>Lớp 1/4</th>
                                    <th>Lớp 2/1</th>
                                    <th>Lớp 2/2</th>
                                    <th>Lớp 2/3</th>
                                    <th>Lớp 2/4</th>
                                    <th>Lớp 3/1</th>
                                    <th>Lớp 3/2</th>
                                    <th>Lớp 3/3</th>
                                    <th>Lớp 3/4</th>
                                    <th>Lớp 3/5</th>
                                    <th>Lớp 4/1</th>
                                    <th>Lớp 4/2</th>
                                    <th>Lớp 5/1</th>
                                    <th>Lớp 6/1</th>
                                    <th>Lớp 7/1</th>
                                    <th>Lớp 7/2</th>
                                    <th>Lớp 8/1</th>
                                    <th>Lớp 10/1</th>
                                    <th>Lớp 11/1</th>
                                </tr>
                            </thead>
                            <tbody>

                                <%--thứ 2--%>
                                <tr>
                                    <td rowspan="8">
                                        <span class="table_day">Thứ hai<br />
                                            <span runat="server" id="txt_Thu2_NgayHoc"></span>
                                        </span>
                                    </td>
                                    <td rowspan="4">Sáng</td>
                                    <td class="tiet" style="text-align: center">1</td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet1_Lop1_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet1_Lop1_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet1_Lop1_1_Id %>)"><%=lbl_Thu2_tiet1_Lop1_1_Mon %><%=lbl_Thu2_GiaoVien1_Lop1_1 %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet1_Lop1_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet1_Lop1_2_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet1_Lop1_2_Id %>)"><%=lbl_Thu2_tiet1_Lop1_2_Mon %><%=lbl_Thu2_GiaoVien1_Lop1_2 %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet1_Lop1_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet1_Lop1_3_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet1_Lop1_3_Id %>)"><%=lbl_Thu2_tiet1_Lop1_3_Mon %><%=lbl_Thu2_GiaoVien1_Lop1_3 %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet1_Lop1_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet1_Lop1_4_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet1_Lop1_4_Id %>)"><%=lbl_Thu2_tiet1_Lop1_4_Mon %><%=lbl_Thu2_GiaoVien1_Lop1_4 %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet1_Lop2_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet1_Lop2_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet1_Lop2_1_Id %>)"><%=lbl_Thu2_tiet1_Lop2_1_Mon %><%=lbl_Thu2_GiaoVien1_Lop2_1 %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet1_Lop2_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet1_Lop2_2_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet1_Lop2_2_Id %>)"><%=lbl_Thu2_tiet1_Lop2_2_Mon %><%=lbl_Thu2_GiaoVien1_Lop2_2 %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet1_Lop2_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet1_Lop2_3_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet1_Lop2_3_Id %>)"><%=lbl_Thu2_tiet1_Lop2_3_Mon %><%=lbl_Thu2_GiaoVien1_Lop2_3 %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet1_Lop2_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet1_Lop2_4_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet1_Lop2_4_Id %>)"><%=lbl_Thu2_tiet1_Lop2_4_Mon %><%=lbl_Thu2_GiaoVien1_Lop2_4 %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet1_Lop3_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet1_Lop3_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet1_Lop3_1_Id %>)"><%=lbl_Thu2_tiet1_Lop3_1_Mon %><%=lbl_Thu2_GiaoVien1_Lop3_1 %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet1_Lop3_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet1_Lop3_2_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet1_Lop3_2_Id %>)"><%=lbl_Thu2_tiet1_Lop3_2_Mon %><%=lbl_Thu2_GiaoVien1_Lop3_2 %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet1_Lop3_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet1_Lop3_3_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet1_Lop3_3_Id %>)"><%=lbl_Thu2_tiet1_Lop3_3_Mon %><%=lbl_Thu2_GiaoVien1_Lop3_3 %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet1_Lop3_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet1_Lop3_4_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet1_Lop3_4_Id %>)"><%=lbl_Thu2_tiet1_Lop3_4_Mon %><%=lbl_Thu2_GiaoVien1_Lop3_4 %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet1_Lop3_5_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet1_Lop3_5_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet1_Lop3_5_Id %>)"><%=lbl_Thu2_tiet1_Lop3_5_Mon %><%=lbl_Thu2_GiaoVien1_Lop3_5 %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet1_Lop4_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet1_Lop4_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet1_Lop4_1_Id %>)"><%=lbl_Thu2_tiet1_Lop4_1_Mon %><%=lbl_Thu2_GiaoVien1_Lop4_1 %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet1_Lop4_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet1_Lop4_2_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet1_Lop4_2_Id %>)"><%=lbl_Thu2_tiet1_Lop4_2_Mon %><%=lbl_Thu2_GiaoVien1_Lop4_2 %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet1_Lop5_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet1_Lop5_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet1_Lop5_1_Id %>)"><%=lbl_Thu2_tiet1_Lop5_1_Mon %><%=lbl_Thu2_GiaoVien1_Lop5_1 %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet1_Lop6_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet1_Lop6_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet1_Lop6_1_Id %>)"><%=lbl_Thu2_tiet1_Lop6_1_Mon %><%=lbl_Thu2_GiaoVien1_Lop6_1 %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet1_Lop7_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet1_Lop7_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet1_Lop7_1_Id %>)"><%=lbl_Thu2_tiet1_Lop7_1_Mon %><%=lbl_Thu2_GiaoVien1_Lop7_1 %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet1_Lop7_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet1_Lop7_2_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet1_Lop7_2_Id %>)"><%=lbl_Thu2_tiet1_Lop7_2_Mon %><%=lbl_Thu2_GiaoVien1_Lop7_2 %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet1_Lop8_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet1_Lop8_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet1_Lop8_1_Id %>)"><%=lbl_Thu2_tiet1_Lop8_1_Mon %><%=lbl_Thu2_GiaoVien1_Lop8_1 %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet1_Lop10_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet1_Lop10_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet1_Lop10_1_Id %>)"><%=lbl_Thu2_tiet1_Lop10_1_Mon %><%=lbl_Thu2_GiaoVien1_Lop10_1 %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet1_Lop11_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet1_Lop11_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet1_Lop11_1_Id %>)"><%=lbl_Thu2_tiet1_Lop11_1_Mon %><%=lbl_Thu2_GiaoVien1_Lop11_1 %></a></td>
                                </tr>
                                <tr>
                                    <td class="tiet" style="text-align: center">2</td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet2_Lop1_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet2_Lop1_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet2_Lop1_1_Id %>)"><%=lbl_Thu2_tiet2_Lop1_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet2_Lop1_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet2_Lop1_2_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet2_Lop1_2_Id %>)"><%=lbl_Thu2_tiet2_Lop1_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet2_Lop1_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet2_Lop1_3_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet2_Lop1_3_Id %>)"><%=lbl_Thu2_tiet2_Lop1_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet2_Lop1_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet2_Lop1_4_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet2_Lop1_4_Id %>)"><%=lbl_Thu2_tiet2_Lop1_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet2_Lop2_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet2_Lop2_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet2_Lop2_1_Id %>)"><%=lbl_Thu2_tiet2_Lop2_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet2_Lop2_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet2_Lop2_2_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet2_Lop2_2_Id %>)"><%=lbl_Thu2_tiet2_Lop2_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet2_Lop2_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet2_Lop2_3_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet2_Lop2_3_Id %>)"><%=lbl_Thu2_tiet2_Lop2_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet2_Lop2_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet2_Lop2_4_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet2_Lop2_4_Id %>)"><%=lbl_Thu2_tiet2_Lop2_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet2_Lop3_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet2_Lop3_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet2_Lop3_1_Id %>)"><%=lbl_Thu2_tiet2_Lop3_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet2_Lop3_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet2_Lop3_2_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet2_Lop3_2_Id %>)"><%=lbl_Thu2_tiet2_Lop3_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet2_Lop3_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet2_Lop3_3_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet2_Lop3_3_Id %>)"><%=lbl_Thu2_tiet2_Lop3_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet2_Lop3_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet2_Lop3_4_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet2_Lop3_4_Id %>)"><%=lbl_Thu2_tiet2_Lop3_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet2_Lop3_5_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet2_Lop3_5_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet2_Lop3_5_Id %>)"><%=lbl_Thu2_tiet2_Lop3_5_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet2_Lop4_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet2_Lop4_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet2_Lop4_1_Id %>)"><%=lbl_Thu2_tiet2_Lop4_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet2_Lop4_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet2_Lop4_2_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet2_Lop4_2_Id %>)"><%=lbl_Thu2_tiet2_Lop4_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet2_Lop5_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet2_Lop5_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet2_Lop5_1_Id %>)"><%=lbl_Thu2_tiet2_Lop5_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet2_Lop6_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet2_Lop6_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet2_Lop6_1_Id %>)"><%=lbl_Thu2_tiet2_Lop6_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet2_Lop7_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet2_Lop7_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet2_Lop7_1_Id %>)"><%=lbl_Thu2_tiet2_Lop7_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet2_Lop7_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet2_Lop7_2_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet2_Lop7_2_Id %>)"><%=lbl_Thu2_tiet2_Lop7_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet2_Lop8_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet2_Lop8_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet2_Lop8_1_Id %>)"><%=lbl_Thu2_tiet2_Lop8_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet2_Lop10_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet2_Lop10_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet2_Lop10_1_Id %>)"><%=lbl_Thu2_tiet2_Lop10_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet2_Lop11_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet2_Lop11_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet2_Lop11_1_Id %>)"><%=lbl_Thu2_tiet2_Lop11_1_Mon %></a></td>
                                </tr>
                                <tr>
                                    <td class="tiet" style="text-align: center">3</td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet3_Lop1_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet3_Lop1_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet3_Lop1_1_Id %>)"><%=lbl_Thu2_tiet3_Lop1_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet3_Lop1_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet3_Lop1_2_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet3_Lop1_2_Id %>)"><%=lbl_Thu2_tiet3_Lop1_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet3_Lop1_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet3_Lop1_3_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet3_Lop1_3_Id %>)"><%=lbl_Thu2_tiet3_Lop1_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet3_Lop1_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet3_Lop1_4_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet3_Lop1_4_Id %>)"><%=lbl_Thu2_tiet3_Lop1_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet3_Lop2_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet3_Lop2_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet3_Lop2_1_Id %>)"><%=lbl_Thu2_tiet3_Lop2_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet3_Lop2_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet3_Lop2_2_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet3_Lop2_2_Id %>)"><%=lbl_Thu2_tiet3_Lop2_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet3_Lop2_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet3_Lop2_3_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet3_Lop2_3_Id %>)"><%=lbl_Thu2_tiet3_Lop2_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet3_Lop2_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet3_Lop2_4_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet3_Lop2_4_Id %>)"><%=lbl_Thu2_tiet3_Lop2_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet3_Lop3_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet3_Lop3_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet3_Lop3_1_Id %>)"><%=lbl_Thu2_tiet3_Lop3_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet3_Lop3_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet3_Lop3_2_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet3_Lop3_2_Id %>)"><%=lbl_Thu2_tiet3_Lop3_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet3_Lop3_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet3_Lop3_3_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet3_Lop3_3_Id %>)"><%=lbl_Thu2_tiet3_Lop3_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet3_Lop3_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet3_Lop3_4_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet3_Lop3_4_Id %>)"><%=lbl_Thu2_tiet3_Lop3_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet3_Lop3_5_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet3_Lop3_5_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet3_Lop3_5_Id %>)"><%=lbl_Thu2_tiet3_Lop3_5_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet3_Lop4_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet3_Lop4_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet3_Lop4_1_Id %>)"><%=lbl_Thu2_tiet3_Lop4_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet3_Lop4_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet3_Lop4_2_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet3_Lop4_2_Id %>)"><%=lbl_Thu2_tiet3_Lop4_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet3_Lop5_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet3_Lop5_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet3_Lop5_1_Id %>)"><%=lbl_Thu2_tiet3_Lop5_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet3_Lop6_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet3_Lop6_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet3_Lop6_1_Id %>)"><%=lbl_Thu2_tiet3_Lop6_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet3_Lop7_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet3_Lop7_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet3_Lop7_1_Id %>)"><%=lbl_Thu2_tiet3_Lop7_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet3_Lop7_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet3_Lop7_2_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet3_Lop7_2_Id %>)"><%=lbl_Thu2_tiet3_Lop7_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet3_Lop8_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet3_Lop8_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet3_Lop8_1_Id %>)"><%=lbl_Thu2_tiet3_Lop8_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet3_Lop10_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet3_Lop10_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet3_Lop10_1_Id %>)"><%=lbl_Thu2_tiet3_Lop10_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet3_Lop11_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet3_Lop11_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet3_Lop11_1_Id %>)"><%=lbl_Thu2_tiet3_Lop11_1_Mon %></a></td>
                                </tr>
                                <tr>
                                    <td class="tiet" style="text-align: center">4</td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet4_Lop1_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet4_Lop1_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet4_Lop1_1_Id %>)"><%=lbl_Thu2_tiet4_Lop1_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet4_Lop1_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet4_Lop1_2_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet4_Lop1_2_Id %>)"><%=lbl_Thu2_tiet4_Lop1_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet4_Lop1_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet4_Lop1_3_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet4_Lop1_3_Id %>)"><%=lbl_Thu2_tiet4_Lop1_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet4_Lop1_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet4_Lop1_4_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet4_Lop1_4_Id %>)"><%=lbl_Thu2_tiet4_Lop1_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet4_Lop2_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet4_Lop2_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet4_Lop2_1_Id %>)"><%=lbl_Thu2_tiet4_Lop2_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet4_Lop2_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet4_Lop2_2_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet4_Lop2_2_Id %>)"><%=lbl_Thu2_tiet4_Lop2_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet4_Lop2_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet4_Lop2_3_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet4_Lop2_3_Id %>)"><%=lbl_Thu2_tiet4_Lop2_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet4_Lop2_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet4_Lop2_4_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet4_Lop2_4_Id %>)"><%=lbl_Thu2_tiet4_Lop2_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet4_Lop3_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet4_Lop3_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet4_Lop3_1_Id %>)"><%=lbl_Thu2_tiet4_Lop3_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet4_Lop3_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet4_Lop3_2_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet4_Lop3_2_Id %>)"><%=lbl_Thu2_tiet4_Lop3_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet4_Lop3_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet4_Lop3_3_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet4_Lop3_3_Id %>)"><%=lbl_Thu2_tiet4_Lop3_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet4_Lop3_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet4_Lop3_4_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet4_Lop3_4_Id %>)"><%=lbl_Thu2_tiet4_Lop3_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet4_Lop3_5_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet4_Lop3_5_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet4_Lop3_5_Id %>)"><%=lbl_Thu2_tiet4_Lop3_5_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet4_Lop4_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet4_Lop4_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet4_Lop4_1_Id %>)"><%=lbl_Thu2_tiet4_Lop4_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet4_Lop4_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet4_Lop4_2_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet4_Lop4_2_Id %>)"><%=lbl_Thu2_tiet4_Lop4_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet4_Lop5_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet4_Lop5_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet4_Lop5_1_Id %>)"><%=lbl_Thu2_tiet4_Lop5_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet4_Lop6_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet4_Lop6_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet4_Lop6_1_Id %>)"><%=lbl_Thu2_tiet4_Lop6_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet4_Lop7_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet4_Lop7_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet4_Lop7_1_Id %>)"><%=lbl_Thu2_tiet4_Lop7_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet4_Lop7_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet4_Lop7_2_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet4_Lop7_2_Id %>)"><%=lbl_Thu2_tiet4_Lop7_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet4_Lop8_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet4_Lop8_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet4_Lop8_1_Id %>)"><%=lbl_Thu2_tiet4_Lop8_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet4_Lop10_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet4_Lop10_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet4_Lop10_1_Id %>)"><%=lbl_Thu2_tiet4_Lop10_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet4_Lop11_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet4_Lop11_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet4_Lop11_1_Id %>)"><%=lbl_Thu2_tiet4_Lop11_1_Mon %></a></td>
                                </tr>
                                <tr>
                                    <td rowspan="4">Chiều</td>
                                    <td class="tiet" style="text-align: center">5</td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet5_Lop1_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet5_Lop1_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet5_Lop1_1_Id %>)"><%=lbl_Thu2_tiet5_Lop1_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet5_Lop1_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet5_Lop1_2_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet5_Lop1_2_Id %>)"><%=lbl_Thu2_tiet5_Lop1_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet5_Lop1_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet5_Lop1_3_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet5_Lop1_3_Id %>)"><%=lbl_Thu2_tiet5_Lop1_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet5_Lop1_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet5_Lop1_4_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet5_Lop1_4_Id %>)"><%=lbl_Thu2_tiet5_Lop1_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet5_Lop2_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet5_Lop2_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet5_Lop2_1_Id %>)"><%=lbl_Thu2_tiet5_Lop2_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet5_Lop2_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet5_Lop2_2_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet5_Lop2_2_Id %>)"><%=lbl_Thu2_tiet5_Lop2_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet5_Lop2_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet5_Lop2_3_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet5_Lop2_3_Id %>)"><%=lbl_Thu2_tiet5_Lop2_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet5_Lop2_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet5_Lop2_4_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet5_Lop2_4_Id %>)"><%=lbl_Thu2_tiet5_Lop2_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet5_Lop3_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet5_Lop3_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet5_Lop3_1_Id %>)"><%=lbl_Thu2_tiet5_Lop3_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet5_Lop3_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet5_Lop3_2_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet5_Lop3_2_Id %>)"><%=lbl_Thu2_tiet5_Lop3_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet5_Lop3_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet5_Lop3_3_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet5_Lop3_3_Id %>)"><%=lbl_Thu2_tiet5_Lop3_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet5_Lop3_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet5_Lop3_4_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet5_Lop3_4_Id %>)"><%=lbl_Thu2_tiet5_Lop3_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet5_Lop3_5_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet5_Lop3_5_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet5_Lop3_5_Id %>)"><%=lbl_Thu2_tiet5_Lop3_5_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet5_Lop4_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet5_Lop4_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet5_Lop4_1_Id %>)"><%=lbl_Thu2_tiet5_Lop4_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet5_Lop4_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet5_Lop4_2_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet5_Lop4_2_Id %>)"><%=lbl_Thu2_tiet5_Lop4_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet5_Lop5_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet5_Lop5_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet5_Lop5_1_Id %>)"><%=lbl_Thu2_tiet5_Lop5_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet5_Lop6_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet5_Lop6_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet5_Lop6_1_Id %>)"><%=lbl_Thu2_tiet5_Lop6_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet5_Lop7_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet5_Lop7_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet5_Lop7_1_Id %>)"><%=lbl_Thu2_tiet5_Lop7_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet5_Lop7_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet5_Lop7_2_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet5_Lop7_2_Id %>)"><%=lbl_Thu2_tiet5_Lop7_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet5_Lop8_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet5_Lop8_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet5_Lop8_1_Id %>)"><%=lbl_Thu2_tiet5_Lop8_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet5_Lop10_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet5_Lop10_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet5_Lop10_1_Id %>)"><%=lbl_Thu2_tiet5_Lop10_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet5_Lop11_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet5_Lop11_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet5_Lop11_1_Id %>)"><%=lbl_Thu2_tiet5_Lop11_1_Mon %></a></td>
                                </tr>
                                <tr>
                                    <td class="tiet" style="text-align: center">6</td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet6_Lop1_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet6_Lop1_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet6_Lop1_1_Id %>)"><%=lbl_Thu2_tiet6_Lop1_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet6_Lop1_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet6_Lop1_2_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet6_Lop1_2_Id %>)"><%=lbl_Thu2_tiet6_Lop1_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet6_Lop1_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet6_Lop1_3_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet6_Lop1_3_Id %>)"><%=lbl_Thu2_tiet6_Lop1_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet6_Lop1_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet6_Lop1_4_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet6_Lop1_4_Id %>)"><%=lbl_Thu2_tiet6_Lop1_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet6_Lop2_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet6_Lop2_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet6_Lop2_1_Id %>)"><%=lbl_Thu2_tiet6_Lop2_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet6_Lop2_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet6_Lop2_2_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet6_Lop2_2_Id %>)"><%=lbl_Thu2_tiet6_Lop2_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet6_Lop2_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet6_Lop2_3_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet6_Lop2_3_Id %>)"><%=lbl_Thu2_tiet6_Lop2_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet6_Lop2_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet6_Lop2_4_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet6_Lop2_4_Id %>)"><%=lbl_Thu2_tiet6_Lop2_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet6_Lop3_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet6_Lop3_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet6_Lop3_1_Id %>)"><%=lbl_Thu2_tiet6_Lop3_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet6_Lop3_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet6_Lop3_2_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet6_Lop3_2_Id %>)"><%=lbl_Thu2_tiet6_Lop3_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet6_Lop3_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet6_Lop3_3_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet6_Lop3_3_Id %>)"><%=lbl_Thu2_tiet6_Lop3_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet6_Lop3_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet6_Lop3_4_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet6_Lop3_4_Id %>)"><%=lbl_Thu2_tiet6_Lop3_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet6_Lop3_5_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet6_Lop3_5_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet6_Lop3_5_Id %>)"><%=lbl_Thu2_tiet6_Lop3_5_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet6_Lop4_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet6_Lop4_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet6_Lop4_1_Id %>)"><%=lbl_Thu2_tiet6_Lop4_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet6_Lop4_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet6_Lop4_2_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet6_Lop4_2_Id %>)"><%=lbl_Thu2_tiet6_Lop4_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet6_Lop5_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet6_Lop5_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet6_Lop5_1_Id %>)"><%=lbl_Thu2_tiet6_Lop5_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet6_Lop6_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet6_Lop6_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet6_Lop6_1_Id %>)"><%=lbl_Thu2_tiet6_Lop6_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet6_Lop7_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet6_Lop7_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet6_Lop7_1_Id %>)"><%=lbl_Thu2_tiet6_Lop7_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet6_Lop7_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet6_Lop7_2_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet6_Lop7_2_Id %>)"><%=lbl_Thu2_tiet6_Lop7_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet6_Lop8_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet6_Lop8_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet6_Lop8_1_Id %>)"><%=lbl_Thu2_tiet6_Lop8_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet6_Lop10_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet6_Lop10_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet6_Lop10_1_Id %>)"><%=lbl_Thu2_tiet6_Lop10_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet6_Lop11_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet6_Lop11_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet6_Lop11_1_Id %>)"><%=lbl_Thu2_tiet6_Lop11_1_Mon %></a></td>
                                </tr>
                                <tr>
                                    <td class="tiet" style="text-align: center">7</td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet7_Lop1_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet7_Lop1_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet7_Lop1_1_Id %>)"><%=lbl_Thu2_tiet7_Lop1_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet7_Lop1_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet7_Lop1_2_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet7_Lop1_2_Id %>)"><%=lbl_Thu2_tiet7_Lop1_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet7_Lop1_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet7_Lop1_3_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet7_Lop1_3_Id %>)"><%=lbl_Thu2_tiet7_Lop1_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet7_Lop1_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet7_Lop1_4_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet7_Lop1_4_Id %>)"><%=lbl_Thu2_tiet7_Lop1_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet7_Lop2_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet7_Lop2_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet7_Lop2_1_Id %>)"><%=lbl_Thu2_tiet7_Lop2_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet7_Lop2_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet7_Lop2_2_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet7_Lop2_2_Id %>)"><%=lbl_Thu2_tiet7_Lop2_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet7_Lop2_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet7_Lop2_3_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet7_Lop2_3_Id %>)"><%=lbl_Thu2_tiet7_Lop2_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet7_Lop2_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet7_Lop2_4_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet7_Lop2_4_Id %>)"><%=lbl_Thu2_tiet7_Lop2_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet7_Lop3_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet7_Lop3_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet7_Lop3_1_Id %>)"><%=lbl_Thu2_tiet7_Lop3_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet7_Lop3_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet7_Lop3_2_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet7_Lop3_2_Id %>)"><%=lbl_Thu2_tiet7_Lop3_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet7_Lop3_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet7_Lop3_3_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet7_Lop3_3_Id %>)"><%=lbl_Thu2_tiet7_Lop3_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet7_Lop3_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet7_Lop3_4_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet7_Lop3_4_Id %>)"><%=lbl_Thu2_tiet7_Lop3_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet7_Lop3_5_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet7_Lop3_5_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet7_Lop3_5_Id %>)"><%=lbl_Thu2_tiet7_Lop3_5_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet7_Lop4_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet7_Lop4_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet7_Lop4_1_Id %>)"><%=lbl_Thu2_tiet7_Lop4_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet7_Lop4_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet7_Lop4_2_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet7_Lop4_2_Id %>)"><%=lbl_Thu2_tiet7_Lop4_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet7_Lop5_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet7_Lop5_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet7_Lop5_1_Id %>)"><%=lbl_Thu2_tiet7_Lop5_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet7_Lop6_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet7_Lop6_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet7_Lop6_1_Id %>)"><%=lbl_Thu2_tiet7_Lop6_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet7_Lop7_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet7_Lop7_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet7_Lop7_1_Id %>)"><%=lbl_Thu2_tiet7_Lop7_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet7_Lop7_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet7_Lop7_2_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet7_Lop7_2_Id %>)"><%=lbl_Thu2_tiet7_Lop7_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet7_Lop8_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet7_Lop8_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet7_Lop8_1_Id %>)"><%=lbl_Thu2_tiet7_Lop8_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet7_Lop10_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet7_Lop10_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet7_Lop10_1_Id %>)"><%=lbl_Thu2_tiet7_Lop10_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet7_Lop11_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet7_Lop11_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet7_Lop11_1_Id %>)"><%=lbl_Thu2_tiet7_Lop11_1_Mon %></a></td>
                                </tr>
                                <tr>
                                    <td class="tiet" style="text-align: center">8</td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet8_Lop1_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet8_Lop1_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet8_Lop1_1_Id %>)"><%=lbl_Thu2_tiet8_Lop1_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet8_Lop1_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet8_Lop1_2_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet8_Lop1_2_Id %>)"><%=lbl_Thu2_tiet8_Lop1_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet8_Lop1_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet8_Lop1_3_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet8_Lop1_3_Id %>)"><%=lbl_Thu2_tiet8_Lop1_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet8_Lop1_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet8_Lop1_4_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet8_Lop1_4_Id %>)"><%=lbl_Thu2_tiet8_Lop1_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet8_Lop2_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet8_Lop2_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet8_Lop2_1_Id %>)"><%=lbl_Thu2_tiet8_Lop2_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet8_Lop2_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet8_Lop2_2_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet8_Lop2_2_Id %>)"><%=lbl_Thu2_tiet8_Lop2_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet8_Lop2_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet8_Lop2_3_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet8_Lop2_3_Id %>)"><%=lbl_Thu2_tiet8_Lop2_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet8_Lop2_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet8_Lop2_4_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet8_Lop2_4_Id %>)"><%=lbl_Thu2_tiet8_Lop2_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet8_Lop3_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet8_Lop3_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet8_Lop3_1_Id %>)"><%=lbl_Thu2_tiet8_Lop3_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet8_Lop3_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet8_Lop3_2_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet8_Lop3_2_Id %>)"><%=lbl_Thu2_tiet8_Lop3_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet8_Lop3_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet8_Lop3_3_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet8_Lop3_3_Id %>)"><%=lbl_Thu2_tiet8_Lop3_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet8_Lop3_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet8_Lop3_4_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet8_Lop3_4_Id %>)"><%=lbl_Thu2_tiet8_Lop3_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet8_Lop3_5_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet8_Lop3_5_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet8_Lop3_5_Id %>)"><%=lbl_Thu2_tiet8_Lop3_5_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet8_Lop4_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet8_Lop4_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet8_Lop4_1_Id %>)"><%=lbl_Thu2_tiet8_Lop4_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet8_Lop4_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet8_Lop4_2_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet8_Lop4_2_Id %>)"><%=lbl_Thu2_tiet8_Lop4_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet8_Lop5_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet8_Lop5_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet8_Lop5_1_Id %>)"><%=lbl_Thu2_tiet8_Lop5_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet8_Lop6_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet8_Lop6_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet8_Lop6_1_Id %>)"><%=lbl_Thu2_tiet8_Lop6_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet8_Lop7_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet8_Lop7_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet8_Lop7_1_Id %>)"><%=lbl_Thu2_tiet8_Lop7_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet8_Lop7_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet8_Lop7_2_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet8_Lop7_2_Id %>)"><%=lbl_Thu2_tiet8_Lop7_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet8_Lop8_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet8_Lop8_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet8_Lop8_1_Id %>)"><%=lbl_Thu2_tiet8_Lop8_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet8_Lop10_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet8_Lop10_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet8_Lop10_1_Id %>)"><%=lbl_Thu2_tiet8_Lop10_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu2_tiet8_Lop11_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu2_tiet8_Lop11_1_Id %>" onclick="myDetail(<%=lbl_Thu2_tiet8_Lop11_1_Id %>)"><%=lbl_Thu2_tiet8_Lop11_1_Mon %></a></td>
                                </tr>

                                <%--thứ 3--%>
                                <tr>
                                    <td rowspan="8">
                                        <span class="table_day">Thứ ba<br />
                                            <span runat="server" id="Span1"></span>
                                        </span>
                                    </td>
                                    <td rowspan="4">Sáng</td>
                                    <td class="tiet" style="text-align: center">1</td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet1_Lop1_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet1_Lop1_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet1_Lop1_1_Id %>)"><%=lbl_Thu3_tiet1_Lop1_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet1_Lop1_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet1_Lop1_2_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet1_Lop1_2_Id %>)"><%=lbl_Thu3_tiet1_Lop1_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet1_Lop1_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet1_Lop1_3_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet1_Lop1_3_Id %>)"><%=lbl_Thu3_tiet1_Lop1_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet1_Lop1_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet1_Lop1_4_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet1_Lop1_4_Id %>)"><%=lbl_Thu3_tiet1_Lop1_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet1_Lop2_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet1_Lop2_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet1_Lop2_1_Id %>)"><%=lbl_Thu3_tiet1_Lop2_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet1_Lop2_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet1_Lop2_2_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet1_Lop2_2_Id %>)"><%=lbl_Thu3_tiet1_Lop2_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet1_Lop2_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet1_Lop2_3_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet1_Lop2_3_Id %>)"><%=lbl_Thu3_tiet1_Lop2_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet1_Lop2_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet1_Lop2_4_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet1_Lop2_4_Id %>)"><%=lbl_Thu3_tiet1_Lop2_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet1_Lop3_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet1_Lop3_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet1_Lop3_1_Id %>)"><%=lbl_Thu3_tiet1_Lop3_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet1_Lop3_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet1_Lop3_2_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet1_Lop3_2_Id %>)"><%=lbl_Thu3_tiet1_Lop3_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet1_Lop3_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet1_Lop3_3_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet1_Lop3_3_Id %>)"><%=lbl_Thu3_tiet1_Lop3_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet1_Lop3_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet1_Lop3_4_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet1_Lop3_4_Id %>)"><%=lbl_Thu3_tiet1_Lop3_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet1_Lop3_5_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet1_Lop3_5_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet1_Lop3_5_Id %>)"><%=lbl_Thu3_tiet1_Lop3_5_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet1_Lop4_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet1_Lop4_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet1_Lop4_1_Id %>)"><%=lbl_Thu3_tiet1_Lop4_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet1_Lop4_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet1_Lop4_2_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet1_Lop4_2_Id %>)"><%=lbl_Thu3_tiet1_Lop4_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet1_Lop5_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet1_Lop5_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet1_Lop5_1_Id %>)"><%=lbl_Thu3_tiet1_Lop5_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet1_Lop6_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet1_Lop6_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet1_Lop6_1_Id %>)"><%=lbl_Thu3_tiet1_Lop6_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet1_Lop7_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet1_Lop7_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet1_Lop7_1_Id %>)"><%=lbl_Thu3_tiet1_Lop7_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet1_Lop7_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet1_Lop7_2_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet1_Lop7_2_Id %>)"><%=lbl_Thu3_tiet1_Lop7_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet1_Lop8_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet1_Lop8_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet1_Lop8_1_Id %>)"><%=lbl_Thu3_tiet1_Lop8_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet1_Lop10_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet1_Lop10_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet1_Lop10_1_Id %>)"><%=lbl_Thu3_tiet1_Lop10_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet1_Lop11_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet1_Lop11_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet1_Lop11_1_Id %>)"><%=lbl_Thu3_tiet1_Lop11_1_Mon %></a></td>
                                </tr>
                                <tr>
                                    <td class="tiet" style="text-align: center">2</td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet2_Lop1_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet2_Lop1_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet2_Lop1_1_Id %>)"><%=lbl_Thu3_tiet2_Lop1_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet2_Lop1_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet2_Lop1_2_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet2_Lop1_2_Id %>)"><%=lbl_Thu3_tiet2_Lop1_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet2_Lop1_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet2_Lop1_3_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet2_Lop1_3_Id %>)"><%=lbl_Thu3_tiet2_Lop1_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet2_Lop1_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet2_Lop1_4_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet2_Lop1_4_Id %>)"><%=lbl_Thu3_tiet2_Lop1_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet2_Lop2_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet2_Lop2_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet2_Lop2_1_Id %>)"><%=lbl_Thu3_tiet2_Lop2_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet2_Lop2_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet2_Lop2_2_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet2_Lop2_2_Id %>)"><%=lbl_Thu3_tiet2_Lop2_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet2_Lop2_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet2_Lop2_3_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet2_Lop2_3_Id %>)"><%=lbl_Thu3_tiet2_Lop2_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet2_Lop2_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet2_Lop2_4_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet2_Lop2_4_Id %>)"><%=lbl_Thu3_tiet2_Lop2_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet2_Lop3_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet2_Lop3_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet2_Lop3_1_Id %>)"><%=lbl_Thu3_tiet2_Lop3_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet2_Lop3_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet2_Lop3_2_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet2_Lop3_2_Id %>)"><%=lbl_Thu3_tiet2_Lop3_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet2_Lop3_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet2_Lop3_3_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet2_Lop3_3_Id %>)"><%=lbl_Thu3_tiet2_Lop3_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet2_Lop3_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet2_Lop3_4_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet2_Lop3_4_Id %>)"><%=lbl_Thu3_tiet2_Lop3_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet2_Lop3_5_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet2_Lop3_5_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet2_Lop3_5_Id %>)"><%=lbl_Thu3_tiet2_Lop3_5_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet2_Lop4_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet2_Lop4_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet2_Lop4_1_Id %>)"><%=lbl_Thu3_tiet2_Lop4_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet2_Lop4_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet2_Lop4_2_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet2_Lop4_2_Id %>)"><%=lbl_Thu3_tiet2_Lop4_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet2_Lop5_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet2_Lop5_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet2_Lop5_1_Id %>)"><%=lbl_Thu3_tiet2_Lop5_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet2_Lop6_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet2_Lop6_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet2_Lop6_1_Id %>)"><%=lbl_Thu3_tiet2_Lop6_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet2_Lop7_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet2_Lop7_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet2_Lop7_1_Id %>)"><%=lbl_Thu3_tiet2_Lop7_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet2_Lop7_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet2_Lop7_2_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet2_Lop7_2_Id %>)"><%=lbl_Thu3_tiet2_Lop7_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet2_Lop8_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet2_Lop8_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet2_Lop8_1_Id %>)"><%=lbl_Thu3_tiet2_Lop8_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet2_Lop10_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet2_Lop10_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet2_Lop10_1_Id %>)"><%=lbl_Thu3_tiet2_Lop10_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet2_Lop11_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet2_Lop11_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet2_Lop11_1_Id %>)"><%=lbl_Thu3_tiet2_Lop11_1_Mon %></a></td>
                                </tr>
                                <tr>
                                    <td class="tiet" style="text-align: center">3</td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet3_Lop1_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet3_Lop1_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet3_Lop1_1_Id %>)"><%=lbl_Thu3_tiet3_Lop1_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet3_Lop1_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet3_Lop1_2_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet3_Lop1_2_Id %>)"><%=lbl_Thu3_tiet3_Lop1_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet3_Lop1_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet3_Lop1_3_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet3_Lop1_3_Id %>)"><%=lbl_Thu3_tiet3_Lop1_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet3_Lop1_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet3_Lop1_4_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet3_Lop1_4_Id %>)"><%=lbl_Thu3_tiet3_Lop1_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet3_Lop2_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet3_Lop2_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet3_Lop2_1_Id %>)"><%=lbl_Thu3_tiet3_Lop2_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet3_Lop2_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet3_Lop2_2_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet3_Lop2_2_Id %>)"><%=lbl_Thu3_tiet3_Lop2_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet3_Lop2_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet3_Lop2_3_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet3_Lop2_3_Id %>)"><%=lbl_Thu3_tiet3_Lop2_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet3_Lop2_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet3_Lop2_4_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet3_Lop2_4_Id %>)"><%=lbl_Thu3_tiet3_Lop2_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet3_Lop3_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet3_Lop3_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet3_Lop3_1_Id %>)"><%=lbl_Thu3_tiet3_Lop3_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet3_Lop3_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet3_Lop3_2_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet3_Lop3_2_Id %>)"><%=lbl_Thu3_tiet3_Lop3_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet3_Lop3_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet3_Lop3_3_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet3_Lop3_3_Id %>)"><%=lbl_Thu3_tiet3_Lop3_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet3_Lop3_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet3_Lop3_4_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet3_Lop3_4_Id %>)"><%=lbl_Thu3_tiet3_Lop3_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet3_Lop3_5_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet3_Lop3_5_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet3_Lop3_5_Id %>)"><%=lbl_Thu3_tiet3_Lop3_5_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet3_Lop4_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet3_Lop4_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet3_Lop4_1_Id %>)"><%=lbl_Thu3_tiet3_Lop4_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet3_Lop4_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet3_Lop4_2_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet3_Lop4_2_Id %>)"><%=lbl_Thu3_tiet3_Lop4_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet3_Lop5_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet3_Lop5_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet3_Lop5_1_Id %>)"><%=lbl_Thu3_tiet3_Lop5_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet3_Lop6_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet3_Lop6_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet3_Lop6_1_Id %>)"><%=lbl_Thu3_tiet3_Lop6_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet3_Lop7_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet3_Lop7_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet3_Lop7_1_Id %>)"><%=lbl_Thu3_tiet3_Lop7_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet3_Lop7_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet3_Lop7_2_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet3_Lop7_2_Id %>)"><%=lbl_Thu3_tiet3_Lop7_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet3_Lop8_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet3_Lop8_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet3_Lop8_1_Id %>)"><%=lbl_Thu3_tiet3_Lop8_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet3_Lop10_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet3_Lop10_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet3_Lop10_1_Id %>)"><%=lbl_Thu3_tiet3_Lop10_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet3_Lop11_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet3_Lop11_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet3_Lop11_1_Id %>)"><%=lbl_Thu3_tiet3_Lop11_1_Mon %></a></td>
                                </tr>
                                <tr>
                                    <td class="tiet" style="text-align: center">4</td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet4_Lop1_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet4_Lop1_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet4_Lop1_1_Id %>)"><%=lbl_Thu3_tiet4_Lop1_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet4_Lop1_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet4_Lop1_2_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet4_Lop1_2_Id %>)"><%=lbl_Thu3_tiet4_Lop1_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet4_Lop1_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet4_Lop1_3_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet4_Lop1_3_Id %>)"><%=lbl_Thu3_tiet4_Lop1_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet4_Lop1_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet4_Lop1_4_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet4_Lop1_4_Id %>)"><%=lbl_Thu3_tiet4_Lop1_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet4_Lop2_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet4_Lop2_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet4_Lop2_1_Id %>)"><%=lbl_Thu3_tiet4_Lop2_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet4_Lop2_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet4_Lop2_2_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet4_Lop2_2_Id %>)"><%=lbl_Thu3_tiet4_Lop2_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet4_Lop2_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet4_Lop2_3_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet4_Lop2_3_Id %>)"><%=lbl_Thu3_tiet4_Lop2_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet4_Lop2_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet4_Lop2_4_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet4_Lop2_4_Id %>)"><%=lbl_Thu3_tiet4_Lop2_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet4_Lop3_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet4_Lop3_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet4_Lop3_1_Id %>)"><%=lbl_Thu3_tiet4_Lop3_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet4_Lop3_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet4_Lop3_2_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet4_Lop3_2_Id %>)"><%=lbl_Thu3_tiet4_Lop3_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet4_Lop3_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet4_Lop3_3_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet4_Lop3_3_Id %>)"><%=lbl_Thu3_tiet4_Lop3_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet4_Lop3_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet4_Lop3_4_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet4_Lop3_4_Id %>)"><%=lbl_Thu3_tiet4_Lop3_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet4_Lop3_5_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet4_Lop3_5_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet4_Lop3_5_Id %>)"><%=lbl_Thu3_tiet4_Lop3_5_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet4_Lop4_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet4_Lop4_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet4_Lop4_1_Id %>)"><%=lbl_Thu3_tiet4_Lop4_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet4_Lop4_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet4_Lop4_2_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet4_Lop4_2_Id %>)"><%=lbl_Thu3_tiet4_Lop4_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet4_Lop5_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet4_Lop5_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet4_Lop5_1_Id %>)"><%=lbl_Thu3_tiet4_Lop5_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet4_Lop6_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet4_Lop6_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet4_Lop6_1_Id %>)"><%=lbl_Thu3_tiet4_Lop6_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet4_Lop7_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet4_Lop7_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet4_Lop7_1_Id %>)"><%=lbl_Thu3_tiet4_Lop7_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet4_Lop7_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet4_Lop7_2_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet4_Lop7_2_Id %>)"><%=lbl_Thu3_tiet4_Lop7_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet4_Lop8_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet4_Lop8_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet4_Lop8_1_Id %>)"><%=lbl_Thu3_tiet4_Lop8_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet4_Lop10_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet4_Lop10_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet4_Lop10_1_Id %>)"><%=lbl_Thu3_tiet4_Lop10_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet4_Lop11_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet4_Lop11_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet4_Lop11_1_Id %>)"><%=lbl_Thu3_tiet4_Lop11_1_Mon %></a></td>
                                </tr>
                                <tr>
                                    <td rowspan="4">Chiều</td>
                                    <td class="tiet" style="text-align: center">5</td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet5_Lop1_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet5_Lop1_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet5_Lop1_1_Id %>)"><%=lbl_Thu3_tiet5_Lop1_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet5_Lop1_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet5_Lop1_2_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet5_Lop1_2_Id %>)"><%=lbl_Thu3_tiet5_Lop1_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet5_Lop1_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet5_Lop1_3_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet5_Lop1_3_Id %>)"><%=lbl_Thu3_tiet5_Lop1_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet5_Lop1_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet5_Lop1_4_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet5_Lop1_4_Id %>)"><%=lbl_Thu3_tiet5_Lop1_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet5_Lop2_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet5_Lop2_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet5_Lop2_1_Id %>)"><%=lbl_Thu3_tiet5_Lop2_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet5_Lop2_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet5_Lop2_2_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet5_Lop2_2_Id %>)"><%=lbl_Thu3_tiet5_Lop2_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet5_Lop2_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet5_Lop2_3_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet5_Lop2_3_Id %>)"><%=lbl_Thu3_tiet5_Lop2_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet5_Lop2_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet5_Lop2_4_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet5_Lop2_4_Id %>)"><%=lbl_Thu3_tiet5_Lop2_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet5_Lop3_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet5_Lop3_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet5_Lop3_1_Id %>)"><%=lbl_Thu3_tiet5_Lop3_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet5_Lop3_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet5_Lop3_2_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet5_Lop3_2_Id %>)"><%=lbl_Thu3_tiet5_Lop3_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet5_Lop3_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet5_Lop3_3_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet5_Lop3_3_Id %>)"><%=lbl_Thu3_tiet5_Lop3_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet5_Lop3_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet5_Lop3_4_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet5_Lop3_4_Id %>)"><%=lbl_Thu3_tiet5_Lop3_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet5_Lop3_5_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet5_Lop3_5_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet5_Lop3_5_Id %>)"><%=lbl_Thu3_tiet5_Lop3_5_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet5_Lop4_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet5_Lop4_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet5_Lop4_1_Id %>)"><%=lbl_Thu3_tiet5_Lop4_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet5_Lop4_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet5_Lop4_2_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet5_Lop4_2_Id %>)"><%=lbl_Thu3_tiet5_Lop4_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet5_Lop5_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet5_Lop5_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet5_Lop5_1_Id %>)"><%=lbl_Thu3_tiet5_Lop5_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet5_Lop6_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet5_Lop6_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet5_Lop6_1_Id %>)"><%=lbl_Thu3_tiet5_Lop6_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet5_Lop7_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet5_Lop7_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet5_Lop7_1_Id %>)"><%=lbl_Thu3_tiet5_Lop7_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet5_Lop7_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet5_Lop7_2_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet5_Lop7_2_Id %>)"><%=lbl_Thu3_tiet5_Lop7_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet5_Lop8_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet5_Lop8_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet5_Lop8_1_Id %>)"><%=lbl_Thu3_tiet5_Lop8_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet5_Lop10_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet5_Lop10_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet5_Lop10_1_Id %>)"><%=lbl_Thu3_tiet5_Lop10_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet5_Lop11_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet5_Lop11_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet5_Lop11_1_Id %>)"><%=lbl_Thu3_tiet5_Lop11_1_Mon %></a></td>
                                </tr>
                                <tr>
                                    <td class="tiet" style="text-align: center">6</td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet6_Lop1_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet6_Lop1_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet6_Lop1_1_Id %>)"><%=lbl_Thu3_tiet6_Lop1_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet6_Lop1_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet6_Lop1_2_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet6_Lop1_2_Id %>)"><%=lbl_Thu3_tiet6_Lop1_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet6_Lop1_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet6_Lop1_3_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet6_Lop1_3_Id %>)"><%=lbl_Thu3_tiet6_Lop1_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet6_Lop1_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet6_Lop1_4_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet6_Lop1_4_Id %>)"><%=lbl_Thu3_tiet6_Lop1_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet6_Lop2_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet6_Lop2_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet6_Lop2_1_Id %>)"><%=lbl_Thu3_tiet6_Lop2_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet6_Lop2_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet6_Lop2_2_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet6_Lop2_2_Id %>)"><%=lbl_Thu3_tiet6_Lop2_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet6_Lop2_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet6_Lop2_3_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet6_Lop2_3_Id %>)"><%=lbl_Thu3_tiet6_Lop2_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet6_Lop2_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet6_Lop2_4_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet6_Lop2_4_Id %>)"><%=lbl_Thu3_tiet6_Lop2_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet6_Lop3_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet6_Lop3_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet6_Lop3_1_Id %>)"><%=lbl_Thu3_tiet6_Lop3_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet6_Lop3_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet6_Lop3_2_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet6_Lop3_2_Id %>)"><%=lbl_Thu3_tiet6_Lop3_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet6_Lop3_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet6_Lop3_3_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet6_Lop3_3_Id %>)"><%=lbl_Thu3_tiet6_Lop3_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet6_Lop3_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet6_Lop3_4_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet6_Lop3_4_Id %>)"><%=lbl_Thu3_tiet6_Lop3_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet6_Lop3_5_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet6_Lop3_5_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet6_Lop3_5_Id %>)"><%=lbl_Thu3_tiet6_Lop3_5_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet6_Lop4_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet6_Lop4_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet6_Lop4_1_Id %>)"><%=lbl_Thu3_tiet6_Lop4_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet6_Lop4_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet6_Lop4_2_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet6_Lop4_2_Id %>)"><%=lbl_Thu3_tiet6_Lop4_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet6_Lop5_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet6_Lop5_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet6_Lop5_1_Id %>)"><%=lbl_Thu3_tiet6_Lop5_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet6_Lop6_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet6_Lop6_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet6_Lop6_1_Id %>)"><%=lbl_Thu3_tiet6_Lop6_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet6_Lop7_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet6_Lop7_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet6_Lop7_1_Id %>)"><%=lbl_Thu3_tiet6_Lop7_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet6_Lop7_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet6_Lop7_2_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet6_Lop7_2_Id %>)"><%=lbl_Thu3_tiet6_Lop7_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet6_Lop8_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet6_Lop8_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet6_Lop8_1_Id %>)"><%=lbl_Thu3_tiet6_Lop8_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet6_Lop10_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet6_Lop10_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet6_Lop10_1_Id %>)"><%=lbl_Thu3_tiet6_Lop10_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet6_Lop11_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet6_Lop11_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet6_Lop11_1_Id %>)"><%=lbl_Thu3_tiet6_Lop11_1_Mon %></a></td>
                                </tr>
                                <tr>
                                    <td class="tiet" style="text-align: center">7</td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet7_Lop1_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet7_Lop1_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet7_Lop1_1_Id %>)"><%=lbl_Thu3_tiet7_Lop1_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet7_Lop1_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet7_Lop1_2_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet7_Lop1_2_Id %>)"><%=lbl_Thu3_tiet7_Lop1_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet7_Lop1_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet7_Lop1_3_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet7_Lop1_3_Id %>)"><%=lbl_Thu3_tiet7_Lop1_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet7_Lop1_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet7_Lop1_4_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet7_Lop1_4_Id %>)"><%=lbl_Thu3_tiet7_Lop1_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet7_Lop2_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet7_Lop2_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet7_Lop2_1_Id %>)"><%=lbl_Thu3_tiet7_Lop2_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet7_Lop2_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet7_Lop2_2_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet7_Lop2_2_Id %>)"><%=lbl_Thu3_tiet7_Lop2_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet7_Lop2_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet7_Lop2_3_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet7_Lop2_3_Id %>)"><%=lbl_Thu3_tiet7_Lop2_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet7_Lop2_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet7_Lop2_4_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet7_Lop2_4_Id %>)"><%=lbl_Thu3_tiet7_Lop2_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet7_Lop3_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet7_Lop3_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet7_Lop3_1_Id %>)"><%=lbl_Thu3_tiet7_Lop3_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet7_Lop3_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet7_Lop3_2_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet7_Lop3_2_Id %>)"><%=lbl_Thu3_tiet7_Lop3_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet7_Lop3_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet7_Lop3_3_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet7_Lop3_3_Id %>)"><%=lbl_Thu3_tiet7_Lop3_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet7_Lop3_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet7_Lop3_4_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet7_Lop3_4_Id %>)"><%=lbl_Thu3_tiet7_Lop3_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet7_Lop3_5_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet7_Lop3_5_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet7_Lop3_5_Id %>)"><%=lbl_Thu3_tiet7_Lop3_5_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet7_Lop4_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet7_Lop4_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet7_Lop4_1_Id %>)"><%=lbl_Thu3_tiet7_Lop4_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet7_Lop4_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet7_Lop4_2_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet7_Lop4_2_Id %>)"><%=lbl_Thu3_tiet7_Lop4_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet7_Lop5_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet7_Lop5_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet7_Lop5_1_Id %>)"><%=lbl_Thu3_tiet7_Lop5_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet7_Lop6_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet7_Lop6_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet7_Lop6_1_Id %>)"><%=lbl_Thu3_tiet7_Lop6_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet7_Lop7_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet7_Lop7_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet7_Lop7_1_Id %>)"><%=lbl_Thu3_tiet7_Lop7_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet7_Lop7_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet7_Lop7_2_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet7_Lop7_2_Id %>)"><%=lbl_Thu3_tiet7_Lop7_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet7_Lop8_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet7_Lop8_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet7_Lop8_1_Id %>)"><%=lbl_Thu3_tiet7_Lop8_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet7_Lop10_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet7_Lop10_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet7_Lop10_1_Id %>)"><%=lbl_Thu3_tiet7_Lop10_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet7_Lop11_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet7_Lop11_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet7_Lop11_1_Id %>)"><%=lbl_Thu3_tiet7_Lop11_1_Mon %></a></td>
                                </tr>
                                <tr>
                                    <td class="tiet" style="text-align: center">8</td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet8_Lop1_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet8_Lop1_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet8_Lop1_1_Id %>)"><%=lbl_Thu3_tiet8_Lop1_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet8_Lop1_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet8_Lop1_2_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet8_Lop1_2_Id %>)"><%=lbl_Thu3_tiet8_Lop1_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet8_Lop1_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet8_Lop1_3_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet8_Lop1_3_Id %>)"><%=lbl_Thu3_tiet8_Lop1_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet8_Lop1_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet8_Lop1_4_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet8_Lop1_4_Id %>)"><%=lbl_Thu3_tiet8_Lop1_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet8_Lop2_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet8_Lop2_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet8_Lop2_1_Id %>)"><%=lbl_Thu3_tiet8_Lop2_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet8_Lop2_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet8_Lop2_2_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet8_Lop2_2_Id %>)"><%=lbl_Thu3_tiet8_Lop2_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet8_Lop2_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet8_Lop2_3_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet8_Lop2_3_Id %>)"><%=lbl_Thu3_tiet8_Lop2_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet8_Lop2_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet8_Lop2_4_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet8_Lop2_4_Id %>)"><%=lbl_Thu3_tiet8_Lop2_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet8_Lop3_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet8_Lop3_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet8_Lop3_1_Id %>)"><%=lbl_Thu3_tiet8_Lop3_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet8_Lop3_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet8_Lop3_2_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet8_Lop3_2_Id %>)"><%=lbl_Thu3_tiet8_Lop3_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet8_Lop3_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet8_Lop3_3_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet8_Lop3_3_Id %>)"><%=lbl_Thu3_tiet8_Lop3_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet8_Lop3_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet8_Lop3_4_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet8_Lop3_4_Id %>)"><%=lbl_Thu3_tiet8_Lop3_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet8_Lop3_5_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet8_Lop3_5_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet8_Lop3_5_Id %>)"><%=lbl_Thu3_tiet8_Lop3_5_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet8_Lop4_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet8_Lop4_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet8_Lop4_1_Id %>)"><%=lbl_Thu3_tiet8_Lop4_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet8_Lop4_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet8_Lop4_2_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet8_Lop4_2_Id %>)"><%=lbl_Thu3_tiet8_Lop4_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet8_Lop5_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet8_Lop5_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet8_Lop5_1_Id %>)"><%=lbl_Thu3_tiet8_Lop5_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet8_Lop6_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet8_Lop6_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet8_Lop6_1_Id %>)"><%=lbl_Thu3_tiet8_Lop6_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet8_Lop7_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet8_Lop7_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet8_Lop7_1_Id %>)"><%=lbl_Thu3_tiet8_Lop7_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet8_Lop7_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet8_Lop7_2_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet8_Lop7_2_Id %>)"><%=lbl_Thu3_tiet8_Lop7_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet8_Lop8_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet8_Lop8_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet8_Lop8_1_Id %>)"><%=lbl_Thu3_tiet8_Lop8_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet8_Lop10_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet8_Lop10_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet8_Lop10_1_Id %>)"><%=lbl_Thu3_tiet8_Lop10_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu3_tiet8_Lop11_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu3_tiet8_Lop11_1_Id %>" onclick="myDetail(<%=lbl_Thu3_tiet8_Lop11_1_Id %>)"><%=lbl_Thu3_tiet8_Lop11_1_Mon %></a></td>
                                </tr>

                                <%--thứ 4--%>
                                <tr>
                                    <td rowspan="8">
                                        <span class="table_day">Thứ tư<br />
                                            <span runat="server" id="Span2"></span>
                                        </span>
                                    </td>
                                    <td rowspan="4">Sáng</td>
                                    <td class="tiet" style="text-align: center">1</td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet1_Lop1_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet1_Lop1_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet1_Lop1_1_Id %>)"><%=lbl_Thu4_tiet1_Lop1_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet1_Lop1_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet1_Lop1_2_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet1_Lop1_2_Id %>)"><%=lbl_Thu4_tiet1_Lop1_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet1_Lop1_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet1_Lop1_3_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet1_Lop1_3_Id %>)"><%=lbl_Thu4_tiet1_Lop1_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet1_Lop1_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet1_Lop1_4_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet1_Lop1_4_Id %>)"><%=lbl_Thu4_tiet1_Lop1_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet1_Lop2_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet1_Lop2_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet1_Lop2_1_Id %>)"><%=lbl_Thu4_tiet1_Lop2_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet1_Lop2_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet1_Lop2_2_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet1_Lop2_2_Id %>)"><%=lbl_Thu4_tiet1_Lop2_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet1_Lop2_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet1_Lop2_3_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet1_Lop2_3_Id %>)"><%=lbl_Thu4_tiet1_Lop2_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet1_Lop2_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet1_Lop2_4_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet1_Lop2_4_Id %>)"><%=lbl_Thu4_tiet1_Lop2_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet1_Lop3_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet1_Lop3_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet1_Lop3_1_Id %>)"><%=lbl_Thu4_tiet1_Lop3_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet1_Lop3_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet1_Lop3_2_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet1_Lop3_2_Id %>)"><%=lbl_Thu4_tiet1_Lop3_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet1_Lop3_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet1_Lop3_3_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet1_Lop3_3_Id %>)"><%=lbl_Thu4_tiet1_Lop3_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet1_Lop3_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet1_Lop3_4_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet1_Lop3_4_Id %>)"><%=lbl_Thu4_tiet1_Lop3_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet1_Lop3_5_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet1_Lop3_5_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet1_Lop3_5_Id %>)"><%=lbl_Thu4_tiet1_Lop3_5_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet1_Lop4_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet1_Lop4_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet1_Lop4_1_Id %>)"><%=lbl_Thu4_tiet1_Lop4_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet1_Lop4_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet1_Lop4_2_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet1_Lop4_2_Id %>)"><%=lbl_Thu4_tiet1_Lop4_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet1_Lop5_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet1_Lop5_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet1_Lop5_1_Id %>)"><%=lbl_Thu4_tiet1_Lop5_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet1_Lop6_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet1_Lop6_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet1_Lop6_1_Id %>)"><%=lbl_Thu4_tiet1_Lop6_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet1_Lop7_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet1_Lop7_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet1_Lop7_1_Id %>)"><%=lbl_Thu4_tiet1_Lop7_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet1_Lop7_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet1_Lop7_2_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet1_Lop7_2_Id %>)"><%=lbl_Thu4_tiet1_Lop7_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet1_Lop8_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet1_Lop8_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet1_Lop8_1_Id %>)"><%=lbl_Thu4_tiet1_Lop8_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet1_Lop10_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet1_Lop10_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet1_Lop10_1_Id %>)"><%=lbl_Thu4_tiet1_Lop10_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet1_Lop11_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet1_Lop11_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet1_Lop11_1_Id %>)"><%=lbl_Thu4_tiet1_Lop11_1_Mon %></a></td>
                                </tr>
                                <tr>
                                    <td class="tiet" style="text-align: center">2</td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet2_Lop1_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet2_Lop1_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet2_Lop1_1_Id %>)"><%=lbl_Thu4_tiet2_Lop1_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet2_Lop1_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet2_Lop1_2_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet2_Lop1_2_Id %>)"><%=lbl_Thu4_tiet2_Lop1_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet2_Lop1_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet2_Lop1_3_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet2_Lop1_3_Id %>)"><%=lbl_Thu4_tiet2_Lop1_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet2_Lop1_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet2_Lop1_4_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet2_Lop1_4_Id %>)"><%=lbl_Thu4_tiet2_Lop1_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet2_Lop2_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet2_Lop2_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet2_Lop2_1_Id %>)"><%=lbl_Thu4_tiet2_Lop2_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet2_Lop2_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet2_Lop2_2_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet2_Lop2_2_Id %>)"><%=lbl_Thu4_tiet2_Lop2_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet2_Lop2_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet2_Lop2_3_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet2_Lop2_3_Id %>)"><%=lbl_Thu4_tiet2_Lop2_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet2_Lop2_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet2_Lop2_4_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet2_Lop2_4_Id %>)"><%=lbl_Thu4_tiet2_Lop2_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet2_Lop3_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet2_Lop3_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet2_Lop3_1_Id %>)"><%=lbl_Thu4_tiet2_Lop3_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet2_Lop3_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet2_Lop3_2_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet2_Lop3_2_Id %>)"><%=lbl_Thu4_tiet2_Lop3_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet2_Lop3_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet2_Lop3_3_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet2_Lop3_3_Id %>)"><%=lbl_Thu4_tiet2_Lop3_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet2_Lop3_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet2_Lop3_4_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet2_Lop3_4_Id %>)"><%=lbl_Thu4_tiet2_Lop3_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet2_Lop3_5_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet2_Lop3_5_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet2_Lop3_5_Id %>)"><%=lbl_Thu4_tiet2_Lop3_5_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet2_Lop4_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet2_Lop4_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet2_Lop4_1_Id %>)"><%=lbl_Thu4_tiet2_Lop4_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet2_Lop4_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet2_Lop4_2_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet2_Lop4_2_Id %>)"><%=lbl_Thu4_tiet2_Lop4_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet2_Lop5_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet2_Lop5_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet2_Lop5_1_Id %>)"><%=lbl_Thu4_tiet2_Lop5_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet2_Lop6_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet2_Lop6_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet2_Lop6_1_Id %>)"><%=lbl_Thu4_tiet2_Lop6_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet2_Lop7_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet2_Lop7_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet2_Lop7_1_Id %>)"><%=lbl_Thu4_tiet2_Lop7_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet2_Lop7_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet2_Lop7_2_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet2_Lop7_2_Id %>)"><%=lbl_Thu4_tiet2_Lop7_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet2_Lop8_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet2_Lop8_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet2_Lop8_1_Id %>)"><%=lbl_Thu4_tiet2_Lop8_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet2_Lop10_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet2_Lop10_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet2_Lop10_1_Id %>)"><%=lbl_Thu4_tiet2_Lop10_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet2_Lop11_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet2_Lop11_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet2_Lop11_1_Id %>)"><%=lbl_Thu4_tiet2_Lop11_1_Mon %></a></td>
                                </tr>
                                <tr>
                                    <td class="tiet" style="text-align: center">3</td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet3_Lop1_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet3_Lop1_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet3_Lop1_1_Id %>)"><%=lbl_Thu4_tiet3_Lop1_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet3_Lop1_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet3_Lop1_2_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet3_Lop1_2_Id %>)"><%=lbl_Thu4_tiet3_Lop1_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet3_Lop1_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet3_Lop1_3_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet3_Lop1_3_Id %>)"><%=lbl_Thu4_tiet3_Lop1_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet3_Lop1_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet3_Lop1_4_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet3_Lop1_4_Id %>)"><%=lbl_Thu4_tiet3_Lop1_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet3_Lop2_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet3_Lop2_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet3_Lop2_1_Id %>)"><%=lbl_Thu4_tiet3_Lop2_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet3_Lop2_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet3_Lop2_2_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet3_Lop2_2_Id %>)"><%=lbl_Thu4_tiet3_Lop2_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet3_Lop2_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet3_Lop2_3_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet3_Lop2_3_Id %>)"><%=lbl_Thu4_tiet3_Lop2_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet3_Lop2_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet3_Lop2_4_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet3_Lop2_4_Id %>)"><%=lbl_Thu4_tiet3_Lop2_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet3_Lop3_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet3_Lop3_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet3_Lop3_1_Id %>)"><%=lbl_Thu4_tiet3_Lop3_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet3_Lop3_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet3_Lop3_2_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet3_Lop3_2_Id %>)"><%=lbl_Thu4_tiet3_Lop3_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet3_Lop3_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet3_Lop3_3_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet3_Lop3_3_Id %>)"><%=lbl_Thu4_tiet3_Lop3_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet3_Lop3_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet3_Lop3_4_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet3_Lop3_4_Id %>)"><%=lbl_Thu4_tiet3_Lop3_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet3_Lop3_5_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet3_Lop3_5_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet3_Lop3_5_Id %>)"><%=lbl_Thu4_tiet3_Lop3_5_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet3_Lop4_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet3_Lop4_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet3_Lop4_1_Id %>)"><%=lbl_Thu4_tiet3_Lop4_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet3_Lop4_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet3_Lop4_2_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet3_Lop4_2_Id %>)"><%=lbl_Thu4_tiet3_Lop4_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet3_Lop5_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet3_Lop5_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet3_Lop5_1_Id %>)"><%=lbl_Thu4_tiet3_Lop5_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet3_Lop6_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet3_Lop6_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet3_Lop6_1_Id %>)"><%=lbl_Thu4_tiet3_Lop6_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet3_Lop7_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet3_Lop7_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet3_Lop7_1_Id %>)"><%=lbl_Thu4_tiet3_Lop7_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet3_Lop7_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet3_Lop7_2_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet3_Lop7_2_Id %>)"><%=lbl_Thu4_tiet3_Lop7_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet3_Lop8_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet3_Lop8_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet3_Lop8_1_Id %>)"><%=lbl_Thu4_tiet3_Lop8_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet3_Lop10_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet3_Lop10_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet3_Lop10_1_Id %>)"><%=lbl_Thu4_tiet3_Lop10_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet3_Lop11_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet3_Lop11_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet3_Lop11_1_Id %>)"><%=lbl_Thu4_tiet3_Lop11_1_Mon %></a></td>
                                </tr>
                                <tr>
                                    <td class="tiet" style="text-align: center">4</td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet4_Lop1_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet4_Lop1_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet4_Lop1_1_Id %>)"><%=lbl_Thu4_tiet4_Lop1_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet4_Lop1_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet4_Lop1_2_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet4_Lop1_2_Id %>)"><%=lbl_Thu4_tiet4_Lop1_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet4_Lop1_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet4_Lop1_3_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet4_Lop1_3_Id %>)"><%=lbl_Thu4_tiet4_Lop1_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet4_Lop1_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet4_Lop1_4_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet4_Lop1_4_Id %>)"><%=lbl_Thu4_tiet4_Lop1_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet4_Lop2_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet4_Lop2_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet4_Lop2_1_Id %>)"><%=lbl_Thu4_tiet4_Lop2_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet4_Lop2_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet4_Lop2_2_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet4_Lop2_2_Id %>)"><%=lbl_Thu4_tiet4_Lop2_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet4_Lop2_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet4_Lop2_3_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet4_Lop2_3_Id %>)"><%=lbl_Thu4_tiet4_Lop2_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet4_Lop2_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet4_Lop2_4_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet4_Lop2_4_Id %>)"><%=lbl_Thu4_tiet4_Lop2_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet4_Lop3_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet4_Lop3_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet4_Lop3_1_Id %>)"><%=lbl_Thu4_tiet4_Lop3_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet4_Lop3_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet4_Lop3_2_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet4_Lop3_2_Id %>)"><%=lbl_Thu4_tiet4_Lop3_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet4_Lop3_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet4_Lop3_3_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet4_Lop3_3_Id %>)"><%=lbl_Thu4_tiet4_Lop3_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet4_Lop3_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet4_Lop3_4_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet4_Lop3_4_Id %>)"><%=lbl_Thu4_tiet4_Lop3_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet4_Lop3_5_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet4_Lop3_5_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet4_Lop3_5_Id %>)"><%=lbl_Thu4_tiet4_Lop3_5_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet4_Lop4_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet4_Lop4_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet4_Lop4_1_Id %>)"><%=lbl_Thu4_tiet4_Lop4_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet4_Lop4_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet4_Lop4_2_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet4_Lop4_2_Id %>)"><%=lbl_Thu4_tiet4_Lop4_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet4_Lop5_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet4_Lop5_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet4_Lop5_1_Id %>)"><%=lbl_Thu4_tiet4_Lop5_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet4_Lop6_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet4_Lop6_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet4_Lop6_1_Id %>)"><%=lbl_Thu4_tiet4_Lop6_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet4_Lop7_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet4_Lop7_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet4_Lop7_1_Id %>)"><%=lbl_Thu4_tiet4_Lop7_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet4_Lop7_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet4_Lop7_2_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet4_Lop7_2_Id %>)"><%=lbl_Thu4_tiet4_Lop7_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet4_Lop8_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet4_Lop8_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet4_Lop8_1_Id %>)"><%=lbl_Thu4_tiet4_Lop8_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet4_Lop10_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet4_Lop10_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet4_Lop10_1_Id %>)"><%=lbl_Thu4_tiet4_Lop10_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet4_Lop11_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet4_Lop11_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet4_Lop11_1_Id %>)"><%=lbl_Thu4_tiet4_Lop11_1_Mon %></a></td>
                                </tr>
                                <tr>
                                    <td rowspan="4">Chiều</td>
                                    <td class="tiet" style="text-align: center">5</td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet5_Lop1_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet5_Lop1_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet5_Lop1_1_Id %>)"><%=lbl_Thu4_tiet5_Lop1_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet5_Lop1_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet5_Lop1_2_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet5_Lop1_2_Id %>)"><%=lbl_Thu4_tiet5_Lop1_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet5_Lop1_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet5_Lop1_3_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet5_Lop1_3_Id %>)"><%=lbl_Thu4_tiet5_Lop1_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet5_Lop1_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet5_Lop1_4_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet5_Lop1_4_Id %>)"><%=lbl_Thu4_tiet5_Lop1_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet5_Lop2_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet5_Lop2_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet5_Lop2_1_Id %>)"><%=lbl_Thu4_tiet5_Lop2_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet5_Lop2_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet5_Lop2_2_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet5_Lop2_2_Id %>)"><%=lbl_Thu4_tiet5_Lop2_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet5_Lop2_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet5_Lop2_3_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet5_Lop2_3_Id %>)"><%=lbl_Thu4_tiet5_Lop2_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet5_Lop2_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet5_Lop2_4_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet5_Lop2_4_Id %>)"><%=lbl_Thu4_tiet5_Lop2_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet5_Lop3_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet5_Lop3_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet5_Lop3_1_Id %>)"><%=lbl_Thu4_tiet5_Lop3_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet5_Lop3_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet5_Lop3_2_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet5_Lop3_2_Id %>)"><%=lbl_Thu4_tiet5_Lop3_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet5_Lop3_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet5_Lop3_3_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet5_Lop3_3_Id %>)"><%=lbl_Thu4_tiet5_Lop3_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet5_Lop3_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet5_Lop3_4_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet5_Lop3_4_Id %>)"><%=lbl_Thu4_tiet5_Lop3_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet5_Lop3_5_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet5_Lop3_5_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet5_Lop3_5_Id %>)"><%=lbl_Thu4_tiet5_Lop3_5_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet5_Lop4_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet5_Lop4_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet5_Lop4_1_Id %>)"><%=lbl_Thu4_tiet5_Lop4_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet5_Lop4_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet5_Lop4_2_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet5_Lop4_2_Id %>)"><%=lbl_Thu4_tiet5_Lop4_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet5_Lop5_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet5_Lop5_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet5_Lop5_1_Id %>)"><%=lbl_Thu4_tiet5_Lop5_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet5_Lop6_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet5_Lop6_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet5_Lop6_1_Id %>)"><%=lbl_Thu4_tiet5_Lop6_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet5_Lop7_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet5_Lop7_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet5_Lop7_1_Id %>)"><%=lbl_Thu4_tiet5_Lop7_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet5_Lop7_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet5_Lop7_2_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet5_Lop7_2_Id %>)"><%=lbl_Thu4_tiet5_Lop7_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet5_Lop8_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet5_Lop8_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet5_Lop8_1_Id %>)"><%=lbl_Thu4_tiet5_Lop8_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet5_Lop10_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet5_Lop10_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet5_Lop10_1_Id %>)"><%=lbl_Thu4_tiet5_Lop10_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet5_Lop11_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet5_Lop11_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet5_Lop11_1_Id %>)"><%=lbl_Thu4_tiet5_Lop11_1_Mon %></a></td>
                                </tr>
                                <tr>
                                    <td class="tiet" style="text-align: center">6</td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet6_Lop1_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet6_Lop1_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet6_Lop1_1_Id %>)"><%=lbl_Thu4_tiet6_Lop1_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet6_Lop1_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet6_Lop1_2_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet6_Lop1_2_Id %>)"><%=lbl_Thu4_tiet6_Lop1_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet6_Lop1_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet6_Lop1_3_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet6_Lop1_3_Id %>)"><%=lbl_Thu4_tiet6_Lop1_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet6_Lop1_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet6_Lop1_4_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet6_Lop1_4_Id %>)"><%=lbl_Thu4_tiet6_Lop1_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet6_Lop2_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet6_Lop2_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet6_Lop2_1_Id %>)"><%=lbl_Thu4_tiet6_Lop2_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet6_Lop2_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet6_Lop2_2_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet6_Lop2_2_Id %>)"><%=lbl_Thu4_tiet6_Lop2_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet6_Lop2_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet6_Lop2_3_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet6_Lop2_3_Id %>)"><%=lbl_Thu4_tiet6_Lop2_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet6_Lop2_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet6_Lop2_4_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet6_Lop2_4_Id %>)"><%=lbl_Thu4_tiet6_Lop2_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet6_Lop3_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet6_Lop3_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet6_Lop3_1_Id %>)"><%=lbl_Thu4_tiet6_Lop3_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet6_Lop3_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet6_Lop3_2_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet6_Lop3_2_Id %>)"><%=lbl_Thu4_tiet6_Lop3_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet6_Lop3_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet6_Lop3_3_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet6_Lop3_3_Id %>)"><%=lbl_Thu4_tiet6_Lop3_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet6_Lop3_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet6_Lop3_4_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet6_Lop3_4_Id %>)"><%=lbl_Thu4_tiet6_Lop3_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet6_Lop3_5_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet6_Lop3_5_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet6_Lop3_5_Id %>)"><%=lbl_Thu4_tiet6_Lop3_5_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet6_Lop4_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet6_Lop4_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet6_Lop4_1_Id %>)"><%=lbl_Thu4_tiet6_Lop4_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet6_Lop4_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet6_Lop4_2_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet6_Lop4_2_Id %>)"><%=lbl_Thu4_tiet6_Lop4_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet6_Lop5_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet6_Lop5_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet6_Lop5_1_Id %>)"><%=lbl_Thu4_tiet6_Lop5_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet6_Lop6_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet6_Lop6_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet6_Lop6_1_Id %>)"><%=lbl_Thu4_tiet6_Lop6_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet6_Lop7_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet6_Lop7_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet6_Lop7_1_Id %>)"><%=lbl_Thu4_tiet6_Lop7_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet6_Lop7_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet6_Lop7_2_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet6_Lop7_2_Id %>)"><%=lbl_Thu4_tiet6_Lop7_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet6_Lop8_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet6_Lop8_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet6_Lop8_1_Id %>)"><%=lbl_Thu4_tiet6_Lop8_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet6_Lop10_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet6_Lop10_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet6_Lop10_1_Id %>)"><%=lbl_Thu4_tiet6_Lop10_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet6_Lop11_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet6_Lop11_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet6_Lop11_1_Id %>)"><%=lbl_Thu4_tiet6_Lop11_1_Mon %></a></td>
                                </tr>
                                <tr>
                                    <td class="tiet" style="text-align: center">7</td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet7_Lop1_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet7_Lop1_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet7_Lop1_1_Id %>)"><%=lbl_Thu4_tiet7_Lop1_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet7_Lop1_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet7_Lop1_2_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet7_Lop1_2_Id %>)"><%=lbl_Thu4_tiet7_Lop1_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet7_Lop1_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet7_Lop1_3_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet7_Lop1_3_Id %>)"><%=lbl_Thu4_tiet7_Lop1_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet7_Lop1_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet7_Lop1_4_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet7_Lop1_4_Id %>)"><%=lbl_Thu4_tiet7_Lop1_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet7_Lop2_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet7_Lop2_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet7_Lop2_1_Id %>)"><%=lbl_Thu4_tiet7_Lop2_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet7_Lop2_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet7_Lop2_2_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet7_Lop2_2_Id %>)"><%=lbl_Thu4_tiet7_Lop2_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet7_Lop2_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet7_Lop2_3_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet7_Lop2_3_Id %>)"><%=lbl_Thu4_tiet7_Lop2_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet7_Lop2_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet7_Lop2_4_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet7_Lop2_4_Id %>)"><%=lbl_Thu4_tiet7_Lop2_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet7_Lop3_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet7_Lop3_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet7_Lop3_1_Id %>)"><%=lbl_Thu4_tiet7_Lop3_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet7_Lop3_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet7_Lop3_2_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet7_Lop3_2_Id %>)"><%=lbl_Thu4_tiet7_Lop3_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet7_Lop3_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet7_Lop3_3_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet7_Lop3_3_Id %>)"><%=lbl_Thu4_tiet7_Lop3_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet7_Lop3_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet7_Lop3_4_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet7_Lop3_4_Id %>)"><%=lbl_Thu4_tiet7_Lop3_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet7_Lop3_5_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet7_Lop3_5_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet7_Lop3_5_Id %>)"><%=lbl_Thu4_tiet7_Lop3_5_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet7_Lop4_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet7_Lop4_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet7_Lop4_1_Id %>)"><%=lbl_Thu4_tiet7_Lop4_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet7_Lop4_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet7_Lop4_2_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet7_Lop4_2_Id %>)"><%=lbl_Thu4_tiet7_Lop4_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet7_Lop5_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet7_Lop5_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet7_Lop5_1_Id %>)"><%=lbl_Thu4_tiet7_Lop5_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet7_Lop6_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet7_Lop6_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet7_Lop6_1_Id %>)"><%=lbl_Thu4_tiet7_Lop6_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet7_Lop7_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet7_Lop7_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet7_Lop7_1_Id %>)"><%=lbl_Thu4_tiet7_Lop7_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet7_Lop7_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet7_Lop7_2_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet7_Lop7_2_Id %>)"><%=lbl_Thu4_tiet7_Lop7_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet7_Lop8_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet7_Lop8_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet7_Lop8_1_Id %>)"><%=lbl_Thu4_tiet7_Lop8_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet7_Lop10_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet7_Lop10_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet7_Lop10_1_Id %>)"><%=lbl_Thu4_tiet7_Lop10_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet7_Lop11_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet7_Lop11_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet7_Lop11_1_Id %>)"><%=lbl_Thu4_tiet7_Lop11_1_Mon %></a></td>
                                </tr>
                                <tr>
                                    <td class="tiet" style="text-align: center">8</td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet8_Lop1_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet8_Lop1_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet8_Lop1_1_Id %>)"><%=lbl_Thu4_tiet8_Lop1_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet8_Lop1_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet8_Lop1_2_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet8_Lop1_2_Id %>)"><%=lbl_Thu4_tiet8_Lop1_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet8_Lop1_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet8_Lop1_3_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet8_Lop1_3_Id %>)"><%=lbl_Thu4_tiet8_Lop1_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet8_Lop1_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet8_Lop1_4_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet8_Lop1_4_Id %>)"><%=lbl_Thu4_tiet8_Lop1_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet8_Lop2_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet8_Lop2_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet8_Lop2_1_Id %>)"><%=lbl_Thu4_tiet8_Lop2_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet8_Lop2_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet8_Lop2_2_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet8_Lop2_2_Id %>)"><%=lbl_Thu4_tiet8_Lop2_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet8_Lop2_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet8_Lop2_3_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet8_Lop2_3_Id %>)"><%=lbl_Thu4_tiet8_Lop2_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet8_Lop2_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet8_Lop2_4_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet8_Lop2_4_Id %>)"><%=lbl_Thu4_tiet8_Lop2_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet8_Lop3_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet8_Lop3_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet8_Lop3_1_Id %>)"><%=lbl_Thu4_tiet8_Lop3_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet8_Lop3_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet8_Lop3_2_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet8_Lop3_2_Id %>)"><%=lbl_Thu4_tiet8_Lop3_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet8_Lop3_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet8_Lop3_3_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet8_Lop3_3_Id %>)"><%=lbl_Thu4_tiet8_Lop3_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet8_Lop3_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet8_Lop3_4_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet8_Lop3_4_Id %>)"><%=lbl_Thu4_tiet8_Lop3_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet8_Lop3_5_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet8_Lop3_5_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet8_Lop3_5_Id %>)"><%=lbl_Thu4_tiet8_Lop3_5_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet8_Lop4_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet8_Lop4_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet8_Lop4_1_Id %>)"><%=lbl_Thu4_tiet8_Lop4_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet8_Lop4_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet8_Lop4_2_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet8_Lop4_2_Id %>)"><%=lbl_Thu4_tiet8_Lop4_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet8_Lop5_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet8_Lop5_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet8_Lop5_1_Id %>)"><%=lbl_Thu4_tiet8_Lop5_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet8_Lop6_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet8_Lop6_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet8_Lop6_1_Id %>)"><%=lbl_Thu4_tiet8_Lop6_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet8_Lop7_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet8_Lop7_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet8_Lop7_1_Id %>)"><%=lbl_Thu4_tiet8_Lop7_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet8_Lop7_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet8_Lop7_2_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet8_Lop7_2_Id %>)"><%=lbl_Thu4_tiet8_Lop7_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet8_Lop8_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet8_Lop8_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet8_Lop8_1_Id %>)"><%=lbl_Thu4_tiet8_Lop8_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet8_Lop10_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet8_Lop10_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet8_Lop10_1_Id %>)"><%=lbl_Thu4_tiet8_Lop10_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu4_tiet8_Lop11_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu4_tiet8_Lop11_1_Id %>" onclick="myDetail(<%=lbl_Thu4_tiet8_Lop11_1_Id %>)"><%=lbl_Thu4_tiet8_Lop11_1_Mon %></a></td>
                                </tr>

                                <%--thứ 5--%>
                                <tr>
                                    <td rowspan="8">
                                        <span class="table_day">Thứ năm<br />
                                            <span runat="server" id="Span3"></span>
                                        </span>
                                    </td>
                                    <td rowspan="4">Sáng</td>
                                    <td class="tiet" style="text-align: center">1</td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet1_Lop1_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet1_Lop1_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet1_Lop1_1_Id %>)"><%=lbl_Thu5_tiet1_Lop1_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet1_Lop1_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet1_Lop1_2_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet1_Lop1_2_Id %>)"><%=lbl_Thu5_tiet1_Lop1_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet1_Lop1_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet1_Lop1_3_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet1_Lop1_3_Id %>)"><%=lbl_Thu5_tiet1_Lop1_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet1_Lop1_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet1_Lop1_4_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet1_Lop1_4_Id %>)"><%=lbl_Thu5_tiet1_Lop1_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet1_Lop2_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet1_Lop2_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet1_Lop2_1_Id %>)"><%=lbl_Thu5_tiet1_Lop2_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet1_Lop2_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet1_Lop2_2_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet1_Lop2_2_Id %>)"><%=lbl_Thu5_tiet1_Lop2_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet1_Lop2_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet1_Lop2_3_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet1_Lop2_3_Id %>)"><%=lbl_Thu5_tiet1_Lop2_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet1_Lop2_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet1_Lop2_4_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet1_Lop2_4_Id %>)"><%=lbl_Thu5_tiet1_Lop2_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet1_Lop3_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet1_Lop3_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet1_Lop3_1_Id %>)"><%=lbl_Thu5_tiet1_Lop3_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet1_Lop3_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet1_Lop3_2_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet1_Lop3_2_Id %>)"><%=lbl_Thu5_tiet1_Lop3_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet1_Lop3_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet1_Lop3_3_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet1_Lop3_3_Id %>)"><%=lbl_Thu5_tiet1_Lop3_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet1_Lop3_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet1_Lop3_4_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet1_Lop3_4_Id %>)"><%=lbl_Thu5_tiet1_Lop3_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet1_Lop3_5_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet1_Lop3_5_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet1_Lop3_5_Id %>)"><%=lbl_Thu5_tiet1_Lop3_5_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet1_Lop4_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet1_Lop4_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet1_Lop4_1_Id %>)"><%=lbl_Thu5_tiet1_Lop4_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet1_Lop4_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet1_Lop4_2_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet1_Lop4_2_Id %>)"><%=lbl_Thu5_tiet1_Lop4_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet1_Lop5_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet1_Lop5_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet1_Lop5_1_Id %>)"><%=lbl_Thu5_tiet1_Lop5_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet1_Lop6_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet1_Lop6_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet1_Lop6_1_Id %>)"><%=lbl_Thu5_tiet1_Lop6_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet1_Lop7_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet1_Lop7_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet1_Lop7_1_Id %>)"><%=lbl_Thu5_tiet1_Lop7_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet1_Lop7_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet1_Lop7_2_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet1_Lop7_2_Id %>)"><%=lbl_Thu5_tiet1_Lop7_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet1_Lop8_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet1_Lop8_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet1_Lop8_1_Id %>)"><%=lbl_Thu5_tiet1_Lop8_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet1_Lop10_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet1_Lop10_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet1_Lop10_1_Id %>)"><%=lbl_Thu5_tiet1_Lop10_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet1_Lop11_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet1_Lop11_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet1_Lop11_1_Id %>)"><%=lbl_Thu5_tiet1_Lop11_1_Mon %></a></td>
                                </tr>
                                <tr>
                                    <td class="tiet" style="text-align: center">2</td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet2_Lop1_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet2_Lop1_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet2_Lop1_1_Id %>)"><%=lbl_Thu5_tiet2_Lop1_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet2_Lop1_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet2_Lop1_2_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet2_Lop1_2_Id %>)"><%=lbl_Thu5_tiet2_Lop1_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet2_Lop1_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet2_Lop1_3_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet2_Lop1_3_Id %>)"><%=lbl_Thu5_tiet2_Lop1_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet2_Lop1_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet2_Lop1_4_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet2_Lop1_4_Id %>)"><%=lbl_Thu5_tiet2_Lop1_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet2_Lop2_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet2_Lop2_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet2_Lop2_1_Id %>)"><%=lbl_Thu5_tiet2_Lop2_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet2_Lop2_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet2_Lop2_2_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet2_Lop2_2_Id %>)"><%=lbl_Thu5_tiet2_Lop2_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet2_Lop2_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet2_Lop2_3_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet2_Lop2_3_Id %>)"><%=lbl_Thu5_tiet2_Lop2_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet2_Lop2_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet2_Lop2_4_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet2_Lop2_4_Id %>)"><%=lbl_Thu5_tiet2_Lop2_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet2_Lop3_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet2_Lop3_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet2_Lop3_1_Id %>)"><%=lbl_Thu5_tiet2_Lop3_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet2_Lop3_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet2_Lop3_2_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet2_Lop3_2_Id %>)"><%=lbl_Thu5_tiet2_Lop3_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet2_Lop3_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet2_Lop3_3_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet2_Lop3_3_Id %>)"><%=lbl_Thu5_tiet2_Lop3_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet2_Lop3_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet2_Lop3_4_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet2_Lop3_4_Id %>)"><%=lbl_Thu5_tiet2_Lop3_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet2_Lop3_5_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet2_Lop3_5_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet2_Lop3_5_Id %>)"><%=lbl_Thu5_tiet2_Lop3_5_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet2_Lop4_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet2_Lop4_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet2_Lop4_1_Id %>)"><%=lbl_Thu5_tiet2_Lop4_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet2_Lop4_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet2_Lop4_2_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet2_Lop4_2_Id %>)"><%=lbl_Thu5_tiet2_Lop4_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet2_Lop5_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet2_Lop5_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet2_Lop5_1_Id %>)"><%=lbl_Thu5_tiet2_Lop5_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet2_Lop6_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet2_Lop6_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet2_Lop6_1_Id %>)"><%=lbl_Thu5_tiet2_Lop6_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet2_Lop7_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet2_Lop7_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet2_Lop7_1_Id %>)"><%=lbl_Thu5_tiet2_Lop7_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet2_Lop7_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet2_Lop7_2_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet2_Lop7_2_Id %>)"><%=lbl_Thu5_tiet2_Lop7_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet2_Lop8_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet2_Lop8_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet2_Lop8_1_Id %>)"><%=lbl_Thu5_tiet2_Lop8_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet2_Lop10_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet2_Lop10_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet2_Lop10_1_Id %>)"><%=lbl_Thu5_tiet2_Lop10_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet2_Lop11_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet2_Lop11_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet2_Lop11_1_Id %>)"><%=lbl_Thu5_tiet2_Lop11_1_Mon %></a></td>
                                </tr>
                                <tr>
                                    <td class="tiet" style="text-align: center">3</td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet3_Lop1_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet3_Lop1_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet3_Lop1_1_Id %>)"><%=lbl_Thu5_tiet3_Lop1_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet3_Lop1_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet3_Lop1_2_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet3_Lop1_2_Id %>)"><%=lbl_Thu5_tiet3_Lop1_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet3_Lop1_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet3_Lop1_3_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet3_Lop1_3_Id %>)"><%=lbl_Thu5_tiet3_Lop1_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet3_Lop1_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet3_Lop1_4_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet3_Lop1_4_Id %>)"><%=lbl_Thu5_tiet3_Lop1_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet3_Lop2_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet3_Lop2_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet3_Lop2_1_Id %>)"><%=lbl_Thu5_tiet3_Lop2_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet3_Lop2_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet3_Lop2_2_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet3_Lop2_2_Id %>)"><%=lbl_Thu5_tiet3_Lop2_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet3_Lop2_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet3_Lop2_3_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet3_Lop2_3_Id %>)"><%=lbl_Thu5_tiet3_Lop2_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet3_Lop2_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet3_Lop2_4_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet3_Lop2_4_Id %>)"><%=lbl_Thu5_tiet3_Lop2_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet3_Lop3_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet3_Lop3_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet3_Lop3_1_Id %>)"><%=lbl_Thu5_tiet3_Lop3_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet3_Lop3_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet3_Lop3_2_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet3_Lop3_2_Id %>)"><%=lbl_Thu5_tiet3_Lop3_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet3_Lop3_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet3_Lop3_3_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet3_Lop3_3_Id %>)"><%=lbl_Thu5_tiet3_Lop3_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet3_Lop3_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet3_Lop3_4_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet3_Lop3_4_Id %>)"><%=lbl_Thu5_tiet3_Lop3_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet3_Lop3_5_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet3_Lop3_5_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet3_Lop3_5_Id %>)"><%=lbl_Thu5_tiet3_Lop3_5_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet3_Lop4_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet3_Lop4_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet3_Lop4_1_Id %>)"><%=lbl_Thu5_tiet3_Lop4_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet3_Lop4_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet3_Lop4_2_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet3_Lop4_2_Id %>)"><%=lbl_Thu5_tiet3_Lop4_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet3_Lop5_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet3_Lop5_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet3_Lop5_1_Id %>)"><%=lbl_Thu5_tiet3_Lop5_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet3_Lop6_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet3_Lop6_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet3_Lop6_1_Id %>)"><%=lbl_Thu5_tiet3_Lop6_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet3_Lop7_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet3_Lop7_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet3_Lop7_1_Id %>)"><%=lbl_Thu5_tiet3_Lop7_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet3_Lop7_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet3_Lop7_2_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet3_Lop7_2_Id %>)"><%=lbl_Thu5_tiet3_Lop7_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet3_Lop8_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet3_Lop8_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet3_Lop8_1_Id %>)"><%=lbl_Thu5_tiet3_Lop8_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet3_Lop10_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet3_Lop10_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet3_Lop10_1_Id %>)"><%=lbl_Thu5_tiet3_Lop10_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet3_Lop11_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet3_Lop11_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet3_Lop11_1_Id %>)"><%=lbl_Thu5_tiet3_Lop11_1_Mon %></a></td>
                                </tr>
                                <tr>
                                    <td class="tiet" style="text-align: center">4</td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet4_Lop1_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet4_Lop1_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet4_Lop1_1_Id %>)"><%=lbl_Thu5_tiet4_Lop1_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet4_Lop1_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet4_Lop1_2_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet4_Lop1_2_Id %>)"><%=lbl_Thu5_tiet4_Lop1_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet4_Lop1_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet4_Lop1_3_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet4_Lop1_3_Id %>)"><%=lbl_Thu5_tiet4_Lop1_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet4_Lop1_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet4_Lop1_4_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet4_Lop1_4_Id %>)"><%=lbl_Thu5_tiet4_Lop1_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet4_Lop2_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet4_Lop2_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet4_Lop2_1_Id %>)"><%=lbl_Thu5_tiet4_Lop2_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet4_Lop2_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet4_Lop2_2_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet4_Lop2_2_Id %>)"><%=lbl_Thu5_tiet4_Lop2_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet4_Lop2_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet4_Lop2_3_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet4_Lop2_3_Id %>)"><%=lbl_Thu5_tiet4_Lop2_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet4_Lop2_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet4_Lop2_4_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet4_Lop2_4_Id %>)"><%=lbl_Thu5_tiet4_Lop2_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet4_Lop3_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet4_Lop3_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet4_Lop3_1_Id %>)"><%=lbl_Thu5_tiet4_Lop3_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet4_Lop3_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet4_Lop3_2_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet4_Lop3_2_Id %>)"><%=lbl_Thu5_tiet4_Lop3_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet4_Lop3_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet4_Lop3_3_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet4_Lop3_3_Id %>)"><%=lbl_Thu5_tiet4_Lop3_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet4_Lop3_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet4_Lop3_4_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet4_Lop3_4_Id %>)"><%=lbl_Thu5_tiet4_Lop3_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet4_Lop3_5_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet4_Lop3_5_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet4_Lop3_5_Id %>)"><%=lbl_Thu5_tiet4_Lop3_5_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet4_Lop4_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet4_Lop4_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet4_Lop4_1_Id %>)"><%=lbl_Thu5_tiet4_Lop4_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet4_Lop4_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet4_Lop4_2_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet4_Lop4_2_Id %>)"><%=lbl_Thu5_tiet4_Lop4_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet4_Lop5_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet4_Lop5_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet4_Lop5_1_Id %>)"><%=lbl_Thu5_tiet4_Lop5_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet4_Lop6_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet4_Lop6_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet4_Lop6_1_Id %>)"><%=lbl_Thu5_tiet4_Lop6_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet4_Lop7_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet4_Lop7_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet4_Lop7_1_Id %>)"><%=lbl_Thu5_tiet4_Lop7_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet4_Lop7_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet4_Lop7_2_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet4_Lop7_2_Id %>)"><%=lbl_Thu5_tiet4_Lop7_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet4_Lop8_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet4_Lop8_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet4_Lop8_1_Id %>)"><%=lbl_Thu5_tiet4_Lop8_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet4_Lop10_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet4_Lop10_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet4_Lop10_1_Id %>)"><%=lbl_Thu5_tiet4_Lop10_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet4_Lop11_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet4_Lop11_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet4_Lop11_1_Id %>)"><%=lbl_Thu5_tiet4_Lop11_1_Mon %></a></td>
                                </tr>
                                <tr>
                                    <td rowspan="4">Chiều</td>
                                    <td class="tiet" style="text-align: center">5</td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet5_Lop1_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet5_Lop1_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet5_Lop1_1_Id %>)"><%=lbl_Thu5_tiet5_Lop1_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet5_Lop1_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet5_Lop1_2_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet5_Lop1_2_Id %>)"><%=lbl_Thu5_tiet5_Lop1_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet5_Lop1_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet5_Lop1_3_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet5_Lop1_3_Id %>)"><%=lbl_Thu5_tiet5_Lop1_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet5_Lop1_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet5_Lop1_4_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet5_Lop1_4_Id %>)"><%=lbl_Thu5_tiet5_Lop1_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet5_Lop2_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet5_Lop2_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet5_Lop2_1_Id %>)"><%=lbl_Thu5_tiet5_Lop2_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet5_Lop2_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet5_Lop2_2_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet5_Lop2_2_Id %>)"><%=lbl_Thu5_tiet5_Lop2_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet5_Lop2_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet5_Lop2_3_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet5_Lop2_3_Id %>)"><%=lbl_Thu5_tiet5_Lop2_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet5_Lop2_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet5_Lop2_4_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet5_Lop2_4_Id %>)"><%=lbl_Thu5_tiet5_Lop2_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet5_Lop3_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet5_Lop3_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet5_Lop3_1_Id %>)"><%=lbl_Thu5_tiet5_Lop3_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet5_Lop3_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet5_Lop3_2_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet5_Lop3_2_Id %>)"><%=lbl_Thu5_tiet5_Lop3_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet5_Lop3_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet5_Lop3_3_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet5_Lop3_3_Id %>)"><%=lbl_Thu5_tiet5_Lop3_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet5_Lop3_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet5_Lop3_4_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet5_Lop3_4_Id %>)"><%=lbl_Thu5_tiet5_Lop3_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet5_Lop3_5_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet5_Lop3_5_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet5_Lop3_5_Id %>)"><%=lbl_Thu5_tiet5_Lop3_5_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet5_Lop4_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet5_Lop4_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet5_Lop4_1_Id %>)"><%=lbl_Thu5_tiet5_Lop4_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet5_Lop4_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet5_Lop4_2_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet5_Lop4_2_Id %>)"><%=lbl_Thu5_tiet5_Lop4_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet5_Lop5_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet5_Lop5_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet5_Lop5_1_Id %>)"><%=lbl_Thu5_tiet5_Lop5_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet5_Lop6_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet5_Lop6_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet5_Lop6_1_Id %>)"><%=lbl_Thu5_tiet5_Lop6_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet5_Lop7_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet5_Lop7_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet5_Lop7_1_Id %>)"><%=lbl_Thu5_tiet5_Lop7_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet5_Lop7_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet5_Lop7_2_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet5_Lop7_2_Id %>)"><%=lbl_Thu5_tiet5_Lop7_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet5_Lop8_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet5_Lop8_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet5_Lop8_1_Id %>)"><%=lbl_Thu5_tiet5_Lop8_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet5_Lop10_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet5_Lop10_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet5_Lop10_1_Id %>)"><%=lbl_Thu5_tiet5_Lop10_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet5_Lop11_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet5_Lop11_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet5_Lop11_1_Id %>)"><%=lbl_Thu5_tiet5_Lop11_1_Mon %></a></td>
                                </tr>
                                <tr>
                                    <td class="tiet" style="text-align: center">6</td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet6_Lop1_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet6_Lop1_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet6_Lop1_1_Id %>)"><%=lbl_Thu5_tiet6_Lop1_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet6_Lop1_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet6_Lop1_2_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet6_Lop1_2_Id %>)"><%=lbl_Thu5_tiet6_Lop1_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet6_Lop1_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet6_Lop1_3_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet6_Lop1_3_Id %>)"><%=lbl_Thu5_tiet6_Lop1_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet6_Lop1_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet6_Lop1_4_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet6_Lop1_4_Id %>)"><%=lbl_Thu5_tiet6_Lop1_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet6_Lop2_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet6_Lop2_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet6_Lop2_1_Id %>)"><%=lbl_Thu5_tiet6_Lop2_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet6_Lop2_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet6_Lop2_2_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet6_Lop2_2_Id %>)"><%=lbl_Thu5_tiet6_Lop2_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet6_Lop2_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet6_Lop2_3_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet6_Lop2_3_Id %>)"><%=lbl_Thu5_tiet6_Lop2_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet6_Lop2_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet6_Lop2_4_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet6_Lop2_4_Id %>)"><%=lbl_Thu5_tiet6_Lop2_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet6_Lop3_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet6_Lop3_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet6_Lop3_1_Id %>)"><%=lbl_Thu5_tiet6_Lop3_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet6_Lop3_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet6_Lop3_2_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet6_Lop3_2_Id %>)"><%=lbl_Thu5_tiet6_Lop3_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet6_Lop3_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet6_Lop3_3_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet6_Lop3_3_Id %>)"><%=lbl_Thu5_tiet6_Lop3_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet6_Lop3_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet6_Lop3_4_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet6_Lop3_4_Id %>)"><%=lbl_Thu5_tiet6_Lop3_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet6_Lop3_5_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet6_Lop3_5_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet6_Lop3_5_Id %>)"><%=lbl_Thu5_tiet6_Lop3_5_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet6_Lop4_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet6_Lop4_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet6_Lop4_1_Id %>)"><%=lbl_Thu5_tiet6_Lop4_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet6_Lop4_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet6_Lop4_2_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet6_Lop4_2_Id %>)"><%=lbl_Thu5_tiet6_Lop4_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet6_Lop5_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet6_Lop5_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet6_Lop5_1_Id %>)"><%=lbl_Thu5_tiet6_Lop5_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet6_Lop6_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet6_Lop6_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet6_Lop6_1_Id %>)"><%=lbl_Thu5_tiet6_Lop6_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet6_Lop7_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet6_Lop7_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet6_Lop7_1_Id %>)"><%=lbl_Thu5_tiet6_Lop7_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet6_Lop7_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet6_Lop7_2_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet6_Lop7_2_Id %>)"><%=lbl_Thu5_tiet6_Lop7_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet6_Lop8_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet6_Lop8_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet6_Lop8_1_Id %>)"><%=lbl_Thu5_tiet6_Lop8_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet6_Lop10_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet6_Lop10_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet6_Lop10_1_Id %>)"><%=lbl_Thu5_tiet6_Lop10_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet6_Lop11_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet6_Lop11_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet6_Lop11_1_Id %>)"><%=lbl_Thu5_tiet6_Lop11_1_Mon %></a></td>
                                </tr>
                                <tr>
                                    <td class="tiet" style="text-align: center">7</td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet7_Lop1_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet7_Lop1_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet7_Lop1_1_Id %>)"><%=lbl_Thu5_tiet7_Lop1_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet7_Lop1_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet7_Lop1_2_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet7_Lop1_2_Id %>)"><%=lbl_Thu5_tiet7_Lop1_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet7_Lop1_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet7_Lop1_3_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet7_Lop1_3_Id %>)"><%=lbl_Thu5_tiet7_Lop1_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet7_Lop1_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet7_Lop1_4_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet7_Lop1_4_Id %>)"><%=lbl_Thu5_tiet7_Lop1_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet7_Lop2_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet7_Lop2_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet7_Lop2_1_Id %>)"><%=lbl_Thu5_tiet7_Lop2_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet7_Lop2_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet7_Lop2_2_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet7_Lop2_2_Id %>)"><%=lbl_Thu5_tiet7_Lop2_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet7_Lop2_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet7_Lop2_3_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet7_Lop2_3_Id %>)"><%=lbl_Thu5_tiet7_Lop2_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet7_Lop2_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet7_Lop2_4_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet7_Lop2_4_Id %>)"><%=lbl_Thu5_tiet7_Lop2_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet7_Lop3_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet7_Lop3_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet7_Lop3_1_Id %>)"><%=lbl_Thu5_tiet7_Lop3_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet7_Lop3_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet7_Lop3_2_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet7_Lop3_2_Id %>)"><%=lbl_Thu5_tiet7_Lop3_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet7_Lop3_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet7_Lop3_3_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet7_Lop3_3_Id %>)"><%=lbl_Thu5_tiet7_Lop3_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet7_Lop3_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet7_Lop3_4_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet7_Lop3_4_Id %>)"><%=lbl_Thu5_tiet7_Lop3_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet7_Lop3_5_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet7_Lop3_5_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet7_Lop3_5_Id %>)"><%=lbl_Thu5_tiet7_Lop3_5_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet7_Lop4_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet7_Lop4_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet7_Lop4_1_Id %>)"><%=lbl_Thu5_tiet7_Lop4_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet7_Lop4_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet7_Lop4_2_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet7_Lop4_2_Id %>)"><%=lbl_Thu5_tiet7_Lop4_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet7_Lop5_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet7_Lop5_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet7_Lop5_1_Id %>)"><%=lbl_Thu5_tiet7_Lop5_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet7_Lop6_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet7_Lop6_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet7_Lop6_1_Id %>)"><%=lbl_Thu5_tiet7_Lop6_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet7_Lop7_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet7_Lop7_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet7_Lop7_1_Id %>)"><%=lbl_Thu5_tiet7_Lop7_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet7_Lop7_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet7_Lop7_2_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet7_Lop7_2_Id %>)"><%=lbl_Thu5_tiet7_Lop7_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet7_Lop8_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet7_Lop8_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet7_Lop8_1_Id %>)"><%=lbl_Thu5_tiet7_Lop8_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet7_Lop10_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet7_Lop10_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet7_Lop10_1_Id %>)"><%=lbl_Thu5_tiet7_Lop10_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet7_Lop11_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet7_Lop11_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet7_Lop11_1_Id %>)"><%=lbl_Thu5_tiet7_Lop11_1_Mon %></a></td>
                                </tr>
                                <tr>
                                    <td class="tiet" style="text-align: center">8</td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet8_Lop1_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet8_Lop1_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet8_Lop1_1_Id %>)"><%=lbl_Thu5_tiet8_Lop1_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet8_Lop1_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet8_Lop1_2_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet8_Lop1_2_Id %>)"><%=lbl_Thu5_tiet8_Lop1_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet8_Lop1_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet8_Lop1_3_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet8_Lop1_3_Id %>)"><%=lbl_Thu5_tiet8_Lop1_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet8_Lop1_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet8_Lop1_4_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet8_Lop1_4_Id %>)"><%=lbl_Thu5_tiet8_Lop1_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet8_Lop2_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet8_Lop2_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet8_Lop2_1_Id %>)"><%=lbl_Thu5_tiet8_Lop2_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet8_Lop2_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet8_Lop2_2_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet8_Lop2_2_Id %>)"><%=lbl_Thu5_tiet8_Lop2_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet8_Lop2_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet8_Lop2_3_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet8_Lop2_3_Id %>)"><%=lbl_Thu5_tiet8_Lop2_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet8_Lop2_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet8_Lop2_4_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet8_Lop2_4_Id %>)"><%=lbl_Thu5_tiet8_Lop2_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet8_Lop3_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet8_Lop3_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet8_Lop3_1_Id %>)"><%=lbl_Thu5_tiet8_Lop3_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet8_Lop3_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet8_Lop3_2_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet8_Lop3_2_Id %>)"><%=lbl_Thu5_tiet8_Lop3_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet8_Lop3_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet8_Lop3_3_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet8_Lop3_3_Id %>)"><%=lbl_Thu5_tiet8_Lop3_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet8_Lop3_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet8_Lop3_4_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet8_Lop3_4_Id %>)"><%=lbl_Thu5_tiet8_Lop3_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet8_Lop3_5_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet8_Lop3_5_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet8_Lop3_5_Id %>)"><%=lbl_Thu5_tiet8_Lop3_5_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet8_Lop4_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet8_Lop4_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet8_Lop4_1_Id %>)"><%=lbl_Thu5_tiet8_Lop4_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet8_Lop4_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet8_Lop4_2_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet8_Lop4_2_Id %>)"><%=lbl_Thu5_tiet8_Lop4_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet8_Lop5_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet8_Lop5_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet8_Lop5_1_Id %>)"><%=lbl_Thu5_tiet8_Lop5_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet8_Lop6_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet8_Lop6_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet8_Lop6_1_Id %>)"><%=lbl_Thu5_tiet8_Lop6_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet8_Lop7_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet8_Lop7_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet8_Lop7_1_Id %>)"><%=lbl_Thu5_tiet8_Lop7_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet8_Lop7_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet8_Lop7_2_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet8_Lop7_2_Id %>)"><%=lbl_Thu5_tiet8_Lop7_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet8_Lop8_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet8_Lop8_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet8_Lop8_1_Id %>)"><%=lbl_Thu5_tiet8_Lop8_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet8_Lop10_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet8_Lop10_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet8_Lop10_1_Id %>)"><%=lbl_Thu5_tiet8_Lop10_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu5_tiet8_Lop11_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu5_tiet8_Lop11_1_Id %>" onclick="myDetail(<%=lbl_Thu5_tiet8_Lop11_1_Id %>)"><%=lbl_Thu5_tiet8_Lop11_1_Mon %></a></td>
                                </tr>

                                <%--thứ 6--%>
                                <tr>
                                    <td rowspan="8">
                                        <span class="table_day">Thứ sáu<br />
                                            <span runat="server" id="Span4"></span>
                                        </span>
                                    </td>
                                    <td rowspan="4">Sáng</td>
                                    <td class="tiet" style="text-align: center">1</td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet1_Lop1_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet1_Lop1_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet1_Lop1_1_Id %>)"><%=lbl_Thu6_tiet1_Lop1_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet1_Lop1_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet1_Lop1_2_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet1_Lop1_2_Id %>)"><%=lbl_Thu6_tiet1_Lop1_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet1_Lop1_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet1_Lop1_3_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet1_Lop1_3_Id %>)"><%=lbl_Thu6_tiet1_Lop1_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet1_Lop1_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet1_Lop1_4_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet1_Lop1_4_Id %>)"><%=lbl_Thu6_tiet1_Lop1_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet1_Lop2_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet1_Lop2_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet1_Lop2_1_Id %>)"><%=lbl_Thu6_tiet1_Lop2_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet1_Lop2_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet1_Lop2_2_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet1_Lop2_2_Id %>)"><%=lbl_Thu6_tiet1_Lop2_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet1_Lop2_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet1_Lop2_3_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet1_Lop2_3_Id %>)"><%=lbl_Thu6_tiet1_Lop2_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet1_Lop2_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet1_Lop2_4_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet1_Lop2_4_Id %>)"><%=lbl_Thu6_tiet1_Lop2_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet1_Lop3_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet1_Lop3_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet1_Lop3_1_Id %>)"><%=lbl_Thu6_tiet1_Lop3_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet1_Lop3_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet1_Lop3_2_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet1_Lop3_2_Id %>)"><%=lbl_Thu6_tiet1_Lop3_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet1_Lop3_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet1_Lop3_3_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet1_Lop3_3_Id %>)"><%=lbl_Thu6_tiet1_Lop3_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet1_Lop3_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet1_Lop3_4_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet1_Lop3_4_Id %>)"><%=lbl_Thu6_tiet1_Lop3_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet1_Lop3_5_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet1_Lop3_5_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet1_Lop3_5_Id %>)"><%=lbl_Thu6_tiet1_Lop3_5_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet1_Lop4_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet1_Lop4_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet1_Lop4_1_Id %>)"><%=lbl_Thu6_tiet1_Lop4_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet1_Lop4_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet1_Lop4_2_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet1_Lop4_2_Id %>)"><%=lbl_Thu6_tiet1_Lop4_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet1_Lop5_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet1_Lop5_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet1_Lop5_1_Id %>)"><%=lbl_Thu6_tiet1_Lop5_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet1_Lop6_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet1_Lop6_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet1_Lop6_1_Id %>)"><%=lbl_Thu6_tiet1_Lop6_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet1_Lop7_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet1_Lop7_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet1_Lop7_1_Id %>)"><%=lbl_Thu6_tiet1_Lop7_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet1_Lop7_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet1_Lop7_2_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet1_Lop7_2_Id %>)"><%=lbl_Thu6_tiet1_Lop7_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet1_Lop8_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet1_Lop8_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet1_Lop8_1_Id %>)"><%=lbl_Thu6_tiet1_Lop8_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet1_Lop10_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet1_Lop10_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet1_Lop10_1_Id %>)"><%=lbl_Thu6_tiet1_Lop10_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet1_Lop11_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet1_Lop11_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet1_Lop11_1_Id %>)"><%=lbl_Thu6_tiet1_Lop11_1_Mon %></a></td>
                                </tr>
                                <tr>
                                    <td class="tiet" style="text-align: center">2</td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet2_Lop1_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet2_Lop1_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet2_Lop1_1_Id %>)"><%=lbl_Thu6_tiet2_Lop1_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet2_Lop1_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet2_Lop1_2_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet2_Lop1_2_Id %>)"><%=lbl_Thu6_tiet2_Lop1_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet2_Lop1_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet2_Lop1_3_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet2_Lop1_3_Id %>)"><%=lbl_Thu6_tiet2_Lop1_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet2_Lop1_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet2_Lop1_4_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet2_Lop1_4_Id %>)"><%=lbl_Thu6_tiet2_Lop1_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet2_Lop2_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet2_Lop2_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet2_Lop2_1_Id %>)"><%=lbl_Thu6_tiet2_Lop2_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet2_Lop2_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet2_Lop2_2_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet2_Lop2_2_Id %>)"><%=lbl_Thu6_tiet2_Lop2_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet2_Lop2_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet2_Lop2_3_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet2_Lop2_3_Id %>)"><%=lbl_Thu6_tiet2_Lop2_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet2_Lop2_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet2_Lop2_4_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet2_Lop2_4_Id %>)"><%=lbl_Thu6_tiet2_Lop2_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet2_Lop3_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet2_Lop3_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet2_Lop3_1_Id %>)"><%=lbl_Thu6_tiet2_Lop3_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet2_Lop3_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet2_Lop3_2_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet2_Lop3_2_Id %>)"><%=lbl_Thu6_tiet2_Lop3_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet2_Lop3_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet2_Lop3_3_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet2_Lop3_3_Id %>)"><%=lbl_Thu6_tiet2_Lop3_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet2_Lop3_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet2_Lop3_4_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet2_Lop3_4_Id %>)"><%=lbl_Thu6_tiet2_Lop3_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet2_Lop3_5_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet2_Lop3_5_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet2_Lop3_5_Id %>)"><%=lbl_Thu6_tiet2_Lop3_5_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet2_Lop4_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet2_Lop4_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet2_Lop4_1_Id %>)"><%=lbl_Thu6_tiet2_Lop4_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet2_Lop4_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet2_Lop4_2_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet2_Lop4_2_Id %>)"><%=lbl_Thu6_tiet2_Lop4_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet2_Lop5_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet2_Lop5_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet2_Lop5_1_Id %>)"><%=lbl_Thu6_tiet2_Lop5_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet2_Lop6_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet2_Lop6_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet2_Lop6_1_Id %>)"><%=lbl_Thu6_tiet2_Lop6_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet2_Lop7_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet2_Lop7_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet2_Lop7_1_Id %>)"><%=lbl_Thu6_tiet2_Lop7_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet2_Lop7_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet2_Lop7_2_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet2_Lop7_2_Id %>)"><%=lbl_Thu6_tiet2_Lop7_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet2_Lop8_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet2_Lop8_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet2_Lop8_1_Id %>)"><%=lbl_Thu6_tiet2_Lop8_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet2_Lop10_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet2_Lop10_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet2_Lop10_1_Id %>)"><%=lbl_Thu6_tiet2_Lop10_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet2_Lop11_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet2_Lop11_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet2_Lop11_1_Id %>)"><%=lbl_Thu6_tiet2_Lop11_1_Mon %></a></td>
                                </tr>
                                <tr>
                                    <td class="tiet" style="text-align: center">3</td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet3_Lop1_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet3_Lop1_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet3_Lop1_1_Id %>)"><%=lbl_Thu6_tiet3_Lop1_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet3_Lop1_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet3_Lop1_2_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet3_Lop1_2_Id %>)"><%=lbl_Thu6_tiet3_Lop1_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet3_Lop1_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet3_Lop1_3_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet3_Lop1_3_Id %>)"><%=lbl_Thu6_tiet3_Lop1_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet3_Lop1_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet3_Lop1_4_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet3_Lop1_4_Id %>)"><%=lbl_Thu6_tiet3_Lop1_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet3_Lop2_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet3_Lop2_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet3_Lop2_1_Id %>)"><%=lbl_Thu6_tiet3_Lop2_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet3_Lop2_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet3_Lop2_2_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet3_Lop2_2_Id %>)"><%=lbl_Thu6_tiet3_Lop2_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet3_Lop2_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet3_Lop2_3_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet3_Lop2_3_Id %>)"><%=lbl_Thu6_tiet3_Lop2_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet3_Lop2_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet3_Lop2_4_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet3_Lop2_4_Id %>)"><%=lbl_Thu6_tiet3_Lop2_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet3_Lop3_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet3_Lop3_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet3_Lop3_1_Id %>)"><%=lbl_Thu6_tiet3_Lop3_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet3_Lop3_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet3_Lop3_2_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet3_Lop3_2_Id %>)"><%=lbl_Thu6_tiet3_Lop3_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet3_Lop3_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet3_Lop3_3_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet3_Lop3_3_Id %>)"><%=lbl_Thu6_tiet3_Lop3_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet3_Lop3_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet3_Lop3_4_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet3_Lop3_4_Id %>)"><%=lbl_Thu6_tiet3_Lop3_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet3_Lop3_5_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet3_Lop3_5_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet3_Lop3_5_Id %>)"><%=lbl_Thu6_tiet3_Lop3_5_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet3_Lop4_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet3_Lop4_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet3_Lop4_1_Id %>)"><%=lbl_Thu6_tiet3_Lop4_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet3_Lop4_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet3_Lop4_2_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet3_Lop4_2_Id %>)"><%=lbl_Thu6_tiet3_Lop4_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet3_Lop5_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet3_Lop5_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet3_Lop5_1_Id %>)"><%=lbl_Thu6_tiet3_Lop5_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet3_Lop6_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet3_Lop6_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet3_Lop6_1_Id %>)"><%=lbl_Thu6_tiet3_Lop6_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet3_Lop7_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet3_Lop7_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet3_Lop7_1_Id %>)"><%=lbl_Thu6_tiet3_Lop7_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet3_Lop7_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet3_Lop7_2_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet3_Lop7_2_Id %>)"><%=lbl_Thu6_tiet3_Lop7_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet3_Lop8_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet3_Lop8_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet3_Lop8_1_Id %>)"><%=lbl_Thu6_tiet3_Lop8_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet3_Lop10_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet3_Lop10_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet3_Lop10_1_Id %>)"><%=lbl_Thu6_tiet3_Lop10_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet3_Lop11_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet3_Lop11_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet3_Lop11_1_Id %>)"><%=lbl_Thu6_tiet3_Lop11_1_Mon %></a></td>
                                </tr>
                                <tr>
                                    <td class="tiet" style="text-align: center">4</td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet4_Lop1_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet4_Lop1_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet4_Lop1_1_Id %>)"><%=lbl_Thu6_tiet4_Lop1_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet4_Lop1_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet4_Lop1_2_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet4_Lop1_2_Id %>)"><%=lbl_Thu6_tiet4_Lop1_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet4_Lop1_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet4_Lop1_3_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet4_Lop1_3_Id %>)"><%=lbl_Thu6_tiet4_Lop1_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet4_Lop1_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet4_Lop1_4_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet4_Lop1_4_Id %>)"><%=lbl_Thu6_tiet4_Lop1_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet4_Lop2_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet4_Lop2_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet4_Lop2_1_Id %>)"><%=lbl_Thu6_tiet4_Lop2_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet4_Lop2_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet4_Lop2_2_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet4_Lop2_2_Id %>)"><%=lbl_Thu6_tiet4_Lop2_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet4_Lop2_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet4_Lop2_3_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet4_Lop2_3_Id %>)"><%=lbl_Thu6_tiet4_Lop2_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet4_Lop2_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet4_Lop2_4_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet4_Lop2_4_Id %>)"><%=lbl_Thu6_tiet4_Lop2_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet4_Lop3_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet4_Lop3_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet4_Lop3_1_Id %>)"><%=lbl_Thu6_tiet4_Lop3_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet4_Lop3_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet4_Lop3_2_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet4_Lop3_2_Id %>)"><%=lbl_Thu6_tiet4_Lop3_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet4_Lop3_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet4_Lop3_3_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet4_Lop3_3_Id %>)"><%=lbl_Thu6_tiet4_Lop3_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet4_Lop3_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet4_Lop3_4_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet4_Lop3_4_Id %>)"><%=lbl_Thu6_tiet4_Lop3_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet4_Lop3_5_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet4_Lop3_5_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet4_Lop3_5_Id %>)"><%=lbl_Thu6_tiet4_Lop3_5_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet4_Lop4_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet4_Lop4_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet4_Lop4_1_Id %>)"><%=lbl_Thu6_tiet4_Lop4_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet4_Lop4_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet4_Lop4_2_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet4_Lop4_2_Id %>)"><%=lbl_Thu6_tiet4_Lop4_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet4_Lop5_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet4_Lop5_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet4_Lop5_1_Id %>)"><%=lbl_Thu6_tiet4_Lop5_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet4_Lop6_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet4_Lop6_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet4_Lop6_1_Id %>)"><%=lbl_Thu6_tiet4_Lop6_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet4_Lop7_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet4_Lop7_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet4_Lop7_1_Id %>)"><%=lbl_Thu6_tiet4_Lop7_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet4_Lop7_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet4_Lop7_2_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet4_Lop7_2_Id %>)"><%=lbl_Thu6_tiet4_Lop7_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet4_Lop8_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet4_Lop8_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet4_Lop8_1_Id %>)"><%=lbl_Thu6_tiet4_Lop8_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet4_Lop10_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet4_Lop10_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet4_Lop10_1_Id %>)"><%=lbl_Thu6_tiet4_Lop10_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet4_Lop11_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet4_Lop11_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet4_Lop11_1_Id %>)"><%=lbl_Thu6_tiet4_Lop11_1_Mon %></a></td>
                                </tr>
                                <tr>
                                    <td rowspan="4">Chiều</td>
                                    <td class="tiet" style="text-align: center">5</td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet5_Lop1_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet5_Lop1_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet5_Lop1_1_Id %>)"><%=lbl_Thu6_tiet5_Lop1_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet5_Lop1_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet5_Lop1_2_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet5_Lop1_2_Id %>)"><%=lbl_Thu6_tiet5_Lop1_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet5_Lop1_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet5_Lop1_3_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet5_Lop1_3_Id %>)"><%=lbl_Thu6_tiet5_Lop1_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet5_Lop1_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet5_Lop1_4_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet5_Lop1_4_Id %>)"><%=lbl_Thu6_tiet5_Lop1_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet5_Lop2_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet5_Lop2_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet5_Lop2_1_Id %>)"><%=lbl_Thu6_tiet5_Lop2_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet5_Lop2_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet5_Lop2_2_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet5_Lop2_2_Id %>)"><%=lbl_Thu6_tiet5_Lop2_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet5_Lop2_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet5_Lop2_3_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet5_Lop2_3_Id %>)"><%=lbl_Thu6_tiet5_Lop2_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet5_Lop2_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet5_Lop2_4_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet5_Lop2_4_Id %>)"><%=lbl_Thu6_tiet5_Lop2_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet5_Lop3_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet5_Lop3_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet5_Lop3_1_Id %>)"><%=lbl_Thu6_tiet5_Lop3_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet5_Lop3_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet5_Lop3_2_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet5_Lop3_2_Id %>)"><%=lbl_Thu6_tiet5_Lop3_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet5_Lop3_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet5_Lop3_3_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet5_Lop3_3_Id %>)"><%=lbl_Thu6_tiet5_Lop3_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet5_Lop3_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet5_Lop3_4_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet5_Lop3_4_Id %>)"><%=lbl_Thu6_tiet5_Lop3_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet5_Lop3_5_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet5_Lop3_5_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet5_Lop3_5_Id %>)"><%=lbl_Thu6_tiet5_Lop3_5_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet5_Lop4_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet5_Lop4_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet5_Lop4_1_Id %>)"><%=lbl_Thu6_tiet5_Lop4_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet5_Lop4_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet5_Lop4_2_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet5_Lop4_2_Id %>)"><%=lbl_Thu6_tiet5_Lop4_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet5_Lop5_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet5_Lop5_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet5_Lop5_1_Id %>)"><%=lbl_Thu6_tiet5_Lop5_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet5_Lop6_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet5_Lop6_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet5_Lop6_1_Id %>)"><%=lbl_Thu6_tiet5_Lop6_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet5_Lop7_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet5_Lop7_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet5_Lop7_1_Id %>)"><%=lbl_Thu6_tiet5_Lop7_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet5_Lop7_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet5_Lop7_2_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet5_Lop7_2_Id %>)"><%=lbl_Thu6_tiet5_Lop7_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet5_Lop8_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet5_Lop8_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet5_Lop8_1_Id %>)"><%=lbl_Thu6_tiet5_Lop8_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet5_Lop10_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet5_Lop10_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet5_Lop10_1_Id %>)"><%=lbl_Thu6_tiet5_Lop10_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet5_Lop11_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet5_Lop11_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet5_Lop11_1_Id %>)"><%=lbl_Thu6_tiet5_Lop11_1_Mon %></a></td>
                                </tr>
                                <tr>
                                    <td class="tiet" style="text-align: center">6</td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet6_Lop1_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet6_Lop1_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet6_Lop1_1_Id %>)"><%=lbl_Thu6_tiet6_Lop1_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet6_Lop1_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet6_Lop1_2_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet6_Lop1_2_Id %>)"><%=lbl_Thu6_tiet6_Lop1_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet6_Lop1_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet6_Lop1_3_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet6_Lop1_3_Id %>)"><%=lbl_Thu6_tiet6_Lop1_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet6_Lop1_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet6_Lop1_4_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet6_Lop1_4_Id %>)"><%=lbl_Thu6_tiet6_Lop1_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet6_Lop2_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet6_Lop2_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet6_Lop2_1_Id %>)"><%=lbl_Thu6_tiet6_Lop2_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet6_Lop2_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet6_Lop2_2_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet6_Lop2_2_Id %>)"><%=lbl_Thu6_tiet6_Lop2_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet6_Lop2_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet6_Lop2_3_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet6_Lop2_3_Id %>)"><%=lbl_Thu6_tiet6_Lop2_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet6_Lop2_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet6_Lop2_4_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet6_Lop2_4_Id %>)"><%=lbl_Thu6_tiet6_Lop2_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet6_Lop3_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet6_Lop3_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet6_Lop3_1_Id %>)"><%=lbl_Thu6_tiet6_Lop3_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet6_Lop3_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet6_Lop3_2_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet6_Lop3_2_Id %>)"><%=lbl_Thu6_tiet6_Lop3_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet6_Lop3_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet6_Lop3_3_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet6_Lop3_3_Id %>)"><%=lbl_Thu6_tiet6_Lop3_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet6_Lop3_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet6_Lop3_4_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet6_Lop3_4_Id %>)"><%=lbl_Thu6_tiet6_Lop3_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet6_Lop3_5_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet6_Lop3_5_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet6_Lop3_5_Id %>)"><%=lbl_Thu6_tiet6_Lop3_5_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet6_Lop4_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet6_Lop4_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet6_Lop4_1_Id %>)"><%=lbl_Thu6_tiet6_Lop4_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet6_Lop4_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet6_Lop4_2_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet6_Lop4_2_Id %>)"><%=lbl_Thu6_tiet6_Lop4_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet6_Lop5_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet6_Lop5_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet6_Lop5_1_Id %>)"><%=lbl_Thu6_tiet6_Lop5_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet6_Lop6_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet6_Lop6_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet6_Lop6_1_Id %>)"><%=lbl_Thu6_tiet6_Lop6_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet6_Lop7_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet6_Lop7_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet6_Lop7_1_Id %>)"><%=lbl_Thu6_tiet6_Lop7_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet6_Lop7_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet6_Lop7_2_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet6_Lop7_2_Id %>)"><%=lbl_Thu6_tiet6_Lop7_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet6_Lop8_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet6_Lop8_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet6_Lop8_1_Id %>)"><%=lbl_Thu6_tiet6_Lop8_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet6_Lop10_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet6_Lop10_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet6_Lop10_1_Id %>)"><%=lbl_Thu6_tiet6_Lop10_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet6_Lop11_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet6_Lop11_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet6_Lop11_1_Id %>)"><%=lbl_Thu6_tiet6_Lop11_1_Mon %></a></td>
                                </tr>
                                <tr>
                                    <td class="tiet" style="text-align: center">7</td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet7_Lop1_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet7_Lop1_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet7_Lop1_1_Id %>)"><%=lbl_Thu6_tiet7_Lop1_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet7_Lop1_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet7_Lop1_2_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet7_Lop1_2_Id %>)"><%=lbl_Thu6_tiet7_Lop1_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet7_Lop1_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet7_Lop1_3_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet7_Lop1_3_Id %>)"><%=lbl_Thu6_tiet7_Lop1_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet7_Lop1_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet7_Lop1_4_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet7_Lop1_4_Id %>)"><%=lbl_Thu6_tiet7_Lop1_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet7_Lop2_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet7_Lop2_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet7_Lop2_1_Id %>)"><%=lbl_Thu6_tiet7_Lop2_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet7_Lop2_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet7_Lop2_2_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet7_Lop2_2_Id %>)"><%=lbl_Thu6_tiet7_Lop2_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet7_Lop2_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet7_Lop2_3_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet7_Lop2_3_Id %>)"><%=lbl_Thu6_tiet7_Lop2_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet7_Lop2_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet7_Lop2_4_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet7_Lop2_4_Id %>)"><%=lbl_Thu6_tiet7_Lop2_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet7_Lop3_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet7_Lop3_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet7_Lop3_1_Id %>)"><%=lbl_Thu6_tiet7_Lop3_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet7_Lop3_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet7_Lop3_2_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet7_Lop3_2_Id %>)"><%=lbl_Thu6_tiet7_Lop3_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet7_Lop3_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet7_Lop3_3_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet7_Lop3_3_Id %>)"><%=lbl_Thu6_tiet7_Lop3_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet7_Lop3_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet7_Lop3_4_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet7_Lop3_4_Id %>)"><%=lbl_Thu6_tiet7_Lop3_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet7_Lop3_5_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet7_Lop3_5_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet7_Lop3_5_Id %>)"><%=lbl_Thu6_tiet7_Lop3_5_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet7_Lop4_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet7_Lop4_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet7_Lop4_1_Id %>)"><%=lbl_Thu6_tiet7_Lop4_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet7_Lop4_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet7_Lop4_2_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet7_Lop4_2_Id %>)"><%=lbl_Thu6_tiet7_Lop4_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet7_Lop5_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet7_Lop5_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet7_Lop5_1_Id %>)"><%=lbl_Thu6_tiet7_Lop5_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet7_Lop6_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet7_Lop6_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet7_Lop6_1_Id %>)"><%=lbl_Thu6_tiet7_Lop6_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet7_Lop7_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet7_Lop7_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet7_Lop7_1_Id %>)"><%=lbl_Thu6_tiet7_Lop7_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet7_Lop7_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet7_Lop7_2_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet7_Lop7_2_Id %>)"><%=lbl_Thu6_tiet7_Lop7_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet7_Lop8_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet7_Lop8_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet7_Lop8_1_Id %>)"><%=lbl_Thu6_tiet7_Lop8_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet7_Lop10_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet7_Lop10_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet7_Lop10_1_Id %>)"><%=lbl_Thu6_tiet7_Lop10_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet7_Lop11_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet7_Lop11_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet7_Lop11_1_Id %>)"><%=lbl_Thu6_tiet7_Lop11_1_Mon %></a></td>
                                </tr>
                                <tr>
                                    <td class="tiet" style="text-align: center">8</td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet8_Lop1_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet8_Lop1_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet8_Lop1_1_Id %>)"><%=lbl_Thu6_tiet8_Lop1_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet8_Lop1_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet8_Lop1_2_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet8_Lop1_2_Id %>)"><%=lbl_Thu6_tiet8_Lop1_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet8_Lop1_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet8_Lop1_3_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet8_Lop1_3_Id %>)"><%=lbl_Thu6_tiet8_Lop1_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet8_Lop1_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet8_Lop1_4_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet8_Lop1_4_Id %>)"><%=lbl_Thu6_tiet8_Lop1_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet8_Lop2_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet8_Lop2_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet8_Lop2_1_Id %>)"><%=lbl_Thu6_tiet8_Lop2_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet8_Lop2_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet8_Lop2_2_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet8_Lop2_2_Id %>)"><%=lbl_Thu6_tiet8_Lop2_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet8_Lop2_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet8_Lop2_3_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet8_Lop2_3_Id %>)"><%=lbl_Thu6_tiet8_Lop2_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet8_Lop2_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet8_Lop2_4_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet8_Lop2_4_Id %>)"><%=lbl_Thu6_tiet8_Lop2_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet8_Lop3_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet8_Lop3_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet8_Lop3_1_Id %>)"><%=lbl_Thu6_tiet8_Lop3_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet8_Lop3_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet8_Lop3_2_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet8_Lop3_2_Id %>)"><%=lbl_Thu6_tiet8_Lop3_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet8_Lop3_3_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet8_Lop3_3_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet8_Lop3_3_Id %>)"><%=lbl_Thu6_tiet8_Lop3_3_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet8_Lop3_4_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet8_Lop3_4_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet8_Lop3_4_Id %>)"><%=lbl_Thu6_tiet8_Lop3_4_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet8_Lop3_5_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet8_Lop3_5_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet8_Lop3_5_Id %>)"><%=lbl_Thu6_tiet8_Lop3_5_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet8_Lop4_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet8_Lop4_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet8_Lop4_1_Id %>)"><%=lbl_Thu6_tiet8_Lop4_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet8_Lop4_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet8_Lop4_2_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet8_Lop4_2_Id %>)"><%=lbl_Thu6_tiet8_Lop4_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet8_Lop5_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet8_Lop5_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet8_Lop5_1_Id %>)"><%=lbl_Thu6_tiet8_Lop5_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet8_Lop6_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet8_Lop6_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet8_Lop6_1_Id %>)"><%=lbl_Thu6_tiet8_Lop6_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet8_Lop7_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet8_Lop7_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet8_Lop7_1_Id %>)"><%=lbl_Thu6_tiet8_Lop7_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet8_Lop7_2_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet8_Lop7_2_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet8_Lop7_2_Id %>)"><%=lbl_Thu6_tiet8_Lop7_2_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet8_Lop8_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet8_Lop8_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet8_Lop8_1_Id %>)"><%=lbl_Thu6_tiet8_Lop8_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet8_Lop10_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet8_Lop10_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet8_Lop10_1_Id %>)"><%=lbl_Thu6_tiet8_Lop10_1_Mon %></a></td>
                                    <td class="tiet" runat="server" id="txt_Thu6_tiet8_Lop11_1_Mon"><a href="javascript:void(0)" id="<%=lbl_Thu6_tiet8_Lop11_1_Id %>" onclick="myDetail(<%=lbl_Thu6_tiet8_Lop11_1_Id %>)"><%=lbl_Thu6_tiet8_Lop11_1_Mon %></a></td>
                                </tr>

                            </tbody>
                        </table>
                        <table id="header-fixed"></table>
                    </div>
                </div>
            </div>
        </div>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <div style="display: none">
                    <input id="txt_ID" type="text" runat="server" />
                    <a href="javascript:void(0)" id="btnXemChiTiet" type="button" runat="server" onserverclick="btnXemChiTiet_ServerClick">Xem chi tiết</a>
                    <a href="javascript:void(0)" id="btnDuGio" type="button" runat="server" onserverclick="btnDuGio_ServerClick">Dự giờ</a>
                    <input id="txtThuHoc" type="text" runat="server" />
                    <input id="txtBuoiHoc" type="text" runat="server" />
                    <input id="txtTietHoc" type="text" runat="server" />
                    <input id="txtLop" type="text" runat="server" />
                    <input id="txtNgayDay" type="text" runat="server" />
                    <input id="txtMonHoc" type="text" runat="server" />
                    <input id="txtNguoiSoan" type="text" runat="server" />
                    <input id="txtTietPPCT" type="text" runat="server" />
                    <input id="txtTenBaiGiang" type="text" runat="server" />
                    <input id="txtGhiChu" type="text" runat="server" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <%--modal--%>
        <div style="display: none" id="box-modal">
            <p class="name">CHI TIẾT LỊCH BÁO GIẢNG</p>
            <table class="table table-hover">
                <tr>
                    <th style="text-align: center">Thứ</th>
                    <th style="text-align: center">Buổi</th>
                    <th style="text-align: center">Tiết</th>
                    <th style="text-align: center">Lớp</th>
                    <th style="text-align: center">Ngày dạy</th>
                    <th style="text-align: center">Môn</th>
                    <th style="text-align: center">Tiết<br />
                        (PPCT)</th>
                    <th style="text-align: center">Tên bài giảng</th>
                    <th style="text-align: center">Ghi chú</th>
                    <th style="text-align: center">Người soạn</th>
                </tr>
                <%--thứ 2--%>
                <tr>
                    <td id="thu"></td>
                    <td id="buoi"></td>
                    <td id="tiet"></td>
                    <td id="lop"></td>
                    <td id="ngayday"></td>
                    <td id="mon"></td>
                    <td id="tietppct"></td>
                    <td id="baigiang"></td>
                    <td id="ghichu"></td>
                    <td id="nguoisoan"></td>
                </tr>
            </table>
            <a id="btnDangKyDuGio" href="javascript:void(0)" onclick="DangKyDuGio()" class="btn btn-primary">Đăng ký dự giờ</a>
            <a id="btnTieptuc" href="javascript:void(0)" class="btn btn-primary">Đóng</a>
        </div>
    </form>
    <script>
        var tableOffset = $("#table-1").offset().top;
        var $header = $("#table-1 > thead").clone();
        var $fixedHeader = $("#header-fixed").append($header);

        $(window).bind("scroll", function () {
            var offset = $(this).scrollTop();

            if (offset >= tableOffset && $fixedHeader.is(":hidden")) {
                $fixedHeader.show();
            }
            else if (offset < tableOffset) {
                $fixedHeader.hide();
            }
        });
    </script>
    <script>
        function DangKyDuGio() {
            document.getElementById("<%= btnDuGio.ClientID%>").click();
        }
        var a, b, c;
        function myDetail(id) {
            document.getElementById("<%= txt_ID.ClientID%>").value = id;
            document.getElementById("<%= btnXemChiTiet.ClientID%>").click();
        }
        function showDetail() {
            document.getElementById("thu").innerHTML = document.getElementById("<%= txtThuHoc.ClientID%>").value;
            document.getElementById("buoi").innerHTML = document.getElementById("<%= txtBuoiHoc.ClientID%>").value;
            document.getElementById("tiet").innerHTML = document.getElementById("<%= txtTietHoc.ClientID%>").value;
            document.getElementById("lop").innerHTML = document.getElementById("<%= txtLop.ClientID%>").value;
            document.getElementById("ngayday").innerHTML = document.getElementById("<%= txtNgayDay.ClientID%>").value;
            document.getElementById("mon").innerHTML = document.getElementById("<%= txtMonHoc.ClientID%>").value;
            document.getElementById("tietppct").innerHTML = document.getElementById("<%= txtTietPPCT.ClientID%>").value;
            document.getElementById("baigiang").innerHTML = document.getElementById("<%= txtTenBaiGiang.ClientID%>").value;
            document.getElementById("ghichu").innerHTML = document.getElementById("<%= txtGhiChu.ClientID%>").value;
            document.getElementById("nguoisoan").innerHTML = document.getElementById("<%= txtNguoiSoan.ClientID%>").value;
            document.getElementById("box-modal").style.display = "block";
            document.getElementById("btnTieptuc").addEventListener("click", function () {
                document.getElementById("box-modal").style.display = "none";
            });
        };
    </script>

</body>
</html>
