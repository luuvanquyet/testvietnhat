﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_LichCongTac_module_XemLichTongNamTest : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    public int solandugio;
    public string tuanhoc;
    //public string[] arrayList;
    // giáo viên trong tiết học
    //public string lbl_Thu2_GiaoVien1_Lop1_1, lbl_Thu2_GiaoVien1_Lop1_2, lbl_Thu2_GiaoVien1_Lop1_3, lbl_Thu2_GiaoVien1_Lop1_4, lbl_Thu2_GiaoVien1_Lop2_1, lbl_Thu2_GiaoVien1_Lop2_2, lbl_Thu2_GiaoVien1_Lop2_3, lbl_Thu2_GiaoVien1_Lop2_4, lbl_Thu2_GiaoVien1_Lop3_1, lbl_Thu2_GiaoVien1_Lop3_2, lbl_Thu2_GiaoVien1_Lop3_3, lbl_Thu2_GiaoVien1_Lop3_4, lbl_Thu2_GiaoVien1_Lop3_5, lbl_Thu2_GiaoVien1_Lop4_1, lbl_Thu2_GiaoVien1_Lop4_2, lbl_Thu2_GiaoVien1_Lop5_1, lbl_Thu2_GiaoVien1_Lop6_1, lbl_Thu2_GiaoVien1_Lop6_2, lbl_Thu2_GiaoVien1_Lop7_1, lbl_Thu2_GiaoVien1_Lop7_2, lbl_Thu2_GiaoVien1_Lop8_1, lbl_Thu2_GiaoVien1_Lop10_1, lbl_Thu2_GiaoVien1_Lop11_1;
    string[] Lop = { "1/1", "1/2", "1/3", "1/4", "2/1", "2/2", "2/3", "2/4", "3/1", "3/2", "3/3", "3/4", "3/5", "4/1", "4/2", "5/1", "6/1", "7/1","7/2", "8/1", "10/1", "11/1" };

    //, "7/1", "7/2", "8/1", "10/1", "11/1" 

    //ArrayList Thu = new ArrayList() { 2, 3, 4 };

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //đổ ra ds tuần học
            var dsTuan = from t in db.tbHocTap_Tuans where t.tuan_hidden == false select t;
            ddlTuanHoc.Items.Clear();
            ddlTuanHoc.Items.Insert(0, "Chọn Tuần học");
            ddlTuanHoc.AppendDataBoundItems = true;
            ddlTuanHoc.DataTextField = "tuan_name";
            ddlTuanHoc.DataValueField = "tuan_id";
            ddlTuanHoc.DataSource = dsTuan;
            ddlTuanHoc.DataBind();
            //ddlTuanHoc.DataSource = dsTuan;
            //ddlTuanHoc.DataBind();
            //loadData();
        }
    }

    private void loadData()
    {

        List<ThuHoc1> thuhoc = new List<ThuHoc1>();
        // lưu thứ học vào list thuhoc
        //for (int thu = 2; thu < 3; thu++)
        //{
        ThuHoc1 thuHoc2 = new ThuHoc1();
        thuHoc2.thu_id = 2;
        thuHoc2.thu_name = "Thứ 2";
        thuhoc.Add(thuHoc2);
        ThuHoc1 thuHoc3 = new ThuHoc1();
        thuHoc3.thu_id = 3;
        thuHoc3.thu_name = "Thứ 3";
        thuhoc.Add(thuHoc3);
        ThuHoc1 thuHoc4 = new ThuHoc1();
        thuHoc4.thu_id = 4;
        thuHoc4.thu_name = "Thứ 4";
        thuhoc.Add(thuHoc4);
        ThuHoc1 thuHoc5 = new ThuHoc1();
        thuHoc5.thu_id = 5;
        thuHoc5.thu_name = "Thứ 5";
        thuhoc.Add(thuHoc5);
        ThuHoc1 thuHoc6 = new ThuHoc1();
        thuHoc6.thu_id = 6;
        thuHoc6.thu_name = "Thứ 6";
        thuhoc.Add(thuHoc6);
        rpThu.DataSource = thuhoc;
        rpThu.DataBind();
    }
    protected void ddlTuanHoc_SelectedIndexChanged(object sender, EventArgs e)
    {
        loadData();
    }
    //protected List<LichBaoGiang> getDataBaoGiang(int thu, int tiet)
    //{
    //    //ArrayList ArrListThu2Tiet1 = new ArrayList();
    //    List<LichBaoGiang> LBG = new List<LichBaoGiang>();
    //    var gettuanhoc = (from t in db.tbHocTap_Tuans
    //                      where t.tuan_hidden == false
    //                      && t.tuan_id == Convert.ToInt32(ddlTuanHoc.SelectedItem.Value)
    //                      select t).SingleOrDefault();
    //    if (ddlKhoi.SelectedValue == "KhoiTH")
    //    {
    //        for (int i = 0; i < Lop1.Length; i++)
    //        {
    //            //string IDBG = "0";
    //            var getData = from s in db.tbLichBaoGiangChiTiets
    //                          join t in db.tbLichBaoGiangTheoTuans on s.lichbaogiangtheotuan_id equals t.lichbaogiangtheotuan_id
    //                          join bg in db.tbLichBaoGiangs on t.lichbaogiang_id equals bg.lichbaogiang_id
    //                          join u in db.admin_Users on s.username_id equals u.username_id
    //                          where t.lichbaogiangtheotuan_thuhoc == "Thứ " + thu
    //                            && s.lichbaogiangchitiet_lop == Lop1[i]
    //                            && s.lichbaogiangchitiet_tiethoc == "Tiết " + tiet
    //                            && bg.tuan_id == gettuanhoc.tuan_id
    //                          orderby bg.lichbaogiang_id descending
    //                          select new
    //                          {
    //                              s.lichbaogiangchitiet_id,
    //                              s.lichbaogiangchitiet_monhoc,
    //                              u.username_id,
    //                              u.username_fullname
    //                          };
    //            if (getData.Count() == 0)
    //            {
    //                LBG.Add(new LichBaoGiang()
    //                {
    //                    lichbaogiangchitiet_id = 0,
    //                    thu_id = 0,
    //                    tiet_id = 0,
    //                    lichbaogiangchitiet_monhoc = "",
    //                    username_username = ""
    //                });
    //            }
    //            else
    //            {
    //                LBG.Add(new LichBaoGiang()
    //                {
    //                    lichbaogiangchitiet_id = getData.FirstOrDefault().lichbaogiangchitiet_id,
    //                    thu_id = thu,
    //                    tiet_id = tiet,
    //                    lichbaogiangchitiet_monhoc = getData.FirstOrDefault().lichbaogiangchitiet_monhoc,
    //                    username_username = getData.FirstOrDefault().username_fullname.Split(' ').Last()
    //                });
    //            }
    //        }
    //        return LBG;
    //    }
    //    else
    //    {
    //        for (int i = 0; i < Lop2.Length; i++)
    //        {
    //            //string IDBG = "0";
    //            var getData = from s in db.tbLichBaoGiangChiTiets
    //                          join t in db.tbLichBaoGiangTheoTuans on s.lichbaogiangtheotuan_id equals t.lichbaogiangtheotuan_id
    //                          join bg in db.tbLichBaoGiangs on t.lichbaogiang_id equals bg.lichbaogiang_id
    //                          join u in db.admin_Users on s.username_id equals u.username_id
    //                          where t.lichbaogiangtheotuan_thuhoc == "Thứ " + thu
    //                            && s.lichbaogiangchitiet_lop == Lop2[i]
    //                            && s.lichbaogiangchitiet_tiethoc == "Tiết " + tiet
    //                            && bg.tuan_id == gettuanhoc.tuan_id
    //                          orderby bg.lichbaogiang_id descending
    //                          select new
    //                          {
    //                              s.lichbaogiangchitiet_id,
    //                              s.lichbaogiangchitiet_monhoc,
    //                              u.username_id,
    //                              u.username_fullname
    //                          };
    //            if (getData.Count() == 0)
    //            {
    //                LBG.Add(new LichBaoGiang()
    //                {
    //                    lichbaogiangchitiet_id = 0,
    //                    thu_id = 0,
    //                    tiet_id = 0,
    //                    lichbaogiangchitiet_monhoc = "",
    //                    username_username = ""
    //                });
    //            }
    //            else
    //            {
    //                LBG.Add(new LichBaoGiang()
    //                {
    //                    lichbaogiangchitiet_id = getData.FirstOrDefault().lichbaogiangchitiet_id,
    //                    thu_id = thu,
    //                    tiet_id = tiet,
    //                    lichbaogiangchitiet_monhoc = getData.FirstOrDefault().lichbaogiangchitiet_monhoc,
    //                    username_username = getData.FirstOrDefault().username_fullname.Split(' ').Last()
    //                });
    //            }
    //        }
    //        return LBG;
    //    }
    //}
    protected void rpThu_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        List<SoTiet> tietHoc = new List<SoTiet>();
        Repeater rpThu2Tiet = e.Item.FindControl("rpThu2Tiet") as Repeater;
        int _idthu = int.Parse(DataBinder.Eval(e.Item.DataItem, "thu_id").ToString());
        SoTiet tiet1 = new SoTiet();
        tiet1.id = 1;
        tiet1.tiet = 1;
        tiet1.thu_id = _idthu;
        tietHoc.Add(tiet1);
        SoTiet tiet2 = new SoTiet();
        tiet2.id = 2;
        tiet2.tiet = 2;
        tiet2.thu_id = _idthu;
        tietHoc.Add(tiet2);
        SoTiet tiet3 = new SoTiet();
        tiet3.id = 3;
        tiet3.tiet = 3;
        tiet3.thu_id = _idthu;
        tietHoc.Add(tiet3);
        SoTiet tiet4 = new SoTiet();
        tiet4.id = 4;
        tiet4.tiet = 4;
        tiet4.thu_id = _idthu;
        tietHoc.Add(tiet4);
        SoTiet tiet5 = new SoTiet();
        tiet5.id = 5;
        tiet5.tiet = 5;
        tiet5.thu_id = _idthu;
        tietHoc.Add(tiet5);
        SoTiet tiet6 = new SoTiet();
        tiet6.id = 6;
        tiet6.tiet = 6;
        tiet6.thu_id = _idthu;
        tietHoc.Add(tiet6);
        SoTiet tiet7 = new SoTiet();
        tiet7.id = 7;
        tiet7.tiet = 7;
        tiet7.thu_id = _idthu;
        tietHoc.Add(tiet7);
        SoTiet tiet8 = new SoTiet();
        tiet8.id = 8;
        tiet8.tiet = 8;
        tiet8.thu_id = _idthu;
        tietHoc.Add(tiet8);
        rpThu2Tiet.DataSource = tietHoc;
        rpThu2Tiet.DataBind();
    }

    protected void rpThu2Tiet_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Repeater rpThu2Mon = e.Item.FindControl("rpThu2Mon") as Repeater;
        int tiet = int.Parse(DataBinder.Eval(e.Item.DataItem, "tiet").ToString());
        int thu = int.Parse(DataBinder.Eval(e.Item.DataItem, "thu_id").ToString());
        //var LBG1 = getDataBaoGiang(thu, tiet);
        var gettuanhoc = (from t in db.tbHocTap_Tuans
                          where t.tuan_hidden == false
                          && t.tuan_id == Convert.ToInt32(ddlTuanHoc.SelectedItem.Value)
                          select t).SingleOrDefault();
        //List<LichBaoGiang1> LBG1 = new List<LichBaoGiang1>();

        List<LichBaoGiang1> LBG1 = new List<LichBaoGiang1>();
        List<LichBaoGiang1> LBG2 = new List<LichBaoGiang1>();
        var getData = from s in db.tbLichBaoGiangChiTiets
                      join t in db.tbLichBaoGiangTheoTuans on s.lichbaogiangtheotuan_id equals t.lichbaogiangtheotuan_id
                      join bg in db.tbLichBaoGiangs on t.lichbaogiang_id equals bg.lichbaogiang_id
                      join u in db.admin_Users on s.username_id equals u.username_id
                      where t.lichbaogiangtheotuan_thuhoc == "Thứ " + thu
                          //&& Convert.ToInt32(s.lichbaogiangchitiet_lop.Split('/').First())>5
                        && s.lichbaogiangchitiet_tiethoc == "Tiết " + tiet
                        && bg.tuan_id == gettuanhoc.tuan_id
                        && s.lichbaogiangchitiet_lop != ""
                      orderby bg.lichbaogiang_id descending
                      select new
                      {
                          s.lichbaogiangchitiet_id,
                          s.lichbaogiangchitiet_monhoc,
                          u.username_id,
                          u.username_fullname,
                          s.lichbaogiangchitiet_tiethoc,
                          s.lichbaogiangchitiet_lop,
                          t.lichbaogiangtheotuan_thuhoc
                      };
        /*
         * duyệt trong getData để tìm ra những dữ liệu của khối thcs
         * và add vào 1 list
         */
        for (int i = 0; i < Lop.Length; i++)
        {
            foreach (var item in getData)
            {
                //nếu có dữ liệu của lớp đó thì add vào List và thoát foreach, tăng i lên 1
                if (Lop[i] == item.lichbaogiangchitiet_lop)
                {
                    LBG1.Add(new LichBaoGiang1()
                    {
                        id=i,
                        thu_id = thu,
                        tiet_id = tiet,
                        lichbaogiangchitiet_id = item.lichbaogiangchitiet_id,
                        lichbaogiangchitiet_monhoc = item.lichbaogiangchitiet_monhoc,
                        username_username = item.username_fullname.Split(' ').Last()
                    });
                    break;
                }
                //ngược lại nếu chạy hết foreach mà không có dữ liệu thì add 1 cái rỗng
                else
                {
                }
            }
        }

        for (int i = 0; i < Lop.Length; i++)
        {
            LBG2.Add(new LichBaoGiang1()
            {
                id=i,
                thu_id = thu,
                tiet_id = tiet,
                lichbaogiangchitiet_id = 0,
                lichbaogiangchitiet_monhoc = "",
                username_username = ""
            });
        }
        var getData2 = LBG1.Union(LBG2).ToList();
        var getData3 = from g in getData2
                       group g by g.id into item
                       orderby item.Key
                       select new
                       {
                           item.Key,
                           item.First().thu_id,
                           item.First().tiet_id,
                           item.First().lichbaogiangchitiet_id,
                           item.First().lichbaogiangchitiet_monhoc,
                           item.First().username_username,
                       };
        rpThu2Mon.DataSource = getData3;
        rpThu2Mon.DataBind();
    }
    protected void btnXemChiTiet_ServerClick(object sender, EventArgs e)
    {
        try
        {
            //lấy id
            int _id = Convert.ToInt32(txt_ID.Value);
            //lấy thông tin chi tiết của lịch báo giảng
            var getDetail = (from ct in db.tbLichBaoGiangChiTiets
                             join bg in db.tbLichBaoGiangTheoTuans on ct.lichbaogiangtheotuan_id equals bg.lichbaogiangtheotuan_id
                             join u in db.admin_Users on ct.username_id equals u.username_id
                             where ct.lichbaogiangchitiet_id == _id
                             select new
                             {
                                 ct.lichbaogiangchitiet_id,
                                 ct.lichbaogiangchitiet_tiethoc,
                                 ct.lichbaogiangchitiet_tenbaigiang,
                                 ct.lichbaogiangchitiet_tietchuongtrinh,
                                 ct.lichbaogiangchitiet_ghichu,
                                 ct.lichbaogiangchitiet_monhoc,
                                 ct.lichbaogiangchitiet_buoihoc,
                                 ct.lichbaogiangchitiet_lop,
                                 ct.lichbaogiangtheotuan_id,
                                 bg.lichbaogiangtheotuan_thuhoc,
                                 bg.lichbaogiangtheotuan_ngayhoc,
                                 u.username_id,
                                 u.username_fullname

                             }).Take(1).SingleOrDefault();
            txtThuHoc.Value = getDetail.lichbaogiangtheotuan_thuhoc;
            txtBuoiHoc.Value = getDetail.lichbaogiangchitiet_buoihoc;
            txtGhiChu.Value = getDetail.lichbaogiangchitiet_ghichu;
            txtNguoiSoan.Value = getDetail.username_fullname;
            txtMonHoc.Value = getDetail.lichbaogiangchitiet_monhoc;
            txtTenBaiGiang.Value = getDetail.lichbaogiangchitiet_tenbaigiang;
            txtTietHoc.Value = getDetail.lichbaogiangchitiet_tiethoc;
            txtTietPPCT.Value = getDetail.lichbaogiangchitiet_tietchuongtrinh;
            txtLop.Value = getDetail.lichbaogiangchitiet_lop;
            txtNgayDay.Value = getDetail.lichbaogiangtheotuan_ngayhoc.Value.ToString("dd-MM-yyyy").Replace(' ', 'T');
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "text", "showDetail()", true);
        }
        catch (Exception ex)
        {
            alert.alert_Error(Page, "Đã xảy ra lỗi, vui lòng liên hệ IT", "");
        }
    }
    protected void btnDuGio_ServerClick(object sender, EventArgs e)
    {
        try
        {
            //lấy id của tiết được chọn
            int _id = Convert.ToInt32(txt_ID.Value);
            //lấy thông tin của tk login
            var getuser = (from u in db.admin_Users
                           where u.username_username == Request.Cookies["UserName"].Value
                           select u).FirstOrDefault();
            //kiểm tra xem tiết học này đã được username đăng ký dự giờ chưa
            var checkDuGio = from s in db.tbHocTap_DuGios
                             where s.lichbaogiangchitiet_id == _id
                             && s.nguoidugio_id == getuser.username_id
                             && s.hidden == false
                             select s;
            //lấy thông tin chi tiết của lịch báo giảng
            var getDetail = (from ct in db.tbLichBaoGiangChiTiets
                             join bg in db.tbLichBaoGiangTheoTuans on ct.lichbaogiangtheotuan_id equals bg.lichbaogiangtheotuan_id
                             join u in db.admin_Users on ct.username_id equals u.username_id
                             where ct.lichbaogiangchitiet_id == _id
                             select new
                             {
                                 ct.lichbaogiangchitiet_id,
                                 ct.lichbaogiangchitiet_tiethoc,
                                 ct.lichbaogiangchitiet_tenbaigiang,
                                 ct.lichbaogiangchitiet_tietchuongtrinh,
                                 ct.lichbaogiangchitiet_ghichu,
                                 ct.lichbaogiangchitiet_monhoc,
                                 ct.lichbaogiangchitiet_buoihoc,
                                 ct.lichbaogiangchitiet_lop,
                                 ct.lichbaogiangtheotuan_id,
                                 bg.lichbaogiangtheotuan_thuhoc,
                                 bg.lichbaogiangtheotuan_ngayhoc,
                                 u.username_id,
                                 u.username_fullname

                             }).Take(1).SingleOrDefault();
            //nếu ngày đki dự giờ >= 1 ngày so với tiết dạy
            if (getDetail.lichbaogiangtheotuan_ngayhoc <= DateTime.Now.AddDays(1))
            {
                alert.alert_Error(Page, "Bạn không thể đăng kí dự giờ tiết này vì không đủ 24h", "");
            }
            else if (checkDuGio.Count() > 0)
            {
                // nếu user đã đky dự giờ rồi thì khong cho đk tiếp nữa
                alert.alert_Warning(Page, "Bạn đã đăng kí dự giờ tiết này", "");
            }
            else
            {
                //cho đăng ký dự giờ
                tbHocTap_DuGio insert = new tbHocTap_DuGio();
                insert.lichbaogiangchitiet_id = _id;
                insert.nguoidugio_id = getuser.username_id;
                insert.hidden = false;
                insert.dugio_ngaydangky = DateTime.Now;
                insert.nguoibidugio_id = getDetail.username_id;
                db.tbHocTap_DuGios.InsertOnSubmit(insert);
                db.SubmitChanges();
                alert.alert_Success(Page, "Đăng ký dự giờ thành công", "");
                //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "AlertBox", "swal('Lưu thành công!', '','success').then(function(){window.location = '/admin-lich-bao-giang-khoi-tieu-hoc';})", true);
                //gửi mail về cho giáo viên bị đăng kí dự giờ
                SendMail("luuvanquyet2612@gmail.com");
            }
        }
        catch (Exception ex)
        {
            alert.alert_Error(Page, "Đã xảy ra lỗi, vui lòng liên hệ IT", "");
        }
    }
    private bool SendMail(string email)
    {

        if (email != "")
        {
            try
            {
                var fromAddress = "thongbaovietnhatschool@gmail.com";//  Email Address from where you send the mail 
                var toAddress = email;
                const string fromPassword = "neiabcekdjluofid";
                string subject, title;
                title = "Thông báo";
                subject = "<!DOCTYPE html><html><head><title></title></head><body ><div>" +
                "<h3 style=\"margin-top:0px; text-align:center; color:#029ada\">Bạn có đăng kí dự giờ mới. Xem chi tiết <a href='http://quantri.vietnhatschool.edu.vn/admin-xem-danh-sach-du-gio'>tại đây.</a></h3>" +
                "</div></body></html>";
                var smtp = new System.Net.Mail.SmtpClient();
                {
                    smtp.Host = "smtp.gmail.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    smtp.Credentials = new NetworkCredential(fromAddress, fromPassword);
                    smtp.Timeout = 20000;
                }
                MailMessage mm = new MailMessage();
                mm.From = new MailAddress(fromAddress, "Trường Liên cấp Việt Nhật");
                mm.Subject = title;
                mm.To.Add(toAddress);
                mm.IsBodyHtml = true;
                mm.Body = subject;
                smtp.Send(mm);
                return true;
            }
            catch
            {
                return false;
            }
        }
        else
            return false;
    }
    protected void btnXem_ServerClick(object sender, EventArgs e)
    {
        //if (ddlKhoi.SelectedValue == "KhoiTH")
        //{
        //    rpTieuHoc.DataSource = from lop in db.tbLops where lop.lop_position <= 17 orderby lop.lop_position select lop;
        //    rpTieuHoc.DataBind();
        //}
        //else
        //{
        //    rpTHCS.DataSource = from lop in db.tbLops where lop.lop_position > 17 orderby lop.lop_position select lop;
        //    rpTHCS.DataBind();
        //}
        loadData();
    }
}
public class SoTiet
{
    public int id { get; set; }
    public int tiet { get; set; }
    public int thu_id { get; set; }
}
public class LichBaoGiang1
{
    public int id { get; set; }
    public int thu_id { get; set; }
    public int tiet_id { get; set; }
    public int lichbaogiangchitiet_id { get; set; }
    public string lichbaogiangchitiet_monhoc { get; set; }
    public string username_username { get; set; }

}
public class ThuHoc1
{
    public int thu_id { get; set; }
    public string thu_name { get; set; }
}
