﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="module_XemLichTongNamTest2.aspx.cs" Inherits="admin_page_module_function_module_LichCongTac_module_XemLichTongNamTest" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../../admin_css/vendor.css" rel="stylesheet" />
    <link href="../../../css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="../../../css/bootstrap.min.css" rel="stylesheet" />
    <script src="../../../admin_js/sweetalert.min.js"></script>
    <script src="../../../js/jquery.min.js"></script>
    <script src="../../../js/jquery.dataTables.min.js"></script>
    <script src="../../../js/popper.min.js"></script>
    <script src="../../../js/bootstrap.min.js"></script>
    <%--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>--%>
</head>
<body>
    <style>
        #box-modal {
            width: 1000px;
            height: 298px;
            display: block;
            position: fixed;
            top: 20%;
            left: 14%;
            box-shadow: 7px 6px 16px burlywood;
            border-radius: 16px;
            animation-name: modal;
            animation-duration: 2s;
            z-index: 1000;
            background-color: #e3e8e6;
            outline: none;
        }

            #box-modal .name {
                position: absolute;
                top: 10%;
                left: 31%;
                font-size: 30px;
                font-weight: bold;
                color: #1f58c1;
            }

            #box-modal .table {
                position: absolute;
                top: 30%;
                left: 2%;
                width: 95%;
                text-align: center;
            }

            #box-modal #btnTieptuc {
                position: absolute;
                right: 7%;
                bottom: 13%;
                background-color: azure;
                color: green;
                font-weight: bold;
            }

            #box-modal #btnDangKyDuGio {
                position: absolute;
                right: 20%;
                bottom: 13%;
                background-color: azure;
                color: green;
                font-weight: bold;
            }

        @keyframes modal {
            from {
                top: 0%;
                left: 14%;
            }

            to {
                top: 20%;
                left: 14%;
            }
        }

        .authen-center {
            text-align: center;
        }

        .header-lichtong {
            display: flex;
            align-items: center;
            justify-content: space-between;
        }

        a {
            color: #000;
        }

        p.username {
            color: #0275d8;
        }

        .dugio {
            background-color: #98dc4d;
        }


        .pane--table2 table {
            border-collapse: collapse;
            background: white;
            table-layout: fixed;
            width: 100%;
        }

        .pane--table2 th, .pane--table2 td {
            padding: 8px 16px;
            border: 1px solid #ddd;
            width: 160px;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
        }

        .pane--table2 {
            width: 100%;
            overflow-x: scroll;
        }

            .pane--table2 th, .pane--table2 td {
                width: auto;
                min-width: 160px;
            }

            .pane--table2 tbody {
                overflow-y: scroll;
                overflow-x: hidden;
                display: block;
                height: 340px;
            }

            .pane--table2 thead {
                display: table-row;
            }

        .line__clamp {
            -webkit-line-clamp: 1;
            display: -webkit-box;
            -webkit-box-orient: vertical;
            overflow: hidden;
            width: 125px;
            text-overflow: ellipsis;
        }

        .loading {
            display: none;
            width: 100%;
            height: 100%;
            z-index: 1;
            background-color: rgba(0,0,0,0.6);
            position: fixed;
        }

        .loading__img {
            position: fixed;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }

            .loading__img span {
                position: absolute;
                top: 111%;
                text-align: center !important;
                left: -46px;
                color: #fff;
                font-size: 25px;
                width: 200px;
            }

        .loader {
            border: 16px solid #f3f3f3;
            border-radius: 50%;
            border-top: 16px solid #3498db;
            width: 100px;
            height: 100px;
            -webkit-animation: spin 1s linear infinite; /* Safari */
            animation: spin 1s linear infinite;
        }

        .title__date {
            display: flex;
            align-items: center;
            justify-content: center;
            font-size: 27px;
            font-weight: 600;
            color: #b51a1a;
        }

        .nav__tkb {
            display: flex;
            justify-content: space-around;
            align-items: center;
        }

        .img__tkb {
            width: 75%;
            margin: 0 auto;
            text-align: center;
            display: block;
            margin-top: 50px;
            /*box-shadow: 0 15px 10px rgba(0,0,0,0.9);
            border-radius: 20px;*/
        }

        .nav__tkb .nav-link {
            font-weight: 600;
        }
        /* Safari */
        @-webkit-keyframes spin {
            0% {
                -webkit-transform: rotate(0deg);
            }

            100% {
                -webkit-transform: rotate(360deg);
            }
        }

        @keyframes spin {
            0% {
                transform: rotate(0deg);
            }

            100% {
                transform: rotate(360deg);
            }
        }
    </style>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="smScriptManager" runat="server"></asp:ScriptManager>
        <div class="loading" id="img-loading-icon">
            <%--<div class="spinner-border text-primary" role="status">
                <span class="sr-only">Loading...</span>
            </div>--%>
            <div class="loading__img">
                <div class="loader"></div>
                <span>Vui lòng chờ đợi từ 1 tới 2 phút</span>
            </div>
        </div>
        <div>
            <div class="card card-block">
                <div id="dvKetQua" runat="server">
                    <div>
                        <div class="header-lichtong">
                            <div class="header-lichtong__title">
                                <b>SỞ GIÁO DỤC & ĐÀO TẠO ĐÀ NẴNG</b>
                                <br />
                                <b>TRƯỜNG TH-THCS & THPT VIỆT NHẬT</b>
                            </div>
                            <div style="float: right">
                                <a href="/admin-xem-lich-bao-giang-giao-vien" class="btn btn-primary">Xem chi tiết giáo viên</a>
                                <a href="/admin-lich-bao-giang-tong-theo-lop" class="btn btn-primary">Xem chi tiết theo lớp</a>
                                <a href="/admin-home" class="btn btn-primary">Quay lại</a>
                            </div>
                        </div>
                        <div style="text-align: center">
                            <br />
                            <h3><b>THỜI KHÓA BIỂU NĂM 2020 - 2021 </b></h3>
                            <br />
                            <%--<p><b><%=tuanhoc %><b></p>--%>
                        </div>
                        <script>
                            function DisplayLoadingIcon() {
                                $("#img-loading-icon").show();
                                console.log("đã show icon");
                            }
                        </script>
                        <div class="row">
                            <div class="col-6" style="margin: 0 auto; float: unset;">
                                <div class="col-6" style="margin: 0 auto; float: unset;">
                                    <span style="display: flex; align-items: center;" class="mb-2">Tuần:&nbsp;
                                    <asp:DropDownList ID="ddlTuanHoc" CssClass="form-control" runat="server"></asp:DropDownList>
                                        <a href="#" id="btnXem" runat="server" class="btn btn-primary" onclick="DisplayLoadingIcon();" onserverclick="btnXem_ServerClick">Chọn</a>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="pane pane--table2 table-scroll">
            <table>
                <thead>
                    <tr>
                        <th>Thứ</th>
                        <th>Tiết</th>
                        <th>Lớp 1/1</th>
                        <th>Lớp 1/2</th>
                        <th>Lớp 1/3</th>
                        <th>Lớp 1/4</th>
                        <th>Lớp 2/1</th>
                        <th>Lớp 2/2</th>
                        <th>Lớp 2/3</th>
                        <th>Lớp 2/4</th>
                        <th>Lớp 3/1</th>
                        <th>Lớp 3/2</th>
                        <th>Lớp 3/3</th>
                        <th>Lớp 3/4</th>
                        <th>Lớp 4/1</th>
                        <th>Lớp 4/2</th>
                        <th>Lớp 4/3</th>
                        <th>Lớp 4/4</th>
                        <th>Lớp 5/1</th>
                        <th>Lớp 5/2</th>
                        <th>Lớp 6/1</th>
                        <th>Lớp 7/1</th>
                        <th>Lớp 8/1</th>
                        <th>Lớp 9/1</th>
                        <th>Lớp 10/1</th>
                        <th>Lớp 11/1</th>
                        <th>Lớp 12/1</th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater runat="server" ID="rpThu" OnItemDataBound="rpThu_ItemDataBound">
                        <ItemTemplate>
                            <tr>
                                <td rowspan="9"><%#Eval("thu_name") %></td>
                            </tr>
                            <asp:Repeater ID="rpThu2Tiet" runat="server" OnItemDataBound="rpThu2Tiet_ItemDataBound">
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Container.ItemIndex + 1 %></td>
                                        <asp:Repeater runat="server" ID="rpThu2Mon">
                                            <ItemTemplate>
                                                <td style='<%#Eval("lichbaogiangchitiet_mausacdangkidugio") %>' id="txt_<%#Eval("lichbaogiangchitiet_id") %>"><a class="line__clamp" data-toggle="tooltip" data-placement="left" title="Thứ <%#Eval("thu_id") %>, tiết <%#Eval("tiet_id") %>" href="javascript:void(0)" id="<%#Eval("lichbaogiangchitiet_id") %>" onclick="myDetail(<%#Eval("lichbaogiangchitiet_id") %>)"><%#Eval("lichbaogiangchitiet_monhoc") %></a><p class="username"><%#Eval("username_username") %><span style="float: right"><%#Eval("lichbaogiangchitiet_soluongnguoidangki") %></span></p>
                                                </td>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
        </div>
        <br />

        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <div style="display: none">
                    <input id="txt_ID" type="text" runat="server" />
                    <a href="javascript:void(0)" id="btnXemChiTiet" type="button" runat="server" onserverclick="btnXemChiTiet_ServerClick">Xem chi tiết</a>
                    <a href="javascript:void(0)" id="btnDuGio" type="button" runat="server" onserverclick="btnDuGio_ServerClick">Dự giờ</a>
                    <input id="txtThuHoc" type="text" runat="server" />
                    <input id="txtBuoiHoc" type="text" runat="server" />
                    <input id="txtTietHoc" type="text" runat="server" />
                    <input id="txtLop" type="text" runat="server" />
                    <input id="txtNgayDay" type="text" runat="server" />
                    <input id="txtMonHoc" type="text" runat="server" />
                    <input id="txtNguoiSoan" type="text" runat="server" />
                    <input id="txtTietPPCT" type="text" runat="server" />
                    <input id="txtTenBaiGiang" type="text" runat="server" />
                    <input id="txtGhiChu" type="text" runat="server" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <%--modal--%>
        <div style="display: none" id="box-modal">
            <p class="name">CHI TIẾT LỊCH BÁO GIẢNG</p>
            <table class="table table-hover">
                <tr>
                    <th style="text-align: center">Thứ</th>
                    <th style="text-align: center">Buổi</th>
                    <th style="text-align: center">Tiết</th>
                    <th style="text-align: center">Lớp</th>
                    <th style="text-align: center">Ngày dạy</th>
                    <th style="text-align: center">Môn</th>
                    <th style="text-align: center">Tiết<br />
                        (PPCT)</th>
                    <th style="text-align: center">Tên bài giảng</th>
                    <th style="text-align: center">Ghi chú</th>
                    <th style="text-align: center">Người soạn</th>
                </tr>
                <%--thứ 2--%>
                <tr>
                    <td id="thu"></td>
                    <td id="buoi"></td>
                    <td id="tiet"></td>
                    <td id="lop"></td>
                    <td id="ngayday"></td>
                    <td id="mon"></td>
                    <td id="tietppct"></td>
                    <td id="baigiang"></td>
                    <td id="ghichu"></td>
                    <td id="nguoisoan"></td>
                </tr>
            </table>
            <a id="btnDangKyDuGio" href="javascript:void(0)" onclick="DangKyDuGio()" class="btn btn-primary">Đăng ký dự giờ</a>
            <a id="btnTieptuc" href="javascript:void(0)" class="btn btn-primary">Đóng</a>
        </div>
    </form>
    <script>
        $('#maintalbe .pane-hScroll').scroll(function () {
            $('.pane-vScroll').width($('.pane-hScroll').width() + $('.pane-hScroll').scrollLeft());
        });
        function DangKyDuGio() {
            swal("Bạn có thực sự muốn đăng kí dự giờ tiết học này?",
                "",
                "warning",
                {
                    buttons: true,
                    successMode: true
                }).then(function (value) {
                    if (value == true) {
                        var xoa = document.getElementById('<%=btnDuGio.ClientID%>');
                        xoa.click();
                    }
                });
        }
        var a, b, c;
        function myDetail(id) {
            document.getElementById("<%= txt_ID.ClientID%>").value = id;
            document.getElementById("<%= btnXemChiTiet.ClientID%>").click();
        }
        function showDetail() {
            document.getElementById("thu").innerHTML = document.getElementById("<%= txtThuHoc.ClientID%>").value;
            document.getElementById("buoi").innerHTML = document.getElementById("<%= txtBuoiHoc.ClientID%>").value;
            document.getElementById("tiet").innerHTML = document.getElementById("<%= txtTietHoc.ClientID%>").value;
            document.getElementById("lop").innerHTML = document.getElementById("<%= txtLop.ClientID%>").value;
            document.getElementById("ngayday").innerHTML = document.getElementById("<%= txtNgayDay.ClientID%>").value;
            document.getElementById("mon").innerHTML = document.getElementById("<%= txtMonHoc.ClientID%>").value;
            document.getElementById("tietppct").innerHTML = document.getElementById("<%= txtTietPPCT.ClientID%>").value;
            document.getElementById("baigiang").innerHTML = document.getElementById("<%= txtTenBaiGiang.ClientID%>").value;
            document.getElementById("ghichu").innerHTML = document.getElementById("<%= txtGhiChu.ClientID%>").value;
            document.getElementById("nguoisoan").innerHTML = document.getElementById("<%= txtNguoiSoan.ClientID%>").value;
            document.getElementById("box-modal").style.display = "block";
            document.getElementById("btnTieptuc").addEventListener("click", function () {
                document.getElementById("box-modal").style.display = "none";
            });
        }
        //window.onload = function () {
        //    getDugio();
        //}
        //function getDugio(id) {
        //    var element = document.getElementById("txt_" + id);
        //    element.classList.add("dugio");
        //};
        $(document).ready(function () {
            $('#example').DataTable({
                "scrollY": 200,
                "scrollX": true,
                "searching": false,
                "info": false,
                "bPaginate": false
                //"pageLength": false


            });
        });
        $('.pane--table2').scroll(function () {
            $('.pane--table2 table').width($('.pane--table2').width() + $('.pane--table2').scrollLeft());
        });
    </script>

</body>
</html>
