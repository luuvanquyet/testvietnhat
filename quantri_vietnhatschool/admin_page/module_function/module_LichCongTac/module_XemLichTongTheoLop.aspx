﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_XemLichTongTheoLop.aspx.cs" Inherits="admin_page_module_function_module_LichCongTac_module_XemLichTongTheoLop" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <script src="../../../js/jquery.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <style>
        a.lbg__link {
            color: #000 !important;
            text-decoration: none !important;
        }

        p.username {
            color: #0275d8;
        }

        #box-modal {
            width: 1000px;
            height: 298px;
            display: block;
            position: fixed;
            top: 20%;
            left: 14%;
            box-shadow: 7px 6px 16px burlywood;
            border-radius: 16px;
            animation-name: modal;
            animation-duration: 2s;
            z-index: 1000;
            background-color: #e3e8e6;
            outline: none;
        }

            #box-modal .name {
                position: absolute;
                top: 10%;
                left: 31%;
                font-size: 30px;
                font-weight: bold;
                color: #1f58c1;
            }

            #box-modal .table {
                position: absolute;
                top: 30%;
                left: 2%;
                width: 95%;
                text-align: center;
            }

            #box-modal #btnTieptuc {
                position: absolute;
                right: 7%;
                bottom: 13%;
                background-color: azure;
                color: green;
                font-weight: bold;
            }

            #box-modal #btnDangKyDuGio {
                position: absolute;
                right: 20%;
                bottom: 13%;
                background-color: azure;
                color: green;
                font-weight: bold;
            }

        .loading {
            display: none;
            width: 100%;
            height: 100%;
            z-index: 1;
            background-color: rgba(0,0,0,0.6);
            position: fixed;
        }

        .loading__img {
            position: fixed;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }

            .loading__img span {
                position: absolute;
                top: 111%;
                text-align: center !important;
                left: -46px;
                color: #fff;
                font-size: 25px;
                width: 200px;
            }

        .loader {
            border: 16px solid #f3f3f3;
            border-radius: 50%;
            border-top: 16px solid #3498db;
            width: 100px;
            height: 100px;
            -webkit-animation: spin 1s linear infinite; /* Safari */
            animation: spin 1s linear infinite;
        }

        @keyframes modal {
            from {
                top: 0%;
                left: 14%;
            }

            to {
                top: 20%;
                left: 14%;
            }
        }
    </style>
    <div class="loading" id="img-loading-icon">
        <div class="loading__img">
            <div class="loader"></div>
            <span>Vui lòng chờ đợi vài phút</span>
        </div>
    </div>
    <script>
        function DisplayLoadingIcon() {
            $("#img-loading-icon").show();
            console.log("đã show icon");
        }
    </script>
    <div class="card card-block">
        <div class="form-group row">
            <div class="col-sm-10">
                <span style="display: flex; align-items: center;">Tuần:<asp:DropDownList ID="ddlTuanHoc" CssClass="form-control" runat="server" Width="30%"></asp:DropDownList>&nbsp;
                            Lớp:<asp:DropDownList ID="ddlLop" CssClass="form-control" runat="server" Width="30%">
                                <asp:ListItem Value="0">Chọn lớp</asp:ListItem>
                                <asp:ListItem Value="1/1">Lớp 1/1</asp:ListItem>
                                <asp:ListItem Value="1/2">Lớp 1/2</asp:ListItem>
                                <asp:ListItem Value="1/3">Lớp 1/3</asp:ListItem>
                                <asp:ListItem Value="1/4">Lớp 1/4</asp:ListItem>
                                <asp:ListItem Value="2/1">Lớp 2/1</asp:ListItem>
                                <asp:ListItem Value="2/2">Lớp 2/2</asp:ListItem>
                                <asp:ListItem Value="2/3">Lớp 2/3</asp:ListItem>
                                <asp:ListItem Value="2/4">Lớp 2/4</asp:ListItem>
                                <asp:ListItem Value="3/1">Lớp 3/1</asp:ListItem>
                                <asp:ListItem Value="3/2">Lớp 3/2</asp:ListItem>
                                <asp:ListItem Value="3/3">Lớp 3/3</asp:ListItem>
                                <asp:ListItem Value="3/4">Lớp 3/4</asp:ListItem>
                                <asp:ListItem Value="4/1">Lớp 4/1</asp:ListItem>
                                <asp:ListItem Value="4/2">Lớp 4/2</asp:ListItem>
                                <asp:ListItem Value="4/3">Lớp 4/3</asp:ListItem>
                                <asp:ListItem Value="4/4">Lớp 4/4</asp:ListItem>
                                <asp:ListItem Value="5/1">Lớp 5/1</asp:ListItem>
                                <asp:ListItem Value="5/2">Lớp 5/2</asp:ListItem>
                                <asp:ListItem Value="6/1">Lớp 6/1</asp:ListItem>
                                <asp:ListItem Value="7/1">Lớp 7/1</asp:ListItem>
                                <asp:ListItem Value="8/1">Lớp 8/1</asp:ListItem>
                                <asp:ListItem Value="9/1">Lớp 9/1</asp:ListItem>
                                <asp:ListItem Value="10/1">Lớp 10/1</asp:ListItem>
                                <asp:ListItem Value="11/1">Lớp 11/1</asp:ListItem>
                                <asp:ListItem Value="12/1">Lớp 12/1</asp:ListItem>
                            </asp:DropDownList>
                    <a href="javascript:void(0)" class="btn btn-primary text-white ml-2" runat="server" id="btnXem" onserverclick="btnXem_ServerClick">Xem</a>
                </span>
            </div>
        </div>
        <div class="form-group table-responsive">
            <table class="table table-striped table-hover">
                <tr style="text-align: center">
                    <th scope="col">#</th>
                    <th scope="col">Tiết 1</th>
                    <th scope="col">Tiết 2</th>
                    <th scope="col">Tiết 3</th>
                    <th scope="col">Tiết 4</th>
                    <th scope="col">Tiết 5</th>
                    <th scope="col">Tiết 6</th>
                    <th scope="col">Tiết 7</th>
                    <th scope="col">Tiết 8</th>
                </tr>
                <asp:Repeater runat="server" ID="rpThu" OnItemDataBound="rpThu_ItemDataBound">
                    <ItemTemplate>
                        <tr>
                            <th scope="row"><%#Eval("thu_name") %></th>
                            <asp:Repeater runat="server" ID="rpChiTiet">
                                <ItemTemplate>
                                    <th style='<%#Eval("lichbaogiangchitiet_mausacdangkidugio") %>' id="txt_<%#Eval("lichbaogiangchitiet_id") %>"><a class="line__clamp" href="javascript:void(0)" id="<%#Eval("lichbaogiangchitiet_id") %>" onclick="myDetail(<%#Eval("lichbaogiangchitiet_id") %>)"><%#Eval("lichbaogiangchitiet_monhoc") %></a><p class="username"><%#Eval("username_username") %> <span style="float: right"><%#Eval("lichbaogiangchitiet_soluongnguoidangki") %></span></p>
                                    </th>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </table>
        </div>
        <div style="display: none">
            <input id="txt_ID" type="text" runat="server" />
            <a class="lbg__link" href="javascript:void(0)" id="btnXemChiTiet" type="button" runat="server" onserverclick="btnXemChiTiet_ServerClick">Xem chi tiết</a>
            <a class="lbg__link" href="javascript:void(0)" id="btnDuGio" type="button" runat="server" onserverclick="btnDuGio_ServerClick">Dự giờ</a>
            <input id="txtThuHoc" type="text" runat="server" />
            <input id="txtBuoiHoc" type="text" runat="server" />
            <input id="txtTietHoc" type="text" runat="server" />
            <input id="txtLop" type="text" runat="server" />
            <input id="txtNgayDay" type="text" runat="server" />
            <input id="txtMonHoc" type="text" runat="server" />
            <input id="txtNguoiSoan" type="text" runat="server" />
            <input id="txtTietPPCT" type="text" runat="server" />
            <input id="txtTenBaiGiang" type="text" runat="server" />
            <input id="txtGhiChu" type="text" runat="server" />
        </div>
        <%--modal--%>
        <div style="display: none" id="box-modal">
            <p class="name">CHI TIẾT LỊCH BÁO GIẢNG</p>
            <table class="table table-hover">
                <tr>
                    <th style="text-align: center">Thứ</th>
                    <th style="text-align: center">Buổi</th>
                    <th style="text-align: center">Tiết</th>
                    <th style="text-align: center">Lớp</th>
                    <th style="text-align: center">Ngày dạy</th>
                    <th style="text-align: center">Môn</th>
                    <th style="text-align: center">Tiết<br />
                        (PPCT)</th>
                    <th style="text-align: center">Tên bài giảng</th>
                    <th style="text-align: center">Ghi chú</th>
                    <th style="text-align: center">Người soạn</th>
                </tr>
                <tr>
                    <td id="thu"></td>
                    <td id="buoi"></td>
                    <td id="tiet"></td>
                    <td id="lop"></td>
                    <td id="ngayday"></td>
                    <td id="mon"></td>
                    <td id="tietppct"></td>
                    <td id="baigiang"></td>
                    <td id="ghichu"></td>
                    <td id="nguoisoan"></td>
                </tr>
            </table>
            <a class="lbg__link" id="btnDangKyDuGio" href="javascript:void(0)" onclick="DangKyDuGio()" class="btn btn-primary">Đăng ký dự giờ</a>
            <a class="lbg__link" id="btnTieptuc" href="javascript:void(0)" class="btn btn-primary">Đóng</a>
        </div>
    </div>
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
        function DangKyDuGio() {
            swal("Bạn có thực sự muốn đăng kí dự giờ tiết học này?",
                "",
                "warning",
                {
                    buttons: true,
                    successMode: true
                }).then(function (value) {
                    if (value == true) {
                        var xoa = document.getElementById('<%=btnDuGio.ClientID%>');
                        xoa.click();
                    }
                });
        }
        var a, b, c;
        function myDetail(id) {
            document.getElementById("<%= txt_ID.ClientID%>").value = id;
                document.getElementById("<%= btnXemChiTiet.ClientID%>").click();
        }
        function showDetail() {
            document.getElementById("thu").innerHTML = document.getElementById("<%= txtThuHoc.ClientID%>").value;
                document.getElementById("buoi").innerHTML = document.getElementById("<%= txtBuoiHoc.ClientID%>").value;
                document.getElementById("tiet").innerHTML = document.getElementById("<%= txtTietHoc.ClientID%>").value;
                document.getElementById("lop").innerHTML = document.getElementById("<%= txtLop.ClientID%>").value;
                document.getElementById("ngayday").innerHTML = document.getElementById("<%= txtNgayDay.ClientID%>").value;
                document.getElementById("mon").innerHTML = document.getElementById("<%= txtMonHoc.ClientID%>").value;
                document.getElementById("tietppct").innerHTML = document.getElementById("<%= txtTietPPCT.ClientID%>").value;
                document.getElementById("baigiang").innerHTML = document.getElementById("<%= txtTenBaiGiang.ClientID%>").value;
                document.getElementById("ghichu").innerHTML = document.getElementById("<%= txtGhiChu.ClientID%>").value;
                document.getElementById("nguoisoan").innerHTML = document.getElementById("<%= txtNguoiSoan.ClientID%>").value;
            document.getElementById("box-modal").style.display = "block";
            document.getElementById("btnTieptuc").addEventListener("click", function () {
                document.getElementById("box-modal").style.display = "none";
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

