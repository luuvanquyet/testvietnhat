﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_LichCongTac_module_XemLichTongTheoLop : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    public int Thu = 2;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //đổ ra ds tuần học
            var dsTuan = from t in db.tbHocTap_Tuans where t.tuan_hidden == false && t.namhoc_id==3 select t;
            ddlTuanHoc.Items.Clear();
            ddlTuanHoc.Items.Insert(0, "Chọn Tuần học");
            ddlTuanHoc.AppendDataBoundItems = true;
            ddlTuanHoc.DataTextField = "tuan_name";
            ddlTuanHoc.DataValueField = "tuan_id";
            ddlTuanHoc.DataSource = dsTuan;
            ddlTuanHoc.DataBind();
        }

    }

    private void loadData()
    {
        if (ddlTuanHoc.SelectedValue == "0" || ddlTuanHoc.SelectedValue == "Chọn Tuần học"
            && ddlLop.SelectedValue == "0" || ddlLop.SelectedValue == "Chọn lớp")
        {
            alert.alert_Warning(Page, "Vui lòng chọn dữ liệu để xem", "");
        }
        else if (ddlTuanHoc.SelectedValue == "0" || ddlTuanHoc.SelectedValue == "Chọn Tuần học")
        {
            alert.alert_Warning(Page, "Vui lòng chọn tuần", "");
        }
        else if (ddlLop.SelectedValue == "0" || ddlLop.SelectedValue == "Chọn lớp")
        {
            alert.alert_Warning(Page, "Vui lòng chọn lớp", "");
        }
        else
        {
            List<ThuHoc> thuhoc = new List<ThuHoc>();
            ThuHoc thuHoc2 = new ThuHoc();
            thuHoc2.thu_id = 2;
            thuHoc2.thu_name = "Thứ 2";
            thuhoc.Add(thuHoc2);
            ThuHoc thuHoc3 = new ThuHoc();
            thuHoc3.thu_id = 3;
            thuHoc3.thu_name = "Thứ 3";
            thuhoc.Add(thuHoc3);
            ThuHoc thuHoc4 = new ThuHoc();
            thuHoc4.thu_id = 4;
            thuHoc4.thu_name = "Thứ 4";
            thuhoc.Add(thuHoc4);
            ThuHoc thuHoc5 = new ThuHoc();
            thuHoc5.thu_id = 5;
            thuHoc5.thu_name = "Thứ 5";
            thuhoc.Add(thuHoc5);
            ThuHoc thuHoc6 = new ThuHoc();
            thuHoc6.thu_id = 6;
            thuHoc6.thu_name = "Thứ 6";
            thuhoc.Add(thuHoc6);
            rpThu.DataSource = thuhoc;
            rpThu.DataBind();
        }
    }

    protected void rpThu_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Repeater rpChiTiet = e.Item.FindControl("rpChiTiet") as Repeater;
        int _idthu = int.Parse(DataBinder.Eval(e.Item.DataItem, "thu_id").ToString());
        //var LBG = getDataBaoGiang(_idthu, ddlLop.SelectedValue);
        var gettuanhoc = (from t in db.tbHocTap_Tuans
                          where t.tuan_hidden == false
                          && t.tuan_id == Convert.ToInt32(ddlTuanHoc.SelectedItem.Value)
                          select t).SingleOrDefault();
        // hiện tại 5 ngày là chạy 8 tiết tổng cộng là 40 lần chạy getData

        // giờ làm thế nào để chạy 5 ngày là 5 lần get data
        var getData = from s in db.tbLichBaoGiangChiTiets
                      join t in db.tbLichBaoGiangTheoTuans on s.lichbaogiangtheotuan_id equals t.lichbaogiangtheotuan_id
                      join bg in db.tbLichBaoGiangs on t.lichbaogiang_id equals bg.lichbaogiang_id
                      join u in db.admin_Users on s.username_id equals u.username_id
                      where t.lichbaogiangtheotuan_thuhoc == "Thứ " + _idthu
                        && s.lichbaogiangchitiet_lop == ddlLop.SelectedValue
                        //&& s.lichbaogiangchitiet_tiethoc == "Tiết " + i
                        && bg.tuan_id == gettuanhoc.tuan_id
                      orderby bg.lichbaogiang_id descending
                      select new
                      {
                          s.lichbaogiangchitiet_id,
                          s.lichbaogiangchitiet_monhoc,
                          u.username_id,
                          u.username_fullname,
                          s.lichbaogiangchitiet_tiethoc,
                          s.lichbaogiangchitiet_mausacdangkidugio,
                          // Số lượng người đăng ký dự giờ
                          lichbaogiangchitiet_soluongnguoidangki = (from sldg in db.tbHocTap_DuGios
                                                                    where sldg.nguoibidugio_id == bg.username_id && sldg.lichbaogiangchitiet_id == s.lichbaogiangchitiet_id
                                                                    select sldg).Count() == 0 ? "" : (from sldg in db.tbHocTap_DuGios where sldg.nguoibidugio_id == bg.username_id && sldg.lichbaogiangchitiet_id == s.lichbaogiangchitiet_id select sldg).Count() + "",
                      };

        List<LichBaoGiang> LBG1 = new List<LichBaoGiang>();
        List<LichBaoGiang> LBG2 = new List<LichBaoGiang>();
        foreach (var item in getData)
        {
            LBG2.Add(new LichBaoGiang()
            {
                tiet_id = Convert.ToInt32(item.lichbaogiangchitiet_tiethoc.Split(' ').Last()),
                lichbaogiangchitiet_id = item.lichbaogiangchitiet_id,
                lichbaogiangchitiet_monhoc = item.lichbaogiangchitiet_monhoc,
                lichbaogiangchitiet_mausacdangkidugio = item.lichbaogiangchitiet_mausacdangkidugio,
                lichbaogiangchitiet_soluongnguoidangki = item.lichbaogiangchitiet_soluongnguoidangki + "",
                username_username = item.username_fullname.Split(' ').Last()
            });
        }
        for (int index = 1; index <= 8; index++)
        {
            LBG1.Add(new LichBaoGiang()
            {
                tiet_id = index,
                lichbaogiangchitiet_id = 0,
                lichbaogiangchitiet_monhoc = "",
                lichbaogiangchitiet_mausacdangkidugio = "",
                lichbaogiangchitiet_soluongnguoidangki = "",
                username_username = ""
            });
        }
        var getData2 = LBG2.Union(LBG1).ToList();
        var getData3 = from g in getData2
                       group g by g.tiet_id into item
                       orderby item.Key
                       select new
                       {
                           item.Key,
                           item.First().lichbaogiangchitiet_id,
                           item.First().lichbaogiangchitiet_monhoc,
                           item.First().lichbaogiangchitiet_mausacdangkidugio,
                           item.First().lichbaogiangchitiet_soluongnguoidangki,
                           item.First().username_username,
                       };
        rpChiTiet.DataSource = getData3;
        rpChiTiet.DataBind();
    }

    protected void btnXem_ServerClick(object sender, EventArgs e)
    {
        loadData();
    }

    protected void btnXemChiTiet_ServerClick(object sender, EventArgs e)
    {
        try
        {
            //lấy id
            int _id = Convert.ToInt32(txt_ID.Value);
            //lấy thông tin chi tiết của lịch báo giảng
            var getDetail = (from ct in db.tbLichBaoGiangChiTiets
                             join bg in db.tbLichBaoGiangTheoTuans on ct.lichbaogiangtheotuan_id equals bg.lichbaogiangtheotuan_id
                             join u in db.admin_Users on ct.username_id equals u.username_id
                             where ct.lichbaogiangchitiet_id == _id
                             select new
                             {
                                 ct.lichbaogiangchitiet_id,
                                 ct.lichbaogiangchitiet_tiethoc,
                                 ct.lichbaogiangchitiet_tenbaigiang,
                                 ct.lichbaogiangchitiet_tietchuongtrinh,
                                 ct.lichbaogiangchitiet_ghichu,
                                 ct.lichbaogiangchitiet_monhoc,
                                 ct.lichbaogiangchitiet_buoihoc,
                                 ct.lichbaogiangchitiet_lop,
                                 ct.lichbaogiangtheotuan_id,
                                 bg.lichbaogiangtheotuan_thuhoc,
                                 bg.lichbaogiangtheotuan_ngayhoc,
                                 ct.lichbaogiangchitiet_mausacdangkidugio,
                                 u.username_id,
                                 u.username_fullname

                             }).Take(1).SingleOrDefault();
            txtThuHoc.Value = getDetail.lichbaogiangtheotuan_thuhoc;
            txtBuoiHoc.Value = getDetail.lichbaogiangchitiet_buoihoc;
            txtGhiChu.Value = getDetail.lichbaogiangchitiet_ghichu;
            txtNguoiSoan.Value = getDetail.username_fullname;
            txtMonHoc.Value = getDetail.lichbaogiangchitiet_monhoc;
            txtTenBaiGiang.Value = getDetail.lichbaogiangchitiet_tenbaigiang;
            txtTietHoc.Value = getDetail.lichbaogiangchitiet_tiethoc;
            txtTietPPCT.Value = getDetail.lichbaogiangchitiet_tietchuongtrinh;
            txtLop.Value = getDetail.lichbaogiangchitiet_lop;
            txtNgayDay.Value = getDetail.lichbaogiangtheotuan_ngayhoc.Value.ToString("dd-MM-yyyy").Replace(' ', 'T');
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "text", "showDetail()", true);
        }
        catch (Exception ex)
        {
            alert.alert_Error(Page, "Đã xảy ra lỗi, vui lòng liên hệ IT", "");
        }
    }
    protected void btnDuGio_ServerClick(object sender, EventArgs e)
    {
        try
        {
            //lấy id của tiết được chọn
            int _id = Convert.ToInt32(txt_ID.Value);
            //lấy thông tin của tk login
            var getuser = (from u in db.admin_Users
                           where u.username_username == Request.Cookies["UserName"].Value
                           select u).FirstOrDefault();
            //kiểm tra xem tiết học này đã được username đăng ký dự giờ chưa
            var checkDuGio = from s in db.tbHocTap_DuGios
                             where s.lichbaogiangchitiet_id == _id
                             && s.nguoidugio_id == getuser.username_id
                             && s.hidden == false
                             select s;
            //lấy thông tin chi tiết của lịch báo giảng
            // tối thiểu 5 gv được đăng ký dự giờ

            int checksoluong = (from sldg in db.tbHocTap_DuGios
                                where sldg.lichbaogiangchitiet_id == _id
                                select sldg).Count();

            if (checksoluong > 4)
            {
                alert.alert_Error(Page, "Giáo viên đăng kí đã đủ số lượng, vui lòng đăng kí tiết khác!", "");
            }
            else
            {
                var getDetail = (from ct in db.tbLichBaoGiangChiTiets
                                 join bg in db.tbLichBaoGiangTheoTuans on ct.lichbaogiangtheotuan_id equals bg.lichbaogiangtheotuan_id
                                 join u in db.admin_Users on ct.username_id equals u.username_id
                                 where ct.lichbaogiangchitiet_id == _id
                                 select new
                                 {
                                     ct.lichbaogiangchitiet_id,
                                     ct.lichbaogiangchitiet_tiethoc,
                                     ct.lichbaogiangchitiet_tenbaigiang,
                                     ct.lichbaogiangchitiet_tietchuongtrinh,
                                     ct.lichbaogiangchitiet_ghichu,
                                     ct.lichbaogiangchitiet_monhoc,
                                     ct.lichbaogiangchitiet_buoihoc,
                                     ct.lichbaogiangchitiet_lop,
                                     ct.lichbaogiangtheotuan_id,
                                     ct.lichbaogiangchitiet_mausacdangkidugio,
                                     bg.lichbaogiangtheotuan_thuhoc,
                                     bg.lichbaogiangtheotuan_ngayhoc,
                                     u.username_id,
                                     u.username_fullname,
                                     u.username_email

                                 }).Take(1).SingleOrDefault();
                //nếu ngày đki dự giờ >= 24h so với tiết dạy
                if (getDetail.lichbaogiangtheotuan_ngayhoc <= DateTime.Now.AddHours(24))
                {
                    if (Request.Cookies["UserName"].Value == "bld02" || Request.Cookies["UserName"].Value == "bld01")
                    {
                        tbLichBaoGiangChiTiet update = (from ct in db.tbLichBaoGiangChiTiets
                                                        where ct.lichbaogiangchitiet_id == _id
                                                        select ct).SingleOrDefault();

                        update.lichbaogiangchitiet_mausacdangkidugio = "background-color:yellow";
                        db.SubmitChanges();
                        //cho đăng ký dự giờ
                        tbHocTap_DuGio insert = new tbHocTap_DuGio();
                        insert.lichbaogiangchitiet_id = _id;
                        insert.nguoidugio_id = getuser.username_id;
                        insert.hidden = false;
                        insert.dugio_ngaydangky = DateTime.Now;
                        insert.nguoibidugio_id = getDetail.username_id;
                        db.tbHocTap_DuGios.InsertOnSubmit(insert);
                        db.SubmitChanges();
                        alert.alert_Success(Page, "Đăng ký dự giờ thành công", "");
                        //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "AlertBox", "swal('Lưu thành công!', '','success').then(function(){window.location = '/admin-lich-bao-giang-khoi-tieu-hoc';})", true);
                        //gửi mail về cho giáo viên bị đăng kí dự giờ
                        //SendMail("phungduc1989@gmail.com");
                        SendMail(getDetail.username_email);
                    }
                    else
                    {
                        alert.alert_Error(Page, "Bạn không thể đăng kí dự giờ tiết này vì nằm trong khung 24h", "");
                    }
                }
                else if (checkDuGio.Count() > 0)
                {
                    // nếu user đã đky dự giờ rồi thì khong cho đk tiếp nữa
                    alert.alert_Warning(Page, "Bạn đã đăng kí dự giờ tiết này", "");
                }
                else
                {
                    tbLichBaoGiangChiTiet update = (from ct in db.tbLichBaoGiangChiTiets
                                                    where ct.lichbaogiangchitiet_id == _id
                                                    select ct).SingleOrDefault();

                    update.lichbaogiangchitiet_mausacdangkidugio = "background-color:yellow";
                    db.SubmitChanges();
                    //cho đăng ký dự giờ
                    tbHocTap_DuGio insert = new tbHocTap_DuGio();
                    insert.lichbaogiangchitiet_id = _id;
                    insert.nguoidugio_id = getuser.username_id;
                    insert.hidden = false;
                    insert.dugio_ngaydangky = DateTime.Now;
                    insert.nguoibidugio_id = getDetail.username_id;
                    db.tbHocTap_DuGios.InsertOnSubmit(insert);
                    db.SubmitChanges();
                    alert.alert_Success(Page, "Đăng ký dự giờ thành công", "");
                    //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "AlertBox", "swal('Lưu thành công!', '','success').then(function(){window.location = '/admin-lich-bao-giang-khoi-tieu-hoc';})", true);
                    //gửi mail về cho giáo viên bị đăng kí dự giờ
                    //SendMail("phungduc1989@gmail.com");
                    SendMail(getDetail.username_email);
                }
            }
        }
        catch (Exception ex)
        {
            alert.alert_Error(Page, "Đã xảy ra lỗi, vui lòng liên hệ IT", "");
        }
    }
    private bool SendMail(string email)
    {

        if (email != "")
        {
            try
            {
                var fromAddress = "thongbaovietnhatschool@gmail.com";//  Email Address from where you send the mail 
                var toAddress = email;
                const string fromPassword = "neiabcekdjluofid";
                string subject, title;
                title = "Thông báo";
                subject = "<!DOCTYPE html><html><head><title></title></head><body ><div>" +
                "<h3 style=\"margin-top:0px; text-align:center; color:#029ada\">Bạn có đăng kí dự giờ mới. Xem chi tiết <a href='http://quantri.vietnhatschool.edu.vn/admin-xem-danh-sach-du-gio'>tại đây.</a></h3>" +
                "</div></body></html>";
                var smtp = new System.Net.Mail.SmtpClient();
                {
                    smtp.Host = "smtp.gmail.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    smtp.Credentials = new NetworkCredential(fromAddress, fromPassword);
                    smtp.Timeout = 20000;
                }
                MailMessage mm = new MailMessage();
                mm.From = new MailAddress(fromAddress, "Trường Liên cấp Việt Nhật");
                mm.Subject = title;
                mm.To.Add(toAddress);
                mm.IsBodyHtml = true;
                mm.Body = subject;
                smtp.Send(mm);
                return true;
            }
            catch
            {
                return false;
            }
        }
        else
            return false;
    }

}
public class ThuHoc
{
    public int thu_id { get; set; }
    public string thu_name { get; set; }
}
public class LichBaoGiang
{
    public int tiet_id { get; set; }
    public int lichbaogiangchitiet_id { get; set; }
    public string lichbaogiangchitiet_monhoc { get; set; }
    public string lichbaogiangchitiet_mausacdangkidugio { get; set; }
    public string lichbaogiangchitiet_soluongnguoidangki { get; set; }
    public string username_username { get; set; }
}