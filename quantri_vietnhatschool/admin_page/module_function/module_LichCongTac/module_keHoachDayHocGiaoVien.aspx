﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_keHoachDayHocGiaoVien.aspx.cs" Inherits="admin_page_module_function_module_keHoachDayHocGiaoVien" %>

<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <style>
        .omt_menu_trai {
            width: 90%;
            background-color: #fff;
            border: 1px solid #32c5d2;
        }

        .omt_header {
            background-color: #32c5d2;
            width: 100%;
            height: 40px;
            padding: 10px;
            color: #fff;
            font-weight: bold;
        }

        .omt_menu_trai p {
            padding-left: 10px;
        }

        .omt_menu_trai input {
            width: 90%;
        }

        .omt_list {
            padding: 5px;
            width: 100%;
            border: 1px solid #32c5d2;
        }
    </style>
    <script>
        function CountLeft(field, count, max) {
            if (field.value.length > max)
                field.value = field.value.substring(0, max);
            else
                count.value = max - field.value.length;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <script type="text/javascript">
        function func() {
            grvList.Refresh();
            popupControl.Hide();
        }
        function btnChiTiet() {
            document.getElementById('<%=btnChiTiet.ClientID%>').click();
        }
        function confirmDel() {
            swal("Bạn có thực sự muốn xóa?",
                "Nếu xóa, dữ liệu sẽ không thể khôi phục.",
                "warning",
                {
                    buttons: true,
                    dangerMode: true
                }).then(function (value) {
                    if (value == true) {
                        var xoa = document.getElementById('<%=btnXoa.ClientID%>');
                        xoa.click();
                    }
                });
            }
            function isNumberKey(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;
                return true;
            }
    </script>
    <div class="card card-block">
        <div class="form-group row">
            <div class="col-sm-10">
                <%-- <asp:UpdatePanel ID="udButton" runat="server">
                    <ContentTemplate>--%>




                <%-- </ContentTemplate>
                </asp:UpdatePanel>--%>
            </div>
        </div>
        <div style="text-align: center">
            <b>KẾ HOACH DẠY HỌC NĂM HỌC 2020 – 2021</b>
            <br />
            <br />
            <b>MÔN:
                <input type="text" id="txtMon" runat="server" autocomplete="off" />
                LỚP:
                <input type="text" id="txtLop" runat="server" autocomplete="off" />
            </b>
            <br />
            <br />
            <b>Nội dung tóm tắt:
                <input type="text" id="txtSach" runat="server" style="width: 70%" autocomplete="off" /></b>
            <br />
            <br />
            <asp:Repeater ID="rpLop" runat="server">
                <ItemTemplate>
                     <a href="#" id="btnLop" runat="server" class="btn btn-primary"><%#Eval("lop_name") %></a>
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <br />
        <div class="add_Product_Detail row">
            <div class="col-4">
                <div class="omt_menu_trai">
                    <div class="omt_header">
                        Nhập thông tin
                    </div>
                    Tuần:
                    <p>
                        <input type="text" id="txtTuan" runat="server" autocomplete="off" class="form-control" />
                    </p>
                    Tiết:
                    <p>
                        <input type="text" id="txtTiet" runat="server" autocomplete="off" class="form-control" />
                    </p>
                    Phân môn:
                    <p>

                        <input type="text" id="txtPhanMon" runat="server" autocomplete="off" class="form-control" />
                    </p>
                    Tên bài học:
                    <p>

                        <input type="text" id="txtTenBaiHoc" runat="server" autocomplete="off" class="form-control" />
                    </p>
                    Nội dung yêu cầu kiến thức cần đạt được:
                    <p>

                        <input type="text" id="txtNoiDungCanDatDuoc" runat="server" autocomplete="off" class="form-control" />
                    </p>
                    Ghi chú:
                    <p>
                        <input type="text" id="txtGhiChu" runat="server" autocomplete="off" class="form-control" />
                    </p>
                    <p>
                        <a href="#" id="btnLuu" class="btn btn-primary" runat="server" onserverclick="btnLuu_ServerClick">lưu và thêm mới</a>
                        <a href="#" id="btnCapNhat" class="btn btn-primary" runat="server" onserverclick="btnCapNhat_ServerClick">Cập nhật</a>
                    </p>
                </div>
            </div>
            <div class="col-8">
                <div style="display: none">
                    <asp:FileUpload ID="fileUpload1" runat="server" />
                    <asp:Label ID="lblThongBao" runat="server"></asp:Label>
                    <asp:Button ID="btnImport" runat="server" Text="Import" OnClick="btnImport_Click" />
                </div>
                <div class="omt_list">
                    <%-- <asp:UpdatePanel ID="pnUpdate" runat="server">
                        <ContentTemplate>--%>
                    <asp:Button ID="btnRefresh" runat="server" Text="Load lại" CssClass="btn btn-primary" OnClick="btnRefresh_Click" />
                    <asp:Button ID="btnChiTiet" runat="server" Text="Chi tiết" CssClass="btn btn-primary" OnClick="btnChiTiet_Click" />
                    <%-- <input type="submit" class="btn btn-primary" value="Xóa" onclick="confirmDel()" CssClass="invisible" />--%>
                    <asp:Button ID="btnXoa" runat="server" Text="Xóa" CssClass="btn btn-primary" OnClick="btnXoa_Click" />
                    <%--</ContentTemplate>
                    </asp:UpdatePanel>--%>

                    <div class="form-group table-responsive ">
                        <dx:ASPxGridView ID="grvList" runat="server" ClientInstanceName="grvList" KeyFieldName="kehoachdayhoc_id" Width="100%">
                            <Columns>
                                <dx:GridViewCommandColumn ShowSelectCheckbox="True" SelectAllCheckboxMode="Page" VisibleIndex="0" Width="5%">
                                </dx:GridViewCommandColumn>
                                <dx:GridViewDataColumn Caption="Tuần" FieldName="kehoachdayhoc_tuan" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>
                                <dx:GridViewDataColumn Caption="Tiết" FieldName="kehoachdayhoc_tiet" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>
                                <dx:GridViewDataColumn Caption="Phân môn" FieldName="kehoachdayhoc_phanmon" HeaderStyle-HorizontalAlign="Center" Width="20%"></dx:GridViewDataColumn>
                                <dx:GridViewDataColumn Caption="Tên bài học" FieldName="kehoachdayhoc_tenbaihoc" HeaderStyle-HorizontalAlign="Center" Width="30%"></dx:GridViewDataColumn>
                                <dx:GridViewDataColumn Caption="nội dung yêu cầu cần đạt" FieldName="kehoachdayhoc_noidungcandatduoc" HeaderStyle-HorizontalAlign="Center" Width="30%"></dx:GridViewDataColumn>
                                <dx:GridViewDataColumn Caption="Ghi chú" FieldName="kehoachdayhoc_ghichu" HeaderStyle-HorizontalAlign="Center" Width="30%"></dx:GridViewDataColumn>
                            </Columns>
                            <ClientSideEvents RowDblClick="btnChiTiet" />
                            <SettingsSearchPanel Visible="true" />
                            <SettingsBehavior AllowFocusedRow="true" />
                            <SettingsText EmptyDataRow="Trống" SearchPanelEditorNullText="Gỏ từ cần tìm kiếm và enter..." />
                            <SettingsLoadingPanel Text="Đang tải..." />
                            <SettingsPager PageSize="10" Summary-Text="Trang {0} / {1} ({2} trang)"></SettingsPager>
                        </dx:ASPxGridView>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

