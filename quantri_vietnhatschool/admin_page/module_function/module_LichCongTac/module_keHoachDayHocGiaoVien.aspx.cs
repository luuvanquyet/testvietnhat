﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_keHoachDayHocGiaoVien : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            var checkuserid = (from u in db.admin_Users where u.username_username == Request.Cookies["UserName"].Value select u).First();
            Session["_id"] = 0;
            btnCapNhat.Visible = false;
            rpLop.DataSource = from l in db.tbQuanTri_kehoachdayhocs
                               where l.username_id == checkuserid.username_id
                               group l by l.kehoachdayhoc_lop into g
                               select new
                               {
                                   g.First().kehoachdayhoc_lop
                               };
        }
        loadData();
    }
    private void loadData()
    {
        try
        {
            var checkNamHoc = (from nh in db.tbHoctap_NamHocs orderby nh.namhoc_id descending select nh).FirstOrDefault();
            if (Request.Cookies["UserName"] != null)
            {
                var checkuserid = (from u in db.admin_Users where u.username_username == Request.Cookies["UserName"].Value select u).First();
                var getData = from tb in db.tbQuanTri_kehoachdayhocs
                              where tb.username_id == checkuserid.username_id && tb.namhoc_id == checkNamHoc.namhoc_id
                              orderby tb.kehoachdayhoc_id descending
                              select tb;
                grvList.DataSource = getData;
                grvList.DataBind();
            }
        }
        catch (Exception ex)
        {
            alert.alert_Error(Page, "Lỗi", "");
        }
    }

    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        btnLuu.Visible = false;
        btnCapNhat.Visible = true;
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "kehoachdayhoc_id" }));
        var checkid = (from u in db.tbQuanTri_kehoachdayhocs where u.kehoachdayhoc_id == _id select u).SingleOrDefault();
        txtMon.Value = checkid.kehoachdayhoc_mon;
        txtLop.Value = checkid.kehoachdayhoc_lop;
        txtSach.Value = checkid.kehoachdayhoc_noidung;
        txtTuan.Value = checkid.kehoachdayhoc_tuan + "";
        txtTiet.Value = checkid.kehoachdayhoc_tiet + "";
        txtPhanMon.Value = checkid.kehoachdayhoc_phanmon;
        txtTenBaiHoc.Value = checkid.kehoachdayhoc_tenbaihoc;
        txtNoiDungCanDatDuoc.Value = checkid.kehoachdayhoc_noidungcandatduoc;
        txtGhiChu.Value = checkid.kehoachdayhoc_ghichu;
        Session["_id"] = _id;
    }
    protected void btnXoa_Click(object sender, EventArgs e)
    {
        cls_KeHoachDayHoc cls;
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "kehoachdayhoc_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                cls = new cls_KeHoachDayHoc();
                if (cls.Linq_Xoa(Convert.ToInt32(item)))
                    alert.alert_Success(Page, "Xóa thành công", "");

                else
                    alert.alert_Error(Page, "Xóa thất bại", "");
            }
        }
        else
        {
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
        }
        loadData();
    }
    protected void btnLuu_ServerClick(object sender, EventArgs e)
    {
        try
        {
            if (Request.Cookies["UserName"] != null)
            {
                if (txtMon.Value == "" || txtLop.Value == "")
                {
                    alert.alert_Error(Page, "Vui lòng nhập thông tin đầy đủ", "");
                }
                else
                {
                    btnCapNhat.Visible = false;
                    var checkuserid = (from u in db.admin_Users where u.username_username == Request.Cookies["UserName"].Value select u).First();
                    tbQuanTri_kehoachdayhoc insert = new tbQuanTri_kehoachdayhoc();
                    insert.kehoachdayhoc_mon = txtMon.Value;
                    insert.kehoachdayhoc_lop = txtLop.Value;
                    insert.kehoachdayhoc_noidung = txtSach.Value;
                    insert.kehoachdayhoc_tuan = Convert.ToInt16(txtTuan.Value);
                    insert.kehoachdayhoc_tiet = Convert.ToInt16(txtTiet.Value);
                    insert.kehoachdayhoc_phanmon = txtPhanMon.Value;
                    insert.kehoachdayhoc_tenbaihoc = txtTenBaiHoc.Value;
                    insert.kehoachdayhoc_noidungcandatduoc = txtNoiDungCanDatDuoc.Value;
                    insert.kehoachdayhoc_ghichu = txtGhiChu.Value;
                    insert.username_id = checkuserid.username_id;
                    insert.namhoc_id = 1;
                    db.tbQuanTri_kehoachdayhocs.InsertOnSubmit(insert);
                    db.SubmitChanges();
                    loadData();
                    txtTiet.Value = (Convert.ToInt16(txtTiet.Value) + 1) + "";
                    txtPhanMon.Value = "";
                    txtTenBaiHoc.Value = "";
                    txtNoiDungCanDatDuoc.Value = "";
                    txtGhiChu.Value = "";
                }
            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void btnCapNhat_ServerClick(object sender, EventArgs e)
    {
        int id = Convert.ToInt32(Session["_id"].ToString());
        tbQuanTri_kehoachdayhoc update = (from u in db.tbQuanTri_kehoachdayhocs where u.kehoachdayhoc_id == id select u).SingleOrDefault();
        update.kehoachdayhoc_mon = txtMon.Value;
        update.kehoachdayhoc_lop = txtLop.Value;
        update.kehoachdayhoc_noidung = txtSach.Value;
        update.kehoachdayhoc_tuan = Convert.ToInt16(txtTuan.Value);
        update.kehoachdayhoc_tiet = Convert.ToInt16(txtTiet.Value);
        update.kehoachdayhoc_phanmon = txtPhanMon.Value;
        update.kehoachdayhoc_tenbaihoc = txtTenBaiHoc.Value;
        update.kehoachdayhoc_noidungcandatduoc = txtNoiDungCanDatDuoc.Value;
        update.kehoachdayhoc_ghichu = txtGhiChu.Value;
        update.namhoc_id = 1;
        db.SubmitChanges();
        alert.alert_Update(Page, "Đã cập nhật thành công", "");
        loadData();
    }
    protected void btnRefresh_Click(object sender, EventArgs e)
    {
        btnLuu.Visible = true;
        btnCapNhat.Visible = false;
        txtPhanMon.Value = "";
        txtTenBaiHoc.Value = "";
        txtNoiDungCanDatDuoc.Value = "";
        txtGhiChu.Value = "";
        txtTuan.Value = "";
        txtTiet.Value = "";
    }
    protected void btnImport_Click(object sender, EventArgs e)
    {
        string excelContentType = "applicateion/vnd.ms-excel";
        string excel2010ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        if (!fileUpload1.HasFile)
        {
            lblThongBao.Text = "Vui lòng chọn file";
        }
        else
        {
            //try
            //{
                if (fileUpload1.PostedFile.ContentType != excelContentType && fileUpload1.PostedFile.ContentType != excel2010ContentType)
                {
                    lblThongBao.Text = "Vui lòng chọn file excel";
                }
                else
                {
                    string path = string.Concat(Server.MapPath("~/Temp/" + fileUpload1.FileName));
                    fileUpload1.SaveAs(path);
                    string excelConnectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 8.0", path);
                    OleDbConnection connection = new OleDbConnection();
                    connection.ConnectionString = excelConnectionString;
                    OleDbCommand command = new OleDbCommand("Select * from [Sheet1$]", connection);
                    connection.Open();
                    DbDataReader dr = command.ExecuteReader();
                    string sqlconnectionString = @"Data Source=112.78.2.94,1433;Initial Catalog=vie65506_VietNhat;Persist Security Info=True;User ID=vie65506_VietNhat;Password=abc123#!";
                    SqlBulkCopy bulkInsert = new SqlBulkCopy(sqlconnectionString);
                    bulkInsert.DestinationTableName = "tbQuanTri_kehoachdayhoc";
                    bulkInsert.WriteToServer(dr);
                    alert.alert_Success(Page, "Hoàn thành", "");
                }
            //}
            //catch (Exception ex)
            //{
            //    lblThongBao.Text = "Lỗi xảy ra trong quá trình import";
            //}
        }
    }
}