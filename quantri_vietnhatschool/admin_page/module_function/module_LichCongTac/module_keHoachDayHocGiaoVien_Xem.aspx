﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_keHoachDayHocGiaoVien_Xem.aspx.cs" Inherits="admin_page_module_function_module_keHoachDayHocGiaoVien_Xem" %>

<%--<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <script>
        function myClick_Lop(id_lop) {
            document.getElementById("<%=txtLop.ClientID%>").value = id_lop;
            document.getElementById("<%=btnXemLop.ClientID%>").click();
        }
        function myClick_Mon(id_mon) {
            document.getElementById("<%=txtMon.ClientID%>").value = id_mon;
            document.getElementById("<%=btnXemMon.ClientID%>").click();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div>
        <b>Bộ phận:</b>
        <asp:DropDownList ID="ddlBoPhan" runat="server" CssClass="" OnSelectedIndexChanged="ddlBoPhan_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
        <b>Giáo viên:</b>
        <asp:DropDownList ID="ddlGiaoVien" runat="server" CssClass="" OnSelectedIndexChanged="ddlGiaoVien_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
        <%-- <button type="button" class="btn btn-primary bd-example-modal-lg" data-toggle="modal" data-target="#exampleModal">
            Xuất file
        </button>--%>
        <div style="display: none">
            <a href="#" id="btnXuatFile" runat="server" onserverclick="btnXuatFile_ServerClick" class="btn btn-primary">Xuất file</a>
        </div>
    </div>
    <br />
    <div>
        <asp:Repeater ID="rpLop" runat="server">
            <ItemTemplate>
                <a href="javascript:void(0)" id="btnXemLop1" class="btn btn-primary" onclick='myClick_Lop("<%#Eval("kehoachdayhoc_lop") %>")'>Lớp <%#Eval("kehoachdayhoc_lop") %></a>
            </ItemTemplate>
        </asp:Repeater>
    </div>
    <br />
    <div>
        <asp:Repeater ID="rpMon" runat="server">
            <ItemTemplate>
                <a href="javascript:voide(0)" id="btnXemMon1" class="btn btn-primary" onclick='myClick_Mon("<%#Eval("kehoachdayhoc_mon") %>")'>Môn <%#Eval("kehoachdayhoc_mon") %> </a>
            </ItemTemplate>
        </asp:Repeater>
    </div>
    <br />
    <asp:Repeater ID="rpDanhsach" runat="server">
        <ItemTemplate>
            <a href="javascript:void(0)" id="btnXem" class="btn btn-primary" onclick="myClick(<%#Eval("username_id") %>)"><%#Eval("username_fullname") %></a>
        </ItemTemplate>
    </asp:Repeater>
    <br />
    <%--<asp:Button ID="btnXemKetQua" runat="server" Text="Xem" CssClass="btn btn-primary" OnClick="btnXemKetQua_Click" />--%>

    <div style="display: none">
        <input type="text" id="txtLop" runat="server" />
        <input type="text" id="txtuserName" runat="server" />
        <input type="text" id="txtMon" runat="server" />
        <a href="#" id="btnXemLop" runat="server" onserverclick="btnXemLop_ServerClick"></a>
        <%--    <a href="#" id="btnXem" runat="server" onserverclick="btnXem_ServerClick"></a>--%>
        <a href="#" id="btnXemMon" runat="server" onserverclick="btnXemMon_ServerClick"></a>
    </div>
    <div>
        <div style="text-align: center">
            <span style="color: blue; font-size: 18px; font-weight: bold">KẾ HOACH DẠY HỌC </span>
            <br />
            <span style="color: blue; font-size: 18px; font-weight: bold">MÔN <%=mon_hoc %> – LỚP <%=lop_hoc %>- NĂM HỌC 2020 – 2021</span>
            <br />
            <span style="color: blue; font-size: 18px; font-weight: bold"><%=noi_dung %></span>
        </div>
        <table class="table table-borderless">
            <thead style="background-color: #32c5d2; color: #fff">
                <tr>
                    <th colspan="1" scope="col">Tuần</th>
                    <th colspan="1" scope="col">tiết</th>
                    <th colspan="1" scope="col">Phân môn</th>
                    <th colspan="1" scope="col">tên bài học</th>
                    <th colspan="1" scope="col">Nội dung kiến thức yêu cầu cần đạt</th>
                    <th colspan="1" scope="col">ghi chú</th>
                </tr>
            </thead>
            <tbody>
                <asp:Repeater ID="rpListDanhGiaGiaoVien" runat="server">
                    <ItemTemplate>
                        <tr style="text-align: center; background-color: white">
                            <td><%#Eval("kehoachdayhoc_tuan") %></td>
                            <td><%#Eval("kehoachdayhoc_tiet") %></td>
                            <td><%#Eval("kehoachdayhoc_phanmon") %></td>
                            <td><%#Eval("kehoachdayhoc_tenbaihoc") %></td>
                             <td><%#Eval("kehoachdayhoc_noidungcandatduoc") %></td>
                            <td><%#Eval("kehoachdayhoc_ghichu") %></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
        </table>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Dữ liệu</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <%--  <div class="modal-body" style="width: 100%;">
                    <div style="width: 100%; height: auto; background-color: #fff">
                        <rsweb:reportviewer id="ReportViewer1" runat="server" sizetoreportcontent="true" width="3500px" height="4000px"></rsweb:reportviewer>
                    </div>
                </div>--%>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

