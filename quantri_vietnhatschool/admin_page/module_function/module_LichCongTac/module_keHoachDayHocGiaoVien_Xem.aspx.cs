﻿//using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_keHoachDayHocGiaoVien_Xem : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    public string mon_hoc, lop_hoc, noi_dung;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            mon_hoc = txtMon.Value;
            lop_hoc = txtLop.Value;
            if (!IsPostBack)
            {
                var listNV = from nv in db.tbBoPhans where nv.hidden == true orderby nv.bophan_position select nv;
                ddlBoPhan.Items.Clear();
                ddlBoPhan.Items.Insert(0, "Chọn bộ phận");
                ddlBoPhan.AppendDataBoundItems = true;
                ddlBoPhan.DataTextField = "bophan_name";
                ddlBoPhan.DataValueField = "bophan_id";
                ddlBoPhan.DataSource = listNV;
                ddlBoPhan.DataBind();
                ddlGiaoVien.Items.Insert(0, "Chọn giáo viên");
            }
        }
        catch (Exception ex)
        {
            alert.alert_Error(Page, "Lỗi key, vui lòng login lại", "");
        }
    }
    private dsKeHoachDayHoc getdsKeHoachDayHoc(string query)
    {
        string conString = ConfigurationManager.ConnectionStrings["admin_VietNhatConnectionString"].ConnectionString;

        SqlCommand cmd = new SqlCommand(query);

        using (SqlConnection con = new SqlConnection(conString))
        {
            using (SqlDataAdapter sda = new SqlDataAdapter())
            {
                cmd.Connection = con;
                sda.SelectCommand = cmd;
                using (dsKeHoachDayHoc dsKeHoachDayHoc = new dsKeHoachDayHoc())
                {
                    sda.Fill(dsKeHoachDayHoc, "tbQuantri_kehoachdayhoc");
                    return dsKeHoachDayHoc;
                }
            }
        }
    }
    protected void ddlBoPhan_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlBoPhan.Text != "Chọn bộ phận")
            {
                var listGV = from u in db.admin_Users where u.bophan_id == Convert.ToInt16(ddlBoPhan.SelectedValue) select u;
                ddlGiaoVien.Items.Clear();
                ddlGiaoVien.Items.Insert(0, "Chọn giáo viên");
                ddlGiaoVien.AppendDataBoundItems = true;
                ddlGiaoVien.DataTextField = "username_fullname";
                ddlGiaoVien.DataValueField = "username_id";
                ddlGiaoVien.DataSource = listGV;
                ddlGiaoVien.DataBind();
                rpLop.Visible = false;
                rpMon.Visible = false;
                rpListDanhGiaGiaoVien.DataSource = null;
                rpListDanhGiaGiaoVien.DataBind();

            }
        }
        catch (Exception ex)
        {
            alert.alert_Error(Page, "Lỗi", "");
        }
    }
    protected void ddlGiaoVien_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlGiaoVien.Text != "Chọn giáo viên")
            {
                var listLop = from l in db.tbQuanTri_kehoachdayhocs
                              where l.username_id == Convert.ToInt32(ddlGiaoVien.SelectedValue)
                              group l by l.kehoachdayhoc_lop into g
                              select new
                              {
                                  kehoachdayhoc_id = g.Key,
                                  g.First().kehoachdayhoc_lop
                              };
                rpLop.DataSource = listLop;
                rpLop.DataBind();
                rpLop.Visible = true;
                rpMon.Visible = false;
                rpListDanhGiaGiaoVien.DataSource = null;
                rpListDanhGiaGiaoVien.DataBind();
            }
        }
        catch (Exception ex)
        {
            alert.alert_Error(Page, "Lỗi", "");
        }
    }
    protected void btnXemLop_ServerClick(object sender, EventArgs e)
    {
        try
        {
            var checkMonTrongLop = from m in db.tbQuanTri_kehoachdayhocs
                                   where m.kehoachdayhoc_lop == txtLop.Value
                                   && m.username_id == Convert.ToInt32(ddlGiaoVien.SelectedValue)
                                   orderby m.kehoachdayhoc_tiet
                                   group m by m.kehoachdayhoc_mon into g
                                   select new
                                   {
                                       kehoachdayhoc_id = g.Key,
                                       g.First().kehoachdayhoc_mon,
                                       g.First().kehoachdayhoc_noidung
                                   };
            if (checkMonTrongLop.Count() > 1)
            {
                rpMon.DataSource = checkMonTrongLop;
                rpMon.DataBind();
                rpMon.Visible = true;
                mon_hoc = checkMonTrongLop.First().kehoachdayhoc_mon;
                noi_dung = checkMonTrongLop.First().kehoachdayhoc_noidung;
            }
            else
            {
                rpListDanhGiaGiaoVien.DataSource = from kh in db.tbQuanTri_kehoachdayhocs
                                                   orderby kh.kehoachdayhoc_tuan,kh.kehoachdayhoc_tiet
                                                   where kh.kehoachdayhoc_lop == txtLop.Value
                                                   && kh.username_id == Convert.ToInt32(ddlGiaoVien.SelectedValue)
                                                   select kh;
                rpListDanhGiaGiaoVien.DataBind();
                rpMon.Visible = false;
                mon_hoc = checkMonTrongLop.First().kehoachdayhoc_mon;
                noi_dung = checkMonTrongLop.First().kehoachdayhoc_noidung;
            }
        }
        catch (Exception ex)
        {
            alert.alert_Error(Page, "Lỗi", "");
        }
    }
    protected void btnXemMon_ServerClick(object sender, EventArgs e)
    {
        try
        {
            var listDanhSach = from kh in db.tbQuanTri_kehoachdayhocs
                               where kh.kehoachdayhoc_lop == txtLop.Value
                               && kh.username_id == Convert.ToInt32(ddlGiaoVien.SelectedValue)
                               && kh.kehoachdayhoc_mon == txtMon.Value
                               orderby kh.kehoachdayhoc_tuan, kh.kehoachdayhoc_tiet
                               select kh;
            rpListDanhGiaGiaoVien.DataSource = listDanhSach;
            rpListDanhGiaGiaoVien.DataBind();
            noi_dung = listDanhSach.First().kehoachdayhoc_noidung;
        }
        catch (Exception ex)
        {
            alert.alert_Error(Page, "Lỗi", "");
        }
    }
    protected void btnXuatFile_ServerClick(object sender, EventArgs e)
    {
        Response.Redirect("bao-cao-ke-hoach-day-hoc");
    }
}