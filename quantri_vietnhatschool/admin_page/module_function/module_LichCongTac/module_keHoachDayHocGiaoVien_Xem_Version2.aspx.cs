﻿//using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_keHoachDayHocGiaoVien_Xem_Version2 : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    public string mon_hoc, lop_hoc, noi_dung;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            mon_hoc = txtMon.Value;
            lop_hoc = txtLop.Value;
            if (!IsPostBack)
            {
                var listNV = from nv in db.tbBoPhans
                             where nv.hidden == true && nv.bophan_position != null
                             orderby nv.bophan_position
                             select nv;
                ddlBoPhan.Items.Clear();
                ddlBoPhan.Items.Insert(0, "Chọn bộ phận");
                ddlBoPhan.AppendDataBoundItems = true;
                ddlBoPhan.DataTextField = "bophan_name";
                ddlBoPhan.DataValueField = "bophan_id";
                ddlBoPhan.DataSource = listNV;
                ddlBoPhan.DataBind();
                ddlGiaoVien.Items.Insert(0, "Chọn giáo viên");
            }
        }
        catch (Exception ex)
        {
            alert.alert_Error(Page, "Lỗi key, vui lòng login lại", "");
        }
    }
    private dsKeHoachDayHoc getdsKeHoachDayHoc(string query)
    {
        string conString = ConfigurationManager.ConnectionStrings["admin_VietNhatConnectionString"].ConnectionString;

        SqlCommand cmd = new SqlCommand(query);

        using (SqlConnection con = new SqlConnection(conString))
        {
            using (SqlDataAdapter sda = new SqlDataAdapter())
            {
                cmd.Connection = con;
                sda.SelectCommand = cmd;
                using (dsKeHoachDayHoc dsKeHoachDayHoc = new dsKeHoachDayHoc())
                {
                    sda.Fill(dsKeHoachDayHoc, "tbQuanTri_KeHoachDayHoc_Version2s");
                    return dsKeHoachDayHoc;
                }
            }
        }
    }
    protected void ddlBoPhan_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlBoPhan.Text != "Chọn bộ phận")
            {
                var listGV = from u in db.admin_Users where u.bophan_id == Convert.ToInt16(ddlBoPhan.SelectedValue) select u;
                ddlGiaoVien.Items.Clear();
                ddlGiaoVien.Items.Insert(0, "Chọn giáo viên");
                ddlGiaoVien.AppendDataBoundItems = true;
                ddlGiaoVien.DataTextField = "username_fullname";
                ddlGiaoVien.DataValueField = "username_id";
                ddlGiaoVien.DataSource = listGV;
                ddlGiaoVien.DataBind();
                rpLop.Visible = false;
                rpMon.Visible = false;
                //rpContent.DataSource = null;
                //rpContent.DataBind();

            }
        }
        catch (Exception ex)
        {
            alert.alert_Error(Page, "Lỗi", "");
        }
    }
    protected void ddlGiaoVien_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlGiaoVien.Text != "Chọn giáo viên")
            {
                var listLop = from l in db.tbQuanTri_KeHoachDayHoc_Version2s
                              where l.username_id == Convert.ToInt32(ddlGiaoVien.SelectedValue)
                              && l.namhoc_id == Convert.ToInt16(ddlNamHoc.SelectedValue)
                              group l by l.kehoachdayhoc_lop into g
                              select new
                              {
                                  kehoachdayhoc_id = g.Key,
                                  g.First().kehoachdayhoc_lop
                              };
                rpLop.DataSource = listLop;
                rpLop.DataBind();
                rpLop.Visible = true;
                rpMon.Visible = false;
                //rpContent.DataSource = null;
                //rpContent.DataBind();
            }
        }
        catch (Exception ex)
        {
            alert.alert_Error(Page, "Lỗi", "");
        }
    }
    protected void btnXemLop_ServerClick(object sender, EventArgs e)
    {
        try
        {
            var checkMonTrongLop = from m in db.tbQuanTri_KeHoachDayHoc_Version2s
                                   where m.kehoachdayhoc_lop == txtLop.Value
                                   && m.username_id == Convert.ToInt32(ddlGiaoVien.SelectedValue)
                                   && m.namhoc_id == Convert.ToInt16(ddlNamHoc.SelectedValue)
                                   orderby m.kehoachdayhoc_id
                                   group m by m.kehoachdayhoc_mon into g
                                   select new
                                   {
                                       kehoachdayhoc_id = g.Key,
                                       g.First().kehoachdayhoc_mon,
                                       g.First().kehoachdayhoc_noidung
                                   };
            if (checkMonTrongLop.Count() > 1)
            {
                rpMon.DataSource = checkMonTrongLop;
                rpMon.DataBind();
                rpMon.Visible = true;
                mon_hoc = checkMonTrongLop.First().kehoachdayhoc_mon;
                noi_dung = checkMonTrongLop.First().kehoachdayhoc_noidung;
            }
            else
            {
                var getData = from kh in db.tbQuanTri_KeHoachDayHoc_Version2s
                              where kh.kehoachdayhoc_lop == txtLop.Value
                              && kh.username_id == Convert.ToInt32(ddlGiaoVien.SelectedValue)
                                && kh.namhoc_id == Convert.ToInt16(ddlNamHoc.SelectedValue)
                              select kh;
                // var getData = (from fm in db.tbQuanTri_FormMaus where fm.formmau_id == Convert.ToInt32(RouteData.Values["id"]) select fm).Single();
                string embed = "<object data=\"{0}\" type=\"application/pdf\" width=\"100%\" height=\"700px\">";
                embed += "If you are unable to view file, you can download from <a href = \"{0}\">here</a>";
                embed += " or download <a target = \"_blank\" href = \"http://get.adobe.com/reader/\">Adobe PDF Reader</a> to view the file.";
                embed += "</object>";
                ltEmbed.Text = string.Format(embed, ResolveUrl(getData.First().kehoachdayhoc_noidung));
                rpMon.Visible = false;
                mon_hoc = checkMonTrongLop.First().kehoachdayhoc_mon;
                //noi_dung = checkMonTrongLop.First().kehoachdayhoc_noidung;
                dvContent.Visible = true;
            }
        }
        catch (Exception ex)
        {
            alert.alert_Error(Page, "Lỗi", "");
        }
    }
    protected void btnXemMon_ServerClick(object sender, EventArgs e)
    {
        try
        {
            var listDanhSach = from kh in db.tbQuanTri_KeHoachDayHoc_Version2s
                               where kh.kehoachdayhoc_lop == txtLop.Value
                               && kh.username_id == Convert.ToInt32(ddlGiaoVien.SelectedValue)
                               && kh.kehoachdayhoc_mon == txtMon.Value
                               && kh.namhoc_id == Convert.ToInt16(ddlNamHoc.SelectedValue)
                               select kh;
            string embed = "<object data=\"{0}\" type=\"application/pdf\" width=\"100%\" height=\"700px\">";
            embed += "If you are unable to view file, you can download from <a href = \"{0}\">here</a>";
            embed += " or download <a target = \"_blank\" href = \"http://get.adobe.com/reader/\">Adobe PDF Reader</a> to view the file.";
            embed += "</object>";
            ltEmbed.Text = string.Format(embed, ResolveUrl(listDanhSach.First().kehoachdayhoc_noidung));
            //rpContent.DataSource = listDanhSach;
            //rpContent.DataBind();
            noi_dung = listDanhSach.First().kehoachdayhoc_noidung;
            dvContent.Visible = true;
        }
        catch (Exception ex)
        {
            alert.alert_Error(Page, "Lỗi", "");
        }
    }
    protected void btnQuayLai_Click(object sender, EventArgs e)
    {
        Response.Redirect("/admin-home");
    }
    protected void btnXuatFile_ServerClick(object sender, EventArgs e)
    {
        Response.Redirect("bao-cao-ke-hoach-day-hoc");
    }

    protected void ddlNamHoc_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlBoPhan.Text = "Chọn bộ phận";
        ddlGiaoVien.Text = "Chọn giáo viên";
        rpLop.Visible = false;
        rpMon.Visible = false;
        dvContent.Visible = false;
    }
}