﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_keHoachDayHocGiaoVien_version2.aspx.cs" Inherits="admin_page_module_function_module_keHoachDayHocGiaoVien_version2" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v17.1" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <style>
        .omt_menu_trai {
            width: 90%;
            background-color: #fff;
            border: 1px solid #32c5d2;
        }

        .omt_header {
            background-color: #32c5d2;
            width: 100%;
            height: 40px;
            padding: 10px;
            color: #fff;
            font-weight: bold;
        }

        .omt_menu_trai p {
            padding-left: 10px;
        }

        .omt_menu_trai input {
            width: 90%;
        }

        .omt_list {
            padding: 5px;
            width: 100%;
            border: 1px solid #32c5d2;
        }
    </style>
    <script>
        function showPreview(input) {
            if (input.files && input.files[0]) {
                var filerdr = new FileReader();
                filerdr.onload = function (e) {
                    $('#hibodywrapper_popupControl_imgPreview').attr('src', e.target.result);
                }
                filerdr.readAsDataURL(input.files[0]);
            }
        }
        function showPreview1(input) {
            if (input.files && input.files[0]) {
                var filerdr = new FileReader();
                filerdr.onload = function (e) {
                    $('#imgPreview1').attr('src', e.target.result);
                }
                filerdr.readAsDataURL(input.files[0]);
            }
        }
        function showImg(img) {
            $('#hibodywrapper_popupControl_imgPreview').attr('src', img);
        }
        function showImg1_1(img) {
            $('#imgPreview1').attr('src', img);
        }
    </script>

    <script>
        function myClick_Lop(id_lop) {
            document.getElementById("<%=txtLopHidden.ClientID%>").value = id_lop;
            document.getElementById("<%=btnXemLop.ClientID%>").click();
        }
        function myClick_Mon(id_mon) {
            document.getElementById("<%=txtMonHidden.ClientID%>").value = id_mon;
            document.getElementById("<%=btnXemMon.ClientID%>").click();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">

    <div class="card card-block">
        <div>
            <asp:Repeater ID="rpLop" runat="server">
                <ItemTemplate>
                    <a href="javascript:voide(0)" id="btnLop" class="btn btn-primary" onclick='myClick_Lop("<%#Eval("kehoachdayhoc_lop") %>")'>Lớp <%#Eval("kehoachdayhoc_lop") %> </a>
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <br />
        <div>
            <asp:Repeater ID="rpMon" runat="server">
                <ItemTemplate>
                    <a href="javascript:voide(0)" id="btnXemMon1" class="btn btn-primary" onclick='myClick_Mon("<%#Eval("kehoachdayhoc_mon") %>")'>Môn <%#Eval("kehoachdayhoc_mon") %> </a>
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <br />
        <div style="display: none">
            <input type="text" id="txtLopHidden" runat="server" />
            <input type="text" id="txtMonHidden" runat="server" />
            <a href="#" id="btnXemLop" runat="server" onserverclick="btnXemLop_ServerClick"></a>
            <a href="#" id="btnXemMon" runat="server" onserverclick="btnXemMon_ServerClick"></a>
        </div>
        <div style="float: right">
            <a href="/admin-home" class="btn btn-primary">Quay lại</a>
        </div>
        <div style="text-align: center">
            <b>KẾ HOACH DẠY HỌC NĂM HỌC 
                <asp:DropDownList ID="ddlNam" runat="server" OnSelectedIndexChanged ="ddlNam_SelectedIndexChanged" AutoPostBack="true">
                    <asp:ListItem Value="3">2021-2022</asp:ListItem>
                    <asp:ListItem Value="1">2020-2021</asp:ListItem>
                </asp:DropDownList></b>
            <br />
            <br />
            <b>MÔN:
                <input type="text" id="txtMon" runat="server" autocomplete="off" />
                LỚP:
                <input type="text" id="txtLop" runat="server" autocomplete="off" />
            </b>
            <br />
            <br />
            <%-- <b>Nội dung tóm tắt:
                <input type="text" id="txtTomTat" runat="server" style="width: 70%" autocomplete="off" /></b>
            <br />--%>
        </div>
        <div style="text-align: center">
            <div class="col-12">
                <div class="col-12 form-group">
                    <label class="col-12 form-control-label"></label>
                </div>
                <div class="col-12 form-group">
                    <div class="col-12">

                        <div class="colum-5 form-group">
                            <label class="form-control-label">Chọn file pdf :</label>
                            <div id="up1" class="">
                                <asp:FileUpload CssClass="hidden-xs-up" ID="FileUpload1" runat="server" onchange="showPreview1(this)" />
                                <button type="button" class="btn-chang" onclick="clickavatar1()">
                                    <img id="imgPreview1" src="/admin_images/icon_PDF.png" style="max-width: 100%; height: 200px" />
                                </button>
                            </div>
                        </div>
                        <%--<dx:ASPxHtmlEditor ID="edtnoidung" ClientInstanceName="edtnoidung" runat="server" Width="100%" Height="750px" Border-BorderStyle="Solid" Border-BorderWidth="1px" Border-BorderColor="#dddddd">
                            <SettingsHtmlEditing EnablePasteOptions="true" />
                            <Settings AllowHtmlView="true" AllowContextMenu="Default" />
                            <settingsimageupload uploadfolder="~/editorimages"></settingsimageupload>
                            <Toolbars>
                                <dx:HtmlEditorToolbar>
                                    <Items>
                                        <dx:ToolbarCustomCssEdit Width="120px">
                                            <Items>
                                                <dx:ToolbarCustomCssListEditItem TagName="" Text="Clear Style" CssClass="" />
                                                <dx:ToolbarCustomCssListEditItem TagName="H1" Text="Title" CssClass="CommonTitle">
                                                    <PreviewStyle CssClass="CommonTitlePreview" />
                                                </dx:ToolbarCustomCssListEditItem>
                                                <dx:ToolbarCustomCssListEditItem TagName="H3" Text="Header1" CssClass="CommonHeader1">
                                                    <PreviewStyle CssClass="CommonHeader1Preview" />
                                                </dx:ToolbarCustomCssListEditItem>
                                                <dx:ToolbarCustomCssListEditItem TagName="H4" Text="Header2" CssClass="CommonHeader2">
                                                    <PreviewStyle CssClass="CommonHeader2Preview" />
                                                </dx:ToolbarCustomCssListEditItem>
                                                <dx:ToolbarCustomCssListEditItem TagName="Div" Text="Content" CssClass="CommonContent">
                                                    <PreviewStyle CssClass="CommonContentPreview" />
                                                </dx:ToolbarCustomCssListEditItem>
                                                <dx:ToolbarCustomCssListEditItem TagName="Strong" Text="Features" CssClass="CommonFeatures">
                                                    <PreviewStyle CssClass="CommonFeaturesPreview" />
                                                </dx:ToolbarCustomCssListEditItem>
                                                <dx:ToolbarCustomCssListEditItem TagName="Div" Text="Footer" CssClass="CommonFooter">
                                                    <PreviewStyle CssClass="CommonFooterPreview" />
                                                </dx:ToolbarCustomCssListEditItem>
                                                <dx:ToolbarCustomCssListEditItem TagName="" Text="Link" CssClass="Link">
                                                    <PreviewStyle CssClass="LinkPreview" />
                                                </dx:ToolbarCustomCssListEditItem>
                                                <dx:ToolbarCustomCssListEditItem TagName="EM" Text="ImageTitle" CssClass="ImageTitle">
                                                    <PreviewStyle CssClass="ImageTitlePreview" />
                                                </dx:ToolbarCustomCssListEditItem>
                                                <dx:ToolbarCustomCssListEditItem TagName="" Text="ImageMargin" CssClass="ImageMargin">
                                                    <PreviewStyle CssClass="ImageMarginPreview" />
                                                </dx:ToolbarCustomCssListEditItem>
                                            </Items>
                                        </dx:ToolbarCustomCssEdit>
                                        <dx:ToolbarParagraphFormattingEdit>
                                            <Items>
                                                <dx:ToolbarListEditItem Text="Normal" Value="p" />
                                                <dx:ToolbarListEditItem Text="Heading  1" Value="h1" />
                                                <dx:ToolbarListEditItem Text="Heading  2" Value="h2" />
                                                <dx:ToolbarListEditItem Text="Heading  3" Value="h3" />
                                                <dx:ToolbarListEditItem Text="Heading  4" Value="h4" />
                                                <dx:ToolbarListEditItem Text="Heading  5" Value="h5" />
                                                <dx:ToolbarListEditItem Text="Heading  6" Value="h6" />
                                                <dx:ToolbarListEditItem Text="Address" Value="address" />
                                                <dx:ToolbarListEditItem Text="Normal (DIV)" Value="div" />
                                            </Items>
                                        </dx:ToolbarParagraphFormattingEdit>
                                        <dx:ToolbarFontNameEdit>
                                            <Items>
                                                <dx:ToolbarListEditItem Value="Times New Roman" Text="Times New Roman"></dx:ToolbarListEditItem>
                                                <dx:ToolbarListEditItem Value="Tahoma" Text="Tahoma"></dx:ToolbarListEditItem>
                                                <dx:ToolbarListEditItem Value="Verdana" Text="Verdana"></dx:ToolbarListEditItem>
                                                <dx:ToolbarListEditItem Value="Arial" Text="Arial"></dx:ToolbarListEditItem>
                                                <dx:ToolbarListEditItem Value="MS Sans Serif" Text="MS Sans Serif"></dx:ToolbarListEditItem>
                                                <dx:ToolbarListEditItem Value="Courier" Text="Courier"></dx:ToolbarListEditItem>
                                                <dx:ToolbarListEditItem Value="bodoni MT" Text="bodoni MT"></dx:ToolbarListEditItem>
                                            </Items>
                                        </dx:ToolbarFontNameEdit>
                                        <dx:ToolbarFontSizeEdit>
                                            <Items>
                                                <dx:ToolbarListEditItem Value="1" Text="1 (8pt)"></dx:ToolbarListEditItem>
                                                <dx:ToolbarListEditItem Value="2" Text="2 (10pt)"></dx:ToolbarListEditItem>
                                                <dx:ToolbarListEditItem Value="3" Text="3 (12pt)"></dx:ToolbarListEditItem>
                                                <dx:ToolbarListEditItem Value="4" Text="4 (14pt)"></dx:ToolbarListEditItem>
                                                <dx:ToolbarListEditItem Value="5" Text="5 (18pt)"></dx:ToolbarListEditItem>
                                                <dx:ToolbarListEditItem Value="6" Text="6 (24pt)"></dx:ToolbarListEditItem>
                                                <dx:ToolbarListEditItem Value="7" Text="7 (36pt)"></dx:ToolbarListEditItem>
                                            </Items>
                                        </dx:ToolbarFontSizeEdit>
                                        <dx:ToolbarBoldButton BeginGroup="True" />
                                        <dx:ToolbarItalicButton />
                                        <dx:ToolbarUnderlineButton />
                                        <dx:ToolbarStrikethroughButton />
                                        <dx:ToolbarJustifyLeftButton BeginGroup="True" />
                                        <dx:ToolbarJustifyCenterButton />
                                        <dx:ToolbarJustifyRightButton />
                                        <dx:ToolbarJustifyFullButton />
                                        <dx:ToolbarBackColorButton BeginGroup="True" />
                                        <dx:ToolbarFontColorButton />
                                    </Items>
                                </dx:HtmlEditorToolbar>
                            </Toolbars>
                        </dx:ASPxHtmlEditor>--%>
                        <div class="card card-block">

                            <div class="div_content col-12">
                                <asp:Literal ID="ltEmbed" runat="server" />
                            </div>
                        </div>
                    </div>
                    <br />
                    <br />
                    <asp:Button ID="btnLuuvathemmoi" runat="server" CssClass="btn btn-primary" OnClick="btnLuuvathemmoi_Click" Text="Lưu và thêm mới" />
                    <asp:Button ID="btnLuu" runat="server" CssClass="btn btn-primary" OnClick="btnLuu_ServerClick" Text="Lưu" />
                    <asp:Button ID="btnXoa" runat="server" CssClass="btn btn-primary" OnClick="btnXoa_Click" Text="Xóa" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
    <script type="text/javascript">
        function clickavatar1() {
            $("#up1 input[type=file]").click();
        }
    </script>
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

