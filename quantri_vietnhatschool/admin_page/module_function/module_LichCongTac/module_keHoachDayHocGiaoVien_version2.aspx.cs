﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_keHoachDayHocGiaoVien_version2 : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    public string image;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            //edtnoidung.Toolbars.Add(HtmlEditorToolbar.CreateStandardToolbar1());
            if (Request.Cookies["UserName"] != null)
            {
                var checkuserid = (from u in db.admin_Users where u.username_username == Request.Cookies["UserName"].Value select u).First();
                var checkuserkehoachdayhoc = from kh in db.tbQuanTri_KeHoachDayHoc_Version2s
                                             where kh.username_id == checkuserid.username_id 
                                             && kh.namhoc_id == Convert.ToInt16(ddlNam.SelectedValue)
                                             group kh by kh.kehoachdayhoc_lop into g
                                             select new
                                             {
                                                 kehoachdayhoc_id = g.Key,
                                                 kehoachdayhoc_lop = g.First().kehoachdayhoc_lop
                                             };
                if (checkuserkehoachdayhoc.Count() > 0)
                {
                    rpLop.DataSource = checkuserkehoachdayhoc;
                    rpLop.DataBind();
                    rpLop.Visible = true;
                    rpMon.Visible = false;
                }
                else
                {
                    rpLop.Visible = false;
                    rpMon.Visible = false;
                }
            }
            //loadData();
        }

    }
    private void loadData()
    {
        try
        {
            if (Request.Cookies["UserName"] != null)
            {
                var checkuserid = (from u in db.admin_Users where u.username_username == Request.Cookies["UserName"].Value select u).First();
                var getData = from tb in db.tbQuanTri_KeHoachDayHoc_Version2s
                              where tb.username_id == checkuserid.username_id 
                              && tb.namhoc_id == Convert.ToInt16(ddlNam.SelectedValue)
                              orderby tb.kehoachdayhoc_id descending
                              select tb;
                if (getData.Count() > 0)
                {
                    //edtnoidung.Html = getData.First().kehoachdayhoc_noidung;
                    string embed = "<object data=\"{0}\" type=\"application/pdf\" width=\"100%\" height=\"700px\">";
                    embed += "If you are unable to view file, you can download from <a href = \"{0}\">here</a>";
                    embed += " or download <a target = \"_blank\" href = \"http://get.adobe.com/reader/\">Adobe PDF Reader</a> to view the file.";
                    embed += "</object>";
                    ltEmbed.Text = string.Format(embed, ResolveUrl(getData.First().kehoachdayhoc_noidung));
                    txtLop.Value = getData.First().kehoachdayhoc_lop;
                    txtMon.Value = getData.First().kehoachdayhoc_mon;
                    //txtTomTat.Value = getData.First().kehoachdayhoc_name;
                }
            }
        }
        catch (Exception ex)
        {
            alert.alert_Error(Page, "Lỗi", "");
        }
    }
    protected void btnLuu_ServerClick(object sender, EventArgs e)
    {
        try
        {
            if (Request.Cookies["UserName"] != null)
            {
                if (Page.IsValid && FileUpload1.HasFile)
                {
                    String folderUser = Server.MapPath("~/uploadimages/file-kehoachgiangday/");
                    if (!Directory.Exists(folderUser))
                    {
                        Directory.CreateDirectory(folderUser);
                    }
                    //string filename;
                    string ulr = "/uploadimages/file-kehoachgiangday/";
                    HttpFileCollection hfc = Request.Files;
                    //string filename = Path.GetRandomFileName() + Path.GetExtension(FileUpload1.FileName);
                    //string fileName_save = Path.Combine(Server.MapPath("~/uploadimages/file-mau"), filename);
                    string filename = Path.GetFileName(FileUpload1.FileName);
                    string fileName_save = Path.Combine(Server.MapPath("~/uploadimages/file-kehoachgiangday/"), filename);
                    FileUpload1.SaveAs(fileName_save);
                    image = ulr + filename;
                }
                var checkuserid = (from u in db.admin_Users where u.username_username == Request.Cookies["UserName"].Value select u).First();
                tbQuanTri_KeHoachDayHoc_Version2 getData = (from tb in db.tbQuanTri_KeHoachDayHoc_Version2s
                              where tb.username_id == checkuserid.username_id
                              && tb.namhoc_id == Convert.ToInt16(ddlNam.SelectedValue)
                              && tb.kehoachdayhoc_lop == txtLop.Value
                              && tb.kehoachdayhoc_mon == txtMon.Value
                              select tb).SingleOrDefault();
                if (getData!=null)
                {
                    getData.kehoachdayhoc_lop = txtLop.Value;
                    getData.kehoachdayhoc_mon = txtMon.Value;
                    //getData.kehoachdayhoc_name = txtTomTat.Value;
                    getData.namhoc_id = Convert.ToInt16(ddlNam.SelectedValue);
                    getData.kehoachdayhoc_noidung = image;
                    getData.username_id = checkuserid.username_id;
                    db.SubmitChanges();
                    alert.alert_Success(Page, "Cập nhật thành công", "");
                    var checkuserkehoachdayhoc = from kh in db.tbQuanTri_KeHoachDayHoc_Version2s
                                                 where kh.username_id == checkuserid.username_id
                                                 && kh.namhoc_id == Convert.ToInt16(ddlNam.SelectedValue)
                                                 group kh by kh.kehoachdayhoc_lop into g
                                                 select new
                                                 {
                                                     kehoachdayhoc_id = g.Key,
                                                     kehoachdayhoc_lop = g.First().kehoachdayhoc_lop
                                                 };
                    if (checkuserkehoachdayhoc.Count() > 0)
                    {
                        rpLop.DataSource = checkuserkehoachdayhoc;
                        rpLop.DataBind();
                    }
                }
                else
                {
                    tbQuanTri_KeHoachDayHoc_Version2 insert = new tbQuanTri_KeHoachDayHoc_Version2();
                    insert.kehoachdayhoc_lop = txtLop.Value;
                    insert.kehoachdayhoc_mon = txtMon.Value;
                    //insert.kehoachdayhoc_name = txtTomTat.Value;
                    insert.namhoc_id = Convert.ToInt16(ddlNam.SelectedValue);
                    insert.kehoachdayhoc_noidung = image;
                    insert.username_id = checkuserid.username_id;
                    db.tbQuanTri_KeHoachDayHoc_Version2s.InsertOnSubmit(insert);
                    db.SubmitChanges();
                    alert.alert_Success(Page, "Hoàn thành", "");
                    var checkuserkehoachdayhoc = from kh in db.tbQuanTri_KeHoachDayHoc_Version2s
                                                 where kh.username_id == checkuserid.username_id
                                                 && kh.namhoc_id == Convert.ToInt16(ddlNam.SelectedValue)
                                                 group kh by kh.kehoachdayhoc_lop into g
                                                 select new
                                                 {
                                                     kehoachdayhoc_id = g.Key,
                                                     kehoachdayhoc_lop = g.First().kehoachdayhoc_lop
                                                 };
                    if (checkuserkehoachdayhoc.Count() > 0)
                    {
                        rpLop.DataSource = checkuserkehoachdayhoc;
                        rpLop.DataBind();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            alert.alert_Error(Page, "Lỗi liên hệ IT", "");
        }
    }
    protected void btnXemLop_ServerClick(object sender, EventArgs e)
    {
        try
        {
            string embed = "<object data=\"{0}\" type=\"application/pdf\" width=\"100%\" height=\"700px\">";
            embed += "If you are unable to view file, you can download from <a href = \"{0}\">here</a>";
            embed += " or download <a target = \"_blank\" href = \"http://get.adobe.com/reader/\">Adobe PDF Reader</a> to view the file.";
            embed += "</object>";
            var checkuserid = (from u in db.admin_Users where u.username_username == Request.Cookies["UserName"].Value select u).First();
            var checkMonTrongLop = from m in db.tbQuanTri_KeHoachDayHoc_Version2s
                                   where m.kehoachdayhoc_lop == txtLopHidden.Value
                                   && m.username_id == checkuserid.username_id
                                   && m.namhoc_id == Convert.ToInt16(ddlNam.SelectedValue)
                                   orderby m.kehoachdayhoc_id descending
                                   group m by m.kehoachdayhoc_mon into g
                                   select new
                                   {
                                       kehoachdayhoc_id = g.Key,
                                       g.First().kehoachdayhoc_mon,
                                       g.First().kehoachdayhoc_noidung,
                                       g.First().kehoachdayhoc_name,
                                       g.First().kehoachdayhoc_lop
                                   };
            if (checkMonTrongLop.Count() > 1)
            {
                rpMon.DataSource = checkMonTrongLop;
                rpMon.DataBind();
                rpMon.Visible = true;

            }
            else
            {
                var list = from kh in db.tbQuanTri_KeHoachDayHoc_Version2s
                           orderby kh.kehoachdayhoc_id descending
                           where kh.kehoachdayhoc_lop == txtLop.Value
                           && kh.username_id == checkuserid.username_id
                           select kh;
                //txtTomTat.Value = checkMonTrongLop.First().kehoachdayhoc_name;
                txtLop.Value = checkMonTrongLop.First().kehoachdayhoc_lop;
                txtMon.Value = checkMonTrongLop.First().kehoachdayhoc_mon;
                ltEmbed.Text = string.Format(embed, ResolveUrl(checkMonTrongLop.First().kehoachdayhoc_noidung));
                //edtnoidung.Html = checkMonTrongLop.First().kehoachdayhoc_noidung;
                rpMon.Visible = false;
                ltEmbed.Visible = true;
            }
        }
        catch (Exception ex)
        {
            alert.alert_Error(Page, "Lỗi", "");
        }
    }
    protected void btnXemMon_ServerClick(object sender, EventArgs e)
    {
        try
        {
            string embed = "<object data=\"{0}\" type=\"application/pdf\" width=\"100%\" height=\"700px\">";
            embed += "If you are unable to view file, you can download from <a href = \"{0}\">here</a>";
            embed += " or download <a target = \"_blank\" href = \"http://get.adobe.com/reader/\">Adobe PDF Reader</a> to view the file.";
            embed += "</object>";
            var checkuserid = (from u in db.admin_Users where u.username_username == Request.Cookies["UserName"].Value select u).First();
            var listDanhSach = from kh in db.tbQuanTri_KeHoachDayHoc_Version2s
                               where kh.kehoachdayhoc_lop == txtLopHidden.Value
                               && kh.username_id == checkuserid.username_id
                               && kh.kehoachdayhoc_mon == txtMonHidden.Value
                               orderby kh.kehoachdayhoc_id descending
                               select kh;
           // txtTomTat.Value = listDanhSach.First().kehoachdayhoc_name;
            txtLop.Value = listDanhSach.First().kehoachdayhoc_lop;
            txtMon.Value = listDanhSach.First().kehoachdayhoc_mon;
            ltEmbed.Text = string.Format(embed, ResolveUrl(listDanhSach.First().kehoachdayhoc_noidung));
            //edtnoidung.Html = listDanhSach.First().kehoachdayhoc_noidung;
            ltEmbed.Visible = true;
        }
        catch (Exception ex)
        {
            alert.alert_Error(Page, "Lỗi", "");
        }
    }
    protected void btnXoa_Click(object sender, EventArgs e)
    {
        try
        {
            var checkuserid = (from u in db.admin_Users where u.username_username == Request.Cookies["UserName"].Value select u).First();
            tbQuanTri_KeHoachDayHoc_Version2 del = (from kh in db.tbQuanTri_KeHoachDayHoc_Version2s
                                                    where kh.kehoachdayhoc_lop == txtLop.Value
                                                    && kh.username_id == checkuserid.username_id
                                                    && kh.kehoachdayhoc_mon == txtMon.Value
                                                    && kh.namhoc_id == Convert.ToInt32(ddlNam.SelectedValue)
                                                    orderby kh.kehoachdayhoc_id descending
                                                    select kh).First();
            db.tbQuanTri_KeHoachDayHoc_Version2s.DeleteOnSubmit(del);
            db.SubmitChanges();
            alert.alert_Success(Page, "Đã xóa thành công", "");
            var checkuserkehoachdayhoc = from kh in db.tbQuanTri_KeHoachDayHoc_Version2s
                                         where kh.username_id == checkuserid.username_id && kh.namhoc_id == Convert.ToInt16(ddlNam.SelectedValue)
                                         group kh by kh.kehoachdayhoc_lop into g
                                         select new
                                         {
                                             kehoachdayhoc_id = g.Key,
                                             kehoachdayhoc_lop = g.First().kehoachdayhoc_lop
                                         };
            if (checkuserkehoachdayhoc.Count() > 0)
            {
                rpLop.DataSource = checkuserkehoachdayhoc;
                rpLop.DataBind();
                rpLop.Visible = true;
                rpMon.Visible = false;
            }
            else
            {
                rpLop.Visible = false;
                rpMon.Visible = false;
            }
        }
        catch (Exception ex)
        {
            alert.alert_Error(Page, "Lỗi", "");
        }
    }
    protected void btnLuuvathemmoi_Click(object sender, EventArgs e)
    {
        try
        {
            if (Request.Cookies["UserName"] != null)
            {
                if (Page.IsValid && FileUpload1.HasFile)
                {
                    String folderUser = Server.MapPath("~/uploadimages/file-kehoachgiangday/");
                    if (!Directory.Exists(folderUser))
                    {
                        Directory.CreateDirectory(folderUser);
                    }
                    //string filename;
                    string ulr = "/uploadimages/file-kehoachgiangday/";
                    HttpFileCollection hfc = Request.Files;
                    //string filename = Path.GetRandomFileName() + Path.GetExtension(FileUpload1.FileName);
                    //string fileName_save = Path.Combine(Server.MapPath("~/uploadimages/file-mau"), filename);
                    string filename = Path.GetFileName(FileUpload1.FileName);
                    string fileName_save = Path.Combine(Server.MapPath("~/uploadimages/file-kehoachgiangday/"), filename);
                    FileUpload1.SaveAs(fileName_save);
                    image = ulr + filename;
                }
                var checkuserid = (from u in db.admin_Users where u.username_username == Request.Cookies["UserName"].Value select u).First();
                tbQuanTri_KeHoachDayHoc_Version2 getData = (from tb in db.tbQuanTri_KeHoachDayHoc_Version2s
                                                            where tb.username_id == checkuserid.username_id
                                                            && tb.namhoc_id == Convert.ToInt16(ddlNam.SelectedValue)
                                                            && tb.kehoachdayhoc_lop == txtLop.Value
                                                            && tb.kehoachdayhoc_mon == txtMon.Value
                                                            select tb).SingleOrDefault();
                if (getData != null)
                {
                    getData.kehoachdayhoc_lop = txtLop.Value;
                    getData.kehoachdayhoc_mon = txtMon.Value;
                    //getData.kehoachdayhoc_name = txtTomTat.Value;
                    getData.namhoc_id = Convert.ToInt16(ddlNam.SelectedValue);
                   getData.kehoachdayhoc_noidung = image;
                    getData.username_id = checkuserid.username_id;
                    db.SubmitChanges();
                    alert.alert_Success(Page, "Cập nhật thành công", "");
                    var checkuserkehoachdayhoc = from kh in db.tbQuanTri_KeHoachDayHoc_Version2s
                                                 where kh.username_id == checkuserid.username_id && kh.namhoc_id == Convert.ToInt16(ddlNam.SelectedValue)
                                                 group kh by kh.kehoachdayhoc_lop into g
                                                 select new
                                                 {
                                                     kehoachdayhoc_id = g.Key,
                                                     kehoachdayhoc_lop = g.First().kehoachdayhoc_lop
                                                 };
                    if (checkuserkehoachdayhoc.Count() > 0)
                    {
                        rpLop.DataSource = checkuserkehoachdayhoc;
                        rpLop.DataBind();
                    }
                }
                else
                {
                    tbQuanTri_KeHoachDayHoc_Version2 insert = new tbQuanTri_KeHoachDayHoc_Version2();
                    insert.kehoachdayhoc_lop = txtLop.Value;
                    insert.kehoachdayhoc_mon = txtMon.Value;
                    //insert.kehoachdayhoc_name = txtTomTat.Value;
                    insert.namhoc_id = Convert.ToInt16(ddlNam.SelectedValue);
                    insert.kehoachdayhoc_noidung = image;
                    insert.username_id = checkuserid.username_id;
                    db.tbQuanTri_KeHoachDayHoc_Version2s.InsertOnSubmit(insert);
                    db.SubmitChanges();
                    alert.alert_Success(Page, "Hoàn thành", "");
                   
                    var checkuserkehoachdayhoc = from kh in db.tbQuanTri_KeHoachDayHoc_Version2s
                                                 where kh.username_id == checkuserid.username_id && kh.namhoc_id == Convert.ToInt16(ddlNam.SelectedValue)
                                                 group kh by kh.kehoachdayhoc_lop into g
                                                 select new
                                                 {
                                                     kehoachdayhoc_id = g.Key,
                                                     kehoachdayhoc_lop = g.First().kehoachdayhoc_lop
                                                 };
                    if (checkuserkehoachdayhoc.Count() > 0)
                    {
                        rpLop.DataSource = checkuserkehoachdayhoc;
                        rpLop.DataBind();
                    }
                }
                txtLop.Value = "";
                txtMon.Value = "";
                //txtTomTat.Value = "";
                //edtnoidung.Html = "";
            }
        }
        catch (Exception ex)
        {
            alert.alert_Error(Page, "Lỗi liên hệ IT", "");
        }
    }

    protected void ddlNam_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtMon.Value = "";
        txtLop.Value = "";
        ltEmbed.Visible = false;
        rpMon.Visible = false;
        var checkuserid = (from u in db.admin_Users where u.username_username == Request.Cookies["UserName"].Value select u).First();
        var checkuserkehoachdayhoc = from kh in db.tbQuanTri_KeHoachDayHoc_Version2s
                                     where kh.username_id == checkuserid.username_id
                                     && kh.namhoc_id == Convert.ToInt16(ddlNam.SelectedValue)
                                     group kh by kh.kehoachdayhoc_lop into g
                                     select new
                                     {
                                         kehoachdayhoc_id = g.Key,
                                         kehoachdayhoc_lop = g.First().kehoachdayhoc_lop
                                     };
        if (checkuserkehoachdayhoc.Count() > 0)
        {
            rpLop.DataSource = checkuserkehoachdayhoc;
            rpLop.DataBind();
            rpLop.Visible = true;
            rpMon.Visible = false;
        }
        else
        {
            rpLop.Visible = false;
            rpMon.Visible = false;
        }

    }
}