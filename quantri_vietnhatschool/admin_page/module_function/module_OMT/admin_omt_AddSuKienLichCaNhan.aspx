﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="admin_omt_AddSuKienLichCaNhan.aspx.cs" Inherits="admin_page_module_function_module_OMT_omt_module_Chung_admin_omt_AddSuKienLichCaNhan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <script>
        function myAccount() {
            document.getElementById('block_content').style.display = "block";
            document.getElementById('block_content1').style.display = "none";
            //document.getElementById("block_content").visible = true;
        }
        function myTeacher() {
            document.getElementById('block_content1').style.display = "block";
            document.getElementById('block_content').style.display = "none";
            //document.getElementById("block_content").visible = true;
        }
        function checkeds(checkbox) {

            if (checkbox.checked) {
                document.getElementById("send_at_select").style.display = "block";
            }
            else {
                document.getElementById("send_at_select").style.display = "none";
            }
        }
        function checkedss(checkbox) {

            if (checkbox.checked) {
                document.getElementById("prompt_before_minute").style.display = "block";
            }
            else {
                document.getElementById("prompt_before_minute").style.display = "none";
            }
        }

        function showNow() {
            document.getElementById('block_content4').style.display = "none";
        }
        function show() {
            document.getElementById('block-content4').style.display = "block";
        }
        function checkedsss(checkbox) {

            if (checkbox.checked) {
                document.getElementById("open_register").style.display = "block";
                document.getElementById("block-content5").style.display = "block";
            }
            else {
                document.getElementById("open_register").style.display = "none";
                document.getElementById("block-content5").style.display = "none";
            }
        }
    </script>




    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous" />
    <script src="https://cdn.ckeditor.com/4.5.11/standard/ckeditor.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <link rel="stylesheet" href="/jquery.datetimepicker.min.css" />
    <script src="/jquery.js"></script>
    <script src="/jquery.datetimepicker.full.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="main-omt">
        <div class="omt-header">
            <i class="fa fa-list omt__icon" aria-hidden="true"></i>
            <h4 class="header-title">Tạo Sự Kiện</h4>
        </div>
        <div class="portlet-body flip-scroll">
            <div class="fixed-table-container">

                <div class="box box-success">
                    <div class="box-body">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="form-group">
                                        <div class="row">
                                            <label for="title" class="col-md-2 control-label text-right required">Tên sự kiện </label>
                                            <div class="col-md-10">
                                                <input id="title_event" class="form-control" required="" placeholder="Tiêu đề sự kiện" autocomplete="off" name="title_event" type="text" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <label for="time_displayed" class="control-label col-md-2 text-right required">Sự kiện bắt đầu</label>
                                            <div class="col-xs-5">
                                                <input class="form-control daypicker form-control-inline " type="date" />
                                            </div>
                                            <div class="col-xs-5">
                                                <div class="col-10">
                                                    <input class="form-control timepicker" type="time" value="13:45:00" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <label for="time_displayed" class="control-label col-md-2 text-right required">Sự kiện kết thúc</label>
                                            <div class="col-xs-5">
                                                <input class="form-control daypicker form-control-inline " type="date" />
                                            </div>
                                            <div class="col-xs-5">
                                                <div class="col-10">
                                                    <input class="form-control timepicker" type="time" value="13:45:00" />
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="col-md-2 control-label"></label>
                                            <div class="col-lg-2" style="width: 200pt; padding-right: 0;">
                                                <div class="mt-checkbox-list" style="padding: 0;">
                                                    <label class="mt-checkbox mt-checkbox-outline" style="margin-top: 8px; margin-bottom: 0;">
                                                        <input id="is_allow_comment_event" name="is_allow_comment_event" type="checkbox" value="1" />
                                                        Cho phép phản hồi
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <label for="title" class="col-md-2 control-label range text-right required">Phạm vi </label>
                                            <div class="col-md-5">
                                                <ul class="nav nav-tabs range">
                                                    <li class="nav-item dropdown">
                                                        <a class="nav-link dropdown-toggle title" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Lớp Chủ Nhiệm</a>
                                                        <div class="dropdown-menu range">
                                                            <a class="teacher" href="javasrcipt:voide(0)" onclick="myTeacher()">Lớp Chủ Nhiệm</a>
                                                            <a class="account" href="javasrcipt:voide(0)" onclick="myAccount()">tài khoản</a>
                                                        </div>
                                                    </li>
                                                </ul>

                                            </div>


                                            <div class="col-md-5">

                                                <form>
                                                    <div class="form-row align-items-center account" id="block_content1">
                                                        <div class="col-auto">

                                                            <select class="custom-select class mr-sm-2" style="width: 100%" id="inlineFormCustomSelect">
                                                                <option selected="selected">lớp...</option>
                                                                <option value="1">1B1</option>
                                                                <option value="2">2B1</option>
                                                                <option value="3">3B1</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="col-md-7">
                                                <form>
                                                    <div class="form-row align-items-center account" id="block_content">
                                                        <div class="col-auto ">
                                                            <select class="custom-select acount mr-sm-2" style="width: 100%" id="inlineFormCustomSelect2">
                                                                <option selected="selected">Tài Khoản...</option>
                                                                <option value="3292">Nguyễn Tùng Anh [OMT-011 - 1B1 - 16/02/2008]</option>
                                                                <option value="3294">Phạm Hoàng Anh [OMT-012 - 1B1 - 05/11/2008]</option>
                                                                <option value="3296">Phạm Thị Tuyết Anh [OMT-013 - 1B1 - 19/09/2008]</option>
                                                                <option value="3298">Phạm Trâm Anh [OMT-014 - 1B1 - 16/07/2008]</option>
                                                                <option value="3300">Trần Đức Anh [OMT-015 - 1B1 - 12/12/2008]</option>
                                                                <option value="3302">Lê Minh Bảo Anh [OMT-016 - 1B1 - 25/05/2008]</option>
                                                                <option value="3304">Lại Viết Hoàng Anh [OMT-017 - 1B1 - 18/04/2008]</option>
                                                                <option value="3306">Lê Trang Anh [OMT-018 - 1B1 - 27/10/2008]</option>
                                                                <option value="3308">Đào Phương Anh [OMT-019 - 1B1 - 10/12/2008]</option>
                                                                <option value="3310">Lê Sơn Bách [OMT-020 - 1B1 - 07/01/2008]</option>
                                                                <option value="3291">Nguyễn Tuấn Anh [FT011]</option>
                                                                <option value="3297">Phạm Văn Hiệu [FT014]</option>
                                                                <option value="3299">Trần Đức Thành [FT015]</option>
                                                                <option value="3307">Đào Văn Sang [FT019]</option>
                                                                <option value="3309">Lê Ngọc Tân [FT020]</option>
                                                                <option value="3293">Hoàng Thị Thanh Mai [MT009]</option>
                                                                <option value="3295">Quách Thị Ánh Tuyết [MT010]</option>
                                                                <option value="3301">Lê Thị Thu Trang [MT011]</option>
                                                                <option value="3303">Nguyễn Thị Thu [MT012]</option>
                                                                <option value="3305">Nguyễn Thùy Giang [MT013]</option>
                                                            </select>
                                                        </div>


                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group object-type" style="margin-bottom: 0;" id="block_content1">
                                        <div class="row">
                                            <label for="subject" class="col-md-2 control-label range text-right required">Đối tượng</label>
                                            <div class="col-md-9">
                                                <div class="mt-checkbox-inline object">
                                                    <label class="mt-checkbox mt-checkbox-outline">
                                                        <input id="inlineCheckbox3" name="" type="checkbox" value="2" />
                                                        Học sinh
            
                                                    </label>
                                                    <label class="mt-checkbox mt-checkbox-outline">
                                                        <input id="inlineCheckbox4" name="" type="checkbox" value="3" />
                                                        Phụ huynh
            
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>







                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="ImagePath" class="col-md-2 control-label imagePath">Ảnh đại diện</label>
                                    <div class="col-md-9">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 100%; min-height: 185px; margin-bottom: 10px;">
                                                <img id="holder" style="" src="" />
                                            </div>
                                            <div>
                                                <div class="input-group">
                                                    <span class="input-group-btn">
                                                        <a id="lfm" data-input="thumbnail" data-preview="holder" class="btn btn-primary image">
                                                            <i class="fa fa-files-o"></i>Chọn ảnh
                                                        </a>
                                                    </span>
                                                    <input class="form-control select-text" id="thumbnail" autocomplete="off" name="image_path" type="text" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <label for="title" class="col-md-2 control-label content text-right required">Nội Dung </label>
                                    <div class="col-md-10" style="width: 85%; margin-left: -70px">
                                        <textarea name="editor1" id="editor1" rows="10" cols="80"></textarea>
                                        <script>
                                            CKEDITOR.replace('editor1');

                                        </script>

                                        <input name="file" type="file" id="files" class="form-control" value="" />

                                        <script type="text/javascript">
                                            function readTextFile(file, callback, encoding) {
                                                var reader = new FileReader();
                                                reader.addEventListener('load', function (e) {
                                                    callback(this.result);
                                                });
                                                if (encoding) reader.readAsText(file, encoding);
                                                else reader.readAsText(file);
                                            }

                                            function fileChosen(input, output) {
                                                if (input.files && input.files[0]) {
                                                    readTextFile(
                                                        input.files[0],
                                                        function (str) {
                                                            output.value = str;
                                                        }
                                                    );
                                                }
                                            }

                                            $('#files').on('change', function () {
                                                var result = $("#files").text();

                                                fileChosen(this, document.getElementById('editor1'));
                                                CKEDITOR.instances['editor1'].setData(result);
                                            });
                                        </script>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-md-2 control-label" style="width: 9%"></label>
                                <div class="col-lg-2" style="width: 200pt; padding-right: 0;">
                                    <div class="mt-checkbox-list" style="padding: 0;">
                                        <label class="mt-checkbox mt-checkbox-outline" style="margin-top: 8px; margin-bottom: 0;">
                                            <input id="is_allow_show_event" name="is_allow_show_event" type="checkbox" onclick="checkeds(this)" value="1" />
                                            <label>Hiển thị sự kiện cho người dùng</label>
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="show_event_and" id="send_at_select" style="display: none">
                                    <div class="col-lg-2">
                                        <ul class="nav nav-tabs range">
                                            <li class="nav-item dropdown">
                                                <a class="nav-link dropdown-toggle show" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Hiển thị ngay</a>
                                                <div class="dropdown-menu range">
                                                    <a class="show-now" href="javasrcipt:voide(0)" onclick="showNow()">Hiển thị ngay</a>
                                                    <a class="show" href="javasrcipt:voide(0)" onclick="show()">Hẹn gửi thông báo</a>
                                                </div>
                                            </li>
                                        </ul>

                                    </div>
                                    <div class="send_at_date_block" style="display: none;" id="block-content4">
                                        <div class="col-md-1" style="width: 45pt; padding-left: 0; padding-right: 0;">
                                            <label style="padding-top: 7px;">vào lúc</label>
                                        </div>
                                        <div class="col-xs-2">
                                            <input class="form-control daypicker form-control-inline " type="date" />
                                        </div>
                                        <div class="col-xs-2">
                                            <div class="col-10">
                                                <input class="form-control timepicker people" type="time" value="13:45:00" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 calendar-reminder">
                            <div class="form-group">
                                <label class="col-md-2 control-label " style="width: 9%"></label>
                                <div class="col-lg-2" style="width: 130pt;">
                                    <div class="mt-checkbox-list" style="padding: 0;">
                                        <label class="mt-checkbox mt-checkbox-outline" style="margin-top: 5px; margin-bottom: 0;">
                                            <input id="is_allow_event" name="is_allow_event" type="checkbox" onclick="checkedss(this)" value="1" />
                                            <label for="PromptBefore">Nhắc lịch sự kiện</label>
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="block_event" style="display: none;" id="prompt_before_minute">
                                    <div class="col-md-1" style="padding-right: 1px;">
                                        <input class="form-control" name="prompt_before_number" type="number" value="15" />
                                    </div>
                                    <div class="col-md-1" style="padding-left: 1px;">
                                        <select class="custom-select class mr-sm-2" style="width: 100%">
                                            <option selected="selected">Giây...</option>
                                            <option value="seconds">Giây</option>
                                            <option value="minute">Phút</option>
                                            <option value="day">Ngày</option>
                                            <option value="week">Tuần</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3" style="padding-top: 5px">
                                        <label>trước khi sự kiện bắt đầu</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-md-2 control-label" style="width: 9%"></label>
                                <div class="col-md-1" style="padding-right: 0; width: 92pt; margin-top: 5px;">
                                    <div class="mt-checkbox-list" style="padding: 0;">
                                        <label class="mt-checkbox mt-checkbox-outline" style="margin-bottom: 0;">
                                            <input id="is_allow_register" name="is_allow_register" type="checkbox" onclick="checkedsss(this)" value="1" />
                                            <label for="Allow registration">Mở đăng ký</label>
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="block_register" id="open_register" style="display: none;">
                                    <div class="col-lg-1 text-center">
                                        <span>
                                            <label for="To" class="from">Từ</label></span>
                                    </div>
                                    <div class="col-xs-2">
                                        <input type="text" class="form-control daypicker form-control-inline " placeholder="Ngày bắt đầu" id="datetime1" />
                                    </div>



                                    <div class="col-lg-1 text-center">
                                        <span>
                                            <label for="To" class="from">Kết thúc</label></span>
                                    </div>
                                    <div class="col-xs-2">
                                        <input type="text" class="form-control daypicker form-control-inline " placeholder="Ngày kết thúc" id="datetime2" />
                                                                    
                                    </div>



                                    <div class="col-md-4" style="margin-top: 7px;">
                                        <div class="mt-checkbox-list">
                                            <label class="mt-checkbox mt-checkbox-outline">
                                                <input id="confirmation" name="confirmation" type="checkbox" value="1" />
                                                <label for="ConfirMation">Bắt buộc xác nhận tham gia</label>
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="block_register" id="block-content5" style="display: none;">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="col-md-2" style="width: 9%;">
                                    </div>
                                    <div class="col-md-10" style="width: 86%;">
                                        <h3>Thiết lập hình thức đăng ký</h3>
                                        <hr />
                                        <div class="register-form-border" style="border: 1px #3598dc dashed; padding-top: 15px;">

                                            <div id="add_option">
                                            </div>
                                            <div class="form-group event-regyster ">
                                                
                                                <div class="col-md-2" style="padding-right: 0; padding-left: 0; width: 120pt;">
                                                    <input class="form-control" id="option_title" placeholder="Tên sự kiện " autocomplete="off" name="option_title" type="text" />
                                                </div>
                                                <div class="col-sx-2">
                                                    <input type="text" class="form-control daypicker form-control-inline " placeholder="Ngày bắt đầu" id="datetime3" />
                                                                                            
                                                </div>


                                                <div class="col-sx-2">
                                                    <input type="text" class="form-control daypicker form-control-inline " placeholder="Ngày kết thúc" id="datetime4" />
                                                                                           
                                                </div>


                                                <script>
                                                    $('#datetime1').datetimepicker({
                                                        step: 5
                                                    });
                                                    $('#datetime2').datetimepicker({
                                                        step: 5
                                                    });
                                                    $('#datetime3').datetimepicker({
                                                        step: 5
                                                    });
                                                    $('#datetime4').datetimepicker({
                                                        step: 5
                                                    });
                                                </script>
                                               
                                                    <label for="Option" class="control-label col-md-2 event text-right">tối đa</label>
                                               
                                                <div class="col-md-1 people-number-title" style="padding-left: 0; width: 11%;">
                                                    <div class="input-group">
                                                        <input class="form-control people-number" id="option_targets" min="1" max="4" name="option_targets" type="number" value="1" />
                                                        <span class="input-group-btn">                                                   
                                                            <input class="btn blue people-number" type="submit" value="Người" />
                                                        </span>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="col-md-1" style="padding-left: 0; padding-right: 5pt; width: 12%;">
                                                    <div class="input-group">
                                                        <input class="form-control option_cost" id="option_cost" placeholder="Chi phí" autocomplete="off" name="option_cost" type="text" />
                                                        <span class="input-group-btn">
                                                            <input class="btn blue people-number" type="submit" value="VNĐ" />
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-md-1" style="width: 50pt; padding-right: 0; padding-left: 1pt;">
                                                    <a class="btn btn-warning" id="btn_add_option"><i class="fa fa-plus"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>

                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="box box-success">
                    <div class="box-body ">
                        <div class="btn-group">
                            <a href="#" name="btn_save_form" class="btn btn-danger" id="btn_save_form"><i class="fa fa-floppy-o" aria-hidden="true"></i>Tạo mới</a>
                            <a href="#" name="btn_save_draft" class="btn btn-primary" id="btn_save_draft"><i class="fa fa-floppy-o" aria-hidden="true"></i>Lưu nháp</a>

                            <a href="#" class="btn btn-default">
                                <i class="fa fa-remove"></i>
                                <span>Đóng</span>
                            </a>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <!-- /.box-body -->
                </div>
                
             

            </div>
        </div>
    </div>
    <style>
        .main-omt {
            border: 1px solid #32c5d2;
            background-color: #fff;
        }

            .main-omt .omt-header {
                background-color: #32c5d2;
                padding: 4px 7px;
                display: flex;
            }

        .omt-header .header-title {
            font-size: 20px;
            padding: 10px 10px;
            color: white;
            width: 100%;
            margin:0;
        }

        .omt-header .omt__icon {
            font-size: 30px;
            padding: 8px 10px;
            color: white;
        }

        .portlet-body {
            background-color: white;
            padding: 15px;
        }

        .control-label .required, .form-group .required {
            color: #e02222;
            font-size: 14px;
            padding-left: 2px;
        }

        .form-control.daypicker {
            height: 34px;
            font-size: 14px !important;
            width: 154px;
            font-size: 0.75rem;
        }

        .form-group .row [class^='col'] {
            padding-left: 0;
            padding-right: 10px;
        }

        .dropdown-menu.range {
            min-width: 9rem;
        }

        .nav.nav-tabs.range {
            border: none;
            margin-top: 10px;
        }

        .dropdown-menu.range a {
            text-decoration: none;
            padding: 2px 21px;
            color: black;
            font-weight: 600;
            font-size: 15px;
        }

        .dropdown-menu.range .account {
            padding-left: 21px;
        }

        .nav.nav-tabs.range a:hover {
            color: red;
        }



        .nav-tabs .nav-link {
            border: none;
        }

        .nav-link.dropdown-toggle.title {
            font-size: 15px;
            text-decoration: none;
            color: black;
        }

        .control-label.range {
            padding-top: 20px;
        }

        .mt-checkbox-inline.object {
            margin-top: 20px;
        }

        .align-items-center.account {
            margin-top: 10px;
            display: none;
        }

        .control-label.imagePath {
            text-align: right;
            font-size: 12px;
            padding-top: 20px;
        }

        .fileinput-preview.thumbnail {
            width: 100%;
            min-height: 185px;
            margin-bottom: 10px;
        }

        .thumbnail {
            padding: 4px;
            line-height: 1.42857;
            background-color: #fff;
            border: 1px solid #ddd;
            border-radius: 4px;
        }

        .btn.btn-primary.image {
            color: #fff;
            background-color: #337ab7;
            border-color: #2e6da4;
            border-radius: 2px;
            font-size: 10px;
            font-weight: 600;
        }

            .btn.btn-primary.image:hover {
                color: #fff;
                background-color: #286090;
                border-color: #204d74;
            }

        .form-control.select-text {
            height: 34px;
            padding: 6px 12px;
            background-color: #fff;
            border: 1px solid #c2cad8;
            border-radius: 4px;
            margin-top: -1px;
            height: 31px;
        }

        .control-label.content {
            display: block;
            position: relative;
            left: -74px;
            height: 20px;
            padding: 20px 10px 0 0;
        }

        .app .content {
            min-height: 0;
        }

        .custom-select.class {
            height: 34px;
        }

        .custom-select.acount {
            height: 34px;
            margin-left: 84px;
        }

        .nav-link.dropdown-toggle.show {
            width: 170px;
            margin-top: -8px;
            text-decoration: none;
            color: black;
        }

        .dropdown-menu.range .show-now {
            padding-left: 15px;
        }

        .dropdown-menu.range .show {
            padding-left: 15px;
        }

        .form-control.timepicker.people {
            width: 134px;
        }

        .text-center {
            width: 78px;
            padding-top: 7px;
            font-size: 12px;
        }

        .from label {
            max-width: 0 !important;
        }

        .control-label.event.text-right {
            position:relative;
            left:-30px;
            width: 12%;
            text-align: right;
            margin-bottom: 0;
            padding-top: 7px;
            font-size: 12px;
        }

        #datetime3.form-control.daypicker {
            float: left;
            margin-left: 10px;
        }

        #datetime4.form-control.daypicker {
            float: left;
            margin-left: 10px;
        }

        .form-control.people-number {
            width: 51px;
        }

        .btn.blue.people-number {
            color: #FFFFFF;
            background-color: #3598dc;
            border-color: #3598dc;
        }
        .calendar-reminder{
            margin:20px 0;
        }
        .form-control.option_cost{
            width:79px;
        }
        #btn_add_option.btn.btn-warning{
            margin-left:48px;
        }
        .event-regyster{
            display:flex;
        }
        .people-number-title{
            position:relative;
            left:-30px;
        }
        .box.box-success{
            text-align:center;
            margin-top:40px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

