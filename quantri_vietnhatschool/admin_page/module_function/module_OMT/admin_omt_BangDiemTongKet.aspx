﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="admin_omt_BangDiemTongKet.aspx.cs" Inherits="admin_page_module_function_module_OMT_admin_omt_BangDiemTongKet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="main-omt">
        <div class="omt-header">
            <i class="fa fa-user omt__icon" aria-hidden="true"></i>
            <h4 class="header-title">Điểm Tổng Kết</h4>
        </div>
        <div class="omt-top">
            <form class="form-inline  my-2 my-lg-0">
                <input class="form-control search mr-sm-2" type="search" placeholder="Tìm mã,tên..." aria-label="Search" />
            </form>
            <div class="col-auto object">
                <select class="custom-select mr-sm-2" style="width: 100%">
                    <option value="" selected="selected">--1B1</option>
                </select>
                <b class="presentation" role="presentation"></b>
            </div>
            <div class="col-auto object">
                <select class="custom-select mr-sm-2" style="width: 100%">
                    <option value="" selected="selected">cả năm học</option>

                    <option value="">học kì 1</option>
                    <option value="">học kì 2</option>
                </select>
                <b class="presentation" role="presentation"></b>
            </div>
            <div class="col-auto object">
                <select class="custom-select mr-sm-2" style="width: 100%">
                    <option value="" selected="selected">--học lực</option>
                    <option value="">giỏi</option>
                    <option value="">khá</option>
                    <option value="">Trung bình</option>
                    <option value="">yếu</option>
                    <option value="">kém</option>
                </select>
                <b class="presentation" role="presentation"></b>
            </div>
            <div class="col-auto object">
                <select class="custom-select mr-sm-2" style="width: 100%">
                    <option value="" selected="selected">--hạnh kiểm</option>
                    <option value="">tốt</option>
                    <option value="">khá</option>
                    <option value="">Trung bình</option>
                    <option value="">yếu</option>

                </select>
                <b class="presentation" role="presentation"></b>
            </div>
            <div class="col-auto object">
                <select class="custom-select mr-sm-2" style="width: 100%">
                    <option value="" selected="selected">--Danh hiệu</option>
                    <option value="">Học sinh giỏi</option>
                    <option value="">Học sinh khá</option>


                </select>
                <b class="presentation" role="presentation"></b>
            </div>
            <div class="col-md-2" style="padding-left: 5px; padding-right: 5px">
                <button type="button" class="btn bg-green-jungle font-white">
                    <i class="fa fa-file-excel-o font-white"></i>
                </button>
            </div>
        </div>
        <div class="fixed-table-container" >
            <div class="note note-success">
                <p>Dữ liệu được tổng hợp sau khi chạy <b>Tính tổng kết cuối kỳ</b>. Nếu không tồn tại dữ liệu, vui lòng chạy tại menu <b>Tổng kết</b> -&gt; <b>Tính tổng kết cuối kỳ</b> </p>
            </div>
            <div>
                Số lượng học sinh: <b>10</b>
            </div>
            <div class="table-scrollable">
                <table class="table table-bordered table-striped table-condensed flip-content">
                    <thead class="flip-content bg-blue bg-font-blue">
                        <tr>
                            <th style="vertical-align: middle; text-align: center">#</th>
                            <th style="vertical-align: middle; text-align: center">Lớp </th>
                            <th style="vertical-align: middle; text-align: center">Code </th>
                            <th style="vertical-align: middle; text-align: center">Họ tên </th>
                            <th style="vertical-align: middle; text-align: center">Ngày sinh </th>
                            <th style="vertical-align: middle; text-align: center">Điểm TB </th>
                            <th style="vertical-align: middle; text-align: center">Học lực </th>
                            <th style="vertical-align: middle; text-align: center">Hạnh kiểm </th>
                            <th style="vertical-align: middle; text-align: center">Danh hiệu </th>
                            <th style="vertical-align: middle; text-align: center">Toán </th>
                            <th style="vertical-align: middle; text-align: center">Ngữ văn </th>
                            <th style="vertical-align: middle; text-align: center">Tiếng Việt </th>
                            <th style="vertical-align: middle; text-align: center">Vật lý </th>
                            <th style="vertical-align: middle; text-align: center">Việt Nam học </th>
                            <th style="vertical-align: middle; text-align: center">Sinh học </th>
                            <th style="vertical-align: middle; text-align: center">Lịch sử </th>
                            <th style="vertical-align: middle; text-align: center">Lịch sử và Địa lý </th>
                            <th style="vertical-align: middle; text-align: center">Khoa học tự nhiên </th>
                            <th style="vertical-align: middle; text-align: center">Địa lý </th>
                            <th style="vertical-align: middle; text-align: center">Tiếng Anh </th>
                            <th style="vertical-align: middle; text-align: center">Tin học </th>
                            <th style="vertical-align: middle; text-align: center">Công nghệ </th>
                            <th style="vertical-align: middle; text-align: center">Hóa học </th>
                            <th style="vertical-align: middle; text-align: center">Âm nhạc </th>
                            <th style="vertical-align: middle; text-align: center">Mỹ thuật </th>
                            <th style="vertical-align: middle; text-align: center">Thể dục </th>
                            <th style="vertical-align: middle; text-align: center">Tự nhiên xã hội </th>
                            <th style="vertical-align: middle; text-align: center">GDCD </th>
                            <th style="vertical-align: middle; text-align: center">Đạo đức </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th style="vertical-align: middle; text-align: center">1 </th>
                            <th style="vertical-align: middle; text-align: center">1B1 </th>
                            <th style="vertical-align: middle; text-align: center">OMT-011 </th>
                            <th style="vertical-align: middle; text-align: center">Nguyễn Tùng Anh </th>
                            <th style="vertical-align: middle; text-align: center">16/02/2008 </th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                        </tr>
                        <tr>
                            <th style="vertical-align: middle; text-align: center">2 </th>
                            <th style="vertical-align: middle; text-align: center">1B1 </th>
                            <th style="vertical-align: middle; text-align: center">OMT-012 </th>
                            <th style="vertical-align: middle; text-align: center">Phạm Hoàng Anh </th>
                            <th style="vertical-align: middle; text-align: center">05/11/2008 </th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                        </tr>
                        <tr>
                            <th style="vertical-align: middle; text-align: center">3 </th>
                            <th style="vertical-align: middle; text-align: center">1B1 </th>
                            <th style="vertical-align: middle; text-align: center">OMT-013 </th>
                            <th style="vertical-align: middle; text-align: center">Phạm Thị Tuyết Anh </th>
                            <th style="vertical-align: middle; text-align: center">19/09/2008 </th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                        </tr>
                        <tr>
                            <th style="vertical-align: middle; text-align: center">4 </th>
                            <th style="vertical-align: middle; text-align: center">1B1 </th>
                            <th style="vertical-align: middle; text-align: center">OMT-014 </th>
                            <th style="vertical-align: middle; text-align: center">Phạm Trâm Anh </th>
                            <th style="vertical-align: middle; text-align: center">16/07/2008 </th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                        </tr>
                        <tr>
                            <th style="vertical-align: middle; text-align: center">5 </th>
                            <th style="vertical-align: middle; text-align: center">1B1 </th>
                            <th style="vertical-align: middle; text-align: center">OMT-015 </th>
                            <th style="vertical-align: middle; text-align: center">Trần Đức Anh </th>
                            <th style="vertical-align: middle; text-align: center">12/12/2008 </th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                        </tr>
                        <tr>
                            <th style="vertical-align: middle; text-align: center">6 </th>
                            <th style="vertical-align: middle; text-align: center">1B1 </th>
                            <th style="vertical-align: middle; text-align: center">OMT-016 </th>
                            <th style="vertical-align: middle; text-align: center">Lê Minh Bảo Anh </th>
                            <th style="vertical-align: middle; text-align: center">25/05/2008 </th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                        </tr>
                        <tr>
                            <th style="vertical-align: middle; text-align: center">7 </th>
                            <th style="vertical-align: middle; text-align: center">1B1 </th>
                            <th style="vertical-align: middle; text-align: center">OMT-017 </th>
                            <th style="vertical-align: middle; text-align: center">Lại Viết Hoàng Anh </th>
                            <th style="vertical-align: middle; text-align: center">18/04/2008 </th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                        </tr>
                        <tr>
                            <th style="vertical-align: middle; text-align: center">8 </th>
                            <th style="vertical-align: middle; text-align: center">1B1 </th>
                            <th style="vertical-align: middle; text-align: center">OMT-018 </th>
                            <th style="vertical-align: middle; text-align: center">Lê Trang Anh </th>
                            <th style="vertical-align: middle; text-align: center">27/10/2008 </th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                        </tr>
                        <tr>
                            <th style="vertical-align: middle; text-align: center">9 </th>
                            <th style="vertical-align: middle; text-align: center">1B1 </th>
                            <th style="vertical-align: middle; text-align: center">OMT-019 </th>
                            <th style="vertical-align: middle; text-align: center">Đào Phương Anh </th>
                            <th style="vertical-align: middle; text-align: center">10/12/2008 </th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                        </tr>
                        <tr>
                            <th style="vertical-align: middle; text-align: center">10 </th>
                            <th style="vertical-align: middle; text-align: center">1B1 </th>
                            <th style="vertical-align: middle; text-align: center">OMT-020 </th>
                            <th style="vertical-align: middle; text-align: center">Lê Sơn Bách </th>
                            <th style="vertical-align: middle; text-align: center">07/01/2008 </th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                            <th style="vertical-align: middle; text-align: center"></th>
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
    <style>
        .main-omt {
            border: 1px solid #32c5d2;
            background-color: #fff;
        }

            .main-omt .omt-header {
                background-color: #32c5d2;
                padding: 4px 7px;
                display: flex;
            }

        .omt-header .header-title {
            font-size: 20px;
            padding: 10px 10px;
            color: white;
            width: 100%;
        }

        .omt-header .omt__icon {
            font-size: 30px;
            padding: 8px 10px;
            color: white;
        }

        .fixed-table-container {
            width: 98%;
            margin-left: 1%;
        }

        .omt-top {
            display: flex;
            padding: 20px 20px;
        }

        .form-control.search {
            width: 20%;
        }

        .presentation {
            border-color: #999 transparent transparent transparent;
            border-style: solid;
            border-width: 4px 4px 0 4px;
            height: 0;
            left: -15px;
            margin-left: -4px;
            margin-top: -2px;
            position: relative;
            top: 30%;
            width: 0;
        }

        .object {
            margin-left: 15px;
        }

        .btn.bg-green-jungle.font-white {
            background-color: #26C281;
            color: white;
            outline: none;
        }

        .form-control.search {
            height: 38px;
        }

        .note {
            margin: 0 0 20px 0;
            padding: 15px 30px 15px 15px;
            width: 100%;
            min-height: 100px;
        }

            .note.note-success {
                background-color: #c0edf1;
                border-color: #58d0da;
                color: black !important;
                font-size: 12px;
                color: black;
            }

        .bg-blue {
            background: #3598dc !important;
            color: white;
        }

        .table-scrollable {
            width: 100%;
            overflow-x: auto;
            overflow-y: hidden;
            border: 1px solid #e7ecf1;
            margin: 10px 0 !important;
        }

        ::-webkit-scrollbar-thumb {
            height:20px;
            border-radius: 50px;
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
            background-color: #F5F5F5 !important;
        }

        ::-webkit-scrollbar-button {
            background-color: #bbbbbb;
            display: block;
            border-style: solid;
            height: 13px;
            width: 16px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

