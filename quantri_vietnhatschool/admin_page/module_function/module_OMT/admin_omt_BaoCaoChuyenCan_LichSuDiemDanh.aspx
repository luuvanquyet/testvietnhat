﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="admin_omt_BaoCaoChuyenCan_LichSuDiemDanh.aspx.cs" Inherits="admin_page_module_function_module_OMT_admin_omt_BaoCaoChuyenCan_LichSuDiemDanh" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="main-omt">
        <div class="omt-header">
            <i class="fa fa-user omt__icon" aria-hidden="true"></i>
            <h4 class="header-title">Lịch sử ngày nghỉ theo học sinh</h4>
        </div>
        <div class="omt-top">
            <div class="col-md-2 object">
                <select class="custom-select mr-sm-2" style="width: 100%">
                    <option value="" selected="selected">--Tất cả </option>
                    <option value="">1B1</option>
                </select>
                <b class="presentation" role="presentation"></b>
            </div>
            <div class="col-auto object">
                <select class="custom-select mr-sm-2" style="width: 100%">
                    <option value="" selected="selected">--Tất cả </option>
                    <option value="3292">Nguyễn Tùng Anh - [OMT-011]</option>
                    <option value="3294">Phạm Hoàng Anh - [OMT-012]</option>
                    <option value="3296">Phạm Thị Tuyết Anh - [OMT-013]</option>
                    <option value="3298">Phạm Trâm Anh - [OMT-014]</option>
                    <option value="3300">Trần Đức Anh - [OMT-015]</option>
                    <option value="3302">Lê Minh Bảo Anh - [OMT-016]</option>
                    <option value="3304">Lại Viết Hoàng Anh - [OMT-017]</option>
                    <option value="3306">Lê Trang Anh - [OMT-018]</option>
                    <option value="3308">Đào Phương Anh - [OMT-019]</option>
                    <option value="3310">Lê Sơn Bách - [OMT-020]</option>
                </select>
                <b class="presentation" role="presentation"></b>
            </div>
            <div class="col-auto object">
                <input type="text" class="form-control daypicker form-control-inline " placeholder="31/8/2020" id="datetime1" />
            </div>
            <div class="col-auto object">
                <input type="text" class="form-control daypicker form-control-inline " placeholder="04/09/2020" id="datetime2" />

            </div>
            <script>
                $('#datetime1').datetimepicker({
                    step: 5
                });
                $('#datetime2').datetimepicker({
                    step: 5
                });

            </script>
            <div class="col-md-2" style="padding-left: 5px; padding-right: 5px">
                <button type="button" class="btn bg-green-jungle font-white">
                    <i class="fa fa-file-excel-o font-white"></i>
                </button>
            </div>
        </div>
        <div class="fixed-table-container">
            <div class="col-md-4" style="padding: 0 !important">
                <table class="table table-bordered table-striped table-condensed flip-content ">
                    <tbody>
                        <tr>
                            <td style="text-align: center; font-size: 10px; background-color: #e5e5e5;">-
                            </td>
                            <td style="font-size: 10px;">Không điểm danh
                            </td>
                            <td style="text-align: center; font-size: 10px; background-color: #1BA39C">D
                            </td>
                            <td style="font-size: 10px;">Đúng giờ
                            </td>
                            <td style="text-align: center; font-size: 10px; background-color: #f3c200">M
                            </td>
                            <td style="font-size: 10px;">Đến muộn
                            </td>
                            <td style="text-align: center; font-size: 10px; background-color: #f2784b">P
                            </td>
                            <td style="font-size: 10px;">Vắng có phép
                            </td>
                            <td style="text-align: center; font-size: 10px; background-color: #e7505a">K
                            </td>
                            <td style="font-size: 10px;">Vắng không phép
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-6">
            </div>
        </div>
        <div class="fixed-table-container" style="padding-top: 20px;" id="data-items">
            <div class="table-scrollable">
                <table class="table table-bordered table-striped table-condensed flip-content">
                    <thead class="flip-content bg-blue bg-font-blue">
                        <tr>
                        </tr>
                        <tr>
                            <th style="vertical-align: middle; text-align: center; width: 10%">Ngày </th>
                            <th style="vertical-align: middle; text-align: center; width: 20%">Lớp </th>
                            <th style="vertical-align: middle; text-align: center; width: 20%">Giáo viên chủ nhiệm </th>
                            <th style="vertical-align: middle; text-align: center; width: 15%">Trạng thái </th>
                            <th style="vertical-align: middle; text-align: center; width: 35%">Ghi chú </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td style="text-align: center">08/09/2020
                            </td>
                            <td style="text-align: left">1B1
                            </td>
                            <td style="text-align: center">Lương Văn Nguyên
                            </td>
                            <td style="text-align: center; background-color: #e5e5e5;">-
                            </td>
                            <td style="text-align: center"></td>
                        </tr>
                        <tr>
                            <td style="text-align: center">07/09/2020
                            </td>
                            <td style="text-align: left">1B1
                            </td>
                            <td style="text-align: center">Lương Văn Nguyên
                            </td>
                            <td style="text-align: center; background-color: #e5e5e5;">-
                            </td>
                            <td style="text-align: center"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <style>
        .main-omt {
            border: 1px solid #32c5d2;
            background-color: #fff;
        }

            .main-omt .omt-header {
                background-color: #32c5d2;
                padding: 4px 7px;
                display: flex;
            }

        .omt-header .header-title {
            font-size: 20px;
            padding: 10px 10px;
            color: white;
            width: 100%;
        }

        .omt-header .omt__icon {
            font-size: 30px;
            padding: 8px 10px;
            color: white;
        }

        .fixed-table-container {
            width: 98%;
            margin-left: 1%;
        }

        .omt-top {
            display: flex;
            padding: 20px 20px;
        }

        .form-control.search {
            width: 20%;
        }

        .presentation {
            border-color: #999 transparent transparent transparent;
            border-style: solid;
            border-width: 4px 4px 0 4px;
            height: 0;
            left: -15px;
            margin-left: -4px;
            margin-top: -2px;
            position: relative;
            top: 30%;
            width: 0;
        }

        .object {
            margin-left: 15px;
        }

        .btn.bg-green-jungle.font-white {
            background-color: #26C281;
            color: white;
            outline: none;
        }

        .bg-blue {
            background: #3598dc !important;
            color: white;
        }
    </style>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

