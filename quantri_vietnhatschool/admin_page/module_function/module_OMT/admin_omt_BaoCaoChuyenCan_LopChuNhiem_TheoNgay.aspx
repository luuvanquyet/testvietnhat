﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="admin_omt_BaoCaoChuyenCan_LopChuNhiem_TheoNgay.aspx.cs" Inherits="admin_page_module_function_module_OMT_admin_omt_BaoCaoChuyenCan_LopChuNhiem_TheoNgay" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <link rel="stylesheet" href="/jquery.datetimepicker.min.css" />
    <script src="/jquery.js"></script>
    <script src="/jquery.datetimepicker.full.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="main-omt">
        <div class="omt-header">
            <i class="fa fa-user omt__icon" aria-hidden="true"></i>
            <h4 class="header-title">Thống kê Điểm danh</h4>
        </div>
        <div class="omt-top">
            <form class="form-inline  my-2 my-lg-0">
                <input class="form-control search mr-sm-2" type="search" placeholder="Tìm mã,tên..." aria-label="Search" />
            </form>
            <div class="col-md-2 object">
                <select class="custom-select mr-sm-2" style="width: 100%">
                    <option value="" selected="selected">--Tất cả </option>
                    <option value="">1B1</option>
                </select>
                <b class="presentation" role="presentation"></b>
            </div>
            <div class="col-auto object">
                <input type="text" class="form-control daypicker form-control-inline " placeholder="31/8/2020" id="datetime1" />
            </div>
            <div class="col-auto object">
                <input type="text" class="form-control daypicker form-control-inline " placeholder="04/09/2020" id="datetime2" />

            </div>
            <script>
                $('#datetime1').datetimepicker({
                    step: 5
                });
                $('#datetime2').datetimepicker({
                    step: 5
                });

            </script>
            <div class="col-md-2 checkbox-attendance">
                <div class="mt-checkbox-inline">
                    <label class="mt-checkbox">
                        <input class="is_attendance" name="is_attendance" type="checkbox" value="1" />
                        Đã điểm danh
                                       
                    </label>
                </div>
            </div>
            <div class="col-md-2" style="padding-left: 5px; padding-right: 5px">
                <button type="button" class="btn bg-green-jungle font-white">
                    <i class="fa fa-file-excel-o font-white"></i>
                </button>
            </div>
        </div>
        <div class="fixed-table-container" style="padding-top: 20px;" id="data-items">
            <div class="”table-scrollable”">
                <table class="table table-bordered table-striped table-condensed flip-content">
                    <thead class="flip-content bg-blue bg-font-blue">
                        <tr>
                            <th style="vertical-align: middle; text-align: center; width: 2%">Ngày </th>
                            <th style="vertical-align: middle; text-align: center; width: 4%">Lớp </th>
                            <th style="vertical-align: middle; text-align: center; width: 11%">Giáo viên chủ nhiệm </th>
                            <th style="vertical-align: middle; text-align: center; width: 10%">Trạng thái </th>
                            <th style="vertical-align: middle; text-align: center; width: 6%">Thời gian </th>
                            <th style="vertical-align: middle; text-align: center; width: 10%">Điểm danh bởi </th>
                            <th style="vertical-align: middle; text-align: center; width: 4%">Sĩ số </th>
                            <th style="vertical-align: middle; text-align: center; width: 4%">Tổng số </th>
                            <th style="vertical-align: middle; text-align: center; width: 5%">Đến muộn </th>
                            <th style="vertical-align: middle; text-align: center; width: 5%">Vắng có phép </th>
                            <th style="text-align: center;vertical-align: middle; width: 5%">Vắng không phép </th>
                            <th style="text-align: center;vertical-align: middle; width: 10%">Ghi chú suất ăn </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td style="text-align: center;font-size:12px">
                                <a class="Date-object" target="_blank" href="#">04/09/2020</a>
                            </td>
                            <td style="text-align: center;font-size:12px">
                                <a class="Date-object" target="_blank" href="#">1B1</a>
                            </td>
                            <td style="font-size:12px">Lương Văn Nguyên
                            </td>
                            <td style="text-align: center;font-size:12px">
                                <span class="label label-sm label-default">Chưa điểm danh</span>
                            </td>
                            <td style="text-align: center;font-size:12px"></td>
                            <td style="text-align: center;font-size:12px"></td>
                            <td style="text-align: center;font-size:12px">0
                            </td>
                            <td style="text-align: center;font-size:12px">10
                            </td>
                            <td style="text-align: center; background-color: #e5e5e5;font-size:12px">0
                            </td>
                            <td style="text-align: center; background-color: #e5e5e5;font-size:12px">0
                            </td>
                            <td style="text-align: center; background-color: #e5e5e5;font-size:12px">0
                            </td>
                            <td style="text-align: left; font-size: 12px"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <style>
        .main-omt {
            border: 1px solid #32c5d2;
            background-color: #fff;
        }

            .main-omt .omt-header {
                background-color: #32c5d2;
                padding: 4px 7px;
                display: flex;
            }

        .omt-header .header-title {
            font-size: 20px;
            padding: 10px 10px;
            color: white;
            width: 100%;
        }

        .omt-header .omt__icon {
            font-size: 30px;
            padding: 8px 10px;
            color: white;
        }

        .fixed-table-container {
            width: 98%;
            margin-left: 1%;
        }

        .omt-top {
            display: flex;
            padding: 20px 20px;
        }

        .form-control.search {
            width: 20%;
        }

        .presentation {
            border-color: #999 transparent transparent transparent;
            border-style: solid;
            border-width: 4px 4px 0 4px;
            height: 0;
            left: -15px;
            margin-left: -4px;
            margin-top: -2px;
            position: relative;
            top: 30%;
            width: 0;
        }

        .object {
            margin-left: 15px;
        }

        .btn.bg-green-jungle.font-white {
            background-color: #26C281;
            color: white;
            outline: none;
        }

        .form-control.search {
            height: 38px;
        }

        .mt-checkbox {
            display: flex;
            justify-content: space-around;
        }

        .is_attendance {
            width: 19px;
            height: 19px;
        }

        .checkbox-attendance {
            padding-top: 10px;
        }
        .bg-blue {
            background: #3598dc !important;
            color: white;
            font-size:13px;
            
        }
        .Date-object{
            color:blue !important;
            text-decoration:none !important;
        }
        .Date-object:hover{
            text-decoration:underline !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

