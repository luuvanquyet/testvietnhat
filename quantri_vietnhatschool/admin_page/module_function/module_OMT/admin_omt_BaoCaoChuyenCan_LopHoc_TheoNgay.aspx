﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="admin_omt_BaoCaoChuyenCan_LopHoc_TheoNgay.aspx.cs" Inherits="admin_page_module_function_module_OMT_admin_omt_BaoCaoChuyenCan_LopHoc_TheoNgay" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="main-omt">
        <div class="omt-header">
            <i class="fa fa-user omt__icon" aria-hidden="true"></i>
            <h4 class="header-title">Thống kê Điểm danh</h4>
        </div>
        <div class="omt-top">
            <form class="form-inline  my-2 my-lg-0">
                <input class="form-control search mr-sm-2" type="search" placeholder="Tìm mã,tên..." aria-label="Search" />
            </form>
            <div class="col-md-2 object">
                <select class="custom-select mr-sm-2" style="width: 100%">
                    <option value="" selected="selected">--Tất cả </option>
                    <option value="">1B1</option>
                </select>
                <b class="presentation" role="presentation"></b>
            </div>
            <div class="col-auto object">
                <input type="text" class="form-control daypicker form-control-inline " placeholder="31/8/2020" id="datetime1" />
            </div>
            <div class="col-auto object">
                <input type="text" class="form-control daypicker form-control-inline " placeholder="04/09/2020" id="datetime2" />

            </div>
            <script>
                $('#datetime1').datetimepicker({
                    step: 5
                });
                $('#datetime2').datetimepicker({
                    step: 5
                });

            </script>
            <div class="col-md-3 checkbox-attendance">
                <div class="mt-checkbox-inline">
                    <label class="mt-checkbox">
                        <input class="is_attendance" name="is_attendance" type="checkbox" value="1" />
                        Đã điểm danh
                                       
                    </label>
                </div>
            </div>
            <div class="col-md-2  excel" style="padding-left: 5px; padding-right: 5px">
                <button type="button" class="btn bg-green-jungle font-white">
                    <i class="fa fa-file-excel-o font-white"></i>
                </button>
            </div>
            <div class="col-md-2 " style="padding-left: 5px; padding-right: 5px">
                <button type="button" class="btn bg-green-jungle font-white"><i class="fa fa-line-chart font-white"></i>Tổng hợp dữ liệu</button>
            </div>
        </div>
        <div class="fixed-table-container">
            <div id="datatable_include_wrapper" class="dataTables_wrapper no-footer">
                <div class="row" style="margin-top:0">
                    <div class="col-md-6 col-sm-6">
                        <div class="dataTables_length" id="datatable_include_length">
                            <label></label>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div id="datatable_include_filter" class="dataTables_filter">
                            <label>Tìm kiếm<input type="search" class="form-control input-sm input-small input-inline"/></label>
                        </div>
                    </div>
                </div>
                <div class="table-scrollable">
                    <table class="table table-bordered table-striped table-condensed flip-content">
                        <thead class="flip-content bg-blue bg-font-blue">
                            <tr role="row">
                                <th style="vertical-align: middle; text-align: center; width: 5%">Ngày </th>
                                <th style="vertical-align: middle; text-align: center; width: 20%">Lớp học </th>
                                <th style="vertical-align: middle; text-align: center; width: 15%">Tiết học </th>
                                <th style="vertical-align: middle; text-align: center; width: 10%">Lớp chủ nhiệm </th>
                                <th style="vertical-align: middle; text-align: center; width: 10%">Sĩ số </th>
                                <th style="vertical-align: middle; text-align: center; width: 10%">Đến muộn </th>
                                <th style="vertical-align: middle; text-align: center; width: 10%">Vắng có phép </th>
                                <th style="vertical-align: middle; text-align: center; width: 10%">Vắng không phép </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="odd">
                                <td colspan="8" class="dataTables_empty">Không tìm thấy đối tượng table</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-md-5 col-sm-5">
                        <div class="dataTables_info" id="datatable_include_info" role="status" aria-live="polite"></div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <style>
        .main-omt {
            border: 1px solid #32c5d2;
            background-color: #fff;
        }

            .main-omt .omt-header {
                background-color: #32c5d2;
                padding: 4px 7px;
                display: flex;
            }

        .omt-header .header-title {
            font-size: 20px;
            padding: 10px 10px;
            color: white;
            width: 100%;
        }

        .omt-header .omt__icon {
            font-size: 30px;
            padding: 8px 10px;
            color: white;
        }

        .fixed-table-container {
            width: 98%;
            margin-left: 1%;
        }

        .omt-top {
            display: flex;
            padding: 20px 20px;
        }

        .form-control.search {
            width: 20%;
        }

        .presentation {
            border-color: #999 transparent transparent transparent;
            border-style: solid;
            border-width: 4px 4px 0 4px;
            height: 0;
            left: -15px;
            margin-left: -4px;
            margin-top: -2px;
            position: relative;
            top: 30%;
            width: 0;
        }

        .object {
            margin-left: 15px;
        }

        .btn.bg-green-jungle.font-white {
            background-color: #26C281;
            color: white;
            outline: none;
        }

        .form-control.search {
            height: 38px;
        }

        .mt-checkbox {
            display: flex;
            justify-content: space-around;
        }

        .is_attendance {
            width: 19px;
            height: 19px;
        }

        .checkbox-attendance {
            padding-top: 10px;
            left: 34px;
        }

        .bg-blue {
            background: #3598dc !important;
            color: white;
            font-size: 13px;
        }

        .Date-object {
            color: blue !important;
            text-decoration: none !important;
        }

            .Date-object:hover {
                text-decoration: underline !important;
            }

        .excel {
            position: relative !important;
            left: 38px !important;
        }
        .dataTables_filter{
            padding-left:50%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

