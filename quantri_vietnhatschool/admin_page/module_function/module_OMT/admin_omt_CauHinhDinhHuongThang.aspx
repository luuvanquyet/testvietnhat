﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="admin_omt_CauHinhDinhHuongThang.aspx.cs" Inherits="admin_page_module_function_module_OMT_admin_omt_CauHinhDinhHuongThang" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="main-omt">
        <div class="omt-header">
            <i class="fa fa-user omt__icon" aria-hidden="true"></i>
            <h4 class="header-title">Cho Phép Học Sinh Nhập Định Hướng Tháng</h4>
        </div>
        <div class="portlet-body" style="padding: 15px">
            <div class="fixed-table-container" style="padding-top: 20px">
                <div class="portlet light tasks-widget bordered" style="border: 1px solid #e7ecf1 !important; width: 100%; box-shadow: 0px 2px 3px 2px rgba(0, 0, 0, 0.03); border-radius: 2px">
                    <div class="portlet-title" style="border-bottom: 1px solid #e7ecf1; min-height: 48px; width: 98%; margin-left: 1%">
                        <div class="caption">
                            <span class="caption-subject">Lớp Chủ Nhiệm</span>
                        </div>
                    </div>
                    <div class="portlet-body" style="width: 98%; margin-left: 1%">
                        <div class="task-content">
                            <ul class="task-list">
                                <li class="body-top">
                                    <div class="task-checkbox">
                                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                            <input name="" type="checkbox" class="checkboxes" style="width: 19px; height: 20px; margin-top: 20px !important" value="77">
                                            <span></span>
                                        </label>
                                    </div>
                                    <div class="tast-title">
                                        <span class="task-title-sp">1B1</span>
                                    </div>
                                </li>
                                <li>
                                    <button class="btn btn-info">
                                        <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                        Cập nhật
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <style>
        .main-omt {
            border: 1px solid #32c5d2;
            background-color: #fff;
        }

            .main-omt .omt-header {
                background-color: #32c5d2;
                padding: 4px 7px;
                display: flex;
            }

        .omt-header .header-title {
            font-size: 20px;
            padding: 10px 10px;
            color: white;
        }

        .omt-header .omt__icon {
            font-size: 30px;
            padding: 8px 10px;
            color: white;
        }

        .caption {
            display: inline-block;
            font-size: 16px;
            line-height: 18px;
            padding: 10px 0;
            font-weight: 600;
            margin-top: 7px;
        }

        .task-list {
            list-style: none;
            padding: 0;
            margin: 0;
        }

            .task-list li {
                position: relative;
                padding: 10px 10px;
                border-bottom: 1px solid #F4F6F9;
            }

        .mt-checkbox {
            display: inline-block;
            position: relative;
            padding-left: 20px;
            margin-bottom: 15px;
            cursor: pointer;
            font-size: 12px;
        }

        .span {
            position: absolute;
            top: 1px;
            left: 0;
            height: 18px;
            width: 19px;
        }

        .body-top {
            display: flex;
        }

        .tast-title {
            margin-top: 19px;
            margin-left: 5px;
            font-size: 15px;
            font-weight: 600;
        }
    </style>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

