﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="admin_omt_DinhHuongHangThang.aspx.cs" Inherits="admin_page_module_function_module_OMT_admin_omt_DinhHuongHangThang" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="main-omt">
        <div class="omt-header">
            <i class="fa fa-user omt__icon" aria-hidden="true"></i>
            <h4 class="header-title">Xác Định Và Thực Hiện Mục Tiêu Tháng</h4>
        </div>
        <div class="omt-top">
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Tìm mã,tên..." aria-label="Search" />
            </form>
            <select class="form-control form-control-sm form-omt">
                <option>1B1</option>
            </select>
            <div class="col-md-2" style="padding-left: 5px; padding-right: 5px">
                <select name="select_month" id="select_month" class="select2 form-control select2-hidden-accessible" style="width: 160px; height: 35px; padding: 0.1rem 0.75rem; margin-bottom: 10px" tabindex="-1" aria-hidden="true">
                    <option value="">-- Chọn tháng</option>
                    <option value="7">07-2020</option>
                    <option value="8">08-2020</option>
                    <option value="9">09-2020</option>
                    <option value="10">10-2020</option>
                    <option value="11">11-2020</option>
                    <option value="12">12-2020</option>
                    <option value="1">01-2021</option>
                    <option value="2">02-2021</option>
                    <option value="3">03-2021</option>
                    <option value="4">04-2021</option>
                    <option value="5">05-2021</option>
                </select>
                <span class="select2 select2-container select2-container--bootstrap select2-container--below" dir="ltr" style="width: 100%;">
                    <span class="selection">
                        <span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-select_month-container">
                            <span class="select2-selection__arrow" role="presentation">
                                <b role="presentation"></b>
                            </span>
                        </span>
                    </span>
                    <span class="dropdown-wrapper" aria-hidden="true"></span>

                </span>
            </div>
            <div class="col-md-2" style="padding-left: 5px; padding-right: 5px">
                <a type="button" class="btn bg-green font-white" style="background-color: #32c5d2; color: white" href="#"><i class="fa fa-user" aria-hidden="true"></i>Thêm mới</a>
            </div>
        </div>
        <table class="table table-bordered table-striped table-condensed flip-content" style="width: 98%; margin-left: 1%">
            <thead class="flip-content bg-blue bg-font-blue" style="background-color: #3598dc; color: white">
                <tr>
                    <th rowspan="2" style="width: 3%;vertical-align:middle">Stt</th>
                    <th rowspan="2" style="vertical-align: middle; text-align: center; width: 12%">Mã học sinh</th>
                    <th rowspan="2" style="vertical-align: middle; text-align: center; width: 15%">Họ và tên</th>
                    <th rowspan="2" style="vertical-align: middle; text-align: center; width: 5%">Lớp</th>
                    <th rowspan="2" style="vertical-align: middle; text-align: center; width: 5%">Tháng</th>
                    <th colspan="3" style="vertical-align: middle; text-align: center; width: 50%">Mục tiêu</th>
                    <th rowspan="2" style="vertical-align: middle; text-align: center; width: 10%">Đánh giá</th>
                </tr>
                <tr>
                    <th style="vertical-align: middle; text-align: center; width: 17%">Sự kiện </th>
                    <th style="vertical-align: middle; text-align: center; width: 17%">Học tập </th>
                    <th style="vertical-align: middle; text-align: center; width: 16%">Nề nếp</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style="vertical-align: middle; text-align: center">1</td>
                    <td style="vertical-align: middle; text-align: center">OMT-011</td>
                    <td style="vertical-align: middle; text-align: center">Nguyễn Tùng Anh</td>
                    <td style="vertical-align: middle; text-align: center">1B1</td>
                    <td style="vertical-align: middle; text-align: center"></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td style="vertical-align: middle; text-align: center"></td>
                </tr>
                <tr>
                    <td style="vertical-align: middle; text-align: center">2</td>
                    <td style="vertical-align: middle; text-align: center">OMT-012</td>
                    <td style="vertical-align: middle; text-align: center">Phạm Hoàng Anh</td>
                    <td style="vertical-align: middle; text-align: center">1B1</td>
                    <td style="vertical-align: middle; text-align: center"></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td style="vertical-align: middle; text-align: center"></td>
                </tr>
                <tr>
                    <td style="vertical-align: middle; text-align: center">3</td>
                    <td style="vertical-align: middle; text-align: center">OMT-013</td>
                    <td style="vertical-align: middle; text-align: center">Phạm Thị Tuyết Anh</td>
                    <td style="vertical-align: middle; text-align: center">1B1</td>
                    <td style="vertical-align: middle; text-align: center"></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td style="vertical-align: middle; text-align: center"></td>
                </tr>
                <tr>
                    <td style="vertical-align: middle; text-align: center">4</td>
                    <td style="vertical-align: middle; text-align: center">OMT-014</td>
                    <td style="vertical-align: middle; text-align: center">Phạm Trâm Anh</td>
                    <td style="vertical-align: middle; text-align: center">1B1</td>
                    <td style="vertical-align: middle; text-align: center"></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td style="vertical-align: middle; text-align: center"></td>
                </tr>
                <tr>
                    <td style="vertical-align: middle; text-align: center">5</td>
                    <td style="vertical-align: middle; text-align: center">OMT-015</td>
                    <td style="vertical-align: middle; text-align: center">Trần Đức Anh</td>
                    <td style="vertical-align: middle; text-align: center">1B1</td>
                    <td style="vertical-align: middle; text-align: center"></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td style="vertical-align: middle; text-align: center"></td>
                </tr>
                <tr>
                    <td style="vertical-align: middle; text-align: center">6</td>
                    <td style="vertical-align: middle; text-align: center">OMT-016</td>
                    <td style="vertical-align: middle; text-align: center">Lê Minh Bảo Anh</td>
                    <td style="vertical-align: middle; text-align: center">1B1</td>
                    <td style="vertical-align: middle; text-align: center"></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td style="vertical-align: middle; text-align: center"></td>
                </tr>
                <tr>
                    <td style="vertical-align: middle; text-align: center">7</td>
                    <td style="vertical-align: middle; text-align: center">OMT-017</td>
                    <td style="vertical-align: middle; text-align: center">Lại Viết Hoàng Anh</td>
                    <td style="vertical-align: middle; text-align: center">1B1</td>
                    <td style="vertical-align: middle; text-align: center"></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td style="vertical-align: middle; text-align: center"></td>
                </tr>
                <tr>
                    <td style="vertical-align: middle; text-align: center">8</td>
                    <td style="vertical-align: middle; text-align: center">OMT-018</td>
                    <td style="vertical-align: middle; text-align: center">Lê Trang Anh</td>
                    <td style="vertical-align: middle; text-align: center">1B1</td>
                    <td style="vertical-align: middle; text-align: center"></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td style="vertical-align: middle; text-align: center"></td>
                </tr>
                <tr>
                    <td style="vertical-align: middle; text-align: center">9</td>
                    <td style="vertical-align: middle; text-align: center">OMT-019</td>
                    <td style="vertical-align: middle; text-align: center">Đào Phương Anh</td>
                    <td style="vertical-align: middle; text-align: center">1B1</td>
                    <td style="vertical-align: middle; text-align: center"></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td style="vertical-align: middle; text-align: center"></td>
                </tr>
                <tr>
                    <td style="vertical-align: middle; text-align: center">10</td>
                    <td style="vertical-align: middle; text-align: center">OMT-020</td>
                    <td style="vertical-align: middle; text-align: center">Lê Sơn Bách</td>
                    <td style="vertical-align: middle; text-align: center">1B1</td>
                    <td style="vertical-align: middle; text-align: center"></td>
                    <td></td>
                    <td></td>
                    <td></td>

                    <td style="vertical-align: middle; text-align: center"></td>
                </tr>
            </tbody>
        </table>

    </div>
    <style>
        .main-omt {
            border: 1px solid #32c5d2;
            background-color: #fff;
        }

            .main-omt .omt-header {
                background-color: #32c5d2;
                padding: 4px 7px;
                display: flex;
            }

        .omt-header .header-title {
            font-size: 20px;
            padding: 10px 10px;
            color: white;
        }

        .omt-header .omt__icon {
            font-size: 30px;
            padding: 8px 10px;
            color: white;
        }

        .omt-top {
            display: flex;
            padding: 0 20px;
        }

            .omt-top .form-omt {
                width: 17%;
                height: 35px !important;
                margin-right: 15px;
                margin-top: 15px;
                margin-left: 15px;
            }

        .form-control {
            width: 20%;
            height: 35px;
            margin-top: 15px;
        }

        .omt-top .btn {
            height: 35px;
            margin-top: 15px !important;
            margin-left: 5px;
            margin-right: 15px;
            border-radius: 3px;
            padding: 0.4rem 1rem;
        }
    </style>

</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

