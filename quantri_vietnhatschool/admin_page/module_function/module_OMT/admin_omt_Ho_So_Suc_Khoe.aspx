﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="admin_omt_Ho_So_Suc_Khoe.aspx.cs" Inherits="admin_page_module_function_module_OMT_admin_omt_Ho_So_Suc_Khoe" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="main-omt">
        <div class="omt-header">
            <i class="fa fa-calendar omt__icon" aria-hidden="true"></i>
            <h4 class="header-title">Hồ Sơ Y Tế</h4>
        </div>
        <div class="omt-top">
            <div class="col-auto">

                <select class="custom-select mr-sm-2" style="width: 100%" id="inlineFormCustomSelect">
                    <option selected="selected">Chọn Học Sinh...</option>
                    <option value="1">Nguyễn Tùng Anh [OMT-011 - 1B1 - 16/02/2008]</option>
                    <option value="2">Phạm Thị Tuyết Anh [OMT-013 - 1B1 - 19/09/2008]</option>
                    <option value="3">Phạm Trâm Anh [OMT-014 - 1B1 - 16/07/2008]</option>
                    <option value="4">Nguyễn Tùng Anh [OMT-011 - 1B1 - 16/02/2008]</option>
                    <option value="5">Phạm Thị Tuyết Anh [OMT-013 - 1B1 - 19/09/2008]</option>
                    <option value="6">Phạm Trâm Anh [OMT-014 - 1B1 - 16/07/2008]</option>
                    <option value="7">Nguyễn Tùng Anh [OMT-011 - 1B1 - 16/02/2008]</option>
                    <option value="8">Phạm Thị Tuyết Anh [OMT-013 - 1B1 - 19/09/2008]</option>
                    <option value="9">Phạm Trâm Anh [OMT-014 - 1B1 - 16/07/2008]</option>
                    <option value="10">Nguyễn Tùng Anh [OMT-011 - 1B1 - 16/02/2008]</option>
                    <option value="11">Phạm Thị Tuyết Anh [OMT-013 - 1B1 - 19/09/2008]</option>
                    <option value="12">Phạm Trâm Anh [OMT-014 - 1B1 - 16/07/2008]</option>
                    <option value="1">Nguyễn Tùng Anh [OMT-011 - 1B1 - 16/02/2008]</option>
                    <option value="2">Phạm Thị Tuyết Anh [OMT-013 - 1B1 - 19/09/2008]</option>
                    <option value="3">Phạm Trâm Anh [OMT-014 - 1B1 - 16/07/2008]</option>
                    <option value="4">Nguyễn Tùng Anh [OMT-011 - 1B1 - 16/02/2008]</option>
                    <option value="5">Phạm Thị Tuyết Anh [OMT-013 - 1B1 - 19/09/2008]</option>
                    <option value="6">Phạm Trâm Anh [OMT-014 - 1B1 - 16/07/2008]</option>
                    <option value="7">Nguyễn Tùng Anh [OMT-011 - 1B1 - 16/02/2008]</option>
                    <option value="8">Phạm Thị Tuyết Anh [OMT-013 - 1B1 - 19/09/2008]</option>
                    <option value="9">Phạm Trâm Anh [OMT-014 - 1B1 - 16/07/2008]</option>
                    <option value="10">Nguyễn Tùng Anh [OMT-011 - 1B1 - 16/02/2008]</option>
                    <option value="11">Phạm Thị Tuyết Anh [OMT-013 - 1B1 - 19/09/2008]</option>
                    <option value="12">Phạm Trâm Anh [OMT-014 - 1B1 - 16/07/2008]</option>

                </select>
                <b class="presentation" role="presentation"></b>

            </div>
            <div class="col-auto category">
                <select class="custom-select mr-sm-2" style="width: 100%" id="inlineFormCustomSelect2">
                    <option selected="selected">Chọn danh mục bị thương/giải phẫu...</option>
                    <option value="1">Chọn danh mục bị thương/giải phẫu...</option>
                    <option value="2">Thay khớp gối</option>
                    <option value="3">Nẹp đinh chân</option>
                </select>
                <b class="presentation" role="presentation"></b>
            </div>
            <div class="col-auto">
                <select class="custom-select mr-sm-2" style="width: 100%" id="inlineFormCustomSelect3">
                    <option selected="selected">Chọn danh mục bệnh</option>
                    <option value="1">Chọn danh mục bệnh</option>
                </select>
                <b class="presentation" role="presentation"></b>
            </div>
            <div class="col-md-1">
                <button type="button" class="btn bg-green-jungle font-white" style="background-color: #26C281; color: white; outline: none"><i class="fa fa-file-excel-o"></i>Tải về</button>
            </div>
        </div>
        <div class="fixed-table-container" id="data-items">

            <div>
                Số lượng bệnh: <b>0</b>
            </div>

            <table class="table table-bordered table-striped table-condensed flip-content">
                <thead class="flip-content bg-blue bg-font-blue">
                    <tr>
                        <th style="vertical-align: middle; text-align: center; width: 2%">#</th>
                        <th style="vertical-align: middle; text-align: center; width: 10%">Học sinh</th>
                        <th style="vertical-align: middle; text-align: center; width: 15%">Nhu cầu đặc biệt</th>
                        <th style="vertical-align: middle; text-align: center; width: 15%">Bị thương hoặc giải phẫu</th>
                        <th style="vertical-align: middle; text-align: center; width: 15%">Mắc bệnh</th>
                        <th style="vertical-align: middle; text-align: center; width: 15%">Bệnh lý</th>
                        <th style="vertical-align: middle; text-align: center; width: 15%">Liên hệ trường hợp khẩn cấp</th>
                        <th style="vertical-align: middle; text-align: center; width: 15%">Thông tin bác sỹ gia đình(nếu có)</th>

                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td colspan="8">
                            <div class="note note-warning">
                                <p>Không tim thấy thông tin trên hệ thống. Vui lòng khai báo thông tin </p>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>

        </div>
    </div>
    <style>
        .main-omt {
            border: 1px solid #32c5d2;
            background-color: #fff;
        }

            .main-omt .omt-header {
                background-color: #32c5d2;
                padding: 4px 7px;
                display: flex;
            }

        .omt-header .header-title {
            font-size: 20px;
            padding: 10px 10px;
            color: white;
            width: 100%;
            margin: 0;
        }

        .omt-header .omt__icon {
            font-size: 24px;
            padding: 8px 10px;
            color: white;
        }

        .omt-top {
            display: flex;
            padding: 20px 20px;
        }

        .category {
            margin: 0 10px;
        }

        .presentation {
            border-color: #999 transparent transparent transparent;
            border-style: solid;
            border-width: 4px 4px 0 4px;
            height: 0;
            left: -15px;
            margin-left: -4px;
            margin-top: -2px;
            position: relative;
            top: 30%;
            width: 0;
        }

        .fixed-table-container {
            width: 98%;
            margin-left: 1%;
        }

        .note {
            margin: 0 0 20px 0;
            padding: 15px 30px 15px 15px;
            width: 100%;
        }

            .note.note-warning {
                background-color: #faeaa9;
                border-color: #f3cc31;
                color: black;
            }

        .bg-blue {
            background: #3598dc !important;
            color: white;
        }

       
    </style>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

