﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="admin_omt_LichCaNhan.aspx.cs" Inherits="admin_page_module_function_module_OMT_admin_omt_LichCaNhan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <link href="../../../admin_css/main.css" rel="stylesheet" />
    <script src="../../../admin_js/main.js"></script>
    <script src="../../../admin_js/locales-all.js"></script>
    <script src="../../../admin_js/vi.js"></script>
    <script>
        function myAccount() {
            document.getElementById('block_content').style.display = "block";
            document.getElementById('block_content1').style.display = "none";
            //document.getElementById("block_content").visible = true;
        }
        function myTeacher() {
            document.getElementById('block_content1').style.display = "block";
            document.getElementById('block_content').style.display = "none";
            //document.getElementById("block_content").visible = true;
        }
        function reTurn() {
            document.getElementById('block-content3').style.display = "block";
        }
        function noReturn() {
            document.getElementById('block-content3').style.display = "none";
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="main-omt">
        <div class="omt-header">
            <i class="fa fa-calendar omt__icon" aria-hidden="true"></i>
            <h4 class="header-title">Quản Lý Lịch</h4>
        </div>
        <div class="portlet-body">
            <div class="row" style="margin-top: 2rem">
                <div class="col-md-7 " style="margin-bottom: 5rem;">
                    <div class="row" style="margin-bottom: 1rem">
                        <div class="col-md-12">
                            <%--<button class="btn btn-outline green" id="modal_add_calendar" data-toggle="modal" data-target=".bd-example-modal-lg" style="margin-right: 1rem">
                                <i class="fas fa-plus"></i>Tạo lịch
                            </button>--%>
                            <button class="btn btn-outline green" id="modal_add_calendar" data-toggle="modal" data-target=".bd-example-modal-lg">
                                <i class="fas fa-plus"></i>Tạo lịch
                            </button>
                            <button id="time_today" class="btn btn-outline purple " style="margin-right: 1rem">Hôm nay</button>
                            <button class="btn btn-outline dark " style="margin-right: 1rem">Syc </button>
                        </div>
                    </div>
                    <div id="external-events" class="row">
                        <!-- chỗ này để lịch -->
                        <div id='calendar'></div>
                        <%--<div id="dncalendar-container" style="border: 1px solid #ddd;">
                            <div id="dncalendar-header" class="dncalendar-header">
                                <h2>Tháng 8 2020</h2>
                                <div id="dncalendar-links" class="dncalendar-links">
                                    <div id="dncalendar-prev-month" class="dncalendar-prev-month"></div>
                                    <div id="dncalendar-next-month" class="dncalendar-next-month"></div>
                                </div>
                            </div>
                            <div id="dncalendar-body" class="dncalendar-body">
                                <table style="width: 100%">
                                    <thead>
                                        <tr>
                                            <td class="holiday">Chủ nhật</td>
                                            <td>Thứ 2</td>
                                            <td>Thứ 3</td>
                                            <td>Thứ 4</td>
                                            <td>Thứ 5</td>
                                            <td>Thứ 6</td>
                                            <td class="holiday">Thứ 7</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class=" holiday  note " data-date="26" data-month="7" data-year="2020">
                                                <div class="entry">26</div>
                                            </td>
                                            <td class=" note " data-date="27" data-month="7" data-year="2020">
                                                <div class="entry">27</div>
                                            </td>
                                            <td class=" note " data-date="28" data-month="7" data-year="2020">
                                                <div class="entry">28</div>
                                            </td>
                                            <td class=" note " data-date="29" data-month="7" data-year="2020">
                                                <div class="entry">29</div>
                                            </td>
                                            <td class=" note " data-date="30" data-month="7" data-year="2020">
                                                <div class="entry">30</div>
                                            </td>
                                            <td class="" data-date="31" data-month="7" data-year="2020">
                                                <div class="entry">31</div>
                                            </td>
                                            <td class=" holiday " data-date="1" data-month="8" data-year="2020">
                                                <div class="entry">1</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class=" holiday " data-date="2" data-month="8" data-year="2020">
                                                <div class="entry">2</div>
                                            </td>
                                            <td class="" data-date="3" data-month="8" data-year="2020">
                                                <div class="entry">3</div>
                                            </td>
                                            <td class="" data-date="4" data-month="8" data-year="2020">
                                                <div class="entry">4</div>
                                            </td>
                                            <td class="" data-date="5" data-month="8" data-year="2020">
                                                <div class="entry">5</div>
                                            </td>
                                            <td class="" data-date="6" data-month="8" data-year="2020">
                                                <div class="entry">6</div>
                                            </td>
                                            <td class="" data-date="7" data-month="8" data-year="2020">
                                                <div class="entry">7</div>
                                            </td>
                                            <td class=" holiday " data-date="8" data-month="8" data-year="2020">
                                                <div class="entry">8</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class=" holiday " data-date="9" data-month="8" data-year="2020">
                                                <div class="entry">9</div>
                                            </td>
                                            <td class="" data-date="10" data-month="8" data-year="2020">
                                                <div class="entry">10</div>
                                            </td>
                                            <td class="" data-date="11" data-month="8" data-year="2020">
                                                <div class="entry">11</div>
                                            </td>
                                            <td class="" data-date="12" data-month="8" data-year="2020">
                                                <div class="entry">12</div>
                                            </td>
                                            <td class="" data-date="13" data-month="8" data-year="2020">
                                                <div class="entry">13</div>
                                            </td>
                                            <td class="" data-date="14" data-month="8" data-year="2020">
                                                <div class="entry">14</div>
                                            </td>
                                            <td class=" holiday " data-date="15" data-month="8" data-year="2020">
                                                <div class="entry">15</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class=" today-date  holiday " data-date="16" data-month="8" data-year="2020">
                                                <div class="entry" data-title="">16</div>
                                            </td>
                                            <td class="" data-date="17" data-month="8" data-year="2020">
                                                <div class="entry">17</div>
                                            </td>
                                            <td class="" data-date="18" data-month="8" data-year="2020">
                                                <div class="entry">18</div>
                                            </td>
                                            <td class="" data-date="19" data-month="8" data-year="2020">
                                                <div class="entry">19</div>
                                            </td>
                                            <td class="" data-date="20" data-month="8" data-year="2020">
                                                <div class="entry">20</div>
                                            </td>
                                            <td class="" data-date="21" data-month="8" data-year="2020">
                                                <div class="entry">21</div>
                                            </td>
                                            <td class=" holiday " data-date="22" data-month="8" data-year="2020">
                                                <div class="entry">22</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class=" holiday " data-date="23" data-month="8" data-year="2020">
                                                <div class="entry">23</div>
                                            </td>
                                            <td class="" data-date="24" data-month="8" data-year="2020">
                                                <div class="entry">24</div>
                                            </td>
                                            <td class="" data-date="25" data-month="8" data-year="2020">
                                                <div class="entry">25</div>
                                            </td>
                                            <td class="" data-date="26" data-month="8" data-year="2020">
                                                <div class="entry">26</div>
                                            </td>
                                            <td class="" data-date="27" data-month="8" data-year="2020">
                                                <div class="entry">27</div>
                                            </td>
                                            <td class="" data-date="28" data-month="8" data-year="2020">
                                                <div class="entry">28</div>
                                            </td>
                                            <td class=" holiday " data-date="29" data-month="8" data-year="2020">
                                                <div class="entry">29</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class=" holiday " data-date="30" data-month="8" data-year="2020">
                                                <div class="entry">30</div>
                                            </td>
                                            <td class="" data-date="31" data-month="8" data-year="2020">
                                                <div class="entry">31</div>
                                            </td>
                                            <td class="" data-date="1" data-month="9" data-year="2020">
                                                <div class="entry">1</div>
                                            </td>
                                            <td class="" data-date="2" data-month="9" data-year="2020">
                                                <div class="entry">2</div>
                                            </td>
                                            <td class="" data-date="3" data-month="9" data-year="2020">
                                                <div class="entry">3</div>
                                            </td>
                                            <td class="" data-date="4" data-month="9" data-year="2020">
                                                <div class="entry">4</div>
                                            </td>
                                            <td class=" holiday " data-date="5" data-month="9" data-year="2020">
                                                <div class="entry">5</div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>--%>
                    </div>
                </div>
                <div class="col-md-5 " style="margin-bottom: 5rem; margin-top: 2rem" id="md-right">

                    <div id="content_list_calendar">
                        <div id="">
                            <div class="portlet-body">
                                <div class="scroller" style="height: 40em;" data-always-visible="1" data-rail-visible1="0" data-handle-color="#D7DCE2">
                                    <div class="todo-container ">
                                        <ul class="todo-projects-container">
                                            <li class="todo-projects-item">
                                                <div class="row">
                                                    <div class="pull-left" style="padding-top: 2rem">
                                                        <p class="day">Thứ 6</p>
                                                        <h3>24</h3>
                                                    </div>
                                                    <div class="col-md-10 col-xs-10 pull-right">
                                                        <div class="row">
                                                            <a href="#" style="text-decoration: none; color: white">
                                                                <div class="todo-project-item-foot" style="padding: 10px">
                                                                    <p class="todo-inline todo-float-r" style="color: white; float: right">Lịch rảnh</p>
                                                                    <p class="todo-inline todo-float-l" style="color: white">
                                                                        <i class="fa fa-clock-o"></i>24-07-2020
                                                                    </p>
                                                                    <p style="color: white;" class="todo-float-l">
                                                                        Lịch rảnh chiều 24/07
                                                                    </p>
                                                                </div>
                                                            </a>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12" style="margin-top: -20px">
                                                                <div class="btn-group pull-left">
                                                                    <a style="color: white;" class="btn"><i class="fa fa-user tooltips" aria-hidden="true" data-toggle="tooltip" data-placement="top" data-original-title="Danh sách đăng kí"></i>
                                                                        1
                                                                    </a>
                                                                    <a style="color: white; padding: 6px 0px;" class="btn"><i class="fa fa-comments tooltips" aria-hidden="true" data-toggle="tooltip" data-placement="top" data-original-title="Lượt comment"></i>
                                                                        0
                                                                    </a>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </li>

                                            <li class="todo-projects-item">
                                                <div class="row">
                                                    <div class=" pull-right" style="background-color: #666600;">
                                                        <div class="row" style="padding: 10px">
                                                            <a class="todo-project-item-foot" style="text-decoration: none; color: white">
                                                                <p class="todo-inline todo-float-r" style="color: white; float: right">Công việc</p>
                                                                <p class="todo-inline todo-float-l" style="color: white">
                                                                    <i class="fa fa-clock-o" aria-hidden="true"></i>
                                                                    23/07/2020 - 30/07/2020
                                                                </p>
                                                                <p class="todo-float-l" style="color: white">
                                                                    Chuẩn bị cho ngày khai giảng năm học mới
                                                                </p>
                                                            </a>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-12 " style="margin-top: -20px">
                                                                <div class="btn-group pull-left">
                                                                    <a style="color: white;" class="btn"><i class="fa fa-user tooltips" aria-hidden="true" data-toggle="tooltip" data-placement="top" data-original-title="Danh sách đăng kí"></i>
                                                                        3
                                                                    </a>
                                                                    <a style="color: white; padding: 6px 0px;" class="btn"><i class="fa fa-comments tooltips" aria-hidden="true" data-toggle="tooltip" data-placement="top" data-original-title="Lượt comment"></i>
                                                                        0
                                                                    </a>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>

                                        </ul>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>

    </div>
    <%--<div class="calendar__omt">
        <div class="container">
            <div id='calendar'></div>
        </div>
    </div>--%>
    <!-- Large modal -->


    <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <img src="/admin_images/close15-512.png" style="width: 9px; height: 9px" alt="" />
                    </button>
                    <ul class="tabs">
                        <li class="tab-link current" data-tab="tab-1">Lịch rãnh</li>
                        <li class="tab-link" data-tab="tab-2">Cuộc hẹn</li>
                        <li class="tab-link" data-tab="tab-3">Lời nhắc</li>
                        <%--<li class="tab-link" data-tab="tab-4">Tab Four</li>--%>
                    </ul>
                </div>
                <div class="modal-body">
                    <div id="load_schedule"></div>
                    <div id="div_overlay_process"></div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <input id="title" type="text" class="form-control" />
                        <label for="form_control_1">Tiêu đề</label>
                    </div>
                    <div class="form-group form-md-line-input form-md-floating-label">
                        <textarea class="form-control" style="margin: 0px -129px 0px 0px; width: 100%; height: 50px;" id="content"></textarea>
                        <label for="form_control_1">Ghi chú(không bắt buộc)</label>
                    </div>
                    <form class="form-horizontal form-bordered">
                        <input type="hidden" id="calendar_type" value="1" />

                    </form>

                </div>
                <div class="container">
                    <div id="tab-1" class="tab-content current">
                        <div class="form-group">
                            <div class="row">
                                <label class="control-label col-md-2 col-xs-2">Từ</label>
                                <div class="col-md-2 col-xs-5">
                                    <input id="start_date_day_schedule" class="green form-control form-control-inline " style="width: 140px; font-size: 0.75rem" type="date" />
                                </div>
                                <div class="col-md-2 col-xs-5">

                                    <div class="col-10">
                                        <input class="form-control timepicker" type="time" value="13:45:00" id="example-time-input4" />
                                    </div>

                                </div>
                                <label class="control-label col-md-2 col-xs-2" style="text-align: center;">đến</label>
                                <div class="col-md-2 col-xs-5">
                                    <input id="end_date_day_schedule" class="green form-control form-control-inline date-picker-back-date" style="width: 140px; font-size: 0.75rem" size="16" type="date" />
                                </div>
                                <div class="col-md-2 col-xs-5">

                                    <div class="col-10">
                                        <input class="form-control timepicker" type="time" value="13:45:00" id="example-time-input3" />
                                    </div>

                                </div>

                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="control-label col-md-2" style="margin-top: 15px">Phạm vi</label>
                                <div class="col-md-3">
                                    <ul class="nav nav-tabs range">
                                        <li class="nav-item dropdown">
                                            <a class="nav-link dropdown-toggle title" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Lớp Chủ Nhiệm</a>
                                            <div class="dropdown-menu range">
                                                <a class="teacher" href="javasrcipt:voide(0)" onclick="myTeacher()">Lớp Chủ Nhiệm</a>
                                                <a class="account" href="javasrcipt:voide(0)" onclick="myAccount()">tài khoản</a>
                                            </div>
                                        </li>
                                    </ul>

                                </div>


                                <div class="col-md-7">
                                    <form>
                                        <div class="form-row align-items-center account" id="block_content1">
                                            <div class="col-auto">

                                                <select class="custom-select mr-sm-2" style="width: 100%" id="inlineFormCustomSelect">
                                                    <option selected="selected">lớp...</option>
                                                    <option value="1">1B1</option>
                                                    <option value="2">2B1</option>
                                                    <option value="3">3B1</option>
                                                </select>
                                            </div>
                                        </div>
                                    </form>

                                    <div class="col-md-7" style="display: none">

                                        <div id="load_school">

                                            <select required class=" form-control" style="margin-top: 15px" id="object_ids" data-size="8" multiple="multiple" name="object_ids[]">
                                                <option value="3292">Nguyễn Tùng Anh [OMT-011 - 1B1 - 16/02/2008]</option>
                                                <option value="3294">Phạm Hoàng Anh [OMT-012 - 1B1 - 05/11/2008]</option>
                                                <option value="3296">Phạm Thị Tuyết Anh [OMT-013 - 1B1 - 19/09/2008]</option>
                                                <option value="3298">Phạm Trâm Anh [OMT-014 - 1B1 - 16/07/2008]</option>
                                                <option value="3300">Trần Đức Anh [OMT-015 - 1B1 - 12/12/2008]</option>
                                                <option value="3302">Lê Minh Bảo Anh [OMT-016 - 1B1 - 25/05/2008]</option>
                                                <option value="3304">Lại Viết Hoàng Anh [OMT-017 - 1B1 - 18/04/2008]</option>
                                                <option value="3306">Lê Trang Anh [OMT-018 - 1B1 - 27/10/2008]</option>
                                                <option value="3308">Đào Phương Anh [OMT-019 - 1B1 - 10/12/2008]</option>
                                                <option value="3310">Lê Sơn Bách [OMT-020 - 1B1 - 07/01/2008]</option>
                                                <option value="3291">Nguyễn Tuấn Anh [FT011]</option>
                                                <option value="3297">Phạm Văn Hiệu [FT014]</option>
                                                <option value="3299">Trần Đức Thành [FT015]</option>
                                                <option value="3307">Đào Văn Sang [FT019]</option>
                                                <option value="3309">Lê Ngọc Tân [FT020]</option>
                                                <option value="3293">Hoàng Thị Thanh Mai [MT009]</option>
                                                <option value="3295">Quách Thị Ánh Tuyết [MT010]</option>
                                                <option value="3301">Lê Thị Thu Trang [MT011]</option>
                                                <option value="3303">Nguyễn Thị Thu [MT012]</option>
                                                <option value="3305">Nguyễn Thùy Giang [MT013]</option>
                                                <option value="3471">Lương Văn Nguyên [omt-gv02@so.edu.vn - 0906080810 - nguyenlv@gmail.com]</option>
                                                <option value="3015">Nguyễn Xuân Ánh [anhnx - anhnx@so.edu.vn]</option>
                                                <option value="3500">Phạm Hương Thu [omt-gv31@so.edu.vn - omt-gv31@so.edu.vn]</option>
                                                <option value="3501">Phùng Thị Thúy Hằng [omt-gv32@so.edu.vn - omt-gv32@so.edu.vn]</option>
                                                <option value="3503">Nguyễn Phương Anh [omt-gv34@so.edu.vn - omt-gv34@so.edu.vn]</option>
                                                <option value="3502">Nguyễn Thanh Thảo [omt-gv33@so.edu.vn - omt-gv33@so.edu.vn]</option>
                                                <option value="3498">Hoàng Trọng Nghĩa [omt-gv29@so.edu.vn - omt-gv29@so.edu.vn]</option>
                                                <option value="3499">Nguyễn Thị Thanh Hương [omt-gv30@so.edu.vn - omt-gv30@so.edu.vn]</option>
                                            </select>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <form>
                                        <div class="form-row align-items-center account" id="block_content">
                                            <div class="col-auto ">
                                                <select class="custom-select mr-sm-2" style="width: 100%" id="inlineFormCustomSelect2">
                                                    <option selected="selected">Tài Khoản...</option>
                                                    <option value="3292">Nguyễn Tùng Anh [OMT-011 - 1B1 - 16/02/2008]</option>
                                                    <option value="3294">Phạm Hoàng Anh [OMT-012 - 1B1 - 05/11/2008]</option>
                                                    <option value="3296">Phạm Thị Tuyết Anh [OMT-013 - 1B1 - 19/09/2008]</option>
                                                    <option value="3298">Phạm Trâm Anh [OMT-014 - 1B1 - 16/07/2008]</option>
                                                    <option value="3300">Trần Đức Anh [OMT-015 - 1B1 - 12/12/2008]</option>
                                                    <option value="3302">Lê Minh Bảo Anh [OMT-016 - 1B1 - 25/05/2008]</option>
                                                    <option value="3304">Lại Viết Hoàng Anh [OMT-017 - 1B1 - 18/04/2008]</option>
                                                    <option value="3306">Lê Trang Anh [OMT-018 - 1B1 - 27/10/2008]</option>
                                                    <option value="3308">Đào Phương Anh [OMT-019 - 1B1 - 10/12/2008]</option>
                                                    <option value="3310">Lê Sơn Bách [OMT-020 - 1B1 - 07/01/2008]</option>
                                                    <option value="3291">Nguyễn Tuấn Anh [FT011]</option>
                                                    <option value="3297">Phạm Văn Hiệu [FT014]</option>
                                                    <option value="3299">Trần Đức Thành [FT015]</option>
                                                    <option value="3307">Đào Văn Sang [FT019]</option>
                                                    <option value="3309">Lê Ngọc Tân [FT020]</option>
                                                    <option value="3293">Hoàng Thị Thanh Mai [MT009]</option>
                                                    <option value="3295">Quách Thị Ánh Tuyết [MT010]</option>
                                                    <option value="3301">Lê Thị Thu Trang [MT011]</option>
                                                    <option value="3303">Nguyễn Thị Thu [MT012]</option>
                                                    <option value="3305">Nguyễn Thùy Giang [MT013]</option>
                                                </select>
                                            </div>


                                        </div>
                                    </form>
                                </div>

                                <div id="load_user_fix">
                                </div>

                            </div>
                        </div>
                        <div id="subject_hide">
                            <div class="form-group">
                                <div class="row">
                                    <label class="control-label col-md-2" style="margin-top: 15px">Đối Tượng</label>
                                    <div class="col-md-10">
                                        <div class="form-check form-check-inline form-check__omt">
                                            <input class="form-check-input" type="checkbox" name="inlineCheckbox1" id="inlineRadio1" value="option1" />
                                            <label class="form-check-label" for="inlineRadio1">Học Sinh</label>
                                        </div>
                                        <div class="form-check form-check-inline form-check__omt">
                                            <input class="form-check-input" type="checkbox" name="inlineCheckbox2" id="inlineRadio2" value="option2" />
                                            <label class="form-check-label" for="inlineRadio2">Phụ Huynh</label>
                                        </div>
                                        <div class="form-check form-check-inline form-check__omt">
                                            <input class="form-check-input" type="checkbox" name="inlineCheckbox3" id="inlineRadio3" value="option3" />
                                            <label class="form-check-label" for="inlineRadio3">Giáo viên chủ nhiệm</label>
                                        </div>
                                        <div class="form-check form-check-inline form-check__omt">
                                            <input class="form-check-input" type="checkbox" name="inlineCheckbox4" id="inlineRadio4" value="option4" />
                                            <label class="form-check-label" for="inlineRadio4">Giáo viên</label>
                                        </div>
                                        <div class="form-check form-check-inline form-check__omt">
                                            <input class="form-check-input" type="checkbox" name="inlineCheckbox5" id="inlineRadio5" value="option5" />
                                            <label class="form-check-label" for="inlineRadio5">Nhân viên</label>
                                        </div>
                                        <div class="form-check form-check-inline form-check__omt" style="margin-left: 0">
                                            <input class="form-check-input" type="checkbox" name="inlineCheckbox6" id="inlineRadio6" value="option6" />
                                            <label class="form-check-label" for="inlineRadio6">Lớp Trưởng</label>
                                        </div>
                                        <div class="form-check form-check-inline form-check__omt">
                                            <input class="form-check-input" type="checkbox" name="inlineCheckbox7" id="inlineRadio7" value="option7" />
                                            <label class="form-check-label" for="inlineRadio7">Trưởng ban PH</label>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <label class="control-label col-md-2 col-xs-2 ">Tùy chỉnh</label>
                                <div class="col-md-2 col-xs-10">
                                    <select name="is_repeat_schedule" id="is_repeat_schedule" class="form-control green">
                                        <option value="0">Không lặp</option>
                                        <option value="1">Lặp lại</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="tab-2" class="tab-content">
                    <div class="form-group">
                        <div class="row">
                            <label class="control-label col-md-2 col-xs-2">Từ</label>
                            <div class="col-md-2 col-xs-5">
                                <input class="form-control form-control-inline " style="width: 140px; font-size: 0.75rem" size="16" type="date" />
                            </div>
                            <div class="col-md-2 col-xs-5">

                                <div class="col-10">
                                    <input class="form-control timepicker" type="time" value="13:45:00" id="example-time-input2" />
                                </div>

                            </div>
                            <label class="control-label col-md-2 col-xs-2" style="text-align: center;">đến</label>
                            <div class="col-md-2 col-xs-5">
                                <input class=" form-control form-control-inline date-picker-back-date" style="width: 140px; font-size: 0.75rem" size="16" type="date" />
                            </div>
                            <div class="col-md-2 col-xs-5">

                                <div class="col-10">
                                    <input class="form-control timepicker" type="time" value="13:45:00" id="example-time-input1" />
                                </div>

                            </div>

                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label class="control-label people-recive col-md-2 col-xs-2" style="text-align: left;">Người nhận hẹn</label>
                            <div class="col-md-10">
                                <form>
                                    <div class="form-row align-items-center people-recive">
                                        <div class="col-auto ">
                                            <select class="custom-select mr-sm-2" style="width: 100%" id="inlineFormCustomSelect3">
                                                <option selected="selected">Người Nhận Hẹn...</option>
                                                <option value="3292">Nguyễn Tùng Anh [OMT-011 - 1B1 - 16/02/2008]</option>
                                                <option value="3294">Phạm Hoàng Anh [OMT-012 - 1B1 - 05/11/2008]</option>
                                                <option value="3296">Phạm Thị Tuyết Anh [OMT-013 - 1B1 - 19/09/2008]</option>
                                                <option value="3298">Phạm Trâm Anh [OMT-014 - 1B1 - 16/07/2008]</option>
                                                <option value="3300">Trần Đức Anh [OMT-015 - 1B1 - 12/12/2008]</option>
                                                <option value="3302">Lê Minh Bảo Anh [OMT-016 - 1B1 - 25/05/2008]</option>
                                                <option value="3304">Lại Viết Hoàng Anh [OMT-017 - 1B1 - 18/04/2008]</option>
                                                <option value="3306">Lê Trang Anh [OMT-018 - 1B1 - 27/10/2008]</option>
                                                <option value="3308">Đào Phương Anh [OMT-019 - 1B1 - 10/12/2008]</option>
                                                <option value="3310">Lê Sơn Bách [OMT-020 - 1B1 - 07/01/2008]</option>
                                                <option value="3291">Nguyễn Tuấn Anh [FT011]</option>
                                                <option value="3297">Phạm Văn Hiệu [FT014]</option>
                                                <option value="3299">Trần Đức Thành [FT015]</option>
                                                <option value="3307">Đào Văn Sang [FT019]</option>
                                                <option value="3309">Lê Ngọc Tân [FT020]</option>
                                                <option value="3293">Hoàng Thị Thanh Mai [MT009]</option>
                                                <option value="3295">Quách Thị Ánh Tuyết [MT010]</option>
                                                <option value="3301">Lê Thị Thu Trang [MT011]</option>
                                                <option value="3303">Nguyễn Thị Thu [MT012]</option>
                                                <option value="3305">Nguyễn Thùy Giang [MT013]</option>
                                                <option value="3471">Lương Văn Nguyên [omt-gv02@so.edu.vn - 0906080810 - nguyenlv@gmail.com]</option>
                                                <option value="3015">Nguyễn Xuân Ánh [anhnx - anhnx@so.edu.vn]</option>
                                                <option value="3500">Phạm Hương Thu [omt-gv31@so.edu.vn - omt-gv31@so.edu.vn]</option>
                                                <option value="3501">Phùng Thị Thúy Hằng [omt-gv32@so.edu.vn - omt-gv32@so.edu.vn]</option>
                                                <option value="3503">Nguyễn Phương Anh [omt-gv34@so.edu.vn - omt-gv34@so.edu.vn]</option>
                                                <option value="3502">Nguyễn Thanh Thảo [omt-gv33@so.edu.vn - omt-gv33@so.edu.vn]</option>
                                                <option value="3498">Hoàng Trọng Nghĩa [omt-gv29@so.edu.vn - omt-gv29@so.edu.vn]</option>
                                                <option value="3499">Nguyễn Thị Thanh Hương [omt-gv30@so.edu.vn - omt-gv30@so.edu.vn]</option>
                                            </select>
                                        </div>


                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2 col-xs-2"></div>
                        </div>
                        <div class="row">
                            <div class="col-md-5 col-xs-10">
                                <div class="mt-checkbox-inline">
                                    <label class="mt-checkbox mt-checkbox-outline" id="abc1b">
                                        <input type="checkbox" id="request_confirmation" value="0" />

                                        <span></span>Bắt buộc xác nhận
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="tab-3" class="tab-content">
                    <div class="form-group">
                        <div class="row">
                            <label class="control-label col-md-2 col-xs-2" style="text-align: left;">Thời gian</label>
                            <div class="col-md-2 col-xs-5">
                                <input class=" form-control form-control-inline " style="width: 140px; font-size: 0.75rem" type="date" />
                            </div>
                            <div class="col-md-2 col-xs-5">

                                <div class="col-10">
                                    <input class="form-control timepicker" type="time" value="13:45:00" id="example-time-input" />
                                </div>

                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="control-label custom col-md-2 col-xs-2">Tùy chỉnh</label>
                                <div class="col-md-2 col-xs-5">
                                    <ul class="nav nav-tabs range">
                                        <li class="nav-item dropdown">
                                            <a class="nav-link dropdown-toggle title" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Lặp Lại</a>
                                            <div class="dropdown-menu range">
                                                <a class="return" href="javasrcipt:voide(0)" onclick="reTurn()">Lặp lại</a>
                                                <a class="noreturn" href="javasrcipt:voide(0)" onclick="noReturn()">Không lặp lại</a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="form-group" id="block-content3" style="display: none;">
                            <div class="row">
                                <label class="control-label  col-md-2 col-xs-2">Lặp lại mỗi</label>
                                <div class="col-md-2 col-xs-5">
                                    <input min="1" value="1" type="number" class="form-control return blue" id="number_repeat_number_prompt" />
                                </div>
                                <div class="col-md-2 col-xs-5">
                                    <select name="country" class="form-control blue" id="number_repeat_type_prompt">
                                        <option value="1">Ngày</option>
                                        <option value="2">Tuần</option>
                                        <option value="3">Tháng</option>
                                        <option value="4">Năm</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group" style="display: none;" id="block-content3">
                            <div class="row" id="ssh2" style="display: none;">
                                <label class="control-label col-md-2 col-xs-2" style="text-align: left;">Lặp lại vào</label>
                                <div class="col-md-10 col-xs-10">
                                    <div class="mt-checkbox-inline">
                                        <label class="mt-checkbox mt-checkbox-outline" id="abcz1">
                                            <input type="checkbox" class="day_in_week_prompt" name="day_in_week_prompt[]" value="2" />
                                            <span></span>Thứ 2
                                        </label>
                                        <label class="mt-checkbox mt-checkbox-outline" id="abcz2">
                                            <input type="checkbox" class="day_in_week_prompt" name="day_in_week_prompt[]" value="3" />
                                            <span></span>Thứ 3
                                        </label>
                                        <label class="mt-checkbox mt-checkbox-outline" id="abcz3">
                                            <input type="checkbox" class="day_in_week_prompt" name="day_in_week_prompt[]" value="4" />
                                            <span></span>Thứ 4
                                        </label>
                                        <label class="mt-checkbox mt-checkbox-outline" id="abcz4">
                                            <input type="checkbox" class="day_in_week_prompt" name="day_in_week_prompt[]" value="5" />
                                            <span></span>Thứ 5
                                        </label>
                                        <label class="mt-checkbox mt-checkbox-outline" id="abcz5">
                                            <input type="checkbox" class="day_in_week_prompt" name="day_in_week_prompt[]" value="6" />
                                            <span></span>Thứ 6
                                        </label>
                                        <label class="mt-checkbox mt-checkbox-outline" id="abcz6">
                                            <input type="checkbox" class="day_in_week_prompt" name="day_in_week_prompt[]" value="7" />
                                            <span></span>Thứ 7
                                        </label>
                                        <label class="mt-checkbox mt-checkbox-outline" id="abcz7">
                                            <input type="checkbox" class="day_in_week_prompt" name="day_in_week_prompt[]" value="8" />
                                            <span></span>Chủ nhật
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <label class="control-label col-md-2 col-xs-2" style="text-align: left;">Kết thúc</label>
                                <div class="col-md-2 col-xs-5">
                                    <select id="number_appear_prompt" class="form-control blue">
                                        <option value="1">Không</option>
                                        <option value="2">Ngày</option>
                                        <option value="3">Sau</option>
                                    </select>
                                    <form action="#"></form>
                                </div>
                                <div class="col-md-2 col-xs-5">
                                    <input class="form-control daypicker form-control-inline " style="width: 140px; font-size: 0.75rem" type="date" />
                                </div>
                                <div class="col-md-2 col-xs-5" id="xyz5b" style="display: none;">
                                    <input id="number_time_slot_prompt" min="1" value="5" class="blue form-control" type="number" />
                                </div>
                                <div class="col-md-3 col-xs-5" id="xyz6b" style="display: none;">
                                    <label class="control-label">lần xuất hiện</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="modal-footet__title" style="font-size: 12px; color: #8b130f; padding-bottom: 2px">Lưu ý: Ngày kết thúc là ngày sự kiện/lịch kết thúc hoàn toàn</div>
                    <button id="submit_schedule" type="button" class="btn btn-outline-primary">Lưu</button>
                </div>
            </div>
        </div>

    </div>

    <style>
        ul.tabs {
            margin: 0px;
            padding: 0px;
            list-style: none;
        }

            ul.tabs li {
                background: none;
                color: #222;
                display: inline-block;
                padding: 10px 15px;
                cursor: pointer;
            }

                ul.tabs li.current {
                    background: #ededed;
                    color: #222;
                }

        .tab-content {
            display: none;
            background: #fff;
            padding: 15px;
        }

            .tab-content.current {
                display: inherit;
                animation: feadIt 0.10s linear;
            }

        @keyframes feadIt {
            0% {
                opacity: 0;
            }

            100% {
                opacity: 1;
            }
        }

        .main-omt {
            border: 1px solid #32c5d2;
            background-color: #fff;
        }

            .main-omt .omt-header {
                background-color: #32c5d2;
                padding: 4px 7px;
                display: flex;
            }

        .omt-header .header-title {
            font-size: 20px;
            padding: 10px 10px;
            color: white;
        }

        .omt-header .omt__icon {
            font-size: 30px;
            padding: 8px 10px;
            color: white;
        }

        .btn {
            display: inline-block;
            font-size: 13px;
            font-weight: 600;
            border: 1px solid transparent;
        }

            .btn.btn-outline.green {
                border-color: #32c5d2;
                color: #32c5d2;
                background: none;
                outline: none;
                font-weight: 600;
            }

                .btn.btn-outline.green:hover {
                    background-color: #32c5d2;
                    color: white;
                    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.1), 0 1px 2px rgba(0, 0, 0, 0.18);
                }

            .btn.btn-outline.purple {
                border-color: purple;
                color: purple;
                background: none;
                outline: none;
                font-weight: 600;
                margin-left: 1rem;
            }

                .btn.btn-outline.purple:hover {
                    background-color: purple;
                    color: white;
                    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.1), 0 1px 2px rgba(0, 0, 0, 0.18);
                }

            .btn.btn-outline.dark {
                border-color: black;
                color: black;
                background: none;
                outline: none;
                font-weight: 600;
            }

                .btn.btn-outline.dark:hover {
                    background-color: black;
                    color: white;
                    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.1), 0 1px 2px rgba(0, 0, 0, 0.18);
                }

        .dncalendar-body tbody td .entry {
            padding: 11px 0 13px;
            position: relative;
        }

        .dncalendar-body table td.holiday {
            color: #c55 !important;
            text-align: center;
        }

        .dncalendar-body tbody tr td {
            width: 13%;
            border-top: 1px solid #ddd;
            color: #bdbdbd;
            background: #f8f8f8;
            text-decoration: none;
            vertical-align: middle;
            text-align: center;
        }

        .row {
            margin-right: 0px;
            margin-left: 0px;
            margin-top: 20px;
        }

        .dncalendar-header h2 {
            color: #434343;
            font-size: 20px;
            line-height: 1;
            text-align: left;
            font-weight: bold;
            padding-left: 10px;
            padding-top: 10px
        }

        .dncalendar-header .dncalendar-links {
            height: 19px;
            width: 100%;
            position: absolute;
            top: 18px;
            right: 0;
        }

            .dncalendar-header .dncalendar-links .dncalendar-prev-month {
                position: absolute;
                right: 70px;
                top: 70px;
                background: url('/admin_images/left.jpg') no-repeat 0 0;
                background-size: 15px 15px;
            }

            .dncalendar-header .dncalendar-links div {
                cursor: pointer;
                width: 35px;
                height: 35px;
                -webkit-user-select: none;
                touch-action: pan-y;
                -webkit-user-drag: none;
                -webkit-tap-highlight-color: rgba(0,0,0,0);
            }

            .dncalendar-header .dncalendar-links .dncalendar-next-month {
                position: absolute;
                right: 40px;
                top: 70px;
                background: url('/admin_images/right.png') no-repeat 0 0;
                background-size: 15px 15px;
            }

        .todo-container .todo-projects-container {
            padding: 0;
        }

            .todo-container .todo-projects-container > li {
                list-style: none;
                padding: 30px 20px;
            }

        .todo-projects-item {
            border: 1px solid #ebf0f5;
        }

        .todo-container .todo-projects-item:hover {
            background-color: #fafbfc;
            cursor: pointer;
        }

        .pull-right {
            margin-left: 22px;
            background-color: orange;
        }

        .modal .modal-dialog {
            z-index: 10051;
        }

        .modal .modal-content {
            border-radius: 2px;
            border: 0;
            margin-left: 20px;
        }

        .modal .modal-header {
            border-bottom: 1px solid #EFEFEF;
            background-color: white !important;
        }

            .modal .modal-header .close {
                margin-top: 0px !important;
            }

        .nav-tabs {
            margin-bottom: 10px;
            border: none;
        }

            .nav-tabs > li > a,
            .nav-pills > li > a {
                font-size: 12px;
            }

            .nav-tabs > li.active > a, .nav-pills > li.active > a:focus, .nav-pills > li.active > a:hover {
                color: #fff;
                background-color: #337ab7;
                text-decoration: none;
                display: inline-block;
            }

        .nav > li > a {
            padding: 10px 15px;
        }

        .nav-tabs > li {
            float: left;
            margin-left: 2px;
        }

        .nav > li > a:focus, .nav > li > a:hover {
            text-decoration: none;
            background-color: #eee;
            color: #23527c;
        }

        .nav > li > a:focus, .nav > li > a {
            text-decoration: none;
            color: #23527c;
        }

        button.close {
            padding: 0;
            cursor: pointer;
            background: 0 0;
            border: 0;
            -webkit-appearance: none;
            outline: none;
        }

        .close {
            display: inline-block;
            margin-top: 0px;
            margin-right: 0px;
            width: 9px;
            height: 9px;
            background-repeat: no-repeat !important;
        }

        .modal .modal-body {
            border-radius: 2px;
            border: 0;
        }

        .modal-body {
            position: relative;
            padding: 15px;
        }

        .form-group.form-md-line-input {
            position: relative;
            margin: 0 0 35px 0;
            padding-top: 20px;
        }

            .form-group.form-md-line-input .form-control {
                background: none;
                border: 0;
                border-bottom: 1px solid #c2cad8;
                border-radius: 0;
                color: #555555;
                box-shadow: none;
                padding-left: 0;
                padding-right: 0;
                font-size: 12px;
            }

        .form-control {
            outline: none !important;
            box-shadow: none !important;
            width: 100%;
            height: 34px;
            padding: 6px 12px;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        }

        .form-group.form-md-line-input.form-md-floating-label .form-control ~ label {
            font-size: 14px;
            top: 25px;
            transition: 0.2s ease all;
            color: #999;
        }

        .form-group.form-md-line-input.form-md-floating-label .form-control[readonly] ~ label, .form-group.form-md-line-input.form-md-floating-label .form-control.edited ~ label, .form-group.form-md-line-input.form-md-floating-label .form-control.form-control-static ~ label, .form-group.form-md-line-input.form-md-floating-label .form-control:focus:not([readonly]) ~ label, .form-group.form-md-line-input.form-md-floating-label .form-control.focus:not([readonly]) ~ label {
            top: 0;
            font-size: 11px;
        }

        .form-group.form-md-line-input .form-control ~ label, .form-group.form-md-line-input .form-control ~ .form-control-focus {
            width: 100%;
            position: absolute;
            left: 0;
            bottom: 0;
            pointer-events: none;
        }

            .form-group.form-md-line-input .form-control ~ label, .form-group.form-md-line-input .form-control ~ .form-control-focus:focus {
                position: absolute;
                top: 15px;
            }

        .form-control.green {
            border-color: #32c5d2;
        }

        .select2-container--bootstrap .select2-selection--multiple .select2-selection__choice {
            color: #555;
            background: #fff;
            border: 1px solid #ccc;
            border-radius: 4px;
            cursor: default;
            float: left;
            margin: 5px 0 0 6px;
            padding: 0 6px;
        }

        .form-check__omt .form-check-label {
            padding-left: 0.7rem;
        }

        .modal-footer .modal-footet__title {
            text-align: left;
        }

        .btn.btn-outline-primary {
            border: 1px solid #0275d8;
            outline: none !important;
        }

        .form-row.align-items-center.account {
            margin-left: -10px;
            margin-top: 10px;
            display: none;
        }

        .dropdown-menu.range {
            min-width: 9rem;
        }

        .nav.nav-tabs.range {
            border: none;
            margin-top: 10px;
        }

        .dropdown-menu.range a {
            text-decoration: none;
            padding: 2px 18px;
            color: black;
            font-weight: 600;
            font-size: 15px;
        }

        .dropdown-menu.range .account {
            padding-left: 18px;
        }

        .nav.nav-tabs.range a:hover {
            color: red;
        }

        .nav > li > a:focus, .nav > li > a:hover {
            background-color: white !important;
        }

        .nav-tabs .nav-link {
            border: none;
        }

        .nav-link.dropdown-toggle.title {
            font-size: 15px;
        }

        .tabbable-line ul li.active a:hover {
            color: blue !important;
        }

        /*            .nav > li > a:focus, .nav > li > a:hover {
                background-color: white !important;
            }*/

        .form-row.align-items-center.people-recive {
            margin-top: 10px;
        }

        .control-label.people-recive {
            margin-top: 20px;
        }

        .mt-checkbox.mt-checkbox-outline {
            margin-left: 157px;
        }

        .form-control.timepicker {
            width: 140px;
        }

        .form-group .row [class^='col'] {
            padding-left: 2px;
            padding-right: 10px;
        }

        .control-label.custom {
            text-align: left;
            margin-top: 20px;
        }

        .form-control.return.blue {
            text-align: left;
            height: 38px !important;
        }

        .form-control.daypicker {
            height: 38px;
        }

        .tabs .tab-link.current {
            background-color: #337ab7;
            color: white;
        }

        /*#calendar {
            max-width: 1100px;
            margin: 40px auto;
        }*/
    </style>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
    <script>
        $(document).ready(function () {

            $('ul.tabs li').click(function () {
                var tab_id = $(this).attr('data-tab');

                $('ul.tabs li').removeClass('current');
                $('.tab-content').removeClass('current');

                $(this).addClass('current');
                $("#" + tab_id).addClass('current');
            })

        });
    </script>
    <script>
        document.addEventListener('DOMContentLoaded', function () {
            var calendarEl = document.getElementById('calendar');

            var calendar = new FullCalendar.Calendar(calendarEl, {
                initialView: 'dayGridMonth',
                initialDate: '2020-09-07',
                locale: 'vi',
                schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
                headerToolbar: {
                    left: 'prev,next today',
                    center: 'title',
                    right: false
                },
            });

            calendar.render();
        });
    </script>

</asp:Content>

