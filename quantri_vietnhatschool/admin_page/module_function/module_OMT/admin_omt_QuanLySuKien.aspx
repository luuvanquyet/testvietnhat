﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="admin_omt_QuanLySuKien.aspx.cs" Inherits="admin_page_module_function_module_OMT_admin_omt_QuanLySuKien" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="main-omt">
        <div class="omt-header">
            <i class="fa fa-list omt__icon" aria-hidden="true"></i>
            <h4 class="header-title">Quản Lý Sự Kiện</h4>
        </div>
        <div class="omt-top">
            <form class="form-inline  my-2 my-lg-0">
                <input class="form-control search mr-sm-2" type="search" placeholder="Tìm mã,tên..." aria-label="Search" />
            </form>
            <div class="form-check regyster form-check-inline">
                <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1" />
                <label class="form-check-label" for="inlineCheckbox1">Cho Phép Đăng Kí</label>
            </div>
            <div class="col-md-1" style="padding-left: 5px; padding-right: 5px">
                <a href="admin_omt_AddSuKienLichCaNhan.aspx" class="btn btn-primary" style="outline:none"> Thêm mới</a>
            </div>
            <div class="col-md-1" style="padding-left: 5px; padding-right: 5px; margin-left: 35px">
                <button type="button" class="btn bg-green-jungle font-white" style="background-color: #26C281; color: white; outline: none">
                    <i class="fa fa-file-excel-o font-white">Tải Về</i>
                </button>

            </div>
        </div>
        <div class="fixed-table-container" style="width: 98%; margin-left: 1%" id="data-items">
            <div>
                Tổng số hồ sơ: <b>10</b>
            </div>
            <table class="table table-bordered table-striped table-condensed flip-content">
                <thead class="flip-content bg-blue bg-font-blue">
                    <tr>
                        <th style="vertical-align: middle; text-align: center; width: 10%"></th>
                        <th style="vertical-align: middle; text-align: center; width: 20%">Tên sự kiện </th>
                        <th style="vertical-align: middle; text-align: center; width: 15%">Phạm vi </th>
                        <th style="vertical-align: middle; text-align: center; width: 10%">Thời gian</th>
                        <th style="vertical-align: middle; text-align: center; width: 10%">Thông tin đăng ký</th>
                        <th style="vertical-align: middle; text-align: center; width: 10%">Trạng thái </th>
                        <th style="vertical-align: middle; text-align: center; width: 7%">Chủ động gửi</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td colspan="8">
                            <div class="note note-warning">
                                <p>Không tim thấy thông tin  sự kiện trên hệ thống. Vui lòng khai báo Sự kiện </p>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <style>
        .main-omt {
            border: 1px solid #32c5d2;
            background-color: #fff;
        }

            .main-omt .omt-header {
                background-color: #32c5d2;
                padding: 4px 7px;
                display: flex;
            }

        .omt-header .header-title {
            font-size: 20px;
            padding: 10px 10px;
            color: white;
            width: 100%;
        }

        .omt-header .omt__icon {
            font-size: 30px;
            padding: 8px 10px;
            color: white;
        }

        .omt-top {
            display: flex;
            padding: 20px 20px;
        }

        .form-control.search {
            width: 20%;
        }

        .form-check.regyster.form-check-inline {
            margin-left: 20px;
            margin-right: 20PX;
            padding-top: 10px;
        }



        .btn.btn-primary.update {
            margin-left: 2px;
        }

        .btn.btn-danger.delete {
            width: 121px;
            margin-left: 2px;
        }

        .note {
            margin: 0 0 20px 0;
            padding: 15px 30px 15px 15px;
            width: 100%;
        }

            .note.note-warning {
                background-color: #faeaa9;
                border-color: #f3cc31;
                color: black;
            }

        .bg-blue {
            background: #3598dc !important;
            color:white;
        }
    </style>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

