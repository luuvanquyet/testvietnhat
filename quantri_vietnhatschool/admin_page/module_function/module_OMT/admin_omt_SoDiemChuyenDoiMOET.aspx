﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="admin_omt_SoDiemChuyenDoiMOET.aspx.cs" Inherits="admin_page_module_function_module_OMT_admin_omt_SoDiemChuyenDoiMOET" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="main-omt">
        <div class="omt-header">
            <i class="fa fa-calculator omt__icon" aria-hidden="true"></i>
            <h4 class="header-title">Sổ điểm lớp học</h4>
        </div>
        <div class="portlet_body">
            <div class="omt-top">
                <div class="col-md-3" style="padding: 0 5px 0 5px">
                    <div class="note note-warning font-red" style="margin: 0 0 5px 0;color:red">
                        Giai đoạn: <b>Năm học 2020 - 2021  &gt;  Giữa HK1</b>
                    </div>
                </div>
                <div class="col-auto grade">
                    <select class="custom-select mr-sm-2" style="width: 100%">
                        <option selected="selected">Khối 1</option>
                    </select>
                    <b class="presentation" role="presentation"></b>
                </div>
                <div class="col-auto object">
                    <select class="custom-select mr-sm-2" style="width: 100%">
                        <option value="" selected="selected">-- Chọn</option>
                        <option value="18">Mỹ thuật - [MT]</option>
                        <option value="9">Tiếng Anh - [TA]</option>
                        <option value="14">Tiếng Việt - [TV]</option>
                        <option value="1">Toán - [TO]</option>
                        <option value="16">Âm nhạc - [AN]</option>
                        <option value="17">Đạo đức - [DD]</option>
                    </select>
                    <b class="presentation" role="presentation"></b>
                </div>
                <ul class="nav nav-pills">
                <li class="nav-item dropdown">
                    <a style="border: 1px solid; margin: 10px 10px" class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">--chọn</a>
                    <div class="dropdown-menu object" >
                        <a class="dropdown-item" href="#1b0" data-toggle="tab">--chọn</a>
                        <a class="dropdown-item" href="#1b1" data-toggle="tab">1B1</a>
                       
                    </div>
                </li>
            </ul>
                <div class="col-md-2" style="padding-left: 5px; padding-right: 5px">
                    <button type="button" class="btn bg-green-jungle font-white" title="Xuất dữ liệu(file excel)"><i class="fa fa-file-excel-o font-white"></i></button>
                    <button type="button" class="btn bg-yellow font-black" title="Tải lại dữ liệu"><i class="fa fa-refresh" aria-hidden="true"></i></button>
                </div>


            </div>
            <div class="tab-content">
                <div id="1b0" >
                    <div class="fixed-table-container" style="padding-top: 0px;">
                        <div class="note note-success" style="margin: 0px">
                            Ghi chú: Thời gian tính toán dữ liệu từ sổ điểm lớp học vào sổ điểm chuyển đổi MOET có thể mất thời gian lên tới 20 phút.<br>
                            Sau khi thực hiện xong thao tác mapping bài tập/nhập điểm vào hạng mục trên sổ điểm lớp học, các Thầy/Cô vui lòng quay lại Sổ điểm chuyển đổi MOET kiểm tra kết quả quá trình chuyển/tổng hợp điểm sổ điểm MOET.<br>
                            Trong trường hợp quá 20 phút mà hệ thống vẫn chưa cập nhật kết quả trên sổ điểm chuyển đổi MOET, các Thầy/cô vui lòng liên hệ với Phòng Giáo vụ để được hỗ trợ
                        </div>

                    </div>
                </div>

                <div id="1b1" >
                    <div class="fixed-table-container" style="padding-top: 5px;" >
                        <div class="note note-warning">
                            <p>
                                Quý Thầy/cô giáo không thể cập nhật điểm bởi một trong các lý do sau:
                            <br />
                                1. Hạng mục điểm đã bị khóa theo từng giai đoạn<br />
                                2. Điểm của học sinh được chuyển từ nơi khác đến: Từ 1 lớp học khác, từ trường khác<br />
                                3. Hạng mục điểm được thiết lập tự động đồng bộ từ điểm bài tập
                            </p>
                        </div>
                        <div class="table-scrollable">
                            <table class="table table-bordered table-striped table-condensed flip-content">
                                <thead class="flip-content bg-blue bg-font-blue">
                                    <tr>
                                        <th style="vertical-align: middle; text-align: center; width: 5%">#</th>
                                        <th style="vertical-align: middle; text-align: center">Mã HS </th>
                                        <th style="vertical-align: middle; text-align: center">Họ và tên </th>
                                        <th style="vertical-align: middle; text-align: center">Ngày sinh </th>
                                        <th style="vertical-align: middle; text-align: center">Giới tính </th>
                                        <th class="bg-grey-salt" style="vertical-align: middle; text-align: center;">Giữa HK 1</th>
                                        <th class="bg-grey-salt" style="vertical-align: middle; text-align: center;">Cuối HK 1 </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td style="vertical-align: middle; text-align: center">1</td>
                                        <td style="vertical-align: middle; text-align: center">OMT-011</td>
                                        <td style="vertical-align: middle;">Nguyễn Tùng Anh</td>
                                        <td style="vertical-align: middle; text-align: center">16/02/2008</td>
                                        <td style="vertical-align: middle; text-align: center">Nam</td>
                                        <td class="td_input_table_cell " style="vertical-align: middle; text-align: center; background-color: #eef7f1 !important;"></td>
                                        <td class="td_input_table_cell " style="vertical-align: middle; text-align: center; background-color: #eef7f1 !important;"></td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle; text-align: center">2</td>
                                        <td style="vertical-align: middle; text-align: center">OMT-012</td>
                                        <td style="vertical-align: middle;">Phạm Hoàng Anh</td>
                                        <td style="vertical-align: middle; text-align: center">05/11/2008</td>
                                        <td style="vertical-align: middle; text-align: center">Nam</td>
                                        <td class="td_input_table_cell " style="vertical-align: middle; text-align: center; background-color: #eef7f1 !important;"></td>
                                        <td class="td_input_table_cell " style="vertical-align: middle; text-align: center; background-color: #eef7f1 !important;"></td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle; text-align: center">3</td>
                                        <td style="vertical-align: middle; text-align: center">OMT-013</td>
                                        <td style="vertical-align: middle;">Phạm Thị Tuyết Anh</td>
                                        <td style="vertical-align: middle; text-align: center">19/09/2008</td>
                                        <td style="vertical-align: middle; text-align: center">Nữ</td>
                                        <td class="td_input_table_cell " style="vertical-align: middle; text-align: center; background-color: #eef7f1 !important;"></td>
                                        <td class="td_input_table_cell " style="vertical-align: middle; text-align: center; background-color: #eef7f1 !important;"></td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle; text-align: center">4</td>
                                        <td style="vertical-align: middle; text-align: center">OMT-014</td>
                                        <td style="vertical-align: middle;">Phạm Trâm Anh</td>
                                        <td style="vertical-align: middle; text-align: center">16/07/2008</td>
                                        <td style="vertical-align: middle; text-align: center">Nữ</td>
                                        <td class="td_input_table_cell " style="vertical-align: middle; text-align: center; background-color: #eef7f1 !important;"></td>
                                        <td class="td_input_table_cell " style="vertical-align: middle; text-align: center; background-color: #eef7f1 !important;"></td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle; text-align: center">5</td>
                                        <td style="vertical-align: middle; text-align: center">OMT-015</td>
                                        <td style="vertical-align: middle;">Trần Đức Anh</td>
                                        <td style="vertical-align: middle; text-align: center">12/12/2008</td>
                                        <td style="vertical-align: middle; text-align: center">Nam</td>
                                        <td class="td_input_table_cell " style="vertical-align: middle; text-align: center; background-color: #eef7f1 !important;"></td>
                                        <td class="td_input_table_cell " style="vertical-align: middle; text-align: center; background-color: #eef7f1 !important;"></td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle; text-align: center">6</td>
                                        <td style="vertical-align: middle; text-align: center">OMT-016</td>
                                        <td style="vertical-align: middle;">Lê Minh Bảo Anh</td>
                                        <td style="vertical-align: middle; text-align: center">25/05/2008</td>
                                        <td style="vertical-align: middle; text-align: center">Nữ</td>
                                        <td class="td_input_table_cell " style="vertical-align: middle; text-align: center; background-color: #eef7f1 !important;"></td>
                                        <td class="td_input_table_cell " style="vertical-align: middle; text-align: center; background-color: #eef7f1 !important;"></td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle; text-align: center">7</td>
                                        <td style="vertical-align: middle; text-align: center">OMT-017</td>
                                        <td style="vertical-align: middle;">Lại Viết Hoàng Anh</td>
                                        <td style="vertical-align: middle; text-align: center">18/04/2008</td>
                                        <td style="vertical-align: middle; text-align: center">Nam</td>
                                        <td class="td_input_table_cell " style="vertical-align: middle; text-align: center; background-color: #eef7f1 !important;"></td>
                                        <td class="td_input_table_cell " style="vertical-align: middle; text-align: center; background-color: #eef7f1 !important;"></td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle; text-align: center">8</td>
                                        <td style="vertical-align: middle; text-align: center">OMT-018</td>
                                        <td style="vertical-align: middle;">Lê Trang Anh</td>
                                        <td style="vertical-align: middle; text-align: center">27/10/2008</td>
                                        <td style="vertical-align: middle; text-align: center">Nữ</td>
                                        <td class="td_input_table_cell " style="vertical-align: middle; text-align: center; background-color: #eef7f1 !important;"></td>
                                        <td class="td_input_table_cell " style="vertical-align: middle; text-align: center; background-color: #eef7f1 !important;"></td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle; text-align: center">9</td>
                                        <td style="vertical-align: middle; text-align: center">OMT-019</td>
                                        <td style="vertical-align: middle;">Đào Phương Anh</td>
                                        <td style="vertical-align: middle; text-align: center">10/12/2008</td>
                                        <td style="vertical-align: middle; text-align: center">Nữ</td>
                                        <td class="td_input_table_cell " style="vertical-align: middle; text-align: center; background-color: #eef7f1 !important;"></td>
                                        <td class="td_input_table_cell " style="vertical-align: middle; text-align: center; background-color: #eef7f1 !important;"></td>
                                    </tr>
                                    <tr>
                                        <td style="vertical-align: middle; text-align: center">10</td>
                                        <td style="vertical-align: middle; text-align: center">OMT-020</td>
                                        <td style="vertical-align: middle;">Lê Sơn Bách</td>
                                        <td style="vertical-align: middle; text-align: center">07/01/2008</td>
                                        <td style="vertical-align: middle; text-align: center">Nam</td>
                                        <td class="td_input_table_cell " style="vertical-align: middle; text-align: center; background-color: #eef7f1 !important;"></td>
                                        <td class="td_input_table_cell " style="vertical-align: middle; text-align: center; background-color: #eef7f1 !important;"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        .main-omt {
            border: 1px solid #32c5d2;
            background-color: #fff;
        }

            .main-omt .omt-header {
                background-color: #32c5d2;
                padding: 4px 7px;
                display: flex;
            }

        .omt-header .header-title {
            font-size: 20px;
            padding: 10px 10px;
            color: white;
            width: 100%;
            margin: 0;
        }

        .presentation {
            border-color: #999 transparent transparent transparent;
            border-style: solid;
            border-width: 4px 4px 0 4px;
            height: 0;
            left: -15px;
            margin-left: -4px;
            margin-top: -2px;
            position: relative;
            top: 14%;
            width: 0;
        }

        .omt-header .omt__icon {
            font-size: 24px;
            padding: 8px 10px;
            color: white;
        }

        .omt-top {
            display: flex;
        }

        .note.note-warning {
            background-color: #faeaa9;
            border-color: #f3cc31;
            margin: 0 0 5px 0;
            width: 110%;
        }

        .note {
            margin: 0 0 20px 0;
            padding: 15px 30px 15px 15px;
            width: 100%;
            min-height: 100px;
        }

        .grade {
            margin-left: 30px;
        }

        .object {
            margin-left: 10px;
        }

        .btn.bg-green-jungle.font-white {
            background-color: #26C281;
            color: white;
            outline: none;
            margin-left: 15px;
        }

        .btn.bg-yellow.font-black {
            background-color: #c49f47;
            color: black;
            outline: none;
        }



        .note.note-success {
            background-color: #c0edf1;
            border-color: #58d0da;
            color: black !important;
            font-size: 12px;
            color: black;
        }

        .note.note-warning {
            background-color: #faeaa9;
            border-color: #f3cc31;
            color: black;
            width: 100%;
        }

        .portlet_body {
            padding: 15px;
        }

        .btn.btn-secondary.class {
            margin-left: 15px;
            border-radius: 3px;
        }

        .dropdown-menu.object {
            min-width: 5.5rem !important;
            margin-left: 15px;
        }

        .dropdown.show {
            height: 37px;
        }

        .dropdown-item {
            text-decoration: none !important;
        }
        .nav-link.dropdown-toggle{
            margin:0 10px !important;
        }

        .table-scrollable {
            width: 100%;
            overflow-x: auto;
            overflow-y: hidden;
            border: 1px solid #e7ecf1;
            margin: 10px 0 !important;
        }

        .bg-blue {
            background: #3598dc !important;
            color: white;
        }
        .bg-grey-salt{
            background: #bfcad1 !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

