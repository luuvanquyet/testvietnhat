﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="admin_omt_SoDiemLopHoc.aspx.cs" Inherits="admin_page_module_function_module_OMT_admin_omt_SoDiemLopHoc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="main-omt">
        <div class="omt-header">
            <i class="fa fa-calculator omt__icon" aria-hidden="true"></i>
            <h4 class="header-title">Sổ báo cáo lớp học</h4>
        </div>
        <div class="portlet_body">
            <div class="omt-top">
                <div class="col-md-3" style="padding: 0 5px 0 5px">
                    <div class="note note-warning font-red" style="margin: 0 0 5px 0;">
                        Giai đoạn: <b>Năm học 2020 - 2021  &gt;  Giữa HK1</b>
                    </div>
                </div>
                <div class="col-auto grade">
                    <select class="custom-select mr-sm-2" style="width: 100%">
                        <option selected="selected">Khối 1</option>
                    </select>
                    <b class="presentation" role="presentation"></b>
                </div>
                <div class="col-auto object">
                    <select class="custom-select mr-sm-2" style="width: 100%">
                        <option value="" selected="selected">-- Chọn</option>
                        <option value="18">Mỹ thuật - [MT]</option>
                        <option value="9">Tiếng Anh - [TA]</option>
                        <option value="14">Tiếng Việt - [TV]</option>
                        <option value="1">Toán - [TO]</option>
                        <option value="16">Âm nhạc - [AN]</option>
                        <option value="17">Đạo đức - [DD]</option>
                    </select>
                    <b class="presentation" role="presentation"></b>
                </div>
                <div class="col-auto object">
                    <select class="custom-select mr-sm-2" style="width: 100%">
                        <option value="" selected="selected">-- Chọn</option>
                        <option value="1106">CIE ESL-1B1</option>
                        <option value="1107">CIE Maths-1B1</option>
                        <option value="1108">CIE Science-1B1</option>
                        <option value="1040">Mỹ thuật-1B1</option>
                        <option value="1043">Tiếng Anh-1B1</option>
                        <option value="1041">Tiếng Việt-1B1</option>
                        <option value="1042">Toán-1B1</option>
                        <option value="1044">Âm nhạc-1B1</option>
                        <option value="1045">Đạo đức-1B1</option>
                    </select>
                    <b class="presentation" role="presentation"></b>
                </div>
                <div class="col-md-2" style="padding-left: 5px; padding-right: 5px">
                    <button type="button" class="btn bg-green-jungle font-white" title="Xuất dữ liệu(file excel)"><i class="fa fa-file-excel-o font-white"></i></button>
                    <button type="button" class="btn bg-yellow font-black" title="Tải lại dữ liệu"><i class="fa fa-refresh" aria-hidden="true"></i></button>
                </div>


            </div>
            <div class="fixed-table-container" style="padding-top: 0px;">
                <div class="note note-success" style="margin: 0px">
                    Ghi chú: Thời gian tính toán dữ liệu từ bài tập vào sổ điểm lớp học có thể mất thời gian lên tới 15 phút.<br>
                    Sau khi thực hiện xong thao tác mapping bài tập vào hạng mục trên sổ điểm, các Thầy/Cô vui lòng quay lại Sổ điểm lớp học kiểm tra kết quả thực hiện tổng hợp.<br>
                    Trong trường hợp quá 15 phút mà hệ thống vẫn chưa cập nhật kết quả trên sổ điểm lớp học, các Thầy/cô vui lòng liên hệ với Phòng Giáo vụ để được hỗ trợ
                </div>
            </div>
        </div>
    </div>
    <style>
        .main-omt {
            border: 1px solid #32c5d2;
            background-color: #fff;
        }

            .main-omt .omt-header {
                background-color: #32c5d2;
                padding: 4px 7px;
                display: flex;
            }

        .omt-header .header-title {
            font-size: 20px;
            padding: 10px 10px;
            color: white;
            width: 100%;
            margin: 0;
        }

        .presentation {
            border-color: #999 transparent transparent transparent;
            border-style: solid;
            border-width: 4px 4px 0 4px;
            height: 0;
            left: -15px;
            margin-left: -4px;
            margin-top: -2px;
            position: relative;
            top: 16%;
            width: 0;
        }

        .omt-header .omt__icon {
            font-size: 24px;
            padding: 8px 10px;
            color: white;
        }

        .omt-top {
            display: flex;
           
        }

        .note.note-warning {
            background-color: #faeaa9;
            border-color: #f3cc31;
            margin: 0 0 5px 0;
            width: 110%;
        }

        .note {
            margin: 0 0 20px 0;
            padding: 15px 30px 15px 15px;
            width: 100%;
        }

        .grade {
            margin-left: 30px;
        }

        .object {
            margin-left: 10px;
        }

        .btn.bg-green-jungle.font-white {
            background-color: #26C281;
            color: white;
            outline: none;
        }

        .btn.bg-yellow.font-black {
            background-color: #c49f47;
            color: black;
            outline: none;
        }

       

        .note.note-success {
            background-color: #c0edf1;
            border-color: #58d0da;
            color: black !important;
            font-size: 12px;
            color: black;
        }
        .portlet_body{
            padding:15px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

