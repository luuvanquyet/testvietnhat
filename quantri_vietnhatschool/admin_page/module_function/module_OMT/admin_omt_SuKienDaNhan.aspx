﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="admin_omt_SuKienDaNhan.aspx.cs" Inherits="admin_page_module_function_module_OMT_admin_omt_SuKienDaNhan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="main-omt">
        <div class="omt-header">
            <i class="fa fa-list omt__icon" aria-hidden="true"></i>
            <h4 class="header-title">Sự Kiện</h4>
        </div>
        <div class="omt-top">
            <form class="form-inline  my-2 my-lg-0">
                <input class="form-control search mr-sm-2" type="search" placeholder="Tìm mã,tên..." aria-label="Search" />
            </form>
        </div>
        <div class="fixed-table-container">
            <table class="table table-bordered table-striped table-condensed flip-content">
                <thead class="flip-content bg-blue bg-font-blue">
                    <tr>
                        <th style="vertical-align: middle; text-align: center; width: 20%">Tên sự kiện </th>
                        <th style="vertical-align: middle; text-align: center; width: 15%">Phạm vi </th>
                        <th style="vertical-align: middle; text-align: center; width: 15%">Thời gian</th>
                        <th style="vertical-align: middle; text-align: center; width: 20%">Thông tin đăng ký</th>


                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td colspan="8">
                            <div class="note note-warning">
                                <p>Không tim thấy thông tin  sự kiện trên hệ thống. Vui lòng khai báo Sự kiện </p>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <style>
        .main-omt {
            border: 1px solid #32c5d2;
            background-color: #fff;
        }

            .main-omt .omt-header {
                background-color: #32c5d2;
                padding: 4px 7px;
                display: flex;
            }

        .omt-header .header-title {
            font-size: 20px;
            padding: 10px 10px;
            color: white;
            width: 100%;
        }

        .omt-header .omt__icon {
            font-size: 30px;
            padding: 8px 10px;
            color: white;
        }
        .fixed-table-container{
            width:98%;
            margin-left:1%;
        }

        .omt-top {
            display: flex;
            padding: 20px 20px;
        }

        .form-control.search {
            width: 20%;
        }

        .note {
            margin: 0 0 20px 0;
            padding: 15px 30px 15px 15px;
            width: 100%;
        }

            .note.note-warning {
                background-color: #faeaa9;
                border-color: #f3cc31;
                color: black;
            }

        .bg-blue {
            background: #3598dc !important;
            color: white;
        }
    </style>

</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

