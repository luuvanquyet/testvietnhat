﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="admin_omt_TheoDoiSucKhoe.aspx.cs" Inherits="admin_page_module_function_module_OMT_admin_omt_TheoDoiSucKhoe" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">

    <div class="main-omt">
        <div class="omt-header">
            <i class="fa fa-list omt__icon" aria-hidden="true"></i>
            <h4 class="header-title">Danh Sách Theo Dõi Y Tế</h4>
        </div>
        <div class="omt-top">
            <select class="form-control form-control-sm form-omt">
                <option>1B1</option>
            </select>
            <ul class="nav nav-pills">
                <li class="nav-item dropdown">
                    <a style="border: 1px solid; margin: 10px 10px" class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">--Trạng Thái--</a>
                    <div class="dropdown-menu" style="width: 175px; margin-left: 10px">
                        <a class="dropdown-item" href="#TT" data-toggle="tab">Trạng Thái</a>
                        <a class="dropdown-item" href="#BT" data-toggle="tab">Bình Thường</a>
                        <a class="dropdown-item" href="#NT" data-toggle="tab">Nghiêm Trọng</a>

                    </div>
                </li>
            </ul>
            <div class="col-md-1" style="padding-left: 5px; padding-right: 5px">
                <button id="#themmoi" type="button" class="btn btn-primary" style="outline: none" data-toggle="modal" data-target=".bd-example-modal-lg">Thêm Mới</button>

            </div>
            <div class="col-md-1" style="padding-left: 5px; padding-right: 5px; margin-left: 35px">
                <button type="button" class="btn bg-green-jungle font-white" style="background-color: #26C281; color: white; outline: none">
                    <i class="fa fa-file-excel-o font-white">Tải Về</i>
                </button>

            </div>
        </div>
        <div class="tab-content">
            <div id="TT" class="tab-pane fade in active">
                <div class="fixed-table-container" style="width: 98%; margin-left: 1%" id="data-items">
                    <div>
                        Tổng số hồ sơ: <b>0</b>
                    </div>
                    <table class="table table-bordered table-striped table-condensed flip-content">
                        <thead class="flip-content bg-blue bg-font-blue" style="background-color: #3598dc; color: white">
                            <tr>
                                <th style="vertical-align: middle; text-align: center; padding: 0"></th>
                                <th style="vertical-align: middle; text-align: center; padding: 0">Học sinh</th>
                                <th style="vertical-align: middle; text-align: center; padding: 0">Trạng thái</th>
                                <th style="vertical-align: middle; text-align: center; padding: 0">Chẩn đoán</th>
                                <th style="vertical-align: middle; text-align: center; padding: 0">Phương pháp xử lý/điều trị</th>
                                <th style="vertical-align: middle; text-align: center; padding: 0">Phân loại theo dõi</th>
                                <th style="vertical-align: middle; text-align: center; padding: 0">Gửi thông báo</th>
                            </tr>
                        </thead>
                        <tbody style="color: black">
                            <tr>
                                <td>
                                    <div class="btn-group">
                                        <button class="btn btn-xs green dropdown-toggle" style="font-size: 9px; padding: 3px 8px 3px 8px; color: #FFFFFF; background-color: #32c5d2; position: relative"
                                            type="button" data-toggle="dropdown" aria-expanded="false">
                                            Hành động
                            <i class="fa fa-angle-down"></i>
                                        </button>
                                        <ul class="dropdown-menu" style="min-width: 8rem; height: 95px" role="menu">
                                            <li style="width: 100%">
                                                <a href="#capnhat" class="btn btn-primary" style="margin-left: 6px" data-toggle="modal" data-target=".bd-example-modal-lg1">
                                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                    Cập nhật
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="btn btn-danger" style="color: white; margin-left: 6px; width: 114px">
                                                    <i class="fa fa-trash-o" style="color: white" aria-hidden="true"></i>
                                                    Xóa
                                                </a>
                                            </li>
                                            <li class="divider"></li>
                                        </ul>
                                    </div>
                                </td>
                                <td>
                                    <a style="display: block; color: blue; text-decoration: none; padding: 0" href="#">Phạm Trâm Anh - [OMT-014]
                                    </a>
                                </td>
                                <td style="vertical-align: top; padding: 0">Bình thường
                                </td>
                                <td style="vertical-align: top; padding: 0">sốt nhẹ</td>
                                <td style="vertical-align: top; padding: 0">đã đo nhiệt độ , uống thuốc hạ sốt , nằm nghỉ ngơi</td>
                                <td style="vertical-align: top; padding: 0">đang theo dõi</td>
                                <td>
                                    <span class="btn btn-success" style="cursor: text">Đã gửi
                                    </span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div id="BT" class="tab-pane fade">
                <div class="fixed-table-container" style="width: 98%; margin-left: 1%" id="data-items1">
                    <div>
                        Tổng số hồ sơ: <b>0</b>
                    </div>
                    <table class="table table-bordered table-striped table-condensed flip-content">
                        <thead class="flip-content bg-blue bg-font-blue" style="background-color: #3598dc; color: white">
                            <tr>
                                <th style="vertical-align: middle; text-align: center; padding: 0"></th>
                                <th style="vertical-align: middle; text-align: center; padding: 0">Học sinh</th>
                                <th style="vertical-align: middle; text-align: center; padding: 0">Trạng thái</th>
                                <th style="vertical-align: middle; text-align: center; padding: 0">Chẩn đoán</th>
                                <th style="vertical-align: middle; text-align: center; padding: 0">Phương pháp xử lý/điều trị</th>
                                <th style="vertical-align: middle; text-align: center; padding: 0">Phân loại theo dõi</th>
                                <th style="vertical-align: middle; text-align: center; padding: 0">Gửi thông báo</th>
                            </tr>
                        </thead>
                        <tbody style="color: black">
                            <tr>
                                <td>
                                    <div class="btn-group">
                                        <button class="btn btn-xs green dropdown-toggle" style="font-size: 9px; padding: 3px 8px 3px 8px; color: #FFFFFF; background-color: #32c5d2; position: relative"
                                            type="button" data-toggle="dropdown" aria-expanded="false">
                                            Hành động
                            <i class="fa fa-angle-down"></i>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li style="width: 100%">
                                                <a href="#capnhat" class="btn btn-primary" style="background-color: none; color: gray" data-toggle="modal" data-target=".bd-example-modal-lg1">
                                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                    Cập nhật
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="font-red" style="color: red">
                                                    <i class="fa fa-trash-o" style="color: red" aria-hidden="true"></i>
                                                    Xóa
                                                </a>
                                            </li>
                                            <li class="divider"></li>
                                        </ul>
                                    </div>
                                </td>
                                <td>
                                    <a style="display: block; color: blue; text-decoration: none; padding: 0" href="#">Phạm Trâm Anh - [OMT-014]
                                    </a>
                                </td>
                                <td style="vertical-align: top; padding: 0">Bình thường
                                </td>
                                <td style="vertical-align: top; padding: 0">sốt nhẹ</td>
                                <td style="vertical-align: top; padding: 0">đã đo nhiệt độ , uống thuốc hạ sốt , nằm nghỉ ngơi</td>
                                <td style="vertical-align: top; padding: 0">đang theo dõi</td>
                                <td>
                                    <span class="btn btn-success" style="cursor: text">Đã gửi
                                    </span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div id="NT" class="tab-pane fade ">
                <div class="fixed-table-container" style="width: 98%; margin-left: 1%" id="data-items2">
                    <div>
                        Tổng số hồ sơ: <b>0</b>
                    </div>
                    <table class="table table-bordered table-striped table-condensed flip-content">
                        <thead class="flip-content bg-blue bg-font-blue" style="background-color: #3598dc; color: white">
                            <tr>
                                <th style="vertical-align: middle; text-align: center; padding: 0"></th>
                                <th style="vertical-align: middle; text-align: center; padding: 0">Học sinh</th>
                                <th style="vertical-align: middle; text-align: center; padding: 0">Trạng thái</th>
                                <th style="vertical-align: middle; text-align: center; padding: 0">Chẩn đoán</th>
                                <th style="vertical-align: middle; text-align: center; padding: 0">Phương pháp xử lý/điều trị</th>
                                <th style="vertical-align: middle; text-align: center; padding: 0">Phân loại theo dõi</th>
                                <th style="vertical-align: middle; text-align: center; padding: 0">Gửi thông báo</th>
                            </tr>
                        </thead>
                        <tbody style="color: black">
                            <tr>
                                <td>
                                    <div class="btn-group">
                                        <button class="btn btn-xs green dropdown-toggle" style="font-size: 9px; padding: 3px 8px 3px 8px; color: #FFFFFF; background-color: #32c5d2; position: relative"
                                            type="button" data-toggle="dropdown" aria-expanded="false">
                                            Hành động
                            <i class="fa fa-angle-down"></i>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li style="width: 100%">
                                                <a href="#" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg1">
                                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                    Cập nhật
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="font-red" style="color: red">
                                                    <i class="fa fa-trash-o" style="color: red" aria-hidden="true"></i>
                                                    Xóa
                                                </a>
                                            </li>
                                            <li class="divider"></li>
                                        </ul>
                                    </div>
                                </td>
                                <td>
                                    <a style="display: block; color: blue; text-decoration: none; padding: 0" href="#">Phạm Trâm Anh - [OMT-014]
                                    </a>
                                </td>
                                <td style="vertical-align: top; padding: 0">Bình thường
                                </td>
                                <td style="vertical-align: top; padding: 0">sốt nhẹ</td>
                                <td style="vertical-align: top; padding: 0">đã đo nhiệt độ , uống thuốc hạ sốt , nằm nghỉ ngơi</td>
                                <td style="vertical-align: top; padding: 0">đang theo dõi</td>
                                <td>
                                    <span class="btn btn-success" style="cursor: text">Đã gửi
                                    </span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>



            </div>
        </div>
        <div class="modal fade bd-example-modal-lg1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div id="capnhat">
                        <div class="omt-header">
                            <i class="fa fa-list omt__icon" aria-hidden="true"></i>
                            <h4 class="header-title">Sửa Theo Dõi Y Tế 
                                              <button type="button" class="close modal__close" style="outline:none" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                              </button>
                            </h4>
                          
                        </div>
                        <div class="portlet-body">
                            <div class="fixed-table-container">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="col-md-3 text-right" style="padding-top: 19px;">Học sinh</label>
                                            <div class="col-md-9">
                                                <select name="user_id" class="form-control" id="user_id_select" style="width: 100%;" required="true">
                                                    <option>Phạm Trâm Anh [OMT-014 - 1B1 - 16/07/2008] </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 text-right" style="padding-top: 19px;">Trạng Thái</label>
                                            <div class="col-md-9">
                                                <select name="user_id" class="form-control" id="user_id_select1" style="width: 100%;" required="true">
                                                    <option>Bình Thường</option>
                                                    <option>Nghiêm Trọng</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 text-right" style="padding-top: 20px;">Chuẩn Đoán</label>
                                            <div class="col-md-9">
                                                <textarea class="form-control" id="diagnostic1" rows="1" style="width: 100%; height: 40px" placeholder="Chẩn đoán" name="diagnostic" cols="50">sốt nhẹ</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">

                                        <div class="form-group">
                                            <label class="col-md-3 text-right" style="padding-top: 5px;">
                                                Phương Pháp xử lý/<br />
                                                Điều Trị</label>
                                            <div class="col-md-9">
                                                <textarea class="form-control" id="diagnostic2" rows="1" style="width: 100%; height: 40px" placeholder="Phương Pháp xử lý/Điều Trị" name="diagnostic" cols="50"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group" style="margin-top: 97px">
                                            <label class="col-md-3 text-right" style="padding-top: 5px">Phân Loại Theo Dõi</label>
                                            <div class="col-md-9">
                                                <textarea class="form-control" id="diagnostic3" rows="1" style="width: 100%; height: 40px" placeholder="Phân Loại Theo Dõi" name="diagnostic" cols="50"></textarea>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-md-6">
                                        <label class="col-md-3 text-right" style="padding-top: 7px;"></label>
                                        <div class="col-md-9">
                                            <div class="mt-checkbox-inline">
                                                <label class="mt-checkbox">
                                                    <input id="chk_send_notification" checked="checked" name="send_notification" type="checkbox" value="1" />
                                                    Gửi thông báo
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box box-success" style="text-align: center; padding-top: 15px; padding-bottom: 15px">
                            <div class="box-body">
                                <div class="btn-group">
                                    <a class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg1" style="background-color: #3598dc; color: white" id="btn_save_form"><i class="fa fa-floppy-o"></i>
                                        Cập nhật
                                    </a>
                                    <%--<a class="btn default" style="background-color: red; color: white" href=""><i class="fa fa-remove"></i>Đóng</a>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div id="themmoi">
                        <div class="omt-header">
                            <i class="fa fa-list omt__icon" aria-hidden="true"></i>
                            <h4 class="header-title">Thêm Mới Theo Dõi Y Tế 
                                              <button type="button" class="close modal__close"style="outline:none" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                              </button>
                            </h4>
                        </div>
                        <div class="portlet-body">
                            <div class="fixed-table-container">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="col-md-3 text-right" style="padding-top: 19px;">Học sinh</label>
                                            <div class="col-md-9">
                                                <select name="user_id" class="form-control" id="user_id_select3" style="width: 100%;" required="true">
                                                    <option>Phạm Trâm Anh [OMT-014 - 1B1 - 16/07/2008] </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 text-right" style="padding-top: 19px;">Trạng Thái</label>
                                            <div class="col-md-9">
                                                <select name="user_id" class="form-control" id="user_id_select4" style="width: 100%;" required="true">
                                                    <option>Bình Thường</option>
                                                    <option>Nghiêm Trọng</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 text-right" style="padding-top: 20px;">Chuẩn Đoán</label>
                                            <div class="col-md-9">
                                                <textarea class="form-control" id="diagnostic4" rows="1" style="width: 100%; height: 40px" placeholder="Chẩn đoán" name="diagnostic" cols="50">sốt nhẹ</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">

                                        <div class="form-group">
                                            <label class="col-md-3 text-right" style="padding-top: 5px;">
                                                Phương Pháp xử lý/<br />
                                                Điều Trị</label>
                                            <div class="col-md-9">
                                                <textarea class="form-control" id="diagnostic5" rows="1" style="width: 100%; height: 40px" placeholder="Phương Pháp xử lý/Điều Trị" name="diagnostic" cols="50"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group" style="margin-top: 97px">
                                            <label class="col-md-3 text-right" style="padding-top: 5px">Phân Loại Theo Dõi</label>
                                            <div class="col-md-9">
                                                <textarea class="form-control" id="diagnostic6" rows="1" style="width: 100%; height: 40px" placeholder="Phân Loại Theo Dõi" name="diagnostic" cols="50"></textarea>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-md-6">
                                        <label class="col-md-3 text-right" style="padding-top: 7px;"></label>
                                        <div class="col-md-9">
                                            <div class="mt-checkbox-inline">
                                                <label class="mt-checkbox">
                                                    <input id="chk_send_notification2" checked="checked" name="send_notification" type="checkbox" value="1" />
                                                    Gửi thông báo
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box box-success" style="text-align: center; padding-top: 15px; padding-bottom: 15px">
                            <div class="box-body">
                                <div class="btn-group">
                                    <a class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg" style="background-color: #3598dc; color: white" id="btn_save_form1"><i class="fa fa-floppy-o"></i>
                                        Thêm Mới
                                    </a>
                                    <%--<a class="btn default" style="background-color: red; color: white" href=""><i class="fa fa-remove"></i>Đóng</a>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        .main-omt {
            border: 1px solid #32c5d2;
            background-color: #fff;
        }

            .main-omt .omt-header {
                background-color: #32c5d2;
                padding: 4px 7px;
                display: flex;
            }

        .omt-header .header-title {
            font-size: 20px;
            padding: 10px 10px;
            color: white;
            width: 100%;
        }

        .omt-header .omt__icon {
            font-size: 30px;
            padding: 8px 10px;
            color: white;
        }

        .omt-top {
            display: flex;
            padding: 0 20px;
        }

            .omt-top .form-omt {
                width: 17%;
                height: 35px !important;
                margin-right: 15px;
                margin-top: 10px;
            }

        .form-control {
            height: 35px;
            margin-top: 10px;
            width: 20%;
        }

        .omt-top .btn {
            height: 35px;
            margin-top: 10px !important;
            margin-left: 5px;
            border-radius: 3px;
            padding-bottom: 10px;
        }

        .dropdown-item {
            text-decoration: none !important;
            font-size: 13px;
            padding: 3px 10px 3px 10px !important;
        }

            .dropdown-item:hover {
                background-color: #0275d8 !important;
                color: #fff !important;
            }

        .btn-group ul li a {
            text-decoration: none;
            padding-left: 10px;
        }

            .btn-group ul li a:hover {
                background-color: #f6f6f6;
                text-decoration: none;
            }



        .btn-group .btn {
            border-top-left-radius: 3px;
            border-bottom-left-radius: 3px;
            border-top-right-radius: 3px;
            border-bottom-right-radius: 3px;
        }
        /*        .modal__close{
            float:right;
        }*/
        .modal-content {
            left: 36px;
            top: 74px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

