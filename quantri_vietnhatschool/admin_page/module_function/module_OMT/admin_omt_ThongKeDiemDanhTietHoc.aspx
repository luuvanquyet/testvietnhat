﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="admin_omt_ThongKeDiemDanhTietHoc.aspx.cs" Inherits="admin_page_module_function_module_OMT_admin_omt_ThongKeDiemDanhTietHoc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <link rel="stylesheet" href="/jquery.datetimepicker.min.css" />
    <script src="/jquery.js"></script>
    <script src="/jquery.datetimepicker.full.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="main-omt">
        <div class="omt-header">
            <i class="fa fa-user omt__icon" aria-hidden="true"></i>
            <h4 class="header-title">Thống kê điểm danh tiết học</h4>
        </div>
        <div class="omt-top">
            <div class="col-auto object">
                <select class="custom-select mr-sm-2" style="width: 100%">
                    <option value="" selected="selected">--Tất cả</option>
                    <option value="107">Ăn trưa và Nghỉ trưa</option>
                    <option value="102">Tiết 1</option>
                    <option value="103">Tiết 2</option>
                    <option value="104">Tiết 3</option>
                    <option value="105">Tiết 4</option>
                    <option value="106">Tiết 5</option>
                    <option value="108">Tiết 6</option>
                    <option value="109">Tiết 7</option>
                    <option value="110">Tiết 8</option>
                    <option value="111">Tiết 9</option>
                </select>
                <b class="presentation" role="presentation"></b>
            </div>
            <div class="col-auto object">
                <select class="custom-select mr-sm-2" style="width: 100%">
                    <option value="" selected="selected">--Vắng không phép</option>

                    <option value="3">Vắng có phép</option>
                    <option value="2">Đến muộn</option>
                    <option value="1">Đúng giờ</option>
                </select>
                <b class="presentation" role="presentation"></b>
            </div>
            <div class="col-auto object">
                <input type="text" class="form-control daypicker form-control-inline " placeholder="31/8/2020" id="datetime1" />
            </div>
            <div class="col-auto object">
                <input type="text" class="form-control daypicker form-control-inline " placeholder="04/09/2020" id="datetime2" />

            </div>
            <script>
                $('#datetime1').datetimepicker({
                    step: 5
                });
                $('#datetime2').datetimepicker({
                    step: 5
                });

            </script>
            <div class="col-md-2" style="padding-left: 5px; padding-right: 5px">
                <button type="button" class="btn bg-green-jungle font-white">
                    <i class="fa fa-file-excel-o font-white"></i>
                </button>
            </div>


        </div>
        <div class="fixed-table-container" style="padding-top: 20px;" id="data-items">
            <div class="table-scrollable">
                <div id="datatable_include_wrapper" class="dataTables_wrapper no-footer">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="dataTables_length" id="datatable_include_length">
                                <label></label>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <form class="form-inline  my-2 my-lg-0">
                                <input class="form-control search mr-sm-2" type="search" placeholder="Tìm mã,tên..." aria-label="Search" />
                            </form>
                        </div>
                    </div>
                    <div class="table-scrollable">
                        <table class="table table-bordered table-striped table-condensed flip-content">
                            <thead class="flip-content bg-blue bg-font-blue">
                                <tr role="row">
                                    <th style="vertical-align: middle; text-align: center; width: 27px;">No. </th>
                                    <th style="vertical-align: middle; text-align: center; width: 41px;">Ngày </th>
                                    <th style="vertical-align: middle; text-align: center; width: 88px;">Lớp học </th>
                                    <th style="vertical-align: middle; text-align: center; width: 34px;">Tiết học </th>
                                    <th style="vertical-align: middle; text-align: center; width: 153px;">Họ và tên </th>
                                    <th style="vertical-align: middle; text-align: center; width: 153px;">Họ và tên GV </th>
                                    <th style="vertical-align: middle; text-align: center; width: 50px;">Trạng thái </th>
                                    <th style="vertical-align: middle; text-align: center; width: 200px;">Ghi chú </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="odd">
                                    <td colspan="8" class="dataTables_empty">Không tìm thấy đối tượng table</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <style>
        .main-omt {
            border: 1px solid #32c5d2;
            background-color: #fff;
        }

            .main-omt .omt-header {
                background-color: #32c5d2;
                padding: 4px 7px;
                display: flex;
            }

        .omt-header .header-title {
            font-size: 20px;
            padding: 10px 10px;
            color: white;
            width: 100%;
        }

        .omt-header .omt__icon {
            font-size: 30px;
            padding: 8px 10px;
            color: white;
        }

        .fixed-table-container {
            width: 98%;
            margin-left: 1%;
        }

        .omt-top {
            display: flex;
            padding: 20px 20px;
        }

        .form-control.search {
            width: 40%;
            margin-left: 50%;
            margin-bottom: 10px;
        }

        .presentation {
            border-color: #999 transparent transparent transparent;
            border-style: solid;
            border-width: 4px 4px 0 4px;
            height: 0;
            left: -15px;
            margin-left: -4px;
            margin-top: -2px;
            position: relative;
            top: 30%;
            width: 0;
        }

        .object {
            margin-left: 15px;
        }

        .btn.bg-green-jungle.font-white {
            background-color: #26C281;
            color: white;
            outline: none;
            border-radius: 2px;
        }

        .bg-blue {
            background: #3598dc !important;
            color: white;
        }

        .dataTables_empty {
            text-align: center;
        }

        .table-scrollable {
            width: 100%;        
            border: 1px solid #e7ecf1;
         
        }
    </style>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

