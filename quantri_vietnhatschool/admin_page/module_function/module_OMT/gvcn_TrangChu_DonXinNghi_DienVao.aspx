﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="gvcn_TrangChu_DonXinNghi_DienVao.aspx.cs" Inherits="admin_page_module_function_module_OMT_gvcn_TrangChu_DonXinNghi_DienVao" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="table_notification">
        <div class="omt-header">
            <i class="fa fa-database omt__icon" aria-hidden="true"></i>
            <h4 class="header-title">THÊM MỚI ĐƠN XIN NGHỈ</h4>
        </div>
        <div class="row">
            <span class="text_dienvao col-md-1">Ngày :</span>
            <input type="date" name="name" value="#" class="btn_datetime_dienvao col-md-1"/>
            <input type="date" name="name" value="#" class="btn_datetime_dienvao col-md-1"/>
            <span class="text_dienvao col-md-1">Học sinh :</span>
            <input type="text" name="name" value="" class="txt_search_code_tenhs col-md-2" />
        </div>
        <span class="text_dienvao col-md-2">Chọn tiết học sin nghỉ:</span>
        <div class="radio_box_2">
            <input id="check__dst" type="checkbox" name="name" value="" />
            <label for="check__dst">27/8/2020</label>
        </div>
        <div class="row">
            <div class="radio_box_3">
                <input id="check_t1" type="checkbox" name="name" value="" />
                <label for="check_t1">Tiết 1</label>
                <input id="check_t2" type="checkbox" name="name" value="" />
                <label for="check_t2">Tiết 2</label>
                <input id="check_t3" type="checkbox" name="name" value="" />
                <label for="check_t3">Tiết 3</label>
                <input id="check_t4" type="checkbox" name="name" value="" />
                <label for="check_t4">Tiết 4</label>
                <input id="check_t5" type="checkbox" name="name" value="" />
                <label for="check_t5">Tiết 5</label>
                <input id="check_lunch" type="checkbox" name="name" value="" />
                <label for="check_lunch">Ăn trưa và nghỉ trưa</label>
                <input id="check_t6" type="checkbox" name="name" value="" />
                <label for="check_t6">Tiết 6</label>
                <input id="check_t7" type="checkbox" name="name" value="" />
                <label for="check_t7">Tiết 7</label>
                <input id="check_t8" type="checkbox" name="name" value="" />
                <label for="check_t8">Tiết 8</label>
                <input id="check_t9" type="checkbox" name="name" value="" />
                <label for="check_t9">Tiết 9</label>
            </div>
        </div>
        <span class="txt_lydo">Lý do</span>
        <div class="row">
            <textarea name="txt_lydo" class="input_lydo col-md-6 col-md-6"></textarea>
        </div>
        <div class="row">
            <button class="btn_lydo_luu col-md-1">LƯU</button>
            <button class="btn_lydo_dong col-md-1">ĐÓNG</button>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

