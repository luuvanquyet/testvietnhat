﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="gvcn_Trangchu_diemdanh.aspx.cs" Inherits="admin_page_module_function_module_OMT_gvcn_Trangchu_diemdanh" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="table_notification">
        <div class="omt-header">
            <i class="fa fa-user omt__icon" aria-hidden="true"></i>
            <h4 class="header-title">Thống kê điểm danh</h4>
        </div>
        <div class="row">
            <div class="btn_check_datetime">
                <input type="text" name="name" class="txt_search_code col-md-2" />
                <tool class="menu_table_diemdanh col-md-1">--TẤT CẢ</tool>
                <input type="date" name="name" value="#" class="btn_datetime_điemdanh col-md-2" />
                <input type="date" name="name" value="#" class="btn_datetime_điemdanh col-md-2" />
                <div class="col-md-2">
                    <div class="radio_box">
                        <input id="check__dstb" type="checkbox" name="name" value="" />
                        <label for="check__dstb">Đã điểm danh</label>
                    </div>
                </div>
                <button class="btn_file col-md-1">
                    <i class="fa fa-list omt__icon2" aria-hidden="true"></i>
                </button>
                
            </div>
        </div>
        <div class="table_text2">
        </div>
        <div class="table_text3">
            <table id="customers">
                <tr>
                    <th>Ngày</th>
                    <th>Lớp</th>
                    <th>Giáo viên chủ nhiệm</th>
                    <th>Trạng thái</th>
                    <th>Thời gian</th>
                    <th>Điểm danh bởi</th>
                    <th>Sỉ số</th>
                    <th>Tổng số</th>
                    <th>Đến muộn</th>
                    <th>Vắng có phép</th>
                    <th>Vắng không phép</th>
                    <th>ghi chú suất ăn</th>
                </tr>

                <tr class="table_text4">
                    <td colspan="12">
                        <div class="tb_txt_note">Không có thông báo</div>
                    </td>
                </tr>
            </table>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

