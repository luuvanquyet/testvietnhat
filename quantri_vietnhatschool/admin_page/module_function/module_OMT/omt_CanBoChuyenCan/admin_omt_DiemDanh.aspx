﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="admin_omt_DiemDanh.aspx.cs" Inherits="admin_page_module_function_module_OMT_admin_omt_DiemDanh" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="main-omt">
        <div class="omt-header">
            <i class="fa fa-user omt__icon" aria-hidden="true"></i>
            <h4 class="header-title">Điểm Danh</h4>
        </div>
        <div class="omt-top">
              <select class="form-control form-control-sm form-omt">
                <option>1B1</option>
            </select>
           <div class="form-group pmd-textfield pmd-textfield-floating-label form-group__omt">
                <input type="date" class="form-control" id="datepicker" />
            </div>
        </div>
        <div class="omt-title-top">
            <h4 class="omt-title-top-title">Chưa điểm danh!</h4>
            <h4 class="omt-title-top-title">Kết quả điểm danh học sinh sẽ được gửi về phụ huynh ngay lập tức. Đề nghị thầy/cô kiểm tra kĩ trước khi lưu.</h4>
            <p class="omt-title-top-p">Lưu ý: có 1 đơn xin nghỉ. Có thể có các học sinh xin nghỉ một số tiết. Giáo viên vui lòng kiểm tra kỹ trước khi lưu.</p>
        </div>
        <div class="omt-title-bot">
            <p class="omt-title-top-p">Lưu ý: Bạn sẽ không thể cập nhật thông tin điểm danh cho học sinh khi:</p>
            <p class="omt-title-top-p">1. Điểm danh của ngày đã bị khóa</p>
            <p class="omt-title-top-p">2. Điểm danh của ngày đã quá hạn cho phép cập nhật</p>
        </div>
        <div class="table-omt">
            <table class="table table-bordered">
                <thead class="thead-omt">
                    <tr>
                        <th scope="col">Stt</th>
                        <th scope="col">Ảnh đại diện</th>
                        <th scope="col">Mã HS</th>
                        <th scope="col">Họ và Tên</th>
                        <th scope="col">Giới Tính</th>
                        <th scope="col">Trạng thái</th>
                        <th scope="col">Ghi chú</th>
                        <th scope="col">Ghi chú suất ăn</th>
                        <th scope="col">File đính kèm</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>1</th>
                        <td><i class="fa fa-user table-omt__icon" aria-hidden="true"></i></td>
                        <td>OMT-011</td>
                        <td>Nguyễn Tùng Anh</td>
                        <td>Nam</td>
                        <td>
                            <form action="/action_page.php">
                                <div class="form-check">
                                    <label class="form-check-label" for="radio1">
                                        <input type="radio" class="form-check-input" id="radio1" name="optradio" value="option1" checked />Đúng giờ
                                    </label>
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label" for="radio2">
                                        <input type="radio" class="form-check-input" id="radio2" name="optradio" value="option2"/>Đến muộn
                                    </label>
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label" for="radio3">
                                        <input type="radio" class="form-check-input" id="radio3" name="optradio" value="option3"/>Vắng có phép
                                    </label>
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label" for="radio4">
                                        <input type="radio" class="form-check-input" id="radio4" name="optradio" value="option4"/>Vắng không phép
                                    </label>
                                </div>
                            </form>
                            <td>
                                <textarea id="w3review" name="w3review" style="margin-bottom:35px" rows="4" cols="25"></textarea></td>
                            </td>
                                <textarea id="w3review" name="w3review" style="margin-bottom:35px" rows="4" cols="25"></textarea></td>
                    </tr>
                     <tr>
                        <th>1</th>
                        <td><i class="fa fa-user table-omt__icon" aria-hidden="true"></i></td>
                        <td>OMT-012</td>
                        <td>Phạm Hoàng Anh</td>
                        <td>Nam</td>
                        <td>
                            <form action="/action_page.php">
                                <div class="form-check">
                                    <label class="form-check-label" for="radio1">
                                        <input type="radio" class="form-check-input" id="radio1" name="optradio" value="option1" >Đúng giờ
                                    </label>
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label" for="radio2">
                                        <input type="radio" class="form-check-input" id="radio2" name="optradio" value="option2" checked>Đến muộn
                                    </label>
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label" for="radio3">
                                        <input type="radio" class="form-check-input" id="radio3" name="optradio" value="option3">Vắng có phép
                                    </label>
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label" for="radio4">
                                        <input type="radio" class="form-check-input" id="radio4" name="optradio" value="option4">Vắng không phép
                                    </label>
                                </div>
                            </form>
                            <td>
                                <textarea id="w3review" name="w3review" style="margin-bottom:35px" rows="4" cols="25"></textarea></td>
                            <td>
                                <textarea id="w3review" name="w3review" style="margin-bottom:35px" rows="4" cols="25"></textarea></td>
                    </tr>
                     <tr>
                        <th>1</th>
                        <td><i class="fa fa-user table-omt__icon" aria-hidden="true"></i></td>
                        <td>OMT-011</td>
                        <td>Phạm Thị Tuyết Anh</td>
                        <td>Nữ</td>
                        <td>
                            <form action="/action_page.php">
                                <div class="form-check">
                                    <label class="form-check-label" for="radio1">
                                        <input type="radio" class="form-check-input" id="radio1" name="optradio" value="option1">Đúng giờ
                                    </label>
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label" for="radio2">
                                        <input type="radio" class="form-check-input" id="radio2" name="optradio" value="option2">Đến muộn
                                    </label>
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label" for="radio3">
                                        <input type="radio" class="form-check-input" id="radio3" name="optradio" value="option3"checked>Vắng có phép
                                    </label>
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label" for="radio4">
                                        <input type="radio" class="form-check-input" id="radio4" name="optradio" value="option4">Vắng không phép
                                    </label>
                                </div>
                            </form>
                            <td>
                                <textarea id="w3review" name="w3review" style="margin-bottom:35px" rows="4" cols="25"></textarea></td>
                            <td>
                                <textarea id="w3review" name="w3review" style="margin-bottom:35px" rows="4" cols="25"></textarea></td>
                    </tr>
                   
                </tbody>
            </table>

        </div>
    </div>
    <style>
         .main-omt {
            border: 1px solid #32c5d2;
            background-color: #fff;
        }

            .main-omt .omt-header {
                background-color: #32c5d2;
                padding: 4px 7px;
                display: flex;
            }

        .omt-header .header-title {
            font-size: 20px;
            padding: 10px 10px;
            color: white;
        }

        .omt-header .omt__icon {
            font-size: 30px;
            padding: 8px 10px;
            color: white;
        }

        .omt-top {
            display: flex;
            padding: 0 20px;
        }

            .omt-top .form-omt {
                width: 17%;
                height: 35px !important;
                margin-right: 15px;
                margin-top: 10px;
            }

       

        .form-group__omt {
            margin-top: 10px;
            width: 20%;
         
        }
        .form-control{
            height:36px;
        }
        .omt-title-top {
            padding: 20px;
            background-color: #fbe1e3;
            margin: 0 20px;
            border-radius: 2px;
        }

            .omt-title-top .omt-title-top-title {
                font-size: 12px;
                color: #e73d4a;
                font-weight: 600;
            }

            .omt-title-top .omt-title-top-p {
                font-size: 12px;
                color: #e73d4a;
            }

        .omt-title-bot {
            padding: 20px;
            background-color: #faeaa9;
            margin: 15px 20px;
            border-radius: 2px;
        }

            .omt-title-bot .omt-title-top-p {
                margin: 0;
                font-size: 12px;
                font-weight: 600;
            }

        .header-th {
            width: 25px;
        }

        .thead-omt {
            background-color: #3598dc;
            color: white;
            text-align: center;
        }

        .table thead th {
         vertical-align:middle;
        }
        .table-omt__icon{
            font-size:50px;
            padding-left:23px;
          cursor:pointer;
}
    </style>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

