﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="admin_omt_DanhGiaCuoiTuan.aspx.cs" Inherits="admin_page_module_function_module_OMT_admin_omt_DanhGiaCuoiTuan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <script>
        function myChiTietHocSinh(id) {
            document.getElementById("<%=txtHocSinh.ClientID%>").value = id;
            document.getElementById("<%=btnChiTietHocSinh.ClientID%>").click();
        }
       <%-- function myRatting5(id) {
            document.getElementById("<%=txtRatting.ClientID%>").value = id;
        }
        function myRatting4(id) {
            document.getElementById("<%=txtRatting.ClientID%>").value = id;
        }
        function myRatting3(id) {
            document.getElementById("<%=txtRatting.ClientID%>").value = id;
        }
        function myRatting2(id) {
            document.getElementById("<%=txtRatting.ClientID%>").value = id;
        }
        function myRatting1(id) {
            document.getElementById("<%=txtRatting.ClientID%>").value = id;
        }--%>
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="main-omt">
        <div class="omt-header">
            <i class="fa fa-list-alt omt__icon" aria-hidden="true"></i>
            <h4 class="header-title">Nhận xét thường xuyên</h4>
        </div>
        <div class="omt-top">
            <div class="wrapper__select">
                <div class="col-5" style="margin-right: 10px;">
                    <asp:DropDownList ID="ddlTuan" runat="server" CssClass="form-control"></asp:DropDownList>
                </div>
                <div class="col-4" style="margin-right: 10px;">
                    <asp:DropDownList ID="ddlLop" runat="server" CssClass="form-control"></asp:DropDownList>

                </div>
                <div class="col-2">
                    <a href="#" id="btnXem" class="btn btn-primary" runat="server" onserverclick="btnXem_ServerClick">Xem</a>
                </div>
            </div>
        </div>
         <div style="margin-left:20px;color:blue;">
              <span>Lưu ý:</span><br />
               <%-- <span> - Hiện tại chỉ thí điểm cho khối THCS.</span><br />--%>
                <span>- Giáo viên chỉ ghi những trường hợp cá biệt ví dụ:  </span>
                <br />
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+ Học sinh tiến bộ vượt bậc. </span>
                <br />
                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+ Học sinh hạn chế trong học tập và rèn luyện. </span>
                <br />
             <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+ Nhập thang điểm từ 1 tới 10 cho phần đánh giá. </span>
                <br />
                <span>- Các trường hợp bình thường không cần ghi vào.</span>
            </div>
        <div style="display: none">
            <input type="text" id="txtHocSinh" runat="server" />
            <input type="text" id="txtRatting" runat="server" />
            <a href="#" id="btnChiTietHocSinh" runat="server" onserverclick="btnChiTietHocSinh_ServerClick"></a>
        </div>
        <div class="fixed-table-container">
            <div class="table-left">
                <div class="portlet">
                    <div class="portlet-title">
                        <div class="caption">
                            <span class="caption-title">Danh Sách Học Sinh</span>
                        </div>
                    </div>
                    <div id="main">
                        <div class="container">

                            <div class="group-tabs">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs">
                                    <asp:Repeater ID="rpListHocSinh" runat="server">
                                        <ItemTemplate>
                                            <li class="nav-item"><a href="#" id="btnChiTiet" onclick="myChiTietHocSinh(<%#Eval("hocsinh_id") %>)" data-toggle="tab"><%=STT++ %>/ <%#Eval("hocsinh_fullname") %></a>
                                                <label class="label col-form-label-sm label-success"><%#Eval("ratting") %></label>
                                            </li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-content">
                <div id="tunganh" class="tab-pane fade in active table-right">
                    <div class="portright">
                        <div class="portright-title">
                            <div class="caption">
                                <span class="caption-title">Thông Tin nhận xét Học Sinh</span>
                            </div>
                        </div>
                        <div class="portright-body">
                            <div class="portright-row">
                                <div class="mt-widget-2">
                                    <div class="mt-body">
                                        <img src=" <%=hocsinh_image %>" style="width: 150px; height: 200px" />
                                        <h3 class="mt-body-title">
                                            <%=hotenhocsinh %>
                                        </h3>
                                        <div class="form-group">
                                            <div class="form-title">
                                                <h3>Biểu mẫu đánh giá thường xuyên</h3>
                                            </div>
                                            <div class="form-body">
                                                <table class="table table-bordered">
                                                    <tbody>
                                                        <tr>
                                                            <th>
                                                                <div class="table-NX">
                                                                    <p class="table-title" style="vertical-align: top">Môn học:</p>
                                                                    <textarea class="table-area" id="txtMon" runat="server" name="w3review" rows="2" cols="5" style="font: initial; width: 100%"></textarea>
                                                                </div>
                                                            </th>
                                                            <th>
                                                                <div class="table-NX">
                                                                    <p class="table-title" style="vertical-align: top">Nhận xét:</p>
                                                                    <textarea class="table-area" id="txtNhanXet" runat="server" name="w3review" rows="2" cols="25" style="font: initial; width: 100%"></textarea>
                                                                </div>
                                                            </th>
                                                              <th>
                                                                <div class="table-NX">
                                                                    <p class="table-title" style="vertical-align: top">Đánh giá:</p>
                                                                    <textarea class="table-area" id="txtDanhGia" runat="server" name="w3review" rows="2" cols="5" style="font: initial; width: 100%"></textarea>
                                                                </div>
                                                            </th>
                                                            <%--<th>
                                                                <p class="table-raiting">Đánh Giá:</p>
                                                                <fieldset class="rating one">
                                                                    <input type="radio" id="star5" onclick="myRatting5(5)" name="rating" value="5" />
                                                                    <label class="full" for="star5" title=""></label>

                                                                    <input type="radio" id="star4" onclick="myRatting4(4)" name="rating" value="4" />
                                                                    <label class="full" for="star4" title=""></label>

                                                                    <input type="radio" id="star3" onclick="myRatting3(3)" name="rating" value="3" />
                                                                    <label class="full" for="star3" title=""></label>

                                                                    <input type="radio" id="star2" onclick="myRatting2(2)" name="rating" value="2" />
                                                                    <label class="full" for="star2" title=""></label>

                                                                    <input type="radio" id="star1" onclick="myRatting1(1)" name="rating" value="1" />
                                                                    <label class="full" for="star1" title=""></label>
                                                                </fieldset>
                                                            </th>--%>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <div class="form-fit">
                                                    <a href="#" class="btn btn-primary" id="btnLuu" runat="server" onserverclick="btnLuu_ServerClick">
                                                        <i class="fa fa-pencil-square-o" aria-hidden="true">Lưu</i>
                                                    </a>
                                                    <a href="#" class="btn btn-primary" id="btncapNhat" runat="server" onserverclick="btncapNhat_ServerClick">
                                                        <i class="fa fa-pencil-square-o" aria-hidden="true">Cập nhật</i>
                                                    </a>
                                                    <%--   <a class="btn btn-danger" style="color: white"><i class="fa fa-trash-o" aria-hidden="true"></i>Xóa</a>--%>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-widget-2">
                                    <div class="mt-body">
                                        <div class="form-group">
                                            <div class="form-title">
                                                <h3>Nhận xét các môn học trong tuần</h3>
                                            </div>
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>Môn Học</th>
                                                        <th>Nhận Xét</th>
                                                        <th>Đánh Giá</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <asp:Repeater ID="rpListTuan" runat="server">
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td><%#Eval("nxct_mon") %></td>
                                                                <td><%#Eval("nxct_nhanxet") %> </td>
                                                                <td>
                                                                    <%#Eval("nxct_danhgia") %>
                                                                   <%-- <img src=" <%#Eval("ratting_image") %>" />--%>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        .main-omt {
            border: 1px solid #32c5d2;
            background-color: #fff;
        }

            .main-omt .omt-header {
                background-color: #32c5d2;
                padding: 4px 7px;
                display: flex;
            }

        .omt-header .header-title {
            font-size: 20px;
            padding: 10px 10px;
            color: white;
        }

        .omt-header .omt__icon {
            font-size: 30px;
            padding: 8px 10px;
            color: white;
        }

        .omt-top {
            display: flex;
            /*padding: 0 20px;*/
        }

            .omt-top .form-omt {
                width: 17%;
                height: 40px !important;
                margin-right: 15px;
                margin-top: 10px;
            }

        .fixed-table-container {
            padding-top: 20px;
        }

        .table-left {
            width: 33.333% !important;
            min-height: 1px;
            padding-left: 15px;
            padding-right: 15px;
        }

        .portlet {
            width: 100% !important;
            border: 1px solid #e7ecf1 !important;
        }

        .portlet-title {
            border-bottom: 1px solid #eef1f5;
            min-height: 48px;
        }

        .caption-title {
            padding: 0;
            display: inline-block;
            margin-left: 5%;
            margin-top: 5px;
            font-size: 25px;
            font-weight: bold;
        }

        .group-tabs ul {
            width: 100%;
        }



            .group-tabs ul li {
                position: relative;
                display: inline-block;
                width: 100%;
                min-height: 20px;
                padding: 10px;
                border-bottom: 1px solid #F4F6F9;
            }

        .click {
            background-color: red;
        }

        .group-tabs ul li:hover {
            background-color: #F4F6F9;
            cursor: pointer;
        }

        .group-tabs ul li a {
            text-decoration: none;
        }

            .group-tabs ul li a:hover {
                text-decoration: none;
            }

        .label {
            font-size: 10px;
            font-weight: 600;
            padding: 1px 3px 1px 3px;
            position: absolute;
            top: 20%;
            right: 0px;
            background-color: #36c6d3;
            color: white;
        }

        .tab-content {
            width: 66.66667% !important;
            min-height: 1px;
            padding-left: 15px;
            padding-right: 15px;
        }

        .table-right {
            width: 100%;
            min-height: 1px;
        }

        .portright {
            width: 100% !important;
            border: 1px solid #e7ecf1 !important;
            margin-bottom: 15px;
        }

        .portright-title {
            border-bottom: 1px solid #eef1f5;
            min-height: 48px;
        }

        .fixed-table-container {
            display: flex;
        }

        .portright-body {
            padding: 10px 20px 20px 20px;
        }

        portright-row {
            margin-left: -15px;
            margin-right: -15px;
        }

        .mt-widget-2 {
            border: 1px solid #e7ecf1;
            position: relative;
        }

        .mt-body {
            padding-left: 15px;
            padding-top: 0px;
        }

        .mt-body-title {
            font-size: 20px;
            margin-top: 15px;
            font-weight: bold;
        }

        .mt-body-title-1 {
        }

        .form-group h3 {
            font-size: 22px;
            margin-top: 20px;
            margin-bottom: 10px;
            font-weight: 300;
        }

        .table-NX {
            vertical-align: top;
        }

        .form-group .table-title p {
            font-size: 0.9rem;
            color: #4f5f6f;
        }

        .table-title {
        }

        .table-raiting {
            position: relative;
            top: -15px;
            margin: 0;
        }

        .wrapper__select {
            margin: 10px;
        }

        /*        @import url(//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css);*/

        /*        fieldset, label {
            margin: 0;
            padding: 0;
        }

        body {
            margin: 20px;
        }

        h1 {
            font-size: 1.5em;
            margin: 10px;
        }*/

        /****** Style Star Rating Widget *****/

        .rating {
            border: none;
            float: left;
            margin-left: -6px;
        }

            .rating > input {
                display: none;
            }

            .rating > label:before {
                margin: 5px;
                font-size: -1.25em;
                font-family: FontAwesome;
                display: inline-block;
                content: "\f005";
            }

            .rating > .half:before {
                content: "\f089";
                position: absolute;
            }

            .rating > label {
                color: #ddd;
                float: right;
            }

            /***** CSS Magic to Highlight Stars on Hover *****/

            .rating > input:checked ~ label, /* show gold star when clicked */
            .rating:not(:checked) > label:hover, /* hover current star */
            .rating:not(:checked) > label:hover ~ label {
                color: #FFD700;
            }
                /* hover previous stars in list */

                .rating > input:checked + label:hover, /* hover current star when changing rating */
                .rating > input:checked ~ label:hover,
                .rating > label:hover ~ input:checked ~ label, /* lighten current selection */
                .rating > input:checked ~ label:hover ~ label {
                    color: #FFED85;
                }
    </style>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

