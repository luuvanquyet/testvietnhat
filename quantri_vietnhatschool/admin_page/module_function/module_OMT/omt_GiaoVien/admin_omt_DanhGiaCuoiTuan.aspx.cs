﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_OMT_admin_omt_DanhGiaCuoiTuan : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public int STT = 1;
    public string hotenhocsinh, hocsinh_image;
    cls_Alert alert = new cls_Alert();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            btnLuu.Visible = false;
            btncapNhat.Visible = false;
            var listNV = from l in db.tbLops
                         //join gvtl in db.tbGiaoVienTrongLops on l.lop_id equals gvtl.lop_id
                         //join gv in db.admin_Users on gvtl.taikhoan_id equals gv.username_id
                         select l;
            ddlLop.Items.Clear();
            ddlLop.Items.Insert(0, "Chọn lớp");
            ddlLop.AppendDataBoundItems = true;
            ddlLop.DataTextField = "lop_name";
            ddlLop.DataValueField = "lop_id";
            ddlLop.DataSource = listNV;
            ddlLop.DataBind();
            var listTuan = from l in db.tbHocTap_Tuans where l.tuan_hidden == false && l.namhoc_id==3 select l;
            ddlTuan.Items.Clear();
            ddlTuan.Items.Insert(0, "Chọn tuần");
            ddlTuan.AppendDataBoundItems = true;
            ddlTuan.DataTextField = "tuan_name";
            ddlTuan.DataValueField = "tuan_id";
            ddlTuan.DataSource = listTuan;
            ddlTuan.DataBind();
        }

        //loadData();
    }
    private void loadData()
    {
        if (ddlTuan.SelectedValue == "Chọn tuần" && ddlLop.SelectedValue == "Chọn lớp")
        {
            alert.alert_Error(Page, "Vui lòng chọn tuần và lớp để hiển thỉ danh sách học sinh!", "");
        }
        else
        {
            var checkuserid = (from u in db.admin_Users where u.username_username == Request.Cookies["UserName"].Value select u).First();
            // load data đổ vào var danh sách
            var getData = from l in db.tbLops
                          join hstl in db.tbHocSinhTrongLops on l.lop_id equals hstl.lop_id
                          join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                          where l.lop_id == Convert.ToInt32(ddlLop.SelectedValue)
                          where hstl.namhoc_id==3
                          //orderby hstl.position 
                          select new
                          {
                              STT = STT + 1,
                              hocsinh_fullname = hs.hocsinh_hohocsinh +  hs.hocsinh_name,
                              hstl.hstl_id,
                              hs.hocsinh_id,
                              ratting = (from dg in db.tbHocTap_NhanXetCuoiTuans
                                         where dg.mhstl_id == hs.hocsinh_id && dg.nxct_tuan == ddlTuan.SelectedValue
                                         && dg.giaovien_id == checkuserid.username_id
                                         select dg).Count() > 0 ? "Đã nhận xét" : "Chưa nhận xét"
                          };
            // đẩy dữ liệu vào gridivew
            rpListHocSinh.DataSource = getData;
            rpListHocSinh.DataBind();

        }
    }

    protected void btnChiTietHocSinh_ServerClick(object sender, EventArgs e)
    {
        try
        {
            var checkuserid = (from u in db.admin_Users where u.username_username == Request.Cookies["UserName"].Value select u).First();
            tbHocSinh hs1 = (from hs in db.tbHocSinhs where hs.hocsinh_id == Convert.ToInt32(txtHocSinh.Value) select hs).SingleOrDefault();
            hotenhocsinh = hs1.hocsinh_hohocsinh + hs1.hocsinh_name;
            hocsinh_image = hs1.hocsinh_image;
            var list = from nx in db.tbHocTap_NhanXetCuoiTuans
                       //join i in db.tbImage_Rattings on nx.nxct_danhgia equals i.ratting_id
                       where nx.nxct_tuan == ddlTuan.SelectedValue
                       && nx.mhstl_id == Convert.ToInt32(txtHocSinh.Value)
                       select new
                       {
                           nx.nxct_id,
                           nx.nxct_mon,
                           nx.nxct_nhanxet,
                           nx.nxct_danhgia
                           //i.ratting_image
                       };
            rpListTuan.DataSource = list;
            rpListTuan.DataBind();

            var checkCapNhat = (from up in db.tbHocTap_NhanXetCuoiTuans
                                where up.nxct_tuan == ddlTuan.SelectedValue
                                && up.lop_id == Convert.ToInt16(ddlLop.SelectedValue)
                                && up.mhstl_id == Convert.ToInt32(txtHocSinh.Value)
                                 && up.giaovien_id == checkuserid.username_id
                                select up).SingleOrDefault();
            if (checkCapNhat != null)
            {
                btnLuu.Visible = false;
                btncapNhat.Visible = true;
                txtMon.Value = checkCapNhat.nxct_mon;
            }
            else
            {
                btnLuu.Visible = true;
                btncapNhat.Visible = false;
                txtMon.Value = "";
            }
        }
        catch(Exception ex)
        {
            alert.alert_Error(Page, "Lỗi", "");
        }
    }
    protected void btnLuu_ServerClick(object sender, EventArgs e)
    {
        try
        {
            if (Request.Cookies["UserName"] != null)
            {
                if (txtMon.Value == "" && txtNhanXet.Value == "" && txtRatting.Value == "")
                {
                    alert.alert_Warning(Page, "Vui lòng nhập đầy đủ thông tin!", "");
                }
                else
                {
                    var checkuserid = (from u in db.admin_Users where u.username_username == Request.Cookies["UserName"].Value select u).First();
                    var checkNamHoc = (from nh in db.tbHoctap_NamHocs orderby nh.namhoc_id descending select nh).First();
                    tbHocTap_NhanXetCuoiTuan insert = new tbHocTap_NhanXetCuoiTuan();
                    insert.lop_id = Convert.ToInt16(ddlLop.SelectedValue);
                    insert.nxct_tuan = ddlTuan.SelectedValue;
                    insert.mhstl_id = Convert.ToInt32(txtHocSinh.Value);
                    insert.nxct_mon = txtMon.Value;
                    insert.nxct_nhanxet = txtNhanXet.Value;
                    //insert.nxct_danhgia = Convert.ToInt16(txtRatting.Value);
                    insert.nxct_ngaydanhgia = DateTime.Now;
                    insert.giaovien_id = checkuserid.username_id;
                    insert.namhoc_id = checkNamHoc.namhoc_id;
                    insert.nxct_danhgia = Convert.ToInt16(txtDanhGia.Value);
                    insert.nxct_tinhtrang = "Đã nhận xét";
                    db.tbHocTap_NhanXetCuoiTuans.InsertOnSubmit(insert);
                    db.SubmitChanges();
                    alert.alert_Success(Page, "Hoàn thành nhận xét", "");
                    loadData();
                    btnLuu.Visible = false;
                    btncapNhat.Visible = true;
                    tbHocSinh hs1 = (from hs in db.tbHocSinhs where hs.hocsinh_id == Convert.ToInt32(txtHocSinh.Value) select hs).SingleOrDefault();
                    hotenhocsinh = hs1.hocsinh_hohocsinh + hs1.hocsinh_name;
                    rpListTuan.DataSource = from nx in db.tbHocTap_NhanXetCuoiTuans
                                            //join i in db.tbImage_Rattings on nx.nxct_danhgia equals i.ratting_id
                                            where nx.nxct_tuan == ddlTuan.SelectedValue && nx.mhstl_id == Convert.ToInt32(txtHocSinh.Value)
                                            select new
                                            {
                                                nx.nxct_id,
                                                nx.nxct_mon,
                                                nx.nxct_nhanxet,
                                                nx.nxct_danhgia
                                               // i.ratting_image
                                            };
                    rpListTuan.DataBind();
                }
            }
            else
            {
                alert.alert_Error(Page, "Vui lòng đăng nhập lại", "");
            }
        }
        catch (Exception ex)
        {
            alert.alert_Error(Page, "Lỗi!", "");
        }
    }
    protected void btncapNhat_ServerClick(object sender, EventArgs e)
    {
        var checkuserid = (from u in db.admin_Users where u.username_username == Request.Cookies["UserName"].Value select u).First();
        tbHocTap_NhanXetCuoiTuan update = (from up in db.tbHocTap_NhanXetCuoiTuans
                                           where up.nxct_tuan == ddlTuan.SelectedValue
                                           && up.lop_id == Convert.ToInt16(ddlLop.SelectedValue)
                                           && up.mhstl_id == Convert.ToInt32(txtHocSinh.Value)
                                           && up.giaovien_id == checkuserid.username_id
                                           select up).SingleOrDefault();
        update.nxct_mon = txtMon.Value;
        update.nxct_nhanxet = txtNhanXet.Value;
        update.nxct_danhgia = Convert.ToInt16(txtDanhGia.Value);
        //update.nxct_danhgia = Convert.ToInt16(txtRatting.Value);
        db.SubmitChanges();
        tbHocSinh hs1 = (from hs in db.tbHocSinhs where hs.hocsinh_id == Convert.ToInt32(txtHocSinh.Value) select hs).SingleOrDefault();
        hotenhocsinh = hs1.hocsinh_hohocsinh + hs1.hocsinh_name;
        rpListTuan.DataSource = from nx in db.tbHocTap_NhanXetCuoiTuans
                                //join i in db.tbImage_Rattings on nx.nxct_danhgia equals i.ratting_id
                                where nx.nxct_tuan == ddlTuan.SelectedValue && nx.mhstl_id == Convert.ToInt32(txtHocSinh.Value)
                                select new
                                {
                                    nx.nxct_id,
                                    nx.nxct_mon,
                                    nx.nxct_nhanxet,
                                    nx.nxct_danhgia
                                   //i.ratting_image
                                };
        rpListTuan.DataBind();
        alert.alert_Update(Page, "Cập nhật thành công", "");
    }
    protected void btnXem_ServerClick(object sender, EventArgs e)
    {
        loadData();
    }
}