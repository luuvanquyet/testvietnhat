﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="admin_omt_DanhGiaKyLuat.aspx.cs" Inherits="admin_page_module_function_module_OMT_admin_omt_DanhGiaKyLuat" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <style>
        .omt_container {
            width: 100%;
            border: 1px solid #32c5d2;
            height: auto;
            background-color: #fff;
        }

        .omt_header {
            width: 100%;
            background-color: #32c5d2;
            height: 40px;
            color: #fff;
            padding: 10px;
        }

        .omt_content {
            background-color: #fff;
            border-radius: 5px;
            display: inline-block;
            -webkit-box-shadow: 0 35px 20px #777;
            -moz-box-shadow: 0 35px 20px #777;
            box-shadow: 0 0px 7px #bdbdbd;
            user-select: none;
            cursor: pointer;
            text-align: center;
            font-size: 1.2rem;
            position: relative;
            height: 7rem;
            width: 12rem;
            margin: 1.8rem 1.5rem;
        }

            .omt_content img {
                position: absolute;
                height: 4.4rem;
                width: 4.7rem;
                top: 1rem;
                left: 0px;
                margin: 1rem;
            }

            .omt_content .student-name {
                left: 50px !important;
                font-size: 0.8rem !important;
                white-space: nowrap !important;
                position: absolute;
                width: 12rem;
                height: 6rem;
                top: 0px;
                right: 0px;
                margin: 2rem 0.5rem 2rem 2rem;
                font-size: 1.6rem;
                text-align: left;
            }

            .omt_content .student-point {
                position: absolute;
                top: -2rem;
                right: -2rem;
                padding: 0px;
                margin: 0px;
                border: 0px;
                text-align: right;
            }

                .omt_content .student-point .point-positive {
                    background-color: rgb(147, 213, 83);
                }

                .omt_content .student-point .point-detail {
                    display: inline-block;
                    border: 3px solid white;
                    border-radius: 50% !important;
                    color: white;
                    text-align: center;
                    padding: 0.5rem;
                    box-shadow: rgba(0, 0, 0, 0.15) 0px 1px 3px 0px;
                    font-weight: 600;
                    min-width: 5rem;
                    font-size: 1.8rem;
                }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">

    <div class="omt_container">
        <div class="omt_header">
            <b>Chấm điểm văn minh</b>
        </div>
        <div class="omt_class">
            <select id="cars">
                <option value="volvo">Lớp </option>
                <option value="saab">1/1</option>
                <option value="vw">1/2</option>
                <option value="audi">1/3</option>
            </select>
        </div>
        <a data-toggle="modal" data-target="#exampleModal">
            <div class="omt_content">
                <img src="../../../images/user_noimage.jpg" />
                <div class="student-name">
                    <span>Nguyễn Tùng Anh</span>
                    <div style="color: #6495ED; padding-top: 5px; font-size: 11px">
                        <b>OMT-011</b>
                    </div>
                </div>
                <div class="student-point">
                    <div class="point-detail point-positive title-point-user-3300">
                        100
                    </div>
                </div>
            </div>
        </a>
        <div class="omt_content">
            <img src="../../../images/user_noimage.jpg" />
            <div class="student-name">
                <span>Nguyễn Tùng Anh</span>
                <div style="color: #6495ED; padding-top: 5px; font-size: 11px">
                    <b>OMT-011</b>
                </div>
            </div>
            <div class="student-point">
                <div class="point-detail point-positive title-point-user-3300">
                    100
                </div>
            </div>
        </div>
        <div class="omt_content">
            <img src="../../../images/user_noimage.jpg" />
            <div class="student-name">
                <span>Nguyễn Tùng Anh</span>
                <div style="color: #6495ED; padding-top: 5px; font-size: 11px">
                    <b>OMT-011</b>
                </div>
            </div>
            <div class="student-point">
                <div class="point-detail point-positive title-point-user-3300">
                    100
                </div>
            </div>
        </div>
        <div class="omt_content">
            <img src="../../../images/user_noimage.jpg" />
            <div class="student-name">
                <span>Nguyễn Tùng Anh</span>
                <div style="color: #6495ED; padding-top: 5px; font-size: 11px">
                    <b>OMT-011</b>
                </div>
            </div>
            <div class="student-point">
                <div class="point-detail point-positive title-point-user-3300">
                    100
                </div>
            </div>
        </div>
        <div class="omt_content">
            <img src="../../../images/user_noimage.jpg" />
            <div class="student-name">
                <span>Nguyễn Tùng Anh</span>
                <div style="color: #6495ED; padding-top: 5px; font-size: 11px">
                    <b>OMT-011</b>
                </div>
            </div>
            <div class="student-point">
                <div class="point-detail point-positive title-point-user-3300">
                    100
                </div>
            </div>
        </div>
        <div class="omt_content">
            <img src="../../../images/user_noimage.jpg" />
            <div class="student-name">
                <span>Nguyễn Tùng Anh</span>
                <div style="color: #6495ED; padding-top: 5px; font-size: 11px">
                    <b>OMT-011</b>
                </div>
            </div>
            <div class="student-point">
                <div class="point-detail point-positive title-point-user-3300">
                    100
                </div>
            </div>
        </div>
    </div>
    <%--link popup--%>
    <%--https://getbootstrap.com/docs/4.0/components/modal/--%>

    <%--link tabs and pills--%>
    <%--https://getbootstrap.com/docs/4.0/components/navs/--%>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Profile</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Contact</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                            <div class="table-omt"> 
                                <table class="table table-bordered">
                                    <thead class="thead-omt">
                                        <tr>
                                            <th scope="col">Group comment
                             <input type="checkbox" name="name" value="" />
                                            </th>
                                            <th scope="col">Ảnh đại diện</th>
                                            <th scope="col">Mã HS</th>
                                            <th scope="col">Họ và Tên</th>
                                            <th scope="col">Giới Tính</th>
                                            <th scope="col">Nhận xét /Dặn dò</th>
                                            <th scope="col">Phản hồi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <th scope="row" class="vertical--omt">
                                                <input type="checkbox" name="name" value="" />
                                            </th>
                                            <td>
                                                <i class="fa fa-user table-omt__icon" aria-hidden="true"></i>
                                            </td>
                                            <td>OMT-011</td>
                                            <td>Nguyễn Tùng Anh</td>
                                            <td>Nam</td>
                                            <td>
                                                <textarea id="w3review" name="w3review" rows="4" cols="50"></textarea></td>
                                        </tr>
                                        <tr>
                                            <th scope="row" class="vertical--omt">2</th>
                                            <td>Jacob</td>
                                            <td>Thornton</td>
                                            <td>@fat</td>
                                        </tr>
                                        <tr>
                                            <th scope="row" class="vertical--omt">3</th>
                                            <td>Larry</td>
                                            <td>the Bird</td>
                                            <td>@twitter</td>
                                        </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                        <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">...</div>
                        <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">...</div>
                    </div>


                </div>
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

