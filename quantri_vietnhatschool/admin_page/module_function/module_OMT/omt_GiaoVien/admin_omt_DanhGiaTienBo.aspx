﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="admin_omt_DanhGiaTienBo.aspx.cs" Inherits="admin_page_module_function_module_OMT_admin_omt_DanhGiaTienBo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="main-omt">
        <div class="omt-header">
            <i class="fa fa-user omt__icon" aria-hidden="true"></i>
            <h4 class="header-title">Điểm Danh</h4>
        </div>
        <div class="omt-top">
            <select class="form-control form-control-sm form-omt">
                <option>1B1</option>
            </select>
            <div class="form-group pmd-textfield pmd-textfield-floating-label form-group__omt">
                <input type="text" class="form-control" id="datepicker" />
            </div>
        </div>

        <div class="omt-title-bot">
            <p class="omt-title-top-p">Lưu ý: Bạn sẽ không thể cập nhật đánh giá tiến bộ cho học sinh khi:</p>
            <p class="omt-title-top-p">1. Đánh giá tiến bộ đã bị khóa, chuyển sang trạng thái kiểm tra trước khi gửi tới phụ huynh</p>
            <p class="omt-title-top-p">2. Đánh giá tiến bộ đã gửi tới phụ huynh</p>
            <p class="omt-title-top-p">3. Đánh giá tiến bộ đã quá hạn cho phép cập nhật</p>
        </div>
        <div class="table-omt omt-title-bot">
            <table class="table table-bordered">
                <thead class="thead-omt">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Mã HS</th>
                        <th scope="col">Họ tên</th>
                        <th scope="col">Ngày sinh</th>
                        <th scope="col">Đặc điểm tính cách của HS</th>
                        <th scope="col">Sở thích</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>1</th>
                        <td>OMT-011</td>
                        <td>Nguyễn Tùng Anh</td>
                        <td>20/09/2000</td>
                        <td> <textarea id="txt_1" name="w3review" style="margin-bottom: 35px; width:100%" rows="4" cols="25"></textarea></td>
                         <td> <textarea id="txt_111" name="w3review" style="margin-bottom: 35px; width:100%" rows="4" cols="25"></textarea></td>
                    </tr>
                    <tr>
                        <th>1</th>
                        <td>OMT-011</td>
                        <td>Nguyễn Tùng Anh</td>
                        <td>20/09/2000</td>
                        <td> <textarea id="txt_12" name="w3review" style="margin-bottom: 35px; width:100%" rows="4" cols="25" ></textarea></td>
                         <td> <textarea id="txt_112" name="w3review" style="margin-bottom: 35px; width:100%" rows="4" cols="25"></textarea></td>
                    </tr>
                    <tr>
                        <th>1</th>
                        <td>OMT-011</td>
                        <td>Nguyễn Tùng Anh</td>
                        <td>20/09/2000</td>
                        <td> <textarea id="txt_13" name="w3review" style="margin-bottom: 35px; width:100%" rows="4" cols="25"></textarea></td>
                         <td> <textarea id="txt_113" name="w3review" style="margin-bottom: 35px; width:100%" rows="4" cols="25"></textarea></td>
                    </tr>
                    <tr>
                        <th>1</th>
                        <td>OMT-011</td>
                        <td>Nguyễn Tùng Anh</td>
                        <td>20/09/2000</td>
                        <td> <textarea id="txt_14" name="w3review" style="margin-bottom: 35px; width:100%" rows="4" cols="25"></textarea></td>
                         <td> <textarea id="txt_114" name="w3review" style="margin-bottom: 35px; width:100%" rows="4" cols="25"></textarea></td>
                    </tr>
                </tbody>
            </table>

        </div>
    </div>
    <style>
        .main-omt {
            border: 1px solid #32c5d2;
            background-color: #fff;
        }

            .main-omt .omt-header {
                background-color: #32c5d2;
                padding: 4px 7px;
                display: flex;
            }

        .omt-header .header-title {
            padding: 10px 10px;
            color: white;
        }

        .omt-header .omt__icon {
            font-size: 35px;
            padding: 5px 10px;
            color: white;
        }

        .omt-top {
            display: flex;
            padding: 0 20px;
        }

        .form-omt {
            width: 10%;
            height: 38px;
            margin-right: 15px;
            margin-top: 10px;
        }

        .form-group__omt {
            margin-top: 10px;
            width: 35%;
        }

        .omt-title-top {
            padding: 20px;
            background-color: #fbe1e3;
            margin: 0 20px;
            border-radius: 2px;
        }

            .omt-title-top .omt-title-top-title {
                font-size: 12px;
                color: #e73d4a;
                font-weight: 600;
            }

            .omt-title-top .omt-title-top-p {
                font-size: 12px;
                color: #e73d4a;
            }

        .omt-title-bot {
            padding: 20px;
            background-color: #faeaa9;
            margin: 15px 20px;
            border-radius: 2px;
        }

            .omt-title-bot .omt-title-top-p {
                margin: 0;
                font-size: 12px;
                font-weight: 600;
            }

        .header-th {
            width: 25px;
        }

        .thead-omt {
            background-color: #3598dc;
            color: white;
            text-align: center;
        }

        .table thead th {
            vertical-align: middle;
        }

        .table-omt__icon {
            font-size: 50px;
            padding-left: 23px;
            pointer;
        }
    </style>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

