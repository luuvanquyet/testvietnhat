﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="admin_omt_NhanXetHangNgay1.aspx.cs" Inherits="admin_page_module_function_module_OMT_admin_omt_NhanXetHangNgay1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" Runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" Runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" Runat="Server">
     <%--Tạo khung Cha--%>
    <div class="main-omt">
        <div class="omt-header">
            <h4 class="omt-header__title">Cập nhật nhận xét thường xuyên</h4>
        </div>
        <div class="omt-top">
            <%--https://getbootstrap.com/docs/4.0/components/forms/--%>
            <%--Select--%>
            <select class="form-control form-control-sm form-omt">
                <option>Small select</option>
            </select>
            <%--Datetime--%>
            <div class="form-group pmd-textfield pmd-textfield-floating-label form-group__omt">
                <input type="text" class="form-control" id="datepicker" />
            </div>
            <%--Button--%>
            <div class="omt-button">
                <a class="btn btn-primary btn--omt" href="#">Export</a>
                <a class="btn btn-primary btn--omt" href="#">Import</a>
            </div>
            <%--Group--%>
            <a class="btn btn-success btn--omt" href="#">Group Comment</a>
        </div>

        <div class="content-omt__heading">
            <h3>Lưu ý: Bạn sẽ không thể cập nhật nhận xét/dặn dò khi:</h3>
            <p>1. Nhận xét/dặn dò đã bị khóa</p>
            <p>2. Nhận xét/dặn dò đã quá hạn cho phép cập nhật</p>
        </div>
        <%--link icon--%>
        <%--https://fontawesome.com/v4.7.0/icons/--%>
        <%--link textarea--%>
        <%--https://www.w3schools.com/tags/tag_textarea.asp--%>
        <div class="table-omt">
            <table class="table table-bordered">
                <thead class="thead-omt">
                    <tr>
                        <th scope="col">Group comment
                             <input type="checkbox" name="name" value="" />
                        </th>
                        <th scope="col">Ảnh đại diện</th>
                        <th scope="col">Mã HS</th>
                        <th scope="col">Họ và Tên</th>
                        <th scope="col">Giới Tính</th>
                        <th scope="col">Nhận xét /Dặn dò</th>
                        <th scope="col">Phản hồi</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row" class="vertical--omt">
                            <input type="checkbox" name="name" value="" />
                        </th>
                        <td>
                            <i class="fa fa-user table-omt__icon" aria-hidden="true"></i>
                        </td>
                        <td>OMT-011</td>
                        <td>Nguyễn Tùng Anh</td>
                        <td>Nam</td>
                        <td>
                            <textarea id="w3review" name="w3review" rows="4" cols="50"></textarea></td>
                    </tr>
                    <tr>
                        <th scope="row" class="vertical--omt">2</th>
                        <td>Jacob</td>
                        <td>Thornton</td>
                        <td>@fat</td>
                    </tr>
                    <tr>
                        <th scope="row" class="vertical--omt">3</th>
                        <td>Larry</td>
                        <td>the Bird</td>
                        <td>@twitter</td>
                    </tr>
                </tbody>
            </table>

        </div>



    </div>

    <style>
        .main-omt {
            border: 1px solid #32c5d2;
            padding: 20px;
            background-color: #fff;
        }

            .main-omt .omt-header {
                background-color: #32c5d2;
                padding: 15px 7px;
            }

                .main-omt .omt-header .omt-header__title {
                    color: #fff;
                    font-weight: 600;
                }

        .omt-top {
            display: flex;
            align-items: center;
            padding: 10px 17px;
        }

        .form-omt {
            width: 20%;
            margin-right: 17px;
            height: 20%;
        }

        .form-group__omt {
            margin-bottom: 0;
        }

        .btn--omt {
            margin-bottom: 0;
        }

        .omt-button {
            margin: 0 17px;
        }

        .content-omt__heading {
            background-color: #c0edf1;
            padding: 17px 20px;
            margin-bottom: 20px;
        }

            .content-omt__heading p {
                margin-bottom: 5px;
            }

            .content-omt__heading h3 {
                font-size: 16px;
            }

        .table-omt {
            text-align: center;
        }

        .table-omt__icon {
            font-size: 50px;
        }

        .vertical--omt {
            text-align: center;
        }

        .thead-omt {
            background-color: #32c5d2;
            color: #fff;
        }
    </style>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" Runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" Runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" Runat="Server">
</asp:Content>

