﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_OMT_omt_GiaoVien_admin_omt_NhapThoiKhoaBieuGiaoVien : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            loaddata();
        }
    }
    public void loaddata()
    {
        if (Request.Cookies["UserName"] != null)
        {
            var checkuserid = (from u in db.admin_Users where u.username_username == Request.Cookies["UserName"].Value select u).First();
            var getList = from tkb in db.tbThoiKhoaBieuTungGiaoViens where tkb.giaovien_id == checkuserid.username_id select tkb;
            // Thứ 2
            txtTiet1_Thu2.Value = getList.First().tkb_thu2;
            txtTiet2_Thu2.Value = getList.Skip(1).Take(1).Single().tkb_thu2;
            txtTiet3_Thu2.Value = getList.Skip(2).Take(1).Single().tkb_thu2;
            txtTiet4_Thu2.Value = getList.Skip(3).Take(1).Single().tkb_thu2;
            txtTiet5_Thu2.Value = getList.Skip(4).Take(1).Single().tkb_thu2;
            txtTiet6_Thu2.Value = getList.Skip(5).Take(1).Single().tkb_thu2;
            txtTiet7_Thu2.Value = getList.Skip(6).Take(1).Single().tkb_thu2;
            txtTiet8_Thu2.Value = getList.Skip(7).Take(1).Single().tkb_thu2;
            // Thứ 3
            txtTiet1_Thu3.Value = getList.First().tkb_thu3;
            txtTiet2_Thu3.Value = getList.Skip(1).Take(1).Single().tkb_thu3;
            txtTiet3_Thu3.Value = getList.Skip(2).Take(1).Single().tkb_thu3;
            txtTiet4_Thu3.Value = getList.Skip(3).Take(1).Single().tkb_thu3;
            txtTiet5_Thu3.Value = getList.Skip(4).Take(1).Single().tkb_thu3;
            txtTiet6_Thu3.Value = getList.Skip(5).Take(1).Single().tkb_thu3;
            txtTiet7_Thu3.Value = getList.Skip(6).Take(1).Single().tkb_thu3;
            txtTiet8_Thu3.Value = getList.Skip(7).Take(1).Single().tkb_thu3;
            // Thứ 4
            txtTiet1_Thu4.Value = getList.First().tkb_thu4;
            txtTiet2_Thu4.Value = getList.Skip(1).Take(1).Single().tkb_thu4;
            txtTiet3_Thu4.Value = getList.Skip(2).Take(1).Single().tkb_thu4;
            txtTiet4_Thu4.Value = getList.Skip(3).Take(1).Single().tkb_thu4;
            txtTiet5_Thu4.Value = getList.Skip(4).Take(1).Single().tkb_thu4;
            txtTiet6_Thu4.Value = getList.Skip(5).Take(1).Single().tkb_thu4;
            txtTiet7_Thu4.Value = getList.Skip(6).Take(1).Single().tkb_thu4;
            txtTiet8_Thu4.Value = getList.Skip(7).Take(1).Single().tkb_thu4;
            // Thứ 5
            txtTiet1_Thu5.Value = getList.First().tkb_thu5;
            txtTiet2_Thu5.Value = getList.Skip(1).Take(1).Single().tkb_thu5;
            txtTiet3_Thu5.Value = getList.Skip(2).Take(1).Single().tkb_thu5;
            txtTiet4_Thu5.Value = getList.Skip(3).Take(1).Single().tkb_thu5;
            txtTiet5_Thu5.Value = getList.Skip(4).Take(1).Single().tkb_thu5;
            txtTiet6_Thu5.Value = getList.Skip(5).Take(1).Single().tkb_thu5;
            txtTiet7_Thu5.Value = getList.Skip(6).Take(1).Single().tkb_thu5;
            txtTiet8_Thu5.Value = getList.Skip(7).Take(1).Single().tkb_thu5;
            // Thứ 6
            txtTiet1_Thu6.Value = getList.First().tkb_thu6;
            txtTiet2_Thu6.Value = getList.Skip(1).Take(1).Single().tkb_thu6;
            txtTiet3_Thu6.Value = getList.Skip(2).Take(1).Single().tkb_thu6;
            txtTiet4_Thu6.Value = getList.Skip(3).Take(1).Single().tkb_thu6;
            txtTiet5_Thu6.Value = getList.Skip(4).Take(1).Single().tkb_thu6;
            txtTiet6_Thu6.Value = getList.Skip(5).Take(1).Single().tkb_thu6;
            txtTiet7_Thu6.Value = getList.Skip(6).Take(1).Single().tkb_thu6;
            txtTiet8_Thu6.Value = getList.Skip(7).Take(1).Single().tkb_thu6;
        }
    }
    protected void btnLuu_ServerClick(object sender, EventArgs e)
    {
        if (Request.Cookies["UserName"] != null)
        {
            var checkuserid = (from u in db.admin_Users where u.username_username == Request.Cookies["UserName"].Value select u).First();
            var getList = from tkb in db.tbThoiKhoaBieuTungGiaoViens
                          where tkb.giaovien_id == checkuserid.username_id
                          select tkb;
            // Thứ 2
            getList.First().tkb_thu2 = txtTiet1_Thu2.Value;
            getList.Skip(1).Take(1).Single().tkb_thu2 = txtTiet2_Thu2.Value;
            getList.Skip(2).Take(1).Single().tkb_thu2 = txtTiet3_Thu2.Value;
            getList.Skip(3).Take(1).Single().tkb_thu2 = txtTiet4_Thu2.Value;
            getList.Skip(4).Take(1).Single().tkb_thu2 = txtTiet5_Thu2.Value;
            getList.Skip(5).Take(1).Single().tkb_thu2 = txtTiet6_Thu2.Value;
            getList.Skip(6).Take(1).Single().tkb_thu2 = txtTiet7_Thu2.Value;
            getList.Skip(7).Take(1).Single().tkb_thu2 = txtTiet8_Thu2.Value;
            // Thứ 3
            getList.First().tkb_thu3 = txtTiet1_Thu3.Value;
            getList.Skip(1).Take(1).Single().tkb_thu3 = txtTiet2_Thu3.Value;
            getList.Skip(2).Take(1).Single().tkb_thu3 = txtTiet3_Thu3.Value;
            getList.Skip(3).Take(1).Single().tkb_thu3 = txtTiet4_Thu3.Value;
            getList.Skip(4).Take(1).Single().tkb_thu3 = txtTiet5_Thu3.Value;
            getList.Skip(5).Take(1).Single().tkb_thu3 = txtTiet6_Thu3.Value;
            getList.Skip(6).Take(1).Single().tkb_thu3 = txtTiet7_Thu3.Value;
            getList.Skip(7).Take(1).Single().tkb_thu3 = txtTiet8_Thu3.Value;
            // Thứ 4
            getList.First().tkb_thu4 = txtTiet1_Thu4.Value;
            getList.Skip(1).Take(1).Single().tkb_thu4 = txtTiet2_Thu4.Value;
            getList.Skip(2).Take(1).Single().tkb_thu4 = txtTiet3_Thu4.Value;
            getList.Skip(3).Take(1).Single().tkb_thu4 = txtTiet4_Thu4.Value;
            getList.Skip(4).Take(1).Single().tkb_thu4 = txtTiet5_Thu4.Value;
            getList.Skip(5).Take(1).Single().tkb_thu4 = txtTiet6_Thu4.Value;
            getList.Skip(6).Take(1).Single().tkb_thu4 = txtTiet7_Thu4.Value;
            getList.Skip(7).Take(1).Single().tkb_thu4 = txtTiet8_Thu4.Value;
            // Thứ 5
            getList.First().tkb_thu5 = txtTiet1_Thu5.Value;
            getList.Skip(1).Take(1).Single().tkb_thu5 = txtTiet2_Thu5.Value;
            getList.Skip(2).Take(1).Single().tkb_thu5 = txtTiet3_Thu5.Value;
            getList.Skip(3).Take(1).Single().tkb_thu5 = txtTiet4_Thu5.Value;
            getList.Skip(4).Take(1).Single().tkb_thu5 = txtTiet5_Thu5.Value;
            getList.Skip(5).Take(1).Single().tkb_thu5 = txtTiet6_Thu5.Value;
            getList.Skip(6).Take(1).Single().tkb_thu5 = txtTiet7_Thu5.Value;
            getList.Skip(7).Take(1).Single().tkb_thu5 = txtTiet8_Thu5.Value;
            // Thứ 6
            getList.First().tkb_thu6 = txtTiet1_Thu6.Value;
            getList.Skip(1).Take(1).Single().tkb_thu6 = txtTiet2_Thu6.Value;
            getList.Skip(2).Take(1).Single().tkb_thu6 = txtTiet3_Thu6.Value;
            getList.Skip(3).Take(1).Single().tkb_thu6 = txtTiet4_Thu6.Value;
            getList.Skip(4).Take(1).Single().tkb_thu6 = txtTiet5_Thu6.Value;
            getList.Skip(5).Take(1).Single().tkb_thu6 = txtTiet6_Thu6.Value;
            getList.Skip(6).Take(1).Single().tkb_thu6 = txtTiet7_Thu6.Value;
            getList.Skip(7).Take(1).Single().tkb_thu6 = txtTiet8_Thu6.Value;
            db.SubmitChanges();
            alert.alert_Update(Page, "Đã hoàn thành thời khóa biểu", "");
            loaddata();
        }
    }
}