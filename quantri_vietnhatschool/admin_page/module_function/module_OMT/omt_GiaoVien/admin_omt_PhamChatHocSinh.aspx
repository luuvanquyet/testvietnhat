﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="admin_omt_PhamChatHocSinh.aspx.cs" Inherits="admin_page_module_function_module_OMT_admin_omt_PhamChatHocSinh" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="main-omt">
        <div class="omt-header">
            <i class="fa fa-user omt__icon" aria-hidden="true"></i>
            <h4 class="header-title">Phẩm chất học sinh</h4>
        </div>
        <div class="omt-top">
            <select class="form-control form-control-sm form-omt">
                <option>Lớp</option>
            </select>
            <div class="form-group pmd-textfield pmd-textfield-floating-label form-group__omt">
                <input type="text" class="form-control" id="datepicker" />
            </div>
        </div>
        <div class="table-omt omt-title-bot">
            <table class="table table-bordered">
                <thead class="thead-omt">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Ảnh đại diện</th>
                        <th scope="col">Mã HS</th>
                        <th scope="col">Họ tên</th>
                        <th scope="col">Ngày sinh</th>
                        <th scope="col">Học kì 1</th>
                        <th scope="col">Học kì 2</th>
                        <th scope="col">Cả năm</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>1</th>
                        <td>
                            <img src="/images/user_noimage.jpg" style="width:100px;height:100px"/></td>
                        <td>OMT-011</td>
                        <td>Nguyễn Tùng Anh</td>
                        <td>20/09/2000</td>
                        <td>
                            <textarea id="txt_1" name="w3review" style="margin-bottom: 35px; width: 100%" rows="4" cols="25"></textarea></td>
                        <td>
                            <textarea id="txt_111" name="w3review" style="margin-bottom: 35px; width: 100%" rows="4" cols="25"></textarea></td>
                      <td>
                            <textarea id="txt_1111" name="w3review" style="margin-bottom: 35px; width: 100%" rows="4" cols="25"></textarea></td>
                         </tr>
                    <tr>
                        <th>2</th>
                        <td>
                            <img src="/images/user_noimage.jpg"  style="width:100px;height:100px"/></td>
                        <td>OMT-011</td>
                        <td>Nguyễn Tùng Anh</td>
                        <td>20/09/2000</td>
                        <td>
                            <textarea id="txt_12" name="w3review" style="margin-bottom: 35px; width: 100%" rows="4" cols="25"></textarea></td>
                        <td>
                            <textarea id="txt_112" name="w3review" style="margin-bottom: 35px; width: 100%" rows="4" cols="25"></textarea></td>
                      <td>
                            <textarea id="txt_1112" name="w3review" style="margin-bottom: 35px; width: 100%" rows="4" cols="25"></textarea></td>
                         </tr>
                    <tr>
                        <th>3</th>
                        <td>
                            <img src="/images/user_noimage.jpg"  style="width:100px;height:100px"/></td>
                        <td>OMT-011</td>
                        <td>Nguyễn Tùng Anh</td>
                        <td>20/09/2000</td>
                        <td>
                            <textarea id="txt_13" name="w3review" style="margin-bottom: 35px; width: 100%" rows="4" cols="25"></textarea></td>
                        <td>
                            <textarea id="txt_113" name="w3review" style="margin-bottom: 35px; width: 100%" rows="4" cols="25"></textarea></td>
                     <td>
                            <textarea id="txt_1113" name="w3review" style="margin-bottom: 35px; width: 100%" rows="4" cols="25"></textarea></td>
                          </tr>
                    <tr>
                        <th>4</th>
                        <td>
                            <img src="../../images/user_noimage.jpg" style="width:100px;height:100px"/></td>
                        <td>OMT-011</td>
                        <td>Nguyễn Tùng Anh</td>
                        <td>20/09/2000</td>
                        <td>
                            <textarea id="txt_14" name="w3review" style="margin-bottom: 35px; width: 100%" rows="4" cols="25"></textarea></td>
                        <td>
                            <textarea id="txt_114" name="w3review" style="margin-bottom: 35px; width: 100%" rows="4" cols="25"></textarea></td>
                       <td>
                            <textarea id="txt_1114" name="w3review" style="margin-bottom: 35px; width: 100%" rows="4" cols="25"></textarea></td>
                    </tr>
                </tbody>
            </table>

        </div>
    </div>
    <style>
        .main-omt {
            border: 1px solid #32c5d2;
            background-color: #fff;
        }

            .main-omt .omt-header {
                background-color: #32c5d2;
                padding: 4px 7px;
                display: flex;
            }

        .omt-header .header-title {
            padding: 10px 10px;
            color: white;
        }

        .omt-header .omt__icon {
            font-size: 35px;
            padding: 5px 10px;
            color: white;
        }

        .omt-top {
            display: flex;
            padding: 0 20px;
        }

        .form-omt {
            width: 10%;
            height: 38px;
            margin-right: 15px;
            margin-top: 10px;
        }

        .form-group__omt {
            margin-top: 10px;
            width: 35%;
        }

        .omt-title-top {
            padding: 20px;
            background-color: #fbe1e3;
            margin: 0 20px;
            border-radius: 2px;
        }

            .omt-title-top .omt-title-top-title {
                font-size: 12px;
                color: #e73d4a;
                font-weight: 600;
            }

            .omt-title-top .omt-title-top-p {
                font-size: 12px;
                color: #e73d4a;
            }

        .omt-title-bot {
            padding: 20px;
            background-color: #faeaa9;
            margin: 15px 20px;
            border-radius: 2px;
        }

            .omt-title-bot .omt-title-top-p {
                margin: 0;
                font-size: 12px;
                font-weight: 600;
            }

        .header-th {
            width: 25px;
        }

        .thead-omt {
            background-color: #3598dc;
            color: white;
            text-align: center;
        }

        .table thead th {
            vertical-align: middle;
        }

        .table-omt__icon {
            font-size: 50px;
            padding-left: 23px;
            pointer;
        }
    </style>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

