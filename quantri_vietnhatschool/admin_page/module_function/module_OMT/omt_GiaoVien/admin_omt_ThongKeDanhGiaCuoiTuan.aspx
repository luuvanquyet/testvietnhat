﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="admin_omt_ThongKeDanhGiaCuoiTuan.aspx.cs" Inherits="admin_page_module_function_module_OMT_admin_omt_ThongKeDanhGiaCuoiTuan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <script>
        function myChiTietHocSinh(id) {
            document.getElementById("<%=txtHocSinh.ClientID%>").value = id;
            document.getElementById("<%=btnChiTietHocSinh.ClientID%>").click();
        }
        function myHocSinhID(id, date) {
            document.getElementById("<%=txtHocSinhID.ClientID%>").value = id;
            document.getElementById("<%=txtDate.ClientID%>").value = date;

            document.getElementById("<%=btnChiTiet.ClientID%>").click();
        }

      <%--  function myRatting5(id) {
            document.getElementById("<%=txtRatting.ClientID%>").value = id;
        }
        function myRatting4(id) {
            document.getElementById("<%=txtRatting.ClientID%>").value = id;
        }
        function myRatting3(id) {
            document.getElementById("<%=txtRatting.ClientID%>").value = id;
        }
        function myRatting2(id) {
            document.getElementById("<%=txtRatting.ClientID%>").value = id;
        }
        function myRatting1(id) {
            document.getElementById("<%=txtRatting.ClientID%>").value = id;
        }--%>
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@0.7.0"></script>
    <script src="https://code.jquery.com/jquery-latest.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="main-omt">
        <div class="omt-header">
            <i class="fa fa-list-alt omt__icon" aria-hidden="true"></i>
            <h4 class="header-title">Thống kê kết quả rèn luyện của học sinh</h4>
        </div>
        <div class="omt-top">
            <div class="wrapper__select">
                <div class="col-3" style="margin-right: 10px;">
                    <input type="date" id="dteTuNgay" runat="server" class="form-control" />
                </div>
                <div class="col-3" style="margin-right: 10px;">
                    <input type="date" id="dteDenNgay" runat="server" class="form-control" />
                </div>
                <div class="col-2" style="margin-right: 10px;">
                    <asp:DropDownList ID="ddlLop" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlLop_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                </div>
                <div class="col-2" style="margin-right: 10px;">
                    <asp:DropDownList ID="ddlHocSinh" runat="server" CssClass="form-control"></asp:DropDownList>
                </div>
                <div class="col-1">
                    <a href="#" id="btnXem" class="btn btn-primary" runat="server" onserverclick="btnXem_ServerClick">Xem</a>
                </div>
            </div>
        </div>
        <div style="display: none">
            <input type="text" id="txtHocSinh" runat="server" />
            <input type="text" id="txtRatting" runat="server" />
            <a href="#" id="btnChiTietHocSinh" runat="server"></a>
        </div>
        <div class="wrapper__select">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col" style="width: 15%">Tên học sinh</th>
                        <th scope="col" style="width: 8%">Lớp</th>
                        <th scope="col" style="width: 10%">Môn học</th>
                        <th scope="col" style="width: 12%">Ngày nhận xét</th>
                        <th scope="col" style="width: 52%">Nội dung nhận xét</th>
                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater ID="rpList" runat="server">
                        <ItemTemplate>
                            <tr>
                                <td><%=STT++%></td>
                                <td><%#Eval("hocsinh_name") %></td>
                                <td><%#Eval("lop_name") %></td>
                                <td><%#Eval("nxct_mon") %></td>
                                <td><%#Eval("nxct_ngaydanhgia", "{0: dd-MM-yyyy}") %></td>
                                <td><%#Eval("nxct_nhanxet") %> </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
            <div id="div_BieuDoDuong" runat="server" class="col-12" style="margin-top: 40px;">
                <canvas id="myChart" style="width: 100%"></canvas>
            </div>

            <div style="display: none">
                <input type="text" id="txtNgay" runat="server" />
                <asp:UpdatePanel ID="upPnl" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnChiTiet" runat="server" OnClick="btnChiTiet_Click" />
                    </ContentTemplate>
                </asp:UpdatePanel>
                <input type="text" id="txtHocSinhID" runat="server" />
                <input type="text" id="txtDate" runat="server" />
                <input type="text" id="txtDiemTrungBinh" runat="server" />
                <input type="text" id="txtTenHocSinh" runat="server" />
                <input type="text" id="txtHocSinhCode" runat="server" />
                <input type="text" id="txtTitle" value="Thống kê số lần làm bài tập của các học sinh" />
            </div>

        </div>
        <%--Day la bieu do tron--%>
        <div id="div_BieuDoTron" runat="server" class="col-12" style="margin-top: 10px">
            <canvas id="pie-chart"></canvas>
        </div>
        <div style="display: none;">
            <input type="text" id="txtTenMonHoc1" runat="server" />
            <input type="text" id="txtSoLanKiemTra1" runat="server" />
        </div>

        <%--Day la bieu do cot--%>
        <div id="div_DropDownChonBieuDo" runat="server" class="col-4" style="margin: 30px 10px 30px 0px;">
            <asp:DropDownList ID="ddlThongKe" runat="server" OnSelectedIndexChanged="ddlThongKe_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control">
                <asp:ListItem Value="0" Text="Chọn thống kê" />
                <asp:ListItem Value="1" Text="Theo tháng" />
                <asp:ListItem Value="2" Text="Theo môn học" />
            </asp:DropDownList>
        </div>
        <br />
        <div id="div_TuNgayToiNgay" runat="server" class="col-12" style="margin-bottom : 30px;">
            <div class="col-2" style="margin-right: 10px;">
                <asp:DropDownList ID="ddlHocSinhTheoMon" runat="server" CssClass="form-control"></asp:DropDownList>
            </div>
            <div class="col-2">
                <asp:DropDownList ID="dllMonHoc" runat="server" OnSelectedIndexChanged="dllMonHoc_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control">
                </asp:DropDownList>
            </div>
        </div>
        <br />
        <div id="div_Thang" runat="server" class="col-4" style="margin: 30px 10px 30px 0px;">
            <asp:DropDownList ID="ddlThang" runat="server" OnSelectedIndexChanged="ddlThang_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control">
                <asp:ListItem Value="1" Text="Tháng 1" />
                <asp:ListItem Value="2" Text="Tháng 2" />
                <asp:ListItem Value="3" Text="Tháng 3" />
                <asp:ListItem Value="4" Text="Tháng 4" />
                <asp:ListItem Value="5" Text="Tháng 5" />
                <asp:ListItem Value="6" Text="Tháng 6" />
                <asp:ListItem Value="7" Text="Tháng 7" />
                <asp:ListItem Value="8" Text="Tháng 8" />
                <asp:ListItem Value="9" Text="Tháng 9" />
                <asp:ListItem Value="10" Text="Tháng 10" />
                <asp:ListItem Value="11" Text="Tháng 11" />
                <asp:ListItem Value="12" Text="Tháng 12" />
            </asp:DropDownList>
        </div>
        <div id="div_BieuDoCot" runat="server" class="col-12" style="margin-top: 10px">
            <canvas id="chart"></canvas>
        </div>
        <div style="display: none;">
            <input type="text" id="txtTenMonHoc2" runat="server" />
            <input type="text" id="txtSoLanKiemTra2" runat="server" />
        </div>

        <%-- popup --%>
        <dx:ASPxPopupControl ID="popupControl" runat="server" Width="1200" Height="800" CloseAction="CloseButton" ShowCollapseButton="True" ShowMaximizeButton="True" ScrollBars="Auto" CloseOnEscape="true" Modal="True"
            PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" ClientInstanceName="popupControl" ShowFooter="true"
            HeaderText="" AllowDragging="True" PopupAnimationType="Fade" EnableViewState="False" AutoUpdatePosition="true">
            <ContentCollection>
                <dx:PopupControlContentControl runat="server">
                    <asp:UpdatePanel ID="upRp" runat="server">
                        <ContentTemplate>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col" style="width: 15%">Tên học sinh</th>
                                        <th scope="col" style="width: 8%">Lớp</th>
                                        <th scope="col" style="width: 10%">Môn học</th>
                                        <th scope="col" style="width: 12%">Ngày nhận xét</th>
                                        <th scope="col" style="width: 10%">Điểm</th>
                                        <th scope="col" style="width: 44%">Nội dung nhận xét</th>
                                    </tr>
                                </thead>
                                <tbody>


                                    <asp:Repeater ID="rpChiTiet" runat="server">
                                        <ItemTemplate>
                                            <tr>
                                                <td><%=STT1++%></td>
                                                <td><%#Eval("hocsinh_name") %></td>
                                                <td><%#Eval("lop_name") %></td>
                                                <td><%#Eval("nxct_mon") %></td>
                                                <td><%#Eval("nxct_ngaydanhgia", "{0: dd-MM-yyyy}") %></td>
                                                <td><%#Eval("nxct_danhgia") %></td>
                                                <td><%#Eval("nxct_nhanxet") %> </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </dx:PopupControlContentControl>
            </ContentCollection>

            <FooterContentTemplate>
                <div class="mar_but button">
                    <asp:UpdatePanel ID="udSave" runat="server">
                        <ContentTemplate>
                        </ContentTemplate>
                        <Triggers>
                        </Triggers>
                    </asp:UpdatePanel>
                </div>

            </FooterContentTemplate>
            <ContentStyle>
                <Paddings PaddingBottom="0px" />
            </ContentStyle>
        </dx:ASPxPopupControl>

    </div>

    <%--bieu do tron--%>
    <script>
        for (var a = [], i = 0; i < 25; ++i) a[i] = "#" + (Math.random() * 0xFFFFFF << 0).toString(16).padStart(6, '0');


        var kq_TenMonHoc = document.getElementById("<%=txtTenMonHoc1.ClientID%>").value.replaceAll("'", '"');
        var array_TenMonHoc = JSON.parse("[" + kq_TenMonHoc + "]");

        var kq_SoLanLamBai = document.getElementById("<%=txtSoLanKiemTra1.ClientID%>").value.replaceAll("'", "");
        var array_SoLanLamBai = JSON.parse("[" + kq_SoLanLamBai + "]");

        var data = [{
            data: array_SoLanLamBai,
            backgroundColor: a,
            borderColor: "#fff"
        }];
        var options = {
            tooltips: {
                enabled: true
            },
            plugins: {
                datalabels: {
                    formatter: (value, ctx) => {
                        let sum = ctx.dataset._meta[0].total;
                        let percentage = (value * 100 / sum).toFixed(2) + "%";
                        return percentage;
                    },
                    color: '#ffffff'
                }
            },
            title: {
                display: true,
                text: "Thống kê phần trăm học sinh có số điểm lớn hơn 5, bé hơn 5 và bằng 0"
            },
        };
        var ctx = document.getElementById('pie-chart').getContext('2d'),
            elements = [];
        var myChart = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: array_TenMonHoc,
                datasets: data
            },
            options: options
        });
    </script>

    <%--bieu do cot--%>
    <script>
        for (var a = [], i = 0; i < 25; ++i) a[i] = "#" + (Math.random() * 0xFFFFFF << 0).toString(16).padStart(6, '0');

        /* ĐÂY LÀ NHỮNG GÌ HIỆN RA CỘT X*/
        var kq_TenMonHoc = document.getElementById("<%=txtTenMonHoc2.ClientID%>").value.replaceAll("'", '"');
        var array_TenMonHoc = JSON.parse("[" + kq_TenMonHoc + "]");

        /* ĐÂY LÀ NHỮNG GÌ HIỆN RA CỘT Y*/
        var kq_SoLanLamBai = document.getElementById("<%=txtSoLanKiemTra2.ClientID%>").value.replaceAll("'", "");
        var array_SoLanLamBai = JSON.parse("[" + kq_SoLanLamBai + "]");

        new Chart("chart", {
            type: "bar",
            data: {
                labels: array_TenMonHoc, /*Xvalues*/
                datasets: [{
                    backgroundColor: a,
                    data: array_SoLanLamBai /*Yvalues*/
                }]
            },
            options: {
                legend: { display: false },
                title: {
                    display: true,
                    text: "Biểu đồ thống kê theo môn học hoặc theo tháng"
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            max: 10,
                            min: 0,
                            stepSize: 1
                        }
                    }]
                }
            },
        });
    </script>

    <script>
        var kq_HocSinhCode = document.getElementById("<%=txtHocSinhCode.ClientID%>").value.replaceAll("'", '"');
        var array_hocsinhcode = JSON.parse("[" + kq_HocSinhCode + "]");

        var kq_NgayKiemTra = document.getElementById("<%=txtNgay.ClientID%>").value.replaceAll("'", '"');
        var array_TenNgayKiemTra = JSON.parse("[" + kq_NgayKiemTra + "]");

        var kq_DiemTrungBinh = document.getElementById("<%=txtDiemTrungBinh.ClientID%>").value;
        var elements_DiemTrungBinh = kq_DiemTrungBinh.replaceAll("*", '"')
        var array_DiemTrungBinh = JSON.parse("[" + elements_DiemTrungBinh + "]");

        var kq_tenhocsinh = document.getElementById("<%=txtTenHocSinh.ClientID%>").value.replaceAll("'", '"');
        var array_tenhocsinh = JSON.parse("[" + kq_tenhocsinh + "]");

        console.log(kq_HocSinhCode);
        console.log(array_hocsinhcode);

        var lineChartData = {
            datasets: []
        },
            array = array_DiemTrungBinh;
        console.log(array_DiemTrungBinh)

        var delayInMillisecond = 1000; // 1 second
        setTimeout(
            array.forEach(function (a, i = 0) {
                lineChartData.datasets.push(
                    {
                        code: array_hocsinhcode[i],
                        label: array_tenhocsinh[i++],
                        borderColor: '#' + (0x1000000 + Math.random() * 0xffffff).toString(16).substr(1, 6),
                        backgroundColor: "#fff",
                        fill: false,
                        data: JSON.parse(a),
                    });
            }), delayInMillisecond)


        var options = {
            responsive: true,
            axisY: {
                onlyInteger: true
            },
            tooltips: {
                enabled: true
            },
            elements: {
                line: {
                    tension: 0.3
                }
            },
            scales: {
                yAxes: [{
                    display: true,
                    ticks: {
                        beginAtZero: true,
                        max: 10,
                        min: 0,
                        stepSize: 1,
                    }
                }]
            },
            title: {
                display: true,
                text: "Thống kê điểm trung bình học sinh trong khoảng thời gian"
            },

        };
        var data = {
            labels: array_TenNgayKiemTra,
            datasets: lineChartData.datasets
        }
        var lineCanvas = document.getElementById('myChart');

        var ctx = lineCanvas.getContext('2d');
        elements = [];

        var chartInstance = new Chart(ctx, {
            type: 'line',
            data: data,
            options: options,
        });

        lineCanvas.onclick = function (e) {
            var firstPoint = chartInstance.getElementAtEvent(e)[0];
            if (firstPoint) {
                var firstPoint_dataset_index = firstPoint._datasetIndex
                //console.log("+2 firstPoint_dataset_index::")
                //console.log(firstPoint_dataset_index)

                var label = chartInstance.data.labels[firstPoint._index];
                //console.log("+3 label::")
                console.log(label)

                var code = chartInstance.data.datasets[firstPoint_dataset_index].code;
                //  console.log("+4 code:")
                // console.log(code)
                myHocSinhID(code, label);
            }
        }
    </script>
    <style>
        .main-omt {
            border: 1px solid #32c5d2;
            background-color: #fff;
        }

            .main-omt .omt-header {
                background-color: #32c5d2;
                padding: 4px 7px;
                display: flex;
            }

        .omt-header .header-title {
            font-size: 20px;
            padding: 10px 10px;
            color: white;
        }

        .omt-header .omt__icon {
            font-size: 30px;
            padding: 8px 10px;
            color: white;
        }

        .omt-top {
            display: flex;
            /*padding: 0 20px;*/
        }

            .omt-top .form-omt {
                width: 17%;
                height: 40px !important;
                margin-right: 15px;
                margin-top: 10px;
            }

        .fixed-table-container {
            padding-top: 20px;
        }

        .table-left {
            width: 33.333% !important;
            min-height: 1px;
            padding-left: 15px;
            padding-right: 15px;
        }

        .portlet {
            width: 100% !important;
            border: 1px solid #e7ecf1 !important;
        }

        .portlet-title {
            border-bottom: 1px solid #eef1f5;
            min-height: 48px;
        }

        .caption-title {
            padding: 0;
            display: inline-block;
            margin-left: 5%;
            margin-top: 5px;
            font-size: 25px;
            font-weight: bold;
        }

        .group-tabs ul {
            width: 100%;
        }

            .group-tabs ul li {
                position: relative;
                display: inline-block;
                width: 100%;
                min-height: 20px;
                padding: 10px;
                border-bottom: 1px solid #F4F6F9;
            }

        .click {
            background-color: red;
        }

        .gro p-ta; {
            ackroun F9;
            cu .group tabs text-de oration:;
        }

        u a:hover {
            text-d coration: none;
            .lab l;

        {
            ont-size: 10p;
            -weight: 600;
            d 1px 3px;
            ion: a solut top: 20%;
            i b r: #36 6d3;
            ;
            . ab-content width: 66.666 7% !i min-height: 1px;
            dding-left: 15 x;
            pa 5px;
        }

        . {
            width: 0 min-h ight: .portright;

        {
            width: 1 0% !mporant;
            r id #e7ecf1 !i p margi -botto;
        }

        .po t border b id #eef f5; m x;
        }

        e ainer {
            play: flex;
            .portright- ody 10px 20px 20p;
        }

        po marg; mar
        }

        . 2 border: 1p po ve;
        }

        ody {
            p 5px;
            o;
        }

        y-title {
            z m rgin-top: 15 x ont-weight bold;
            .mt-bod;

        {
            . o 3 font-size: 2 margi -top: 20p ma gin-bo h;
        }

        ve g
        }

        .form-group .table-title p {
            font-size: 0.9rem;
            f6f;
        }

        .table-title {
        }

        .table-raiting {
            position: relative;
            top: -15px;
            margin: 0;
        }

        .wrapper__select {
            margin-top: 10px;
            margin-left: 10px;
            width: 98%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

