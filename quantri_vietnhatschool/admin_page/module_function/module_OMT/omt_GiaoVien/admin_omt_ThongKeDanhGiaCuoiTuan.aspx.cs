﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_OMT_admin_omt_ThongKeDanhGiaCuoiTuan : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public int STT = 1;
    public int STT1 = 1;
    public string hotenhocsinh, hocsinh_image;
    cls_Alert alert = new cls_Alert();
    private static int _idhs = 0;
    private static DateTime date;
    public string monhoc = "";
    public string solan = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            var listNV = from l in db.tbLops
                         select l;
            ddlLop.Items.Clear();
            ddlLop.Items.Insert(0, "Chọn lớp");
            ddlLop.AppendDataBoundItems = true;
            ddlLop.DataTextField = "lop_name";
            ddlLop.DataValueField = "lop_id";
            ddlLop.DataSource = listNV;
            ddlLop.DataBind();

            //drop Down List để hiện môn học. Dùng cho biểu đồ cuối cùng (chọn theo môn học)
            var listMon = from l in db.tbMonHocs
                         select l;
            dllMonHoc.Items.Clear();
            dllMonHoc.Items.Insert(0, "Chọn môn học");
            dllMonHoc.AppendDataBoundItems = true;
            dllMonHoc.DataTextField = "monhoc_name";
            dllMonHoc.DataValueField = "monhoc_id";
            dllMonHoc.DataSource = listMon;
            dllMonHoc.DataBind();

            //anBieuDo();

            if (_idhs != 0)
            {
                ShowData();
            }
        }
    }
    //Dùng để ẩn tất cả các div chưa dùng tới
    private void anBieuDo()
    {
        div_TuNgayToiNgay.Visible = false;
        div_BieuDoTron.Visible = false;
        div_BieuDoDuong.Visible = false;
        div_BieuDoCot.Visible = false;
        div_DropDownChonBieuDo.Visible = false;
        div_Thang.Visible = false;
    }
    private void loadData()
    {
        if (dteTuNgay.Value == "" && dteDenNgay.Value == "")
        {
            alert.alert_Warning(Page, "Vui lòng chọn từ ngày nào đến ngày nào để hiển thỉ danh sách học sinh!", "");
        }
        else
        {
            if (ddlLop.SelectedValue == "Chọn lớp")
            {
                alert.alert_Warning(Page, "Vui lòng chọn lớp để hiển thỉ danh sách học sinh!", "");
            }
            else
            {
                if (ddlHocSinh.SelectedValue == "Tất cả" || ddlHocSinh.SelectedValue == "")
                {
                    var getData = from nx in db.tbHocTap_NhanXetCuoiTuans
                                  join l in db.tbLops on nx.lop_id equals l.lop_id
                                  //join hstl in db.tbHocSinhTrongLops on nx.mhstl_id equals hstl.hocsinh_id
                                  join hs in db.tbHocSinhs on nx.mhstl_id equals hs.hocsinh_id
                                  where nx.lop_id == Convert.ToInt32(ddlLop.SelectedValue)
                                  && nx.nxct_ngaydanhgia >= Convert.ToDateTime(dteTuNgay.Value)
                                  && nx.nxct_ngaydanhgia <= Convert.ToDateTime(dteDenNgay.Value)
                                  select new
                                  {
                                      hocsinh_name = hs.hocsinh_hohocsinh + hs.hocsinh_name,
                                      nx.nxct_mon,
                                      nx.nxct_ngaydanhgia,
                                      nx.nxct_nhanxet,
                                      nx.nxct_danhgia,
                                      l.lop_name,
                                  };

                    rpList.DataSource = getData;
                    rpList.DataBind();
                }
                else
                {
                    var getData = from nx in db.tbHocTap_NhanXetCuoiTuans
                                  join l in db.tbLops on nx.lop_id equals l.lop_id
                                  //join hstl in db.tbHocSinhTrongLops on nx.mhstl_id equals hstl.hocsinh_id
                                  join hs in db.tbHocSinhs on nx.mhstl_id equals hs.hocsinh_id
                                  where nx.lop_id == Convert.ToInt32(ddlLop.SelectedValue)
                                  && hs.hocsinh_id == Convert.ToInt32(ddlHocSinh.SelectedValue)
                                  && nx.nxct_ngaydanhgia >= Convert.ToDateTime(dteTuNgay.Value)
                                  && nx.nxct_ngaydanhgia <= Convert.ToDateTime(dteDenNgay.Value)
                                  select new
                                  {
                                      hocsinh_name = hs.hocsinh_hohocsinh + hs.hocsinh_name,
                                      nx.nxct_mon,
                                      nx.nxct_ngaydanhgia,
                                      nx.nxct_nhanxet,
                                      nx.nxct_danhgia,
                                      l.lop_name,
                                  };
                    rpList.DataSource = getData;
                    rpList.DataBind();
                }

            }
        }
    }
    //Hàm để reset
    public void ResetData()
    {
        dteTuNgay.Value = "";
        dteDenNgay.Value = "";
    }
    protected void btnXem_ServerClick(object sender, EventArgs e)
    {
        if (dteTuNgay.Value == "" || dteDenNgay.Value == "")
        {
            ResetData();
            alert.alert_Warning(Page, "Chưa chọn ngày!", "");
        }
        else if (Convert.ToDateTime(dteTuNgay.Value) >= Convert.ToDateTime(dteDenNgay.Value))
        {
            ResetData();
            alert.alert_Error(Page, "Dữ liệu ngày không hợp lệ !", "");
        }
        else
        {
            div_DropDownChonBieuDo.Visible = true;
            loadData();
            LoadThongKe(Convert.ToDateTime(dteTuNgay.Value), Convert.ToDateTime(dteDenNgay.Value));
            LoadThongKeBieuDoTron();
        }
    }
    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        _idhs = Convert.ToInt32(txtHocSinhID.Value);
        date = DateTime.ParseExact(txtDate.Value, "dd/MM/yyyy", null);
        ShowData();
    }

    public void ShowData()
    {
        var loadData = from nxct in db.tbHocTap_NhanXetCuoiTuans
                       join hs in db.tbHocSinhs on nxct.mhstl_id equals hs.hocsinh_id
                       join lop in db.tbLops on nxct.lop_id equals lop.lop_id
                       where nxct.mhstl_id == _idhs
                       && nxct.lop_id == Convert.ToInt32(ddlLop.SelectedValue)
                       && nxct.nxct_ngaydanhgia.Value.Day == date.Day
                       && nxct.nxct_ngaydanhgia.Value.Month == date.Month
                       && nxct.nxct_ngaydanhgia.Value.Year == date.Year
                       select new
                       {
                           hocsinh_name = hs.hocsinh_hohocsinh + hs.hocsinh_name,
                           nxct.nxct_mon,
                           nxct.nxct_ngaydanhgia,
                           nxct.nxct_nhanxet,
                           nxct.nxct_danhgia,
                           lop.lop_name,
                       };
        rpChiTiet.DataSource = loadData;
        rpChiTiet.DataBind();
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Update", "popupControl.Show();", true);
    }
    //LOAD BIỂU ĐỒ ĐƯỜNG
    protected void LoadThongKe(DateTime tungay, DateTime denngay)
    {
        div_BieuDoDuong.Visible = true;
        var dates = new List<DateTime>();

        //list các học sinh trong lớp có làm bài test
        List<tbHocTap_NhanXetCuoiTuan> list_HocSinh = db.tbHocTap_NhanXetCuoiTuans.Where(test => test.lop_id == Convert.ToInt32(ddlLop.SelectedValue)).GroupBy(test => test.mhstl_id).Select(g => g.First()).ToList();
        string[] list_TenHocSinh = new string[list_HocSinh.Count + 1];
        string[] list_DiemTb = new string[list_HocSinh.Count + 1];

        string arr_day = "";
        string arr_diemtrungbinh = "";
        string arr_tenhocsinh = "";
        string arr_hocsinhcode = "";
        int days = (int)((denngay - tungay).TotalDays);
        string[] list_Day = new string[days + 1];
        int n = 0;

        //lấy được tất cả các ngày từ tungay -> denngay theo định dạng (MM/dd/yyyy hh:mm:ss) thêm vào list dates[]
        for (var dt = tungay; dt <= denngay; dt = dt.AddDays(1))
        {
            dates.Add(dt);
        }

        do
        {
            //dùng chuỗi arr_day lưu các giá trị ngày để xuất lên chart "'dd/MM/yyyy', 'dd/MM/yyyy', ..."
            if (n == days)
            {
                if (dates[n].Month <= 9)
                    arr_day = arr_day + "'" + dates[n].Day + "/0" + dates[n].Month + "/" + dates[n].Year + "'";
                else
                    arr_day = arr_day + "'" + dates[n].Day + "/" + dates[n].Month + "/" + dates[n].Year + "'";
            }
            else
            {
                if (dates[n].Month <= 9)
                    arr_day = arr_day + "'" + dates[n].Day + "/0" + dates[n].Month + "/" + dates[n].Year + "', ";
                else
                    arr_day = arr_day + "'" + dates[n].Day + "/" + dates[n].Month + "/" + dates[n].Year + "', ";
            }
            n++;
        } while (n <= days);


        for (int j = 0; j <= list_HocSinh.Count - 1; j++)
        {
            for (int i = 0; i <= days; i++)
            {
                //tính số lần làm bài trong ngày
                var count = (from kq in db.tbHocTap_NhanXetCuoiTuans
                             where kq.mhstl_id == list_HocSinh[j].mhstl_id
                             && kq.nxct_ngaydanhgia.Value.Day == dates[i].Day
                             && kq.nxct_ngaydanhgia.Value.Month == dates[i].Month
                             && kq.nxct_ngaydanhgia.Value.Year == dates[i].Year
                             select kq).Count();
                if (count != 0)
                {
                    //tính điểm tb
                    var sum_score = (from kq in db.tbHocTap_NhanXetCuoiTuans
                                     where kq.mhstl_id == list_HocSinh[j].mhstl_id
                                     && kq.nxct_ngaydanhgia.Value.Day == dates[i].Day
                                     && kq.nxct_ngaydanhgia.Value.Month == dates[i].Month
                                     && kq.nxct_ngaydanhgia.Value.Year == dates[i].Year
                                     select kq.nxct_danhgia).Sum();

                    list_DiemTb[j] += (Convert.ToInt32(sum_score) / count).ToString() + ", ";
                }
                //ko làm tức là ngày đó k có điểm
                else
                {
                    list_DiemTb[j] += "0,";
                }
            }
        }
        //xử lý các chuỗi để mapping với các txt trên chart
        for (int i = 0; i <= list_HocSinh.Count - 1; i++)
        {
            if (i == list_HocSinh.Count - 1)
            {
                //xử lý thành chuỗi của điểm trung bình của các học sinh "*[...]*, *[...]*, ..."
                arr_diemtrungbinh = arr_diemtrungbinh + "*[" + list_DiemTb[i].TrimEnd(',') + "]*";

                //get tên học sinh vào chuỗi arr_tenhocsinh
                list_TenHocSinh[i] = (from hs in db.tbHocSinhs
                                      where hs.hocsinh_id == list_HocSinh[i].mhstl_id
                                      select hs).FirstOrDefault().hocsinh_name;
                arr_tenhocsinh = arr_tenhocsinh + "'" + list_TenHocSinh[i] + "'";

                //get mã học sinh vào chuỗi arr_hocsinhcode 
                arr_hocsinhcode = arr_hocsinhcode + "'" + list_HocSinh[i].mhstl_id + "'";

            }
            else
            {
                arr_diemtrungbinh = arr_diemtrungbinh + "*[" + list_DiemTb[i].TrimEnd(',') + "]*, ";
                list_TenHocSinh[i] = (from hs in db.tbHocSinhs
                                      where hs.hocsinh_id == list_HocSinh[i].mhstl_id
                                      select hs).FirstOrDefault().hocsinh_name;
                arr_tenhocsinh = arr_tenhocsinh + "'" + list_TenHocSinh[i] + "', ";
                arr_hocsinhcode = arr_hocsinhcode + "'" + list_HocSinh[i].mhstl_id + "', ";
            }
        }

        txtDiemTrungBinh.Value = arr_diemtrungbinh;
        txtTenHocSinh.Value = arr_tenhocsinh.Replace("\r\n", "");
        txtNgay.Value = arr_day;
        txtHocSinhCode.Value = arr_hocsinhcode;
    }

    //LOAD BIỂU ĐỒ TRÒN
    protected void LoadThongKeBieuDoTron()
    {
        div_BieuDoTron.Visible = true;
        //GET RA ĐIỂM LỚN HƠN 5 VÀ BÉ HƠN 5 VÀ BẰNG 0 TRONG SỐ NGÀY ĐƯỢC CHỌN
        var getSoDiemLonHon5 = from kq in db.tbHocTap_NhanXetCuoiTuans
                                   where kq.lop_id == Convert.ToInt32(ddlLop.SelectedValue)
                                  && kq.nxct_ngaydanhgia >= Convert.ToDateTime(dteTuNgay.Value)
                                  && kq.nxct_ngaydanhgia <= Convert.ToDateTime(dteDenNgay.Value)
                                   group kq by kq.lop_id into g
                                   select new
                                   {
                                       g.Key,
                                       soLan = (from kq in db.tbHocTap_NhanXetCuoiTuans
                                                where kq.nxct_danhgia >= 5
                                                select kq).Count(),
                                   };
            var getSoDiemBeHon5 = from kq in db.tbHocTap_NhanXetCuoiTuans
                                  where kq.lop_id == Convert.ToInt32(ddlLop.SelectedValue)
                                  && kq.nxct_ngaydanhgia >= Convert.ToDateTime(dteTuNgay.Value)
                                  && kq.nxct_ngaydanhgia <= Convert.ToDateTime(dteDenNgay.Value)
                                  group kq by kq.lop_id into g
                                  select new
                                  {
                                      g.Key,
                                      soLan = (from kq in db.tbHocTap_NhanXetCuoiTuans
                                               where kq.nxct_danhgia < 5
                                               select kq).Count(),
                                  };
            var getSoDiemBangKhong = from kq in db.tbHocTap_NhanXetCuoiTuans
                                     where kq.lop_id == Convert.ToInt32(ddlLop.SelectedValue)
                                  && kq.nxct_ngaydanhgia >= Convert.ToDateTime(dteTuNgay.Value)
                                  && kq.nxct_ngaydanhgia <= Convert.ToDateTime(dteDenNgay.Value)
                                     group kq by kq.lop_id into g
                                     select new
                                     {
                                         g.Key,
                                         soLan = (from kq in db.tbHocTap_NhanXetCuoiTuans
                                                  where kq.nxct_danhgia == 0
                                                  select kq).Count(),
                                     };
        //THÊM PHẦN TỬ VÀO MẢNG
            foreach (var item in getSoDiemLonHon5)
            {
                solan = solan + "'" + item.soLan + "',";
            }
            foreach (var item in getSoDiemBeHon5)
            {
                solan = solan + "'" + item.soLan + "',";
            }
            foreach (var item in getSoDiemBangKhong)
            {
                solan = solan + "'" + item.soLan + "',";
            }

        string so_lan = solan.TrimEnd(',');
        txtSoLanKiemTra1.Value = so_lan;
        txtTenMonHoc1.Value = "'Lớn hơn 5','Bé hơn 5','Bằng 0'";
    }

    //Hàm dùng để load thống kê theo môn (dùng cho biểu đồ cuối cùng lựa theo môn)
    protected void LoadThongKeTheoMonHoc(string Monhoc)
    {
        div_BieuDoCot.Visible = true;
        //kiểm tra điều kiện
        if (ddlHocSinhTheoMon.SelectedValue == "")
        {
            alert.alert_Warning(Page, "Vui lòng chọn học sinh để hiển thỉ danh sách !", "");
        }
        else
        {
            //tạo mảng các học sinh dựa trên id lớp(chọn ở trên cùng) và id học sinh đã chọn(chỉ chọn được khi bấm vào chọn THEO MÔN HỌC)
            List<tbHocTap_NhanXetCuoiTuan> list_HocSinh = db.tbHocTap_NhanXetCuoiTuans.Where(test => test.lop_id == Convert.ToInt32(ddlLop.SelectedValue) && test.mhstl_id == Convert.ToInt32(ddlHocSinhTheoMon.SelectedValue)).GroupBy(test => test.mhstl_id).Select(g => g.First()).ToList();
            string[] list_TenHocSinh = new string[list_HocSinh.Count + 1];
            //string[] list_MonHoc = new string[2 * list_HocSinh.Count];
            string arr_tenhocsinh = "";
            string arr_monhoc = "";
            string arr_temp = "";
            //DUYỆT QUA TẤT CẢ HỌC SINH CÓ TRONG MẢNG
            for (int j = 0; j <= list_HocSinh.Count - 1; j++)
            {
                //VAR LIST NÀY ĐỂ LẤY RA SỐ ĐIỂM CỦA HỌC SINH ĐƯỢC CHỌN
                var list = (from kq in db.tbHocTap_NhanXetCuoiTuans
                            join hs in db.tbHocSinhs on list_HocSinh[j].mhstl_id equals hs.hocsinh_id
                            where kq.lop_id == Convert.ToInt32(ddlLop.SelectedValue)
                                 && kq.mhstl_id == list_HocSinh[j].mhstl_id
                                 && kq.nxct_mon == Monhoc
                                 && kq.nxct_ngaydanhgia >= Convert.ToDateTime(dteTuNgay.Value)
                                 && kq.nxct_ngaydanhgia <= Convert.ToDateTime(dteDenNgay.Value)
                            select kq.nxct_danhgia).ToList();
                //THÊM TẤT CẢ VÀO MẢNG NÀY
                foreach (var i in list)
                    arr_monhoc = arr_monhoc + "'" + i.ToString() + "',";

                //VAR LIST 2 ĐỂ LẤY RA TÊN HỌC SINH PHÙ HỢP VỚI ID ĐƯỢC CHỌN
                var list2 = (from kq in db.tbHocTap_NhanXetCuoiTuans
                             join hs in db.tbHocSinhs on list_HocSinh[j].mhstl_id equals hs.hocsinh_id
                             where kq.lop_id == Convert.ToInt32(ddlLop.SelectedValue)
                             && kq.mhstl_id == list_HocSinh[j].mhstl_id
                             select hs.hocsinh_name).ToList();
                //THÊM TẤT CẢ VÀO MẢNG NÀY
                foreach (var g in list2)
                    arr_tenhocsinh = arr_tenhocsinh + "'" + g.ToString() + "',";
            }
            txtSoLanKiemTra2.Value = arr_monhoc.TrimEnd(',');
            arr_temp = arr_tenhocsinh.Replace("\r\n", "");
            txtTenMonHoc2.Value = arr_temp.TrimEnd(',');
        }
    }

    //Hàm dùng để load thống kê theo THÁNG (dùng cho biểu đồ cuối cùng lựa theo THÁNG)
    //**********************************************************************************
    protected void LoadThongKeTheoThang(int month)
    {
        div_BieuDoCot.Visible = true;
        //kiểm tra điều kiện
        if (ddlThang.SelectedValue == "")
        {
            alert.alert_Warning(Page, "Vui lòng chọn tháng để hiển thỉ danh sách !", "");
        }
        else
        {

        }
    }
    //**********************************************************************************
    protected void ddlLop_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlLop.SelectedValue == "Chọn lớp")
        {
            ddlHocSinh.Text = "Tất cả";
        }
        else
        {
            var listNV = from l in db.tbLops
                             //join gvtl in db.tbGiaoVienTrongLops on l.lop_id equals gvtl.lop_id
                         join hstl in db.tbHocSinhTrongLops on l.lop_id equals hstl.lop_id
                         join hs in db.tbHocSinhs on hstl.hocsinh_id equals hs.hocsinh_id
                         where l.lop_id == Convert.ToInt32(ddlLop.SelectedValue)
                         select new
                         {
                             hocsinh_name = hs.hocsinh_hohocsinh + hs.hocsinh_name,
                             hs.hocsinh_id
                         };
            ddlHocSinh.Items.Clear();
            ddlHocSinh.Items.Insert(0, "Tất cả");
            ddlHocSinh.AppendDataBoundItems = true;
            ddlHocSinh.DataTextField = "hocsinh_name";
            ddlHocSinh.DataValueField = "hocsinh_id";
            ddlHocSinh.DataSource = listNV;
            ddlHocSinh.DataBind();

            ddlHocSinhTheoMon.Items.Clear();
            ddlHocSinhTheoMon.AppendDataBoundItems = true;
            ddlHocSinhTheoMon.DataTextField = "hocsinh_name";
            ddlHocSinhTheoMon.DataValueField = "hocsinh_id";
            ddlHocSinhTheoMon.DataSource = listNV;
            ddlHocSinhTheoMon.DataBind();
        }
    }

    protected void ddlThongKe_SelectedIndexChanged(object sender, EventArgs e)
    {
        string option = ddlThongKe.Text;
        if (option == "1")
        {
            div_TuNgayToiNgay.Visible = false;
            div_Thang.Visible = true;
        }
        else if (option == "2")
        {
            div_TuNgayToiNgay.Visible = true;
            div_Thang.Visible = false;
        }
    }

    protected void dllMonHoc_SelectedIndexChanged(object sender, EventArgs e)
    {
        string option = dllMonHoc.SelectedItem.ToString();
        LoadThongKeTheoMonHoc(option);
    }

    protected void ddlThang_SelectedIndexChanged(object sender, EventArgs e)
    {
        int month = Convert.ToInt32(ddlThang.SelectedValue);
        LoadThongKeTheoThang(month);
    }
}