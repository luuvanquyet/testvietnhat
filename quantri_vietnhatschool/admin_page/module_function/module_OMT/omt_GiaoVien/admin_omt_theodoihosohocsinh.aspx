﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="admin_omt_theodoihosohocsinh.aspx.cs" Inherits="admin_page_module_function_module_OMT_admin_omt_theodoihosohocsinh" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="main-omt">
        <div class="omt-header">
            <i class="fa fa-server omt__icon" aria-hidden="true"></i>
            <h4 class="header-title">Quản Lý Phê Duyệt Hồ Sơ Học Sinh</h4>
        </div>
        <div class="omt-top">
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Tìm mã,tên..." aria-label="Search"/>
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
            <ul class="nav nav-pills">
                <li class="nav-item dropdown">
                    <a style="border: 1px solid; margin: 10px 10px" class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">--Danh Mục Hồ Sơ--</a>
                    <div class="dropdown-menu" style="width: 175px; margin-left: 10px">
                        <a class="dropdown-item" href="#dmhs" data-toggle="tab">Danh Mục Hồ Sơ</a>
                        <a class="dropdown-item" href="#duadonhs" data-toggle="tab">Hồ Sơ Đưa Đón Học Sinh</a>
                        <a class="dropdown-item" href="#hocba" data-toggle="tab">Học Bạ</a>
                        <a class="dropdown-item" href="#gks" data-toggle="tab">Giấy Khai Sinh</a>
                        <a class="dropdown-item" href="#hsts" data-toggle="tab">Hồ Sơ Tuyển Sinh</a>
                    </div>
                </li>
            </ul>

            <ul class="nav nav-pills">
                <li class="nav-item dropdown">
                    <a style="border: 1px solid; margin: 10px 10px" class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">--Trạng Thái--</a>
                    <div class="dropdown-menu" style="width: 175px; margin-left: 10px">
                        <a class="dropdown-item" href="#">Chờ Phê Duyệt</a>
                        <a class="dropdown-item" href="#">Đã Phê Duyệt</a>
                        <a class="dropdown-item" href="#">Từ Chối Phê Duyệt</a>
                    </div>
                </li>
            </ul>
            <select class="form-control form-control-sm form-omt" style="margin-left: 150px">
                <option>1B1</option>
            </select>
        </div>
        <div class="tab-content">
            <div id="dmhs" class="tab-pane fade in active">
                <table style="width: 98%; margin: 5px 12px" class="table table-bordered table-striped table-condensed flip-content">
                    <thead class="flip-content bg-blue bg-font-blue" style="background-color: #3598dc; color: white">
                        <tr>
                            <th  style="vertical-align: top; text-align: center;width:2%">#</th>
                            <th  style="vertical-align: top; text-align: center;width:10%">Mã học sinh </th>
                            <th  style="vertical-align: top; text-align: center;width:15%">Tên học sinh </th>
                            <th  style="vertical-align: top; text-align: center;width:6%">Lớp</th>
                            <th  style="vertical-align: top; text-align: center;width:6%">Giới tính </th>
                            <th  style="vertical-align: top; text-align: center;width:10%">Ngày sinh </th>
                            <th  style="vertical-align: top; text-align: center;width:10%">Đã hoàn thiện</th>
                            <th style="vertical-align: top; text-align: center">Hồ sơ đưa đón học sinh </th>
                            <th style="vertical-align: top; text-align: center">HỌC BẠ </th>
                            <th style="vertical-align: top; text-align: center">GIẤY KHAI SINH </th>
                            <th style="vertical-align: top; text-align: center">Hồ sơ tuyển sinh </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>OMT-019</td>
                            <td>Đào Phương Anh
                            </td>
                            <td>1B1
                            </td>
                            <td>
                                <span>Nữ</span>
                            </td>
                            <td>10/12/2008
                            </td>
                            <td>0/4
                            </td>
                            <td style="text-align: center"></td>
                            <td style="text-align: center"></td>
                            <td style="text-align: center"></td>
                            <td style="text-align: center"></td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>OMT-018</td>
                            <td>Lê Trang Anh
                            </td>
                            <td>1B1
                            </td>
                            <td>
                                <span>Nữ</span>
                            </td>
                            <td>27/10/2008
                            </td>
                            <td>0/4
                            </td>
                            <td style="text-align: center"></td>
                            <td style="text-align: center"></td>
                            <td style="text-align: center"></td>
                            <td style="text-align: center"></td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>OMT-017</td>
                            <td>Lại Viết Hoàng Anh
                            </td>
                            <td>1B1
                            </td>
                            <td>
                                <span>Nam</span>
                            </td>
                            <td>18/04/2008
                            </td>
                            <td>0/4
                            </td>
                            <td style="text-align: center"></td>
                            <td style="text-align: center"></td>
                            <td style="text-align: center"></td>
                            <td style="text-align: center"></td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>OMT-016</td>
                            <td>Lê Minh Bảo Anh
                            </td>
                            <td>1B1
                            </td>
                            <td>
                                <span>Nữ</span>
                            </td>
                            <td>25/05/2008
                            </td>
                            <td>0/4
                            </td>
                            <td style="text-align: center"></td>
                            <td style="text-align: center"></td>
                            <td style="text-align: center"></td>
                            <td style="text-align: center"></td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>OMT-015</td>
                            <td>Trần Đức Anh
                            </td>
                            <td>1B1
                            </td>
                            <td>
                                <span>Nam</span>
                            </td>
                            <td>12/12/2008
                            </td>
                            <td>0/4
                            </td>
                            <td style="text-align: center"></td>
                            <td style="text-align: center"></td>
                            <td style="text-align: center"></td>
                            <td style="text-align: center"></td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td>OMT-014</td>
                            <td>Phạm Trâm Anh
                            </td>
                            <td>1B1
                            </td>
                            <td>
                                <span>Nữ</span>
                            </td>
                            <td>16/07/2008
                            </td>
                            <td>0/4
                            </td>
                            <td style="text-align: center"></td>
                            <td style="text-align: center"></td>
                            <td style="text-align: center"></td>
                            <td style="text-align: center"></td>
                        </tr>
                        <tr>
                            <td>7</td>
                            <td>OMT-013</td>
                            <td>Phạm Thị Tuyết Anh
                            </td>
                            <td>1B1
                            </td>
                            <td>
                                <span>Nữ</span>
                            </td>
                            <td>19/09/2008
                            </td>
                            <td>0/4
                            </td>
                            <td style="text-align: center"></td>
                            <td style="text-align: center"></td>
                            <td style="text-align: center"></td>
                            <td style="text-align: center"></td>
                        </tr>
                        <tr>
                            <td>8</td>
                            <td>OMT-012</td>
                            <td>Phạm Hoàng Anh
                            </td>
                            <td>1B1
                            </td>
                            <td>
                                <span>Nam</span>
                            </td>
                            <td>05/11/2008
                            </td>
                            <td>0/4
                            </td>
                            <td style="text-align: center"></td>
                            <td style="text-align: center"></td>
                            <td style="text-align: center"></td>
                            <td style="text-align: center"></td>
                        </tr>
                        <tr>
                            <td>9</td>
                            <td>OMT-011</td>
                            <td>Nguyễn Tùng Anh
                            </td>
                            <td>1B1
                            </td>
                            <td>
                                <span>Nam</span>
                            </td>
                            <td>16/02/2008
                            </td>
                            <td>0/4
                            </td>
                            <td style="text-align: center"></td>
                            <td style="text-align: center"></td>
                            <td style="text-align: center"></td>
                            <td style="text-align: center"></td>
                        </tr>
                        <tr>
                            <td>10</td>
                            <td>OMT-020</td>
                            <td>Lê Sơn Bách
                            </td>
                            <td>1B1
                            </td>
                            <td>
                                <span>Nam</span>
                            </td>
                            <td>07/01/2008
                            </td>
                            <td>0/4
                            </td>
                            <td style="text-align: center"></td>
                            <td style="text-align: center"></td>
                            <td style="text-align: center"></td>
                            <td style="text-align: center"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div id="duadonhs" class="tab-pane fade">
                <table style="width: 98%; margin: 5px 12px" class="table table-bordered table-striped table-condensed flip-content">
                    <thead class="flip-content bg-blue bg-font-blue" style="background-color: #3598dc; color: white">
                        <tr>
                            <th width="2%" style="vertical-align: middle; text-align: center">#</th>
                            <th width="10%" style="vertical-align: middle; text-align: center">Mã học sinh </th>
                            <th width="15%" style="vertical-align: middle; text-align: center">Tên học sinh </th>
                            <th width="6%">Lớp</th>
                            <th width="6%" style="vertical-align: middle; text-align: center">Giới tính </th>
                            <th width="10%" style="vertical-align: middle; text-align: center">Ngày sinh </th>
                            <th width="10%">Đã hoàn thiện</th>
                            <th style="vertical-align: middle; text-align: center">Hồ sơ đưa đón học sinh </th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div id="hocba" class="tab-pane fade">
                <table style="width: 98%; margin: 5px 12px" class="table table-bordered table-striped table-condensed flip-content">
                    <thead class="flip-content bg-blue bg-font-blue" style="background-color: #3598dc; color: white">
                        <tr>
                            <th width="2%" style="vertical-align: middle; text-align: center">#</th>
                            <th width="10%" style="vertical-align: middle; text-align: center">Mã học sinh </th>
                            <th width="15%" style="vertical-align: middle; text-align: center">Tên học sinh </th>
                            <th width="6%">Lớp</th>
                            <th width="6%" style="vertical-align: middle; text-align: center">Giới tính </th>
                            <th width="10%" style="vertical-align: middle; text-align: center">Ngày sinh </th>
                            <th width="10%">Đã hoàn thiện</th>
                            <th style="vertical-align: middle; text-align: center">Học Bạ </th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div id="gks" class="tab-pane fade">
                <table style="width: 98%; margin: 5px 12px" class="table table-bordered table-striped table-condensed flip-content">
                    <thead class="flip-content bg-blue bg-font-blue" style="background-color: #3598dc; color: white">
                        <tr>
                            <th width="2%" style="vertical-align: middle; text-align: center">#</th>
                            <th width="10%" style="vertical-align: middle; text-align: center">Mã học sinh </th>
                            <th width="15%" style="vertical-align: middle; text-align: center">Tên học sinh </th>
                            <th width="6%">Lớp</th>
                            <th width="6%" style="vertical-align: middle; text-align: center">Giới tính </th>
                            <th width="10%" style="vertical-align: middle; text-align: center">Ngày sinh </th>
                            <th width="10%">Đã hoàn thiện</th>
                            <th style="vertical-align: middle; text-align: center">Giấy Khai Sinh </th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <div id="hsts" class="tab-pane fade">
                <table style="width: 98%; margin: 5px 12px" class="table table-bordered table-striped table-condensed flip-content">
                    <thead class="flip-content bg-blue bg-font-blue" style="background-color: #3598dc; color: white">
                        <tr>
                            <th width="2%" style="vertical-align: middle; text-align: center">#</th>
                            <th width="10%" style="vertical-align: middle; text-align: center">Mã học sinh </th>
                            <th width="15%" style="vertical-align: middle; text-align: center">Tên học sinh </th>
                            <th width="6%">Lớp</th>
                            <th width="6%" style="vertical-align: middle; text-align: center">Giới tính </th>
                            <th width="10%" style="vertical-align: middle; text-align: center">Ngày sinh </th>
                            <th width="10%">Đã hoàn thiện</th>
                            <th style="vertical-align: middle; text-align: center">Hồ Sơ Tuyển Sinh </th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <style>
        .main-omt {
            border: 1px solid #32c5d2;
            background-color: #fff;
        }

            .main-omt .omt-header {
                background-color: #32c5d2;
                padding: 4px 7px;
                display: flex;
            }

        .omt-header .header-title {
            font-size: 20px;
            padding: 10px 10px;
            color: white;
        }

        .omt-header .omt__icon {
            font-size: 30px;
            padding: 8px 10px;
            color: white;
        }

        .omt-top {
            display: flex;
            padding: 0 20px;
        }

            .omt-top .form-omt {
                width: 17%;
                height: 35px !important;
                margin-right: 15px;
                margin-top: 10px;
            }

        .form-control {
            height: 35px;
            margin-top: 10px;
            width: 20%;
        }

        .omt-top .btn {
            height: 35px;
            margin-top: 10px !important;
            margin-left: 5px;
            border-radius: 3px;
            padding-bottom: 10px;
        }

        .dropdown-item {
            text-decoration: none !important;
            font-size: 13px;
            padding: 3px 10px 3px 10px !important;
        }

            .dropdown-item:hover {
                background-color: #0275d8 !important;
                color: #fff !important;
            }
    </style>


</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

