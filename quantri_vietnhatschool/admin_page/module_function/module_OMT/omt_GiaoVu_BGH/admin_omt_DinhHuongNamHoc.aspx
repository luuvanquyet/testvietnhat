﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="admin_omt_DinhHuongNamHoc.aspx.cs" Inherits="admin_page_module_function_module_OMT_admin_omt_DinhHuongNamHoc" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="main-omt">
        <div class="omt-header">
            <i class="fa fa-user omt__icon" aria-hidden="true"></i>
            <h4 class="header-title">Quản Lý Định Hướng Đầu Năm</h4>
        </div>
        <div class="omt-top">
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Tìm mã,tên..." aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
            <select class="form-control form-control-sm form-omt">
                <option>1B1</option>
            </select>
            <div class="col-md-2" style="padding-left: 5px; padding-right: 5px">
                <a type="button" class="btn bg-green font-white" style="background-color: #32c5d2; color: white" href="#"><i class="fa fa-user" aria-hidden="true"></i>Thêm mới</a>
            </div>
        </div>
        <div class="fixed-table-container" style="width:98%;margin-left:1%" id="data-items">
            <style type="text/css">
                .group_action a {
                    margin: 0 !important;
                    padding: 3px 6px !important;
                }
            </style>
            <table class="table table-bordered table-striped table-condensed flip-content">
                <thead class="flip-content bg-blue bg-font-blue" style="background-color:#3598dc;color:white">
                    <tr>
                        <th width="3%" style="vertical-align: middle; text-align: center"></th>
                        <th width="10%" style="vertical-align: middle; text-align: center">Mã học sinh</th>
                        <th width="13%" style="vertical-align: middle; text-align: center">Họ và tên</th>
                        <th width="5%" style="vertical-align: middle; text-align: center">Lớp</th>
                        <th width="30%" style="vertical-align: middle; text-align: center">Mục tiêu năm học</th>
                        <th width="20%" style="vertical-align: middle; text-align: center">Mong muốn</th>
                        <th width="10%" style="vertical-align: middle; text-align: center">Kỹ năng cần có</th>
                        <th width="10%" style="vertical-align: middle; text-align: center">Hành động</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="vertical-align: middle; text-align: center">1</td>
                        <td style="vertical-align: middle; text-align: center">OMT-011</td>
                        <td style="vertical-align: middle; text-align: center">Nguyễn Tùng Anh</td>
                        <td style="vertical-align: middle; text-align: center">1B1</td>
                        <td style="vertical-align: middle; text-align: center"></td>
                        <td style="vertical-align: middle; text-align: center"></td>
                        <td style="vertical-align: middle; text-align: center"></td>
                        <td style="text-align: center" class="group_action">
                            <a class="btn btn-info" title="Xem chi tiết"><i class="fa fa-eye" aria-hidden="true"></i></a>
                            <a href="#" class="btn btn-primary" title="Sửa mục tiêu đầu năm học"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            <a class="btn btn-danger" title="Xóa thông tin"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle; text-align: center">2</td>
                        <td style="vertical-align: middle; text-align: center">OMT-012</td>
                        <td style="vertical-align: middle; text-align: center">Phạm Hoàng Anh</td>
                        <td style="vertical-align: middle; text-align: center">1B1</td>
                        <td style="vertical-align: middle; text-align: center"></td>
                        <td style="vertical-align: middle; text-align: center"></td>
                        <td style="vertical-align: middle; text-align: center"></td>
                        <td style="text-align: center" class="group_action">
                            <a class="btn btn-info" title="Xem chi tiết"><i class="fa fa-eye" aria-hidden="true"></i></a>
                            <a href="#" class="btn btn-primary" title="Sửa mục tiêu đầu năm học"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            <a class="btn btn-danger" title="Xóa thông tin"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle; text-align: center">3</td>
                        <td style="vertical-align: middle; text-align: center">OMT-013</td>
                        <td style="vertical-align: middle; text-align: center">Phạm Thị Tuyết Anh</td>
                        <td style="vertical-align: middle; text-align: center">1B1</td>
                        <td style="vertical-align: middle; text-align: center"></td>
                        <td style="vertical-align: middle; text-align: center"></td>
                        <td style="vertical-align: middle; text-align: center"></td>
                        <td style="text-align: center" class="group_action">
                            <a class="btn btn-info" title="Xem chi tiết"><i class="fa fa-eye" aria-hidden="true"></i></a>
                            <a href="#" class="btn btn-primary" title="Sửa mục tiêu đầu năm học"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            <a class="btn btn-danger" title="Xóa thông tin"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle; text-align: center">4</td>
                        <td style="vertical-align: middle; text-align: center">OMT-014</td>
                        <td style="vertical-align: middle; text-align: center">Phạm Trâm Anh</td>
                        <td style="vertical-align: middle; text-align: center">1B1</td>
                        <td style="vertical-align: middle; text-align: center"></td>
                        <td style="vertical-align: middle; text-align: center"></td>
                        <td style="vertical-align: middle; text-align: center"></td>
                        <td style="text-align: center" class="group_action">
                            <a class="btn btn-info" title="Xem chi tiết"><i class="fa fa-eye" aria-hidden="true"></i></a>
                            <a href="#" class="btn btn-primary" title="Sửa mục tiêu đầu năm học"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            <a class="btn btn-danger" title="Xóa thông tin"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle; text-align: center">5</td>
                        <td style="vertical-align: middle; text-align: center">OMT-015</td>
                        <td style="vertical-align: middle; text-align: center">Trần Đức Anh</td>
                        <td style="vertical-align: middle; text-align: center">1B1</td>
                        <td style="vertical-align: middle; text-align: center"></td>
                        <td style="vertical-align: middle; text-align: center"></td>
                        <td style="vertical-align: middle; text-align: center"></td>
                        <td style="text-align: center" class="group_action">
                            <a class="btn btn-info" title="Xem chi tiết"><i class="fa fa-eye" aria-hidden="true"></i></a>
                            <a href="#" class="btn btn-primary" title="Sửa mục tiêu đầu năm học"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            <a class="btn btn-danger" title="Xóa thông tin"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle; text-align: center">6</td>
                        <td style="vertical-align: middle; text-align: center">OMT-016</td>
                        <td style="vertical-align: middle; text-align: center">Lê Minh Bảo Anh</td>
                        <td style="vertical-align: middle; text-align: center">1B1</td>
                        <td style="vertical-align: middle; text-align: center"></td>
                        <td style="vertical-align: middle; text-align: center"></td>
                        <td style="vertical-align: middle; text-align: center"></td>
                        <td style="text-align: center" class="group_action">
                            <a class="btn btn-info" title="Xem chi tiết"><i class="fa fa-eye" aria-hidden="true"></i></a>
                            <a href="#" class="btn btn-primary" title="Sửa mục tiêu đầu năm học"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            <a class="btn btn-danger" title="Xóa thông tin"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle; text-align: center">7</td>
                        <td style="vertical-align: middle; text-align: center">OMT-017</td>
                        <td style="vertical-align: middle; text-align: center">Lại Viết Hoàng Anh</td>
                        <td style="vertical-align: middle; text-align: center">1B1</td>
                        <td style="vertical-align: middle; text-align: center"></td>
                        <td style="vertical-align: middle; text-align: center"></td>
                        <td style="vertical-align: middle; text-align: center"></td>
                        <td style="text-align: center" class="group_action">
                            <a class="btn btn-info" title="Xem chi tiết"><i class="fa fa-eye" aria-hidden="true"></i></a>
                            <a href="#" class="btn btn-primary" title="Sửa mục tiêu đầu năm học"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            <a class="btn btn-danger" title="Xóa thông tin"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle; text-align: center">8</td>
                        <td style="vertical-align: middle; text-align: center">OMT-018</td>
                        <td style="vertical-align: middle; text-align: center">Lê Trang Anh</td>
                        <td style="vertical-align: middle; text-align: center">1B1</td>
                        <td style="vertical-align: middle; text-align: center"></td>
                        <td style="vertical-align: middle; text-align: center"></td>
                        <td style="vertical-align: middle; text-align: center"></td>
                        <td style="text-align: center" class="group_action">
                            <a class="btn btn-info" title="Xem chi tiết"><i class="fa fa-eye" aria-hidden="true"></i></a>
                            <a href="#" class="btn btn-primary" title="Sửa mục tiêu đầu năm học"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            <a class="btn btn-danger" title="Xóa thông tin"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle; text-align: center">9</td>
                        <td style="vertical-align: middle; text-align: center">OMT-019</td>
                        <td style="vertical-align: middle; text-align: center">Đào Phương Anh</td>
                        <td style="vertical-align: middle; text-align: center">1B1</td>
                        <td style="vertical-align: middle; text-align: center"></td>
                        <td style="vertical-align: middle; text-align: center"></td>
                        <td style="vertical-align: middle; text-align: center"></td>
                        <td style="text-align: center" class="group_action">
                            <a class="btn btn-info" title="Xem chi tiết"><i class="fa fa-eye" aria-hidden="true"></i></a>
                            <a href="#" class="btn btn-primary" title="Sửa mục tiêu đầu năm học"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            <a class="btn btn-danger" title="Xóa thông tin"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: middle; text-align: center">10</td>
                        <td style="vertical-align: middle; text-align: center">OMT-020</td>
                        <td style="vertical-align: middle; text-align: center">Lê Sơn Bách</td>
                        <td style="vertical-align: middle; text-align: center">1B1</td>
                        <td style="vertical-align: middle; text-align: center"></td>
                        <td style="vertical-align: middle; text-align: center"></td>
                        <td style="vertical-align: middle; text-align: center"></td>
                        <td style="text-align: center" class="group_action">
                            <a class="btn btn-info" title="Xem chi tiết"><i class="fa fa-eye" aria-hidden="true"></i></a>
                            <a href="#" class="btn btn-primary" title="Sửa mục tiêu đầu năm học"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                            <a class="btn btn-danger" title="Xóa thông tin"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                        </td>
                    </tr>
                </tbody>
            </table>

        </div>
    </div>
    <style>
        .main-omt {
            border: 1px solid #32c5d2;
            background-color: #fff;
        }

            .main-omt .omt-header {
                background-color: #32c5d2;
                padding: 4px 7px;
                display: flex;
            }

        .omt-header .header-title {
            font-size: 20px;
            padding: 10px 10px;
            color: white;
        }

        .omt-header .omt__icon {
            font-size: 30px;
            padding: 8px 10px;
            color: white;
        }

        .omt-top {
            display: flex;
            padding: 0 20px;
        }

            .omt-top .form-omt {
                width: 17%;
                height: 35px !important;
                margin-right: 15px;
                margin-top: 10px;
            }

        .form-control {
            height: 35px;
            margin-top: 10px;
            width: 20%;
        }

        .omt-top .btn {
            height: 35px;
            margin-top: 10px !important;
            margin-left: 5px;
            margin-right: 15px;
            border-radius: 3px;
            padding: 0.4rem 1rem;
        }
    </style>

</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

