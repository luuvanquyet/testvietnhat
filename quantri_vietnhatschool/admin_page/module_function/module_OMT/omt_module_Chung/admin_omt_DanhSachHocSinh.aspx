﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="admin_omt_DanhSachHocSinh.aspx.cs" Inherits="admin_page_module_function_module_OMT_admin_omt_DanhSachHocSinh" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="main-omt">
        <div class="omt-header">
            <i class="fa fa-users omt__icon" aria-hidden="true"></i>
            <h4 class="header-title">Quản Lý Học Sinh</h4>
        </div>
        <div class="omt-top">
            <select class="form-control form-control-sm form-omt">
                <option>1B1</option>
            </select>
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Tìm kiếm học sinh" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
            <div class="col-md-1" style="padding-left: 5px; padding-right: 5px">
                <button type="button" class="btn bg-green-jungle font-white" style="background-color: #26C281; color: white; outline: none" onclick="loadDataItems(null,1)">
                    <i class="fa fa-file-excel-o font-white"></i>
                </button>
            </div>
        </div>
        <table class="table table-bordered" style="width: 98%; margin-left: 12px; margin-right: 12px">
            <thead>
                <tr>
                    <th width="2%" rowspan="2" style="vertical-align: middle; text-align: center; background-color: #3598dc; color: white">#</th>
                    <th width="16%" rowspan="2" style="vertical-align: middle; text-align: center; background-color: #3598dc; color: white">Họ và Tên
                        <i class="fa fa-filter" data-toggle="modal" href="#"></i>
                    </th>
                    <th width="10%" rowspan="2" style="vertical-align: middle; text-align: center; background-color: #3598dc; color: white">Giới tính</th>
                    <th width="10%" rowspan="2" style="vertical-align: middle; text-align: center; background-color: #3598dc; color: white">Ngày Sinh</th>
                    <th width="30%" colspan="2" style="text-align: center; background-color: #3598dc; color: white">Bố</th>
                    <th width="30%" colspan="2" style="text-align: center; background-color: #3598dc; color: white">Mẹ</th>

                </tr>
                <tr>
                    <th width="12%" style="text-align: center; background-color: #3598dc; color: white">Họ Và Tên</th>
                    <th width="12%" style="text-align: center; background-color: #3598dc; color: white">Liên Hệ</th>
                    <th width="12%" style="text-align: center; background-color: #3598dc; color: white">Họ Và Tên</th>
                    <th width="12%" style="text-align: center; background-color: #3598dc; color: white">Liên Hệ</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <th scope="row">1</th>
                    <td style="vertical-align: top">
                        <a style="font-weight: 600; text-decoration: none; color: #337ab7" href="#">Nguyễn Tùng Anh</a>
                        <a href="#" title="Thêm mới Thông báo"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
                        <i class="fa fa-mobile" aria-hidden="true" title="Chưa đăng ký truy cập App"></i>
                        <br>
                        <b title="Mã">ID:</b>OMT-011
                        <br>
                        <b title="Tên đăng nhập">U:</b>OMT-011
                    </td>
                    <td style="vertical-align: top">Nam</td>
                    <td style="vertical-align: top">16/02/2008</td>
                    <td style="vertical-align: top">
                        <a style="font-weight: 600; text-decoration: none; color: #337ab7" href="#">Nguyễn Tuấn Anh</a>
                        <br>
                        <b title="Tên đăng nhập">Tên Đăng Nhập:</b>FT011
                    </td>
                    <td style="vertical-align: top">
                        <b title="Số điện thoại">Phone:</b> <a href="tel:FT011" style="text-decoration: none">FT011</a><br>
                        <b title="Email">EMail:</b> <a href="#"></a>
                    </td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <th scope="row">2</th>
                    <td style="vertical-align: top">
                        <a style="font-weight: 600; text-decoration: none; color: #337ab7" href="#">Phạm Hoàng Anh</a>
                        <a href="#" title="Thêm mới Thông báo"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
                        <i class="fa fa-mobile" aria-hidden="true" title="Chưa đăng ký truy cập App"></i>
                        <br>
                        <b title="Mã">ID:</b>OMT-012
                        <br>
                        <b title="Tên đăng nhập">U:</b>OMT-012
                    </td>
                    <td style="vertical-align: top">Nam</td>
                    <td style="vertical-align: top">05/11/2008</td>
                    <td style="vertical-align: top"></td>
                    <td style="vertical-align: top"></td>
                    <td style="vertical-align: top">
                        <a style="font-weight: 600; text-decoration: none; color: #337ab7" href="#">Hoàng Thị Thanh Mai</a>
                        <br>
                        <b title="Tên đăng nhập">Tên Đăng Nhập:</b>MT009
                    </td>
                    <td style="vertical-align: top">
                        <b title="Số điện thoại">Phone:</b> <a href="tel:FT011" style="text-decoration: none">MT009</a><br>
                        <b title="Email">EMail:</b> <a href="#"></a>
                    </td>
                </tr>
                <tr>
                    <th scope="row">3</th>
                    <td style="vertical-align: top">
                        <a style="font-weight: 600; text-decoration: none; color: #337ab7" href="#">Phạm Thị Tuyết Anh</a>
                        <a href="#" title="Thêm mới Thông báo"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
                        <i class="fa fa-mobile" aria-hidden="true" title="Chưa đăng ký truy cập App"></i>
                        <br>
                        <b title="Mã">ID:</b>OMT-013
                        <br>
                        <b title="Tên đăng nhập">U:</b>OMT-013
                    </td>
                    <td style="vertical-align: top">Nữ</td>
                    <td style="vertical-align: top">19/09/2008</td>
                    <td style="vertical-align: top"></td>
                    <td style="vertical-align: top"></td>
                    <td style="vertical-align: top">
                        <a style="font-weight: 600; text-decoration: none; color: #337ab7" href="#">Quách Thị Ánh Tuyết</a>
                        <br>
                        <b title="Tên đăng nhập">Tên Đăng Nhập:</b>MT010
                    </td>
                    <td style="vertical-align: top">
                        <b title="Số điện thoại">Phone:</b> <a href="tel:FT011" style="text-decoration: none">MT010</a><br>
                        <b title="Email">EMail:</b> <a href="#"></a>
                    </td>
                </tr>
                <tr>
                    <th scope="row">4</th>
                    <td style="vertical-align: top">
                        <a style="font-weight: 600; text-decoration: none; color: #337ab7" href="#">Phạm Trâm Anh</a>
                        <a href="#" title="Thêm mới Thông báo"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
                        <i class="fa fa-mobile" aria-hidden="true" title="Chưa đăng ký truy cập App"></i>
                        <br>
                        <b title="Mã">ID:</b>OMT-014
                        <br>
                        <b title="Tên đăng nhập">U:</b>OMT-014
                    </td>
                    <td style="vertical-align: top">Nữ</td>
                    <td style="vertical-align: top">16/07/2008</td>
                    <td style="vertical-align: top">
                        <a style="font-weight: 600; text-decoration: none; color: #337ab7" href="#">Phạm Văn Hiệu</a>
                        <br>
                        <b title="Tên đăng nhập">Tên Đăng Nhập:</b>FT014
                    </td>
                    <td style="vertical-align: top">
                        <b title="Số điện thoại">Phone:</b> <a href="tel:FT011" style="text-decoration: none">FT014</a><br>
                        <b title="Email">EMail:</b> <a href="#"></a>
                    </td>
                    <td style="vertical-align: top"></td>
                    <td style="vertical-align: top"></td>
                </tr>
                <tr>
                    <th scope="row">5</th>
                    <td style="vertical-align: top">
                        <a style="font-weight: 600; text-decoration: none; color: #337ab7" href="#">Trần Đức Anh</a>
                        <a href="#" title="Thêm mới Thông báo"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
                        <i class="fa fa-mobile" aria-hidden="true" title="Chưa đăng ký truy cập App"></i>
                        <br>
                        <b title="Mã">ID:</b>OMT-015
                        <br>
                        <b title="Tên đăng nhập">U:</b>OMT-015
                    </td>
                    <td style="vertical-align: top">Nữ</td>
                    <td style="vertical-align: top">12/12/2008</td>
                    <td style="vertical-align: top">
                        <a style="font-weight: 600; text-decoration: none; color: #337ab7" href="#">Trần Đức Thành</a>
                        <br>
                        <b title="Tên đăng nhập">Tên Đăng Nhập:</b>FT015
                    </td>
                    <td style="vertical-align: top">
                        <b title="Số điện thoại">Phone:</b> <a href="tel:FT011" style="text-decoration: none">FT015</a><br>
                        <b title="Email">EMail:</b> <a href="#"></a>
                    </td>
                    <td style="vertical-align: top"></td>
                    <td style="vertical-align: top"></td>
                </tr>


            </tbody>
        </table>
    </div>
    <style>
        .main-omt {
            border: 1px solid #32c5d2;
            background-color: #fff;
        }

            .main-omt .omt-header {
                background-color: #32c5d2;
                padding: 4px 7px;
                display: flex;
            }

        .omt-header .header-title {
            font-size: 20px;
            padding: 10px 10px;
            color: white;
        }

        .omt-header .omt__icon {
            font-size: 30px;
            padding: 8px 10px;
            color: white;
        }

        .omt-top {
            display: flex;
            padding: 0 20px;
        }

            .omt-top .form-omt {
                width: 17%;
                height: 35px !important;
                margin-right: 15px;
                margin-top: 10px;
            }

        .form-control {
            height: 35px;
            margin-top: 10px;
            width: 20%;
        }

        .omt-top .btn {
            height: 35px;
            margin-top: 10px !important;
            margin-left: 5px;
            border-radius: 3px;
            padding-bottom: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

