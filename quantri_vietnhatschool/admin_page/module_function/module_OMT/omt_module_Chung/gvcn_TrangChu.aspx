﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="gvcn_TrangChu.aspx.cs" Inherits="admin_page_OMT_module_gvcn_gvcn_trangchu" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="table">
        <div class="col-lg-6 col-xs-12 col-sm-12">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="dashboard">
                        <div class="number1">
                            <span>1</span>
                            <span class="icon">
                                <i class="icon-calendar"></i>
                            </span>
                        </div>
                        <div>
                            <span class="note1">THÔNG BÁO</span>
                            <div class="check1"></div>
                        </div>
                    </div>
                    <div>
                        <div class="dashboard2">
                            <span class="note2">ĐÃ ĐỌC</span>
                            <span class="note3">100%</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="dashboard">
                        <div class="number2">
                            <span>0</span>
                            <span class="icon">
                                <i class="icon-calendar"></i>
                            </span>
                        </div>
                        <div>
                            <span class="note2">ĐƠN XIN NGHỈ</span>
                            <div class="check2"></div>
                        </div>
                    </div>
                    <div>
                        <div class="dashboard2">
                            <span class="note2">ĐÃ DUYỆT</span>
                            <span class="note3">100%</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="dashboard">
                        <div class="number1">
                            <span>0</span>
                            <span class="icon">
                                <i class="icon-calendar"></i>
                            </span>
                        </div>
                        <div>
                            <span class="note1">CÔNG VIỆC</span>
                            <div class="check1"></div>
                        </div>
                    </div>
                    <div>
                        <div class="dashboard2">
                            <span class="note2">ĐÃ ĐỌC</span>
                            <span class="note3">100%</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="dashboard">
                        <div class="number2">
                            <span>0</span>
                            <span class="icon">
                                <i class="icon-calendar"></i>
                            </span>
                        </div>
                        <div>
                            <span class="note2">HỌC SINH NGHỈ HỌC</span>
                            <div class="check2"></div>
                        </div>
                    </div>
                    <div>
                        <div class="dashboard2">
                            <span class="note2">NGHỈ CÓ PHÉP</span>
                            <span class="note3">100%</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="dashboard">
                        <div class="number1">
                            <span>0</span>
                            <span class="icon">
                                <i class="icon-calendar"></i>
                            </span>
                        </div>
                        <div>
                            <span class="note1">LỊCH</span>
                            <div class="check1"></div>
                        </div>
                    </div>
                    <div>
                        <div class="dashboard2">
                            <span class="note2">SỰ KIỆN</span>
                            <span class="note3">100%</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="dashboard">
                        <div class="number2">
                            <span>10</span>
                            <span class="icon">
                                <i class="icon-calendar"></i>
                            </span>
                        </div>
                        <div>
                            <span class="note2">HỌC SINH</span>
                            <div class="check2"></div>
                        </div>
                    </div>
                    <div>
                        <div class="dashboard2">
                            <span class="note2">NỮ</span>
                            <span class="note3">100%</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-xs-12 col-sm-12 mt-2">
        <div class="portlet">
            <div class="portlet_up">
                <span>THÔNG BÁO MỚI</span>
                <span class="right">Hoạt động gần đây</span>
                <span class="right">Thông báo gần đây</span>
                <div class="check3"></div>
            </div>
            <div class="portlet_down">
                <span class="note4">Bạn không có hoạt động nào</span>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

