﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="gvcn_TrangChu_ThongBao.aspx.cs" Inherits="admin_page_OMT_module_gvcn_gvcn_trangchu_thongbao" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="table_notification">
        <div class="table_text1">
            <h2><i class="fas fa-graduation-cap"></i>Danh sách Thông báo </h2>
        </div>
        <button type="button" class="dropdown_all btn" data-toggle="dropdown">--Tất cả &emsp; &emsp; </button>
        <div class="table_text2">
            <p><b>Số lượng thông báo: 1 </b></p>
        </div>
        <div class="table_text3">
            <table id="customers">

                <tr>
                    <th></th>
                    <th>Tên thông báo</th>
                    <th>Gửi đến </th>
                    <th>Trạng thái</th>
                    <th>Gửi lúc</th>
                </tr>

                <tr>
                    <td><i class="far fa-check-circle" style="color: blue;"></i></td>
                    <td>Thư mời dự lễ khai giảng năm học mới 2020 - 2021</td>
                    <td>Tiểu học Hồng Đức</td>
                    <td>Đã đọc </td>
                    <td>16:00:07 23/07/2020</td>
                </tr>

            </table>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

