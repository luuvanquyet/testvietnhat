﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="omt_QuanLyCongViec.aspx.cs" Inherits="admin_page_module_function_module_OMT_omt_module_Chung_omt_QuanLyCongViec" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <link href="css_QuanLyCongViec.css" rel="stylesheet" />
    <script>
        function myHienThi() {
            document.getElementById('block_content').style.display = "block"
            //document.getElementById("block_content").visible = true;
        }
        function myTat() {
            document.getElementById('block_content').style.display = "none"
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="container">
        <div class="omt_header">
            Quản lý công việc
        </div>
        <div class="row">
            <hr />
            <div class="omt_tinhtrang">
                <div class="col-4">
                    <div class="omt_tinhtrang_dangcho">
                        Đang chờ xác nhận
                    </div>
                </div>
                <div class="col-4">
                    <div class="omt_tinhtrang_danghoatdong">
                        Đang hoạt động
                    </div>
                </div>
                <div class="col-4">
                    <div class="omt_tinhtrang_dangbikhoa">
                        Đã bị khóa
                    </div>
                </div>
            </div>
            <%-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg">Large modal</button>--%>

            <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="container">
                            <div class="row">
                                <div class="popup_title">
                                    <i class="fa fa-building" aria-hidden="true"></i>
                                    <input type="text" value="Chúng tôi yêu Đà nẵng" />
                                </div>
                                <div class="popup-content">
                                    <div class="col-8">
                                        <div class="col-5" style="margin-right: 90px;">
                                            <div>Ngày bắt đầu</div>
                                            <div>
                                                <input type="date" class="form-control" />
                                            </div>
                                        </div>
                                        <div class="col-5">
                                            <div>Ngày kết thúc</div>
                                            <div>
                                                <input type="date" class="form-control" />
                                            </div>
                                        </div>
                                        <%-- <div class="col-6"></div>--%>
                                        <div class="popup-mota">
                                            <div>
                                                <i class="fa fa-bars" aria-hidden="true"></i>
                                                Mô tả
                                            </div>
                                            <input type="text" />
                                            <a href="#" id="btnLuu">Lưu</a>
                                        </div>
                                        <div class="popup-tepdinhkem">
                                            <i class="fa fa-paperclip" aria-hidden="true"></i>
                                            Tệp đính kèm
                                        </div>
                                        <div class="popup-binhluan">
                                            <i class="fa fa-comment-o" aria-hidden="true"></i>
                                            Bình luận
                                    <textarea></textarea>
                                            <a href="#" id="btnThemBinhLuan" class="btn btn-primary">Thêm mới</a>
                                        </div>
                                        <div class="popup-traodoi">
                                            <i class="fa fa-rss" aria-hidden="true"></i>
                                            Trao đổi
                                        </div>
                                        <div class="popup-comment">
                                            <img class="work-avatar" src="user_noimage.jpg" style="width: 30px; height: 30px; border-radius: 50% !important;" />
                                            Phùng Đức ngày 17/08/2020
                                            <div class="popup-comment-ketqua">Nội dung nhập phía trên</div>
                                            <hr />
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="popup-traodoi">
                                            QUẢN LÝ CÔNG VIỆC
                                        </div>
                                        <div>
                                            <div id="accordion">
                                                <div class="card">
                                                    <div class="card-header" id="headingOne">
                                                        <h5 class="mb-0">
                                                            <a href="javascript:void(0)" class="tab-thanhvien" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Thành viên
                                                            </a>
                                                        </h5>
                                                    </div>

                                                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                                        <div class="card-body">
                                                            <asp:DropDownList ID="ddlThanhVien" runat="server" CssClass="form-control" Width="90%"></asp:DropDownList>
                                                            <hr />
                                                            Phùng Đức
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card">
                                                    <div class="card-header" id="headingTwo">
                                                        <h5 class="mb-0">
                                                            <a href="javascript:void(0)" class="tab-thanhvien collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Công việc
                                                            </a>
                                                        </h5>
                                                    </div>
                                                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                                        <div class="card-body">
                                                            <input type="text" id="txtCongViec_popup" class="form-control" style="width: 90%" placeholder="Nhập để thêm mới công việc" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card">
                                                    <div class="card-header" id="headingThree">
                                                        <h5 class="mb-0">
                                                            <a href="javascript:void(0)" class="tab-thanhvien collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">Tệp đính kèm
                                                            </a>
                                                        </h5>
                                                    </div>
                                                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                                        <div class="card-body">
                                                            <asp:FileUpload ID="fileUpload1" runat="server" />
                                                            <asp:Label ID="lblThongBao" runat="server"></asp:Label>
                                                            <asp:Button ID="btnImport" runat="server" Text="Import" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" id="omtqlcv">
            <div class="col-4">
                <div class="omt_content">
                    <div class="omt_Title">Tiêu đề</div>

                    <div class="omt_themmoi"><a href="javasrcipt:voide(0)" onclick="myHienThi()">+ Thêm mới công việc</a></div>

                    <div class="omt_textbox" id="block_content" style="display: none">
                        <textarea type="text" id="txtContent" runat="server" style="width: 90%" />
                        <a href="#" id="btnThemMoi" class="btn btn-primary">Thêm mới</a> <a href="#" id="btnTat" onclick="myTat()" class="btn btn-primary">Tắt</a>
                    </div>
                    <div class="omt_content_chitiet">
                        <div class="omt_trangthai"></div>

                        <div class="omt_hienthicontent">
                            nội dung ở đây
                        </div>
                        <div class="omt_popup">
                            <a href="#" data-toggle="modal" data-target=".bd-example-modal-lg">
                                <i class="fa fa-comment-o" aria-hidden="true">0</i>
                                <i class="fa fa-paperclip" aria-hidden="true">0</i>
                                <i class="fa fa-check-square-o" aria-hidden="true">0</i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="omt_content">
                    <div class="omt_Title">Tiêu đề 2</div>

                    <div class="omt_themmoi"><a href="javasrcipt:voide(0)" onclick="myHienThi()">+ Thêm mới công việc</a></div>

                    <div class="omt_textbox" id="block_content" style="display: none">
                        <textarea type="text" id="Textarea1" runat="server" style="width: 90%" />
                        <a href="#" id="btnThemMoi" class="btn btn-primary">Thêm mới</a> <a href="#" id="btnTat" onclick="myTat()" class="btn btn-primary">Tắt</a>
                    </div>
                    <div class="omt_content_chitiet">
                        <div class="omt_trangthai"></div>

                        <div class="omt_hienthicontent">
                            nội dung ở đây
                        </div>
                        <div class="omt_popup">
                            <a href="#" data-toggle="modal" data-target=".bd-example-modal-lg">
                                <i class="fa fa-comment-o" aria-hidden="true">0</i>
                                <i class="fa fa-paperclip" aria-hidden="true">0</i>
                                <i class="fa fa-check-square-o" aria-hidden="true">0</i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="omt_content">
                    <div class="omt_Title">Tiêu đề 3</div>

                    <div class="omt_themmoi"><a href="javasrcipt:voide(0)" onclick="myHienThi()">+ Thêm mới công việc</a></div>

                    <div class="omt_textbox" id="block_content" style="display: none">
                        <textarea type="text" id="Textarea2" runat="server" style="width: 90%" />
                        <a href="#" id="btnThemMoi" class="btn btn-primary">Thêm mới</a> <a href="#" id="btnTat" onclick="myTat()" class="btn btn-primary">Tắt</a>
                    </div>
                    <div class="omt_content_chitiet">
                        <div class="omt_trangthai"></div>

                        <div class="omt_hienthicontent">
                            nội dung ở đây
                        </div>
                        <div class="omt_popup">
                            <a href="#" data-toggle="modal" data-target=".bd-example-modal-lg">
                                <i class="fa fa-comment-o" aria-hidden="true">0</i>
                                <i class="fa fa-paperclip" aria-hidden="true">0</i>
                                <i class="fa fa-check-square-o" aria-hidden="true">0</i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">

</asp:Content>

