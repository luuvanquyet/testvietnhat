﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="web_ThongBao_ThongBaoDaGui.aspx.cs" Inherits="admin_page_module_function_module_OMT_omt_module_Chung_web_ThongBao_ThongBaoDaGui" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="table_notification">
        <div class="omt-header">
            <i class="fa fa-user omt__icon" aria-hidden="true"></i>
            <h4 class="header-title">Quản lý Thông báo</h4>
        </div>
        <div class="row">
                <input type="text" name="name" placeholder="Tìm tiêu đề....." class="txt_search_dstg col-md-2" />
                <button class="btn_search_dstg col-md-1">
                    <i class="fa fa-search omt__icon2" aria-hidden="true"></i>
                </button>
        </div>
        <div class="table_text2">
        </div>
        <div class="table_text3">
            <table id="customers">
                <tr>
                    <th></th>
                    <th>Tiêu đề</th>
                    <th>Trường học</th>
                    <th>Học sinh</th>
                    <th>Nhóm người nhận</th>
                    <th>Phạm vi gửi</th>
                    <th>Người nhận</th>
                    <th>Đã gửi</th>
                    <th>Đã đọc</th>
                    <th>Người gửi</th>
                </tr>

                <tr class="table_text4">
                    <td></td>
                    <td>1</td>
                    <td>1B1-Thông báo về việc chuẩn bị năm học mới</td>
                    <td>Trường Tiểu học OMT Hà Nội</td>
                    <td>Phụ huynh</td>
                    <td>Lớp chủ nhiệm:- 1B1</td>
                    <td>10</td>
                    <td>10</td>
                    <td>1</td>
                    <td>Lương Văn Nguyên 16:01:52 23/07/2020</td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

