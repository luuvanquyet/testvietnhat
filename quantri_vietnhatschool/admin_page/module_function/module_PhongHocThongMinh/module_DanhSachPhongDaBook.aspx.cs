﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_DanhSachPhongDaBook : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {
        loadData();
    }
    private void loadData()
    {
        var getData = from tb in db.tblichbookphongchitiets
                      join p in db.tbLichBookPhongThongMinhs on tb.lichbookphongthongminh_id equals p.lichbookphongthongminh_id
                      where tb.lichbookphongchitiet_createdate != null && tb.lichbookphongchitiet_giaovien == Request.Cookies["UserName"].Value
                      select new
                      {
                          p.lichbookphongthongminh_name,
                          tb.lichbookphongchitiet_giaovien,
                          tb.lichbookphongchitiet_createdate,
                          tiethoc_id = tb.tiethoc_id == 1 ? "Sân bóng đá" : tb.tiethoc_id == 2 ? "Sân bóng rổ" : tb.tiethoc_id == 3 ? "Sân cầu lông" : tb.tiethoc_id == 4 ? "Hội trường tầng 1" : tb.tiethoc_id == 5 ? "Hội trường tầng 6 bên ngoài" : "Hội trường tầng 6 trong",
                          tb.lichbookphongchitiet_checkin,
                          tb.lichbookphongchitiet_id
                      };
        grvList.DataSource = getData;
        grvList.DataBind();
    }
    private void setNULL()
    {

    }

    protected void btnThem_Click(object sender, EventArgs e)
    {
        Session["_id"] = 0;
        setNULL();
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupControl.Show();", true);
    }
}