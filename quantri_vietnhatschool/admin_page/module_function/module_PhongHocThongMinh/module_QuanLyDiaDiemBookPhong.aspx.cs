﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_QuanLyPhongThongMinh : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;


    protected void Page_Load(object sender, EventArgs e)
    {
        //// Xem kết quả tổng kết
        if (!IsPostBack)
        {

            var listNV = from nv in db.tbHocTap_Tuans select nv;
            ddlThang.Items.Clear();
            ddlThang.Items.Insert(0, "Chọn");
            ddlThang.AppendDataBoundItems = true;
            ddlThang.DataTextField = "tuan_name";
            ddlThang.DataValueField = "tuan_id";
            ddlThang.DataSource = listNV;
            ddlThang.DataBind();
            //string thang = "/" + DateTime.Now.Month;
            //if (ddlThang.SelectedValue != "Chọn")
            //{
            //    rpBookRoom.DataSource = from nb in db.tbLichBookPhongThongMinhs
            //                            where nb.tuan_id == Convert.ToInt16(ddlThang.SelectedValue)
            //                            select nb;
            //    rpBookRoom.DataBind();
            //}
        }
        var getKhachHang = from gv in db.admin_Users where gv.username_username == Request.Cookies["UserName"].Value select gv;
        txtGiaoVien.Value = getKhachHang.SingleOrDefault().username_fullname;

    }

    protected void btnThem_Click(object sender, EventArgs e)
    {

    }
    protected void rpBookRoom_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Repeater rpChiTiet = e.Item.FindControl("rpChiTiet") as Repeater;
        int lichbookphongthongminh_id = int.Parse(DataBinder.Eval(e.Item.DataItem, "lichbookphongthongminh_id").ToString());
        rpChiTiet.DataSource = from ct in db.tblichbookphongchitiets
                               where ct.lichbookphongthongminh_id == lichbookphongthongminh_id
                               select new
                               {
                                   ct.lichbookphongchitiet_class,
                                   ct.lichbookphongchitiet_id,
                                   giaovien = ct.lichbookphongchitiet_giaovien == null ? "Chưa book" : ct.lichbookphongchitiet_giaovien,
                               };
        rpChiTiet.DataBind();
    }
    protected void btnLoc_ServerClick(object sender, EventArgs e)
    {

        rpBookRoom.DataSource = from nb in db.tbLichBookPhongThongMinhs
                                where nb.tuan_id == Convert.ToInt16(ddlThang.SelectedValue)
                                select nb;
        rpBookRoom.DataBind();
    }
    protected void btnSave_ServerClick(object sender, EventArgs e)
    {
        try
        {
            if (txtLop.Value == "")
            {
                alert.alert_Error(Page, "Vui lòng nhập đầy đủ thông tin", "");
            }
            else
            {
                tblichbookphongchitiet update = (from l in db.tblichbookphongchitiets
                                                 where l.lichbookphongchitiet_id == Convert.ToInt32(txtId.Value)
                                                 select l).SingleOrDefault();
                if (update.lichbookphongchitiet_status == "chưa book")
                {
                    update.lichbookphongchitiet_lop = txtLop.Value;
                    update.lichbookphongchitiet_monhoc = txtMonHoc.Value;
                    update.lichbookphongchitiet_giaovien = Request.Cookies["UserName"].Value;
                    update.lichbookphongchitiet_status = "đã book";
                    update.lichbookphongchitiet_class = "danger";
                    update.lichbookphongchitiet_noidung = txtNoiDung.Value;
                    update.lichbookphongchitiet_createdate = DateTime.Now;
                    db.SubmitChanges();
                    //string thang = "/" + ddlThang.SelectedValue;
                    //rpBookRoom.DataSource = from nb in db.tbLichBookPhongThongMinhs where nb.lichbookphongthongminh_name.Contains(thang) select nb;
                    //rpBookRoom.DataBind();
                    alert.alert_Success(Page, "Đã hoàn thành", "");
                    rpBookRoom.DataSource = from nb in db.tbLichBookPhongThongMinhs
                                            where nb.tuan_id == Convert.ToInt16(ddlThang.SelectedValue)
                                            select nb;
                    rpBookRoom.DataBind();
                }
                else
                {
                    alert.alert_Error(Page, "Phòng đã được book vui lòng chọn tiết khác chưa book!", "");
                    // string thang = "/" + ddlThang.SelectedValue;
                    //rpBookRoom.DataSource = from nb in db.tbLichBookPhongThongMinhs where nb.lichbookphongthongminh_name.Contains(thang) select nb;
                    //rpBookRoom.DataBind();
                }
            }
        }
        catch (Exception ex)
        {
            alert.alert_Error(Page, "Lỗi!", "");
        }
    }

    protected void btnHuy_ServerClick(object sender, EventArgs e)
    {
        try
        {
            tblichbookphongchitiet update = (from l in db.tblichbookphongchitiets
                                             where l.lichbookphongchitiet_id == Convert.ToInt32(txtId.Value)
                                             select l).SingleOrDefault();
            if (update.lichbookphongchitiet_status == "đã book")
            {
                update.lichbookphongchitiet_lop = null;
                update.lichbookphongchitiet_monhoc = null;
                update.lichbookphongchitiet_giaovien = null;
                update.lichbookphongchitiet_status = "chưa book";
                update.lichbookphongchitiet_class = "primary";
                update.lichbookphongchitiet_noidung = null;
                update.lichbookphongchitiet_createdate = null;
                update.lichbookphongchitiet_checkin = null;
                db.SubmitChanges();
                //string thang = "/" + ddlThang.SelectedValue;
                //rpBookRoom.DataSource = from nb in db.tbLichBookPhongThongMinhs where nb.lichbookphongthongminh_name.Contains(thang) select nb;
                //rpBookRoom.DataBind();
                alert.alert_Success(Page, "Đã hủy phòng", "");
                rpBookRoom.DataSource = from nb in db.tbLichBookPhongThongMinhs
                                        where nb.tuan_id == Convert.ToInt16(ddlThang.SelectedValue)
                                        select nb;
                rpBookRoom.DataBind();
            }
            else
            {
                alert.alert_Error(Page, "Phòng hiện tại trống không thể hủy được!", "");
                // string thang = "/" + ddlThang.SelectedValue;
                //rpBookRoom.DataSource = from nb in db.tbLichBookPhongThongMinhs where nb.lichbookphongthongminh_name.Contains(thang) select nb;
                //rpBookRoom.DataBind();
            }
        }
        catch (Exception ex)
        {
            alert.alert_Error(Page, "Lỗi!", "");
        }
    }
}