﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_QuanLyPhongThongMinh.aspx.cs" Inherits="admin_page_module_function_module_QuanLyPhongThongMinh" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v17.1" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <link href="../css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous" />
    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" />
    <script>
        function myLichbookphong(id) {
            document.getElementById("<%=txtId.ClientID%>").value = id;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div id="block_1" runat="server" class="card card-block">
        <div class="form-group row">
            <div class="col-sm-10" id="dvThem" runat="server">
                <asp:DropDownList ID="ddlThang" runat="server">
                    <asp:ListItem Value="1" Text="Tháng 1"></asp:ListItem>
                    <asp:ListItem Value="2" Text="Tháng 2"></asp:ListItem>
                    <asp:ListItem Value="3" Text="Tháng 3"></asp:ListItem>
                    <asp:ListItem Value="4" Text="Tháng 4"></asp:ListItem>
                    <asp:ListItem Value="5" Text="Tháng 5"></asp:ListItem>
                    <asp:ListItem Value="6" Text="Tháng 6"></asp:ListItem>
                    <asp:ListItem Value="7" Text="Tháng 7"></asp:ListItem>
                    <asp:ListItem Value="8" Text="Tháng 8"></asp:ListItem>
                    <asp:ListItem Value="9" Text="Tháng 9"></asp:ListItem>
                    <asp:ListItem Value="10" Text="Tháng 10"></asp:ListItem>
                    <asp:ListItem Value="11" Text="Tháng 11"></asp:ListItem>
                </asp:DropDownList>
                <a id="btnLoc" runat="server" class="btn btn-primary" onserverclick="btnLoc_ServerClick">Lọc</a>
            </div>
        </div>
        <table id="example" class="display nowrap table-hover table-bordered" style="width: 100%; text-align: center">
            <thead>
                <tr>
                    <th>Tiết Học</th>
                    <th>Tiết 1</th>
                    <th>Tiết 2</th>
                    <th>Tiết 3</th>
                    <th>Tiết 4</th>
                    <th>Tiết 5</th>
                    <th>Tiết 6</th>
                    <th>Tiết 7</th>
                </tr>
            </thead>
            <tbody>
                <asp:Repeater ID="rpBookRoom" runat="server" OnItemDataBound="rpBookRoom_ItemDataBound">
                    <ItemTemplate>
                        <tr>
                            <td><%#Eval("lichbookphongthongminh_name") %></td>
                            <%-- <td><a href="#" class="btn btn-<%#Eval("lichbookphongthongminh_class") %>"><%#Eval("lichbookphongthongminh_status") %></a></td>--%>
                            <asp:Repeater ID="rpChiTiet" runat="server">
                                <ItemTemplate>
                                    <td><a href="#" class="btn btn-<%#Eval("lichbookphongchitiet_class") %>" data-toggle="modal" onclick="myLichbookphong(<%#Eval("lichbookphongchitiet_id") %>)" data-target="#exampleModalCenter"><%#Eval("giaovien") %></a></td>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
        </table>
        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div style="display: none">
                            <input type="text" id="txtId" runat="server" name="class" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label class="label-book">Lớp: *</label>

                            <input type="text" id="txtLop" runat="server" name="class" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label class="label-book">Môn Học: *</label>
                            <input type="text" name="class" id="txtMonHoc" runat="server" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label class="label-book">Nội Dung: *</label>
                            <input type="text" name="class" id="txtNoiDung" runat="server" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label class="label-book">Giáo Viên: *</label>
                            <input type="text" disabled name="class" id="txtGiaoVien" runat="server" class="form-control" />
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" id="txtGhiChu" runat="server" cols="50" rows="4" placeholder="Ghi chú ..."></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <a id="btnSave" runat="server" onserverclick="btnSave_ServerClick" class="btn btn-primary">Save</a>
                        <%--<button type="button" class="btn btn-primary">Save</button>--%>
                    </div>
                </div>
            </div>
        </div>
        <style>
            div.dataTables_wrapper {
                width: 90%;
                margin: 0 auto;
            }

            table.dataTable.nowrap th, table.dataTable.nowrap td {
                white-space: nowrap;
                padding: 10px 25px;
                text-align: center;
            }

            .modal-title {
                font-size: 25px;
            }

            .form-group label {
                font-weight: 600;
            }
        </style>
    </div>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="../js/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>

    <script>
        $(document).ready(function () {
            $('#example').DataTable({
                //"scrollY": 200,
                "ordering": false,
                "scrollX": true
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

