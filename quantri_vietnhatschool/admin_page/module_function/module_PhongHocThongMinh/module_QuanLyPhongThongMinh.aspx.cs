﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_QuanLyPhongThongMinh : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
  

    protected void Page_Load(object sender, EventArgs e)
    {
        //// Xem kết quả tổng kết
        if (!IsPostBack)
        {
            string thang = "/" + DateTime.Now.Month;
            rpBookRoom.DataSource = from nb in db.tbLichBookPhongThongMinhs 
                                    where nb.lichbookphongthongminh_name.Contains(thang) 
                                    select nb;
            rpBookRoom.DataBind();
        }
        var getKhachHang = from gv in db.admin_Users where gv.username_username == Request.Cookies["UserName"].Value select gv;
        txtGiaoVien.Value = getKhachHang.SingleOrDefault().username_fullname;
        
    }

    protected void btnThem_Click(object sender, EventArgs e)
    {

    }
    protected void rpBookRoom_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Repeater rpChiTiet = e.Item.FindControl("rpChiTiet") as Repeater;
        int lichbookphongthongminh_id = int.Parse(DataBinder.Eval(e.Item.DataItem, "lichbookphongthongminh_id").ToString());
        rpChiTiet.DataSource = from ct in db.tblichbookphongchitiets
                               where ct.lichbookphongthongminh_id == lichbookphongthongminh_id
                               select new { 
                                   ct.lichbookphongchitiet_class,
                                   ct.lichbookphongchitiet_id,
                                   giaovien = ct.lichbookphongchitiet_giaovien == null ? "Chưa book" : ct.lichbookphongchitiet_giaovien,
                               };
        rpChiTiet.DataBind();
    }
    protected void btnLoc_ServerClick(object sender, EventArgs e)
    {
        string thang = "/" + ddlThang.SelectedValue;
        rpBookRoom.DataSource = from nb in db.tbLichBookPhongThongMinhs where nb.lichbookphongthongminh_name.Contains(thang) select nb;
        rpBookRoom.DataBind();
    }
    protected void btnSave_ServerClick(object sender, EventArgs e)
    {
        try
        {
            if (txtLop.Value == "" || txtMonHoc.Value == "" || txtNoiDung.Value == "")
            {
                alert.alert_Error(Page, "Vui lòng nhập đầy đủ thông tin", "");
            }
            else
            {
                tblichbookphongchitiet update = (from l in db.tblichbookphongchitiets
                                                 where l.lichbookphongchitiet_id == Convert.ToInt32(txtId.Value)
                                                 select l).SingleOrDefault();
                if (update.lichbookphongchitiet_status == "chưa book")
                {
                    update.lichbookphongchitiet_lop = txtLop.Value;
                    update.lichbookphongchitiet_monhoc = txtMonHoc.Value;
                    update.lichbookphongchitiet_giaovien = Request.Cookies["UserName"].Value;
                    update.lichbookphongchitiet_status = "đã book";
                    update.lichbookphongchitiet_class = "danger";
                    update.lichbookphongchitiet_noidung = txtNoiDung.Value;

                    update.lichbookphongchitiet_createdate = DateTime.Now;

                    db.SubmitChanges();
                    string thang = "/" + ddlThang.SelectedValue;
                    rpBookRoom.DataSource = from nb in db.tbLichBookPhongThongMinhs where nb.lichbookphongthongminh_name.Contains(thang) select nb;
                    rpBookRoom.DataBind();
                    //update.lichbookphongchitiet_createbook =Convert.ToDateTime()

                }
                else
                {
                    alert.alert_Error(Page, "Phòng đã được book vui lòng chọn tiết khác chưa book!", "");
                    string thang = "/" + ddlThang.SelectedValue;
                    rpBookRoom.DataSource = from nb in db.tbLichBookPhongThongMinhs where nb.lichbookphongthongminh_name.Contains(thang) select nb;
                    rpBookRoom.DataBind();
                }
            }
        }
        catch(Exception ex)
        {
            alert.alert_Error(Page, "Lỗi!", "");
        }
    }
}