﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_TaoLichBookChoPhong.aspx.cs" Inherits="admin_page_module_function_module_TaoLichBookChoPhong" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="card card-block">
        <asp:UpdatePanel ID="udPopup" runat="server">
            <ContentTemplate>
                <div class="popup-main">
                    <%-- Start tiêu chuẩn 1--%>
                    <div class="div_content col-12">
                        <div class="col-12">
                            <div class="col-12 form-group">
                                <label class="col-12 form-control-label">Tạo lịch các địa điểm tổ chức lễ hội thể thao:</label>
                            </div>
                            <div class="col-12 form-group">
                                <div class="col-12">
                                    <asp:DropDownList ID="ddlThang" runat="server">
                                        <asp:ListItem Value="0" Text="Chọn tuần cần tạo"></asp:ListItem>
                                        <asp:ListItem Value="24" Text="Tuần 23"></asp:ListItem>
                                        <asp:ListItem Value="25" Text="Tuần 24"></asp:ListItem>
                                        <asp:ListItem Value="26" Text="Tuần 25"></asp:ListItem>
                                        <asp:ListItem Value="27" Text="Tuần 26"></asp:ListItem>
                                        <asp:ListItem Value="28" Text="Tuần 27"></asp:ListItem>
                                        <asp:ListItem Value="29" Text="Tuần 28"></asp:ListItem>
                                        <asp:ListItem Value="30" Text="Tuần 29"></asp:ListItem>
                                        <asp:ListItem Value="31" Text="Tuần 30"></asp:ListItem>
                                        <asp:ListItem Value="32" Text="Tuần 31"></asp:ListItem>
                                        <asp:ListItem Value="33" Text="Tuần 32"></asp:ListItem>
                                        <asp:ListItem Value="34" Text="Tuần 33"></asp:ListItem>
                                        <asp:ListItem Value="35" Text="Tuần 34"></asp:ListItem>
                                    </asp:DropDownList>

                                   <%-- <asp:DropDownList ID="ddlDiaDiem" runat="server">
                                        <asp:ListItem Value="0" Text="Chọn Địa điểm cần tạo"></asp:ListItem>
                                        <asp:ListItem Value="1" Text="Sân bóng đá"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Hội trường tầng 1"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Hội trường tầng 6"></asp:ListItem>
                                    </asp:DropDownList>--%>
                                     <asp:Button ID="btnLuu" runat="server" ClientIDMode="Static" Text="Tạo phòng" CssClass="btn btn-primary" OnClick="btnLuu_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

