﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_TaoLichBookChoPhong : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnLuu_Click(object sender, EventArgs e)
    {
        var checkThangTonTai = (from t in db.tbLichBookPhongThongMinhs
                                where t.tuan_id == Convert.ToInt16(ddlThang.SelectedValue)
                                select t).FirstOrDefault();
        if (checkThangTonTai != null)
        {
            alert.alert_Warning(Page, "Tuần này đã tạo vui lòng kiểm tra lại!", "");
        }
        else
        {

            for (int i = 2; i <= 6; i++)
            {
                tbLichBookPhongThongMinh insert = new tbLichBookPhongThongMinh();
                insert.lichbookphongthongminh_name = "Thứ " + i;
                //insert.lichphong_id = Convert.ToInt32(ddlDiaDiem.SelectedValue);
                insert.tuan_id = Convert.ToInt16(ddlThang.SelectedValue);
                db.tbLichBookPhongThongMinhs.InsertOnSubmit(insert);
                db.SubmitChanges();
                for (int j = 1; j <= 6; j++)
                {
                    tblichbookphongchitiet insertChiTiet = new tblichbookphongchitiet();
                    insertChiTiet.lichbookphongthongminh_id = insert.lichbookphongthongminh_id;
                    insertChiTiet.lichbookphongchitiet_class = "primary";
                    insertChiTiet.lichbookphongchitiet_status = "chưa book";
                    insertChiTiet.tiethoc_id = j;
                    db.tblichbookphongchitiets.InsertOnSubmit(insertChiTiet);
                    db.SubmitChanges();
                }
            }
            alert.alert_Success(Page, "Đã tạo xong", "");
        }
       
    }
}
