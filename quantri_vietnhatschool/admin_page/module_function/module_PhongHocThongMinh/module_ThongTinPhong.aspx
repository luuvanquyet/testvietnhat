﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_ThongTinPhong.aspx.cs" Inherits="admin_page_module_function_module_ThongTinPhong" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v17.1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
   <div class="title-thongke">
        <h4>Thống Kê Phòng Học Thông Minh</h4>
        <hr />
    </div>
    <div id="block_1" runat="server" class="card card-block">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label class="slgv" for="soluongGVDK">Số lượng giáo viên đăng ký :</label>
                        <input type="text" id="txtSoLuongDangKy" runat="server" name="slgvdk" style="text-align: right; padding-right: 5px" />
                        <label class="">&nbsp; / <%=lblTongGiaoVien %> Giáo Viên</label>
                        <a href="#" id="btnXem1" runat="server" onserverclick="btnXem1_ServerClick" class="btn btn-primary">Xem</a>
                    </div>
                    <div class="form-group">
                        <label class="slgv" for="soluongGVDK">Số lượng giáo viên không đăng ký :</label>
                        <input type="text" name="slgvdk" id="txtSoLuongKhongDangKy" runat="server" style="text-align: right; padding-right: 5px" />
                        <label class="">&nbsp; / <%=lblTongGiaoVien %> Giáo Viên</label>
                        <a href="#" id="btnXem2" runat="server" onserverclick="btnXem2_ServerClick" class="btn btn-primary">Xem</a>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label class="slgv" for="soluongGVDK">Số lượng giáo viên đăng ký nhưng không dùng phòng :</label>
                        <input type="text" id="txtDangKyKhongDungPhong" runat="server" name="slgvdk" style="text-align: right; padding-right: 5px" />
                        <label class="">&nbsp; / <%=lblTongDangKyGiaoVien %> Giáo Viên</label>
                        <a href="#" id="btnXem3" runat="server" onserverclick="btnXem3_ServerClick" class="btn btn-primary">Xem</a>
                    </div>
                    <div class="form-group">
                        <label class="slgv" for="soluongGVDK">Số lượng giáo viên dùng phòng học thông minh nhiều nhất :</label>
                        <input type="text" id="txtDangKyNhieuNhat" runat="server" name="slgvdk" style="text-align: right; padding-right: 5px" />
                        <label class="">&nbsp; /  <%=lblTongGiaoVien %> Giáo Viên</label>
                        <a href="#" id="btnXem4" runat="server" onserverclick="btnXem4_ServerClick" class="btn btn-primary">Xem</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Giáo viên</th>
                        <th scope="col">Số lượng (Lần)</th>

                    </tr>
                </thead>
                <tbody>
                    <asp:Repeater ID="rpXem1" runat="server">
                        <ItemTemplate>
                            <tr>
                                <th scope="row"><%=STT++ %></th>
                                <td><%#Eval("lichbookphongchitiet_giaovien") %></td>
                                <td style="text-align:center"><%#Eval("lichbookphongchitiet_sum") %></td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </tbody>
            </table>
        </div>
    </div>
    <style>
        .title-thongke h4 {
            text-align: center;
            margin-top: 50px;
            color: #2980b9;
            font-weight: 600;
            font-size: 30px;
        }

        .slgv {
            display: block;
        }
    </style>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

