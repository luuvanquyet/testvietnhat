﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_ThongTinPhong : System.Web.UI.Page
{
    public int STT = 1;
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    public string lblTongGiaoVien, lblTongDangKyGiaoVien;
    protected void Page_Load(object sender, EventArgs e)
    {
        var checkUser = from tk in db.admin_Users where tk.groupuser_id == 3 select tk;
        // Tổng giáo viên
        lblTongGiaoVien = checkUser.Count().ToString();
        // Kiểm tra số lượng giáo viên đăng ký trong phòng học
        var checkSoLuongDangKy = from u in db.tblichbookphongchitiets
                                 where u.lichbookphongchitiet_giaovien != null
                                 group u by u.lichbookphongchitiet_giaovien into g
                                 select g;
        if (checkSoLuongDangKy.Count() > 0)
        {
            // số lượng giáo viên đăng ký trong phòng học
            txtSoLuongDangKy.Value = checkSoLuongDangKy.Count() + "";
            lblTongDangKyGiaoVien = checkSoLuongDangKy.Count() + "";
            // Số lượng giáo viên không đăng ký trong phòng học
            txtSoLuongKhongDangKy.Value = (Convert.ToInt16(lblTongGiaoVien) - Convert.ToInt16(txtSoLuongDangKy.Value)) + "";
        }
        else
        {
            // số lượng giáo viên đăng ký trong phòng học
            txtSoLuongDangKy.Value = "0";
            lblTongDangKyGiaoVien = "0";
            // Số lượng giáo viên không đăng ký trong phòng học
            txtSoLuongKhongDangKy.Value = (Convert.ToInt16(lblTongGiaoVien) - Convert.ToInt16(txtSoLuongDangKy.Value)) + "";
        }
        // Kiểm tra số lường giáo viên đăng ký nhưng không dùng phòng
        var checkSoLuongDangKyKhongDungPhong = from sl in db.tblichbookphongchitiets
                                               where sl.lichbookphongchitiet_checkin == null && sl.lichbookphongchitiet_class == "danger"
                                               group sl by sl.lichbookphongchitiet_giaovien into g
                                               select g;
        if (checkSoLuongDangKyKhongDungPhong.Count() > 0)
        {
            txtDangKyKhongDungPhong.Value = checkSoLuongDangKyKhongDungPhong.Count() + "";
        }
        // Kiểm tra số lượng giáo viên dùng phòng học thông minh nhiều nhất
        var checkSoLuongGiaoVienDungPhongNhieuNhat = from gv in db.tblichbookphongchitiets
                                                     where gv.lichbookphongchitiet_checkin != null
                                                     group gv by gv.lichbookphongchitiet_giaovien into g
                                                     select g;
        if (checkSoLuongGiaoVienDungPhongNhieuNhat.Count() > 0)
        {
            txtDangKyNhieuNhat.Value = checkSoLuongGiaoVienDungPhongNhieuNhat.Count() + "";
        }
        else
        {
            txtDangKyNhieuNhat.Value = "0";
        }
    }
    protected void btnXem1_ServerClick(object sender, EventArgs e)
    {
        var list = from l in db.tblichbookphongchitiets
                   where l.lichbookphongchitiet_createdate != null
                   group l by l.lichbookphongchitiet_giaovien into g
                   select new
                   {
                       lichbookphongchitiet_giaovien = (from gv in db.admin_Users where gv.username_username == g.Key select gv).FirstOrDefault().username_fullname,
                       lichbookphongchitiet_sum = (from l in db.tblichbookphongchitiets
                                                   where l.lichbookphongchitiet_giaovien == g.Key
                                                   select l).Count().ToString()
                   };
        rpXem1.DataSource = list;
        rpXem1.DataBind();
    }
    protected void btnXem2_ServerClick(object sender, EventArgs e)
    {
        var listTong = db.admin_Users.Where(f => !db.tblichbookphongchitiets.Any(d => d.lichbookphongchitiet_giaovien == f.username_username) && f.groupuser_id == 3).Select(i => new { lichbookphongchitiet_giaovien = (from gv in db.admin_Users where gv.username_username == i.username_username select gv).FirstOrDefault().username_fullname, lichbookphongchitiet_sum = 0 });
        rpXem1.DataSource = listTong;
        rpXem1.DataBind();
    }

    protected void btnXem3_ServerClick(object sender, EventArgs e)
    {
        var checkSoLuongDangKyKhongDungPhong = from sl in db.tblichbookphongchitiets
                                               where sl.lichbookphongchitiet_checkin == null && sl.lichbookphongchitiet_class == "danger"
                                               group sl by sl.lichbookphongchitiet_giaovien into g
                                               select new
                                               {
                                                   lichbookphongchitiet_giaovien = (from gv in db.admin_Users where gv.username_username == g.Key select gv).FirstOrDefault().username_fullname,
                                                   lichbookphongchitiet_sum = (from l in db.tblichbookphongchitiets
                                                                               where l.lichbookphongchitiet_giaovien == g.Key && l.lichbookphongchitiet_checkin == null
                                                                               select l).Count().ToString()
                                               };
        if (checkSoLuongDangKyKhongDungPhong.Count() > 0)
        {
            rpXem1.DataSource = checkSoLuongDangKyKhongDungPhong;
            rpXem1.DataBind();
        }
    }

    protected void btnXem4_ServerClick(object sender, EventArgs e)
    {
        var checkSoLuongGiaoVienDungPhongNhieuNhat = (from gv in db.tblichbookphongchitiets
                                                      where gv.lichbookphongchitiet_checkin != null
                                                      group gv by gv.lichbookphongchitiet_giaovien into g
                                                      select new
                                                      {
                                                          lichbookphongchitiet_giaovien = (from gv in db.admin_Users where gv.username_username == g.Key select gv).FirstOrDefault().username_fullname,
                                                          lichbookphongchitiet_sum = (from l in db.tblichbookphongchitiets
                                                                                      where l.lichbookphongchitiet_giaovien == g.Key && l.lichbookphongchitiet_checkin != null
                                                                                      select l).Count().ToString()
                                                      }).OrderByDescending(x => x.lichbookphongchitiet_sum);
        if (checkSoLuongGiaoVienDungPhongNhieuNhat.Count() > 0)
        {
            rpXem1.DataSource = checkSoLuongGiaoVienDungPhongNhieuNhat;
            rpXem1.DataBind();
        }
    }
}