﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_CongVanDiThuThu : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    string image;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["_id"] = 0;
        }
        loadData();

    }
    private void loadData()
    {
        var getData = from n in db.tbQuanLyCongVanDis
                      orderby n.congvandi_id descending
                      where n.loaicongvan_id==2
                      select new
                      {
                          n.congvandi_id,
                          n.congvandi_name,
                          n.congvandi_file,
                          n.congvandi_createdate,
                          n.congvandi_sohieu,
                          n.congvandi_soluong,
                          n.congvandi_ngayvanban,
                          n.congvandi_noinhanvanban,
                          n.congvandi_trachnhiem,
                          n.congvandi_nguoiky,
                          n.congvandi_ghichu,
                          n.congvan_soden,
                          n.congvan_tacgia,
                          n.congvan_tinhtrang_dachuyen
                      };
        grvList.DataSource = getData;
        grvList.DataBind();
        //ddlBoPhan.DataSource = from bp in db.tbBoPhans orderby bp.bophan_position select bp;
        //ddlBoPhan.DataBind();
        lkBoPhan.DataSource = from bp in db.tbBoPhans orderby bp.bophan_position select bp;
        lkBoPhan.DataBind();

        //ddlUser.DataSource = from tb in db.admin_Users
        //                            select tb;
        //ddlUser.DataBind();
        //if (lkBoPhan.Value != "")
        //{
        var listNV = from nv in db.admin_Users select nv;
        lkNhanVien.DataSource = listNV;
        lkNhanVien.DataBind();
        //if (lkBoPhan.Text!="")
        //{
        //    string listIDBP = lkBoPhan.Text;
        //    string[] checkedBP = listIDBP.Split(',');
        //    foreach (var item in checkedBP)
        //    {

        //    }
        //}
        //}
        //get ds nhân viên

    }
    private void setNULL()
    {
        //txtSoDen.Text = "";
        //txtTacGia.Text = "";
        txtTneLoai.Text = "";
        txtGhiChu.Text = "";
       // txtNguoiKy.Text = "";
        txtSoKyHieu.Text = "";
        //txtSoLuong.Text = "";
        lkBoPhan.Text = "";
        lkNhanVien.Text = "";
    }
    protected void btnThem_Click(object sender, EventArgs e)
    {
        Session["_id"] = 0;
        setNULL();
        //txtNguoiKy.Text = (from u in db.admin_Users where u.username_username == Request.Cookies["UserName"].Value select u).First().username_fullname;
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupControl.Show();", true);
    }
    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "congvandi_id" }));
        Session["_id"] = _id;
        var getData = (from n in db.tbQuanLyCongVanDis
                       join u in db.admin_Users on n.username_id equals u.username_id
                       where n.congvandi_id == _id
                       select new
                       {
                           n.congvandi_id,
                           n.congvandi_name,
                           n.congvandi_file,
                           n.congvandi_trachnhiem,
                           n.congvandi_ghichu,
                           n.congvandi_sohieu,
                           n.congvandi_soluong,
                           n.congvandi_nguoiky,
                           n.congvandi_ngayvanban,
                           n.congvandi_noinhanvanban,
                           
                           u.username_fullname
                       }).Single();
        txtSoKyHieu.Text = getData.congvandi_sohieu;
        dteNgayThangGui.Value = getData.congvandi_ngayvanban.Value.ToString("yyyy-MM-dd").Replace(' ', 'T');
        //txtNguoiKy.Text = getData.congvandi_nguoiky;
        // txtSoLuong.Text = getData.congvandi_soluong;
        txtTneLoai.Text = getData.congvandi_name;
        txtGhiChu.Text = getData.congvandi_ghichu;
        var getcongvanden = from cv in db.tbQuanLyCongVanDens
                            join u in db.admin_Users on cv.username_id equals u.username_id
                            where cv.congvandi_id == _id
                            select new
                            {
                                cv.congvandi_id,
                                cv.username_id,
                                u.bophan_id,
                            };
        string nv, bp;

        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupControl.Show();showImg1_1('" + "images/icon_PDF.png" + "'); ", true);
        // loadData();
    }
    protected void btnXoa_Click(object sender, EventArgs e)
    {
        cls_CongVanDi cls;
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "congvandi_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                cls = new cls_CongVanDi();
                tbQuanLyCongVanDi checkImage = (from i in db.tbQuanLyCongVanDis where i.congvandi_id == Convert.ToInt32(item) select i).SingleOrDefault();
                if (cls.Linq_Xoa(Convert.ToInt32(item)))
                {
                    var getcongvanden = from cvd in db.tbQuanLyCongVanDens
                                        where cvd.congvandi_id == Convert.ToInt32(item)
                                        select cvd;
                    foreach (var cv in getcongvanden)
                    {
                        db.tbQuanLyCongVanDens.DeleteOnSubmit(cv);
                        db.SubmitChanges();
                    }
                    alert.alert_Success(Page, "Xóa thành công", "");
                }
                else
                    alert.alert_Error(Page, "Xóa thất bại", "");
            }
        }
        else
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
        popupControl.ShowOnPageLoad = false;
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "grvList.Refresh();", true);
    }

    private bool SendMail(string email)
    {

        if (email != "")
        {
            try
            {
                var fromAddress = "thongbaovietnhatschool@gmail.com";//  Email Address from where you send the mail 
                var toAddress = email;
                const string fromPassword = "neiabcekdjluofid";
                string subject, title;
                title = "Thầy có công văn mới ạ";
                subject = "<!DOCTYPE html><html><head><title></title></head><body ><div>" +
                "<h3 style=\"margin-top:0px; text-align:center; color:#029ada\">Thầy có công văn mới. Xem chi tiết <a href='http://quantri.vietnhatschool.edu.vn/admin-duyet-cong-van-noi-bo-van-thu'>tại đây.</a></h3>" +
                "</div></body></html>";
                var smtp = new System.Net.Mail.SmtpClient();
                {
                    smtp.Host = "smtp.gmail.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    smtp.Credentials = new NetworkCredential(fromAddress, fromPassword);
                    smtp.Timeout = 20000;
                }
                MailMessage mm = new MailMessage();
                mm.From = new MailAddress(fromAddress, "Trường Liên cấp Việt Nhật");
                mm.Subject = title;
                mm.To.Add(toAddress);
                mm.IsBodyHtml = true;
                mm.Body = subject;
                smtp.Send(mm);
                return true;
            }
            catch
            {
                return false;
            }
        }
        else
            return false;
    }
    protected void btnLuu_Click(object sender, EventArgs e)
    {
        if (Page.IsValid && FileUpload1.HasFile)
        {
            String folderUser = Server.MapPath("~/File_CongVan/");
            if (!Directory.Exists(folderUser))
            {
                Directory.CreateDirectory(folderUser);
            }
            //string filename;
            string ulr = "/File_CongVan/";
            HttpFileCollection hfc = Request.Files;
            string filename = Path.GetFileName(FileUpload1.FileName);
            string fileName_save = Path.Combine(Server.MapPath("~/File_CongVan"), filename);
            FileUpload1.SaveAs(fileName_save);
            image = ulr + filename;
        }
        cls_CongVanDi cls = new cls_CongVanDi();

        var getuser = from u in db.admin_Users
                      where u.username_username == Request.Cookies["UserName"].Value
                      select u;
        if (Session["_id"].ToString() == "0")
        {
            //List<object> selectedKey = grvl_GiaoVien.GridView.GetSelectedFieldValues(new string[] { "username_id" });
            //lưu ở bảng công văn đi
            tbQuanLyCongVanDi insert = new tbQuanLyCongVanDi();
            if (image == null)
                insert.congvandi_file = "images/icon_PDF.png";
            else
                insert.congvandi_file = image;
           // insert.congvandi_createdate = Convert.ToDateTime(dteNgayThangDen.Value);
            //insert.congvan_soden = txtSoDen.Text;
            //insert.congvan_tacgia = txtTacGia.Text;
            insert.congvandi_sohieu = txtSoKyHieu.Text;
            insert.congvandi_ngayvanban = Convert.ToDateTime(dteNgayThangGui.Value);
            insert.congvandi_name = txtTneLoai.Text;
            insert.congvandi_soluong = txtSoLuong.Text;
            // insert.username_id = getuser.FirstOrDefault().username_id;
            insert.congvandi_ghichu = txtGhiChu.Text;
            //insert.congvandi_soluong = txtSoLuong.Text;
            //insert.congvandi_nguoiky = txtNguoiKy.Text;

            insert.congvan_tinhtrang_dachuyen = "Đã chuyển chờ duyệt";
            insert.loaicongvan_id = 2;
            db.tbQuanLyCongVanDis.InsertOnSubmit(insert);
            db.SubmitChanges();
            SendMail("phungduc1989@gmail.com");
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "AlertBox", "swal('Đã chuyển tới thầy Hiệu Trưởng!', '','success').then(function(){window.location = '/admin-cong-van-di-van-thu';})", true);
        }
        popupControl.ShowOnPageLoad = false;
        loadData();
    }
    protected void btnChuyenCongVan_Click(object sender, EventArgs e)
    {
          _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "congvandi_id" }));
          Response.Redirect("admin-chuyen-cong-van-toi-cac-bo-phan-" + _id);
    }
}