﻿<%@ Page Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_NoiDungChuyenCongVanToiCacBoPhan.aspx.cs" Inherits="admin_page_module_function_module_QuanLyDeadline_module_NoiDungChuyenCongVanToiCacBoPhan" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v17.1" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <script>
        function CountLeft(field, count, max) {
            if (field.value.length > max)
                field.value = field.value.substring(0, max);
            else
                count.value = max - field.value.length;
        }
    </script>
    <script type="text/javascript">
        function CloseGridLookup() {
            lkBoPhan.ConfirmCurrentSelection();
            lkBoPhan.HideDropDown();
            lkBoPhan.Focus();
        }
    </script>
    <style>
        .border-left {
            border: 1px solid #808080;
            padding: 10px 10px 10px 10px;
            width: 100%;
            height: auto;
        }
          .border-right {
            border: 1px solid #808080;
            padding: 8px 10px 6px 10px;
            width: 72%;
            height: auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="card card-block">
        <div class="col-12 form-group">
            <asp:Button ID="btnQuayLai" runat="server" Text="Quay lại" CssClass="btn btn-primary" OnClick="btnQuayLai_Click" />
        </div>
        <div class="div_content col-12">

            <div class="col-3" style="margin: 5px;">
                <div class="border-left">
                   
                    <p>
                        <label class="form-control-label">Ngày, tháng văn bản:</label>&nbsp;<asp:Label ID="lblNgayThangGui" runat="server"></asp:Label>
                    </p>
                   
                    <p>
                        <label class="form-control-label">Số ký hiệu, văn bản:</label>&nbsp;<asp:Label ID="lblSoKyHieu" runat="server"></asp:Label>
                    </p>
                    <p>
                        <label class="form-control-label">Người ký:</label>&nbsp;<asp:Label ID="lblNguoiKy" runat="server"></asp:Label>
                    </p>
                    <p>
                        <label class="form-control-label">Tên loại và trích yếu nội dung văn bản:</label>&nbsp;<asp:Label ID="lblTenLoai" runat="server"></asp:Label>
                    </p>
                    <p>
                        <label class="form-control-label">Ghi chú:</label>&nbsp;<asp:Label ID="lblGhiChu" runat="server"></asp:Label>
                    </p>
                </div>
                <br />
              <%--  <div class="border-left">
                </div>--%>
            </div>
            <div class="col-8 border-right">
                <asp:UpdatePanel ID="upVanBan" runat="server">
                        <ContentTemplate>
                            <label class="form-control-label">Nơi nhận văn bản:</label>
                            <div class="">
                                <%--<dx:ASPxComboBox ID="ddlBoPhan" NullText="Chọn bộ phận" runat="server" ValueType="System.Int32" TextField="bophan_name" ValueField="bophan_id" ClientInstanceName="ddlUser" AutoPostBack="true" CssClass="" Width="97%"></dx:ASPxComboBox>--%>
                                <dx:ASPxGridLookup ID="lkBoPhan" runat="server" SelectionMode="Multiple" ClientInstanceName="lkBoPhan"
                                    KeyFieldName="bophan_id" Width="170px" TextFormatString="{0}" MultiTextSeparator=", " Caption="Bộ phận">
                                    <Columns>
                                        <dx:GridViewCommandColumn ShowSelectCheckbox="True" />
                                        <dx:GridViewDataColumn FieldName="bophan_name" Caption="Bộ phận" />
                                        <dx:GridViewDataColumn FieldName="bophan_id" Settings-AllowAutoFilter="false" Visible="false" />
                                    </Columns>
                                    <GridViewProperties>
                                        <Templates>
                                            <StatusBar>
                                                <table class="OptionsTable" style="float: right">
                                                    <tr>
                                                        <td>
                                                            <dx:ASPxButton ID="Close" runat="server" AutoPostBack="true" Text="Chọn" ClientSideEvents-Click="CloseGridLookup" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </StatusBar>
                                        </Templates>
                                        <Settings ShowFilterRow="True" ShowStatusBar="Visible" />
                                        <SettingsPager PageSize="10" EnableAdaptivity="true" />
                                    </GridViewProperties>
                                </dx:ASPxGridLookup>
                            </div>
                            <div>
                                <div class="form-group table-responsive">
                                    <dx:ASPxGridView ID="grvList" runat="server" ClientInstanceName="grvList" KeyFieldName="username_id" Width="100%">
                                        <Columns>
                                            <dx:GridViewCommandColumn ShowSelectCheckbox="True" SelectAllCheckboxMode="Page" VisibleIndex="0" Width="2%">
                                            </dx:GridViewCommandColumn>
                                            <dx:GridViewDataColumn Caption="Họ tên" FieldName="username_fullname" HeaderStyle-HorizontalAlign="Center" Width="40%"></dx:GridViewDataColumn>
                                        </Columns>
                                        <SettingsSearchPanel Visible="true" />
                                        <SettingsBehavior AllowFocusedRow="true" />
                                        <SettingsText EmptyDataRow="Không có dữ liệu" SearchPanelEditorNullText="Gỏ từ cần tìm kiếm và enter..." />
                                        <SettingsLoadingPanel Text="Đang tải..." />
                                        <SettingsPager PageSize="50" Summary-Text="Trang {0} / {1} ({2} trang)"></SettingsPager>
                                    </dx:ASPxGridView>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                <%--<asp:Literal ID="ltEmbed" runat="server" />--%>
                <asp:Button ID="btnChuyen" runat="server" Text="Chuyển" OnClick="btnChuyen_Click" CssClass="btn btn-primary" />
            </div>
        </div>

    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
    <script type="text/javascript">
        function clickavatar1() {
            $("#up1 input[type=file]").click();
        }
    </script>
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>
