﻿<%@ Page Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_NoiDungCongVanDen.aspx.cs" Inherits="admin_page_module_function_module_QuanLyDeadline_module_NoiDungCongVanDen" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v17.1" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <script>
        function CountLeft(field, count, max) {
            if (field.value.length > max)
                field.value = field.value.substring(0, max);
            else
                count.value = max - field.value.length;
        }
    </script>
    <style>
        .border-left {
            border: 1px solid #808080;
            padding: 10px 10px 10px 10px;
            width: 100%;
            height: auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="card card-block">
        <div class="col-12 form-group">
            <asp:Button ID="btnQuayLai" runat="server" Text="Quay lại" CssClass="btn btn-primary" OnClick="btnQuayLai_Click" />
        </div>
        <div class="div_content col-12">
            <div class="col-3" style="margin: 5px;">
                <div class="border-left">
                    <p>
                        <label class="form-control-label">Ngày, tháng đến: </label>
                        &nbsp;<asp:Label ID="lblNgayThangDen" runat="server"></asp:Label>
                    </p>
                    <p>
                        <label class="form-control-label">Ngày, tháng văn bản:</label>&nbsp;<asp:Label ID="lblNgayThangGui" runat="server"></asp:Label>
                    </p>
                    <p>
                        <label class="form-control-label">Số đến:</label>&nbsp;<asp:Label ID="lblSoDen" runat="server"></asp:Label>
                    </p>
                    <p>
                        <label class="form-control-label">Số ký hiệu, văn bản:</label>&nbsp;<asp:Label ID="lblSoKyHieu" runat="server"></asp:Label>
                    </p>
                    <p>
                        <label class="form-control-label">Người ký:</label>&nbsp;<asp:Label ID="lblNguoiKy" runat="server"></asp:Label>
                    </p>
                    <p>
                        <label class="form-control-label">Tác giả:</label>&nbsp;<asp:Label ID="lblTacGia" runat="server"></asp:Label>
                    </p>
                    <p>
                        <label class="form-control-label">Tên loại và trích yếu nội dung văn bản:</label>&nbsp;<asp:Label ID="lblTenLoai" runat="server"></asp:Label>
                    </p>
                    <p>
                        <label class="form-control-label">Ghi chú:</label>&nbsp;<asp:Label ID="lblGhiChu" runat="server"></asp:Label>
                    </p>
                </div>
            </div>
            <div class="col-8">
                <asp:Literal ID="ltEmbed" runat="server" />
                <asp:Button ID="btnDaXem" runat="server" Text="Đã xem" OnClick="btnDaXem_Click" CssClass="btn btn-primary" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
    <script type="text/javascript">
        function clickavatar1() {
            $("#up1 input[type=file]").click();
        }
    </script>
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>
