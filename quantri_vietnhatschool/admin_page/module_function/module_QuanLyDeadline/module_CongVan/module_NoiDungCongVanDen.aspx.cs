﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_QuanLyDeadline_module_NoiDungCongVanDen : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    protected void Page_Load(object sender, EventArgs e)
    {
        var getData = (from cv in db.tbQuanLyCongVanDens
                       join n in db.tbQuanLyCongVanDis on cv.congvandi_id equals n.congvandi_id
                       join u in db.admin_Users on cv.username_id equals u.username_id
                       where cv.congvanden_id == Convert.ToInt32(RouteData.Values["id"])
                       select new
                       {
                           n.congvandi_createdate,
                           n.congvan_soden,
                           n.congvan_tacgia,
                           n.congvandi_id,
                           n.congvandi_name,
                           n.congvandi_file,
                           n.congvandi_trachnhiem,
                           n.congvandi_ghichu,
                           n.congvandi_sohieu,
                           n.congvandi_soluong,
                           n.congvandi_nguoiky,
                           n.congvandi_ngayvanban,
                       }).Single();
        lblNgayThangDen.Text = getData.congvandi_createdate.Value.ToString("dd/MM/yyyy").Replace(' ', 'T');
        lblSoDen.Text = getData.congvan_soden;
        lblSoKyHieu.Text = getData.congvandi_sohieu;
        lblTacGia.Text = getData.congvan_tacgia;
        lblNgayThangGui.Text = getData.congvandi_ngayvanban.Value.ToString("dd/MM/yyyy").Replace(' ', 'T');
        lblNguoiKy.Text = getData.congvandi_nguoiky;
        // txtSoLuong.Text = getData.congvandi_soluong;
        lblTenLoai.Text = getData.congvandi_name;
        lblGhiChu.Text = getData.congvandi_ghichu;
        string embed = "<object data=\"{0}\" type=\"application/pdf\" width=\"100%\" height=\"500px\">";
        embed += "If you are unable to view file, you can download from <a href = \"{0}\">here</a>";
        embed += " or download <a target = \"_blank\" href = \"http://get.adobe.com/reader/\">Adobe PDF Reader</a> to view the file.";
        embed += "</object>";
        ltEmbed.Text = string.Format(embed, ResolveUrl(getData.congvandi_file));
       
    }

    protected void btnQuayLai_Click(object sender, EventArgs e)
    {
        Response.Redirect("/admin-cong-van-den");
    }
    protected void btnDaXem_Click(object sender, EventArgs e)
    {
        tbQuanLyCongVanDen checkCongVanDen = (from cvd in db.tbQuanLyCongVanDens where cvd.congvanden_id == Convert.ToInt32(RouteData.Values["id"]) select cvd).SingleOrDefault();
        checkCongVanDen.congvanden_status = "Đã xem";
        db.SubmitChanges();
        alert.alert_Success(Page, "Đã báo về BGH", "");
    }
}