﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="module_NoiDungCongvanChuyenDen_Vesrion2.aspx.cs" Inherits="admin_page_module_function_module_QuanLyDeadline_module_CongVan_module_NoiDungCongvanChuyenDen_Vesrion2" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v17.1" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="/admin_css/vendor.css" />
    <link rel="stylesheet" href="/admin_css/app-blue.css" />
    <link href="/admin_css/admin_style.css" rel="stylesheet" />
    <link href="/admin_css/datepicker.min.css" rel="stylesheet" />
    <link href="css/Lan.css" rel="stylesheet" />
    <script src="/admin_js/sweetalert.min.js"></script>
    <script src="../../../js/jquery-3.5.1.min.js"></script>
    <script src="../../../admin_js/js_index/index.js"></script>
    <link href="../../../admin_css/css_index/index.css" rel="stylesheet" />
    <script>
        function CountLeft(field, count, max) {
            if (field.value.length > max)
                field.value = field.value.substring(0, max);
            else
                count.value = max - field.value.length;
        }
    </script>
    <style>
        .border-left {
            border: 1px solid #808080;
            padding: 10px 10px 10px 10px;
            width: 100%;
            height: auto;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scrvanban" runat="server"></asp:ScriptManager>
        <div class="card card-block">
            <div class="col-12 form-group">
                <asp:Button ID="btnQuayLai" runat="server" Text="Quay lại" CssClass="btn btn-primary" OnClick="btnQuayLai_Click" />
            </div>
            <div class="div_content col-12">

                <div class="col-3" style="margin: 5px;">
                    <div class="border-left">
                        <p>
                            <label class="form-control-label">Ngày, tháng đến: </label>
                            &nbsp;<asp:Label ID="lblNgayThangDen" runat="server"></asp:Label>
                        </p>
                        <p>
                            <label class="form-control-label">Ngày, tháng văn bản:</label>&nbsp;<asp:Label ID="lblNgayThangGui" runat="server"></asp:Label>
                        </p>
                        <p>
                            <label class="form-control-label">Số đến:</label>&nbsp;<asp:Label ID="lblSoDen" runat="server"></asp:Label>
                        </p>
                        <p>
                            <label class="form-control-label">Số ký hiệu, văn bản:</label>&nbsp;<asp:Label ID="lblSoKyHieu" runat="server"></asp:Label>
                        </p>
                        <p>
                            <label class="form-control-label">Người ký:</label>&nbsp;<asp:Label ID="lblNguoiKy" runat="server"></asp:Label>
                        </p>
                        <p>
                            <label class="form-control-label">Tác giả:</label>&nbsp;<asp:Label ID="lblTacGia" runat="server"></asp:Label>
                        </p>
                        <p>
                            <label class="form-control-label">Tên loại và trích yếu nội dung văn bản:</label>&nbsp;<asp:Label ID="lblTenLoai" runat="server"></asp:Label>
                        </p>
                        <p>
                            <label class="form-control-label">Ghi chú:</label>&nbsp;<asp:Label ID="lblGhiChu" runat="server"></asp:Label>
                        </p>
                    </div>
                    <br />
                    <div class="border-left">
                        <asp:UpdatePanel ID="upVanBan" runat="server">
                            <ContentTemplate>
                                <label class="form-control-label">Nội dung chú thích:</label>
                                <textarea id="txtNoiDungGhichu"  runat="server" name="name" rows="3" cols="50"></textarea>
                                <div class="">
                                </div>
                                <div>
                                    <label class="form-control-label">Nơi nhận văn bản:</label>
                                    <div class="form-group table-responsive">
                                        <dx:ASPxGridView ID="grvList" runat="server" ClientInstanceName="grvList" KeyFieldName="username_id" Width="100%">
                                            <Columns>
                                                <dx:GridViewCommandColumn ShowSelectCheckbox="True" SelectAllCheckboxMode="Page" VisibleIndex="0" Width="2%">
                                                </dx:GridViewCommandColumn>
                                                <dx:GridViewDataColumn Caption="Bộ phận" FieldName="bophan_name" HeaderStyle-HorizontalAlign="Center" Width="40%"></dx:GridViewDataColumn>
                                                <dx:GridViewDataColumn Caption="Trưởng bộ phận" FieldName="username_fullname" HeaderStyle-HorizontalAlign="Center" Width="40%"></dx:GridViewDataColumn>
                                            </Columns>
                                            <SettingsSearchPanel Visible="false" />
                                            <SettingsBehavior AllowFocusedRow="true" />
                                            <SettingsText EmptyDataRow="Không có dữ liệu" />
                                            <SettingsLoadingPanel Text="Đang tải..." />
                                            <SettingsPager PageSize="10" Summary-Text="Trang {0} / {1} ({2} trang)"></SettingsPager>
                                        </dx:ASPxGridView>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </div>
                </div>
                <%--<div class="fixed__test"></div>--%>
                <div class="col-8 fixed__congvan">
                    <%--<h4 id="txtCongVan" class="fixed__congvan--title"></h4>--%>
                    <asp:Literal ID="ltEmbed" runat="server" />
                    <asp:Button ID="btnChuyen" runat="server" Text="Chuyển" OnClick="btnChuyen_Click" CssClass="btn btn-primary" />
                </div>
            </div>

        </div>
        <script type="text/javascript">
            function clickavatar1() {
                $("#up1 input[type=file]").click();
            }
        </script>
    </form>
</body>
</html>
