﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_QuanLyDeadline_module_CongVan_module_NoiDungCongvanChuyenDen_Vesrion2 : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            var getData = (from n in db.tbQuanLyCongVanDis
                           where n.congvandi_id == Convert.ToInt32(RouteData.Values["id"])
                           select new
                           {
                               n.congvandi_createdate,
                               n.congvan_soden,
                               n.congvan_tacgia,
                               n.congvandi_id,
                               n.congvandi_name,
                               n.congvandi_file,
                               n.congvandi_trachnhiem,
                               n.congvandi_ghichu,
                               n.congvandi_sohieu,
                               n.congvandi_soluong,
                               n.congvandi_nguoiky,
                               n.congvandi_ngayvanban,
                           }).Single();
            lblNgayThangDen.Text = getData.congvandi_createdate.Value.ToString("dd/MM/yyyy").Replace(' ', 'T');
            lblSoDen.Text = getData.congvan_soden;
            lblSoKyHieu.Text = getData.congvandi_sohieu;
            lblTacGia.Text = getData.congvan_tacgia;
            lblNgayThangGui.Text = getData.congvandi_ngayvanban.Value.ToString("dd/MM/yyyy").Replace(' ', 'T');
            lblNguoiKy.Text = getData.congvandi_nguoiky;
            // txtSoLuong.Text = getData.congvandi_soluong;
            lblTenLoai.Text = getData.congvandi_name;
            lblGhiChu.Text = getData.congvandi_ghichu;
            string embed = "<object data=\"{0}\" type=\"application/pdf\" width=\"100%\" height=\"800px\">";
            embed += "If you are unable to view file, you can download from <a href = \"{0}\">here</a>";
            embed += " or download <a target = \"_blank\" href = \"http://get.adobe.com/reader/\">Adobe PDF Reader</a> to view the file.";
            embed += "</object>";
            ltEmbed.Text = string.Format(embed, ResolveUrl(getData.congvandi_file));
        }
       
        var list = from bp in db.tbBoPhans
                   join u in db.admin_Users on bp.bophan_id equals u.bophan_id
                   where u.username_truongbophan == true && bp.hidden ==true
                   select new
                   {
                       u.username_id,
                       bp.bophan_name,
                       u.username_fullname,
                   };
        grvList.DataSource = list;
        grvList.DataBind();
    }

    protected void btnQuayLai_Click(object sender, EventArgs e)
    {
        Response.Redirect("/admin-quan-ly-cong-van");
    }
    protected class User_BoPhan
    {
        public int username_id { get; set; }
        public string username_fullname { get; set; }
    }

    protected void btnChuyen_Click(object sender, EventArgs e)
    {
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "username_id" });
        tbQuanLyCongVanDi checkCongVan = (from cv in db.tbQuanLyCongVanDis
                                          where cv.congvandi_id == Convert.ToInt32(RouteData.Values["id"])
                                          select cv).SingleOrDefault();
        // Cập nhật lại tình trạng để bên văn thư biết là thầy HT đã chuyển công văn tới các bộ phận.
        checkCongVan.congvan_tinhtrang_dachuyen = "Đã chuyển công văn tới các bộ phận";
        checkCongVan.congvandi_nguoiky = "Thầy Đặng Thanh";
        //checkCongVan.congvandi_noidungthayHT = txtNoiDungGhichu.Value;
        db.SubmitChanges();
        foreach (var item in selectedKey)
        {
            tbQuanLyCongVanDen insert = new tbQuanLyCongVanDen();
            insert.congvandi_id = Convert.ToInt32(RouteData.Values["id"]);
            insert.username_id = Convert.ToInt32(item);
            insert.congvanden_noidungthayHT = txtNoiDungGhichu.Value;
            // Nắm bắt tình trạng để thầy Hiệu Trưởng biết các bộ phận có xem hay không?
            insert.congvanden_status = "Chưa xem";
            db.tbQuanLyCongVanDens.InsertOnSubmit(insert);
            db.SubmitChanges();
        }
        // Còn thiếu gửi về mail cho các username của các bộ phận.
        alert.alert_Success(Page, "Đã chuyển công văn tới các bộ phận", "");
    }
}