﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_CongVanDen : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    string image;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["_id"] = 0;
        }
        loadData();

    }
    private void loadData()
    {
        var getuser = (from u in db.admin_Users
                       where u.username_username == Request.Cookies["UserName"].Value
                       select u).FirstOrDefault();
        var getData = from n in db.tbQuanLyCongVanDens
                      join cv in db.tbQuanLyCongVanDis on n.congvandi_id equals cv.congvandi_id
                      join u in db.admin_Users on cv.username_id equals u.username_id
                      where cv.congvandi_content.Contains(getuser.username_id.ToString()) && n.username_id == getuser.username_id
                      select new
                      {
                          n.congvanden_id,
                          cv.congvandi_id,
                          cv.congvandi_file,
                          cv.username_id,
                          u.username_fullname,
                          cv.congvandi_createdate,
                          cv.congvandi_name
                      };
        grvList.DataSource = getData;
        grvList.DataBind();
        //ddlloaisanpham.DataSource = from tb in db.admin_Users
        //                            select tb;
        //ddlloaisanpham.DataBind();

    }
    private void setNULL()
    {
        //txtTieuDe.Text = "";
        //imgPreview.Src = "";
    }
    protected void btnThem_Click(object sender, EventArgs e)
    {
        Session["_id"] = 0;
        setNULL();
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupControl.Show();", true);
    }
    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "congvanden_id" }));
        Session["_id"] = _id;
        Response.Redirect("/admin-noi-dung-cong-van-den-"+_id);
       

    }


}