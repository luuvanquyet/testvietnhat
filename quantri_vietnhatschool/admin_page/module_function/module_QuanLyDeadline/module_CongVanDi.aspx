﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_CongVanDi.aspx.cs" Inherits="admin_page_module_function_module_CongVanDi" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v17.1" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <script>
        function CountLeft(field, count, max) {
            if (field.value.length > max)
                field.value = field.value.substring(0, max);
            else
                count.value = max - field.value.length;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <script type="text/javascript">
        function func() {
            grvList.Refresh();
            popupControl.Hide();
        }
        function btnChiTiet() {
            document.getElementById('<%=btnChiTiet.ClientID%>').click();
        }
        function popupHide() {
            document.getElementById('btnClosePopup').click();
        }
        function checkNULL() {
            var CityName = document.getElementById('<%= txtTieuDe.ClientID%>');

            if (CityName.value.trim() == "") {
                swal('Tên form không được để trống!', '', 'warning').then(function () { CityName.focus(); });
                return false;
            }
            return true;
        }
        function confirmDel() {
            swal("Bạn có thực sự muốn xóa?",
                "Nếu xóa, dữ liệu sẽ không thể khôi phục.",
                "warning",
                {
                    buttons: true,
                    dangerMode: true
                }).then(function (value) {
                    if (value == true) {
                        var xoa = document.getElementById('<%=btnXoa.ClientID%>');
                        xoa.click();
                    }
                });
        }

        function showPreview(input) {
            if (input.files && input.files[0]) {
                var filerdr = new FileReader();
                filerdr.onload = function (e) {
                    $('#hibodywrapper_popupControl_imgPreview').attr('src', e.target.result);
                }
                filerdr.readAsDataURL(input.files[0]);
            }
        }
        function showPreview1(input) {
            if (input.files && input.files[0]) {
                var filerdr = new FileReader();
                filerdr.onload = function (e) {
                    $('#imgPreview1').attr('src', e.target.result);
                }
                filerdr.readAsDataURL(input.files[0]);
            }
        }
        function showImg(img) {
            $('#hibodywrapper_popupControl_imgPreview').attr('src', img);
        }
        function showImg1_1(img) {
            $('#imgPreview1').attr('src', img);
        }
    </script>
    <script type="text/javascript">
        function CloseGridLookup() {
            lkBoPhan.ConfirmCurrentSelection();
            lkBoPhan.HideDropDown();
            lkBoPhan.Focus();
        }
        function CloseGridLookup2() {
            lkNhanVien.ConfirmCurrentSelection();
            lkNhanVien.HideDropDown();
            lkNhanVien.Focus();
        }

    </script>
    <div class="card card-block">
        <div class="form-group row">
            <div class="col-sm-10">
                <asp:UpdatePanel ID="udButton" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnThem" runat="server" Text="Thêm" CssClass="btn btn-primary" OnClick="btnThem_Click" />
                        <asp:Button ID="btnChiTiet" runat="server" Text="Chi tiết" CssClass="btn btn-primary" OnClick="btnChiTiet_Click" />
                        <input type="submit" class="btn btn-primary" value="Xóa" onclick="confirmDel()" />
                        <asp:Button ID="btnXoa" runat="server" CssClass="invisible" OnClick="btnXoa_Click" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>

        </div>
        <div class="form-group table-responsive">
            <dx:ASPxGridView ID="grvList" runat="server" ClientInstanceName="grvList" KeyFieldName="congvandi_id" Width="100%">
                <Columns>
                    <dx:GridViewCommandColumn ShowSelectCheckbox="True" SelectAllCheckboxMode="Page" VisibleIndex="0" Width="2%">
                    </dx:GridViewCommandColumn>
                    <dx:GridViewDataColumn Caption="Số ký hiệu, văn bản" FieldName="congvandi_name" HeaderStyle-HorizontalAlign="Center" Width="40%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Ngày, tháng văn bản" FieldName="congvandi_createdate" HeaderStyle-HorizontalAlign="Center" Width="15%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Tên loại và trích yếu nội dung văn bản" FieldName="congvandi_name" HeaderStyle-HorizontalAlign="Center" Width="40%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Người ký" FieldName="congvandi_name" HeaderStyle-HorizontalAlign="Center" Width="40%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Nơi nhận văn bản" FieldName="congvandi_name" HeaderStyle-HorizontalAlign="Center" Width="40%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Tên công văn" FieldName="congvandi_name" HeaderStyle-HorizontalAlign="Center" Width="40%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Người nhận văn bản" FieldName="congvandi_name" HeaderStyle-HorizontalAlign="Center" Width="40%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Số lượng bản" FieldName="congvandi_name" HeaderStyle-HorizontalAlign="Center" Width="40%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Ghi chú" FieldName="congvandi_name" HeaderStyle-HorizontalAlign="Center" Width="40%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn FieldName="congvandi_file" Width="120px" Caption="File" HeaderStyle-HorizontalAlign="Center" CellStyle-VerticalAlign="Middle">
                        <DataItemTemplate>
                            <img src="images/icon_PDF.png" width="100" />
                        </DataItemTemplate>
                    </dx:GridViewDataColumn>

                </Columns>
                <ClientSideEvents RowDblClick="btnChiTiet" />
                <SettingsSearchPanel Visible="true" />
                <SettingsBehavior AllowFocusedRow="true" />
                <SettingsText EmptyDataRow="Không có dữ liệu" SearchPanelEditorNullText="Gỏ từ cần tìm kiếm và enter..." />
                <SettingsLoadingPanel Text="Đang tải..." />
                <SettingsPager PageSize="10" Summary-Text="Trang {0} / {1} ({2} trang)"></SettingsPager>
            </dx:ASPxGridView>
        </div>
    </div>
    <dx:ASPxPopupControl ID="popupControl" runat="server" Width="900px" Height="600px" CloseAction="CloseButton" ShowCollapseButton="True" ShowMaximizeButton="True" ScrollBars="Auto" CloseOnEscape="true" Modal="True"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" ClientInstanceName="popupControl" ShowFooter="true"
        HeaderText="Công văn đi" AllowDragging="True" PopupAnimationType="Fade" EnableViewState="False" AutoUpdatePosition="true" ClientSideEvents-CloseUp="function(s,e){grvList.Refresh();}">
        <ContentCollection>
            <dx:PopupControlContentControl runat="server">
                <asp:UpdatePanel ID="udPopup" runat="server">
                    <ContentTemplate>
                        <div class="popup-main">
                            <div class="div_content col-12">
                                <div class="col-12">
                                    <div class="col-12 form-group">
                                        <div class="col-6">
                                            <label class="form-control-label">Số ký hiệu, văn bản:</label>
                                            <div class="">
                                                <asp:TextBox ID="txtSoKyHieu" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="90%"> </asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <label class="form-control-label">Ngày, tháng văn bản:</label>
                                            <div class="">
                                                <input id="dteNgayThangGui" type="date" runat="server" style="width: 90%" class="form-control" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12 form-group">
                                        <div class="col-6">
                                            <label class="form-control-label">Người ký:</label>
                                            <div class="">
                                                <asp:TextBox ID="txtNguoiKy" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="90%"> </asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <label class="form-control-label">Số lượng bản:</label>
                                            <div class="">
                                                <asp:TextBox ID="txtSoLuong" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="90%"> </asp:TextBox>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12 form-group">
                                        <div class="col-6">
                                            <label class="form-control-label">Nơi nhận văn bản:</label>
                                            <div class="">
                                                <%--<dx:ASPxComboBox ID="ddlBoPhan" NullText="Chọn bộ phận" runat="server" ValueType="System.Int32" TextField="bophan_name" ValueField="bophan_id" ClientInstanceName="ddlUser" AutoPostBack="true" CssClass="" Width="97%"></dx:ASPxComboBox>--%>
                                                <dx:ASPxGridLookup ID="lkBoPhan" runat="server" SelectionMode="Multiple" ClientInstanceName="lkBoPhan"
                                                    KeyFieldName="bophan_id" Width="250px" TextFormatString="{0}" MultiTextSeparator=", " Caption="Bộ phận" AutoPostBack="true">
                                                    <Columns>
                                                        <dx:GridViewCommandColumn ShowSelectCheckbox="True" />
                                                        <dx:GridViewDataColumn FieldName="bophan_id" Settings-AllowAutoFilter="false"  />
                                                        <dx:GridViewDataColumn FieldName="bophan_name" Caption="Bộ phận" />
                                                    </Columns>
                                                    <GridViewProperties>
                                                        <Templates>
                                                            <StatusBar>
                                                                <table class="OptionsTable" style="float: right">
                                                                    <tr>
                                                                        <td>
                                                                            <dx:ASPxButton ID="Close" runat="server" AutoPostBack="false" Text="Close" ClientSideEvents-Click="CloseGridLookup" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </StatusBar>
                                                        </Templates>
                                                        <Settings ShowFilterRow="True" ShowStatusBar="Visible" />
                                                        <SettingsPager PageSize="5" EnableAdaptivity="true" />
                                                    </GridViewProperties>
                                                </dx:ASPxGridLookup>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <label class="form-control-label">Người nhận văn bản:</label>
                                            <div class="">
                                                <%--<dx:ASPxComboBox ID="ddlUser" NullText="Chọn nhân viên" runat="server" ValueType="System.Int32" TextField="username_fullname" ValueField="username_id" ClientInstanceName="ddlUser" AutoPostBack="true" CssClass="" Width="97%"></dx:ASPxComboBox>--%>
                                                <dx:ASPxGridLookup ID="lkNhanVien" runat="server" SelectionMode="Multiple" ClientInstanceName="lkNhanVien"
                                                    KeyFieldName="username_id" Width="250px" TextFormatString="{0}" MultiTextSeparator=", " Caption="Nhân viên">
                                                    <Columns>
                                                        <dx:GridViewCommandColumn ShowSelectCheckbox="True" />
                                                        <dx:GridViewDataColumn FieldName="username_id" Settings-AllowAutoFilter="false" />
                                                        <dx:GridViewDataColumn FieldName="username_fullname" Caption="Nhân viên" />
                                                    </Columns>
                                                    <GridViewProperties>
                                                        <Templates>
                                                            <StatusBar>
                                                                <table class="OptionsTable" style="float: right">
                                                                    <tr>
                                                                        <td>
                                                                            <dx:ASPxButton ID="Close" runat="server" AutoPostBack="false" Text="Close" ClientSideEvents-Click="CloseGridLookup2" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </StatusBar>
                                                        </Templates>
                                                        <Settings ShowFilterRow="True" ShowStatusBar="Visible" />
                                                        <SettingsPager PageSize="5" EnableAdaptivity="true" />
                                                    </GridViewProperties>
                                                </dx:ASPxGridLookup>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-12 form-group">
                                        <label class="col-2 form-control-label">Tên loại và trích yếu nội dung văn bản:</label>
                                        <div class="col-10">
                                            <asp:TextBox ID="txtTieuDe" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="94%"> </asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="col-12 form-group">
                                        <label class="col-2 form-control-label">Ghi chú:</label>
                                        <div class="col-10">
                                            <asp:TextBox ID="txtGhiChu" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="94%"> </asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-12 form-group">
                                        <%--<div class="text-color1"><i>Kích thước ảnh tiêu chuẩn 790x525</i></div>--%>
                                        <div class="colum-5 form-group">
                                            <label class="form-control-label">Chọn file:</label>
                                            <div id="up1" class="">
                                                <asp:FileUpload CssClass="hidden-xs-up" ID="FileUpload1" runat="server" onchange="showPreview1(this)" />
                                                <button type="button" class="btn-chang" onclick="clickavatar1()">
                                                    <img id="imgPreview1" src="/admin_images/up-img.png" style="max-width: 100%; height: 150px" />
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <FooterContentTemplate>
            <div class="mar_but button">
                <asp:Button ID="btnLuu" runat="server" ClientIDMode="Static" Text="Lưu" CssClass="btn btn-primary" OnClientClick="return checkNULL()" OnClick="btnLuu_Click" />
            </div>
        </FooterContentTemplate>
        <ContentStyle>
            <Paddings PaddingBottom="0px" />
        </ContentStyle>
    </dx:ASPxPopupControl>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
    <script type="text/javascript">
        function clickavatar1() {
            $("#up1 input[type=file]").click();
        }
    </script>
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

