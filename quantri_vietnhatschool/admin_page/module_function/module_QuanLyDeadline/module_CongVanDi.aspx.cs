﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_CongVanDi : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    string image;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["_id"] = 0;
        }
        loadData();

    }
    private void loadData()
    {
        var getData = from n in db.tbQuanLyCongVanDis
                      join u in db.admin_Users on n.username_id equals u.username_id
                      orderby n.congvandi_id descending
                      select new
                      {
                          n.congvandi_id,
                          n.congvandi_name,
                          n.congvandi_file,
                          n.congvandi_createdate
                      };
        grvList.DataSource = getData;
        grvList.DataBind();
        //ddlBoPhan.DataSource = from bp in db.tbBoPhans orderby bp.bophan_position select bp;
        //ddlBoPhan.DataBind();
        lkBoPhan.DataSource = from bp in db.tbBoPhans orderby bp.bophan_position select bp;
        lkBoPhan.DataBind();

        //ddlUser.DataSource = from tb in db.admin_Users
        //                            select tb;
        //ddlUser.DataBind();
        //if (lkBoPhan.Value != "")
        //{
        var listNV = from nv in db.admin_Users where nv.bophan_id > 2 select nv;
        lkNhanVien.DataSource = listNV;
        lkNhanVien.DataBind();
        //if (lkBoPhan.Text!="")
        //{
        //    string listIDBP = lkBoPhan.Text;
        //    string[] checkedBP = listIDBP.Split(',');
        //    foreach (var item in checkedBP)
        //    {

        //    }
        //}
        //}
        //get ds nhân viên

    }
    private void setNULL()
    {
        txtTieuDe.Text = "";
        txtGhiChu.Text = "";
        txtNguoiKy.Text = "";
        txtSoKyHieu.Text = "";
        txtSoLuong.Text = "";
        lkBoPhan.Text = "";
        lkNhanVien.Text = "";
    }
    protected void btnThem_Click(object sender, EventArgs e)
    {
        Session["_id"] = 0;
        setNULL();
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupControl.Show();", true);
    }
    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "congvandi_id" }));
        Session["_id"] = _id;
        var getData = (from n in db.tbQuanLyCongVanDis
                       join u in db.admin_Users on n.username_id equals u.username_id
                       where n.congvandi_id == _id
                       select new
                       {
                           n.congvandi_id,
                           n.congvandi_name,
                           n.congvandi_file,
                           n.congvandi_trachnhiem,
                           n.congvandi_ghichu,
                           n.congvandi_sohieu,
                           n.congvandi_soluong,
                           n.congvandi_nguoiky,
                           n.congvandi_ngayvanban,
                           u.username_fullname
                       }).Single();
        txtSoKyHieu.Text = getData.congvandi_sohieu;
        dteNgayThangGui.Value = getData.congvandi_ngayvanban.Value.ToString("yyyy-MM-dd").Replace(' ', 'T');
        txtNguoiKy.Text = getData.congvandi_nguoiky;
        txtSoLuong.Text = getData.congvandi_soluong;
        txtTieuDe.Text = getData.congvandi_name;
        txtGhiChu.Text = getData.congvandi_ghichu;
        var getcongvanden = from cv in db.tbQuanLyCongVanDens
                            join u in db.admin_Users on cv.username_id equals u.username_id
                            where cv.congvandi_id == _id
                            select new
                            {
                                cv.congvandi_id,
                                cv.username_id,
                                u.bophan_id,
                            };
        string nv, bp;
        
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupControl.Show();showImg1_1('" + "images/icon_PDF.png" + "'); ", true);
        // loadData();
    }
    protected void btnXoa_Click(object sender, EventArgs e)
    {
        cls_CongVanDi cls;
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "congvandi_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                cls = new cls_CongVanDi();
                tbQuanLyCongVanDi checkImage = (from i in db.tbQuanLyCongVanDis where i.congvandi_id == Convert.ToInt32(item) select i).SingleOrDefault();
                if (cls.Linq_Xoa(Convert.ToInt32(item)))
                {
                    var getcongvanden = from cvd in db.tbQuanLyCongVanDens
                                        where cvd.congvandi_id == Convert.ToInt32(item)
                                        select cvd;
                    foreach (var cv in getcongvanden)
                    {
                        db.tbQuanLyCongVanDens.DeleteOnSubmit(cv);
                        db.SubmitChanges();
                    }
                    alert.alert_Success(Page, "Xóa thành công", "");
                }
                else
                    alert.alert_Error(Page, "Xóa thất bại", "");
            }
        }
        else
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
        popupControl.ShowOnPageLoad = false;
        loadData();
    }
    public bool checknull()
    {
        if (lkNhanVien.Text != "")
            return true;
        else return false;
    }
    protected void btnLuu_Click(object sender, EventArgs e)
    {
        string listIDBP = lkBoPhan.Text;
        string listIDNV = lkNhanVien.Text;
        string[] checkedBP = listIDBP.Split(',');
        string[] checkedNV = listIDNV.Split(',');
        if (Page.IsValid && FileUpload1.HasFile)
        {
            String folderUser = Server.MapPath("~/File_CongVan/");
            if (!Directory.Exists(folderUser))
            {
                Directory.CreateDirectory(folderUser);
            }
            //string filename;
            string ulr = "/File_CongVan/";
            HttpFileCollection hfc = Request.Files;
            string filename = Path.GetRandomFileName() + Path.GetExtension(FileUpload1.FileName);
            string fileName_save = Path.Combine(Server.MapPath("~/File_CongVan"), filename);
            FileUpload1.SaveAs(fileName_save);
            image = ulr + filename;
        }

        cls_CongVanDi cls = new cls_CongVanDi();
        if (checknull() == false)
            alert.alert_Warning(Page, "Hãy nhập đầy đủ thông tin!", "");
        else
        {
            var getuser = from u in db.admin_Users
                          where u.username_username == Request.Cookies["UserName"].Value
                          select u;
            if (Session["_id"].ToString() == "0")
            {
                //List<object> selectedKey = grvl_GiaoVien.GridView.GetSelectedFieldValues(new string[] { "username_id" });
                //lưu ở bảng công văn đi
                tbQuanLyCongVanDi insert = new tbQuanLyCongVanDi();
                insert.congvandi_name = txtTieuDe.Text;
                if (image == null)
                    insert.congvandi_file = "images/icon_PDF.png";
                else
                    insert.congvandi_file = image;
                insert.congvandi_createdate = DateTime.Now;
                insert.username_id = getuser.FirstOrDefault().username_id;
                insert.congvandi_ghichu = txtGhiChu.Text;
                insert.congvandi_soluong = txtSoLuong.Text;
                insert.congvandi_nguoiky = txtNguoiKy.Text;
                insert.congvandi_sohieu = txtSoKyHieu.Text;
                insert.congvandi_ngayvanban = Convert.ToDateTime(dteNgayThangGui.Value);
                db.tbQuanLyCongVanDis.InsertOnSubmit(insert);
                db.SubmitChanges();
                //lưu ở bảng công văn đến
                foreach (var item in checkedNV)
                {
                    //lấy ra tất cả tk thuộc bộ phận đang duyệt và kiểm tra
                    // nếu id tk có trong list tk được chọn thì lưu vào
                    // ngược lại thì bỏ qua
                    //var checkTK = from u in db.admin_Users
                    //              where u.bophan_id == Convert.ToInt32(item)
                    //              select u;
                    // intersection = a.(element => secondArray.includes(element));
                    //int maxid = db.tbQuanLyCongVanDis.Max(x => x.congvandi_id);
                    tbQuanLyCongVanDen cv = new tbQuanLyCongVanDen();
                    cv.congvandi_id = insert.congvandi_id;
                    cv.username_id = Convert.ToInt32(item);
                    db.tbQuanLyCongVanDens.InsertOnSubmit(cv);
                    try
                    {
                        db.SubmitChanges();
                        alert.alert_Success(Page, "Lưu thành công!", "");
                    }
                    catch { }
                }
            }
            else
            {
                //update bảng công văn đi
                tbQuanLyCongVanDi update = db.tbQuanLyCongVanDis.Where(x => x.congvandi_id == Convert.ToInt32(Session["_id"].ToString())).FirstOrDefault();
                update.congvandi_name = txtTieuDe.Text;
                if (image == null)
                {
                }
                else
                    update.congvandi_file = image;
                update.congvandi_createdate = DateTime.Now;
                update.username_id = getuser.FirstOrDefault().username_id;
                update.congvandi_ghichu = txtGhiChu.Text;
                update.congvandi_soluong = txtSoLuong.Text;
                update.congvandi_nguoiky = txtNguoiKy.Text;
                update.congvandi_sohieu = txtSoKyHieu.Text;
                update.congvandi_ngayvanban = Convert.ToDateTime(dteNgayThangGui.Value);
                db.SubmitChanges();
                //xóa hết data cũ ở table công văn đến và inser lại mới
                var checkCVD = from cv in db.tbQuanLyCongVanDens
                               where cv.congvandi_id == Convert.ToInt32(Session["_id"].ToString())
                               select cv;
                db.tbQuanLyCongVanDens.DeleteAllOnSubmit(checkCVD);
                db.SubmitChanges();
                foreach (var item in checkedNV)
                {
                    //lấy ra tất cả tk thuộc bộ phận đang duyệt và kiểm tra
                    // nếu id tk có trong list tk được chọn thì lưu vào
                    // ngược lại thì bỏ qua
                    //var checkTK = from u in db.admin_Users
                    //              where u.bophan_id == Convert.ToInt32(item)
                    //              select u;
                    // intersection = a.(element => secondArray.includes(element));
                    //int maxid = db.tbQuanLyCongVanDis.Max(x => x.congvandi_id);
                    tbQuanLyCongVanDen cv = new tbQuanLyCongVanDen();
                    cv.congvandi_id = Convert.ToInt32(Session["_id"].ToString());
                    cv.username_id = Convert.ToInt32(item);
                    db.tbQuanLyCongVanDens.InsertOnSubmit(cv);
                    try
                    {
                        db.SubmitChanges();
                        alert.alert_Success(Page, "Lưu thành công!", "");
                    }
                    catch { }
                }
                alert.alert_Success(Page, "Lưu thành công!", "");
            }
            popupControl.ShowOnPageLoad = false;
            loadData();
        }
    }

}