﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_CongVanDi_SaoLuu : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    string image;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["_id"] = 0;
        }
        loadData();

    }
    private void loadData()
    {
        var getData = from n in db.tbQuanLyCongVanDis
                      join u in db.admin_Users on n.username_id equals u.username_id
                      orderby n.congvandi_id descending
                      select new
                      {
                          n.congvandi_id,
                          n.congvandi_name,
                          n.congvandi_file,
                          n.congvandi_createdate
                      };
        grvList.DataSource = getData;
        grvList.DataBind();
        //ddlUser.DataSource = from tb in db.admin_Users
        //                            select tb;
        //ddlUser.DataBind();
        var listNV = from nv in db.admin_Users where nv.bophan_id == 1 select nv;
        ddlUser.DataSource = listNV;
        ddlUser.DataBind();
        //get ds nhân viên
        var user = from nv in db.admin_Users where nv.bophan_id == 1 select nv;
        grvl_GiaoVien.DataSource = user;
        grvl_GiaoVien.DataBind();
    }
    private void setNULL()
    {
        txtTieuDe.Text = "";
        //imgPreview.Src = "";
    }
    protected void btnThem_Click(object sender, EventArgs e)
    {
        Session["_id"] = 0;
        setNULL();
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupControl.Show();", true);
    }
    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "congvandi_id" }));
        Session["_id"] = _id;
        var getData = (from n in db.tbQuanLyCongVanDis
                       join u in db.admin_Users on n.username_id equals u.username_id
                       where n.congvandi_id == _id
                       select new
                       {
                           n.congvandi_id,
                           n.congvandi_name,
                           n.congvandi_file,
                           n.congvandi_trachnhiem,
                           n.congvandi_content,
                           u.username_fullname
                       }).Single();
        txtTieuDe.Text = getData.congvandi_name;
        ddlUser.Text = (from u in db.admin_Users
                        where u.username_id == Convert.ToInt32(getData.congvandi_trachnhiem)
                        select u).SingleOrDefault().username_fullname;
        //var getbp = (from u in db.admin_Users
        //                      where getData.congvandi_content.Contains(u.username_id+"")
        //                      select u).SingleOrDefault().username_id;
        //grvl_GiaoVien.Text = getbp.ToString();
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupControl.Show();showImg1_1('" + "images/icon_PDF.png" + "'); ", true);
        // loadData();
    }
    protected void btnXoa_Click(object sender, EventArgs e)
    {
        cls_CongVanDi cls;
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "congvandi_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                cls = new cls_CongVanDi();
                tbQuanLyCongVanDi checkImage = (from i in db.tbQuanLyCongVanDis where i.congvandi_id == Convert.ToInt32(item) select i).SingleOrDefault();
                if (cls.Linq_Xoa(Convert.ToInt32(item)))
                {
                    var getcongvanden = from cvd in db.tbQuanLyCongVanDens
                                        where cvd.congvandi_id == Convert.ToInt32(item)
                                        select cvd;
                    foreach (var cv in getcongvanden)
                    {
                        db.tbQuanLyCongVanDens.DeleteOnSubmit(cv);
                        db.SubmitChanges();
                    }
                    alert.alert_Success(Page, "Xóa thành công", "");
                }
                else
                    alert.alert_Error(Page, "Xóa thất bại", "");
            }
        }
        else
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
        popupControl.ShowOnPageLoad = false;
        loadData();
    }
    public bool checknull()
    {
        if (txtTieuDe.Text != "")
            return true;
        else return false;
    }
    protected void btnLuu_Click(object sender, EventArgs e)
    {

        if (Page.IsValid && FileUpload1.HasFile)
        {
            String folderUser = Server.MapPath("~/File_CongVan/");
            if (!Directory.Exists(folderUser))
            {
                Directory.CreateDirectory(folderUser);
            }
            //string filename;
            string ulr = "/File_CongVan/";
            HttpFileCollection hfc = Request.Files;
            string filename = Path.GetRandomFileName() + Path.GetExtension(FileUpload1.FileName);
            string fileName_save = Path.Combine(Server.MapPath("~/File_CongVan"), filename);
            FileUpload1.SaveAs(fileName_save);
            image = ulr + filename;
        }

        cls_CongVanDi cls = new cls_CongVanDi();
        if (checknull() == false)
            alert.alert_Warning(Page, "Hãy nhập đầy đủ thông tin!", "");
        else
        {
            var getuser = from u in db.admin_Users
                          where u.username_username == Request.Cookies["UserName"].Value
                          select u;
            if (Session["_id"].ToString() == "0")
            {
                List<object> selectedKey = grvl_GiaoVien.GridView.GetSelectedFieldValues(new string[] { "username_id" });
                //lưu ở bảng công văn đi
                tbQuanLyCongVanDi insert = new tbQuanLyCongVanDi();
                insert.congvandi_name = txtTieuDe.Text;
                if (image == null)
                    insert.congvandi_file = "images/icon_PDF.png";
                else
                    insert.congvandi_file = image;
                insert.congvandi_content = grvl_GiaoVien.Text;
                insert.congvandi_createdate = DateTime.Now;
                insert.congvandi_trachnhiem = ddlUser.SelectedItem.Value + "";
                insert.username_id = getuser.FirstOrDefault().username_id;
                db.tbQuanLyCongVanDis.InsertOnSubmit(insert);
                db.SubmitChanges();
                //lưu ở bảng công văn đến
                foreach (var item in selectedKey)
                {
                    int maxid = db.tbQuanLyCongVanDis.Max(x => x.congvandi_id);
                    tbQuanLyCongVanDen cv = new tbQuanLyCongVanDen();
                    cv.congvandi_id = maxid;
                    cv.username_id = Convert.ToInt32(item);
                    db.tbQuanLyCongVanDens.InsertOnSubmit(cv);
                    try
                    {
                        db.SubmitChanges();
                        alert.alert_Success(Page, "Lưu thành công!", "");
                    }
                    catch { }
                }
            }
            else
            {
                //update bảng công văn đi
                tbQuanLyCongVanDi update = db.tbQuanLyCongVanDis.Where(x => x.congvandi_id == Convert.ToInt32(Session["_id"].ToString())).FirstOrDefault();
                update.congvandi_name = txtTieuDe.Text;
                if (image == null)
                {
                }
                else
                    update.congvandi_file = image;
                update.congvandi_content = grvl_GiaoVien.Text;
                update.congvandi_createdate = DateTime.Now;
                update.congvandi_trachnhiem = ddlUser.SelectedItem.Value + "";
                update.username_id = getuser.FirstOrDefault().username_id;
                db.SubmitChanges();
                //cập nhật bảng công văn đến
                List<object> selectedKey = grvl_GiaoVien.GridView.GetSelectedFieldValues(new string[] { "username_id" });
                foreach (var item in selectedKey)
                {
                    var getcv = (from cv in db.tbQuanLyCongVanDens
                                where cv.congvandi_id == Convert.ToInt32(Session["_id"].ToString())
                                && cv.username_id == Convert.ToInt32(item)
                                select cv).FirstOrDefault();
                    if (getcv == null)
                    {
                        tbQuanLyCongVanDen cv = new tbQuanLyCongVanDen();
                        cv.congvandi_id = Convert.ToInt32(Session["_id"].ToString());
                        cv.username_id = Convert.ToInt32(item);
                        db.tbQuanLyCongVanDens.InsertOnSubmit(cv);
                        try
                        {
                            db.SubmitChanges();
                        }
                        catch { }
                    }
                    else
                    {
                    }
                }
                alert.alert_Success(Page,"Lưu thành công!","");
            }
            popupControl.ShowOnPageLoad = false;
            loadData();
        }
    }

}