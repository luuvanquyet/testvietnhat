﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_GiaoViec : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    string image;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["_id"] = 0;
        }
        loadData();

    }
    private void loadData()
    {
        var getData = from n in db.tbQuanLyCongViecs
                      join u in db.admin_Users on n.username_id equals u.username_id
                      //orderby n.quanlycongviec_id descending
                      select new
                      {
                          n.quanlycongviec_id,
                          n.quanlycongviec_name,
                          u.username_fullname,
                          n.quanlycongviec_createdate,
                          n.quanlycongviec_enddate,
                          n.quanlycongviec_image
                      };
        grvList.DataSource = getData;
        grvList.DataBind();
        var listNV = from nv in db.admin_Users where nv.bophan_id == 1 select nv;
        ddlUser.DataSource = listNV;
        ddlUser.DataBind();

    }
    private void setNULL()
    {
        txtName.Value = "";
        //imgPreview.Src = "";
    }
    protected void btnThem_Click(object sender, EventArgs e)
    {
        Session["_id"] = 0;
        setNULL();
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupControl.Show();", true);
    }
    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "quanlycongviec_id" }));
        Session["_id"] = _id;
        var getData = (from n in db.tbQuanLyCongViecs
                       join u in db.admin_Users on n.username_id equals u.username_id
                       where n.quanlycongviec_id == _id
                       select new
                       {
                           n.quanlycongviec_id,
                           n.quanlycongviec_name,
                           n.quanlycongviec_image,
                           u.username_fullname
                       }).Single();
        txtName.Value = getData.quanlycongviec_name;
        ddlUser.Items.FindByText(getData.username_fullname).Selected =true;
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupControl.Show();", true);
        // loadData();
    }
    protected void btnXoa_Click(object sender, EventArgs e)
    {
        cls_News cls;
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "quanlycongviec_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                cls = new cls_News();
                tbWebsite_New checkImage = (from i in db.tbWebsite_News where i.news_id == Convert.ToInt32(item) select i).SingleOrDefault();
                if (cls.Linq_Xoa(Convert.ToInt32(item)))
                {
                    alert.alert_Success(Page, "Xóa thành công", "");
                }
                else
                    alert.alert_Error(Page, "Xóa thất bại", "");
            }
        }
        else
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
        popupControl.ShowOnPageLoad = false;
        loadData();
    }
    public bool checknull()
    {
        if (txtName.Value != "")
            return true;
        else return false;
    }
    protected void btnLuu_Click(object sender, EventArgs e)
    {
        //cls_News cls = new cls_News();
        //if (checknull() == false)
        //    alert.alert_Warning(Page, "Hãy nhập đầy đủ thông tin!", "");
        //else
        //{
        if (Session["_id"].ToString() == "0")
        {
            // lưu vào bảng quản lý công việc
            tbQuanLyCongViec insert = new tbQuanLyCongViec();
            insert.quanlycongviec_name = txtName.Value;
            insert.username_id = Convert.ToInt32(ddlUser.Value);
            insert.quanlycongviec_createdate = DateTime.Now;
            db.tbQuanLyCongViecs.InsertOnSubmit(insert);
            try
            {
                db.SubmitChanges();
                alert.alert_Success(Page, "Lưu thành công!", "");
            }
            catch { }
        }
        else
        {
            //cập nhật
            tbQuanLyCongViec update = db.tbQuanLyCongViecs.Where(x => x.quanlycongviec_id == Convert.ToInt32(Session["_id"].ToString())).FirstOrDefault();
            update.quanlycongviec_name = txtName.Value;
            update.username_id = Convert.ToInt32(ddlUser.Value);
            update.quanlycongviec_createdate = DateTime.Now;
            db.SubmitChanges();
            alert.alert_Success(Page, "Lưu thành công!", "");

        }
        popupControl.ShowOnPageLoad = false;
        loadData();
        // }
    }

    protected void btnTrangChu_Click(object sender, EventArgs e)
    {
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "news_id" });
        if (selectedKey.Count > 0)
        {

            foreach (var item in selectedKey)
            {

                var getsp = (from sp in db.tbWebsite_News where sp.news_id == Convert.ToInt32(item) select sp).SingleOrDefault();
                if (getsp.news_trangchu == null || getsp.news_trangchu == false)
                {
                    getsp.news_trangchu = true;
                    db.SubmitChanges();
                    alert.alert_Success(Page, "Đã Xử Lí Xong!", "");
                }
                else
                {
                    if (getsp.news_trangchu == true)
                    {
                        getsp.news_trangchu = false;
                        db.SubmitChanges();
                        alert.alert_Success(Page, "Đã Xử Lí Xong!", "");
                    }
                    else
                    {
                        alert.alert_Error(Page, "Xử Lí thất bại!", "");
                    }
                }
            }
        }
    }

    protected void btnTrungHocPT_Click(object sender, EventArgs e)
    {
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "news_id" });
        if (selectedKey.Count > 0)
        {

            foreach (var item in selectedKey)
            {

                var getsp = (from sp in db.tbWebsite_News where sp.news_id == Convert.ToInt32(item) select sp).SingleOrDefault();
                if (getsp.news_trunghocphothong == null || getsp.news_trunghocphothong == false)
                {
                    getsp.news_trunghocphothong = true;
                    db.SubmitChanges();
                    alert.alert_Success(Page, "Đã Xử Lí Xong!", "");
                }
                else
                {
                    if (getsp.news_trunghocphothong == true)
                    {
                        getsp.news_trunghocphothong = false;
                        db.SubmitChanges();
                        alert.alert_Success(Page, "Đã Xử Lí Xong!", "");
                    }
                    else
                    {
                        alert.alert_Error(Page, "Xử Lí thất bại!", "");
                    }
                }
            }
        }
    }

    protected void btnTrungHocCS_Click(object sender, EventArgs e)
    {
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "news_id" });
        if (selectedKey.Count > 0)
        {

            foreach (var item in selectedKey)
            {

                var getsp = (from sp in db.tbWebsite_News where sp.news_id == Convert.ToInt32(item) select sp).SingleOrDefault();
                if (getsp.news_trunghoccoso == null || getsp.news_trunghoccoso == false)
                {
                    getsp.news_trunghoccoso = true;
                    db.SubmitChanges();
                    alert.alert_Success(Page, "Đã Xử Lí Xong!", "");
                }
                else
                {
                    if (getsp.news_trunghoccoso == true)
                    {
                        getsp.news_trunghoccoso = false;
                        db.SubmitChanges();
                        alert.alert_Success(Page, "Đã Xử Lí Xong!", "");
                    }
                    else
                    {
                        alert.alert_Error(Page, "Xử Lí thất bại!", "");
                    }
                }
            }
        }
    }

    protected void btnTieuHoc_Click(object sender, EventArgs e)
    {
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "news_id" });
        if (selectedKey.Count > 0)
        {

            foreach (var item in selectedKey)
            {

                var getsp = (from sp in db.tbWebsite_News where sp.news_id == Convert.ToInt32(item) select sp).SingleOrDefault();
                if (getsp.news_tieuhoc == null || getsp.news_tieuhoc == false)
                {
                    getsp.news_tieuhoc = true;
                    db.SubmitChanges();
                    alert.alert_Success(Page, "Đã Xử Lí Xong!", "");
                }
                else
                {
                    if (getsp.news_tieuhoc == true)
                    {
                        getsp.news_tieuhoc = false;
                        db.SubmitChanges();
                        alert.alert_Success(Page, "Đã Xử Lí Xong!", "");
                    }
                    else
                    {
                        alert.alert_Error(Page, "Xử Lí thất bại!", "");
                    }
                }
            }
        }
    }

    protected void btnMamNon_Click(object sender, EventArgs e)
    {
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "news_id" });
        if (selectedKey.Count > 0)
        {

            foreach (var item in selectedKey)
            {

                var getsp = (from sp in db.tbWebsite_News where sp.news_id == Convert.ToInt32(item) select sp).SingleOrDefault();
                if (getsp.news_mamnon == null || getsp.news_mamnon == false)
                {
                    getsp.news_mamnon = true;
                    db.SubmitChanges();
                    alert.alert_Success(Page, "Đã Xử Lí Xong!", "");
                }
                else
                {
                    if (getsp.news_mamnon == true)
                    {
                        getsp.news_mamnon = false;
                        db.SubmitChanges();
                        alert.alert_Success(Page, "Đã Xử Lí Xong!", "");
                    }
                    else
                    {
                        alert.alert_Error(Page, "Xử Lí thất bại!", "");
                    }
                }
            }
        }
    }

}