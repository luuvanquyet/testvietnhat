﻿<%@ Page Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_NoiDungChiTiet.aspx.cs" Inherits="admin_page_module_function_module_WebSite_module_NoiDungChiTiet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <script>
        function btnChiTiet() {
            document.getElementById('<%=btnChiTiet.ClientID%>').click();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="card card-block">
        <div class="col-12">
            <div class="col-12">
                <div class="col-12 form-group">
                    <label class="col-2 form-control-label">Thông tin :</label>
                    <div class="col-10">
                        <asp:TextBox ID="txtTitle" Enabled="false" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="50%"> </asp:TextBox>
                    </div>
                </div>
                <div class="col-12 form-group">
                    <label class="col-2 form-control-label">Ngày hết hạn :</label>
                    <div class="col-10">
                        <%--   <asp:TextBox ID="" runat="server" CssClass="form-control boxed" Width="30%" ></asp:TextBox>--%>
                        <%-- <input type='text' id="dteEndDate" class="datepicker-here" data-position="right top" data-language='en' style="width:30%"/>--%>
                        <input id="dteEndDate" runat="server" type='text' class="datepicker-here" data-position="right top" data-language='en' data-date-format="dd/mm/yyyy" />
                    </div>
                </div>
                <div class="col-12 form-group">
                    <label class="col-2 form-control-label">Hoàn thành (%) :</label>
                    <div class="col-8">
                        <asp:TextBox ID="txtPercent" runat="server" CssClass="form-control" Width="25%"></asp:TextBox>
                    </div>
                </div>
                <div class="col-12 form-group">
                    <label class="col-2 form-control-label">Nội dung :</label>
                    <div class="col-10">
                        <%-- <asp:TextBox ID="txtNoiDung" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="50%"> </asp:TextBox>--%>
                        <textarea id="txtNoiDung" runat="server" class="rsize" cols="30" rows="3"></textarea>
                    </div>
                </div>
                 <div class="col-12 form-group">
                    <label class="col-2 form-control-label">Lý do quá hạn :</label>
                    <div class="col-10">
                        <%-- <asp:TextBox ID="txtNoiDung" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="50%"> </asp:TextBox>--%>
                        <textarea id="txtLyDoQuaHan" runat="server" class="rsize" cols="30" rows="3"></textarea>
                    </div>
                </div>
                <div class="col-12 form-group">
                    <div class="col-10">
                        <a href="../../admin-quan-ly-cong-viec-ca-nhan" class="btn btn-primary">Quay về</a>
                        <a href="#" id="btnSave" runat="server" onserverclick="btnSave_ServerClick" class="btn btn-primary">Lưu</a>
                        <a href="#" id="btnChiTiet" runat="server" onserverclick="btnChiTiet_ServerClick" class="btn btn-primary" style="display: none">Chi tiết</a>
                        <a href="#" id="btnCapNhat" runat="server" onserverclick="btnCapNhat_ServerClick" class="btn btn-primary">Cập nhật</a>
                        <a href="#" id="btnXoa" runat="server" onserverclick="btnXoa_ServerClick" class="btn btn-primary">Xóa</a>
                        <a href="#" id="btnDuyet" runat="server" onserverclick="btnDuyet_ServerClick" class="btn btn-primary">Duyệt</a>
                        <a href="#" id="btnHuyDuyet" runat="server" onserverclick="btnHuyDuyet_ServerClick" class="btn btn-primary">Hủy duyệt</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group table-responsive">
            <dx:ASPxGridView ID="grvList" runat="server" ClientInstanceName="grvList" KeyFieldName="quanlycongviec_chitiet_id" Width="100%">
                <Columns>
                    <dx:GridViewCommandColumn ShowSelectCheckbox="True" SelectAllCheckboxMode="Page" VisibleIndex="0" Width="5%">
                    </dx:GridViewCommandColumn>
                    <dx:GridViewDataColumn Caption="Nội dung" FieldName="quanlycongviec_chitiet_content" HeaderStyle-HorizontalAlign="Center" Width="50%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Ngày hết hạn" FieldName="quanlycongviec_chitiet_enddate" HeaderStyle-HorizontalAlign="Center" Width="30%" Settings-AllowEllipsisInText="True"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Hoàn thành (%)" FieldName="quanlycongviec_chitiet_percent" Width="20%" HeaderStyle-HorizontalAlign="Center" CellStyle-VerticalAlign="Middle"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Duyệt" FieldName="quanlycongviec_chitiet_status" Width="20%" HeaderStyle-HorizontalAlign="Center" CellStyle-VerticalAlign="Middle"></dx:GridViewDataColumn>
                </Columns>
                <ClientSideEvents RowDblClick="btnChiTiet" />
                <%--<SettingsSearchPanel Visible="true" />--%>
                <SettingsBehavior AllowFocusedRow="true" />
                <SettingsText EmptyDataRow="Không có dữ liệu" SearchPanelEditorNullText="Gỏ từ cần tìm kiếm và enter..." />
                <SettingsLoadingPanel Text="Đang tải..." />
                <SettingsPager PageSize="10" Summary-Text="Trang {0} / {1} ({2} trang)"></SettingsPager>
            </dx:ASPxGridView>
        </div>
    </div>
    <style>
        .datepicker-here {
            width: 30%;
            z-index: 9999 !important;
        }

        .rsize {
            width: 30%;
            resize: none;
        }

        .form-control {
            border: 1px solid #a9a1a1;
        }
    </style>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

