﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_WebSite_module_NoiDungChiTiet : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    DataTable dtSchool;
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {
        var checkgroup = (from g in db.admin_Users where g.username_username == Request.Cookies["UserName"].Value select g).SingleOrDefault();
        if (checkgroup.groupuser_id == 1 || checkgroup.groupuser_id == 2)
        {
            btnDuyet.Visible = true;
            btnHuyDuyet.Visible = true;
            btnSave.Visible = false;
            btnXoa.Visible = false;
        }
        else
        {
            var checkDuyet = (from d in db.tbQuanLyCongViecs
                              where d.quanlycongviec_id == Convert.ToInt32(RouteData.Values["id"]) && d.quanlycongviec_duyet == true
                              select d).SingleOrDefault();
            if (checkDuyet != null)
            {
                btnSave.Visible = false;
                btnXoa.Visible = false;
            }
            btnDuyet.Visible = false;
            btnHuyDuyet.Visible = false;
        }
        if (!IsPostBack)
        {
           
            var check_id = (from tt in db.tbQuanLyCongViecs where tt.quanlycongviec_id == Convert.ToInt32(RouteData.Values["id"]) select tt).SingleOrDefault();
            if (check_id != null)
            {
                txtTitle.Text = check_id.quanlycongviec_name;
            }
            getList();
        }
    }
    protected void getList()
    {
        grvList.DataSource = (from tt in db.tbQuanLyCongViecs
                              join ct in db.tbQuanLyCongViec_ChiTiets on tt.quanlycongviec_id equals ct.quanlycongviec_id
                              where ct.quanlycongviec_id == Convert.ToInt32(RouteData.Values["id"])
                              select new
                              {
                                  tt.quanlycongviec_name,
                                  ct.quanlycongviec_chitiet_content,
                                  ct.quanlycongviec_chitiet_percent,
                                  ct.quanlycongviec_chitiet_enddate,
                                  ct.quanlycongviec_chitiet_id,
                                  quanlycongviec_chitiet_status = ct.quanlycongviec_chitiet_status == true ? "Đã duyệt" : "Chưa duyệt"
                              });
        grvList.DataBind();
    }
    protected void btnSave_ServerClick(object sender, EventArgs e)
    {
        tbQuanLyCongViec_ChiTiet insert = new tbQuanLyCongViec_ChiTiet();
        insert.quanlycongviec_chitiet_content = txtNoiDung.Value;
        insert.quanlycongviec_chitiet_enddate = Convert.ToDateTime(dteEndDate.Value);
        insert.quanlycongviec_chitiet_percent = txtPercent.Text;
        insert.quanlycongviec_id = Convert.ToInt32(RouteData.Values["id"]);
        db.tbQuanLyCongViec_ChiTiets.InsertOnSubmit(insert);
        try
        {
            db.SubmitChanges();
            txtNoiDung.Value = "";
            txtPercent.Text = "";
            getList();
            alert.alert_Success(Page, "Đã lưu", "");
        }
        catch (Exception ex)
        {
            alert.alert_Error(Page, "Có lỗi xảy ra, vui lòng liên hệ trực tiếp IT", "");
        }
    }

    protected void btnCapNhat_ServerClick(object sender, EventArgs e)
    {
        //images/yellow-co.jpg
        tbQuanLyCongViec_ChiTiet update = (from ct in db.tbQuanLyCongViec_ChiTiets where ct.quanlycongviec_chitiet_id == Convert.ToInt32(Session["_id"]) select ct).SingleOrDefault();
        update.quanlycongviec_chitiet_content = txtNoiDung.Value;
        update.quanlycongviec_chitiet_enddate = Convert.ToDateTime(dteEndDate.Value);
        update.quanlycongviec_chitiet_percent = txtPercent.Text;
        update.quanlycongviec_chitiet_lydo = txtLyDoQuaHan.Value;
        if (txtPercent.Text == "100")
        {
            update.quanlycongviec_chitiet_flag = true;
        }
        try
        {
            db.SubmitChanges();
            getList();
            alert.alert_Success(Page, "Đã cập nhật thành công", "");
        }
        catch (Exception ex)
        {
            alert.alert_Error(Page, "Có lỗi xảy ra, vui lòng liên hệ trực tiếp IT", "");
        }
    }
    protected void btnChiTiet_ServerClick(object sender, EventArgs e)
    {

        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "quanlycongviec_chitiet_id" }));
        Session["_id"] = _id;
        var getData = (from nc in db.tbQuanLyCongViec_ChiTiets
                       where nc.quanlycongviec_chitiet_id == _id
                       select new
                       {
                           nc.quanlycongviec_chitiet_id,
                           nc.quanlycongviec_chitiet_content,
                           nc.quanlycongviec_chitiet_percent,
                           nc.quanlycongviec_chitiet_enddate
                       }).Single();
        txtNoiDung.Value = getData.quanlycongviec_chitiet_content;
        dteEndDate.Value = Convert.ToDateTime(getData.quanlycongviec_chitiet_enddate).ToString("dd/MM/yyyy");
        txtPercent.Text = getData.quanlycongviec_chitiet_percent;
    }

    protected void btnXoa_ServerClick(object sender, EventArgs e)
    {
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "quanlycongviec_chitiet_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                tbQuanLyCongViec_ChiTiet del = (from nc in db.tbQuanLyCongViec_ChiTiets
                                                where nc.quanlycongviec_chitiet_id == Convert.ToInt32(item)
                                                select nc).Single();
                db.tbQuanLyCongViec_ChiTiets.DeleteOnSubmit(del);
                db.SubmitChanges();
                getList();
            }
            alert.alert_Success(Page, "Đã xóa dữ liệu", "");
        }
        else
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
    }

    protected void btnDuyet_ServerClick(object sender, EventArgs e)
    {
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "quanlycongviec_chitiet_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                tbQuanLyCongViec_ChiTiet Duyet = (from nc in db.tbQuanLyCongViec_ChiTiets
                                                  where nc.quanlycongviec_chitiet_id == Convert.ToInt32(item)
                                                  select nc).Single();
                Duyet.quanlycongviec_chitiet_status = true;
                db.SubmitChanges();

            }
            getList();
            alert.alert_Success(Page, "Đã Duyệt", "");
        }
        else
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
    }

    protected void btnHuyDuyet_ServerClick(object sender, EventArgs e)
    {
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "quanlycongviec_chitiet_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                tbQuanLyCongViec_ChiTiet Duyet = (from nc in db.tbQuanLyCongViec_ChiTiets
                                                  where nc.quanlycongviec_chitiet_id == Convert.ToInt32(item)
                                                  select nc).Single();
                Duyet.quanlycongviec_chitiet_status = false;
                db.SubmitChanges();

            }
            getList();
            alert.alert_Success(Page, "Hủy Duyệt", "");
        }
        else
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
    }
}
