﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_QuanLyDeadline_module_NoiDungCongVanDen : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    protected void Page_Load(object sender, EventArgs e)
    {
        var getData = (from n in db.tbQuanLyCongVanDens
                       join cv in db.tbQuanLyCongVanDis on n.congvandi_id equals cv.congvandi_id
                       join u in db.admin_Users on n.username_id equals u.username_id
                       where n.congvanden_id == Convert.ToInt32(RouteData.Values["id"])
                       select new
                       {
                           n.congvanden_id,
                           cv.congvandi_name,
                           cv.congvandi_file
                       }).Single();
        lbName.InnerText = getData.congvandi_name;
        string embed = "<object data=\"{0}\" type=\"application/pdf\" width=\"100%\" height=\"500px\">";
        embed += "If you are unable to view file, you can download from <a href = \"{0}\">here</a>";
        embed += " or download <a target = \"_blank\" href = \"http://get.adobe.com/reader/\">Adobe PDF Reader</a> to view the file.";
        embed += "</object>";
        ltEmbed.Text = string.Format(embed, ResolveUrl(getData.congvandi_file));
       
    }

    protected void btnQuayLai_Click(object sender, EventArgs e)
    {
        Response.Redirect("/admin-cong-van-den");
    }
}