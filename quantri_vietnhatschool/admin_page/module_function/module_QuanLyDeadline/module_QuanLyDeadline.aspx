﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_QuanLyDeadline.aspx.cs" Inherits="admin_page_module_function_module_QuanLyDeadline" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v17.1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <script>
        function myDuyet(id) {
            document.getElementById("<%=txt_idCongViec.ClientID%>").value = id;
            document.getElementById("<%=btnAcive.ClientID%>").click();
        }
        function checkAmentityChuaChon(id) {
            var arrayvalue = document.getElementById("<%= txtAmentityChuaChon.ClientID %>").value;
            var array = JSON.parse("[" + arrayvalue + "]");
            var index = array.indexOf(id);
            if (index > -1) {
                array.splice(index, 1);
                document.getElementById("<%= txtAmentityChuaChon.ClientID %>").value = array;
                document.getElementById("<%= txtAmentityChuaChonCount.ClientID %>").value = document.getElementById("<%= txtAmentityChuaChonCount.ClientID %>").value - 1
            }
            else {

                document.getElementById("<%= txtAmentityChuaChonCount.ClientID %>").value = array.push(id);
                document.getElementById("<%= txtAmentityChuaChon.ClientID %>").value = array;
            }
        }
        function myLuu() {
            document.getElementById("<%= btnLuuDuyet.ClientID %>").click();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">

    <div style="display: none">
        <input id="txt_idCongViec" runat="server" />
        <a href="#" id="btnAcive" runat="server" onserverclick="btnAcive_ServerClick"></a>
        <input id="txt_idCongViecChiTiet" runat="server" />
        <asp:UpdatePanel ID="upActiveChiTiet" runat="server">
            <ContentTemplate>
                <a href="#" id="btnAciveChiTiet" runat="server" onserverclick="btnAciveChiTiet_ServerClick"></a>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div class="container">
        <h4 class="title-qlvc text-primary">Quản Lý Thời Gian Công Việc</h4>
        <hr />
        <div class="card card-block">
            <div class="content-qlcv">
                <dx:ASPxComboBox ID="ddlTaiKhoan" runat="server" TextField="username_fullname" ValueField="username_id" ClientInstanceName="ddlTaiKhoan" CssClass="" Width="20%"></dx:ASPxComboBox>
                <a class="btn btn-success btn-select" id="btnShow" runat="server" onserverclick="btnShow_Click" href="#">Chọn</a>
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Thông Tin</th>
                            <th scope="col">Ngày Bắt Đầu</th>
                            <th scope="col">Ngày Kết Thúc</th>
                            <th scope="col">Hoàn thành</th>
                            <th scope="col">Lý Do Quá Hạn</th>
                            <th scope="col">Chi Tiết</th>
                            <th scope="col">Duyệt</th>
                        </tr>
                    </thead>
                    <tbody>
                        <asp:Repeater ID="rpList" runat="server">
                            <ItemTemplate>
                                <tr <%#Eval("quanlycongviec_class") %>>
                                    <th scope="row"><%=STT++ %></th>
                                    <td><%#Eval("quanlycongviec_name") %> <img class="flag" src="<%#Eval("quanlycongviec_flag") %>"/> </td>
                                    <td style="text-align: center"><%#Convert.ToDateTime(Eval("quanlycongviec_createdate")).ToShortDateString()%></td>
                                    <td style="text-align: center"><%#Convert.ToDateTime(Eval("quanlycongviec_enddate")).ToShortDateString()%></td>
                                    <td style="text-align: center"><%#Eval("hoanthanhpercent") %></td>
                                    <td style="text-align: center"><a class="<%#Eval("classquahan") %>" href="#" data-toggle="modal" data-target="#modalQuaHan<%#Eval("quanlycongviec_id") %>"><%#Eval("XemQuaHan") %></a></td>
                                    <td><a class="<%#Eval("classcongviec") %>" href="#" data-toggle="modal" data-target="#modalChiTiet<%#Eval("quanlycongviec_id") %>"><%#Eval("XemChiTiet") %></a></td>
                                    <td style="text-align: center"><a class="<%#Eval("classDuyet") %>" href="#" id="btnDuyet" onclick="myDuyet(<%#Eval("quanlycongviec_id") %>)"><%#Eval("quanlycongviec_DuyetCongViec") %></a></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </tbody>
                </table>
            </div>
            <div style="display: none">
                <input type="text" id="txtAmentityChuaChon" runat="server" />
                <input type="text" id="txtAmentityChuaChonCount" runat="server" />
                <a href="#" id="btnLuuDuyet" runat="server" onserverclick="btnLuuDuyet_ServerClick"></a>
            </div>
        </div>
        <%-- modal lý do quá hạn --%>
        <asp:Repeater ID="rpMoDalQuaHan" runat="server">
            <ItemTemplate>
                <div class="modal fade" id="modalQuaHan<%#Eval("quanlycongviec_id") %>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="exampleModalLabel">Lý do quá hạn</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="content-popup">
                                    <div class="container">
                                        <h4 class="label-title"><%#Eval("quanlycongviec_name") %></h4>
                                    </div>
                                    <div class="grv-content">
                                        <h6>Nội Dung:</h6>
                                        <p><%#Eval("quanlycongviec_note") %></p>
                                    </div>
                                </div>
                            </div>
                            <%-- <div class="modal-footer">
                        <a class="btn btn-primary" href="#">Đã xem</a>
                    </div>--%>
                        </div>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>

        <%--modal chi tiết--%>

        <asp:Repeater ID="rpChiTietNoiDung" runat="server" OnItemDataBound="rpChiTietNoiDung_ItemDataBound">
            <ItemTemplate>
                <div class="modal fade" id="modalChiTiet<%#Eval("quanlycongviec_id") %>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg modal-chitiet" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="exampleModalLabel1">Nội dung chi tiết</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="content-popup">
                                    <div class="container">
                                        <h4 class="label-title"><%#Eval("quanlycongviec_name") %></h4>
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                            <ContentTemplate>
                                                <table class="table table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th scope="col">STT</th>
                                                            <th scope="col">Nội dung</th>
                                                            <th scope="col">Ngày hết hạn</th>
                                                            <th scope="col">Hoàn thành (%)</th>
                                                            <th scope="col">Duyệt</th>
                                                            <th scope="col">Check</th>

                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        <asp:Repeater ID="rpListChiTiet" runat="server">
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <th scope="row"><%=STT1++ %></th>
                                                                    <td><%#Eval("quanlycongviec_chitiet_content") %></td>
                                                                    <td><%#Convert.ToDateTime(Eval("quanlycongviec_chitiet_enddate")).ToShortDateString()%></td>
                                                                    <td><%#Eval("quanlycongviec_chitiet_percent") %></td>
                                                                    <td style="color: white"><a id="btnDuyet" class="btn btn-primary"><%#Eval("quanlycongviec_chitiet_status") %></a></td>
                                                                    <td>
                                                                        <input type="checkbox" id="<%#Eval("quanlycongviec_chitiet_id") %>" value="(<%#Eval("quanlycongviec_chitiet_id") %>)" onclick="checkAmentityChuaChon(<%#Eval("quanlycongviec_chitiet_id") %>)">
                                                                        <%--   <input type="checkbox" name="check" value="" />--%></td>
                                                                </tr>
                                                            </ItemTemplate>
                                                        </asp:Repeater>
                                                    </tbody>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>

                                    </div>
                                    <div class="modal-footer">
                                        <a class="btn btn-primary" href="#" id="btnLuu" onclick="myLuu()">Lưu</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
    <style>
        input[type='checkbox']:checked:after {
            background: #5F95FC;
            content: '\2714';
            color: #fff;
            font-size: 23px;
        }

        input[type='checkbox']:after {
            content: "";
            vertical-align: middle;
            text-align: center;
            line-height: 13px;
            position: absolute;
            cursor: pointer;
            height: 20px;
            width: 20px;
            left: 0;
            top: 0;
            font-size: 10px;
            /*-webkit-box-shadow: inset 0 1px 1px #5F95FC, 0 1px 0 #5F95FC;
            -moz-box-shadow: inset 0 1px 1px #5F95FC, 0 1px 0 #5F95FC;
            box-shadow: inset 0 1px 1px #5F95FC, 0 1px 0 #5F95FC;
            background: #5F95FC;*/
        }

        input[type=checkbox] {
            vertical-align: middle;
            position: relative;
            bottom: -5px;
            left: 15px;
            width: 20px;
            height: 20px;
        }

        .title-qlvc {
            text-align: center;
            font-weight: 600;
        }

        .content-qlcv {
            /*background-color: whitesmoke;*/
            overflow-x: auto;
        }

        .custom-select {
            width: 300px;
        }

        .btn-select {
            width: 100px;
            margin-top: 5px;
            position: absolute;
            top: 14px;
            left: 22%;
        }

        }

        .label-title {
            display: block;
            margin-left: 50px;
            margin-top: 10px;
            margin-bottom: 15px;
        }

        .modal-title {
            display: inline-block;
        }

        .modal-header .close {
            margin-top: 5px;
        }

        .grv-content {
            margin-top: 20px;
        }

        .title-popup {
            margin-top: 15px;
            margin-bottom: 25px;
        }

        h4.label-title {
            text-align: center;
            margin: 0;
            font-size: 30px;
            font-weight: 600;
            margin-bottom: 30px;
            margin-top: 15px;
        }

        .modal.in .modal-dialog.modal-chitiet {
            -webkit-transform: translate(0, 0);
            -ms-transform: translate(0, 0);
            -o-transform: translate(0, 0);
            transform: translate(0, 0);
            margin-left: 23%;
        }

        .dxeButtonEditSys.dxeButtonEdit_Moderno {
            border-collapse: separate;
            margin-bottom: 20px;
        }
        .flag{
            max-width:100%;
            height:20px;
            margin-left:20px;
        }
        @media only screen and (max-width:480px) {
            table#hibodywrapper_ddlTaiKhoan {
                width: 55%;
            }

            .btn-select {
                width: 100px;
                margin-top: 5px;
                position: absolute;
                top: 4px;
                left: 60%;
            }
        }
    </style>

</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

