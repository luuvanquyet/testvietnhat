﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_QuanLyDeadline : System.Web.UI.Page
{
    public int STT, STT1 = 1;
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            var listNV = from nv in db.admin_Users where nv.groupuser_id == 4 && nv.username_username != "vpnguyen" select nv;
            ddlTaiKhoan.DataSource = listNV;
            ddlTaiKhoan.DataBind();
            // listData();
        }
        rpMoDalQuaHan.DataSource = from ld in db.tbQuanLyCongViecs
                                   where ld.quanlycongviec_duyet == true
                                   select ld;
        rpMoDalQuaHan.DataBind();
        rpChiTietNoiDung.DataSource = from ld in db.tbQuanLyCongViecs
                                      select ld;
        rpChiTietNoiDung.DataBind();

    }
    protected void listData()
    {
        rpList.DataSource = from l in db.tbQuanLyCongViecs
                            where l.username_id == Convert.ToInt32(ddlTaiKhoan.Value) && l.quanlycongviec_private==false
                            orderby l.quanlycongviec_class
                            select new
                            {
                                l.quanlycongviec_id,
                                l.quanlycongviec_name,
                                l.quanlycongviec_createdate,
                                l.quanlycongviec_enddate,
                                l.quanlycongviec_quahan,
                                l.quanlycongviec_class,
                                hoanthanhpercent = (from a in db.tbQuanLyCongViec_ChiTiets where a.quanlycongviec_id == l.quanlycongviec_id && a.quanlycongviec_chitiet_status == true select a).Count() + "/" + (from ct in db.tbQuanLyCongViec_ChiTiets where ct.quanlycongviec_id == l.quanlycongviec_id select ct).Count(),
                                quanlycongviec_DuyetCongViec = l.quanlycongviec_class != null ? "" : "Duyệt",
                                XemQuaHan = l.quanlycongviec_duyet == true && l.quanlycongviec_note != null ? "Xem" : "",
                                classquahan = l.quanlycongviec_duyet == true && l.quanlycongviec_note != null ? "btn btn-primary" : "",
                                classDuyet = l.quanlycongviec_duyet == true ? "" : "btn btn-primary",
                                XemChiTiet = (from ct in db.tbQuanLyCongViec_ChiTiets where ct.quanlycongviec_id == l.quanlycongviec_id select ct).Count() > 0 ? "Xem" : "",
                                classcongviec = (from ct in db.tbQuanLyCongViec_ChiTiets where ct.quanlycongviec_id == l.quanlycongviec_id select ct).Count() > 0 ? "btn btn-primary" : "",
                                quanlycongviec_flag = (from ct in db.tbQuanLyCongViec_ChiTiets where ct.quanlycongviec_id == l.quanlycongviec_id && ct.quanlycongviec_chitiet_flag == true select ct).Count() > 0 ? "../../../images/yellow-co.jpg" : ""
                            };
        rpList.DataBind();
    }
    protected void btnShow_Click(object sender, EventArgs e)
    {
        listData();
    }

    protected void btnAcive_ServerClick(object sender, EventArgs e)
    {
        tbQuanLyCongViec update = (from cv in db.tbQuanLyCongViecs
                                   where cv.quanlycongviec_id == Convert.ToInt32(txt_idCongViec.Value)
                                   select cv).SingleOrDefault();
        update.quanlycongviec_duyet = true;
        update.quanlycongviec_class = "style='background-color: red; color: #fff;'";
        db.SubmitChanges();
        alert.alert_Success(Page, "Đã duyệt nội dung", "");
        listData();

    }

    protected void rpChiTietNoiDung_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        Repeater rpListChiTiet = e.Item.FindControl("rpListChiTiet") as Repeater;
        int quanlycongviec_id = int.Parse(DataBinder.Eval(e.Item.DataItem, "quanlycongviec_id").ToString());
        rpListChiTiet.DataSource = from ct in db.tbQuanLyCongViec_ChiTiets
                                   where ct.quanlycongviec_id == quanlycongviec_id
                                   select new
                                   {
                                       ct.quanlycongviec_chitiet_id,
                                       ct.quanlycongviec_chitiet_content,
                                       ct.quanlycongviec_chitiet_percent,
                                       ct.quanlycongviec_chitiet_enddate,
                                       quanlycongviec_chitiet_status = ct.quanlycongviec_chitiet_status == true ? "Đã duyệt" : "Chưa duyệt"
                                   };
        rpListChiTiet.DataBind();
    }

    protected void btnAciveChiTiet_ServerClick(object sender, EventArgs e)
    {
        tbQuanLyCongViec_ChiTiet Duyet = (from nc in db.tbQuanLyCongViec_ChiTiets
                                          where nc.quanlycongviec_chitiet_id == Convert.ToInt32(txt_idCongViecChiTiet.Value)
                                          select nc).Single();
        Duyet.quanlycongviec_chitiet_status = true;
        db.SubmitChanges();

    }
    protected void btnLuuDuyet_ServerClick(object sender, EventArgs e)
    {
        if (txtAmentityChuaChon.Value != "")
        {
            string[] amentiy = txtAmentityChuaChon.Value.Split(',');
            foreach (string item in amentiy)
            {
                tbQuanLyCongViec_ChiTiet checkChiTietDuyet = (from d in db.tbQuanLyCongViec_ChiTiets where d.quanlycongviec_chitiet_id == Convert.ToInt32(item) select d).SingleOrDefault();
                checkChiTietDuyet.quanlycongviec_chitiet_status = true;
                checkChiTietDuyet.quanlycongviec_chitiet_flag = false;
                db.SubmitChanges();
            }
            listData();
            rpChiTietNoiDung.DataSource = from ld in db.tbQuanLyCongViecs
                                          select ld;
            rpChiTietNoiDung.DataBind();
            alert.alert_Success(Page, "Đã duyệt xong", "");

        }
    }
}