﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_WebSite_module_QuanLyDeadlineCaNhan : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    DataTable dtSchool;
    private int _id;
    public string title, summary, content, image, vitri, TrungHocCoSo, TrangChu, TrungHocPhoThong, MamNon, TieuHoc;
    protected void Page_Load(object sender, EventArgs e)
    {
        edtnoidung1.Toolbars.Add(HtmlEditorToolbar.CreateStandardToolbar1());
        if (!IsPostBack)
        {
            Session["_id"] = 0;
        }
        loadData();
    }
    private void loadData()
    {
            var getData = from nc in db.tbQuanLyCongViecs
                          join sc in db.admin_Users on nc.username_id equals sc.username_id
                          where sc.username_username == Request.Cookies["UserName"].Value
                          orderby nc.quanlycongviec_id descending
                          select new
                          {
                              nc.quanlycongviec_id,
                              nc.quanlycongviec_name,
                              nc.quanlycongviec_createdate,
                              nc.quanlycongviec_enddate,
                              nc.quanlycongviec_quahan,
                              quanlycongviec_duyet = nc.quanlycongviec_duyet == true ? "Đã duyệt" : "Chưa duyệt",
                              quanlycongviec_private = nc.quanlycongviec_private == true?"Riêng tư":""
                          };

            grvList.DataSource = getData;
            grvList.DataBind();
    }
    private void setNULL()
    {
        txtTitle.Text = "";
        edtnoidung1.Html = "";
    }
    protected void btnThem_Click(object sender, EventArgs e)
    {
        // Khi nhấn nút thêm thì mật định session id = 0 để thêm mới
        Session["_id"] = 0;
        // gọi hàm setNull để trả toàn bộ các control về rỗng
        setNULL();
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupControl.Show();", true);
        loadData();
    }

    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        // get value từ việc click vào gridview
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "quanlycongviec_id" }));
        var checkDuyet = from d in db.tbQuanLyCongViecs where d.quanlycongviec_duyet == true && d.quanlycongviec_id == _id select d;
        if (checkDuyet.Count() > 0)
        {
            alert.alert_Warning(Page, "Quản trị đã duyệt vì thế bạn không thể cập nhật lại thông tin", "");
        }
        else
        {
            // đẩy id vào session
            Session["_id"] = _id;
            var getData = (from nc in db.tbQuanLyCongViecs
                           join sc in db.admin_Users on nc.username_id equals sc.username_id
                           where nc.quanlycongviec_id == _id
                           select new
                           {
                               nc.quanlycongviec_id,
                               nc.quanlycongviec_name,
                               nc.quanlycongviec_createdate,
                               nc.quanlycongviec_enddate,
                               nc.quanlycongviec_content
                           }).Single();
            txtTitle.Text = getData.quanlycongviec_name;
            ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupControl.Show();", true);
            loadData();
        }
    }
    public bool checknull()
    {
        if (txtTitle.Text != "")
            return true;
        else return false;
    }
    protected void btnLuu_Click(object sender, EventArgs e)
    {
        
        cls_QuanLyCongViec cls = new cls_QuanLyCongViec();
        if (Session["_id"].ToString() == "0")
        {
            var checkUser = (from u in db.admin_Users where u.username_username == Request.Cookies["UserName"].Value select u).SingleOrDefault();

            if (cls.insert_CongViec(checkUser.username_id, txtTitle.Text, Convert.ToDateTime(dteCreateDate.Value), Convert.ToDateTime(dteEndDate.Value), Convert.ToBoolean(ckRiengTu.Checked)))
            {
                alert.alert_Success(Page, "Thêm thành công", "");
                //popupControl.ShowOnPageLoad = false;
                loadData();
            }
            else alert.alert_Error(Page, "Thêm thất bại", "");

        }
        else
        {
            if (dteCreateDate.Value == "" && dteEndDate.Value =="")
            {
                if (cls.update_CongViec1(Convert.ToInt32(Session["_id"].ToString()), txtTitle.Text, Convert.ToBoolean(ckRiengTu.Checked)))
                {
                    ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Cập nhật thành công','','success').then(function(){grvList.Refresh();})", true);
                    popupControl.ShowOnPageLoad = false;
                    loadData();
                }
                else alert.alert_Error(Page, "Cập nhật thất bại", "");
                loadData();
            }
            else{
                if (cls.update_CongViec(Convert.ToInt32(Session["_id"].ToString()), txtTitle.Text, Convert.ToDateTime(dteCreateDate.Value), Convert.ToDateTime(dteEndDate.Value), Convert.ToBoolean(ckRiengTu.Checked)))
                {
                    ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Cập nhật thành công','','success').then(function(){grvList.Refresh();})", true);
                    popupControl.ShowOnPageLoad = false;
                    loadData();
                }
                else alert.alert_Error(Page, "Cập nhật thất bại", "");
                loadData();
            }
        }
    }
    protected void btnXoa_Click(object sender, EventArgs e)
    {
        alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
    }

    protected void btnLyDoQuaHan_Click(object sender, EventArgs e)
    {
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "quanlycongviec_id" }));

        tbQuanLyCongViec getData = (from cv in db.tbQuanLyCongViecs where cv.quanlycongviec_duyet == true && cv.quanlycongviec_id == _id && cv.quanlycongviec_enddate<DateTime.Now select cv).SingleOrDefault();
        if (getData!=null)
        {
              TextBox1.Text = getData.quanlycongviec_name;
            edtnoidung1.Html = getData.quanlycongviec_note;
            ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupControl1.Show();", true);
        }
        else
        {
            alert.alert_Warning(Page, "Thời gian kết thúc chưa hết cho nên không thể viết lý do", "");
        }
    }

    protected void btnLuuLyDo_Click(object sender, EventArgs e)
    {
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "quanlycongviec_id" }));
        tbQuanLyCongViec update = (from cv in db.tbQuanLyCongViecs where cv.quanlycongviec_id == _id select cv).SingleOrDefault();
        update.quanlycongviec_note = edtnoidung1.Html;
        db.SubmitChanges();
        alert.alert_Success(Page, "Đã gửi về cho quản trị xem, bạn có quyền chỉnh sửa cho các lần sau", "");
    }

    protected void btnNoiDungChiTiet_Click(object sender, EventArgs e)
    {
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "quanlycongviec_id" }));
        Response.Redirect("admin-nhap-quan-ly-deadline-" + _id);
    }
}