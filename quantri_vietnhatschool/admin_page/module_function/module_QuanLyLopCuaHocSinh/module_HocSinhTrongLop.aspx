﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_HocSinhTrongLop.aspx.cs" Inherits="admin_page_module_function_module_HocSinhTrongLop" %>

<%@ Register Assembly="DevExpress.Web.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">

    <div class="card card-block">
        <div class="row header_header">
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <div class="col-sm-2">
                        <asp:DropDownList ID="ddlKhoi" runat="server" CssClass="form-control" Style="outline: solid" OnSelectedIndexChanged="ddlKhoi_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                    </div>
                     <div class="col-sm-2">
                        <asp:DropDownList ID="ddlLop" runat="server" CssClass="form-control" Style="outline: solid" OnSelectedIndexChanged="ddlLop_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                    </div>  
                   
                     <div class="col-sm-2">
                          <label id="lbCheck" runat="server"></label>
                    </div>
                      <div class="col-sm-2">
                    </div>
                      <div class="col-sm-2">
                          <asp:Button ID="btnUp" runat="server" Text="Nhập dữ liệu học sinh từ File Excel" CssClass="btn btn-primary" OnClick="btnUp_Click" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
           
        </div>
   
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                 <div style="text-align: center; font-size: 28px; font-weight: bold; color: blue">
                         <label id="lbcontent" runat="server"></label>
                 </div>
                <div class="form-group table-responsive">
                    <dx:ASPxGridView ID="grvList" runat="server" ClientInstanceName="grvList" KeyFieldName="hocsinh_id" Width="100%">
                        <Columns>
                            <dx:GridViewDataColumn Caption="STT" HeaderStyle-HorizontalAlign="Center" Width="2%">
                                <DataItemTemplate>
                                    <%#Container.ItemIndex+1 %>
                                </DataItemTemplate>
                            </dx:GridViewDataColumn>
                            <dx:GridViewCommandColumn ShowSelectCheckbox="True" SelectAllCheckboxMode="Page" VisibleIndex="0" Width="2%"></dx:GridViewCommandColumn>
                          <%--   <dx:GridViewDataColumn Caption="Họ học sinh" FieldName="hocsinh_ho" HeaderStyle-HorizontalAlign="Center" Width="60%"></dx:GridViewDataColumn>--%>
                            <dx:GridViewDataColumn Caption="Tên học sinh" FieldName="hocsinh_name" HeaderStyle-HorizontalAlign="Center" Width="20%"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn Caption="Giới tính" FieldName="gender" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>
                        </Columns>
                        <SettingsSearchPanel Visible="true" />
                        <SettingsBehavior AllowFocusedRow="true" />
                        <SettingsText EmptyDataRow="Không có dữ liệu" SearchPanelEditorNullText="Gỏ từ cần tìm kiếm và enter..." />
                        <SettingsLoadingPanel Text="Đang tải..." />
                        <SettingsPager PageSize="50" Summary-Text="Trang {0} / {1} ({2} trang)"></SettingsPager>
                    </dx:ASPxGridView>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    
        <%--  --%>
      <dx:ASPxPopupControl ID="popupControl" runat="server" Width="500" Height="400" CloseAction="CloseButton" ShowCollapseButton="True" ShowMaximizeButton="True" ScrollBars="Auto" CloseOnEscape="true" Modal="True"
            PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" ClientInstanceName="popupControl" ShowFooter="true"
            HeaderText="" AllowDragging="True" PopupAnimationType="Fade" EnableViewState="False" AutoUpdatePosition="true" ClientSideEvents-CloseUp="function(s,e){grvListCuocHop.Refresh();}">
            <ContentCollection>
                <dx:PopupControlContentControl runat="server">
                    <h4>Chọn File từ máy tính</h4>
                    <br />
                            <div class="popup-main">
                                    <asp:FileUpload ID="fuUpload" multiple="multiple" runat="server"/>  
                                     <hr />
                            </div>
                </dx:PopupControlContentControl>
            </ContentCollection>

            <FooterContentTemplate>
                <div class="mar_but button">
                    <asp:UpdatePanel ID="udSave" runat="server">
                        <ContentTemplate>
                               <asp:Button ID="btnLuu" runat="server" Text="Lưu" CssClass="btn btn-primary" OnClick="btnLuu_Click" ClientMode="static"/>
                        </ContentTemplate>
                           <Triggers>  
                             <asp:PostBackTrigger ControlID="btnLuu"/>  
                         </Triggers>  
                    </asp:UpdatePanel>
                </div>
                 
            </FooterContentTemplate>
            <ContentStyle>
                <Paddings PaddingBottom="0px" />
            </ContentStyle>
        </dx:ASPxPopupControl>
</asp:Content>

<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

