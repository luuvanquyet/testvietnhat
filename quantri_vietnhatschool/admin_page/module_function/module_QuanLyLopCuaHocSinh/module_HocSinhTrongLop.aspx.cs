﻿using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Web.UI;

public partial class admin_page_module_function_module_HocSinhTrongLop : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    DataTable dtCauHoi;
    private static int _idLop = 0;
    private static string _nameLop = "";
    //code tiêp

    protected void Page_Load(object sender, EventArgs e)
    {
        if (_idLop != 0)
        {
            LoadData();
        }

        //lấy thông tin tk đăng nhập 
        var getUser = (from u in db.admin_Users
                       where u.username_username == Request.Cookies["UserName"].Value
                       select u).SingleOrDefault();
        if (!IsPostBack)
        {

            var getKhoi = from k in db.tbKhois
                          where k.khoi_id <= 12
                          select k;
            ddlKhoi.Items.Clear();
            ddlKhoi.AppendDataBoundItems = true;
            ddlKhoi.Items.Insert(0, "Chọn khối");
            ddlKhoi.DataValueField = "khoi_id";
            ddlKhoi.DataTextField = "khoi_name";
            ddlKhoi.DataSource = getKhoi;
            ddlKhoi.DataBind();
            ddlLop.Visible = false;
        }
    }
    public void CloseData()
    {
        _idLop = 0;
        _nameLop = "";
        grvList.DataSource = null;
        grvList.DataBind();
        lbcontent.InnerText = "";
    }
    protected void ddlKhoi_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlKhoi.SelectedItem.Text == "Chọn khối")
        {
            CloseData();
            ddlLop.Visible = false;
        }
        else
        {
            CloseData();
            LoadDllKhoi();
        }

    }

    public void LoadDllKhoi()
    {
        var getdataMon = from lop in db.tbLops
                         where lop.khoi_id == Convert.ToInt32(ddlKhoi.SelectedValue)
                         select lop;
        ddlLop.Items.Clear();
        ddlLop.AppendDataBoundItems = true;
        ddlLop.Items.Insert(0, "Chọn lớp");
        ddlLop.DataValueField = "lop_id";
        ddlLop.DataTextField = "lop_name";
        ddlLop.DataSource = getdataMon;
        ddlLop.DataBind();
        ddlLop.Visible = true;
    }

    protected void ddlLop_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlLop.SelectedItem.Text == "Chọn lớp")
        {
            CloseData();
        }
        else
        {
            _idLop = Convert.ToInt32(ddlLop.SelectedValue);
            _nameLop = ddlLop.SelectedItem.Text;
            LoadData();
        }
    }
    public void LoadData()
    {
        var getData = from hs in db.tbHocSinhs
                      join hstl in db.tbHocSinhTrongLops on hs.hocsinh_id equals hstl.hocsinh_id
                      join lop in db.tbLops on hstl.lop_id equals lop.lop_id
                      where lop.lop_id == _idLop
                      select new
                      {
                          hs.hocsinh_id,
                          hs.hocsinh_name,
                          gender = Convert.ToBoolean(hs.hocsinh_gioitinh) ? "Nam" : "Nữ",
                      };
        lbcontent.InnerText = _nameLop.ToUpper();
        grvList.DataSource = getData;
        grvList.DataBind();
    }


    protected void btnUp_Click(object sender, EventArgs e)
    {
        CloseData();
        ddlKhoi.ClearSelection();
        ddlLop.Visible = false;
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupControl.Show();", true);
    }



    protected void btnLuu_Click(object sender, EventArgs e)
    {
        if (!fuUpload.HasFile)
        {
            alert.alert_Warning(Page, "Chưa chọn file!", "");
        }
        else
        {
            try
            {
                // import file excel to database //(tbHocSinh)
                string fileName = fuUpload.FileName;
                string ext = Path.GetExtension(fileName);
            

                if (ext.ToLower() == ".xls" || ext.ToLower().Equals(".xlsx"))
                {
                    string path = string.Concat(Server.MapPath("~/Excel Files/" + fileName));

                    //Thư mục chưa có file thì lưu lại
                    if (!File.Exists(path))
                    {
                        fuUpload.SaveAs(path);
                    }
                
                    cls_ExcelApiTest eat = new cls_ExcelApiTest(path);
                    eat.OpenExcel();
                    int rowCount = eat.GetRowCount("Sheet1");
                    try
                    {
                        for (int i = 2; i <= rowCount; i++)
                        {
                            //Cách 1:
                            //Chức năng thêm
                            //lấy giá trị từng cells trong file excel bỏi column name
                            string hocsinh_code = eat.GetCellData("Sheet1", "Mã học sinh", i);
                            var exist_hs = (from h in db.HocSinhs where h.hocsinh_code == hocsinh_code select h).FirstOrDefault();
                            if(exist_hs != null)
                            {
                                continue; //skip
                            }
                            else
                            {
                                string hocsinh_ho = eat.GetCellData("Sheet1", "Họ", i);
                                string hocsinh_ten = eat.GetCellData("Sheet1", "Tên", i);
                                string hocsinh_gioitinh = eat.GetCellData("Sheet1", "Giới tính", i);
                                DateTime hocsinh_ngaysinh = Convert.ToDateTime(eat.GetCellData("Sheet1", "Ngày sinh", i));
                                string hocsinh_tenba = eat.GetCellData("Sheet1", "Họ và tên ba", i);
                                string hocsinh_tenme = eat.GetCellData("Sheet1", "Họ và tên mẹ", i);
                                string hocsinh_nghenghiepba = eat.GetCellData("Sheet1", "Nghề nghiệp ba", i);
                                string hocsinh_nghenghiepme = eat.GetCellData("Sheet1", "Nghề nghiệp mẹ", i);
                                string hocsinh_emailba = eat.GetCellData("Sheet1", "Email ba", i);
                                string hocsinh_emailme = eat.GetCellData("Sheet1", "Email mẹ", i);
                                string hocsinh_sdtba = eat.GetCellData("Sheet1", "Số điện thoại ba", i);
                                string hocsinh_sdtme = eat.GetCellData("Sheet1", "Số điện thoại mẹ", i);
                                string hocsinh_noio = eat.GetCellData("Sheet1", "Nơi ở", i);

                                //lưu dữ liệu học sinh 
                                HocSinh hs = new HocSinh(); //table HocSinh test
                                hs.hocsinh_code = hocsinh_code;
                                hs.hocsinh_ho = hocsinh_ho;
                                hs.hocsinh_ten = hocsinh_ten;
                                hs.hocsinh_gioitinh = hocsinh_gioitinh;
                                hs.hocsinh_ngaysinh = hocsinh_ngaysinh;
                                hs.hocsinh_emailba = hocsinh_emailba;
                                hs.hocsinh_emailme = hocsinh_emailme;
                                hs.hocsinh_noio = hocsinh_noio;
                                hs.hocsinh_tenba = hocsinh_tenba;
                                hs.hocsinh_tenme = hocsinh_tenme;
                                hs.hocsinh_sdtba = hocsinh_sdtba;
                                hs.hocsinh_sdtme = hocsinh_sdtme;
                                db.HocSinhs.InsertOnSubmit(hs);
                            }    
                        }
                        db.SubmitChanges();
                        eat.CloseExcel();
                        alert.alert_Success(Page, "Lưu dữ liệu thành công!", "");
                    }
                    catch (Exception)
                    { 
                        alert.alert_Error(Page, "Có lỗi!", "");
                        eat.CloseExcel();
                    }
                }
                else
                {
                    alert.alert_Warning(Page, "Vui lòng chọn file excel!", "");
                }
            }
            catch (Exception)
            {
                alert.alert_Warning(Page, "Vui lòng liên hệ tổ IT!", "");
                
            }
        }
    }
}
