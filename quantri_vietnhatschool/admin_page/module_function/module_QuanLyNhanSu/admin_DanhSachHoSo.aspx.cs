﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_admin_DanhSachHoSo : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {
            loadData();

    }
    private void loadData()
    {
        var getData = from tb in db.tbQuanLyNhanSu_ThongTinCaNhans
                      select tb;
        grvList.DataSource = getData;
        grvList.DataBind();
    }
    protected void btnThem_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/admin-ho-so-thong-tin-0");
    }
    protected void btnChiTiet_Click(object sender, EventArgs e)
    {

        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "thongtincanhan_id" }));
        Response.Redirect("admin-ho-so-thong-tin-"+_id);
    }
    protected void btnXoa_Click(object sender, EventArgs e)
    {
        cls_ThongTinCaNhan cls;
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "thongtincanhan_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                cls = new cls_ThongTinCaNhan();
                if (cls.Linq_Xoa(Convert.ToInt32(item)))
                    alert.alert_Success(Page, "Xóa thành công", "");
                else
                    alert.alert_Error(Page, "Xóa thất bại", "");
            }
        }
        else
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
    }
}