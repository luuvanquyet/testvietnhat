﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_WebSite_admin_DanhSachThuViec : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["_id"] = 0;
        }
        loadData();
    }
    private void loadData()
    {
        // load data đổ vào var danh sách
        var getData = from nc in db.tbQuanLyNhanSu_HopDongThuViecs
                      join sc in db.tbQuanLyNhanSu_ThongTinCaNhans on nc.thongtincanhan_id equals sc.thongtincanhan_id
                      orderby nc.hopdongthuviec_id descending
                      select new
                      {
                          nc.hopdongthuviec_id,
                          sc.thongtincanhan_hotenThuong,
                          sc.thongtincanhan_sodienthoai,
                          sc.thongtincanhan_email,
                          nc.hopdongthuviec_ngaythangnambatdau,
                          nc.hopdongthuviec_ngaythangnamketthuc,
                          nc.hopdongthuviec_soHDTV,
                      };
        // đẩy dữ liệu vào gridivew
        grvList.DataSource = getData;
        grvList.DataBind();
    }
    private void setNULL()
    {
        txtHoTen.Text = "";
        txtSoDienThoai.Text = "";
        dteNgayBatDau.Value = "";
        dteNgayKetThuc.Value = "";
        txtEmail.Text = "";
        txtSoHDTV.Text = "";
    }
    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        // get value từ việc click vào gridview
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "hopdongthuviec_id" }));
        // đẩy id vào session
        Session["_id"] = _id;
        var getData = (from nc in db.tbQuanLyNhanSu_HopDongThuViecs
                       join sc in db.tbQuanLyNhanSu_ThongTinCaNhans on nc.thongtincanhan_id equals sc.thongtincanhan_id
                       orderby nc.hopdongthuviec_id descending
                       where nc.hopdongthuviec_id == _id
                       select new
                       {
                           nc.hopdongthuviec_id,
                           sc.thongtincanhan_hotenThuong,
                           sc.thongtincanhan_sodienthoai,
                           sc.thongtincanhan_email,
                           nc.hopdongthuviec_ngaythangnambatdau,
                           nc.hopdongthuviec_ngaythangnamketthuc,
                           nc.hopdongthuviec_soHDTV,
                       }).Single();
        txtHoTen.Text = getData.thongtincanhan_hotenThuong;
        txtSoDienThoai.Text = getData.thongtincanhan_sodienthoai;
        txtEmail.Text = getData.thongtincanhan_email;
        if (getData.hopdongthuviec_ngaythangnambatdau != null)
        {
            dteNgayBatDau.Value = getData.hopdongthuviec_ngaythangnambatdau.Value.ToString("yyyy-MM-dd").Replace(' ', 'T');
            dteNgayKetThuc.Value = getData.hopdongthuviec_ngaythangnamketthuc.Value.ToString("yyyy-MM-dd").Replace(' ', 'T');
        }
        txtSoHDTV.Text = getData.hopdongthuviec_soHDTV;
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupControl.Show();", true);
        loadData();
    }
    public bool checknull()
    {
        if (txtHoTen.Text != "")
            return true;
        else return false;
    }
    protected void btnLuu_Click(object sender, EventArgs e)
    {
        cls_NhanVienThuViec cls = new cls_NhanVienThuViec();
        if (cls.Sua_NhanVienThuViec(Convert.ToInt32(Session["_id"].ToString()), dteNgayBatDau.Value, dteNgayKetThuc.Value, txtSoHDTV.Text))
                {
                    ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Cập nhật thành công','','success').then(function(){grvList.Refresh();})", true);
                    popupControl.ShowOnPageLoad = false;
                    loadData();
                }
                else alert.alert_Error(Page, "Cập nhật thất bại", "");
        loadData();
    }
}