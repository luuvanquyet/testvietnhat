﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_admin_HomThuGopY : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {
            loadData();

    }
    private void loadData()
    {
        var getData = from tb in db.tbQuanTri_HomThuGopies
                      select tb;
        grvList.DataSource = getData;
        grvList.DataBind();
    }
    protected void btnThem_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/admin-noi-dung-hom-thu-gop-y-0");
    }
    protected void btnChiTiet_Click(object sender, EventArgs e)
    {

        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "homthugopy_id" }));
        Response.Redirect("admin-noi-dung-hom-thu-gop-y-" + _id);
    }
    protected void btnXoa_Click(object sender, EventArgs e)
    {
        cls_HomThuGopY cls;
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "homthugopy_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                cls = new cls_HomThuGopY();
                if (cls.Linq_Xoa(Convert.ToInt32(item)))
                    alert.alert_Success(Page, "Xóa thành công", "");
                else
                    alert.alert_Error(Page, "Xóa thất bại", "");
            }
        }
        else
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
    }
}