﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_WebSite_admin_HopDongLaoDong : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["_id"] = 0;
        }
        loadData();
    }
    private void loadData()
    {
        // load data đổ vào var danh sách
        var getData = from nc in db.tbQuanLyNhanSu_HopDongLaoDongs
                      join sc in db.tbQuanLyNhanSu_ThongTinCaNhans on nc.thongtincanhan_id equals sc.thongtincanhan_id
                      orderby nc.hopdonglaodong_id descending
                      select new
                      {
                          nc.hopdonglaodong_id,
                          sc.thongtincanhan_hotenThuong,
                          sc.thongtincanhan_sodienthoai,
                          sc.thongtincanhan_email,
                          nc.hopdonglaodong_ngaythangnambatdau,
                          nc.hopdonglaodong_ngaythangnamketthuc,
                          nc.hopdonglaodong_soHDLD,
                      };
        // đẩy dữ liệu vào gridivew
        grvList.DataSource = getData;
        grvList.DataBind();
    }
    private void setNULL()
    {
        txtHoTen.Text = "";
        txtSoDienThoai.Text = "";
        dteNgayBatDau.Value = "";
        dteNgayKetThuc.Value = "";
        txtEmail.Text = "";
        txtSoHDTV.Text = "";
    }
    protected void btnThem_Click(object sender, EventArgs e)
    {
        // Khi nhấn nút thêm thì mật định session id = 0 để thêm mới
        Session["_id"] = 0;
        // gọi hàm setNull để trả toàn bộ các control về rỗng
        setNULL();
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupControl.Show();", true);
        loadData();
    }

    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        // get value từ việc click vào gridview
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "hopdonglaodong_id" }));
        // đẩy id vào session
        Session["_id"] = _id;
        var getData = (from nc in db.tbQuanLyNhanSu_HopDongLaoDongs
                       join sc in db.tbQuanLyNhanSu_ThongTinCaNhans on nc.thongtincanhan_id equals sc.thongtincanhan_id
                       orderby nc.hopdonglaodong_id descending
                       where nc.hopdonglaodong_id == _id
                       select new
                       {
                           nc.hopdonglaodong_id,
                           sc.thongtincanhan_hotenThuong,
                           sc.thongtincanhan_sodienthoai,
                           sc.thongtincanhan_email,
                           nc.hopdonglaodong_ngaythangnambatdau,
                           nc.hopdonglaodong_ngaythangnamketthuc,
                           nc.hopdonglaodong_soHDLD,
                       }).Single();
        txtHoTen.Text = getData.thongtincanhan_hotenThuong;
        txtSoDienThoai.Text = getData.thongtincanhan_sodienthoai;
        txtEmail.Text = getData.thongtincanhan_email;
        if (getData.hopdonglaodong_ngaythangnambatdau != null)
        {
            dteNgayBatDau.Value = getData.hopdonglaodong_ngaythangnambatdau.Value.ToString("yyyy-MM-dd").Replace(' ', 'T');
            dteNgayKetThuc.Value = getData.hopdonglaodong_ngaythangnamketthuc.Value.ToString("yyyy-MM-dd").Replace(' ', 'T');
        }
        txtSoHDTV.Text = getData.hopdonglaodong_soHDLD;
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupControl.Show();", true);
        loadData();
    }
    public bool checknull()
    {
        if (txtHoTen.Text != "")
            return true;
        else return false;
    }
    protected void btnLuu_Click(object sender, EventArgs e)
    {
        cls_NhanVienThuViec cls = new cls_NhanVienThuViec();
        if (cls.Sua_HopDonglaoDong(Convert.ToInt32(Session["_id"].ToString()), dteNgayBatDau.Value, dteNgayKetThuc.Value, txtSoHDTV.Text))
        {
            ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Lưu thành công','','success').then(function(){grvList.Refresh();})", true);
            popupControl.ShowOnPageLoad = false;
            loadData();
        }
        else alert.alert_Error(Page, "Lưu thất bại", "");
        loadData();
    }
}