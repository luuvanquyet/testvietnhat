﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="admin_NoiDungHomThuGopY.aspx.cs" Inherits="admin_page_module_function_module_QuanLyNhanSu_admin_NoiDungHomThuGopY" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="card card-block">
        <div class="popup-main">
            <div style="text-align:center">
                <span style="font-weight:bold;font-size:30px">HỒM THƯ GÓP Ý</span>
            </div>
            <br />
            <div class="div_content col-12">
                <div class="col-12 form-group">
                    <label class="col-2 form-control-label">Tiêu đề:</label>
                    <div class="col-10">
                        <asp:TextBox ID="txtTieuDe" runat="server" ClientIDMode="Static" CssClass="form-control boxed"> </asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="div_content col-12">
                <div class="col-12 form-group">
                    <label class="col-2 form-control-label">Nội dung:</label>
                    <div class="col-10">
                        <textarea id="edtNoiDung" runat="server" class="form-control boxed" name="edtNoiDung" rows="4" cols="50"></textarea>
                        <br />
                        <a href="#" id="btnGui" runat="server" class="btn btn-primary" onserverclick="btnGui_ServerClick">Gửi</a>
                    </div>
                </div>
            </div>
            <div class="div_content col-12">
                <div class="col-12 form-group">
                    Lưu ý: Các nội dung góp ý của giáo viên sẽ gửi trực tiếp về BGH, và nội dung của giáo viên sẽ được giữ bí mật.
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

