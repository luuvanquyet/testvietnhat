﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_QuanLyNhanSu_admin_NoiDungHomThuGopY : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnGui_ServerClick(object sender, EventArgs e)
    {
        if(txtTieuDe.Text=="" && edtNoiDung.Value=="")
        {
            alert.alert_Error(Page, "Vui lòng nhập đầy đủ thông tin", "");
        }   
        else
        {
            if (Request.Cookies["UserName"] != null)
            {
                var checkuserid = (from u in db.admin_Users where u.username_username == Request.Cookies["UserName"].Value select u).First();
                tbQuanTri_HomThuGopY insert = new tbQuanTri_HomThuGopY();
                insert.homthugopy_title = txtTieuDe.Text;
                insert.homthugopy_content = edtNoiDung.Value;
                insert.username_id = checkuserid.username_id;
                db.tbQuanTri_HomThuGopies.InsertOnSubmit(insert);
                db.SubmitChanges();
                alert.alert_Success(Page, "Đã gửi về BGH", "");
            }
        }    
    }
}