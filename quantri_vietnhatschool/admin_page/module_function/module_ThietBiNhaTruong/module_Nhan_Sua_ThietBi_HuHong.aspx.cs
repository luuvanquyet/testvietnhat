﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_Nhan_Sua_ThietBi_HuHong : System.Web.UI.Page
{
    public int STT = 1;
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    string image;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["_id"] = 0;
        }
        loadData();
    }
    private void loadData()
    {
        if (Request.Cookies["UserName"] != null)
        {
            var checkuserid = (from u in db.admin_Users where u.username_username == Request.Cookies["UserName"].Value select u).First();
            var getData = from n in db.tbQuanTri_NhomThietBiNhaTruongs
                          join tb in db.tbQuanTri_ThietBiNhaTruongs on n.nhomthietbi_id equals tb.nhomthietbi_id
                          join u in db.admin_Users on tb.username_id equals u.username_id
                          where tb.thietbi_nguoinhansua == checkuserid.username_fullname
                          orderby tb.thietbi_id descending
                          select new
                          {
                              n.nhomthietbi_name,
                              tb.thietbi_id,
                              tb.thietbi_name,
                              tb.thietbi_noidung,
                              tb.thietbi_note,
                              tb.thietbi_createdate,
                              u.username_fullname,
                              tb.thietbi_status,
                              color = tb.thietbi_color,
                              tb.thietbi_ngayquahan,
                              tb.thietbi_nguoinhansua,
                              thietbi_hidden = tb.thietbi_status=="Hoàn thành"?"style=display:none":""
                          };
            rpList.DataSource = getData;
            rpList.DataBind();
        }
    }

    protected void btnThem_Click(object sender, EventArgs e)
    {
        Session["_id"] = 0;
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupControl.Show();", true);
    }
    protected void btnXoa_Click(object sender, EventArgs e)
    {

    }
    protected void btnDuyet_Click(object sender, EventArgs e)
    {
        if (Request.Cookies["UserName"] != null)
        {
            var checkuserid = (from u in db.admin_Users where u.username_username == Request.Cookies["UserName"].Value select u).First();
            tbQuanTri_ThietBiNhaTruong updateDuyet = (from d in db.tbQuanTri_ThietBiNhaTruongs where d.thietbi_id == Convert.ToInt32(txt_ThietBi_ID.Value) select d).SingleOrDefault();
            updateDuyet.thietbi_status = checkuserid.username_fullname + " đã tiếp nhận";
            db.SubmitChanges();
            alert.alert_Update(Page, "Đã tiếp nhận thành công ", "");
            loadData();
        }
    }

    private bool SendMail(string email)
    {

        if (email != "")
        {
            try
            {
                var fromAddress = "thongbaovietnhatschool@gmail.com";//  Email Address from where you send the mail 
                var toAddress = email;
                const string fromPassword = "neiabcekdjluofid";
                string subject, title;
                title = "Thông báo";
                subject = "<!DOCTYPE html><html><head><title></title></head><body ><div>" +
                "<h3 style=\"margin-top:0px; text-align:center; color:#029ada\">Bạn nhận được thông tin báo hỏng thiết bị từ Cô Nguyên. Xem chi tiết <a href='http://quantri.vietnhatschool.edu.vn/admin-nhan-xu-ly-thiet-bi-hu-hong'>tại đây.</a></h3>" +
                "</div></body></html>";
                var smtp = new System.Net.Mail.SmtpClient();
                {
                    smtp.Host = "smtp.gmail.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    smtp.Credentials = new NetworkCredential(fromAddress, fromPassword);
                    smtp.Timeout = 20000;
                }
                MailMessage mm = new MailMessage();
                mm.From = new MailAddress(fromAddress, "Trường Liên cấp Việt Nhật");
                mm.Subject = title;
                mm.To.Add(toAddress);
                mm.IsBodyHtml = true;
                mm.Body = subject;
                smtp.Send(mm);
                return true;
            }
            catch
            {
                return false;
            }
        }
        else
            return false;
    }
}