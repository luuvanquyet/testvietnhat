﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_ThietBi_HuHong.aspx.cs" Inherits="admin_page_module_function_module_ThietBi_HuHong" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v17.1" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <script>
        function CountLeft(field, count, max) {
            if (field.value.length > max)
                field.value = field.value.substring(0, max);
            else
                count.value = max - field.value.length;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <script type="text/javascript">
        function func() {
            grvList.Refresh();
            popupControl.Hide();
        }
       <%-- function btnChiTiet() {
            document.getElementById('<%=btnChiTiet.ClientID%>').click();
        }--%>
        function popupHide() {
            document.getElementById('btnClosePopup').click();
        }
        function checkNULL() {
            var CityName = document.getElementById('<%= txtTieuDe.ClientID%>');

            if (CityName.value.trim() == "") {
                swal('Tên form không được để trống!', '', 'warning').then(function () { CityName.focus(); });
                return false;
            }
            return true;
        }
       <%-- function confirmDel() {
            swal("Bạn có thực sự muốn xóa?",
                "Nếu xóa, dữ liệu sẽ không thể khôi phục.",
                "warning",
                {
                    buttons: true,
                    dangerMode: true
                }).then(function (value) {
                    if (value == true) {
                        var xoa = document.getElementById('<%=btnXoa.ClientID%>');
                        xoa.click();
                    }
                });
        }--%>

        function showPreview(input) {
            if (input.files && input.files[0]) {
                var filerdr = new FileReader();
                filerdr.onload = function (e) {
                    $('#hibodywrapper_popupControl_imgPreview').attr('src', e.target.result);
                }
                filerdr.readAsDataURL(input.files[0]);
            }
        }
        function showPreview1(input) {
            if (input.files && input.files[0]) {
                var filerdr = new FileReader();
                filerdr.onload = function (e) {
                    $('#imgPreview1').attr('src', e.target.result);
                }
                filerdr.readAsDataURL(input.files[0]);
            }
        }
        function showImg(img) {
            $('#hibodywrapper_popupControl_imgPreview').attr('src', img);
        }
        function showImg1_1(img) {
            $('#imgPreview1').attr('src', img);
        }
    </script>
    <div class="card card-block">
        <div class="form-group row">
            <div class="col-sm-10">
                <asp:UpdatePanel ID="udButton" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnThem" runat="server" Text="Thêm" CssClass="btn btn-primary" OnClick="btnThem_Click" />
                         <asp:Button ID="btnHoanThanh" runat="server" Text="Nhân viên đã hoàn thành" CssClass="btn btn-primary" OnClick="btnHoanThanh_Click" />
                        <%--<asp:Button ID="btnChiTiet" runat="server" Text="Chi tiết" CssClass="btn btn-primary" OnClick="btnChiTiet_Click" />--%>
                        <%-- <input type="submit" class="btn btn-primary" value="Xóa" onclick="confirmDel()" />
                        <asp:Button ID="btnXoa" runat="server" CssClass="invisible" OnClick="btnXoa_Click" />--%>
                        <%--<asp:Button ID="btnSenMail" runat="server" Text="Gửi mail xử lý" CssClass="btn btn-primary" OnClick="btnSenMail_Click" />--%>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>

        </div>
        <div>
            Lưu ý qui trình <b>tình trạng</b> các bước:
            <br />
            + Bước 1: Chưa duyệt. (Nhân viên chưa tiếp nhận)<br />
            + Bước 2: nhân viên đã tiếp nhận. (Nhân viên sẽ làm việc với giáo viên và bấm vào tiếp nhận).<br />
            + Bước 3: Hoàn thành.(Sau khi nhân viên sửa xong, giáo viên click vào hoàn thành để báo về TBP văn phòng)<br />
        </div>

        <div class="form-group table-responsive">
            <dx:ASPxGridView ID="grvList" runat="server" ClientInstanceName="grvList" KeyFieldName="thietbi_id" Width="100%">
                <Columns>
                    <%--  <dx:GridViewCommandColumn ShowSelectCheckbox="True" SelectAllCheckboxMode="Page" VisibleIndex="0" Width="2%">
                    </dx:GridViewCommandColumn>--%>
                    <dx:GridViewDataColumn Caption="Giáo viên" FieldName="username_fullname" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Lớp" FieldName="thietbi_name" HeaderStyle-HorizontalAlign="Center" Width="5%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Thiết bị hư hỏng" FieldName="thietbi_noidung" HeaderStyle-HorizontalAlign="Center" Width="5%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Ghi chú" FieldName="thietbi_note" HeaderStyle-HorizontalAlign="Center" Width="20%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Ngày tạo" FieldName="thietbi_createdate" HeaderStyle-HorizontalAlign="Center" Width="7%"></dx:GridViewDataColumn>
                     <dx:GridViewDataColumn Caption="Ngày tiếp nhận" FieldName="thietbi_ngaytiepnhan" HeaderStyle-HorizontalAlign="Center" Width="7%"></dx:GridViewDataColumn>
                     <dx:GridViewDataColumn Caption="Ngày quá hạn" FieldName="thietbi_ngayquahan" HeaderStyle-HorizontalAlign="Center" Width="7%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Cần xử lý" FieldName="nhomthietbi_name" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>
                    <dx:GridViewDataColumn Caption="Tình trạng" FieldName="thietbi_status" HeaderStyle-HorizontalAlign="Center" Width="10%"></dx:GridViewDataColumn>
                </Columns>
                <%--  <ClientSideEvents RowDblClick="btnChiTiet" />--%>
                <SettingsSearchPanel Visible="true" />
                <SettingsBehavior AllowFocusedRow="true" />
                <SettingsText EmptyDataRow="Không có dữ liệu" SearchPanelEditorNullText="Gỏ từ cần tìm kiếm và enter..." />
                <SettingsLoadingPanel Text="Đang tải..." />
                <SettingsPager PageSize="10" Summary-Text="Trang {0} / {1} ({2} trang)"></SettingsPager>
            </dx:ASPxGridView>
        </div>
    </div>
    <dx:ASPxPopupControl ID="popupControl" runat="server" Width="600px" Height="400px" CloseAction="CloseButton" ShowCollapseButton="True" ShowMaximizeButton="True" ScrollBars="Auto" CloseOnEscape="true" Modal="True"
        PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" ClientInstanceName="popupControl" ShowFooter="true"
        HeaderText="Báo thiết bị hư hỏng" AllowDragging="True" PopupAnimationType="Fade" EnableViewState="False" AutoUpdatePosition="true" ClientSideEvents-CloseUp="function(s,e){grvList.Refresh();}">
        <ContentCollection>
            <dx:PopupControlContentControl runat="server">
                <asp:UpdatePanel ID="udPopup" runat="server">
                    <ContentTemplate>
                        <div class="popup-main">
                            <div class="div_content col-12">
                                <div class="col-12">
                                    <div class="col-9">

                                        <%-- <div class="col-12 form-group">
                                            <label class="col-3 form-control-label">Giáo viên:</label>
                                            <div class="col-9">
                                                <asp:TextBox ID="txtGiaoVien" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="100%"> </asp:TextBox>
                                            </div>
                                        </div>--%>
                                        <div class="col-12 form-group">
                                            <label class="col-3 form-control-label">Lớp:</label>
                                            <div class="col-9">
                                                <asp:TextBox ID="txtTieuDe" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="100%"> </asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-12 form-group">
                                            <label class="col-3 form-control-label">Thiết bị hư:</label>
                                            <%-- <div class="col-10">
                                                <dx:ASPxComboBox ID="ddlloaisanpham" runat="server" ValueType="System.Int32" TextField="nhomthietbi_name" ValueField="nhomthietbi_id" ClientInstanceName="ddlloaisanpham" CssClass="" Width="100%"></dx:ASPxComboBox>
                                            </div>--%>
                                            <input type="checkbox" id="ckDien" runat="server" name="ckDien" value="Điện" />
                                            <label for="ckDien">Điện</label>
                                            <input type="checkbox" id="ckNuoc" runat="server" name="ckNuoc" value="Nước" />
                                            <label for="ckNuoc">Nước</label>
                                            <input type="checkbox" id="ckMang" runat="server" name="ckMang" value="Mạng" />
                                            <label for="ckMang">Mạng</label>
                                            <input type="checkbox" id="ckCSVC" runat="server" name="ckCSVC" value="Cơ sở vật chất" />
                                            <label for="ckCSVC">Cơ sở vật chất</label>
                                        </div>
                                        <div class="col-12 form-group">
                                            <label class="col-3 form-control-label">Ghi chú:</label>
                                            <div class="col-9">
                                                <asp:TextBox ID="txttomtat" runat="server" ClientIDMode="Static" CssClass="form-control boxed" Width="100%"> </asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-12 form-group">
                                            <label class="col-3 form-control-label">Tình trạng:</label>
                                            <div class="col-9">
                                                <dx:ASPxComboBox ID="ddlloaisanpham" runat="server" ValueType="System.Int32" TextField="nhomthietbi_name" ValueField="nhomthietbi_id" ClientInstanceName="ddlloaisanpham" CssClass="" Width="100%"></dx:ASPxComboBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </dx:PopupControlContentControl>
        </ContentCollection>
        <FooterContentTemplate>
            <div class="mar_but button">
                <asp:Button ID="btnLuu" runat="server" ClientIDMode="Static" Text="Gửi về văn phòng" CssClass="btn btn-primary" OnClientClick="return checkNULL()" OnClick="btnLuu_Click" />
            </div>
        </FooterContentTemplate>
        <ContentStyle>
            <Paddings PaddingBottom="0px" />
        </ContentStyle>
    </dx:ASPxPopupControl>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
    <script type="text/javascript">
        function clickavatar1() {
            $("#up1 input[type=file]").click();
        }
    </script>
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

