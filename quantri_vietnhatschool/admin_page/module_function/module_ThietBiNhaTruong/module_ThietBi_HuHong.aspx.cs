﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_ThietBi_HuHong : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    string image;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["_id"] = 0;
        }
        loadData();
    }
    private void loadData()
    {
        if (Request.Cookies["UserName"] != null)
        {
            var getData = from n in db.tbQuanTri_NhomThietBiNhaTruongs
                          join tb in db.tbQuanTri_ThietBiNhaTruongs on n.nhomthietbi_id equals tb.nhomthietbi_id
                          join u in db.admin_Users on tb.username_id equals u.username_id
                          where u.username_username == Request.Cookies["UserName"].Value
                          orderby tb.thietbi_id descending
                          select new
                          {
                              n.nhomthietbi_name,
                              tb.thietbi_id,
                              tb.thietbi_name,
                              tb.thietbi_noidung,
                              tb.thietbi_note,
                              tb.thietbi_createdate,
                              tb.thietbi_ngaytiepnhan,
                              tb.thietbi_ngayquahan,
                              u.username_fullname,
                              tb.thietbi_status
                          };
            grvList.DataSource = getData;
            grvList.DataBind();
            ddlloaisanpham.DataSource = from tb in db.tbQuanTri_NhomThietBiNhaTruongs
                                        select tb;
            ddlloaisanpham.DataBind();
        }
    }
    private void setNULL()
    {
        txtTieuDe.Text = "";
        txttomtat.Text = "";

        //imgPreview.Src = "";
    }
    protected void btnThem_Click(object sender, EventArgs e)
    {
        Session["_id"] = 0;
        setNULL();
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupControl.Show();", true);
    }
    private bool SendMail(string email)
    {

        if (email != "")
        {
            try
            {
                var fromAddress = "thongbaovietnhatschool@gmail.com";//  Email Address from where you send the mail 
                var toAddress = email;
                const string fromPassword = "neiabcekdjluofid";
                string subject, title;
                title = "Thông báo";
                subject = "<!DOCTYPE html><html><head><title></title></head><body ><div>" +
                "<h3 style=\"margin-top:0px; text-align:center; color:#029ada\">Bạn nhận được thông tin báo hỏng thiết bị từ giáo viên. Xem chi tiết <a href='http://quantri.vietnhatschool.edu.vn/admin-xu-ly-thiet-bi-hu-hong'>tại đây.</a></h3>" +
                "</div></body></html>";
                var smtp = new System.Net.Mail.SmtpClient();
                {
                    smtp.Host = "smtp.gmail.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    smtp.Credentials = new NetworkCredential(fromAddress, fromPassword);
                    smtp.Timeout = 20000;
                }
                MailMessage mm = new MailMessage();
                mm.From = new MailAddress(fromAddress, "Trường Liên cấp Việt Nhật");
                mm.Subject = title;
                mm.To.Add(toAddress);
                mm.IsBodyHtml = true;
                mm.Body = subject;
                smtp.Send(mm);
                return true;
            }
            catch
            {
                return false;
            }
        }
        else
            return false;
    }
    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "thietbi_id" }));
        Session["_id"] = _id;
        var getData = (from n in db.tbQuanTri_NhomThietBiNhaTruongs
                       join tb in db.tbQuanTri_ThietBiNhaTruongs on n.nhomthietbi_id equals tb.nhomthietbi_id
                       join u in db.admin_Users on tb.username_id equals u.username_id
                       where tb.thietbi_id == _id
                       select new
                       {
                           n.nhomthietbi_name,
                           tb.thietbi_id,
                           tb.thietbi_name,
                           tb.thietbi_noidung,
                           tb.thietbi_note,
                           tb.thietbi_createdate,
                           u.username_fullname,
                           tb.thietbi_status
                       }).Single();
        txtTieuDe.Text = getData.thietbi_name;
        //ddlloaisanpham.Text = getData.nhomthietbi_name;
        txttomtat.Text = getData.thietbi_noidung;
        // txtNote.Text = getData.thietbi_note;
        // dteDate.Value = getData.news_datecreate.ToString();
        //imgPreview.Src = getData.news_image;

        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupControl.Show();", true);
        // loadData();
    }
    protected void btnXoa_Click(object sender, EventArgs e)
    {
        cls_ThietBi_HuHong cls;
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "thietbi_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                cls = new cls_ThietBi_HuHong();
                if (cls.Linq_Xoa(Convert.ToInt32(item)))
                {
                    alert.alert_Success(Page, "Xóa thành công", "");
                }
                else
                    alert.alert_Error(Page, "Xóa thất bại", "");
            }
        }
        else
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
        popupControl.ShowOnPageLoad = false;
        loadData();
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "grvList.Refresh();", true);
    }
    public bool checknull()
    {
        if (txtTieuDe.Text != "" || txttomtat.Text != "")
            return true;
        else return false;
    }
    protected void btnLuu_Click(object sender, EventArgs e)
    {
        var checkuserid = (from u in db.admin_Users where u.username_username == Request.Cookies["UserName"].Value select u).First();
        cls_ThietBi_HuHong cls = new cls_ThietBi_HuHong();
        if (checknull() == false)
            alert.alert_Warning(Page, "Hãy nhập đầy đủ thông tin!", "");
        else
        {
            if (Session["_id"].ToString() == "0")
            {
                string noidung = "";
                if (ckDien.Checked == true)
                    noidung = noidung + ckDien.Value + ", ";
                if (ckNuoc.Checked == true)
                    noidung = noidung + ckNuoc.Value + ", ";
                if (ckMang.Checked == true)
                    noidung = noidung + ckMang.Value + ", ";
                if (ckCSVC.Checked == true)
                    noidung = noidung + ckCSVC.Value + " ";
                if (cls.Linq_Them(txtTieuDe.Text, txttomtat.Text, noidung, checkuserid.username_id, Convert.ToInt32(ddlloaisanpham.Value),"Chưa duyệt"))

                {
                    alert.alert_Success(Page, "Đã gửi về văn phòng", "");
                    //SendMail("ducpn@vjis.edu.vn");
                    //SendMail("nguyenttt@vjis.edu.vn");
                    SendMail("tungtt@vjis.edu.vn,ridv@vjis.edu.vn,anhhn@vjis.edu.vn");
                }
                else alert.alert_Error(Page, "Có lỗi xảy ra", "");

            }
            //else
            //{
            //    if (cls.Linq_Sua(Convert.ToInt32(Session["_id"].ToString()), txtTieuDe.Text, txttomtat.Text, 1, checkuserid.username_id, "Chưa duyệt"))
            //        alert.alert_Success(Page, "Cập nhật thành công", "");
            //    else alert.alert_Error(Page, "Cập nhật thất bại", "");
            //}
            popupControl.ShowOnPageLoad = false;
            loadData();
        }
    }
    protected void btnHoanThanh_Click(object sender, EventArgs e)
    {
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "thietbi_id" }));
        tbQuanTri_ThietBiNhaTruong update = db.tbQuanTri_ThietBiNhaTruongs.Where(x => x.thietbi_id == _id).FirstOrDefault();
        update.thietbi_status = "Hoàn thành";
        db.SubmitChanges();
        //SendMail("ducpn@vjis.edu.vn");
        SendMail1("nguyenttt@vjis.edu.vn");
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Alert", "swal('Hoàn thành và đã gửi về văn phòng !','','success').then(function(){grvList.UnselectRows();})", true);
    }
    private bool SendMail1(string email)
    {

        if (email != "")
        {
            try
            {
                var fromAddress = "thongbaovietnhatschool@gmail.com";//  Email Address from where you send the mail 
                var toAddress = email;
                const string fromPassword = "neiabcekdjluofid";
                string subject, title;
                title = "Thông báo";
                subject = "<!DOCTYPE html><html><head><title></title></head><body ><div>" +
                "<h3 style=\"margin-top:0px; text-align:center; color:#029ada\">Cô nhận được thông báo hoàn thành sửa chửa từ giáo viên. Xem chi tiết <a href='http://quantri.vietnhatschool.edu.vn/admin-thong-ke-thiet-bi-hu-hong'>tại đây.</a></h3>" +
                "</div></body></html>";
                var smtp = new System.Net.Mail.SmtpClient();
                {
                    smtp.Host = "smtp.gmail.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    smtp.Credentials = new NetworkCredential(fromAddress, fromPassword);
                    smtp.Timeout = 20000;
                }
                MailMessage mm = new MailMessage();
                mm.From = new MailAddress(fromAddress, "Trường Liên cấp Việt Nhật");
                mm.Subject = title;
                mm.To.Add(toAddress);
                mm.IsBodyHtml = true;
                mm.Body = subject;
                smtp.Send(mm);
                return true;
            }
            catch
            {
                return false;
            }
        }
        else
            return false;
    }
}