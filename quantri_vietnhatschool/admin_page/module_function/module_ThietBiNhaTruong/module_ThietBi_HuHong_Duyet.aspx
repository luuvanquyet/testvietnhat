﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_ThietBi_HuHong_Duyet.aspx.cs" Inherits="admin_page_module_function_module_ThietBi_HuHong_Duyet" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v17.1" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <script>
        function CountLeft(field, count, max) {
            if (field.value.length > max)
                field.value = field.value.substring(0, max);
            else
                count.value = max - field.value.length;
        }
        function myGui(id) {
            document.getElementById("<%=txt_ThietBi_ID.ClientID%>").value = id;
            document.getElementById("<%=btnChuyenNhanVien.ClientID%>").click();
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <script type="text/javascript">
        function func() {
            grvList.Refresh();
        }
    </script>
    <div class="card card-block">
        <div class="form-group row">
            <div>
                Lưu ý qui trình <b>tình trạng</b> các bước:
            <br />
             
            + Bước 1: Chưa duyệt. (Nhân viên chưa tiếp nhận)<br />
            + Bước 2: nhân viên đã tiếp nhận. (Nhân viên sẽ làm việc với giáo viên và bấm vào tiếp nhận).<br />
            + Bước 3: Hoàn thành.(Sau khi nhân viên sửa xong, giáo viên click vào hoàn thành để báo về TBP văn phòng)<br />
                <br />
            </div>
        </div>
        <div>
            <div class="col-3" style="margin-right: 10px">
                Nhân viên:
            <asp:DropDownList ID="ddlNhanVien" CssClass="form-control" runat="server"></asp:DropDownList>
            </div>
            <div class="col-3">
                Tình trạng:
            <asp:DropDownList ID="ddlTinhTrang" CssClass="form-control" runat="server"></asp:DropDownList>
            </div>
        </div>
        <br />
        <br />
        <br />
        <br />
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nhân viên nhận sửa</th>
                    <th scope="col">Giáo viên</th>
                    <th scope="col">Lớp</th>
                    <th scope="col">Thiết bị hư hỏng</th>
                    <th scope="col">Ghi chú</th>
                    <th scope="col">Ngày bắt đầu</th>
                    <th scope="col">Ngày tiếp nhận</th>
                    <th scope="col">Ngày hết hạn</th>
                    <th scope="col">Cần xử lý</th>
                    <th scope="col">Tình trạng</th>
                    <th scope="col">#</th>
                </tr>
            </thead>
            <tbody>
                <asp:Repeater ID="rpList" runat="server">
                    <ItemTemplate>
                        <tr>
                            <th scope="row"><%=STT++ %></th>
                            <td><%#Eval("thietbi_nguoinhansua") %></td>
                            <td><%#Eval("username_fullname") %></td>
                            <td><%#Eval("thietbi_name") %></td>
                            <td><%#Eval("thietbi_noidung") %></td>
                            <td><%#Eval("thietbi_note") %></td>
                            <td><%#Eval("thietbi_createdate") %></td>
                            <td><%#Eval("thietbi_ngaytiepnhan") %></td>
                            <td><%#Eval("thietbi_ngayquahan") %></td>
                            <td><%#Eval("nhomthietbi_name") %></td>
                            <td <%#Eval("thietbi_color") %>><%#Eval("thietbi_status") %></td>
                            <td><a id="btnGui" class="btn btn-primary text-white" onclick="myGui(<%#Eval("thietbi_id") %>)">Tiếp nhận</a></td>
                        </tr>
                    </ItemTemplate>
                </asp:Repeater>
            </tbody>
        </table>
        <div style="display: none">
            <input type="text" id="txt_ThietBi_ID" runat="server" />
            <a href="#" id="btnChuyenNhanVien" runat="server" onserverclick="btnDuyet_Click"></a>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

