﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_ThietBi_HuHong_Duyet : System.Web.UI.Page
{
    public int STT = 1;
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    string image;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["_id"] = 0;
        }
        loadData();
    }
    private void loadData()
    {
        var getData = from n in db.tbQuanTri_NhomThietBiNhaTruongs
                      join tb in db.tbQuanTri_ThietBiNhaTruongs on n.nhomthietbi_id equals tb.nhomthietbi_id
                      join u in db.admin_Users on tb.username_id equals u.username_id
                      where tb.thietbi_status !="Hoàn thành"
                      orderby tb.thietbi_id descending
                      select new
                      {
                          n.nhomthietbi_name,
                          tb.thietbi_id,
                          tb.thietbi_name,
                          tb.thietbi_noidung,
                          tb.thietbi_note,
                          tb.thietbi_createdate,
                          tb.thietbi_ngaytiepnhan,
                          u.username_fullname,
                          tb.thietbi_status,
                          color = tb.thietbi_color,
                          tb.thietbi_ngayquahan,
                          tb.thietbi_nguoinhansua,
                          thietbi_color = tb.thietbi_ngayquahan< DateTime.Now?"style=background-color:red;color:white":""
                      };
        rpList.DataSource = getData;
        rpList.DataBind();
        //ddlloaisanpham.DataSource = from tb in db.tbQuanTri_NhomThietBiNhaTruongs
        //                            select tb;
        //ddlloaisanpham.DataBind();
        if (!IsPostBack)
        {
            var listNV = from u in db.admin_Users where u.username_id == 120 || u.username_id == 52 || u.username_id == 45 || u.username_id==28 select u;
            ddlNhanVien.Items.Clear();
            ddlNhanVien.Items.Insert(0, "Chọn");
            ddlNhanVien.AppendDataBoundItems = true;
            ddlNhanVien.DataTextField = "username_fullname";
            ddlNhanVien.DataValueField = "username_id";
            ddlNhanVien.DataSource = listNV;
            ddlNhanVien.DataBind();
            ddlTinhTrang.DataSource = from tt in db.tbQuanTri_NhomThietBiNhaTruongs select tt;
            ddlTinhTrang.DataTextField = "nhomthietbi_name";
            ddlTinhTrang.DataValueField = "nhomthietbi_id";
            ddlTinhTrang.DataBind();
        }
    }

    protected void btnThem_Click(object sender, EventArgs e)
    {
        Session["_id"] = 0;
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupControl.Show();", true);
    }
    protected void btnXoa_Click(object sender, EventArgs e)
    {

    }
    protected void btnDuyet_Click(object sender, EventArgs e)
    {
        try
        {
            tbQuanTri_ThietBiNhaTruong updateDuyet = (from d in db.tbQuanTri_ThietBiNhaTruongs where d.thietbi_id == Convert.ToInt32(txt_ThietBi_ID.Value) select d).SingleOrDefault();
            updateDuyet.nhomthietbi_id = Convert.ToInt32(ddlTinhTrang.SelectedValue);
            if (updateDuyet.nhomthietbi_id == 1)
            {
                updateDuyet.thietbi_ngaytiepnhan = DateTime.Now;
                updateDuyet.thietbi_ngayquahan = DateTime.Now.AddDays(1);
            }
            if (updateDuyet.nhomthietbi_id == 2)
            {
                updateDuyet.thietbi_ngaytiepnhan = DateTime.Now;
                updateDuyet.thietbi_ngayquahan = DateTime.Now.AddDays(3);
            }
            if (updateDuyet.nhomthietbi_id == 4)
            {
                updateDuyet.thietbi_ngaytiepnhan = DateTime.Now;
                updateDuyet.thietbi_ngayquahan = DateTime.Now.AddDays(5);
            }
            if (updateDuyet.nhomthietbi_id == 5)
            {
                updateDuyet.thietbi_ngaytiepnhan = DateTime.Now;
                updateDuyet.thietbi_ngayquahan = DateTime.Now.AddDays(7);
            }
            updateDuyet.thietbi_status = ddlNhanVien.SelectedItem.Text + " đã tiếp nhận";
            updateDuyet.thietbi_nguoinhansua = ddlNhanVien.SelectedItem.Text;
            db.SubmitChanges();
            admin_User getUser = (from u in db.admin_Users where u.username_id == Convert.ToInt32(ddlNhanVien.SelectedValue) select u).SingleOrDefault();
            //SendMail(getUser.username_email);

            //alert.alert_Update(Page, "Đã gửi thông báo tới " + ddlNhanVien.SelectedItem.Text, "");
            alert.alert_Update(Page, "Đã tiếp nhận", "");
            loadData();
        }
        catch(Exception ex)
        {
            alert.alert_Error(Page, "Lỗi", "Vui lòng liên hệ IT");
        }
    }

    private bool SendMail(string email)
    {

        if (email != "")
        {
            try
            {
                var fromAddress = "thongbaovietnhatschool@gmail.com";//  Email Address from where you send the mail 
                var toAddress = email;
                const string fromPassword = "neiabcekdjluofid";
                string subject, title;
                title = "Thông báo";
                subject = "<!DOCTYPE html><html><head><title></title></head><body ><div>" +
                "<h3 style=\"margin-top:0px; text-align:center; color:#029ada\">Bạn nhận được thông tin báo hỏng thiết bị từ Cô Nguyên. Xem chi tiết <a href='http://quantri.vietnhatschool.edu.vn/admin-nhan-xu-ly-thiet-bi-hu-hong'>tại đây.</a></h3>" +
                "</div></body></html>";
                var smtp = new System.Net.Mail.SmtpClient();
                {
                    smtp.Host = "smtp.gmail.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;
                    smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    smtp.Credentials = new NetworkCredential(fromAddress, fromPassword);
                    smtp.Timeout = 20000;
                }
                MailMessage mm = new MailMessage();
                mm.From = new MailAddress(fromAddress, "Trường Liên cấp Việt Nhật");
                mm.Subject = title;
                mm.To.Add(toAddress);
                mm.IsBodyHtml = true;
                mm.Body = subject;
                smtp.Send(mm);
                return true;
            }
            catch
            {
                return false;
            }
        }
        else
            return false;
    }
}