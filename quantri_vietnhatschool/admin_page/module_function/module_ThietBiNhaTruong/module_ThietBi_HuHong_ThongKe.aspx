﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_ThietBi_HuHong_ThongKe.aspx.cs" Inherits="admin_page_module_function_module_ThietBi_HuHong_ThongKe" %>

<%@ Register TagPrefix="dx" Namespace="DevExpress.Web" Assembly="DevExpress.Web.v17.1" %>
<%@ Register Assembly="DevExpress.Web.ASPxHtmlEditor.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxHtmlEditor" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.ASPxSpellChecker.v17.1, Version=17.1.3.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web.ASPxSpellChecker" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <style>
        a .block_ThongKe {
            text-decoration: none;
        }

        .block_ThongKe {
            border: 1px solid #808080;
            width: 165px;
            height: 59px;
            text-align: center;
            background-color: green;
            color: white;
            padding: 10px;
            font-weight: bold;
        }

        a:hover .block_ThongKe {
            background-color: red;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <script type="text/javascript">
        function func() {
            grvList.Refresh();
        }
    </script>
    <div class="card card-block">

        <div class="col-2">
            <a href="admin-thong-ke-thiet-bi-hu-hong" id="btnRi" runat="server" onserverclick="btnRi_ServerClick">
                <div class="block_ThongKe">
                    Đặng Văn Ri
                            <br />
                    <%=count_HoanThanhRi %>/ <%=count_TongRi %>
                </div>
            </a>
        </div>
        <div class="col-2">
            <a href="admin-thong-ke-thiet-bi-hu-hong" id="btnAnh" runat="server" onserverclick="btnAnh_ServerClick">
                <div class="block_ThongKe">
                   Huỳnh Ngọc Ánh
                            <br />
                    <%=count_HoanThanhAnh %>/ <%=count_TongAnh %>
                </div>
            </a>
        </div>
        <div class="col-2" >
            <a href="admin-thong-ke-thiet-bi-hu-hong" id="btnTung" runat="server" onserverclick="btnTung_ServerClick">
                <div class="block_ThongKe">
                    Trương Thanh Tùng
                            <br />
                    <%=count_HoanThanhTung %>/ <%=count_TongTung %>
                </div>
            </a>
        </div>
        <div class="col-2">
            <a href="admin-thong-ke-thiet-bi-hu-hong" id="btnToa" runat="server" onserverclick="btnToa_ServerClick">
                <div class="block_ThongKe">
                    Trương Công Tỏa
                            <br />
                    <%=count_HoanThanhToa %>/ <%=count_TongToa %>
                </div>
            </a>
        </div>
         <div class="col-2">
            <a href="admin-thong-ke-thiet-bi-hu-hong" id="btnCong" runat="server" onserverclick="btnCong_ServerClick">
                <div class="block_ThongKe">
                   Công
                            <br />
                    <%=count_HoanThanhCong %>/ <%=count_TongCong %>
                </div>
            </a>
        </div>
    </div>

    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nhân viên nhận sửa</th>
                <th scope="col">Giáo viên</th>
                <th scope="col">Lớp</th>
                <th scope="col">Thiết bị hư hỏng</th>
                <th scope="col">Ghi chú</th>
                <th scope="col">Ngày bắt đầu</th>
                <th scope="col">Ngày hết hạn</th>
                <th scope="col">Cần xử lý</th>
                <th scope="col">Tình trạng</th>
            </tr>
        </thead>
        <tbody>
            <asp:Repeater ID="rpList" runat="server">
                <ItemTemplate>
                    <tr>
                        <th scope="row"><%=STT++ %></th>
                        <td><%#Eval("thietbi_nguoinhansua") %></td>
                        <td><%#Eval("username_fullname") %></td>
                        <td><%#Eval("thietbi_name") %></td>
                        <td><%#Eval("thietbi_noidung") %></td>
                        <td><%#Eval("thietbi_note") %></td>
                        <td><%#Eval("thietbi_createdate") %></td>
                        <td><%#Eval("thietbi_ngayquahan") %></td>
                        <td><%#Eval("nhomthietbi_name") %></td>
                        <td><%#Eval("thietbi_status") %></td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </tbody>
    </table>

</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

