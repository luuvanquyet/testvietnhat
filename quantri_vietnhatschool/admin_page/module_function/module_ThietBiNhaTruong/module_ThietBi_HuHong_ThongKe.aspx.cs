﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_ThietBi_HuHong_ThongKe : System.Web.UI.Page
{
    public int STT = 1;
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    string image;
    public string count_TongRi, count_HoanThanhRi, count_TongAnh, count_HoanThanhAnh, count_TongTung, count_HoanThanhTung, count_HoanThanhToa, count_TongToa, count_HoanThanhCong, count_TongCong;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["_id"] = 0;
        }
     
        count_TongRi = (from tk in db.tbQuanTri_ThietBiNhaTruongs
                        where tk.thietbi_nguoinhansua.Contains("Ri")
                        select tk).Count() + "";
        count_HoanThanhRi = (from tk in db.tbQuanTri_ThietBiNhaTruongs
                             where tk.thietbi_nguoinhansua.Contains("Ri") && tk.thietbi_status == "Hoàn thành"
                             select tk).Count() + "";
        count_TongAnh = (from tk in db.tbQuanTri_ThietBiNhaTruongs
                         where tk.thietbi_nguoinhansua.Contains("Ánh")
                         select tk).Count() + "";
        count_HoanThanhAnh = (from tk in db.tbQuanTri_ThietBiNhaTruongs
                              where tk.thietbi_nguoinhansua.Contains("Ánh") && tk.thietbi_status == "Hoàn thành"
                              select tk).Count() + "";
        count_TongTung = (from tk in db.tbQuanTri_ThietBiNhaTruongs
                          where tk.thietbi_nguoinhansua.Contains("Tùng")
                          select tk).Count() + "";
        count_HoanThanhTung = (from tk in db.tbQuanTri_ThietBiNhaTruongs
                               where tk.thietbi_nguoinhansua.Contains("Tùng") && tk.thietbi_status == "Hoàn thành"
                               select tk).Count() + "";
        count_TongToa = (from tk in db.tbQuanTri_ThietBiNhaTruongs
                          where tk.thietbi_nguoinhansua.Contains("Tỏa")
                          select tk).Count() + "";
        count_HoanThanhToa = (from tk in db.tbQuanTri_ThietBiNhaTruongs
                               where tk.thietbi_nguoinhansua.Contains("Tỏa") && tk.thietbi_status == "Hoàn thành"
                               select tk).Count() + "";
        count_TongCong = (from tk in db.tbQuanTri_ThietBiNhaTruongs
                         where tk.thietbi_nguoinhansua.Contains("Công")
                         select tk).Count() + "";
        count_HoanThanhCong = (from tk in db.tbQuanTri_ThietBiNhaTruongs
                              where tk.thietbi_nguoinhansua.Contains("Công") && tk.thietbi_status == "Hoàn thành"
                              select tk).Count() + "";

    }
    private void loadData( string name)
    {
        var getData = from n in db.tbQuanTri_NhomThietBiNhaTruongs
                      join tb in db.tbQuanTri_ThietBiNhaTruongs on n.nhomthietbi_id equals tb.nhomthietbi_id
                      join u in db.admin_Users on tb.username_id equals u.username_id
                      where tb.thietbi_status == "Hoàn thành" && tb.thietbi_nguoinhansua.Contains(name)
                      orderby tb.thietbi_id descending
                      select new
                      {
                          n.nhomthietbi_name,
                          tb.thietbi_id,
                          tb.thietbi_name,
                          tb.thietbi_noidung,
                          tb.thietbi_note,
                          tb.thietbi_createdate,
                          u.username_fullname,
                          tb.thietbi_status,
                          color = tb.thietbi_color,
                          tb.thietbi_ngayquahan,
                          tb.thietbi_nguoinhansua
                      };
        rpList.DataSource = getData;
        rpList.DataBind();
        //ddlloaisanpham.DataSource = from tb in db.tbQuanTri_NhomThietBiNhaTruongs
        //                            select tb;
        //ddlloaisanpham.DataBind();
        if (!IsPostBack)
        {
            //var listNV = from u in db.admin_Users where u.username_id == 120 || u.username_id == 52 || u.username_id == 45 select u;
            //ddlNhanVien.Items.Clear();
            //ddlNhanVien.Items.Insert(0, "Chọn");
            //ddlNhanVien.AppendDataBoundItems = true;
            //ddlNhanVien.DataTextField = "username_fullname";
            //ddlNhanVien.DataValueField = "username_id";
            //ddlNhanVien.DataSource = listNV;
            //ddlNhanVien.DataBind();
            //ddlTinhTrang.DataSource = from tt in db.tbQuanTri_NhomThietBiNhaTruongs select tt;
            //ddlTinhTrang.DataTextField = "nhomthietbi_name";
            //ddlTinhTrang.DataValueField = "nhomthietbi_id";
            //ddlTinhTrang.DataBind();
        }
    }

    protected void btnThem_Click(object sender, EventArgs e)
    {
        Session["_id"] = 0;
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupControl.Show();", true);
    }
    protected void btnXoa_Click(object sender, EventArgs e)
    {

    }




    protected void btnRi_ServerClick(object sender, EventArgs e)
    {
        loadData("Ri");
    }

    protected void btnAnh_ServerClick(object sender, EventArgs e)
    {
        loadData("Anh");
    }

    protected void btnTung_ServerClick(object sender, EventArgs e)
    {
        loadData("Tùng");
    }

    protected void btnToa_ServerClick(object sender, EventArgs e)
    {
        loadData("Tỏa");
    }


    protected void btnCong_ServerClick(object sender, EventArgs e)
    {
        loadData("Công");
    }
}