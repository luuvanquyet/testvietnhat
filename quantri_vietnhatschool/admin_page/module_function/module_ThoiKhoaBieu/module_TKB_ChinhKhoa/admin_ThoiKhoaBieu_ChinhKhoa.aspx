﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="admin_ThoiKhoaBieu_ChinhKhoa.aspx.cs" Inherits="admin_page_module_function_module_ThoiKhoaBieu_module_TKB_Tiet8_admin_ThoiKhoaBieu_ChinhKhoa" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
    <link href="../../../admin_css/vendor.css" rel="stylesheet" />
    <link href="../../../css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="../../../css/bootstrap.min.css" rel="stylesheet" />
    <script src="../../../admin_js/sweetalert.min.js"></script>
    <script src="../../../js/jquery.min.js"></script>
    <script src="../../../js/jquery.dataTables.min.js"></script>
    <script src="../../../js/popper.min.js"></script>
    <script src="../../../js/bootstrap.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div style="float: right">
        <a href="/admin-home" class="btn btn-primary">Quay lại</a>
    </div>
    <%--Tabs and Pills--%>
    <div class="container">
        <div style="text-align: center; color: blue; font-weight: bold; font-size: 18px; margin-top: 24px; margin-bottom: 20px">THỜI KHÓA BIỂU TRƯỜNG LIÊN CẤP VIỆT NHẬT</div>
        <div style="justify-content: center; display: flex;">
            <ul class="nav nav-pills mb-3 nav__tkb" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link btn btn-outline-primary" id="pills__k0" data-toggle="pill" href="#pills-k0" role="tab" aria-controls="pills-home1" aria-selected="true">TKB Tổng</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link btn btn-outline-primary" id="pills__k1" data-toggle="pill" href="#pills-k1" role="tab" aria-controls="pills-home" aria-selected="true">Khối 1</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link btn btn-outline-primary" id="pills__k2" data-toggle="pill" href="#pills-k2" role="tab" aria-controls="pills-profile" aria-selected="false">Khối 2</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link btn btn-outline-primary" id="pills__k3" data-toggle="pill" href="#pills-k3" role="tab" aria-controls="pills-contact" aria-selected="false">Khối 3</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link btn btn-outline-primary" id="pills__k4" data-toggle="pill" href="#pills-k4" role="tab" aria-controls="pills-contact" aria-selected="false">Khối 4</a>
                </li>
                  <li class="nav-item">
                    <a class="nav-link btn btn-outline-primary" id="pills__k5" data-toggle="pill" href="#pills-k5" role="tab" aria-controls="pills-contact" aria-selected="false">Khối 5</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link btn btn-outline-primary" id="pills__kTHCS" data-toggle="pill" href="#pills-kTHCS" role="tab" aria-controls="pills-contact" aria-selected="false">Khối THCS</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link btn btn-outline-primary" id="pills__kTHPT" data-toggle="pill" href="#pills-kTHPT" role="tab" aria-controls="pills-contact" aria-selected="false">Khối THPT</a>
                </li>
            </ul>
        </div>
    </div>

    <!-- Trigger/Open The Modal -->
<%--<button id="myBtn">Open Modal</button>--%>
    <div id="myModal" class="modal">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close">&times;</span>
  </div>

</div>
        <script>
            // Get the modal
            var modal = document.getElementById("myModal");

            // Get the button that opens the modal
            var btn = document.getElementById("pills__k0");

            // Get the <span> element that closes the modal
            var span = document.getElementsByClassName("close")[0];

            // When the user clicks on the button, open the modal
            btn.onclick = function () {
                modal.style.display = "block";
            }

            // When the user clicks on <span> (x), close the modal
            span.onclick = function () {
                modal.style.display = "none";
            }

            // When the user clicks anywhere outside of the modal, close it
            window.onclick = function (event) {
                if (event.target == modal) {
                    modal.style.display = "none";
                }
            }
        </script>
    <style>
        /* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 28; /* Sit on top */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content/Box */
.modal-content {
  z-index: 1000;
    background-color: #fefefe;
    /* margin: 15% auto; */
    padding: 20px;
    border: 1px solid #888;
    width: 100%;
    height: 100%;
}

/* The Close Button */
.close {
  text-align: end;
  color: #020aff;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: black;
  text-decoration: none;
  cursor: pointer;
}
    </style>

    <div class="container">
        <div style="text-align: center" class="tab-content tab__tkb" id="pills-tabContent">
            <div class="tab-pane fade show" id="pills-k1" role="tabpanel" aria-labelledby="pills__k1">
                <img src="../../../../admin_images/tbk__images/TKB-khoi1.png" class="img__tkb" alt="Thời Khóa Biểu Khối 1" />
            </div>
            <div class="tab-pane fade" id="pills-k2" role="tabpanel" aria-labelledby="pills__k2">
                <img src="../../../../admin_images/tbk__images/TKB-khoi2.png" class="img__tkb" alt="Thời Khóa Biểu Khối 2" />
            </div>
            <div class="tab-pane fade" id="pills-k3" role="tabpanel" aria-labelledby="pills__k3">
                <img src="../../../../admin_images/tbk__images/TKB-khoi3.png" class="img__tkb" alt="Thời Khóa Biểu Khối 3" />
            </div>
            <div class="tab-pane fade" id="pills-k4" role="tabpanel" aria-labelledby="pills__k4">
                <img src="../../../../admin_images/tbk__images/TKB-khoi4.png" class="img__tkb" alt="Thời Khóa Biểu Khối 4" />
            </div>
            <div class="tab-pane fade" id="pills-k5" role="tabpanel" aria-labelledby="pills__k5">
                <img src="../../../../admin_images/tbk__images/TKB-Khoi5.png" class="img__tkb" alt="Thời Khóa Biểu Khối 5" />
            </div>
            <div class="tab-pane fade" id="pills-kTHCS" role="tabpanel" aria-labelledby="pills__kTHCS">
                <img src="../../../../admin_images/tbk__images/TKB-THCS.png" class="img__tkb" alt="Thời Khóa Biểu THCS" />
            </div>
             <div class="tab-pane fade" id="pills-kTHPT" role="tabpanel" aria-labelledby="pills__kTHPT">
                <img src="../../../../admin_images/tbk__images/TKB-THPT.png" class="img__tkb" alt="Thời Khóa Biểu THPT" />
            </div>
        </div>
    </div>
    <div style="text-align: center">
        <img src="../../../../admin_images/tbk__images/magv2021-2022.png" />
    </div>

</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

