﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_ThuKho_BanHang : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    private int stt = 1;
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {
        loaddata();
    }
    private void loaddata()
    {
        Session["spChiTiet"] = null;
        var getdata = from nh in db.tbThuKho_BanHangs
                      join u in db.admin_Users on nh.username_id equals u.username_id
                      orderby nh.banhang_id descending
                      select new { 
                       nh.banhang_code,
                       nh.banhang_id,
                       u.username_fullname,
                       nh.banhang_creatdate
                      };
        grvList.DataSource = getdata;
        grvList.DataBind();
    }
    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "banhang_id" }));
        Response.Redirect("admin-ban-hang-chi-tiet-" + _id);
    }
}