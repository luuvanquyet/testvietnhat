﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_ThuKho_BanHangInsert : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public string adminName;
    DataTable dtProduct;
    public int stt = 1;
    int masp;
    string tensp;
    int sl;
    int gianhap;
    int thanhtien;
    cls_Alert alert = new cls_Alert();
    protected void Page_Load(object sender, EventArgs e)
    {

        var list = from sp in db.tbThuKho_SanPhams
                   join kh in db.tbThuKho_TonKhos on sp.sanpham_id equals kh.sanpham_id
                   select new
                   {
                       sp.sanpham_id,
                       sp.sanpham_name,
                       kh.tonkho_soluong
                   };
        grvList.DataSource = list;
        grvList.DataBind();
        //lấy thông tin của tk đang nhập
        var getuser = (from u in db.admin_Users
                       where u.username_username == Request.Cookies["UserName"].Value
                       select u).FirstOrDefault();
        txtNhanVien.Value = getuser.username_fullname;
        string matutang = Matutang();
        txtMaNhap.Value = matutang;
        loaddatatable();
        dtProduct = (DataTable)Session["spChiTiet"];
        rp_grvChiTiet.DataSource = dtProduct;
        rp_grvChiTiet.DataBind();
        //hiện ngày tháng 
        //var new_day = DateTime.Now.ToString("dd/MM/yyyy");
        txtNgayNhap.Value = DateTime.Now.ToString("dd/MM/yyyy");

    }
    //Hàm tự tăng
    public string Matutang()
    {
        int year = DateTime.Now.Year;
        var list = from nk in db.tbThuKho_BanHangs select nk;
        string s = "BH";
        if (list.Count() <= 0)
            s = "BH00001";
        else
        {
            var list1 = from nk in db.tbThuKho_BanHangs orderby nk.banhang_code descending select nk;
            string chuoi = list1.First().banhang_code;
            int k;
            k = Convert.ToInt32(chuoi.Substring(2, 5));
            k = k + 1;
            if (k < 10) s = s + "0000";
            else if (k < 100)
                s = s + "000";
            else if (k < 1000)
                s = s + "00";
            else if (k < 10000)
                s = s + "0";
            s = s + k.ToString();
        }
        return s;
    }
    public void loaddatatable()
    {
        try
        {
            if (dtProduct == null)
            {
                dtProduct = new DataTable();
                dtProduct.Columns.Add("sanpham_id", typeof(int));
                dtProduct.Columns.Add("sanpham_name", typeof(string));
                dtProduct.Columns.Add("banhang_soluong", typeof(int));
            }
        }
        catch { }
    }
    public void loadGrv()
    {

    }
    protected void btnChiTiet_ServerClick(object sender, EventArgs e)
    {
        try
        {
            //kiểm tra add 2 lần có thêm vào gridview hay không
            int _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "sanpham_id" }));
            var checkSanPham = (from sp in db.tbThuKho_SanPhams where sp.sanpham_id == _id select sp).SingleOrDefault();
            if (Session["spChiTiet"] != null)
            {
                dtProduct = (DataTable)Session["spChiTiet"];
                DataRow[] row_id = dtProduct.Select("sanpham_id = '" + _id + "'");
                if (row_id.Length != 0)
                {
                    alert.alert_Warning(Page, "Sản phẩm này đã có trong danh sách nhập", "");
                }
                else
                {
                    DataRow row = dtProduct.NewRow();
                    row["sanpham_id"] = checkSanPham.sanpham_id;
                    row["sanpham_name"] = checkSanPham.sanpham_name;
                    row["banhang_soluong"] = 1;
                    dtProduct.Rows.Add(row);
                    Session["spChiTiet"] = dtProduct;
                }
            }
            else
            {
                loaddatatable();
                DataRow row = dtProduct.NewRow();
                row["sanpham_id"] = checkSanPham.sanpham_id;
                row["sanpham_name"] = checkSanPham.sanpham_name;
                row["banhang_soluong"] = 1;
                dtProduct.Rows.Add(row);
                Session["spChiTiet"] = dtProduct;
            }
            rp_grvChiTiet.DataSource = dtProduct;
            rp_grvChiTiet.DataBind();
            btnSave.Visible = true;
        }
        catch { }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        dtProduct = (DataTable)Session["spChiTiet"];
        try
        {
            if (dtProduct.Rows.Count <= 0 || dtProduct == null) alert.alert_Warning(Page, "Bạn chưa có sản phẩm nào", "");
        }
        catch { }
        if (dtProduct == null) alert.alert_Warning(Page, "Bạn chưa có sản phẩm nào", "");
        else
        {
            
                // lưu dữ liệu vào bảng nhập hàng
                try
                {
                    if (dtProduct.Rows.Count > 0)
                    {
                        tbThuKho_BanHang insertNH = new tbThuKho_BanHang();
                        insertNH.banhang_code = txtMaNhap.Value;
                        insertNH.banhang_creatdate = DateTime.Now;
                        insertNH.username_id = (from u in db.admin_Users where u.username_username== Request.Cookies["UserName"].Value select u).SingleOrDefault().username_id;
                        db.tbThuKho_BanHangs.InsertOnSubmit(insertNH);
                        db.SubmitChanges();
                        //lưu dữ liệu nhập hàng chi tiết trong table vào datatable
                        foreach (DataRow row in dtProduct.Rows)
                        {
                            var checkNhapHang = from nh in db.tbThuKho_BanHang_ChiTiets where nh.banhang_code == txtMaNhap.Value select nh;
                            if (checkNhapHang != txtMaNhap)
                            {
                                tbThuKho_BanHang_ChiTiet insertNHCT = new tbThuKho_BanHang_ChiTiet();
                                insertNHCT.sanpham_id = Convert.ToInt32(row["sanpham_id"]);
                                insertNHCT.banhang_code = txtMaNhap.Value;
                                insertNHCT.banhang_soluong = Convert.ToInt32(row["banhang_soluong"]);
                                db.tbThuKho_BanHang_ChiTiets.InsertOnSubmit(insertNHCT);
                                db.SubmitChanges();
                                Session["spChiTiet"] = null;
                                dtProduct = (DataTable)Session["spChiTiet"];
                                rp_grvChiTiet.DataSource = dtProduct;
                                rp_grvChiTiet.DataBind();
                                //lưu vào bảng kho hàng
                                var checkkh = (from kh in db.tbThuKho_TonKhos
                                               where kh.sanpham_id == Convert.ToInt32(row["sanpham_id"])
                                               select kh).SingleOrDefault();
                                var checksp = (from sp in db.tbThuKho_SanPhams
                                               where sp.sanpham_id == Convert.ToInt32(row["sanpham_id"])
                                               select sp).SingleOrDefault();
                                if (checkkh == null)
                                {
                                    tbThuKho_TonKho insert_slsp = new tbThuKho_TonKho();
                                    insert_slsp.tonkho_soluong = Convert.ToInt32(row["banhang_soluong"]);
                                    insert_slsp.sanpham_id = Convert.ToInt32(row["sanpham_id"]);
                                    db.tbThuKho_TonKhos.InsertOnSubmit(insert_slsp);
                                    db.SubmitChanges();
                                }
                                else
                                {
                                    checkkh.tonkho_soluong = checkkh.tonkho_soluong - Convert.ToInt32(row["banhang_soluong"]);
                                    db.SubmitChanges();
                                }
                                //checksp.product_price_entry = Convert.ToInt32(row["ctnh_gianhap"]);
                                db.SubmitChanges();
                                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "AlertBox", "swal('Bản hàng thành công!', '','success').then(function(){window.location = '/admin-ban-hang';})", true);
                                //alert.alert_Success(Page, "Nhập sách thành công", "");
                            }
                        }
                    }
                }
                catch { }
                string matutang = Matutang();
                txtMaNhap.Value = matutang;
            
        }
    }

    protected void NhapHang_ServerClick(object sender, EventArgs e)
    {
        // kiểm tra id
        int _id = Convert.ToInt32(txt_ID.Value);
        if (Session["spChiTiet"] != null)
        {
            dtProduct = (DataTable)Session["spChiTiet"];
            // chạy foreach để lặp lại các row 
            foreach (DataRow row in dtProduct.Rows)
            {
                string product_id = row["sanpham_id"].ToString();
                if (product_id == _id.ToString())
                {
                    // lưu data bằng input đầu vào
                    row.SetField("banhang_soluong", txt_SoLuong.Value);
                    rp_grvChiTiet.DataSource = dtProduct;
                    rp_grvChiTiet.DataBind();
                }
            }
        }
    }

    protected void btnXoa_ServerClick(object sender, EventArgs e)
    {
        int _id = Convert.ToInt32(txt_ID.Value);
        dtProduct = (DataTable)Session["spChiTiet"];
        foreach (DataRow row in dtProduct.Rows)
        {
            string product_id = row["sanpham_id"].ToString();
            if (product_id == _id.ToString())
            {
                dtProduct.Rows.Remove(row);
                Session["spChiTiet"] = dtProduct;
                break;
            }
        }
        rp_grvChiTiet.DataSource = dtProduct;
        rp_grvChiTiet.DataBind();
        alert.alert_Success(Page, "Xóa thành công", "");
    }
}