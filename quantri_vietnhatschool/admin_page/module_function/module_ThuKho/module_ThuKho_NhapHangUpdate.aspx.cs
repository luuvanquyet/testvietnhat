﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_ThuKho_NhapHangUpdate : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public string adminName;
    DataTable dtProduct;
    public int stt = 1;
    int masp;
    string tensp;
    int sl;
    int gianhap;
    int thanhtien;
    cls_Alert alert = new cls_Alert();
    protected void Page_Load(object sender, EventArgs e)
    {
        var list = from sp in db.tbThuKho_SanPhams
                   //join kh in db.tbThuKho_TonKhos on sp.sanpham_id equals kh.sanpham_id
                   select new
                   {
                       sp.sanpham_id,
                       sp.sanpham_name,
                       //kh.tonkho_soluong
                   };
        grvList.DataSource = list;
        grvList.DataBind();
        if (!IsPostBack)
        {
            int _id = Convert.ToInt32(RouteData.Values["id"]);
            var getdataid = (from nh in db.tbThuKho_NhapHangs where nh.nhaphang_id == _id select nh).SingleOrDefault();
            txtMaNhap.Value = getdataid.nhaphang_code;
            txtNhanVien.Value = (from u in db.admin_Users where u.username_id == getdataid.username_id select u).SingleOrDefault().username_fullname;
            txtNgayNhap.Value = getdataid.nhaphang_createdate.Value.ToString("dd-MM-yyyy").Replace(' ', 'T');
            txtNoiDung.Value = getdataid.nhaphang_content;
            // nếu sestion chi tiet mà co du lieu roi thi khong cho chạy vao nua
            loaddatatable();
            //dtProduct = (DataTable)Session["spChiTiet"];
            //rp_grvChiTiet.DataSource = dtProduct;
            //rp_grvChiTiet.DataBind();
            if (RouteData.Values["id"] != null)
            {
                //get mã tự tăng của nhập hàng chi tiết
                var getctnh = (from ctnh in db.tbThuKho_NhapHang_ChiTiets
                               join product in db.tbThuKho_SanPhams on ctnh.sanpham_id equals product.sanpham_id
                               where getdataid.nhaphang_code == ctnh.nhaphang_code
                               select new
                               {
                                   ctnh.sanpham_id,
                                   product.sanpham_name,
                                   ctnh.nhaphang_chitiet_id,
                                   ctnh.nhaphang_soluong,
                               });
                //loaddata ra datatable
                foreach (var item in getctnh)
                {
                    DataRow row = dtProduct.NewRow();
                    row["sanpham_id"] = item.sanpham_id;
                    row["sanpham_name"] = item.sanpham_name;
                    row["nhaphang_soluong"] = item.nhaphang_soluong;
                    dtProduct.Rows.Add(row);
                };
                Session["spChiTiet"] = dtProduct;
                rp_grvChiTiet.DataSource = dtProduct;
                rp_grvChiTiet.DataBind();
            }
        }
    }

    public void loaddatatable()
    {
        if (dtProduct == null)
        {
            dtProduct = new DataTable();
            dtProduct.Columns.Add("sanpham_id", typeof(int));
            dtProduct.Columns.Add("sanpham_name", typeof(string));
            dtProduct.Columns.Add("nhaphang_soluong", typeof(int));
        }
    }
    protected void btnChiTiet_ServerClick(object sender, EventArgs e)
    {
        //kiểm tra add 2 lần có thêm vào gridview hay không
        int _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "sanpham_id" }));
        var checkSanPham = (from sp in db.tbThuKho_SanPhams where sp.sanpham_id == _id select sp).SingleOrDefault();
        if (Session["spChiTiet"] != null)
        {
            dtProduct = (DataTable)Session["spChiTiet"];
            DataRow[] row_id = dtProduct.Select("sanpham_id = '" + _id + "'");
            if (row_id.Length != 0)
            {
                alert.alert_Warning(Page, "Sản phẩm đã có", "");
            }
            else
            {
                DataRow row = dtProduct.NewRow();
                row["sanpham_id"] = checkSanPham.sanpham_id;
                row["sanpham_name"] = checkSanPham.sanpham_name;
                row["nhaphang_soluong"] = 1;
                dtProduct.Rows.Add(row);
                Session["spChiTiet"] = dtProduct;
            }
        }
        // insert lưu vào database

        //End inseert lưu vào database
        rp_grvChiTiet.DataSource = dtProduct;
        rp_grvChiTiet.DataBind();

    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        dtProduct = (DataTable)Session["spChiTiet"];
        try
        {
            if (dtProduct.Rows.Count <= 0 || dtProduct == null) alert.alert_Warning(Page, "Bạn chưa có sản phẩm nào", "");
        }
        catch { }
        if (dtProduct == null) alert.alert_Warning(Page, "Bạn chưa có sản phẩm nào", "");
        else
        {
            if (txtNoiDung.Value == "") alert.alert_Warning(Page, "Bạn chưa nhập nội dung", "");
            else
            {
                // lưu dữ liệu vào bảng nhập hàng
                try
                {
                    if (dtProduct.Rows.Count > 0)
                    {
                        // -------------------------thêm vào bảng chi tiết ----------------------------
                        foreach (DataRow row in dtProduct.Rows)
                        {
                            //kiểm tra bảng id ma tu tang co bang ma tu tang trong bang tu tang chi tiet hay khogn
                            //var checkNhapHang = from nh in db.tbThuKho_NhapHang_ChiTiets where nh.nhaphang_code == txtMaNhap.Value select nh;
                            // kiểm tra product này đã có trong bảng chi tiết
                            var checkprdt = (from ctnh in db.tbThuKho_NhapHang_ChiTiets
                                             where ctnh.nhaphang_code == txtMaNhap.Value
                                             where ctnh.sanpham_id == Convert.ToInt32(row["sanpham_id"])
                                             select ctnh);
                            //checkprdt kho hàng
                            var checkkh = (from kh in db.tbThuKho_TonKhos
                                           where kh.sanpham_id == Convert.ToInt32(row["sanpham_id"])
                                           select kh).SingleOrDefault();
                            //checkprdt sản phẩm trong kho hàng
                            //var checksp = (from sp in db.tbThuKho_SanPhams
                            //               where sp.sanpham_id == Convert.ToInt32(row["sanpham_id"])
                            //               select sp).SingleOrDefault();
                            //nếu sách này đã được nhập trước đó thì cập nhật lại
                            if (checkprdt.Count() > 0)
                            {
                                foreach (var product in checkprdt)
                                {
                                    ////nếu có sản phẩm này trong bản chi tiết nhập hàng thì mình update nó lại
                                    if (product.sanpham_id == Convert.ToInt32(row["sanpham_id"]))
                                    {
                                        //update lại bảng tồn kho
                                        if (checkkh.sanpham_id == Convert.ToInt32(row["sanpham_id"]))
                                        {
                                            //-----------------thêm vào bảng kho hàng---------------------------
                                            //nếu "số lượng lúc thay đổi" nhỏ hơn "số lượng ban đầu trong bảng chi tiết" thì trừ đi
                                            if (product.nhaphang_soluong > Convert.ToInt32(row["nhaphang_soluong"]))
                                            {
                                                checkkh.tonkho_soluong = checkkh.tonkho_soluong - (product.nhaphang_soluong - Convert.ToInt32(row["nhaphang_soluong"]));
                                                db.SubmitChanges();
                                            }
                                            //nếu "số lượng lúc thay đổi" lớn hơn "số lượng ban đầu trong bảng chi tiết" thì cộng lại
                                            if (product.nhaphang_soluong < Convert.ToInt32(row["nhaphang_soluong"]))
                                            {
                                                checkkh.tonkho_soluong = checkkh.tonkho_soluong + (Convert.ToInt32(row["nhaphang_soluong"]) - product.nhaphang_soluong);
                                                db.SubmitChanges();
                                            }
                                        }
                                        //update vào bảng chi tiết nhập sách
                                        product.nhaphang_soluong = Convert.ToInt32(row["nhaphang_soluong"]);
                                        db.SubmitChanges();
                                    }
                                }
                            }
                            else
                            {
                                // nếu chua thì mình insert vào
                                tbThuKho_NhapHang_ChiTiet insertNHCT = new tbThuKho_NhapHang_ChiTiet();
                                insertNHCT.sanpham_id = Convert.ToInt32(row["sanpham_id"]);
                                insertNHCT.nhaphang_code = txtMaNhap.Value;
                                insertNHCT.nhaphang_soluong = Convert.ToInt32(row["nhaphang_soluong"]);
                                db.tbThuKho_NhapHang_ChiTiets.InsertOnSubmit(insertNHCT);
                                db.SubmitChanges();
                                dtProduct = (DataTable)Session["spChiTiet"];
                                rp_grvChiTiet.DataSource = dtProduct;
                                rp_grvChiTiet.DataBind();
                                //-----------------thêm vào bảng kho hàng---------------------------
                                //lưu vào bảng kho hàng
                                if (checkkh == null)
                                {
                                    tbThuKho_TonKho insert_slsp = new tbThuKho_TonKho();
                                    insert_slsp.tonkho_soluong = Convert.ToInt32(row["nhaphang_soluong"]);
                                    insert_slsp.sanpham_id = Convert.ToInt32(row["sanpham_id"]);
                                    db.tbThuKho_TonKhos.InsertOnSubmit(insert_slsp);
                                    db.SubmitChanges();
                                }
                                else
                                {
                                    checkkh.tonkho_soluong = checkkh.tonkho_soluong + Convert.ToInt32(row["nhaphang_soluong"]);
                                    db.SubmitChanges();
                                }
                            }
                            //checksp.product_price_entry = Convert.ToInt32(row["ctnh_gianhap"]);
                            db.SubmitChanges();
                        }
                    }
                    int _id = Convert.ToInt32(RouteData.Values["id"]);
                    var getdataid = (from nh in db.tbThuVien_NhapSaches where nh.nhapsach_id == _id select nh).SingleOrDefault();
                    getdataid.nhapsach_content = txtNoiDung.Value;
                    db.SubmitChanges();
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "AlertBox", "swal('Nhập hàng thành công!', '','success').then(function(){window.location = '/admin-nhap-hang';})", true);
                }
                catch { }
            }
        }
    }

    protected void NhapHang_ServerClick(object sender, EventArgs e)
    {
        // kiểm tra id
        int _id = Convert.ToInt32(txt_ID.Value);
        if (Session["spChiTiet"] != null)
        {
            dtProduct = (DataTable)Session["spChiTiet"];
            // chạy foreach để lặp lại các row 
            foreach (DataRow row in dtProduct.Rows)
            {
                string product_id = row["sanpham_id"].ToString();
                if (product_id == _id.ToString())
                {
                    // lưu data bằng input đầu vào
                    row.SetField("nhaphang_soluong", txt_SoLuong.Value);
                    rp_grvChiTiet.DataSource = dtProduct;
                    rp_grvChiTiet.DataBind();
                }
            }
        }
    }

    protected void btnXoa_ServerClick(object sender, EventArgs e)
    {
        int _id = Convert.ToInt32(txt_ID.Value);
        dtProduct = (DataTable)Session["spChiTiet"];
        foreach (DataRow row in dtProduct.Rows)
        {
            // xóa trong table 
            var checkprdt = (from ctnh in db.tbThuKho_NhapHang_ChiTiets
                             where ctnh.nhaphang_code == txtMaNhap.Value
                             where ctnh.sanpham_id == Convert.ToInt32(txt_ID.Value)
                             select ctnh);
            //checkprdt kho hàng
            var checkkh = (from kh in db.tbThuKho_TonKhos
                           where kh.sanpham_id == Convert.ToInt32(row["sanpham_id"])
                           select kh).SingleOrDefault();
            //checkprdt sản phẩm trong kho hàng
            var checksp = (from sp in db.tbThuKho_SanPhams
                           where sp.sanpham_id == Convert.ToInt32(row["sanpham_id"])
                           select sp).SingleOrDefault();
            // kiểm tra khi xóa thì sẽ  xóa luốn số lượng trong kho hàng đi.
            string sanpham_id = row["sanpham_id"].ToString();

            if (checkprdt.Count() > 0)
            {
                foreach (var product in checkprdt)
                {
                    if (checkkh != null)
                    {
                        //nếu "số lượng lúc thay đổi" nhỏ hơn "số lượng ban đầu trong bảng chi tiết" thì trừ đi
                        if (product.sanpham_id == Convert.ToInt32(row["sanpham_id"]))
                        {
                            checkkh.tonkho_soluong = checkkh.tonkho_soluong - (product.nhaphang_soluong);
                            db.SubmitChanges();
                            db.tbThuKho_NhapHang_ChiTiets.DeleteAllOnSubmit(checkprdt);
                            db.SubmitChanges();
                        }
                    }
                }
            }
            if (sanpham_id == _id.ToString())
            {
                dtProduct.Rows.Remove(row);
                Session["spChiTiet"] = dtProduct;
                break;
            }
        }
        rp_grvChiTiet.DataSource = dtProduct;
        rp_grvChiTiet.DataBind();
        alert.alert_Success(Page, "Xóa Thành Công", "");
    }
}