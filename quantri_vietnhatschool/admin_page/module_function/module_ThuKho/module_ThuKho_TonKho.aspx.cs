﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_ThuKho_TonKho : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    private int stt = 1;
    private int _id;
    protected void Page_Load(object sender, EventArgs e)
    {
        loaddata();
    }
    private void loaddata()
    {
        var getdata = from nh in db.tbThuKho_TonKhos
                      join u in db.tbThuKho_SanPhams on nh.sanpham_id equals u.sanpham_id
                      select new
                      {
                          nh.tonkho_id,
                          u.sanpham_name,
                          nh.tonkho_soluong,
                          nh.tonkho_min
                      };
        grvList.DataSource = getdata;
        grvList.DataBind();
    }

}