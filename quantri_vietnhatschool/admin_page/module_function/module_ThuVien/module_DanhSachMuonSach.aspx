﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_DanhSachMuonSach.aspx.cs" Inherits="admin_page_module_function_module_DanhSachMuonSach" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="container card card-block">
        <div class="form-group row">
            <div class="col-sm-10">
                <span><a href="../../../admin-muon-sach" class="btn btn-primary">Mượn sách mới</a></span>
                <asp:Button CssClass="btn btn-primary" ID="btnChiTiet" runat="server" OnClick="btnChiTiet_Click" Text="Chi tiết"></asp:Button>
                <asp:Button CssClass="btn btn-primary" ID="btnTraSach" runat="server" OnClick="btnTraSach_Click" Text="Trả sách"></asp:Button>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <div class="col-6">
                    <asp:UpdatePanel ID="upListProduct" runat="server">
                        <ContentTemplate>
                            <label>Giáo viên mượn sách</label>
                            <dx:ASPxGridView ID="grvListGV" runat="server" CssClass="table-hover" ClientInstanceName="grvListGV" KeyFieldName="thuvien_booksach_id" Width="90%">
                                <Columns>
                                    <dx:GridViewCommandColumn ShowSelectCheckbox="True" SelectAllCheckboxMode="Page" VisibleIndex="0" Width="5%">
                                    </dx:GridViewCommandColumn>
                                    <dx:GridViewDataColumn Caption="Họ và tên" FieldName="khachhang_name" HeaderStyle-HorizontalAlign="Center" Width="70%" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="Ngày mượn sách" FieldName="thuvien_booksach_tungay" HeaderStyle-HorizontalAlign="Center" Width="70%" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="Ngày hẹn trả" FieldName="thuvien_booksach_denngay" HeaderStyle-HorizontalAlign="Center" Width="20%" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="Quá hạn" FieldName="thuvien_booksach_quahan" HeaderStyle-HorizontalAlign="Center" Width="20%" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>
                                </Columns>
                                <SettingsSearchPanel Visible="true" />
                                <SettingsBehavior AllowFocusedRow="true" />
                                <SettingsText EmptyDataRow="Trống" SearchPanelEditorNullText="Gõ từ cần tìm kiếm và enter..." />
                                <SettingsLoadingPanel Text="Đang tải..." />
                                <SettingsPager PageSize="10" Summary-Text="Trang {0} / {1} ({2} trang)"></SettingsPager>
                            </dx:ASPxGridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <div class="form-group ">
                <div class="col-6">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <label>Học sinh mượn sách</label>
                            <dx:ASPxGridView ID="grvListHS" runat="server" CssClass="table-hover" ClientInstanceName="grvListHS" KeyFieldName="thuvien_booksach_id" Width="90%" >
                                <Columns>
                                    <dx:GridViewCommandColumn ShowSelectCheckbox="True" SelectAllCheckboxMode="Page" VisibleIndex="0" Width="5%">
                                    </dx:GridViewCommandColumn>
                                    <dx:GridViewDataColumn Caption="Họ và tên" FieldName="khachhang_name" HeaderStyle-HorizontalAlign="Center" Width="70%" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="Ngày mượn sách" FieldName="thuvien_booksach_tungay" HeaderStyle-HorizontalAlign="Center" Width="70%" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="Ngày hẹn trả" FieldName="thuvien_booksach_denngay" HeaderStyle-HorizontalAlign="Center" Width="20%" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>
                                    <dx:GridViewDataColumn Caption="Quá hạn" FieldName="thuvien_booksach_quahan" HeaderStyle-HorizontalAlign="Center" Width="20%" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>
                                </Columns>
                                <SettingsSearchPanel Visible="true" />
                                <SettingsBehavior AllowFocusedRow="true" />
                                <SettingsText EmptyDataRow="Trống" SearchPanelEditorNullText="Gõ từ cần tìm kiếm và enter..." />
                                <SettingsLoadingPanel Text="Đang tải..." />
                                <SettingsPager PageSize="10" Summary-Text="Trang {0} / {1} ({2} trang)"></SettingsPager>
                            </dx:ASPxGridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

