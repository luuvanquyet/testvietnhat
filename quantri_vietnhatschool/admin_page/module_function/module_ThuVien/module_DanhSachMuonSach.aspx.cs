﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_DanhSachMuonSach : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    private int _id;
    cls_Alert alert = new cls_Alert();
    protected void Page_Load(object sender, EventArgs e)
    {
        //tính ngày quá hạn
        DateTime toDay = DateTime.Now;
        var getdata = from bs in db.tbThuVien_BookSaches
                      select bs;
        foreach (var item in getdata)
        {
            //var getNgay = from d in db.tbThuVien_BookSaches
            //              where d.thuvien_booksach_id == Convert.ToInt32(item)
            //              select d.thuvien_booksach_quahan;
            DateTime day = Convert.ToDateTime(item.thuvien_booksach_denngay);
            TimeSpan interval = toDay.Subtract(day);
            int a = int.Parse(interval.Days.ToString());
            if (a >0)
            {
                item.thuvien_booksach_quahan = a + " ngày";
            }
            db.SubmitChanges();
        }
        //load danh sách giáo viên mượn sách
        var listGV = from gv in db.tbThuVien_BookSaches
                     where gv.thuvien_bophan == 1 && gv.hidden== false && gv.active==true
                     orderby gv.thuvien_booksach_quahan descending
                     select gv;
        grvListGV.DataSource = listGV;
        grvListGV.DataBind();
        //load danh sách học sinh mượn sách
        var listHS = from gv in db.tbThuVien_BookSaches
                     where gv.thuvien_bophan == 2 && gv.hidden == false && gv.active == true
                     orderby gv.thuvien_booksach_quahan descending
                     select gv;
        grvListHS.DataSource = listHS;
        grvListHS.DataBind();

    }

    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        List<object> selectedGV = grvListGV.GetSelectedFieldValues(new string[] { "thuvien_booksach_id" });
        List<object> selectedHS = grvListHS.GetSelectedFieldValues(new string[] { "thuvien_booksach_id" });
        if (selectedGV.Count <= 0 && selectedHS.Count <= 0)
        {
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu để xem chi tiết", "");
        }
        else if (selectedGV.Count == 1 && selectedHS.Count == 0)
        {
            foreach (var item in selectedGV)
            {
                _id = Convert.ToInt32(item);
            }
            Response.Redirect("admin-muon-sach-" + _id);
        }
        else if (selectedGV.Count == 0 && selectedHS.Count == 1)
        {
            foreach (var item1 in selectedHS)
            {
                _id = Convert.ToInt32(item1);
            }
            Response.Redirect("admin-muon-sach-" + _id);
        }
        else
        {
            alert.alert_Warning(Page, "Bạn chỉ được chọn 1 dữ liệu để xem chi tiết", "");
        }

    }

    protected void btnTraSach_Click(object sender, EventArgs e)
    {
        List<object> selectedGV = grvListGV.GetSelectedFieldValues(new string[] { "thuvien_booksach_id" });
        List<object> selectedHS = grvListHS.GetSelectedFieldValues(new string[] { "thuvien_booksach_id" });
        if (selectedGV.Count <= 0 && selectedHS.Count <= 0)
        {
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu để trả", "");
        }
        else if (selectedGV.Count == 1 && selectedHS.Count == 0)
        {
            foreach (var item in selectedGV)
            {
                _id = Convert.ToInt32(item);
            }
            Response.Redirect("admin-tra-sach-" + _id);
        }
        else if (selectedGV.Count == 0 && selectedHS.Count == 1)
        {
            foreach (var item1 in selectedHS)
            {
                _id = Convert.ToInt32(item1);
            }
            Response.Redirect("admin-tra-sach-" + _id);
        }
        else
        {
            alert.alert_Warning(Page, "Bạn chỉ được chọn 1 dữ liệu để trả", "");
        }
    }
}