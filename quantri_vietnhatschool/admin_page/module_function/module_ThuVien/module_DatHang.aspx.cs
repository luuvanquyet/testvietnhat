﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_DatHang : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public string adminName;
    DataTable dtProduct;
    public int stt = 1;
    int masp;
    string tensp;
    int sl;
    int gianhap;
    int thanhtien;
    cls_Alert alert = new cls_Alert();
    protected void Page_Load(object sender, EventArgs e)
    {
        var list = from kh in db.tbThuVien_TonKhos
                   join sp in db.tbThuVien_Saches on kh.thuvien_sach_id equals sp.thuvien_sach_id
                   where kh.tonkho_soluong > 0
                   select new
                   {
                       kh.tonkho_id,
                       sp.thuvien_sach_name,
                       sp.thuvien_sach_id,
                       kh.tonkho_soluong
                   };
        grvList.DataSource = list;
        grvList.DataBind();
        //hiện ngày tháng 
        //var new_day = DateTime.Now.ToString("dd/MM/yyyy");
        //txtDateGiaohang.Value = new_day;
        // load mã tự tăng
        // load tên nhân viên
        //lấy thông tin của tk đang nhập
        var getuser = (from u in db.admin_Users
                       where u.username_username == Request.Cookies["UserName"].Value
                       select u).FirstOrDefault();
        txtNhanVien.Value = getuser.username_fullname;
        loaddatatable();
        dtProduct = (DataTable)Session["MuonSachChiTiet"];
        rp_grvChiTiet.DataSource = dtProduct;
        rp_grvChiTiet.DataBind();
        //catch { }
    }
    public void loaddatatable()
    {
        try
        {
            if (dtProduct == null)
            {
                dtProduct = new DataTable();
                dtProduct.Columns.Add("thuvien_sach_id", typeof(int));
                dtProduct.Columns.Add("thuvien_sach_name", typeof(string));
                dtProduct.Columns.Add("booksach_chitiet_soluong", typeof(int));
            }
        }
        catch { }
    }
    protected void btnDatHang_ServerClick(object sender, EventArgs e)
    {
        try
        {
            //kiểm tra add 2 lần có thêm vào gridview hay không
            int _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "thuvien_sach_id" }));
            var checkSanPham = (from sp in db.tbThuVien_Saches
                                join tk in db.tbThuVien_TonKhos on sp.thuvien_sach_id equals tk.thuvien_sach_id
                                where sp.thuvien_sach_id == _id
                                select new
                                {
                                    sp.thuvien_sach_id,
                                    sp.thuvien_sach_name
                                }).SingleOrDefault();
            if (Session["MuonSachChiTiet"] != null)
            {
                dtProduct = (DataTable)Session["MuonSachChiTiet"];
                DataRow[] row_id = dtProduct.Select("thuvien_sach_id = '" + _id + "'");
                if (row_id.Length != 0)
                {
                    alert.alert_Warning(Page, "Sách này đã có trong danh sách cho mượn", "");
                }
                else
                {
                    DataRow row = dtProduct.NewRow();
                    row["thuvien_sach_id"] = checkSanPham.thuvien_sach_id;
                    row["thuvien_sach_name"] = checkSanPham.thuvien_sach_name;
                    row["booksach_chitiet_soluong"] = 1;
                    dtProduct.Rows.Add(row);
                    Session["MuonSachChiTiet"] = dtProduct;
                }
            }
            else
            {
                loaddatatable();
                DataRow row = dtProduct.NewRow();
                row["thuvien_sach_id"] = checkSanPham.thuvien_sach_id;
                row["thuvien_sach_name"] = checkSanPham.thuvien_sach_name;
                row["booksach_chitiet_soluong"] = 1;
                dtProduct.Rows.Add(row);
                Session["MuonSachChiTiet"] = dtProduct;
            }
            rp_grvChiTiet.DataSource = dtProduct;
            rp_grvChiTiet.DataBind();
        }
        catch { }
    }

    protected void DatHang_ServerClick(object sender, EventArgs e)
    {
        // kiểm tra id
        int _id = Convert.ToInt32(txt_ID.Value);
        if (Session["MuonSachChiTiet"] != null)
        {
            dtProduct = (DataTable)Session["MuonSachChiTiet"];
            // chạy foreach để lặp lại các row 
            foreach (DataRow row in dtProduct.Rows)
            {
                string product_id = row["thuvien_sach_id"].ToString();
                if (product_id == _id.ToString())
                {
                    // lưu data bằng input đầu vào
                    row.SetField("booksach_chitiet_soluong", txt_SoLuong.Value);
                    rp_grvChiTiet.DataSource = dtProduct;
                    rp_grvChiTiet.DataBind();
                }
            }
        }
    }

    protected void btnXoa_ServerClick(object sender, EventArgs e)
    {
        int _id = Convert.ToInt32(txt_ID.Value);
        dtProduct = (DataTable)Session["MuonSachChiTiet"];
        foreach (DataRow row in dtProduct.Rows)
        {
            string product_id = row["thuvien_sach_id"].ToString();
            if (product_id == _id.ToString())
            {
                dtProduct.Rows.Remove(row);
                Session["MuonSachChiTiet"] = dtProduct;
                break;
            }
        }
        rp_grvChiTiet.DataSource = dtProduct;
        rp_grvChiTiet.DataBind();
        alert.alert_Success(Page, "Xóa thành công!", "");
    }
    protected void XuatHang_Click(object sender, EventArgs e)
    {
        dtProduct = (DataTable)Session["MuonSachChiTiet"];
        try
        {
            if (dtProduct.Rows.Count <= 0 || dtProduct == null)
            {
                alert.alert_Warning(Page, "Bạn chưa có sách nào", "");
            }
        }
        catch { }
        if (dtProduct == null)
        {
            alert.alert_Warning(Page, "Bạn chưa có sách nào", "");
        }
        else
        {
            if (txtBoPhan.Value == "" || dteNgayMuon.Value == "" || dteNgayTra.Value == "")
            {
                alert.alert_Warning(Page, "Bạn chưa nhập đầy đủ nội dung", "");
            }
            else
            {
                // lưu dữ liệu vào bảng nhập hàng
                try
                {
                    if (dtProduct.Rows.Count > 0)
                    {
                        tbThuVien_BookSach insertXH = new tbThuVien_BookSach();
                        insertXH.thuvien_booksach_tungay = Convert.ToDateTime(dteNgayMuon.Value);
                        insertXH.thuvien_booksach_denngay = Convert.ToDateTime(dteNgayTra.Value);
                        insertXH.username_id = (from u in db.admin_Users where u.username_username == txtNhanVien.Value select u).SingleOrDefault().username_id;
                        insertXH.khachhang_name = txtName.Value;
                        insertXH.thuvien_bophan = Convert.ToInt32(txtBoPhan.Value);
                        insertXH.hidden = false;
                        insertXH.active = true;
                        db.tbThuVien_BookSaches.InsertOnSubmit(insertXH);
                        db.SubmitChanges();
                        //lưu dữ liệu nhập hàng chi tiết trong table vào datatable
                        foreach (DataRow row in dtProduct.Rows)
                        {
                            //kiểm tra số lượng còn lại trong khi hàng 
                            //nếu ít hơn sl mượn thì không cho muượn
                            var checkkh = (from kh in db.tbThuVien_TonKhos
                                           where kh.thuvien_sach_id == Convert.ToInt32(row["thuvien_sach_id"])
                                           select kh).SingleOrDefault();
                            var checksp = (from spkh in db.tbThuVien_TonKhos
                                           where spkh.thuvien_sach_id == Convert.ToInt32(row["thuvien_sach_id"])
                                           select spkh).SingleOrDefault();
                            if (checksp.tonkho_soluong < Convert.ToInt32(row["booksach_chitiet_soluong"]))
                            {
                                alert.alert_Warning(Page, "Số lượng sách " + row["thuvien_sach_name"] + " còn lại trong kho không đủ để cho mượn. Vui lòng nhập thêm sách!", "");
                            }
                            else
                            {
                                checkkh.tonkho_soluong = checkkh.tonkho_soluong - Convert.ToInt32(row["booksach_chitiet_soluong"]);
                                //lưu vào bảng chi tiết
                                tbThuVien_BookSach_ChiTiet insertXHCT = new tbThuVien_BookSach_ChiTiet();
                                insertXHCT.thuvien_sach_id = Convert.ToInt32(row["thuvien_sach_id"]);
                                insertXHCT.booksach_chitiet_soluong = Convert.ToInt32(row["booksach_chitiet_soluong"]);
                                insertXHCT.thuvien_booksach_id = insertXH.thuvien_booksach_id;
                                insertXHCT.booksach_chitiet_tungay = Convert.ToDateTime(dteNgayMuon.Value);
                                insertXHCT.booksach_chitiet_denngay = Convert.ToDateTime(dteNgayTra.Value);
                                db.tbThuVien_BookSach_ChiTiets.InsertOnSubmit(insertXHCT);
                                db.SubmitChanges();
                                txtName.Value = "";
                                Session["MuonSachChiTiet"] = null;
                            }
                        }
                        db.SubmitChanges();
                        dtProduct = (DataTable)Session["MuonSachChiTiet"];
                        rp_grvChiTiet.DataSource = dtProduct;
                        rp_grvChiTiet.DataBind();
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "AlertBox", "swal('Mượn sách thành công!', '','success').then(function(){window.location = '/admin-quan-ly-muon-sach';})", true);
                    }
                }
                catch { }
            }
        }
    }
}