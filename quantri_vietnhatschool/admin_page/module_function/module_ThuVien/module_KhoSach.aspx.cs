﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_KhoSach : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    string image;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["_id"] = 0;
        }
        loadData();

    }
    private void loadData()
    {
        var list = from s in db.tbThuVien_Saches
                   //join tk in db.tbThuVien_TonKhos on s.thuvien_sach_id equals tk.thuvien_sach_id
                   join ts in db.tbThuVien_TuSaches on s.thuvien_tusach_id equals ts.thuvien_tusach_id

                   select new
                   {
                       s.thuvien_sach_id,
                       s.thuvien_sach_name,
                       ts.thuvien_tusach_name,
                       //tk.tonkho_soluong
                   };
        grvList.DataSource = list;
        grvList.DataBind();
        ddlTuSach.DataSource = from ts in db.tbThuVien_TuSaches
                               select ts;
        ddlTuSach.DataBind();
    }
    private void setNULL()
    {
        txtName.Text = "";
    }

    protected void btnThem_Click(object sender, EventArgs e)
    {
        Session["_id"] = 0;
        setNULL();
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupControl.Show();", true);
    }

    protected void btnCapNhat_Click(object sender, EventArgs e)
    {
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "thuvien_sach_id" }));
        Session["_id"] = _id;
        var getData = (from tb in db.tbThuVien_Saches
                       join ts in db.tbThuVien_TuSaches on tb.thuvien_tusach_id equals ts.thuvien_tusach_id
                       where tb.thuvien_sach_id == _id
                       select new
                       {
                           tb.thuvien_sach_id,
                           tb.thuvien_sach_name,
                           ts.thuvien_tusach_id,
                           ts.thuvien_tusach_name,
                           tb.thuvien_sach_image
                       }).Single();
        txtName.Text = getData.thuvien_sach_name;
        ddlTuSach.Text = getData.thuvien_tusach_name;
        if (getData.thuvien_sach_image == null)
            ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupControl.Show();showImg1_1('" + "/admin_images/up-img.png" + "'); ", true);
        else
            image = getData.thuvien_sach_image;
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupControl.Show();showImg1_1('" + getData.thuvien_sach_image + "'); ", true);
    }

    protected void btnXoa_Click(object sender, EventArgs e)
    {
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "thuvien_sach_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                tbThuVien_Sach checkSach = (from i in db.tbThuVien_Saches where i.thuvien_sach_id == Convert.ToInt32(item) select i).SingleOrDefault();
                db.tbThuVien_Saches.DeleteOnSubmit(checkSach);
                //tbThuVien_TonKho checkID = (from i in db.tbThuVien_TonKhos where i.thuvien_sach_id == Convert.ToInt32(item) select i).SingleOrDefault();
                //db.tbThuVien_Saches.DeleteOnSubmit(checkSach);
                db.SubmitChanges();
            }
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "AlertBox", "swal('Xóa thành công!', '','success').then(function(){window.location = '/admin-thong-ke-tu-sach';})", true);
        }
        else
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
    }
    public bool checknull()
    {
        if (ddlTuSach.Text != "")
            return true;
        else return false;
    }
    protected void btnLuu_Click(object sender, EventArgs e)
    {
        if (Page.IsValid && FileUpload1.HasFile)
        {
            String folderUser = Server.MapPath("~/uploadimages/thuviensach/");
            if (!Directory.Exists(folderUser))
            {
                Directory.CreateDirectory(folderUser);
            }
            //string filename;
            string ulr = "/uploadimages/thuviensach/";
            HttpFileCollection hfc = Request.Files;
            string filename = Path.GetRandomFileName() + Path.GetExtension(FileUpload1.FileName);
            string fileName_save = Path.Combine(Server.MapPath("~/uploadimages/thuviensach"), filename);
            FileUpload1.SaveAs(fileName_save);
            image = ulr + filename;
        }
        if (checknull() == false)
            alert.alert_Warning(Page, "Hãy chọn tủ sách!", "");
        else
        {
            //trường hợp thêm mới
            if (Session["_id"].ToString() == "0")
            {
                try
                {
                    if (image == null)
                        image = "/admin_images/up-img.png";
                    //lưu vào bảng tbThuVien_Sach
                    tbThuVien_Sach insert = new tbThuVien_Sach();
                    insert.thuvien_sach_name = txtName.Text;
                    insert.thuvien_tusach_id = Convert.ToInt32(ddlTuSach.Value);
                    insert.thuvien_sach_image = image;
                    db.tbThuVien_Saches.InsertOnSubmit(insert);
                    db.SubmitChanges();
                    //lưu vào bảng tồn kho với số lượng còn lại của sách là 0
                    //tbThuVien_TonKho ins = new tbThuVien_TonKho();
                    //ins.thuvien_sach_id = insert.thuvien_sach_id;
                    //ins.tonkho_soluong = 0;
                    //ins.tonkho_min = 3;
                    //db.tbThuVien_TonKhos.InsertOnSubmit(ins);
                    //db.SubmitChanges();
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "AlertBox", "swal('Lưu thành công!', '','success').then(function(){window.location = '/admin-thong-ke-tu-sach';})", true);
                }
                catch { }
            }
            else
            {
                try
                {
                    if (image == null)
                        image = "/admin_images/up-img.png";
                    //cập nhật lại tbThuVien_Sach
                    tbThuVien_Sach update = db.tbThuVien_Saches.Where(x => x.thuvien_sach_id == Convert.ToInt32(Session["_id"].ToString())).FirstOrDefault();
                    update.thuvien_sach_name = txtName.Text;
                    update.thuvien_tusach_id = Convert.ToInt32(ddlTuSach.Value);
                    update.thuvien_sach_image = image;
                    db.SubmitChanges();
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "AlertBox", "swal('Cập nhật thành công!', '','success').then(function(){window.location = '/admin-thong-ke-tu-sach';})", true);
                }
                catch { }
            }
        }
        // popupControl.ShowOnPageLoad = false;
    }
}