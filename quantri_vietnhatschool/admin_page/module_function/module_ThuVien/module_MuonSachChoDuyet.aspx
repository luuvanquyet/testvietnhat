﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_MuonSachChoDuyet.aspx.cs" Inherits="admin_page_module_function_module_ThuVien_module_MuonSachChoDuyet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">

    <div class="card card-block">
        <div class="form-group row">
            <div class="col-sm-10">

                <asp:Button CssClass="btn btn-primary" ID="btnDuyet" runat="server" OnClick="btnDuyet_Click" Text="Duyệt"></asp:Button>
                <%--<asp:Button CssClass="btn btn-primary" ID="btnChiTiet" runat="server" OnClick="btnChiTiet_Click" Text="Chi tiết"></asp:Button>--%>
            </div>
        </div>
        <div class="form-group table-responsive">
            <asp:UpdatePanel ID="upListProduct" runat="server">
                <ContentTemplate>
                    <label>Danh sách mượn sách từ website</label>
                    <dx:ASPxGridView ID="grvList" runat="server" CssClass="table-hover" ClientInstanceName="grvList" KeyFieldName="thuvien_booksach_id" Width="90%">
                        <Columns>
                            <dx:GridViewCommandColumn ShowSelectCheckbox="True" SelectAllCheckboxMode="Page" VisibleIndex="0" Width="5%">
                            </dx:GridViewCommandColumn>
                            <dx:GridViewDataColumn Caption="Họ và tên" FieldName="khachhang_name" HeaderStyle-HorizontalAlign="Center" Width="70%" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn Caption="Ngày mượn sách" FieldName="thuvien_booksach_tungay" HeaderStyle-HorizontalAlign="Center" Width="70%" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn Caption="Bộ phận" FieldName="thuvien_bophan" HeaderStyle-HorizontalAlign="Center" Width="20%" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>
                        </Columns>
                        <SettingsSearchPanel Visible="true" />
                        <SettingsBehavior AllowFocusedRow="true" />
                        <SettingsText EmptyDataRow="Trống" SearchPanelEditorNullText="Gõ từ cần tìm kiếm và enter..." />
                        <SettingsLoadingPanel Text="Đang tải..." />
                        <SettingsPager PageSize="10" Summary-Text="Trang {0} / {1} ({2} trang)"></SettingsPager>
                    </dx:ASPxGridView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

