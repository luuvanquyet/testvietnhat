﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_ThuVien_module_MuonSachChoDuyet : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    protected void Page_Load(object sender, EventArgs e)
    {
        //lấy dữ liệu đổ lên gridview
        var getData = from bs in db.tbThuVien_BookSaches
                      where bs.active == false
                      select new
                      {
                          bs.thuvien_booksach_id,
                          bs.khachhang_name,
                          bs.thuvien_booksach_tungay,
                          thuvien_bophan = bs.thuvien_bophan == 1 ? "Giáo viên" : "Học sinh"
                      };
        grvList.DataSource = getData;
        grvList.DataBind();
    }

    protected void btnDuyet_Click(object sender, EventArgs e)
    {
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "thuvien_booksach_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                //lấy thông tin của đơn hàng mượn sách
                tbThuVien_BookSach checkID = (from i in db.tbThuVien_BookSaches where i.thuvien_booksach_id == Convert.ToInt32(item) select i).SingleOrDefault();
                //cập nhật lại trạng thái
                checkID.active = true;
                checkID.username_id = (from u in db.admin_Users where u.username_username == Request.Cookies["UserName"].Value select u).SingleOrDefault().username_id;
                db.SubmitChanges();
                //kiểm tra số lượng sách chi tiết mượn và cập nhật lại số lượng tồn kho
                var getCT = from ct in db.tbThuVien_BookSach_ChiTiets
                            where ct.thuvien_booksach_id == Convert.ToInt32(item)
                            select ct;
                foreach (var sach in getCT)
                {
                    var tonkho = (from tk in db.tbThuVien_TonKhos
                                  where tk.thuvien_sach_id == Convert.ToInt32(sach.thuvien_sach_id)
                                  select tk).SingleOrDefault();

                    //nếu sl mượn lớn hơn hoặc bằng sl có trong kho thì vẫn cho mượn nhưng upate lại sl mượn=sl trong kho
                    if (sach.booksach_chitiet_soluong>=tonkho.tonkho_soluong)
                    {
                        sach.booksach_chitiet_soluong = tonkho.tonkho_soluong;
                        tonkho.tonkho_soluong = 0;
                        db.SubmitChanges();
                    }
                    //ngược lại thì sl trong kho = sl trong kho - sl mượn
                    else
                    {
                        tonkho.tonkho_soluong = tonkho.tonkho_soluong - sach.booksach_chitiet_soluong;
                        db.SubmitChanges();
                    }
                }
            }
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "AlertBox", "swal('Duyệt thành công!', '','success').then(function(){window.location = '/admin-duyet-don-muon-sach';})", true);
        }
        else
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
    }

    protected void btnChiTiet_Click(object sender, EventArgs e)
    {

    }
}