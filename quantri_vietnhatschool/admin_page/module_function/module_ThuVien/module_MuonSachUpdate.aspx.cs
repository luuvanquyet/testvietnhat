﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_ThuVien_module_MuonSachUpdate : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public string adminName;
    DataTable dtProduct;
    public int stt = 1;
    int masp;
    string tensp;
    int sl;
    int gianhap;
    int thanhtien;
    cls_Alert alert = new cls_Alert();
    protected void Page_Load(object sender, EventArgs e)
    {
        var list = from kh in db.tbThuVien_TonKhos
                   join sp in db.tbThuVien_Saches on kh.thuvien_sach_id equals sp.thuvien_sach_id
                   where kh.tonkho_soluong > 0
                   select new
                   {
                       kh.tonkho_id,
                       sp.thuvien_sach_name,
                       sp.thuvien_sach_id,
                       kh.tonkho_soluong
                   };
        grvList.DataSource = list;
        grvList.DataBind();
        //lấy thông tin của tk đang nhập
        var getuser = (from u in db.admin_Users
                       where u.username_username == Request.Cookies["UserName"].Value
                       select u).FirstOrDefault();
        //lấy thông tin từ bảng booksach
        if (!IsPostBack)
        {
            int _id = Convert.ToInt32(RouteData.Values["id"]);
            var getdetail = (from nh in db.tbThuVien_BookSaches where nh.thuvien_booksach_id == _id select nh).SingleOrDefault();
            txtNhanVien.Value = (from u in db.admin_Users where u.username_id == getdetail.username_id select u).SingleOrDefault().username_fullname;
            txtName.Value = getdetail.khachhang_name;
            dteNgayMuon.Value = getdetail.thuvien_booksach_tungay.Value.ToString("yyyy-MM-dd").Replace(' ', 'T');
            dteNgayTra.Value = getdetail.thuvien_booksach_denngay.Value.ToString("yyyy-MM-dd").Replace(' ', 'T');
            txtBoPhan.Value = getdetail.thuvien_bophan + "";
            // nếu sestion chi tiet mà co du lieu roi thi khong cho chạy vao nua
            loaddatatable();
            //dtProduct = (DataTable)Session["spChiTiet"];
            //rp_grvChiTiet.DataSource = dtProduct;
            //rp_grvChiTiet.DataBind();
            if (RouteData.Values["id"] != null)
            {
                //get mã tự tăng của nhập hàng chi tiết
                var getctms = (from ctms in db.tbThuVien_BookSach_ChiTiets
                               join product in db.tbThuVien_Saches on ctms.thuvien_sach_id equals product.thuvien_sach_id
                               where getdetail.thuvien_booksach_id == ctms.thuvien_booksach_id
                               && ctms.hidden == null
                               select new
                               {
                                   ctms.thuvien_sach_id,
                                   product.thuvien_sach_name,
                                   ctms.booksach_chitiet_soluong

                               });
                //loaddata ra datatable
                foreach (var item in getctms)
                {
                    DataRow row = dtProduct.NewRow();
                    row["thuvien_sach_id"] = item.thuvien_sach_id;
                    row["thuvien_sach_name"] = item.thuvien_sach_name;
                    row["booksach_chitiet_soluong"] = item.booksach_chitiet_soluong;
                    dtProduct.Rows.Add(row);
                };
                Session["MuonSachUpdate"] = dtProduct;
                rp_grvChiTiet.DataSource = dtProduct;
                rp_grvChiTiet.DataBind();
            }
        }
    }
    public void loaddatatable()
    {
        try
        {
            if (dtProduct == null)
            {
                dtProduct = new DataTable();
                dtProduct.Columns.Add("thuvien_sach_id", typeof(int));
                dtProduct.Columns.Add("thuvien_sach_name", typeof(string));
                //dtProduct.Columns.Add("tonkho_soluong", typeof(int));
                dtProduct.Columns.Add("booksach_chitiet_soluong", typeof(int));
            }
        }
        catch { }
    }
    protected void btnDatHang_ServerClick(object sender, EventArgs e)
    {
        try
        {
            //kiểm tra add 2 lần có thêm vào gridview hay không
            int _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "thuvien_sach_id" }));
            var checkSanPham = (from sp in db.tbThuVien_Saches
                                join tk in db.tbThuVien_TonKhos on sp.thuvien_sach_id equals tk.thuvien_sach_id
                                where sp.thuvien_sach_id == _id
                                select new
                                {
                                    sp.thuvien_sach_id,
                                    sp.thuvien_sach_name,
                                    tk.tonkho_soluong
                                }).SingleOrDefault();
            if (Session["MuonSachUpdate"] != null)
            {
                dtProduct = (DataTable)Session["MuonSachUpdate"];
                DataRow[] row_id = dtProduct.Select("thuvien_sach_id = '" + _id + "'");
                if (row_id.Length != 0)
                {
                    alert.alert_Warning(Page, "Sách này đã có trong danh sách cho mượn", "");
                }
                else
                {
                    DataRow row = dtProduct.NewRow();
                    row["thuvien_sach_id"] = checkSanPham.thuvien_sach_id;
                    row["thuvien_sach_name"] = checkSanPham.thuvien_sach_name;
                    //row["tonkho_soluong"] = checkSanPham.tonkho_soluong;
                    row["booksach_chitiet_soluong"] = 1;
                    dtProduct.Rows.Add(row);
                    Session["MuonSachUpdate"] = dtProduct;
                }
            }
            else
            {
                loaddatatable();
                DataRow row = dtProduct.NewRow();
                row["thuvien_sach_id"] = checkSanPham.thuvien_sach_id;
                row["thuvien_sach_name"] = checkSanPham.thuvien_sach_name;
                //row["tonkho_soluong"] = checkSanPham.tonkho_soluong;
                row["booksach_chitiet_soluong"] = 1;
                dtProduct.Rows.Add(row);
                Session["MuonSachUpdate"] = dtProduct;
            }
            rp_grvChiTiet.DataSource = dtProduct;
            rp_grvChiTiet.DataBind();
        }
        catch { }
    }

    protected void DatHang_ServerClick(object sender, EventArgs e)
    {
        // kiểm tra id
        int _id = Convert.ToInt32(txt_ID.Value);
        if (Session["MuonSachUpdate"] != null)
        {
            dtProduct = (DataTable)Session["MuonSachUpdate"];
            // chạy foreach để lặp lại các row 
            foreach (DataRow row in dtProduct.Rows)
            {
                string product_id = row["thuvien_sach_id"].ToString();
                if (product_id == _id.ToString())
                {
                    // lưu data bằng input đầu vào
                    //if (Convert.ToInt32(row["tonkho_soluong"]) < Convert.ToInt32(txt_SoLuong.Value))
                    //{
                    //    alert.alert_Warning(Page, "Số lượng sách " + row["thuvien_sach_name"] + " còn lại trong kho không đủ để cho mượn. Vui lòng nhập lại số lượng!", "");
                    //}
                    row.SetField("booksach_chitiet_soluong", txt_SoLuong.Value);
                    rp_grvChiTiet.DataSource = dtProduct;
                    rp_grvChiTiet.DataBind();
                }
            }
        }
    }

    protected void btnXoa_ServerClick(object sender, EventArgs e)
    {
        int _id = Convert.ToInt32(txt_ID.Value);
        dtProduct = (DataTable)Session["MuonSachUpdate"];
        foreach (DataRow row in dtProduct.Rows)
        {
            string product_id = row["thuvien_sach_id"].ToString();
            if (product_id == _id.ToString())
            {
                dtProduct.Rows.Remove(row);
                Session["MuonSachUpdate"] = dtProduct;
                break;
            }
        }
        rp_grvChiTiet.DataSource = dtProduct;
        rp_grvChiTiet.DataBind();
        alert.alert_Success(Page, "Xóa thành công!", "");
    }

    protected void btnMuonSach_Click(object sender, EventArgs e)
    {
        dtProduct = (DataTable)Session["MuonSachUpdate"];
        try
        {
            if (dtProduct.Rows.Count <= 0 || dtProduct == null)
            {
                alert.alert_Warning(Page, "Bạn chưa có sách nào", "");
            }
        }
        catch { }
        if (dtProduct == null)
        {
            alert.alert_Warning(Page, "Bạn chưa có sách nào", "");
        }
        else
        {
            if (txtBoPhan.Value == "" || dteNgayMuon.Value == "" || dteNgayTra.Value == "")
            {
                alert.alert_Warning(Page, "Bạn chưa nhập đầy đủ nội dung", "");
            }
            else
            {
                // lưu dữ liệu vào bảng nhập hàng
                try
                {
                    if (dtProduct.Rows.Count > 0)
                    {
                        //lưu dữ liệu nhập hàng chi tiết trong table vào datatable
                        foreach (DataRow row in dtProduct.Rows)
                        {
                            // kiểm tra product này đã có trong bảng chi tiết
                            var checksp = (from ctms in db.tbThuVien_BookSach_ChiTiets
                                           where ctms.thuvien_booksach_id == Convert.ToInt32(RouteData.Values["id"])
                                           where ctms.thuvien_sach_id == Convert.ToInt32(row["thuvien_sach_id"])
                                           select ctms);
                            //checkprdt kho hàng
                            var checkkh = (from kh in db.tbThuVien_TonKhos
                                           where kh.thuvien_sach_id == Convert.ToInt32(row["thuvien_sach_id"])
                                           select kh).SingleOrDefault();
                            //nếu có rồi thì update lại số lượng mượn
                            if (checksp.Count() > 0)
                            {
                                foreach (var product in checksp)
                                {
                                    ////nếu có sản phẩm này trong bản chi tiết nhập hàng thì mình update nó lại
                                    if (product.thuvien_sach_id == Convert.ToInt32(row["thuvien_sach_id"]))
                                    {
                                        /*nếu sl mượn > sl trong kho thì thông báo lỗi*/
                                        if (checkkh.tonkho_soluong < Convert.ToInt32(row["booksach_chitiet_soluong"]))
                                        {
                                            alert.alert_Warning(Page, "Số lượng sách " + row["thuvien_sach_name"] + "còn lại trong kho không đủ để cho mượn. Vui lòng nhập thêm sách!", "");
                                        }
                                        else
                                        {
                                            //update lại bảng tồn kho
                                            //nếu "số lượng lúc thay đổi" nhỏ hơn "số lượng ban đầu trong bảng chi tiết" thì trừ đi
                                            if (product.booksach_chitiet_soluong < Convert.ToInt32(row["booksach_chitiet_soluong"]))
                                            {
                                                checkkh.tonkho_soluong = checkkh.tonkho_soluong - (Convert.ToInt32(row["booksach_chitiet_soluong"]) - product.booksach_chitiet_soluong);
                                                db.SubmitChanges();
                                            }
                                            //nếu "số lượng lúc thay đổi" lớn hơn "số lượng ban đầu trong bảng chi tiết" thì cộng lại
                                            if (product.booksach_chitiet_soluong > Convert.ToInt32(row["booksach_chitiet_soluong"]))
                                            {
                                                checkkh.tonkho_soluong = checkkh.tonkho_soluong + (product.booksach_chitiet_soluong - Convert.ToInt32(row["booksach_chitiet_soluong"]));
                                                db.SubmitChanges();
                                            }
                                            //update vào bảng chi tiết nhập sách
                                            product.booksach_chitiet_soluong = Convert.ToInt32(row["booksach_chitiet_soluong"]);
                                            db.SubmitChanges();
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (checkkh.tonkho_soluong < Convert.ToInt32(row["booksach_chitiet_soluong"]))
                                {
                                    alert.alert_Warning(Page, "Số lượng sách " + row["thuvien_sach_name"] + "còn lại trong kho không đủ để cho mượn. Vui lòng nhập thêm sách!", "");
                                }
                                else
                                {
                                    checkkh.tonkho_soluong = checkkh.tonkho_soluong - Convert.ToInt32(row["booksach_chitiet_soluong"]);
                                    // sách được mượn mới
                                    tbThuVien_BookSach_ChiTiet insertXHCT = new tbThuVien_BookSach_ChiTiet();
                                    insertXHCT.thuvien_sach_id = Convert.ToInt32(row["thuvien_sach_id"]);
                                    insertXHCT.booksach_chitiet_soluong = Convert.ToInt32(row["booksach_chitiet_soluong"]);
                                    insertXHCT.thuvien_booksach_id = Convert.ToInt32(RouteData.Values["id"]);
                                    insertXHCT.booksach_chitiet_tungay = Convert.ToDateTime(dteNgayMuon.Value);
                                    insertXHCT.booksach_chitiet_denngay = Convert.ToDateTime(dteNgayTra.Value);
                                    db.tbThuVien_BookSach_ChiTiets.InsertOnSubmit(insertXHCT);
                                    db.SubmitChanges();
                                    txtName.Value = "";
                                    Session["MuonSachUpdate"] = null;
                                }
                            }
                        }
                        int id = Convert.ToInt32(RouteData.Values["id"]);
                        var getdatail = (from bs in db.tbThuVien_BookSaches
                                         where bs.thuvien_booksach_id == id
                                         select bs).SingleOrDefault();
                        getdatail.khachhang_name = txtName.Value;
                        getdatail.thuvien_booksach_tungay = Convert.ToDateTime(dteNgayMuon.Value);
                        getdatail.thuvien_booksach_denngay = Convert.ToDateTime(dteNgayTra.Value);
                        getdatail.thuvien_bophan = Convert.ToInt32(txtBoPhan.Value);
                        db.SubmitChanges();
                        dtProduct = (DataTable)Session["MuonSachUpdate"];
                        rp_grvChiTiet.DataSource = dtProduct;
                        rp_grvChiTiet.DataBind();
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "AlertBox", "swal('Mượn sách thành công!', '','success').then(function(){window.location = '/admin-quan-ly-muon-sach';})", true);
                    }
                }
                catch { }
            }
        }
    }
}