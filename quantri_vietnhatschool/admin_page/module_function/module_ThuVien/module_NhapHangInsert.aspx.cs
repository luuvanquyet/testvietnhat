﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_NhapHangInsert : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public string adminName;
    DataTable dtProduct;
    public int stt = 1;
    int masp;
    string tensp;
    int sl;
    int gianhap;
    int thanhtien;
    cls_Alert alert = new cls_Alert();
    protected void Page_Load(object sender, EventArgs e)
    {
        var list = from sp in db.tbThuVien_Saches
                   //join kh in db.tbThuVien_TonKhos on sp.thuvien_sach_id equals kh.thuvien_sach_id
                   select new
                   {
                       sp.thuvien_sach_id,
                       sp.thuvien_sach_name,
                       //kh.tonkho_soluong
                   };
        grvList.DataSource = list;
        grvList.DataBind();
        //lấy thông tin của tk đang nhập
        var getuser = (from u in db.admin_Users
                       where u.username_username == Request.Cookies["UserName"].Value
                       select u).FirstOrDefault();
        txtNhanVien.Value = getuser.username_fullname;
        string matutang = Matutang();
        txtMaNhap.Value = matutang;
        loaddatatable();
        dtProduct = (DataTable)Session["spChiTiet"];
        rp_grvChiTiet.DataSource = dtProduct;
        rp_grvChiTiet.DataBind();
        //hiện ngày tháng 
        //var new_day = DateTime.Now.ToString("dd/MM/yyyy");
        txtNgayNhap.Value = DateTime.Now.ToString("dd/MM/yyyy");

    }
    //Hàm tự tăng
    public string Matutang()
    {
        int year = DateTime.Now.Year;
        var list = from nk in db.tbThuVien_NhapSaches select nk;
        string s = "NH";
        if (list.Count() <= 0)
            s = "NH00001";
        else
        {
            var list1 = from nk in db.tbThuVien_NhapSaches orderby nk.nhapsach_code descending select nk;
            string chuoi = list1.First().nhapsach_code;
            int k;
            k = Convert.ToInt32(chuoi.Substring(2, 5));
            k = k + 1;
            if (k < 10) s = s + "0000";
            else if (k < 100)
                s = s + "000";
            else if (k < 1000)
                s = s + "00";
            else if (k < 10000)
                s = s + "0";
            s = s + k.ToString();
        }
        return s;
    }
    public void loaddatatable()
    {
        try
        {
            if (dtProduct == null)
            {
                dtProduct = new DataTable();
                dtProduct.Columns.Add("thuvien_sach_id", typeof(int));
                dtProduct.Columns.Add("thuvien_sach_name", typeof(string));
                dtProduct.Columns.Add("nhapsach_chitiet_soluong", typeof(int));
                dtProduct.Columns.Add("nhapsach_gianhap", typeof(int));
                dtProduct.Columns.Add("nhapsach_thanhtien", typeof(int));
            }
        }
        catch { }
    }
    public void loadGrv()
    {

    }
    protected void btnChiTiet_ServerClick(object sender, EventArgs e)
    {
        try
        {
            //kiểm tra add 2 lần có thêm vào gridview hay không
            int _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "thuvien_sach_id" }));
            var checkSanPham = (from sp in db.tbThuVien_Saches where sp.thuvien_sach_id == _id select sp).SingleOrDefault();
            if (Session["spChiTiet"] != null)
            {
                dtProduct = (DataTable)Session["spChiTiet"];
                DataRow[] row_id = dtProduct.Select("thuvien_sach_id = '" + _id + "'");
                if (row_id.Length != 0)
                {
                    alert.alert_Warning(Page, "Sách này đã có trong danh sách nhập", "");
                }
                else
                {
                    DataRow row = dtProduct.NewRow();
                    row["thuvien_sach_id"] = checkSanPham.thuvien_sach_id;
                    row["thuvien_sach_name"] = checkSanPham.thuvien_sach_name;
                    row["nhapsach_chitiet_soluong"] = 1;
                    row["nhapsach_gianhap"] = 0;
                    row["nhapsach_thanhtien"] = 0;
                    dtProduct.Rows.Add(row);
                    Session["spChiTiet"] = dtProduct;
                }
            }
            else
            {
                loaddatatable();
                DataRow row = dtProduct.NewRow();
                row["thuvien_sach_id"] = checkSanPham.thuvien_sach_id;
                row["thuvien_sach_name"] = checkSanPham.thuvien_sach_name;
                row["nhapsach_chitiet_soluong"] = 1;
                row["nhapsach_gianhap"] = 0;
                row["nhapsach_thanhtien"] = 0;
                dtProduct.Rows.Add(row);
                Session["spChiTiet"] = dtProduct;
            }
            rp_grvChiTiet.DataSource = dtProduct;
            rp_grvChiTiet.DataBind();
            btnSave.Visible = true;
        }
        catch { }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        dtProduct = (DataTable)Session["spChiTiet"];
        try
        {
            if (dtProduct.Rows.Count <= 0 || dtProduct == null) alert.alert_Warning(Page, "Bạn chưa có sách nào", "");
        }
        catch { }
        if (dtProduct == null) alert.alert_Warning(Page, "Bạn chưa có sách nào", "");
        else
        {
            if (txtNoiDung.Value == "")
            {
                alert.alert_Warning(Page, "Bạn chưa nhập nội dung", "");
            }

            else
            {
                // lưu dữ liệu vào bảng nhập hàng
                try
                {
                    if (dtProduct.Rows.Count > 0)
                    {
                        tbThuVien_NhapSach insertNH = new tbThuVien_NhapSach();
                        insertNH.nhapsach_code = txtMaNhap.Value;
                        insertNH.nhapsach_content = txtNoiDung.Value;
                        insertNH.nhapsach_createdate = DateTime.Now;
                        insertNH.username_id = (from u in db.admin_Users where u.username_username == txtNhanVien.Value select u).SingleOrDefault().username_id;
                        db.tbThuVien_NhapSaches.InsertOnSubmit(insertNH);
                        db.SubmitChanges();
                        //lưu dữ liệu nhập hàng chi tiết trong table vào datatable
                        foreach (DataRow row in dtProduct.Rows)
                        {
                            var checkNhapHang = from nh in db.tbThuVien_NhapSach_ChiTiets where nh.nhapsach_code == txtMaNhap.Value select nh;
                            if (checkNhapHang != txtMaNhap)
                            {
                                tbThuVien_NhapSach_ChiTiet insertNHCT = new tbThuVien_NhapSach_ChiTiet();
                                insertNHCT.thuvien_sach_id = Convert.ToInt32(row["thuvien_sach_id"]);
                                insertNHCT.nhapsach_code = txtMaNhap.Value;
                                insertNHCT.nhapsach_chitiet_soluong = Convert.ToInt32(row["nhapsach_chitiet_soluong"]);
                                insertNHCT.nhapsach_gianhap = Convert.ToInt32(row["nhapsach_gianhap"]);
                                insertNHCT.nhapsach_thanhtien = Convert.ToInt32(row["nhapsach_thanhtien"]);
                                db.tbThuVien_NhapSach_ChiTiets.InsertOnSubmit(insertNHCT);
                                db.SubmitChanges();
                                Session["spChiTiet"] = null;
                                dtProduct = (DataTable)Session["spChiTiet"];
                                rp_grvChiTiet.DataSource = dtProduct;
                                rp_grvChiTiet.DataBind();
                                //lưu vào bảng kho hàng
                                var checkkh = (from kh in db.tbThuVien_TonKhos
                                               where kh.thuvien_sach_id == Convert.ToInt32(row["thuvien_sach_id"])
                                               select kh).SingleOrDefault();
                                var checksp = (from sp in db.tbThuVien_Saches
                                               where sp.thuvien_sach_id == Convert.ToInt32(row["thuvien_sach_id"])
                                               select sp).SingleOrDefault();
                                if (checkkh == null)
                                {
                                    tbThuVien_TonKho insert_slsp = new tbThuVien_TonKho();
                                    insert_slsp.tonkho_soluong = Convert.ToInt32(row["nhapsach_chitiet_soluong"]);
                                    insert_slsp.thuvien_sach_id = Convert.ToInt32(row["thuvien_sach_id"]);
                                    db.tbThuVien_TonKhos.InsertOnSubmit(insert_slsp);
                                    db.SubmitChanges();
                                }
                                else
                                {
                                    checkkh.tonkho_soluong = checkkh.tonkho_soluong + Convert.ToInt32(row["nhapsach_chitiet_soluong"]);
                                    db.SubmitChanges();
                                }
                                //checksp.product_price_entry = Convert.ToInt32(row["ctnh_gianhap"]);
                                db.SubmitChanges();
                                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "AlertBox", "swal('Nhập sách thành công!', '','success').then(function(){window.location = '/admin-nhap-sach';})", true);
                                //alert.alert_Success(Page, "Nhập sách thành công", "");
                            }
                        }
                    }
                }
                catch { }
                string matutang = Matutang();
                txtMaNhap.Value = matutang;
                txtNoiDung.Value = "";
            }
        }
    }

    protected void NhapHang_ServerClick(object sender, EventArgs e)
    {
        // kiểm tra id
        int _id = Convert.ToInt32(txt_ID.Value);
        if (Session["spChiTiet"] != null)
        {
            dtProduct = (DataTable)Session["spChiTiet"];
            // chạy foreach để lặp lại các row 
            foreach (DataRow row in dtProduct.Rows)
            {
                string product_id = row["thuvien_sach_id"].ToString();
                if (product_id == _id.ToString())
                {
                    // lưu data bằng input đầu vào
                    row.SetField("nhapsach_chitiet_soluong", txt_SoLuong.Value);
                    row.SetField("nhapsach_gianhap", txt_GiaTien.Value);
                    row.SetField("nhapsach_thanhtien", txt_TongTien.Value);
                    rp_grvChiTiet.DataSource = dtProduct;
                    rp_grvChiTiet.DataBind();
                }
            }
        }
    }

    protected void btnXoa_ServerClick(object sender, EventArgs e)
    {
        int _id = Convert.ToInt32(txt_ID.Value);
        dtProduct = (DataTable)Session["spChiTiet"];
        foreach (DataRow row in dtProduct.Rows)
        {
            string product_id = row["thuvien_sach_id"].ToString();
            if (product_id == _id.ToString())
            {
                dtProduct.Rows.Remove(row);
                Session["spChiTiet"] = dtProduct;
                break;
            }
        }
        rp_grvChiTiet.DataSource = dtProduct;
        rp_grvChiTiet.DataBind();
        alert.alert_Success(Page, "Xóa thành công", "");
    }
}