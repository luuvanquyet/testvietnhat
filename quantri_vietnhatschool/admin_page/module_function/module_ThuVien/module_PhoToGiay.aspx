﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="module_PhoToGiay.aspx.cs" Inherits="admin_page_module_function_module_ThuVien_module_PhoToGiay" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="/admin_css/vendor.css" />
    <link rel="stylesheet" href="/admin_css/app-blue.css" />
    <link href="../../admin_css/admin_style.css" rel="stylesheet" />
    <link href="../../admin_css/datepicker.min.css" rel="stylesheet" />
    <link href="../../css/Lan.css" rel="stylesheet" />
    <script src="../../../admin_js/sweetalert.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <br />
        <div class="container" style="text-align: center">
            <div style="margin-top: 20px">
                <h1 style="color: blue">HỆ THỐNG TRƯỜNG LIÊN CẤP VIỆT NHẬT</h1>
                <br />
                <h3>Tổng số lượng giấy đã in:<asp:Label ID="lblSoLuong" runat="server"></asp:Label></h3>
            </div>
            <br />
            <div class="col-4" style="display:list-item">
            </div>
            <div class="col-4">
                <div class="col-12">
                    <b>Chọn khối</b>
                    <select id="ddlBac" runat="server" class="form-control">
                        <option>Khối</option>
                        <option value="Mầm non">Mầm non</option>
                        <option value="Tiểu học">Tiểu học</option>
                        <option value="THCS-THPT">THCS-THPT</option>
                    </select>
                </div>
                <div class="col-12">
                    <b>Họ tên người photo</b>
                    <input type="text" id="txtHoTen" runat="server" class="form-control" />
                </div>
                <div class="col-12">
                    <b>Nội dung photo</b>
                    <input type="text" id="txtNoiDung" runat="server" class="form-control" />
                </div>
                <div class="col-12">
                    <b>Số lượng</b>
                    <input type="text" id="txtSoLuong" runat="server" class="form-control" />
                </div>
                <div class="col-12" style="margin-top: 10px">
                    <a href="#" id="btnLuu" runat="server" onserverclick="btnLuu_ServerClick" class="btn btn-primary">Lưu</a>
                </div>
            </div>
            <div class="col-4">
            </div>
        </div>
    </form>
</body>
</html>
