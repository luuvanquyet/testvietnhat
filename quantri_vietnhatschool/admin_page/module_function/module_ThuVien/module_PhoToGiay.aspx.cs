﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_ThuVien_module_PhoToGiay : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    protected void Page_Load(object sender, EventArgs e)
    {
        lblSoLuong.Text = (from pt in db.tbThuVien_PhoTos where pt.thuvien_tinhtrang == false select pt).Sum(pt=>pt.thuvien_soluong) + "";
    }
    protected void btnLuu_ServerClick(object sender, EventArgs e)
    {
        if (ddlBac.Value == "Khối" || txtHoTen.Value == "" || txtSoLuong.Value == "")
        {
            alert.alert_Error(Page, "Vui lòng nhập đầy đủ thông tin", "");
        }
        else
        {
            tbThuVien_PhoTo insert = new tbThuVien_PhoTo();
            insert.thuvien_bac = ddlBac.Value;
            insert.thuvien_ngay = DateTime.Now;
            insert.thuvien_photo_hoten = txtHoTen.Value;
            insert.thuvien_soluong = Convert.ToInt16(txtSoLuong.Value);
            insert.thuvien_tinhtrang = false;
            db.tbThuVien_PhoTos.InsertOnSubmit(insert);
            db.SubmitChanges();
            alert.alert_Success(Page, "Đã lưu", "");
            lblSoLuong.Text = (from pt in db.tbThuVien_PhoTos where pt.thuvien_tinhtrang == false select pt).Sum(pt => pt.thuvien_soluong) + "";
        }
    }
}