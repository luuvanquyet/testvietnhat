﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_WebSite_module_PhoToGiay_ThongKe : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (dteTungay.Value != "")
        {
            if(ddlbac.Value=="Tất cả")
            {
                var getData = from pt in db.tbThuVien_PhoTos
                              where pt.thuvien_ngay >= Convert.ToDateTime(dteTungay.Value) && pt.thuvien_ngay <= Convert.ToDateTime(dteDenNgay.Value)
                              select pt;
                grvList.DataSource = getData;
                grvList.DataBind();
                txtTongSoLuong.Value = getData.Sum(x => x.thuvien_soluong) + "";
            }
            else
            {
                var getData = from pt in db.tbThuVien_PhoTos
                              where pt.thuvien_ngay >= Convert.ToDateTime(dteTungay.Value) && pt.thuvien_ngay <= Convert.ToDateTime(dteDenNgay.Value)
                              && pt.thuvien_bac == ddlbac.Value
                              select pt;
                grvList.DataSource = getData;
                grvList.DataBind();
                txtTongSoLuong.Value = getData.Sum(x => x.thuvien_soluong) + "";
            }
            
           
        }
    }
    protected void btnXem_ServerClick(object sender, EventArgs e)
    {
        if (ddlbac.Value == "Tất cả")
        {
            var getData = from pt in db.tbThuVien_PhoTos
                          where pt.thuvien_ngay >= Convert.ToDateTime(dteTungay.Value) && pt.thuvien_ngay <= Convert.ToDateTime(dteDenNgay.Value)
                          select pt;
            grvList.DataSource = getData;
            grvList.DataBind();
            txtTongSoLuong.Value = getData.Sum(x => x.thuvien_soluong) + "";
        }
        else
        {
            var getData = from pt in db.tbThuVien_PhoTos
                          where pt.thuvien_ngay >= Convert.ToDateTime(dteTungay.Value) && pt.thuvien_ngay <= Convert.ToDateTime(dteDenNgay.Value)
                          && pt.thuvien_bac == ddlbac.Value
                          select pt;
            grvList.DataSource = getData;
            grvList.DataBind();
            txtTongSoLuong.Value = getData.Sum(x => x.thuvien_soluong) + "";
        }

    }
}