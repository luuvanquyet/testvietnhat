﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin_MasterPage.master" AutoEventWireup="true" CodeFile="module_ThongKeTonKho.aspx.cs" Inherits="admin_page_module_function_module_ThuVien_module_ThongKeTonKho" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headlink" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="hihead" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="himenu" runat="Server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="hibodyhead" runat="Server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="hibodywrapper" runat="Server">
    <div class="card card-block">
        <div class="container">
            <div class="row">
                <div class="form-group table-responsive">
                    <dx:ASPxGridView ID="grvList" runat="server"  ClientInstanceName="grvList" KeyFieldName="thuvien_sach_id" Width="100%">
                        <Columns>
                            <dx:GridViewCommandColumn ShowSelectCheckbox="True" SelectAllCheckboxMode="Page" VisibleIndex="0" Width="2%">
                            </dx:GridViewCommandColumn>
                            <dx:GridViewDataColumn Caption="Tên sách" FieldName="thuvien_sach_name" HeaderStyle-HorizontalAlign="Center" Width="40%" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn Caption="Tổng nhập" FieldName="tongnhap" HeaderStyle-HorizontalAlign="Center" Width="25%" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn Caption="Tổng mượn" FieldName="tongmuon" HeaderStyle-HorizontalAlign="Center" Width="25%" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>
                            <dx:GridViewDataColumn Caption="Số lượng còn lại" FieldName="tonkho" HeaderStyle-HorizontalAlign="Center" Width="25%" HeaderStyle-Font-Bold="true"></dx:GridViewDataColumn>
                        </Columns>
                        <SettingsSearchPanel Visible="true" />
                        <SettingsBehavior AllowFocusedRow="true" />
                        <SettingsText EmptyDataRow="Không có dữ liệu" SearchPanelEditorNullText="Gỏ từ cần tìm kiếm và enter..." />
                        <SettingsLoadingPanel Text="Đang tải..." />
                        <SettingsPager PageSize="10" Summary-Text="Trang {0} / {1} ({2} trang)"></SettingsPager>
                    </dx:ASPxGridView>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="hibodybottom" runat="Server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="hifooter" runat="Server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="hifootersite" runat="Server">
</asp:Content>

