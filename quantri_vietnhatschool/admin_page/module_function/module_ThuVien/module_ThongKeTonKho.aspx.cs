﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_ThuVien_module_ThongKeTonKho : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    protected void Page_Load(object sender, EventArgs e)

    {
        DataTable dtNhap = new DataTable();
        dtNhap.Columns.Add("thuvien_sach_id", typeof(int));
        dtNhap.Columns.Add("thuvien_sach_name", typeof(string));
        dtNhap.Columns.Add("tongnhap", typeof(int));
        dtNhap.Columns.Add("tongmuon", typeof(int));
        dtNhap.Columns.Add("tonkho", typeof(int));
        var getData = from s in db.tbThuVien_Saches
                      join ns in db.tbThuVien_NhapSach_ChiTiets on s.thuvien_sach_id equals ns.thuvien_sach_id
                      select new
                      {
                          s.thuvien_sach_id,
                          s.thuvien_sach_name,
                          ns.nhapsach_chitiet_soluong
                      };
        foreach (var item in getData)
        {
            int tongmuon = 0;
            //nếu datatable chưa có dòng nào thì thêm
            if (dtNhap.Rows.Count == 0)
            {
                DataRow row = dtNhap.NewRow();
                row["thuvien_sach_id"] = item.thuvien_sach_id;
                row["thuvien_sach_name"] = item.thuvien_sach_name;
                row["tongnhap"] = item.nhapsach_chitiet_soluong;
                /*kiểm tra xem sách này đã được mượn chưa
                *nếu chưa thì tổng mượn = 0 
                * và tính tồn kho
                */
                var getMuonSach = from ms in db.tbThuVien_BookSach_ChiTiets
                                  where ms.thuvien_sach_id == item.thuvien_sach_id
                                  select ms;
                if (getMuonSach.Count() == 0)
                {
                    tongmuon = 0;
                    //row["tonkho"] = item.nhapsach_chitiet_soluong;
                }
                else
                {
                    foreach (var ms in getMuonSach)
                    {
                        tongmuon += Convert.ToInt32(ms.booksach_chitiet_soluong);
                    }
                    row["tongmuon"] = tongmuon;
                }
                row["tonkho"] = Convert.ToInt32(row["tongnhap"]) - Convert.ToInt32(row["tongmuon"]);
                dtNhap.Rows.Add(row);
            }
            //nếu có rồi thì kiểm tra id nếu trùng thì count +1
            else
            {
                //int lichbaogiangchitiet_id = row["lichbaogiangchitiet_id"].ToString();
                DataRow row_id = dtNhap.Select("thuvien_sach_id = '" + item.thuvien_sach_id + "'").FirstOrDefault();
                /*kiểm tra xem sách này đã được mượn chưa
                *nếu chưa thì tổng mượn = 0 
                * và tính tồn kho
                */
                var getMuonSach = from ms in db.tbThuVien_BookSach_ChiTiets
                                  where ms.thuvien_sach_id == item.thuvien_sach_id
                                  select ms;
                
                if (getMuonSach.Count() == 0)
                {
                    tongmuon = 0;
                }
                else
                {
                    foreach (var ms in getMuonSach)
                    {
                        tongmuon += Convert.ToInt32(ms.booksach_chitiet_soluong);
                    }
                    //row2["tonkho"] = Convert.ToInt32(row2["tongnhap"]) - Convert.ToInt32(row2["tongmuon"]);
                }
                if (row_id != null)
                {
                    row_id["tongnhap"] = Convert.ToInt32(row_id["tongnhap"]) + item.nhapsach_chitiet_soluong;
                    row_id["tongmuon"] = tongmuon;
                    row_id["tonkho"] = Convert.ToInt32(row_id["tongnhap"]) - Convert.ToInt32(row_id["tongmuon"]);
                }
                else
                {
                    DataRow row2 = dtNhap.NewRow();
                    row2["thuvien_sach_id"] = item.thuvien_sach_id;
                    row2["thuvien_sach_name"] = item.thuvien_sach_name;
                    row2["tongnhap"] = item.nhapsach_chitiet_soluong;
                    row2["tongmuon"] = tongmuon;
                    row2["tonkho"] = Convert.ToInt32(row2["tongnhap"]) - Convert.ToInt32(row2["tongmuon"]);
                    dtNhap.Rows.Add(row2);
                }
            }

        }
        grvList.DataSource = dtNhap;
        grvList.DataBind();
    }
}