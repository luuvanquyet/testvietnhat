﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_ThuVien_module_TraSach : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public string adminName;
    DataTable dtProduct;
    public int stt = 1;
    int masp;
    string tensp;
    int sl;
    int gianhap;
    int thanhtien;
    cls_Alert alert = new cls_Alert();
    protected void Page_Load(object sender, EventArgs e)
    {
        //var list = from kh in db.tbThuVien_TonKhos
        //           join sp in db.tbThuVien_Saches on kh.thuvien_sach_id equals sp.thuvien_sach_id
        //           where kh.tonkho_soluong > 0
        //           select new
        //           {
        //               kh.tonkho_id,
        //               sp.thuvien_sach_name,
        //               sp.thuvien_sach_id
        //           };
        //grvList.DataSource = list;
        //grvList.DataBind();
        //lấy thông tin của tk đang nhập
        var getuser = (from u in db.admin_Users
                       where u.username_username == Request.Cookies["UserName"].Value
                       select u).FirstOrDefault();
        //lấy thông tin từ bảng booksach
        if (!IsPostBack)
        {
            int _id = Convert.ToInt32(RouteData.Values["id"]);
            var getdetail = (from nh in db.tbThuVien_BookSaches where nh.thuvien_booksach_id == _id select nh).SingleOrDefault();
            txtNhanVien.Value = (from u in db.admin_Users where u.username_id == getdetail.username_id select u).SingleOrDefault().username_fullname;
            txtName.Value = getdetail.khachhang_name;
            dteNgayMuon.Value = getdetail.thuvien_booksach_tungay.Value.ToString("yyyy-MM-dd").Replace(' ', 'T');
            dteNgayTra.Value = getdetail.thuvien_booksach_denngay.Value.ToString("yyyy-MM-dd").Replace(' ', 'T');
            txtBoPhan.Value = getdetail.thuvien_bophan + "";
            // nếu sestion chi tiet mà co du lieu roi thi khong cho chạy vao nua
            loaddatatable();
            //dtProduct = (DataTable)Session["spChiTiet"];
            //rp_grvChiTiet.DataSource = dtProduct;
            //rp_grvChiTiet.DataBind();
            if (RouteData.Values["id"] != null)
            {
                //get mã tự tăng của nhập hàng chi tiết
                var getctms = (from ctms in db.tbThuVien_BookSach_ChiTiets
                               join product in db.tbThuVien_Saches on ctms.thuvien_sach_id equals product.thuvien_sach_id
                               where getdetail.thuvien_booksach_id == ctms.thuvien_booksach_id && ctms.hidden==null
                               select new
                               {
                                   ctms.thuvien_sach_id,
                                   product.thuvien_sach_name,
                                   ctms.booksach_chitiet_soluong

                               });
                //loaddata ra datatable
                foreach (var item in getctms)
                {
                    DataRow row = dtProduct.NewRow();
                    row["thuvien_sach_id"] = item.thuvien_sach_id;
                    row["thuvien_sach_name"] = item.thuvien_sach_name;
                    row["booksach_chitiet_soluong"] = item.booksach_chitiet_soluong;
                    dtProduct.Rows.Add(row);
                };
                Session["TraSachChiTiet"] = dtProduct;
                rp_grvChiTiet.DataSource = dtProduct;
                rp_grvChiTiet.DataBind();
            }
        }
    }
    public void loaddatatable()
    {
        try
        {
            if (dtProduct == null)
            {
                dtProduct = new DataTable();
                dtProduct.Columns.Add("thuvien_sach_id", typeof(int));
                dtProduct.Columns.Add("thuvien_sach_name", typeof(string));
                dtProduct.Columns.Add("booksach_chitiet_soluong", typeof(int));
            }
        }
        catch { }
    }
    protected void btnDatHang_ServerClick(object sender, EventArgs e)
    {
        //try
        //{
        //    //kiểm tra add 2 lần có thêm vào gridview hay không
        //    int _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "thuvien_sach_id" }));
        //    var checkSanPham = (from sp in db.tbThuVien_Saches where sp.thuvien_sach_id == _id select sp).SingleOrDefault();
        //    if (Session["TraSachChiTiet"] != null)
        //    {
        //        dtProduct = (DataTable)Session["TraSachChiTiet"];
        //        DataRow[] row_id = dtProduct.Select("thuvien_sach_id = '" + _id + "'");
        //        if (row_id.Length != 0)
        //        {
        //            alert.alert_Warning(Page, "Sách này đã có trong danh sách cho mượn", "");
        //        }
        //        else
        //        {
        //            DataRow row = dtProduct.NewRow();
        //            row["thuvien_sach_id"] = checkSanPham.thuvien_sach_id;
        //            row["thuvien_sach_name"] = checkSanPham.thuvien_sach_name;
        //            row["booksach_chitiet_soluong"] = 1;
        //            dtProduct.Rows.Add(row);
        //            Session["TraSachChiTiet"] = dtProduct;
        //        }
        //    }
        //    else
        //    {
        //        loaddatatable();
        //        DataRow row = dtProduct.NewRow();
        //        row["thuvien_sach_id"] = checkSanPham.thuvien_sach_id;
        //        row["thuvien_sach_name"] = checkSanPham.thuvien_sach_name;
        //        row["booksach_chitiet_soluong"] = 1;
        //        dtProduct.Rows.Add(row);
        //        Session["TraSachChiTiet"] = dtProduct;
        //    }
        //    rp_grvChiTiet.DataSource = dtProduct;
        //    rp_grvChiTiet.DataBind();
        //}
        //catch { }
    }

    protected void DatHang_ServerClick(object sender, EventArgs e)
    {
        // kiểm tra id
        int _id = Convert.ToInt32(txt_ID.Value);
        if (Session["TraSachChiTiet"] != null)
        {
            dtProduct = (DataTable)Session["TraSachChiTiet"];
            // chạy foreach để lặp lại các row 
            foreach (DataRow row in dtProduct.Rows)
            {
                string product_id = row["thuvien_sach_id"].ToString();
                if (product_id == _id.ToString())
                {
                    // lưu data bằng input đầu vào
                    row.SetField("booksach_chitiet_soluong", txt_SoLuong.Value);
                    rp_grvChiTiet.DataSource = dtProduct;
                    rp_grvChiTiet.DataBind();
                }
            }
        }
    }

    protected void btnXoa_ServerClick(object sender, EventArgs e)
    {
        int _id = Convert.ToInt32(txt_ID.Value);
        dtProduct = (DataTable)Session["TraSachChiTiet"];
        foreach (DataRow row in dtProduct.Rows)
        {
            string product_id = row["thuvien_sach_id"].ToString();
            if (product_id == _id.ToString())
            {
                dtProduct.Rows.Remove(row);
                Session["TraSachChiTiet"] = dtProduct;
                break;
            }
        }
        rp_grvChiTiet.DataSource = dtProduct;
        rp_grvChiTiet.DataBind();
        alert.alert_Success(Page, "Xóa thành công!", "");
    }

    protected void btnTraSach_Click(object sender, EventArgs e)
    {
        dtProduct = (DataTable)Session["TraSachChiTiet"];
        if (txtBoPhan.Value == "" || dteNgayMuon.Value == "" || dteNgayTra.Value == "")
        {
            alert.alert_Warning(Page, "Bạn chưa nhập đầy đủ nội dung", "");
        }
        else
        {
            try
            {
                if (dtProduct.Rows.Count > 0)
                {
                    //lưu vào bảng trả sách
                    tbThuVien_TraSach insert = new tbThuVien_TraSach();
                    insert.thuvien_booksach_id = Convert.ToInt32(RouteData.Values["id"]);
                    insert.thuvien_trasach_ngaytra = DateTime.Now;
                    db.tbThuVien_TraSaches.InsertOnSubmit(insert);
                    db.SubmitChanges();
                    //lưu dữ liệu nhập hàng chi tiết trong table vào datatable
                    foreach (DataRow row in dtProduct.Rows)
                    {
                        // kiểm tra product này đã có trong bảng chi tiết
                        var checksp = (from ctms in db.tbThuVien_BookSach_ChiTiets
                                       where ctms.thuvien_booksach_id == Convert.ToInt32(RouteData.Values["id"])
                                       where ctms.thuvien_sach_id == Convert.ToInt32(row["thuvien_sach_id"]) && ctms.hidden==null
                                       select ctms);
                        //checkprdt kho hàng
                        var checkkh = (from kh in db.tbThuVien_TonKhos
                                       where kh.thuvien_sach_id == Convert.ToInt32(row["thuvien_sach_id"])
                                       select kh).SingleOrDefault();
                        foreach (var product in checksp)
                        {
                            if (product.thuvien_sach_id == Convert.ToInt32(row["thuvien_sach_id"]))
                            {
                                // nếu số lượng trả bằng số lượng mượn thì hidden record đó lại và cập nhật lại số lượng trong bảng tồn kho
                                if (product.booksach_chitiet_soluong == Convert.ToInt32(row["booksach_chitiet_soluong"]))
                                {
                                    product.hidden = false;
                                    //update lại số lượng tồn kho
                                    checkkh.tonkho_soluong = checkkh.tonkho_soluong + (Convert.ToInt32(row["booksach_chitiet_soluong"]));
                                    db.SubmitChanges();
                                }
                                //nếu số lượng trả nhỏ hơn số lượng mượn thì thêm record mới với kiểu gia hạn
                                if (product.booksach_chitiet_soluong > Convert.ToInt32(row["booksach_chitiet_soluong"]))
                                {
                                    //ẩn record cũ
                                    product.hidden = false;
                                    //thêm mới record
                                    tbThuVien_BookSach_ChiTiet insertXHCT = new tbThuVien_BookSach_ChiTiet();
                                    insertXHCT.thuvien_sach_id = Convert.ToInt32(row["thuvien_sach_id"]);
                                    insertXHCT.booksach_chitiet_soluong = (product.booksach_chitiet_soluong-Convert.ToInt32(row["booksach_chitiet_soluong"]));
                                    insertXHCT.thuvien_booksach_id = Convert.ToInt32(RouteData.Values["id"]);
                                    insertXHCT.booksach_chitiet_tungay = DateTime.Now;
                                    insertXHCT.booksach_chitiet_denngay = Convert.ToDateTime(dteNgayTra.Value);
                                    insertXHCT.booksach_chitiet_giahan = true;
                                    db.tbThuVien_BookSach_ChiTiets.InsertOnSubmit(insertXHCT);
                                    db.SubmitChanges();
                                    //update lại sl tồn kho = tồn kho + sl tra
                                    checkkh.tonkho_soluong = checkkh.tonkho_soluong + Convert.ToInt32(row["booksach_chitiet_soluong"]);
                                    db.SubmitChanges();
                                }
                                //lưu vào bảng trả sách chi tiết
                                tbThuVien_TraSach_ChiTiet ins = new tbThuVien_TraSach_ChiTiet();
                                ins.thuvien_trasach_id = insert.thuvien_trasach_id;
                                ins.thuvien_sach_id = Convert.ToInt32(row["thuvien_sach_id"]);
                                ins.trasach_chitiet_soluong = Convert.ToInt32(row["booksach_chitiet_soluong"]);
                                db.tbThuVien_TraSach_ChiTiets.InsertOnSubmit(ins);
                                db.SubmitChanges();
                            }
                        }
                    }
                    //kiểm tra nếu đã trả hết sách mượn thì ẩn record mượn sách
                    int count_tra = 0, count_muon=0;
                    //lấy ra list trả sách theo id mượn sách;
                    var getTS = from ts in db.tbThuVien_TraSaches
                                 where ts.thuvien_booksach_id == Convert.ToInt32(RouteData.Values["id"])
                                 select ts;
                    //lấy ra danh sách trả sách chi tiết
                    foreach (var item in getTS)
                    {
                        var getTSCT = from tsct in db.tbThuVien_TraSach_ChiTiets
                                      join ts in db.tbThuVien_TraSaches on tsct.thuvien_trasach_id equals ts.thuvien_trasach_id
                                      where tsct.thuvien_trasach_id == Convert.ToInt32(item.thuvien_trasach_id)
                                      select tsct;
                        foreach (var tsct in getTSCT)
                        {
                            count_tra = count_tra + Convert.ToInt32(tsct.trasach_chitiet_soluong);
                        }
                    }
                    //lấy tổng số sách mượn
                    var getMSCT = from msct in db.tbThuVien_BookSach_ChiTiets
                                  join ms in db.tbThuVien_BookSaches on msct.thuvien_booksach_id equals ms.thuvien_booksach_id
                                   where msct.thuvien_booksach_id == Convert.ToInt32(RouteData.Values["id"]) && msct.booksach_chitiet_giahan==null
                                   select msct;
                    foreach (var ms in getMSCT)
                    {
                        count_muon = count_muon + Convert.ToInt32(ms.booksach_chitiet_soluong);
                    }
                    if (count_tra==count_muon)
                    {
                        var getMS = (from nh in db.tbThuVien_BookSaches where nh.thuvien_booksach_id == Convert.ToInt32(RouteData.Values["id"]) select nh).SingleOrDefault();
                        getMS.hidden = false;
                        db.SubmitChanges();
                    }
                    dtProduct = (DataTable)Session["TraSachChiTiet"];
                    rp_grvChiTiet.DataSource = dtProduct;
                    rp_grvChiTiet.DataBind();
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "AlertBox", "swal('Trả sách thành công!', '','success').then(function(){window.location = '/admin-quan-ly-muon-sach';})", true);
                }
            }
            catch { }
        }
    }
    //}
}