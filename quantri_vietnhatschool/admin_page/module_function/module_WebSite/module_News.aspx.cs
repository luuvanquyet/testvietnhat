﻿using DevExpress.Web.ASPxHtmlEditor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class admin_page_module_function_module_News : System.Web.UI.Page
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    cls_Alert alert = new cls_Alert();
    private int _id;
    string image;
    protected void Page_Load(object sender, EventArgs e)
    {
        edtnoidung.Toolbars.Add(HtmlEditorToolbar.CreateStandardToolbar1());

        if (!IsPostBack)
        {
            Session["_id"] = 0;

        }
        loadData();

    }
    private void loadData()
    {
        var getData = from n in db.tbWebsite_News
                      join tb in db.tbWebsite_NewsCates on n.newcate_id equals tb.newscate_id
                      orderby n.news_datecreate descending
                      select new
                      {
                          n.news_id,
                          n.news_title,
                          n.news_summary,
                          tb.newscate_title,
                          n.news_image,
                          n.news_datecreate,
                          n.news_active,
                          n.news_trangchu,
                          n.news_trunghoccoso,
                          n.news_trunghocphothong,
                          n.news_tieuhoc,
                          n.news_mamnon,
                      };
        grvList.DataSource = getData;
        grvList.DataBind();
        ddlloaisanpham.DataSource = from tb in db.tbWebsite_NewsCates
                                    select tb;
        ddlloaisanpham.DataBind();

    }
    private void setNULL()
    {
        txtTieuDe.Text = "";
        edtnoidung.Html = "";
        txttomtat.Text = "";
        SEO_KEYWORD.Text = "";
        SEO_TITLE.Text = "";
        SEO_LINK.Text = "";
        SEO_DEP.Value = "";
        SEO_IMAGE.Text = "";
        txtMamNon.Checked = false;
        txtTieuHoc.Checked = false;
        txtTrangChu.Checked = false;
        txtTrungHocCS.Checked = false;
        txtTrungHocPT.Checked = false;
        //imgPreview.Src = "";
    }
    protected void btnThem_Click(object sender, EventArgs e)
    {
        Session["_id"] = 0;
        setNULL();
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Insert", "popupControl.Show();", true);
    }
    protected void btnChiTiet_Click(object sender, EventArgs e)
    {
        _id = Convert.ToInt32(grvList.GetRowValues(grvList.FocusedRowIndex, new string[] { "news_id" }));
        Session["_id"] = _id;
        var getData = (from n in db.tbWebsite_News
                       join tb in db.tbWebsite_NewsCates on n.newcate_id equals tb.newscate_id
                       where n.news_id == _id
                       select new
                       {
                           n.news_id,
                           n.news_title,
                           n.news_summary,
                           n.news_content,
                           tb.newscate_title,
                           tb.newscate_id,
                           n.news_image,
                           n.news_datecreate,
                           n.meta_keywords,
                           n.meta_title,
                           n.meta_description,
                           n.link_seo,
                           n.meta_image,
                           n.news_trangchu,
                           n.news_trunghoccoso,
                           n.news_trunghocphothong,
                           n.news_tieuhoc,
                           n.news_mamnon,
                       }).Single();
        txtTieuDe.Text = getData.news_title;
        ddlloaisanpham.Text = getData.newscate_title;
        edtnoidung.Html = getData.news_content;
        txttomtat.Text = getData.news_summary;
       // dteDate.Value = getData.news_datecreate.ToString();
        //imgPreview.Src = getData.news_image;
        SEO_KEYWORD.Text = getData.meta_keywords;
        SEO_TITLE.Text = getData.meta_title;
        SEO_LINK.Text = getData.link_seo;
        SEO_DEP.Value = getData.meta_description;
        SEO_IMAGE.Text = getData.meta_image;
        if (getData.news_trangchu == true)
        {
            txtTrangChu.Checked = true;
        }
        else
        {
            txtTrangChu.Checked = false;
        }
        if (getData.news_trunghocphothong == true)
        {
            txtTrungHocPT.Checked = true;
        }
        else
        {
            txtTrungHocPT.Checked = false;
        }
        if (getData.news_trunghoccoso == true)
        {
            txtTrungHocCS.Checked = true;
        }
        else
        {
            txtTrungHocCS.Checked = false;
        }
        if (getData.news_tieuhoc == true)
        {
            txtTrangChu.Checked = true;
        }
        else
        {
            txtTieuHoc.Checked = false;
        }
        if (getData.news_mamnon == true)
        {
            txtMamNon.Checked = true;
        }
        else
        {
            txtMamNon.Checked = false;
        }
        if (getData.news_image == null)
            ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupControl.Show();showImg1_1('" + "/admin_images/Preview-icon.png" + "'); ", true);
        else
            image = getData.news_image;
        ScriptManager.RegisterClientScriptBlock(Page, this.GetType(), "Detail", "popupControl.Show();showImg1_1('" + getData.news_image + "'); ", true);
        // loadData();
    }
    protected void btnXoa_Click(object sender, EventArgs e)
    {
        cls_News cls;
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "news_id" });
        if (selectedKey.Count > 0)
        {
            foreach (var item in selectedKey)
            {
                cls = new cls_News();
                tbWebsite_New checkImage = (from i in db.tbWebsite_News where i.news_id == Convert.ToInt32(item) select i).SingleOrDefault();
                if (cls.Linq_Xoa(Convert.ToInt32(item)))
                {
                    alert.alert_Success(Page, "Xóa thành công", "");
                }
                else
                    alert.alert_Error(Page, "Xóa thất bại", "");
            }
        }
        else
            alert.alert_Warning(Page, "Bạn chưa chọn dữ liệu", "");
        popupControl.ShowOnPageLoad = false;
        loadData();
    }
    public bool checknull()
    {
        if (txtTieuDe.Text != "" || edtnoidung.Html != "" || txttomtat.Text != "")
            return true;
        else return false;
    }
    protected void btnLuu_Click(object sender, EventArgs e)
    {

        if (Page.IsValid && FileUpload1.HasFile)
        {
            String folderUser = Server.MapPath("~/uploadimages/anh_tintuc/");
            if (!Directory.Exists(folderUser))
            {
                Directory.CreateDirectory(folderUser);
            }
            //string filename;
            string ulr = "/uploadimages/anh_tintuc/";
            HttpFileCollection hfc = Request.Files;
            string filename = Path.GetRandomFileName() + Path.GetExtension(FileUpload1.FileName);
            string fileName_save = Path.Combine(Server.MapPath("~/uploadimages/anh_tintuc"), filename);
            FileUpload1.SaveAs(fileName_save);
            image = ulr + filename;
        }
        admin_User logedMember = Session["AdminLogined"] as admin_User;
        cls_News cls = new cls_News();
        if (checknull() == false)
            alert.alert_Warning(Page, "Hãy nhập đầy đủ thông tin!", "");
        else
        {
            string KEYWORD = "", TitleSeo = "", Link = "", Dep = "", ImageSeo = "";
            {
                if (SEO_KEYWORD.Text != "")
                {
                    KEYWORD = SEO_KEYWORD.Text;
                }
                if (SEO_TITLE.Text != "")
                {
                    TitleSeo = SEO_TITLE.Text;
                }
                if (SEO_LINK.Text != "")
                {
                    Link = SEO_LINK.Text;
                }
                if (SEO_DEP.Value != "")
                {
                    Dep = SEO_DEP.Value;
                }
                if (SEO_IMAGE.Text != "")
                {
                    ImageSeo = SEO_IMAGE.Text;
                }
            }
            if (Session["_id"].ToString() == "0")
            {
              
                if (image == null)
                {
                    image = "/images/790x525.jpg";
                }
                else
                {
                }
                if (cls.Linq_Them(txtTieuDe.Text, image, txttomtat.Text, edtnoidung.Html, Convert.ToInt32(ddlloaisanpham.Value.ToString()), KEYWORD, TitleSeo, Link, Dep, ImageSeo, txtTrangChu.Checked, txtTrungHocPT.Checked, txtTrungHocCS.Checked, txtTieuHoc.Checked, txtMamNon.Checked))
                    alert.alert_Success(Page, "Thêm thành công", "");
                else alert.alert_Error(Page, "Thêm thất bại", "");

            }
            else
            {
                if (cls.Linq_Sua(Convert.ToInt32(Session["_id"].ToString()), txtTieuDe.Text, image, txttomtat.Text, edtnoidung.Html, Convert.ToInt32(ddlloaisanpham.Value.ToString()), KEYWORD, TitleSeo, Link, Dep, ImageSeo, txtTrangChu.Checked, txtTrungHocPT.Checked, txtTrungHocCS.Checked, txtTieuHoc.Checked, txtMamNon.Checked))
                    alert.alert_Success(Page, "Cập nhật thành công", "");
                else alert.alert_Error(Page, "Cập nhật thất bại", "");
            }
            popupControl.ShowOnPageLoad = false;
            loadData();
        }
    }

    protected void btnTrangChu_Click(object sender, EventArgs e)
    {
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "news_id" });
        if (selectedKey.Count > 0)
        {

            foreach (var item in selectedKey)
            {

                var getsp = (from sp in db.tbWebsite_News where sp.news_id == Convert.ToInt32(item) select sp).SingleOrDefault();
                if (getsp.news_trangchu == null || getsp.news_trangchu == false)
                {
                    getsp.news_trangchu = true;
                    db.SubmitChanges();
                    alert.alert_Success(Page, "Đã Xử Lí Xong!", "");
                }
                else
                {
                    if (getsp.news_trangchu == true)
                    {
                        getsp.news_trangchu = false;
                        db.SubmitChanges();
                        alert.alert_Success(Page, "Đã Xử Lí Xong!", "");
                    }
                    else
                    {
                        alert.alert_Error(Page, "Xử Lí thất bại!", "");
                    }
                }
            }
        }
    }

    protected void btnTrungHocPT_Click(object sender, EventArgs e)
    {
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "news_id" });
        if (selectedKey.Count > 0)
        {

            foreach (var item in selectedKey)
            {

                var getsp = (from sp in db.tbWebsite_News where sp.news_id == Convert.ToInt32(item) select sp).SingleOrDefault();
                if (getsp.news_trunghocphothong == null || getsp.news_trunghocphothong == false)
                {
                    getsp.news_trunghocphothong = true;
                    db.SubmitChanges();
                    alert.alert_Success(Page, "Đã Xử Lí Xong!", "");
                }
                else
                {
                    if (getsp.news_trunghocphothong == true)
                    {
                        getsp.news_trunghocphothong = false;
                        db.SubmitChanges();
                        alert.alert_Success(Page, "Đã Xử Lí Xong!", "");
                    }
                    else
                    {
                        alert.alert_Error(Page, "Xử Lí thất bại!", "");
                    }
                }
            }
        }
    }

    protected void btnTrungHocCS_Click(object sender, EventArgs e)
    {
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "news_id" });
        if (selectedKey.Count > 0)
        {

            foreach (var item in selectedKey)
            {

                var getsp = (from sp in db.tbWebsite_News where sp.news_id == Convert.ToInt32(item) select sp).SingleOrDefault();
                if (getsp.news_trunghoccoso == null || getsp.news_trunghoccoso == false)
                {
                    getsp.news_trunghoccoso = true;
                    db.SubmitChanges();
                    alert.alert_Success(Page, "Đã Xử Lí Xong!", "");
                }
                else
                {
                    if (getsp.news_trunghoccoso == true)
                    {
                        getsp.news_trunghoccoso = false;
                        db.SubmitChanges();
                        alert.alert_Success(Page, "Đã Xử Lí Xong!", "");
                    }
                    else
                    {
                        alert.alert_Error(Page, "Xử Lí thất bại!", "");
                    }
                }
            }
        }
    }

    protected void btnTieuHoc_Click(object sender, EventArgs e)
    {
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "news_id" });
        if (selectedKey.Count > 0)
        {

            foreach (var item in selectedKey)
            {

                var getsp = (from sp in db.tbWebsite_News where sp.news_id == Convert.ToInt32(item) select sp).SingleOrDefault();
                if (getsp.news_tieuhoc == null || getsp.news_tieuhoc == false)
                {
                    getsp.news_tieuhoc = true;
                    db.SubmitChanges();
                    alert.alert_Success(Page, "Đã Xử Lí Xong!", "");
                }
                else
                {
                    if (getsp.news_tieuhoc == true)
                    {
                        getsp.news_tieuhoc = false;
                        db.SubmitChanges();
                        alert.alert_Success(Page, "Đã Xử Lí Xong!", "");
                    }
                    else
                    {
                        alert.alert_Error(Page, "Xử Lí thất bại!", "");
                    }
                }
            }
        }
    }

    protected void btnMamNon_Click(object sender, EventArgs e)
    {
        List<object> selectedKey = grvList.GetSelectedFieldValues(new string[] { "news_id" });
        if (selectedKey.Count > 0)
        {

            foreach (var item in selectedKey)
            {

                var getsp = (from sp in db.tbWebsite_News where sp.news_id == Convert.ToInt32(item) select sp).SingleOrDefault();
                if (getsp.news_mamnon == null || getsp.news_mamnon == false)
                {
                    getsp.news_mamnon = true;
                    db.SubmitChanges();
                    alert.alert_Success(Page, "Đã Xử Lí Xong!", "");
                }
                else
                {
                    if (getsp.news_mamnon == true)
                    {
                        getsp.news_mamnon = false;
                        db.SubmitChanges();
                        alert.alert_Success(Page, "Đã Xử Lí Xong!", "");
                    }
                    else
                    {
                        alert.alert_Error(Page, "Xử Lí thất bại!", "");
                    }
                }
            }
        }
    }
    
}